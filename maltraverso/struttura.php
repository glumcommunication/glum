<?php
$thispage = "La Struttura";
include("header.php");
?>

<body>
<div id="maltraverso">
	<div id="all">
		<div id="left"><a href="index.php" title="Home"><img src="images/titolo2.png" width="37" height="459" alt="Centro Sportivo Maltraverso" /></a></div>
		<div id="container">
<?php
include("menu.php");
?>

	<div id="main">

			<div class="boxL">
				<div class="boxMTop">

						<p>La Struttura</p>

				</div>
				<div class="boxLContent">
					<img src="images/planimetria_maltraverso.png" width="840" height="474" alt="">
					<p>Ad appena 5 km dal centro di Poggibonsi e 2 km da Colle Val D’Elsa, immerso nel verde delle colline senesi, sorge 
					il <strong>Centro Sportivo Maltraverso</strong> che si estende su una superficie di oltre 70.000 mq. Nato dalla volontà e dall’esigenza 
					di creare una vera e propria casa per il Poggibonsi Calcio al fine di accrescere e sviluppare il senso di appartenenza 
					di calciatori e collaboratori dell'unione sportiva nei confronti del territorio e della città, Maltraverso viene inaugurato 
					negli anni  2000 e dal quel momento diventa la  nuova sede operativa del settore giovanile della Società <strong>U.S. Poggibonsi</strong>.</p>
					<p>La struttura è anche un centro di aggregazione per le famiglie dei bambini e dei ragazzi che compongono il Settore 
					della scuola calcio della <strong>A.S.D Leoni</strong>, nonché location ospitale per molti altri sport come la beach volley ed i tornei 
					degli amatori di calcio  a 7 ed a 5.</p>
					<p>Maltraverso può contare su qualificate strutture: campi da calcio e da beach soccer, 3 campi di calcio a sette in 
					erba sintetica, 2 campi di calcio a cinque in erba sintetica, campi da beach volley.</p>
					<h2>Sala Congressi</h2>
					<p>Maltraverso offre anche servizi per la ricettività, tra cui centro congressi con oltre 50 posti e proiettore e sale convegno 
					ideale per le conferenze stampa e riunioni con i giocatori, ma anche per conferenze e seminari, congressi, dibattiti e 
					assemblee, lezioni di formazioni.</p>
					<p>La cura dei dettagli offre un carattere distintivo ed esclusivo all’ambiente, il tutto perfettamente inserito tra verdi 
					vialetti, aiuole fiorite, zone relax fatte di confortevoli gazebi e comode sedie.
					</p>
					<h2>Bar e Ristorante</h2>
					<p>Il bar e il ristorante hanno un ruolo importante, meta ideale per degustare le varietà enogastronomiche del nostro 
					territorio. Prodotti tipici che vengono abbinati agli ingredienti classici della cucina italiana: pizzeria, caffetteria 
					e paninoteca , merende e piccole feste a tema e dolci fatti in casa, sono pensati per accompagnare la giornata degli ospiti  
					sportivi con il giusto divertimento e relax.</p>
					<p>Pranzi veloci per atleti, nel rispetto della dieta sportiva.</p>
					<!--<p>Il Centro Sportivo Maltraverso si trova a Poggibonsi ed è il quartier generale di tutte le formazioni giovanili dell'Us Poggibonsi calcio.         
					Il Centro Sportivo Maltraverso è una struttura polifunzionale inserita nella splendida cornice delle colline senesi che circondano la città di Poggibonsi.</p>-->
					<p>Il Centro Sportivo Maltraverso dispone di:</p>
					<ul class="customlist">					
					<li>campo principale di dimensioni 105 X 58 metri con fondo erboso ed impianto di lluminazione;</li>
					<li>campo sussidiario in terra battuta con impianto di illuminazione;</li>
					<li>n. 3 campi di calcio a sette in erba sintetica;</li>
					<li>n. 2 campi di calcio a cinque in erba sintetica;</li>
					<li>n. 1 campo da beach soccer / beach volley con fondo sabbioso;</li>
					<li>n. 8 spogliatoi.</li>
					</ul>
					
					<p>Il Centro Sportivo Maltraverso è dotato inoltre di:</p>
					<ul class="customlist">
					<li>Ristorante, bar;</li>
					<li>Area ricreativa (sia all'aperto che coperta);</li>
					<li>Area convegni;</li>
					<li>Servizi igienici;</li>
					<li>Segreteria sportiva;</li>
					<li>Ampio parcheggio.</li>
					</ul>
					<p>Maltraverso vuole essere un centro polivalente non solo rivolto al calcio. Un luogo accogliente e curato dove ritrovarsi per lavoro, per piacere 
					e per vivere da vicino la passione per lo sport.</p>
				</div>
			</div>
	
		</div>
	</div>

<div id="right"><img src="images/squadre2.png" width="104" height="285" alt="Squadre" usemap="#imgmap201211916523" />
<map id="imgmap201211916523" name="imgmap201211916523">
<area shape="poly" alt="U.S. Poggibonsi" title="U.S. Poggibonsi" coords="0,185,104,185,52,285" href="http://www.uspoggibonsi.it/" target="_blank" />
<area shape="rect" alt="Scuola Calcio" title="La Scuola Calcio" coords="0,0,104,14" href="scuolacalcio.php" target="_self" />

</div>



<?php
include("footer.php");
?>
</div>

</body>
</html>