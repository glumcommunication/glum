-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Nov 27, 2012 alle 12:55
-- Versione del server: 5.5.28
-- Versione PHP: 5.4.6-1ubuntu1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `maltraverso`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `Titolo` varchar(50) NOT NULL,
  `Testo` varchar(2000) NOT NULL,
  `Data` date NOT NULL,
  `Ora` time NOT NULL,
  `slug` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dump dei dati per la tabella `news`
--

INSERT INTO `news` (`Titolo`, `Testo`, `Data`, `Ora`, `slug`, `id`) VALUES
('News', '<p><strong>L''Asd Leoni in collaborazione con l''Us Poggibonsi &egrave; lieta di darvi il benvenuto al 1&deg; Summer Football Camp!</strong></p>\r\n<p>Il Summer Football Camp &egrave; l''occasione per tutti i ragazzi e le ragazze di affinare e migliorare le proprie capacit&agrave; calcistiche, tecniche ed atletiche. Nella splendida cornice del Centro Sportivo di Maltraverso istruttori qualificati dell''Us Poggibonsi seguiranno tutti i partecipanti insegnando loro i segreti di questo meraviglioso sport ed i valori che sono fondamentali per praticare al meglio questa disciplina: correttezza, lealt&agrave;, fair play e rispetto dell''avversario. Il tutto in un''atmosfera familiare e ricca di divertimento. L''evento &egrave; stato realizzato con il patrocinio del Comune di Poggibonsi e della Uisp di Siena.</p>', '2012-10-15', '11:00:54', 'news', 6),
('1Â° Summer Football Camp', '<p><strong>1&deg; Summer Football Camp - Un successo a tinte giallorosse</strong></p>\r\n<p>Si &egrave; concluso sabato scorso il 1&deg; Summer Football Camp organizzato dall''Us Poggibonsi in collaborazione con l''Asd Leoni. Durante la settimana del Camp i ragazzi partecipanti hanno avuto modo di migliorare le proprie attitudini calcistiche e di imparare i fondamentali tecnici e di fair play di questo meraviglioso sport: il tutto nella splendida cornice del Centro Sportivo di Maltraverso a Poggibonsi. <br />A fine corso gli istruttori Stefano Polidori, Riccardo Di Pisello, Riccardo Giacopelli e Paolo Conforti hanno consegnato a tutti i ragazzi un attestato di partecipazione al Campus.</p>', '2012-10-15', '11:04:49', '1-summer-football-camp', 7);

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `md5_id` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `full_name` tinytext COLLATE latin1_general_ci NOT NULL,
  `user_name` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `user_email` varchar(220) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `user_level` tinyint(4) NOT NULL DEFAULT '1',
  `pwd` varchar(220) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `address` text COLLATE latin1_general_ci NOT NULL,
  `country` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `tel` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `fax` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `website` text COLLATE latin1_general_ci NOT NULL,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `users_ip` varchar(200) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `approved` int(1) NOT NULL DEFAULT '0',
  `activation_code` int(10) NOT NULL DEFAULT '0',
  `banned` int(1) NOT NULL DEFAULT '0',
  `ckey` varchar(220) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `ctime` varchar(220) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_email` (`user_email`),
  FULLTEXT KEY `idx_search` (`full_name`,`address`,`user_email`,`user_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=55 ;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `md5_id`, `full_name`, `user_name`, `user_email`, `user_level`, `pwd`, `address`, `country`, `tel`, `fax`, `website`, `date`, `users_ip`, `approved`, `activation_code`, `banned`, `ckey`, `ctime`) VALUES
(54, '', 'admin', 'admin', 'admin@localhost', 5, '4c09e75fa6fe36038ac240e9e4e0126cedef6d8c85cf0a1ae', 'admin', 'Switzerland', '4433093999', '', '', '2010-05-04', '', 1, 0, 0, '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
