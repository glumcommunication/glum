<?php
$thisPage = "Prenotazione campi";
include("header.php");
?>
<body>
<div id="maltraverso">
	<div id="all">
		<div id="left"><a href="index.php" title="Home"><img src="images/titolo2.png" width="37" height="459" alt="Centro Sportivo Maltraverso" /></a></div>
		<div id="container">
<?php
include("menu.php");
echo <<<_END
<div id="main">
<div class="boxL">
				<div class="boxMTop">

						<p>Prenota on line i nostri campi!</p>

				</div>
<div class="boxLContent">
_END;
if ($_POST['doBook']=='Prenota') {
$nome = $telefono = $email = $numero = $data = $start = $end = $campo = $privacy = '0';
$nome = $_POST[nome];
$telefono = $_POST[telefono];
$email = $_POST[email];
$data = $_POST[giorno] . "/" . $_POST[mese] . "/" . $_POST[anno];
$start = $_POST[ora_inizio] . ":" . $_POST[minuto_inizio];
$end = $_POST[ora_fine] . ":" . $_POST[minuto_fine];
$campo = $_POST[campo];
$messaggio = $_POST[messaggio];
$privacy = $_POST[privacy];
$nomino = str_shuffle(str_replace(' ','',substr($nome, -5)));
$active_code = rand(1,99999);
$bookcode = str_shuffle($nomino.$active_code);
if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
$controllo = 0;
} else {
$controllo = 1;
}
if ($nome!=false && $telefono!=false && $email!=false && $data!=false && $start!=false && $end!=false && $campo!=false && $privacy=="accetto")
{
	if ($controllo!=1)
	{
echo <<<_END

<form method="post" action="send.php">
<table id="tableadmin">
	<tr>
		<td>Nome e Cognome:</td>
		<td>$nome</td>	
	</tr>
	<tr>
		<td>Telefono:</td>
		<td>$telefono</td>	
	</tr>
	<tr>
		<td>Email:</td>
		<td>$email</td>	
	</tr>
		<td>Data:</td>
		<td><strong>$data</strong> dalle <strong>$start</strong> alle <strong>$end</strong></td>	
	</tr>
	<tr>
		<td>Campo:</td>
		<td>a <strong>$campo</strong></td>	
	</tr>	
	<tr>
		<td>Messaggio:</td>
		<td>$messaggio</td>	
	</tr>
   <tr>
		<td colspan="2" align="center">
		Se i dati inseriti sono corretti conferma la tua richiesta di prenotazione.          
  		</td>
   </tr>    
   <tr>
		<td colspan="2" align="center">
		<input type="hidden" name="nome" value="$nome" />
		<input type="hidden" name="telefono" value="$telefono" />
		<input type="hidden" name="email" value="$email" />
		<input type="hidden" name="data" value="$data" />
		<input type="hidden" name="start" value="$start" />
		<input type="hidden" name="end" value="$end" />
		<input type="hidden" name="campo" value="$campo" />
		<input type="hidden" name="messaggio" value="$messaggio" />
		<input type="submit" name="doSend" class="btn" value="Conferma" />          
  		</td>
   </tr>    
</table>
</form>
_END;
	}
	else
	{
		echo "<p>Spiacente <span style=\"color:#db2326\">indirizzo email non valido</span>.</p>
			<p><a href='javascript:window.history.back();' style='color: #222222;font-weight:bold;'>Torna indietro</a> e inserisci un indirizzo email valido.</p>";
	}
}
else
{
	echo "<p>Non sono stati riempiti tutti i campi obbligatori</p>
	<p>I seguenti campi sono <span style=\"color:#db2326\">obbligatori</span>:
	<ul class=\"customlist\">";
if ($nome==false) {echo "<li style=\"color: #db2326;\">Nome e Cognome</li>";}
if ($telefono==false) {echo "<li style=\"color: #db2326;\">Telefono</li>";}
if ($email==false) {echo "<li style=\"color: #db2326;\">Email</li>";}
if ($data==false) {echo "<li style=\"color: #db2326;\">Data</li>";}
if ($start==false) {echo "<li style=\"color: #db2326;\">Ora inizio</li>";}
if ($end==false) {echo "<li style=\"color: #db2326;\">Ora fine</li>";}
if ($campo==false) {echo "<li style=\"color: #db2326;\">Campo</li>";}
if ($privacy==false) {echo "<li style=\"color: #db2326;\">Norme sulla privacy</li>";}
	echo "</ul>";
	echo "<p><a href='javascript:window.history.back();' style='color: #222222;font-weight:bold;'>Torna indietro</a> per completare correttamente il form.</p>";
}
}

/*
if ($nome!=false && $telefono!=false && $email!=false && $data!=false && $start!=false && $end!=false && $campo!=false && $privacy=="accetto")
{
	if ($controllo!=1)
	{

*/
if ($_POST['doSend']=='Conferma') {


$nome = $_POST[nome];
$telefono = $_POST[telefono];
$email = $_POST[email];
$data = $_POST[data];
$start = $_POST[start];
$end = $_POST[end];
$campo = $_POST[campo];
$messaggio = $_POST[messaggio];
$headers = "";
$headers .= "From: $email\n";
$headers .= "Reply-To: $email\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=iso-8859-1\n";
$to = "info@maltraverso.it";
$subject = "NEW - Richiesta prenotazione campo a " . $campo;
$message = "Hai ricevuto una nuova richiesta di prenotazione, ecco i dettagli:<br />
Nome: $nome<br />
Telefono: $telefono<br />
E-mail: $email<br />
Data: $data<br />
Orario: $start - $end<br />
Campo: a $campo<br />
Messaggio: $messaggio<br />";

mail ('assunta.maltraverso@gmail.com',$subject,$message,$headers);
		 
		echo "<p>La Vostra richiesta &egrave; stata inviata, risponderemo 
		al pi&ugrave; presto per confermare la prenotazione del campo. Se l'orario da te scelto dovesse essere gi&agrave; prenotato 
				sarai ricontattato telefonicamente per concordare un nuovo orario. Grazie.</p> 
		<p><a href='index.php'>BACK</a></p>";

	}
	/*
	else
	{
		echo "<p>Spiacente <span style=\"color:#db2326\">indirizzo email non valido</span>.</p>
			<p><a href='javascript:history.go(-1)' style='color: #222222;font-weight:bold;'>Torna indietro</a> e inserisci un indirizzo email valido.</p>";
	}
}
else
{
	echo "<p>Non sono stati riempiti tutti i campi obbligatori</p>
	<p>I seguenti campi sono <span style=\"color:#db2326\">obbligatori</span>:
	<ul class=\"customlist\">";
if ($nome==false) {echo "<li style=\"color: #db2326;\">Nome e Cognome</li>";}
if ($telefono==false) {echo "<li style=\"color: #db2326;\">Telefono</li>";}
if ($email==false) {echo "<li style=\"color: #db2326;\">Email</li>";}
if ($data==false) {echo "<li style=\"color: #db2326;\">Data</li>";}
if ($start==false) {echo "<li style=\"color: #db2326;\">Ora inizio</li>";}
if ($end==false) {echo "<li style=\"color: #db2326;\">Ora fine</li>";}
if ($campo==false) {echo "<li style=\"color: #db2326;\">Campo</li>";}
if ($privacy==false) {echo "<li style=\"color: #db2326;\">Norme sulla privacy</li>";}
	echo "</ul>";
	echo "<p><a href='javascript:history.go(-1)' style='color: #222222;font-weight:bold;'>Torna indietro</a> per completare correttamente il form.</p>";
}*/

?>
</div>
</div>
</div>
	</div>

<div id="right"><img src="images/squadre2.png" width="104" height="245" alt="" /></div>

</div>



<?php
include("footer.php");
?>
</div>

</body>
</html>
