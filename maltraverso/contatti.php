<?php
$thispage = "Contatti";
include("header.php");
?>

<body>
<div id="maltraverso">
	<div id="all">
		<div id="left"><a href="index.php" title="Home"><img src="images/titolo2.png" width="37" height="459" alt="Centro Sportivo Maltraverso" /></a></div>
		<div id="container">
<?php
include("menu.php");
?>

		<div id="main">
			<div class="boxL">

				<div class="boxMTop">

						<p>Dove siamo</p>

				</div>
				<div class="boxLContent">
					<p><strong>Centro Sportivo Maltraverso
					<br />Località Maltraverso
					<br />53036 Poggibonsi (SI)
					</strong></p>
					<p>Come raggiungere il Centro Sportivo di Maltraverso<br />
					<strong>Da Firenze:</strong> seguire le indicazioni per Siena (raccordo autostradale Siena-Firenze) uscita Colle Val d'Elsa Nord, 
					prendere per Poggibonsi, dopo circa 2 km voltare a destra e seguire le indicazioni per "Impianti Sportivi Maltraverso". <br />
					<strong>Da Siena:</strong> seguire le indicazioni per Firenze (raccordo autostradale Siena-Firenze) uscita Colle Val d'Elsa 
					Nord, prendere per Poggibonsi, dopo circa 2 km voltare a destra e seguire le indicazioni per "Impianti Sportivi Maltraverso". <br />
					<strong>Dalla SS 429:</strong>   imboccare lo svincolo di Drove, entrare nel raccordo autostradale Siena-Firenze (direzione Siena) 
					ed uscire a Colle Val d'Elsa Nord. Prendere per Poggibonsi e  dopo circa 2 km voltare a destra seguendo le indicazioni per 
					"Impianti Sportivi Maltraverso".
					</p>
					<p><strong>Google Maps</strong> <br />
					<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
					src="https://maps.google.it/maps/ms?msid=206267696275610757214.0004bf1f1302cf67e5be7&amp;msa=0&amp;ie=UTF8&amp;t=h&amp;ll=43.436904,11.155758&amp;spn=0.021814,0.036478&amp;z=14&amp;output=embed">
					</iframe><br />
					<small>Visualizza <a href="https://maps.google.it/maps/ms?msid=206267696275610757214.0004bf1f1302cf67e5be7&amp;msa=0&amp;ie=UTF8&amp;t=h&amp;ll=43.436904,11.155758&amp;spn=0.021814,0.036478&amp;z=14&amp;source=embed" style="color:#0000FF;text-align:left">Centro Sportivo Maltraverso - Poggibonsi</a> in una mappa di dimensioni maggiori</small>
					</p>
					
				</div>
				<div class="boxMTop">
					<div class="boxMBorderTop"></div>
					<div class="boxMTitle">
						<p>Contatti</p>
					</div>
					<div class="boxMBorderBottom"></div>
				</div>
				<div class="boxLContent">
				<p>Centro Sportivo Maltraverso, <br />Località Maltraverso, <br />53036 Poggibonsi (SI).   <br />Tel. 0577.1520038 - Fax 0577.1520489   <br />e-mail: 
				<script type='text/javascript'>
				<!--
				var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
				var path = 'hr' + 'ef' + '=';
				 var addy50598 = 'm&#97;ltr&#97;v&#101;rs&#111;' + '&#64;';
				 addy50598 = addy50598 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
				 var addy50599 = 'inf&#111;' + '&#64;';
				 addy50599 = addy50599 + 'm&#97;ltr&#97;v&#101;rs&#111;' + '&#46;' + 'it';
				 document.write('<a ' + path + '\'' + prefix + ':' + addy50599 + '\'>');
				 document.write(addy50599);
				 document.write('<\/a>');
				 //-->\n </script><script type='text/javascript'>
				 <!--
				 document.write('<span style=\'display: none;\'>');
				 //-->
				 </script>Questo indirizzo email è protetto dagli spambots. E' necessario abilitare JavaScript per vederlo.
				 <script type='text/javascript'>
				 <!--
				 document.write('</');
				 document.write('span>');
				 //-->
				 </script></p> 
					
				</div>				
			</div>
		</div>
</div>

<div id="right"><img src="images/squadre2.png" width="104" height="285" alt="Squadre" usemap="#imgmap201211916523" />
<map id="imgmap201211916523" name="imgmap201211916523">
<area shape="poly" alt="U.S. Poggibonsi" title="U.S. Poggibonsi" coords="0,185,104,185,52,285" href="http://www.uspoggibonsi.it/" target="_blank" />
<area shape="rect" alt="Scuola Calcio" title="La Scuola Calcio" coords="0,0,104,14" href="scuolacalcio.php" target="_self" />

	</div>



<?php
include("footer.php");
?>

	</div>

</body>
</html>