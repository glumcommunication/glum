<?php
$thispage = "Prenota on line i nosti campi";
include("header.php");
?>

<body>
<div id="maltraverso">
	<div id="all">
		<div id="left"><a href="index.php" title="Home"><img src="images/titolo2.png" width="37" height="459" alt="Centro Sportivo Maltraverso" /></a></div>
		<div id="container">
<?php
include("menu.php");
?>

	<div id="main">

			<div class="boxL">
				<div class="boxMTop">

						<p>Prenota i nostri campi!</p>

				</div>

				<div class="boxLContent">
				<p>E' possibile prenotare uno dei nostri campi telefonicamente utilizzando il numero riportato in basso.</p>
				<p style="text-align: center;"><img src="images/telefono.png" width="266" height="100" alt=""></p>
				<p>In alternativa puoi richiedere una prenotazione on line. Compila i campi sottostanti per prenotare il campo all'ora 
				desiderata, riceverai una conferma dell'avvenuta prenotazione all'indirizzo email che ci hai fornito. Se l'orario da 
				te scelto dovesse essere già prenotato sarai ricontattato telefonicamente per fissare un nuovo orario.</p>
				<form  action="send.php"  method="post" id="form">
    				<table id="tableadmin" style="margin: 0 auto;">
              <tr>
              <td><span style="color: #db2326;">* Campi obbligatori</span></td>
              <td>&nbsp;</td>
              </tr>    				
                <tr >
                  <td style="width:170px;"><label >
                      <div align="left">Nome e cognome* :</div>
                      </label>
                  </td>
                  <td><input type="text" name="nome"  value="" id="q1" maxlength="20" size="30" required="required"/>
                  </td>
                </tr>
                <tr >
                  <td><label >
                      <div align="left">Telefono* :</div>
                    </label>
                  </td>
                  <td><input type="text" name="telefono"  value="" id="q2" maxlength="20" size="30" required="required"/></td>
                </tr>
                <tr >
                  <td><label >
                      <div align="left">Email* :</div>
                    </label>
                  </td>
                  <td><input type="text" name="email"  value="" id="q3"  size="30" required="required"/></td>
                </tr>
                 <tr >
        <td>Data* :</td><td> <select name="giorno" size"1">
<?php        
 $oggi = getdate();
$year = $oggi['year'];
$month = $oggi['mon'];
$day = $oggi['mday'];
$hour = $oggi['hours'];
$minute = $oggi['minutes'];

    for ($giorno = 1; $giorno <= 31; $giorno++) {
        echo "<option value=\"" . $giorno . "\"";
        if ($giorno == $day) {
            echo "selected=\"selected\"";
        }
        echo ">" . $giorno . "</option>";
    }

    echo <<<_END
        </select>
        <select name="mese" size"1">
        
_END;

    $mesi = array("", "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre");
    for ($mese = 1; $mese <= 12; $mese++) {
        echo "<option value=\"" . $mese . "\"";
        if ($mese == $month) {
            echo "selected=\"selected\"";
        }
        echo ">" . $mesi[$mese] . "</option>";
    }

    echo <<<_END
        </select>
        <select name="anno" size"1">
        
_END;

    for ($anno = 1900; $anno <= 2050; $anno++) {
        echo "<option value=\"" . $anno . "\"";
        if ($anno == $year) {
            echo "selected=\"selected\"";
        }
        echo ">" . $anno . "</option>";
    }

    echo <<<_END
        </select></td>
_END;
?>
</tr><tr>
       <td>Dalle* :<select name="ora_inizio" size"1">
<?php        
 $oggi = getdate();



    for ($ora_inizio = 0; $ora_inizio <= 23; $ora_inizio++) {
        echo "<option value=\"" . $ora_inizio . "\"";
        if ($ora_inizio == $hour) {
            echo "selected=\"selected\"";
        }
        echo ">" . $ora_inizio . "</option>";
    }

?>

        </select>
			<select name="minuto_inizio" size"1">
			<option value="00" label="00" selected="selected">00</option>
			<option value="15" label="15">15</option>
			<option value="30" label="30">30</option>
			<option value="45" label="45">45</option>						
        </select>
        </td>        

       <td>Alle* :<select name="ora_fine" size"1">
<?php        
 $oggi = getdate();



    for ($ora_fine = 0; $ora_fine <= 23; $ora_fine++) {
        echo "<option value=\"" . $ora_fine . "\"";
        if ($ora_fine == $hour + 1) {
            echo "selected=\"selected\"";
        }
        echo ">" . $ora_fine . "</option>";
    }

?>

        </select>
			<select name="minuto_fine" size"1">
			<option value="00" label="00" selected="selected">00</option>
			<option value="15" label="15">15</option>
			<option value="30" label="30">30</option>
			<option value="45" label="45">45</option>						
        </select>
        </td>   										
					</tr>
                <tr >
                  <td><label>
                      <div align="left">Campo* :</div>
                    </label></td>
                  <td><input type="radio"  name="campo" class="other" id="q14_" value="11" />
                      <label>a 11 </label>
                      <input type="radio"  name="campo" class="other" id="q14_" value="7" />
                      <label>a 7 </label>                      
                      <input type="radio"  name="campo" class="other" id="q14_" value="5" />
                      <label>a 5 </label>

                  </td>
                </tr>					             
                
                <tr >
                  <td height="50" valign="top" ><label>
                      <div align="left"><br />
                        Messaggio :</div>
                    </label>
                  </td>
                  <td height="50"><textarea cols="30" rows="3" name="messaggio" class="text" id="q5"></textarea>
                  </td>
                </tr>
                <tr >
                  <td><label>
                      <div align="left">Privacy**</div>
                    </label></td>
                  <td><input type="checkbox"  name="privacy" class="other" id="q14_" value="accetto" required="required"/>
                      <label>accetto </label>
                  

                  </td>
                </tr>


              <tr>
              <td colspan="2" align="center">
				<input type="submit" name="doBook" class="btn" value="Prenota" />          
              </td>
              
              </tr>              
            </table>
         </form>
         <h6>
         ** <strong>INFORMATIVA per il trattamento dei dati personali</strong>. Ai sensi dell'art. 13 del D.Lgs. n. 196/2003 la raccolta dei 
         suoi dati personali viene effettuata registrando i dati da lei stesso forniti, in qualità di interessato, al momento 
         della richiesta di invio del suo messaggio. I dati personali sono trattati per le seguenti finalità: a) consentire 
         l'invio del suo messaggio contenente i commenti e le opinioni che vorrà esprimere; b) realizzare indagini dirette a 
         verificare il grado di soddisfazione degli utenti sui servizi offerti o richiesti. Per garantire l'efficienza del servizio, 
         la informiamo inoltre che i dati potrebbero essere utilizzati per effettuare prove tecniche e di verifica.
         </h6>
					</div>
				</div>
			</div>
	
		</div>


<div id="right"><img src="images/squadre2.png" width="104" height="285" alt="Squadre" usemap="#imgmap201211916523" />
<map id="imgmap201211916523" name="imgmap201211916523">
<area shape="poly" alt="U.S. Poggibonsi" title="U.S. Poggibonsi" coords="0,185,104,185,52,285" href="http://www.uspoggibonsi.it/" target="_blank" />
<area shape="rect" alt="Scuola Calcio" title="La Scuola Calcio" coords="0,0,104,14" href="scuolacalcio.php" target="_self" />

</div>



<?php
include("footer.php");
?>
</div>

</body>
</html>