<?php
$pagename ="thankyou";
include 'header.php';
include 'menu.php';
?>
<div id="main">


<div class="boxL">
	<div class="boxLContent">

<h2>Grazie!</h2>
      <h3>La tua registrazione &egrave; stata completata.</h3>
      <p>Ti &egrave; stata inviata una email per confermare e attivare l'account, se non la ricevi controlla la cartella 
      della posta indesiderata. Nella email troverai un link per l'attivazione.<br />
      Se hai gi&agrave; attivato il tuo account puoi effettuare l'accesso andando a <a href="login.php">questa pagina</a>.
</div>
</div>
</div>
<?php
include 'footer.php';
?>