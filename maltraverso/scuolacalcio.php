<?php
$thispage = "La Scuola Calcio";
include("header.php");
?>

<body>
<div id="maltraverso">
	<div id="all">
		<div id="left"><a href="index.php" title="Home"><img src="images/titolo2.png" width="37" height="459" alt="Centro Sportivo Maltraverso" /></a></div>
		<div id="container">
<?php
include("menu.php");
?>

	<div id="main">

			<div class="boxL">
				<div class="boxMTop">

						<p>La Scuola Calcio</p>

				</div>
				<div class="boxLContent">
					<!--<p>L'obiettivo cardine della Scuola Calcio ASD LEONI è di far vivere un'esperienza sportiva unica sana e divertente a tutti i bambini che si allenano nelle varie formazioni.<br />
					Gli allenatori, infatti, hanno il compito di concentrare gli allenamenti sia sullo sviluppo delle abilità tecniche, sia sulla cura dei rapporti interpersonali al fine di garantire una crescita formativa a 360 gradi.</p> 
					<p>Sono 7 le squadre che  per l'anno 2012/13 si alleneranno nella Scuola Calcio ASD LEONI presso la struttura polifunzionale di Maltraverso.</p>-->
					<p>
					Grazie ai numerosi campi da calcio, il Centro Sportivo rappresenta il quartier generale di tutte le formazioni della A.S.D. 
					Leoni e le giovanili dell'Us Poggibonsi (esordienti fair play, giovanissimi professionisti b, giovanissimi professionisti, 
					giovanissimi nazionali, allievi nazionali, berretti).</p>
					<p>L'obiettivo cardine della Scuola Calcio A.S.D. LEONI è di far vivere un'esperienza sportiva unica sana e divertente 
					a tutti i bambini che si allenano nelle varie formazioni.</p>
					<p>Gli allenatori, infatti, hanno il compito di concentrare gli allenamenti sia sullo sviluppo delle abilità tecniche, 
					sia sulla cura dei rapporti interpersonali al fine di garantire una crescita formativa a 360 gradi.</p>
					<p>Sono 7 le squadre che  per l'anno 2012/13 si alleneranno nella Scuola Calcio ASD LEONI presso la struttura polifunzionale di Maltraverso.					
					</p>
					<div class="team">
					<p><strong>ASD LEONI PULCINI anno 2001</strong> 
					<br /><em>Allenatore</em>: RONDINELLA LUCIANO 
					<br /><em>Accompagnatore</em>: MILVATTI NICOLA
					<ul class="customlist">
					<li>LUCA’ANDREA ROSARIO</li>
					<li>MILVATTI SALVATORE</li>
					<li>MOLITERNO JACOPO</li>
					<li>MUROLO LUCIANO</li>
					<li>POLI MATTIA</li>
					<li>SIGNORINI LAPO</li>
					<li>SIGNORINI LORENZO</li>
					</ul>
					</p>
					</div>
					<div class="team">
					<p><strong>ASD LEONI PULCINI anno 2002</strong>
					<br /><em>Allenatore</em>: MADAU FEDERICO
					<br /><em>Accompagnatore</em>: CECCATELLI ALESSIO
					<ul class="customlist">
					<li>ABBATE MATTEO</li>
					<li>AIEZZA ALESSIO</li>
					<li>CASTALDO MANUEL</li>
					<li>CECCATELLI FILIPPO</li>
					<li>CORTI ALBERTO</li>
					<li>DENTALE GIOVANNI</li>
					<li>DI COSTANZO MANUEL</li>
					<li>NEZIRI GIANLUCA</li>
					<li>SALVADORI LEONARDO</li>
					<li>SAVENTI SAMUELE</li>
					<li>SYLLA SECK SERIGNE SALIOU</li>
					<li>TALIANI ANDREA</li>
					</ul>
					</p>
					</div>
					<div class="team">					
					<p><strong>ASD LEONI PULCINI anno 2003</strong>
					<br /><em>Allenatore</em>: STEFANO NERI
					<br /><em>Accompagnatore</em>: NICOLA GAGGELLI
					<ul class="customlist">
					<li>COKO ELMIR</li>
					<li>GIOVENCO NICCOLO’</li>
					<li>MANGIACAPRE LUIGI</li>
					<li>PESCE GIANLUCA</li>
					<li>PICCINI PIETRO</li>
					<li>SALVADORI LAPO</li>
					<li>SIMEONE SIMONE</li>
					</ul>
					</p>
					</div>
					<div class="team">				
					<p><strong>ASD LEONI PULCINI anno 2004</strong>
					<br /><em>Allenatore</em>: STEFANO NERI
					<br /><em>Accompagnatore</em>: NICOLA GAGGELLI 
					<ul class="customlist">
					<li>BERTI COSIMO</li>
					<li>GAGGELLI MARCO</li>
					<li>MILVATTI ANGELO</li>
					<li>NENCINI GABRIELE</li>
					<li>POLIDORI FRANCESCO</li>
					<li>SANLEOLINI SAMUELE</li><br />
					
					</ul>
					</p>
					</div>
					<div class="team">
					<p><strong>ASD LEONI PULCINI anno 2005</strong>
					<br /><em>Allenatore</em>: RAFFAELE MONZITTA 
					<br /><em>Accompagnatori</em>: FRANCESCO GUERRANTI,<br /> DAVID FUSI, ILIO BRUNI 
					<ul class="customlist">
					<li>BAGLIO GABRIELE</li>
					<li>GIOVENCO SAMUELE</li>
					<li>GUERRANTI RICCARDO</li>
					<li>MANCINI FRANCESCO</li>
					<li>MANCINI SANDRO</li>
					<li>SPANNOCCHI MILO</li>
					</ul>
					</p>
					</div>
					<div class="team">
					<p><strong>ASD LEONI PULCINI anno 2006</strong>
					<br /><em>Allenatore</em>: RAFFAELE MONZITTA 
					<br /><em>Accompagnatori</em>: FRANCESCO GUERRANTI,<br /> DAVID FUSI, ILIO BRUNI 
					<ul class="customlist">
					<li>DI MAIO MIRKO</li>
					<li>FUSI LORENZO</li>
					<li>LORENZINI NICCOLO’</li>
					<li>PAOLUCCI LEONARDO</li>
					<li>ROSATI NICCOLO’</li><br />
					</ul>
					</p>
					</div>
					<div class="team">
					<p><strong>ASD LEONI PULCINI anno 2007</strong>
					<br /><em>Allenatore</em>: RAFFAELE MONZITTA 
					<br /><em>Accompagnatori</em>: FRANCESCO GUERRANTI,<br /> DAVID FUSI, ILIO BRUNI 
					<ul class="customlist">
					<li>BRUNI LAPO</li>
					<li>CAPEZZUOLI MATTIA</li>
					<li>CARNASCIALI LEONARDO</li>
					<li>GHERARDOTTI MICHELANGELO</li>
					<li>VANNINI LORENZO</li>
					</ul>
					</p>
					</div>
				</div>
			</div>
	
		</div>
	</div>

<div id="right"><img src="images/squadre2.png" width="104" height="285" alt="Squadre" usemap="#imgmap201211916523" />
<map id="imgmap201211916523" name="imgmap201211916523">
<area shape="poly" alt="U.S. Poggibonsi" title="U.S. Poggibonsi" coords="0,185,104,185,52,285" href="http://www.uspoggibonsi.it/" target="_blank" />
<area shape="rect" alt="Scuola Calcio" title="La Scuola Calcio" coords="0,0,104,14" href="scuolacalcio.php" target="_self" />

</div>



<?php
include("footer.php");
?>
</div>

</body>
</html>