<?php
include "header.php";

$img = $_GET['img'];
$prev = $img - 1;
$next = $img + 1;
$min=5;
$max=8;
if($next>$max){
    $next=$min;
}
if($prev<$min){$prev=$max;}
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<div class="container">
    <br/><br/>
    <div class="row">
        <div class="col-xs-1">
            <a href="foto.php?img=<?php echo $prev;?>"><span style= "font-size: 40px; color:black; margin-top: 500%;" class="glyphicon glyphicon-arrow-left"></span></a>
        </div>
        <div class="col-xs-10" class="text-center">
            <img src="img/<?php echo $img; ?>.jpg" class="img-responsive" style="margin:auto;"/>

        </div>
        <div class="col-xs-1">
            <a href="foto.php?img=<?php echo $next;?>"><span style= "font-size: 40px; color:black; margin-top: 500%;" class="glyphicon glyphicon-arrow-right"></span></a>
        </div>
    </div>
    <br/>
    <div class="row" class="text-center">
        <div class="col-sm-4  text-center"> 
            <a href="index.php" class="center"><button  class="btn btn-default text-center">Guarda il Progetto</button></a>
        </div>
        <div class="fb-like" data-href="http://www.sienasiamonoi.it/foto.php?img=<?php echo $img; ?>.jpg" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
    </div>
</div>