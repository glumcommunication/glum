<?php include "header.php"; ?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <a href=""><img src="img/progetto-schema.jpg" class="img-responsive"></a>

        </div>
       
    </div>
    <div class="row">
        <div class="col-sm-12">
            <p><p>&ldquo;Siena Siamo Noi &ndash; Volti di senesi per scelta e per amore&rdquo; &egrave; un progetto ideato da Samuele Mancini e GLuM Communication che vuole raccontare con i ritratti fotografici gli abitanti di Siena. Non solo senesi ma tutti coloro che &ldquo;per scelta e per amore&rdquo; oggi vivono nella nostra citt&agrave;. Pensiamo agli studenti ad esempio, che scelgono di venire a studiare a Siena perch&eacute; credono ancora nella qualit&agrave; dell&#39;offerta formativa dell&#39;ateneo senese. Pensiamo a tutti coloro che &ldquo;per amore&rdquo; di questa citt&agrave; decidono di restare a vivere qui nonostante tutto. L&#39;idea parte dalla consapevolezza che oggi pi&ugrave; che mai sia necessario stare dalla parte di chi crede ancora nella citt&agrave; e di chi, spesso silenziosamente e nell&#39;ombra, continua a mostrare sul suo volto la voglia di fare e di non cedere di fronte al difficile momento di crisi.</p>

<p>&ldquo;Siena Siamo Noi&rdquo; vuole mostrare il volto di chi Siena la vive quotidianamente, di chi lotta per un suo rilancio, di chi la ama.</p>

<p>&nbsp;</p>

<p>PARTECIPA ANCHE TU!</p>

<p>Le fotografie verranno scattate dal 18 Agosto al 20 Settembre tra le 12:30 e le 15:00 nei seguenti luoghi della citt&agrave;:</p>

<ul>
	<li>
	<p>Piazza Matteotti / Piazza Gramsci</p>
	</li>
	<li>
	<p>Piazza Tolomei / Piazza Salimbeni</p>
	</li>
	<li>
	<p>Piazza del Campo</p>
	</li>
	<li>
	<p>Piazza del Duomo</p>
	</li>
	<li>
	<p>Piazza Indipendenza</p>
	</li>
	<li>
	<p>Via Pantaneto / Logge del Papa</p>
	</li>
	<li>
	<p>Porta Romana / Ponte di Romana</p>
	</li>
	<li>
	<p>Massetana Romana</p>
	</li>
	<li>
	<p>Polo universitario San Miniato</p>
	</li>
</ul>

<p>&nbsp;</p>

<p>UNA MOSTRA FOTOGRAFICA</p>

<p>Il progetto avr&agrave; il suo culmine con la realizzazione di un evento espositivo in cui verranno mostrate le foto realizzate a coloro hanno voluto partecipare con il proprio volto al progetto.</p>

<p>&nbsp;</p>

<p>#SIENA2019</p>

<p>Il progetto &ldquo;Siena Siamo Noi&rdquo; supporta la candidatura di Siena a Capitale Europea della Cultura 2019. La candidatura rappresenta per Siena un&rsquo;opportunit&agrave; importante di rilancio della sua identit&agrave; e della sua economia per costruire un nuovo modello di sviluppo locale fondato sulla cultura.</p>

<p>&nbsp;</p>
</p>

        </div>
      
    </div>
</div>

<?php include "./footer.php"; ?>