<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>SienaSiamoNoi</title>
        <meta name="description" content="Siena Siamo Noi – Volti di senesi per scelta e per amore è un progetto ideato da Samuele Mancini e GLuM Communication che vuole raccontare con i ritratti fotografici gli abitanti di Siena.">
        <meta name="keyword" content="siena siamo noi, mostra fotografica, foto evento, birreria la diana">
        <meta name="viewport" content="width=device-width">
        <script src="js/modernizr.min.js"></script>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="stylesheets/style.css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700' rel='stylesheet' type='text/css'>
        <link id="page_favicon" href="favicon.ico" rel="icon" type="image/x-icon" />
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-53636396-1', 'auto');
            ga('send', 'pageview');

        </script>
    </head>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <body>
        <header>
            <div class="container">
                <div class="row">
                    <div id="logo" class="col-xs-7 col-sm-6">
                        <a href="index.php"><img src="img/logo.png" class="img-responsive"></a>
                    </div>
                    <div class="col-xs-3 col-sm-5 ">
                        <nav class="navbar navbar-default" role="navigation">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                    <ul class="nav navbar-nav navbar-right ">
                                        <li><a href="index.php">HOME</a></li>
                                        <li><a href="progetto.php">PROGETTO</a></li>
                                        <li><a href="contatti.php">CONTATTI</a></li>
                                    </ul>
                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container-fluid -->

                        </nav>
                    </div>
                    <div class="col-xs-1 col-sm-1 text-left">
                        <a href="https://www.facebook.com/sienasiamonoi" target="_blank"><img id="facebook" src="img/facebook.png" ></a>
                    </div>
                </div>
            </div>
        </header>