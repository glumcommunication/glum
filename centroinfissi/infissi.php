<?php
$title = 'Home';
include 'header.php';
?>
    
      
<div id="container">
	<div class="mainbox">
		<h2>Gli infissi  </h2>
  		<p>L'azienda Centro Infissi snc di Siena offre un vasto assortimento di <strong>marchi prestigiosi</strong> quali 
  		FISTRALL, EVERALL, CORUS, SCHUCO, così da garantire una qualità dei propri prodotti. Da noi potete trovare una vasta gamma di infissi:</p>
			<ul>
				<li>Finestre di alluminio e di alluminio legno</li>
				<li>Finestre di legno e di legno alluminio</li>
				<li>Finestre di pvc e di pvc legno</li>
				<li>Persiane in legno, in alluminio e in pvc</li>
				<li> Facciate continue</li>
			</ul>

  		<div style="margin-left:57px; margin-top: 30px;">
<img src="images/foto/finestre.jpg" width="886" usemap="#Map" />
<map name="Map" id="Map">
  <area shape="rect" coords="4,213,203,246" href="http://www.schueco.com/web/it/privatkunden/fenster_und_tueren/products/finestre" target="_blank" />
  <area shape="rect" coords="210,211,398,249" href="http://www.sverniciatura.it/02.html" target="_blank" />
  <area shape="rect" coords="414,212,630,248" href="http://everall.it/prodotti/realizzazioni/gallery-lavori" />
  <area shape="rect" coords="640,212,888,248" href="http://www.finstral.com/web/it/Referenze/PVC-e-PVC-alluminio-U1254833335017.1U.html" target="_blank" />
</map>
</div>

</div>
   	  
</div>

<?php
include 'footer.php';
?>