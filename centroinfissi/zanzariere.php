<?php
$title = 'Home';
include 'header.php';
?>
    
      
<div id="container">
	<div class="mainbox">
   	<h2>Sistemi di oscuramento e zanzariere</h2>
		<p>La ditta Centro Infissi snc  offre un vasto assortimento di: <strong>zanzariere avvolgibili</strong>, <strong>tende da sole, veneziane, frangisole</strong>. 
		Per proteggervi dalle calde giornate estive, Centro Infissi propone solo prodotti di alta qualità che durano nel tempo.</p>
		<div style="margin-left:7px; margin-top: 30px;"><img src="images/foto/prodotti_zanzariere.jpg" width="886" usemap="#Map">
			<map name="Map" id="Map">
				<area shape="rect" coords="5,206,199,246" href="http://www.modusinterni.com/02/index.php?option=com_content&view=section&id=6&Itemid=84" target="_blank" />
    			<area shape="rect" coords="207,210,408,247" href="http://www.grifoflex.it/Catalog.aspx" target="_blank" />
			   <area shape="rect" coords="419,212,622,248" href="http://www.pratic.it/prodotti/tende_da_sole.php" target="_blank" />
			   <area shape="rect" coords="633,212,878,247" href="http://www.lupakmetal.com/manuali.php" target="_blank" />
			</map>
  		</div>
	</div>
</div>
<?php
include 'footer.php';
?>