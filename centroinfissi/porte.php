<?php
$title = 'Home';
include 'header.php';
?>
    
      
<div id="container">
	<div class="mainbox">
   	<h2>Le Porte interne</h2>
  		<p>Centro Infissi snc è specializzata nel settore serramenti e infissi e offre un vasto assortimento di porte, dallo 
  		stile classico a quello moderno; dai materiali massello, laminato, vetrate, cristallo. Inoltre Centro Infissi offre una 
  		vasta gamma di porte laminate pronta consegna. I nostri marchi sono: <strong>Dierre, Garofoli, Gidea, Scrigno</strong>.
  		</p>

		<div style="margin-left:7px; margin-top: 30px;">
		<img src="images/foto/prodotti_porte.jpg" width="886" usemap="#Map">
		<map name="Map" id="Map">
			<area shape="rect" coords="3,211,203,247" href="http://www.gidea.it/it/viewdoc.asp?co_id=33" target="_blank" />
			<area shape="rect" coords="207,213,391,245" href="http://www.dierre.com/it/porte/gamma-prodotti/gamma-prodotti.html" target="_blank" />
			<area shape="rect" coords="414,214,628,248" href="http://www.scrigno.it/" target="_blank" />
			<area shape="rect" coords="634,214,895,453" href="http://www.garofoli.com/it/viewdoc.asp?co_id=18" target="_blank" />
		</map>
		</div>
	</div>
</div>

<?php
include 'footer.php';
?>