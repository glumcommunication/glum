<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Centro Infissi Siena- Home</title>
<meta name= "keywords" content= "Infissi, Siena, Tende, Porte, Verande, Tende da sole, Zanzariere, Verande, Portoni, Renaccio, Azienda, Vendita"  /> 
<link href='http://fonts.googleapis.com/css?family=Satisfy' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">


<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-31487748-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


<!-- JavaScripts-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="js/s3Slider.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#slider1').s3Slider({
            timeOut: 7000 
        });
    });
</script>


</head>

<body>
<div style="margin:0 auto; width: 1020px;">
<div id="main">
<div id="header">
    <div class="logo">
    	<a href="index.php"><img src="images/logocrop.png" width="408" height="116" alt="centro infissi siena"></a>
    </div>
<?php
include 'menu.php';
?>    
</div>