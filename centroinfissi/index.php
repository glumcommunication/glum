<?php
$title = 'Home';
include 'header.php';
?>
    
      
  <div id="container">
  
<!--    <h2>Per una casa bella dentro e fuori</h2>-->
    <div id="slider1">
        <ul id="slider1Content">
            <li class="slider1Image">
     
                <a href="infissi.html"><img src="example_images/2.jpg" alt="Infissi" border="0" /></a>
                <span class="right"><strong>Infissi</strong><br /> Finestre e persione di ogni genere, sempre e solo di marche affidabili e prestigiose: Finestrall, Everal, Chorus, Schuco </span></li>
            <li class="slider1Image">
                       <a href="porte.html"><img src="example_images/1.jpg" alt="Porte" border="0"/></a>
                <span class="left"><strong>Porte </strong><br />Le migliori marche a disposizione per i nostri clienti: Garofoli, Gidea, Sgrigno, Adielle<br /> Marche di qualità per proporre una gamma di porte dallo stile classico fino ad arrivare a quello moderno</span></li>
            <li class="slider1Image">
                <a href="sicurezza.html"> <img src="example_images/3.jpg" alt="Sicurezza" border="0"/></a>
                <span class="right"><strong>Sicurezza</strong><br />Una casa sicura è una casa serena. Sistemi di protezione di qualità con Torterolo e Re, Erreci Sicurezza</span></li>
            <li class="slider1Image">
             <a href="zanzariere.html">   <img src="example_images/4.jpg" alt="Sistemi di oscuramento" border="0" /></a>
                <span class="left"><strong>Sistemi di oscuramento e zanzariere</strong><br /> Tende da sole, Veneziane, Zanzariere, Frangisole e Pannelli solari, un ricco repertorio per godere al meglio la tua estate. Pratic, Modus, Grifo Flex, Lupak Metal, sono le marche consigliate per questi prodotti.</span></li>
            <div class="clear slider1Image"></div>
        </ul>
    </div>
   </div>
<?php
include 'footer.php';
?>