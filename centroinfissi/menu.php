<div style="float:right;">

<h2 style="font-family: 'Satisfy', cursive;">Per una casa bella dentro e fuori...</h2>
<div id="nav">
	<ul>
		<li><a href="azienda.php">Azienda</a>

		<li><a href="prodotti.php">Prodotti</a>
			<ul>
				<li><a href="infissi.php">Infissi</a></li>
				<li><a href="serramenti.php">Serramenti</a></li>
				<li><a href="persiane.php">Persiane</a></li>				
				<li><a href="porte.php">Porte interne</a></li>
				<li><a href="sicurezza.php">Sicurezza</a></li>
				<li><a href="scale.php">Scale</a></li>
				<li><a href="basculanti.php">Basculanti</a></li>
				<li><a href="cassonetti.php">Cassonetti termoisolanti</a></li>
				<li><a href="tagliafuoco.php">Tagliafuoco</a></li>																				
				<li><a href="zanzariere.php">Sistemi di oscuramento e Zanzariere</a></li>				
			</ul>
		</li>
		<li><a href="preventivo.php">Preventivo</a></li>
		<li><a href="agevolazioni.php">Agevolazioni</a>
			<ul>
				<li><a href="infissi.php">Infissi</a></li>
				<li><a href="serramenti.php">Serramenti</a></li>
				<li><a href="persiane.php">Persiane</a></li>	
			</ul>
		</li>		
		<li><a href="lavori.php">Lavori eseguiti</a></li>
		<li><a href="contatti.php">Contatti</a></li>
	</ul>
</div>
</div>
