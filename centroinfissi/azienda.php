<?php
$title = 'Home';
include 'header.php';
?>
    
      
<div id="container">
	<div class="mainbox">
		<div class="testo">
			<h2>L'azienda</h2>
			<p>
      		Le prime esperienze lavorative nel campo dei serramenti per Casini M. e Scarpelli F. ricadono nel campo della 
      		sicurezza con blindati, grate e allarmi.<br>
      		Poi con il tempo, acquisendo l'esperienza necessaria, decidono di fondare la Centro Infissi snc e diventare 
      		rivenditori di marchi prestigiosi come: Ballan, Bertolotto, Dierre, Torterolo & Re, Finstral, Korus, Garofoli, 
      		Hydro, Ipea, Metra, Mobirolo, Mottura, MP Infissi, Portedi, Schuco, Velux, Gidea, Door 200, Scrigno.<br>
				La Centro Infissi snc, grazie a precise scelte operative, anche se molto giovane perchè nata nel 2004, è in 
				continuo e progressivo sviluppo nel settore dei serramenti ed infissi.<br>
				La particolare attenzione che dedica alle esigenze ed all'andamento del mercato ha fatto sì che l'azienda possa 
				fornire ad ogni cliente sempre più elevate garanzie di qualità ed affidabilità dei prodotti proposti.<br>
				Per saperne di più venite subito a scoprire tutti i nostri prodotti nell'apposita sezione.
			</p>
		</div>
		<div class="immagine"><img src="images/negozio.png" width="550"></div>
	</div>
</div>


<?php
include 'footer.php';
?>