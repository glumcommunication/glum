<?php
$title = 'Home';
include 'header.php';
?>
    
      
<div id="container">
	<div class="mainbox">
		<div class="testo">
			<h2>Prodotti</h2>
			<p>La ditta Centro Infissi snc è specializzata nel settore serramenti e infissi e offre un vasto assortimento di:</p>
			<ul>
				<li>Porte, portoni, basculanti,  scorrevoli, facciate continue</li>
				<li>Porte blindate,  porte tagliafuoco</li>
				<li>Finestre in alluminio, legno e pvc</li>
				<li>Verande di alluminio</li>
				<li>Infissi di alluminio</li>
				<li>Persiane blindate</li>
				<li>Zanzariere avvolgibili</li>
				<li> tende da sole</li>
				<li> pannelli solari</li>
    		</ul>    
        	<p>La passione e l'impegno dei titolari si traducono in un lavoro sempre preciso e accurato, nel rispetto della tempistica e del progetto stabilito.<br>
         L'esperienza acquisita negli anni, la professionalità, la competenza e un continuo aggiornamento fanno del nostro negozio 
         un autentico punto di riferimento in tutta Siena e provincia.</p>
         <p>Per conoscere nel dettaglio tutte le nostre offerte o per ricevere ulteriori informazioni, non esitate a contattarci. </p>
		</div>
  		<div class="immagine">
  			<img src="images/centroinfissi.jpg">
  		</div>
  		<div style="margin-left:75px;">
		<img src="images/fornitori.jpg" width="750" height="100">
		</div>   
  	</div>
</div>  
<?php
include 'footer.php';
?>