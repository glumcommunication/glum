
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Centro Infissi Siena- Home</title>
<meta name= "keywords" content= "Infissi, Siena, Tende, Porte, Verande, Tende da sole, Zanzariere, Verande, Portoni, Renaccio, Azienda, Vendita"  /> 

<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-31487748-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


<!-- JavaScripts-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="js/s3Slider.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#slider1').s3Slider({
            timeOut: 4000 
        });
    });
</script>


</head>

<body>
<div id="main">
<div id="header">
    <div class="logo">
    	<a href="index.php"><img src="images/logocrop.png" width="408" height="116" alt="centro infissi siena"></a>
    </div>

<div id="nav">
	<ul>
		<li><a href="index.php">Home</a></li>
		<li><a href="azienda.php">Azienda</a>

		<li><a href="prodotti.php">Prodotti</a>
			<ul>
				<li><a href="infissi.php">Infissi</a></li>
				<li><a href="serramenti.php">Serramenti</a></li>
				<li><a href="persiane.php">Persiane</a></li>				
				<li><a href="porte.php">Porte interne</a></li>
				<li><a href="sicurezza.php">Sicurezza</a></li>
				<li><a href="scale.php">Scale</a></li>
				<li><a href="basculanti.php">Basculanti</a></li>
				<li><a href="cassonetti.php">Cassonetti termoisolanti</a></li>
				<li><a href="tagliafuoco.php">Tagliafuoco</a></li>																				
				<li><a href="zanzariere.php">Sistemi di oscuramento e Zanzariere</a></li>				
			</ul>
		</li>
		<li><a href="preventivo.php">Preventivo</a></li>
		<li><a href="agevolazioni.php">Agevolazioni</a>
			<ul>
				<li><a href="infissi.php">Infissi</a></li>
				<li><a href="serramenti.php">Serramenti</a></li>
				<li><a href="persiane.php">Persiane</a></li>	
			</ul>
		</li>		
		<li><a href="lavori.php">Lavori eseguiti</a></li>
		<li><a href="contatti.php">Contatti</a></li>
	</ul>
</div>
    
</div>    
      
<div id="container">
	<div class="mainbox">
		<div class="testo">
			<h2>Prodotti</h2>
			<p>La ditta Centro Infissi snc è specializzata nel settore serramenti e infissi e offre un vasto assortimento di:</p>
			<ul>
				<li>Porte, portoni, basculanti,  scorrevoli, facciate continue</li>
				<li>Porte blindate,  porte tagliafuoco</li>
				<li>Finestre in alluminio, legno e pvc</li>
				<li>Verande di alluminio</li>
				<li>Infissi di alluminio</li>
				<li>Persiane blindate</li>
				<li>Zanzariere avvolgibili</li>
				<li> tende da sole</li>
				<li> pannelli solari</li>
    		</ul>    
        	<p>La passione e l'impegno dei titolari si traducono in un lavoro sempre preciso e accurato, nel rispetto della tempistica e del progetto stabilito.<br>
         L'esperienza acquisita negli anni, la professionalità, la competenza e un continuo aggiornamento fanno del nostro negozio 
         un autentico punto di riferimento in tutta Siena e provincia.</p>
         <p>Per conoscere nel dettaglio tutte le nostre offerte o per ricevere ulteriori informazioni, non esitate a contattarci. </p>
		</div>
  		<div class="immagine">
  			<img src="images/centroinfissi.jpg" width="550">
  		</div>
  		<div style="margin-left:75px;">
		<img src="images/fornitori.jpg" width="750" height="100">
		</div>   
  	</div>
</div>  
	<div id="footer">
		<div class="copyright">
  			<p><strong>Centro Infissi Siena</strong> - Loc. Renaccio, Via della Pace 13 53100 Siena</p>
    		<p>Tel. 0577 532037 | Fax. 0577 225405 | Email info@centroinfissisiena.com<strong> </strong>|  P.I. 01112510522</p>
		</div>

   	<div class="footer_autori">
			<p>Realizzato da: <a href="http://www.idpromoter.it/" target="_blank"><img src="images/idpromoter.png" width="87" height="21" alt="Idpromoter" border="0"/></a></p>
		</div>
	</div>
</div>

</body>
</html>