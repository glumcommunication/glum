-- phpMyAdmin SQL Dump
-- version 3.4.7.1
-- http://www.phpmyadmin.net
--
-- Host: 62.149.150.158
-- Generato il: Gen 30, 2013 alle 11:22
-- Versione del server: 5.5.28
-- Versione PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Sql556026_1`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `food_list`
--

CREATE TABLE IF NOT EXISTS `food_list` (
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(2) NOT NULL,
  `genre` int(2) NOT NULL,
  `active` int(2) NOT NULL,
  `future` int(2) NOT NULL AUTO_INCREMENT,
  KEY `future` (`future`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dump dei dati per la tabella `food_list`
--

INSERT INTO `food_list` (`name`, `price`, `type`, `genre`, `active`, `future`) VALUES
('Spaghetti', '4.50', 1, 0, 0, 1),
('Tagliolini', '3.50', 0, 0, 0, 2),
('Bistecca', '5', 1, 1, 1, 3),
('Patatine', '2.50', 1, 2, 0, 4);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
