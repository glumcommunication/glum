<?php
$pagename = "prodotti";
$pagetitle = "Prodotti";
include_once 'header.php';
?>
	<div id="main">
		<div class="mainContent" id="other">
			<h1>PRODOTTI</h1>
			<h2>Primi</h2>
			<div class="otherL">
				<div class="productT">
					<div class="productR">
						<div class="productC itemP">Paella</div>
						<div class="productC priceP">&euro; ...</div>																
					</div>
					<div class="productR">
						<div class="productC itemP">Gnocchi del Pescatore</div>
						<div class="productC priceP">&euro; ...</div>																
					</div>
					<div class="productR">
						<div class="productC itemP">Risotto di Mare</div>
						<div class="productC priceP">&euro; ...</div>																
					</div>
					<div class="productR">
						<div class="productC itemP">Pennette all'Arrabbiata</div>
						<div class="productC priceP">&euro; ...</div>																
					</div>
					<div class="productR">
						<div class="productC itemP">Tagliolino allo Scogli</div>
						<div class="productC priceP">&euro; ...</div>																
					</div>
					<div class="productR">
						<div class="productC itemP">Pennette al Salmone</div>
						<div class="productC priceP">&euro; ...</div>																
					</div>
					<div class="productR">
						<div class="productC itemP">Tagliatelle ai Funghi</div>
						<div class="productC priceP">&euro; ...</div>																
					</div>
					<div class="productR">
						<div class="productC itemP">Spaghetti alle Vongole</div>
						<div class="productC priceP">&euro; ...</div>																
					</div>
					<div class="productR">
						<div class="productC itemP">Orecchiette del Contadino</div>
						<div class="productC priceP">&euro; ...</div>																
					</div>
				</div>
			</div>
			<div class="otherR">
				<div class="productT">
					<div class="productR">
						<div class="productC itemP">Trofie alla Marinara</div>
						<div class="productC priceP">&euro; ...</div>																
					</div>
					<div class="productR">
						<div class="productC itemP">Fagottini Salmone e Gamberi</div>
						<div class="productC priceP">&euro; ...</div>																
					</div>
					<div class="productR">
						<div class="productC itemP">Risotto del Pescatore</div>
						<div class="productC priceP">&euro; ...</div>																
					</div>
					<div class="productR">
						<div class="productC itemP">Gnocchetti del Golfo</div>
						<div class="productC priceP">&euro; ...</div>																
					</div>
					<div class="productR">
						<div class="productC itemP">Farro del Pescatore</div>
						<div class="productC priceP">&euro; ...</div>																
					</div>
					<div class="productR">
						<div class="productC itemP">Risotto ai Funghi</div>
						<div class="productC priceP">&euro; ...</div>																
					</div>
					<div class="productR">
						<div class="productC itemP">Lasagne alla Bolognese</div>
						<div class="productC priceP">&euro; ...</div>																
					</div>
					<div class="productR">
						<div class="productC itemP">Gnocchi alla Sorrentina</div>
						<div class="productC priceP">&euro; ...</div>																
					</div>
					<div class="productR">
						<div class="productC itemP">Cannelloni Ricotta e Spinaci</div>
						<div class="productC priceP">&euro; ...</div>																
					</div>
				</div>
			</div>
			<h2>Secondi</h2>			
			<div class="otherL">
				<div class="productT">
					<div class="productR">
						<div class="productC itemP">Polpo all’Isolana</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Sauté di Cozze</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Polpo alla Cagliaritana</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Merluzzo con Pomodorini</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Cotoletta (Gran Milanese)</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Scaloppina Gourmet al Limone</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Salsiccia alla Griglia</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Spiedone alla Griglia</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Frittatina al Naturale</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Spiedini (Calamaro e Gambero) alla griglia</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Calamari Ripieni</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Filetto di Orata</div>
						<div class="productC priceP">&euro; ...</div>
					</div>				
				</div>
			</div>
			<div class="otherR">
				<div class="productT">
					<div class="productR">
						<div class="productC itemP">Filetto di Rombo</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Filetto alla Mugnaia</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Filetto di Salmone</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Hamburger di Pesce</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Mini Hamburger con Patate</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Fritto Misto Pastellato</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Frittura Mista Infarinata</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Rosti Patata e Speck</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Mozzarella in Carrozza</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Acciughe Panate</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Crunchy Fish</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Melanzane alla Parmigiana</div>
						<div class="productC priceP">&euro; ...</div>
					</div>				
				</div>
			</div>
			<h2>Antipasti e Contorni</h2>
			<div class="otherL">
				<div class="productT">
					<div class="productR">
						<div class="productC itemP">Patate Fritte</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Patate al Rosmarino</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Bocconcini di Baccalà</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Spinaci al Limone</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Spinaci alla Crema</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Olive Panate</div>
						<div class="productC priceP">&euro; ...</div>
					</div>						
				</div>																																			
			</div>
			<div class="otherR">
				<div class="productT">
					<div class="productR">
						<div class="productC itemP">Frittelle Gamberetti e Zucchine</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Fiori di Zucca Pastellati</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Verdure Miste Pastellate</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Anelli di Cipolla Pastellati</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
					<div class="productR">
						<div class="productC itemP">Mozzarelline Panate</div>
						<div class="productC priceP">&euro; ...</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>


<?php
include_once 'footer.php';
?>