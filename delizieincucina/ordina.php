<?php
$pagename = "ordinaonline";
$pagetitle = "Ordina on line";
$pagedesc = "Delizie in Cucina, ristorante da asporto e vendita al dettaglio di prodotti surgelati. Scegli quello che preferisci tra i nostri piatti del giorno, te lo portiamo a casa noi. Ordina on line o chiama allo 0577 223667.";
include_once 'cdb.php';
include_once 'header.php';
$link = mysql_connect($db_hostname, $db_username, $db_password) or die("Couldn't make connection.");
$db = mysql_select_db($db_database, $link) or die("Couldn't select database");
?>

	<div id="main">
		<div class="mainContent" id="other">
			<h1 style="float:left;width:910px;">ORDINA ON LINE</h1>
<?php
$step = $_GET['step'];
?>
			<ul id="navigator">
				<li <? if($step == '1') {echo "class=\"step\"";} ?>>Scegli</li><li>&emsp;>&emsp;</li>
				<li <? if($step == '2') {echo "class=\"step\"";} ?>>Inserisci i tuoi dati</li><li>&emsp;>&emsp;</li>
				<li <? if($step == '3') {echo "class=\"step\"";} ?>>Riepilogo</li><li>&emsp;>&emsp;</li>
				<li <? if($step == '4') {echo "class=\"step\"";} ?>>Ordine confermato!</li>
			</ul>

<?php
if($step=='0') {
    echo "<h2 style=\"clear:both;float:left;margin-top:50px;\">Pagina in manutenzione, la funzionalità tornerà presto disponibile.</h2>";
}
if($step == '1') {
session_start(order);

$arrayitem = array();
$arrayprice = array();


$sum = 0;
echo "<form method=\"post\" action=\"ordina.php?step=2\" class=\"default\" id=\"order1\">";
$sql1 = "SELECT fish FROM daily_price WHERE id=1";
$result = mysql_query($sql1);
if (!$result) die ("Database access failed: " . mysql_error());
$rowf = mysql_fetch_row($result);
$sql1 = "SELECT meat FROM daily_price WHERE id=1";
$result = mysql_query($sql1);
if (!$result) die ("Database access failed: " . mysql_error());
$rowm = mysql_fetch_row($result);
echo <<<_END

    <div class="otherL">
        <div class="productT">
            <div class="productR">
                <div class="productC itemP"><strong>Menù del giorno di mare</strong></div>
                <div class="productC priceP">&euro; $rowf[0]</div>
                <div class="productC priceP"><input type="text" name="mare" value="0"></div>
            </div>
        </div>
    </div>

    <div class="otherR">
        <div class="productT">
            <div class="productR">
                <div class="productC itemP"><strong>Menù del giorno di terra</strong></div>
                <div class="productC priceP">&euro; $rowm[0]</div>
                <div class="productC priceP"><input type="text" name="terra" value="0"></div>
            </div>
        </div>
    </div>

_END;
$arrayitem[mare] = 'Menù del giorno di mare';
$arrayprice[mare] = $rowf[0];
$arrayitem[terra] = 'Menù del giorno di terra';
$arrayprice[terra] = $rowm[0];
$_SESSION['arrayitem'] = $arrayitem;
$_SESSION['arrayprice'] = $arrayprice;
$sql = "SELECT * FROM food_list WHERE genre=0 AND type=0 ORDER BY future DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
echo "<h2 style=\"clear:both;\">Primi</h2>";
echo "<div class=\"otherL\" style=\"width:439px;\">";
echo "<div class=\"productT\">";
echo "<div class=\"productR\" style=\"height:40px;\"><div class=\"productC itemP\">Piatti</div><div class=\"productC priceP\">Prezzo</div><div class=\"productC priceP\">Quantit&agrave;</div></div>";
for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);

echo <<<_END
					<div class="productR" style="height:40px;">
						<div class="productC itemP">$row[0]
_END;
if($row[1] != "") {

echo " <em>($row[1])</em>";
}
echo <<<_END
						</div>
						<div class="productC priceP">&euro;&emsp;$row[2]</div>
						<div class="productC priceP"><input type="text" name="$j" value="0"></div>																				
					</div>

_END;


$sum += $row[1];
$arrayitem[] = $row[0];
$arrayprice[] = $row[2];
$_SESSION['arrayitem'] = $arrayitem;
$_SESSION['arrayprice'] = $arrayprice;

}
$storej=$j-1;
echo "</div></div>";
$sql = "SELECT * FROM food_list WHERE genre=0 AND type=1 ORDER BY future DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
echo "<div class=\"otherR\" style=\"border:0;width:439px;\"><div class=\"productT\">";
echo "<div class=\"productR\" style=\"height:40px;\"><div class=\"productC itemP\">Piatti</div><div class=\"productC priceP\">Prezzo</div><div class=\"productC priceP\">Quantit&agrave;</div></div>";
for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);
$storej+=1;
echo <<<_END
					<div class="productR" style="height:40px;">
						<div class="productC itemP">$row[0]
_END;
if($row[1] != "") {

echo " <em>($row[1])</em>";
}
echo <<<_END
						</div>
						<div class="productC priceP">&euro;&emsp;$row[2]</div>
						<div class="productC priceP"><input type="text" name="$storej" value="0"></div>																				
					</div>

_END;
$sum += $row[1];
$arrayitem[] = $row[0];
$arrayprice[] = $row[2];
$_SESSION['arrayitem'] = $arrayitem;
$_SESSION['arrayprice'] = $arrayprice;

}

echo "</div></div>";
echo "<h2 style=\"clear:both;\">Secondi</h2>";
$sql = "SELECT * FROM food_list WHERE genre=1 AND type=0 ORDER BY future DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);

echo "<div class=\"otherL\" style=\"width:439px;\">";
echo "<div class=\"productT\">";
echo "<div class=\"productR\" style=\"height:40px;\"><div class=\"productC itemP\">Piatti</div><div class=\"productC priceP\">Prezzo</div><div class=\"productC priceP\">Quantit&agrave;</div></div>";
for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);
$storej+=1;
echo <<<_END
					<div class="productR" style="height:40px;">
						<div class="productC itemP">$row[0]
_END;
if($row[1] != "") {

echo " <em>($row[1])</em>";
}
echo <<<_END
						</div>
						<div class="productC priceP">&euro;&emsp;$row[2]</div>
						<div class="productC priceP"><input type="text" name="$storej" value="0"></div>																				
					</div>

_END;


$sum += $row[1];
$arrayitem[] = $row[0];
$arrayprice[] = $row[2];
$_SESSION['arrayitem'] = $arrayitem;
$_SESSION['arrayprice'] = $arrayprice;

}
echo "</div></div>";
$sql = "SELECT * FROM food_list WHERE genre=1 AND type=1 ORDER BY future DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
echo "<div class=\"otherR\" style=\"border:0;width:439px;\"><div class=\"productT\">";
echo "<div class=\"productR\" style=\"height:40px;\"><div class=\"productC itemP\">Piatti</div><div class=\"productC priceP\">Prezzo</div><div class=\"productC priceP\">Quantit&agrave;</div></div>";
for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);
$storej+=1;
echo <<<_END
					<div class="productR" style="height:40px;">
						<div class="productC itemP">$row[0]
_END;
if($row[1] != "") {

echo " <em>($row[1])</em>";
}
echo <<<_END
						</div>
						<div class="productC priceP">&euro;&emsp;$row[2]</div>
						<div class="productC priceP"><input type="text" name="$storej" value="0"></div>																				
					</div>

_END;
$sum += $row[1];
$arrayitem[] = $row[0];
$arrayprice[] = $row[2];
$_SESSION['arrayitem'] = $arrayitem;
$_SESSION['arrayprice'] = $arrayprice;

}
echo "</div></div>";
echo "<h2 style=\"clear:both;\">Antipasti e Contorni</h2>";
$sql = "SELECT * FROM food_list WHERE genre=2 AND type=0 ORDER BY future DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);

echo "<div class=\"otherL\" style=\"width:439px;\">";
echo "<div class=\"productT\">";
echo "<div class=\"productR\" style=\"height:40px;\"><div class=\"productC itemP\">Piatti</div><div class=\"productC priceP\">Prezzo</div><div class=\"productC priceP\">Quantit&agrave;</div></div>";
for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);
$storej+=1;
echo <<<_END
					<div class="productR" style="height:40px;">
						<div class="productC itemP">$row[0]
_END;
if($row[1] != "") {

echo " <em>($row[1])</em>";
}
echo <<<_END
						</div>
						<div class="productC priceP">&euro;&emsp;$row[2]</div>
						<div class="productC priceP"><input type="text" name="$storej" value="0"></div>																				
					</div>

_END;


$sum += $row[1];
$arrayitem[] = $row[0];
$arrayprice[] = $row[2];
$_SESSION['arrayitem'] = $arrayitem;
$_SESSION['arrayprice'] = $arrayprice;

}
echo "</div></div>";
$sql = "SELECT * FROM food_list WHERE genre=2 AND type=1 ORDER BY future DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
echo "<div class=\"otherR\" style=\"border:0;width:439px;\"><div class=\"productT\">";
echo "<div class=\"productR\" style=\"height:40px;\"><div class=\"productC itemP\">Piatti</div><div class=\"productC priceP\">Prezzo</div><div class=\"productC priceP\">Quantit&agrave;</div></div>";
for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);
$storej+=1;
echo <<<_END
					<div class="productR" style="height:40px;">
						<div class="productC itemP">$row[0]
_END;
if($row[1] != "") {

echo " <em>($row[1])</em>";
}
echo <<<_END
						</div>
						<div class="productC priceP">&euro;&emsp;$row[2]</div>
						<div class="productC priceP"><input type="text" name="$storej" value="0"></div>																				
					</div>

_END;
$sum += $row[1];
$arrayitem[] = $row[0];
$arrayprice[] = $row[2];
$_SESSION['arrayitem'] = $arrayitem;
$_SESSION['arrayprice'] = $arrayprice;

}
echo "</div></div>";
echo "<h2 style=\"clear:both;\">Bevande</h2>";
$sql = "SELECT * FROM food_list WHERE type=2 ORDER BY future DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);

echo "<div class=\"otherL\" style=\"width:439px;\">";
echo "<div class=\"productT\">";
echo "<div class=\"productR\" style=\"height:40px;\"><div class=\"productC itemP\">Piatti</div><div class=\"productC priceP\">Prezzo</div><div class=\"productC priceP\">Quantit&agrave;</div></div>";
for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);
$storej+=1;
echo <<<_END
					<div class="productR" style="height:40px;">
						<div class="productC itemP">$row[0]
_END;
if($row[1] != "") {

echo " <em>($row[1])</em>";
}
echo <<<_END
						</div>
						<div class="productC priceP">&euro;&emsp;$row[2]</div>
						<div class="productC priceP"><input type="text" name="$storej" value="0"></div>																				
					</div>

_END;


$sum += $row[1];
$arrayitem[] = $row[0];
$arrayprice[] = $row[2];
$_SESSION['arrayitem'] = $arrayitem;
$_SESSION['arrayprice'] = $arrayprice;

}
echo "</div></div>";
echo "<p style=\"clear:both;margin-top:20px;text-align:center;\"><input type=\"submit\" name=\"submit\" value=\"Continua\"></p>";
echo "</div></form>";
}
if($step == '2') {
session_start(order);

$_SESSION['arrayqt'] = $_POST;
?>

<form method="post" action="ordina.php?step=3" class="default" id="order3">

	<h2 style="clear:both;">Men&ugrave; del Giorno</h2>
	<p>Inserisci i tuoi dati</p>
	<div class="otherL" style="border:0;margin:25px 260px;width:439px;">
		<div class="productT">
			<div class="productR" style="height:40px;">
				<div class="productC" style="padding:0 10px;">Nome e Cognome*</div>
				<div class="productC"><input type="text" name="name" style="width:250px;"></div>
			</div>
			<div class="productR" style="height:40px;">
				<div class="productC" style="padding:0 10px;">Indirizzo*</div>
				<div class="productC"><input type="text" name="address" style="width:250px;"></div>
			</div>
			<div class="productR" style="height:40px;">
				<div class="productC" style="padding:0 10px;">Telefono*</div>
				<div class="productC"><input type="text" name="phone" style="width:250px;"></div>
			</div>
			<div class="productR" style="height:40px;">
				<div class="productC" style="padding:0 10px;">Email*</div>
				<div class="productC"><input type="text" name="email" style="width:250px;"></div>
			</div>
			<div class="productR" style="height:40px;">
				<div class="productC" style="padding:0 10px;">Orario di consegna preferito*</div>
				<div class="productC">
					<select name="orario">
						<option value="11:00">11:00</option>
						<option value="11:30">11:30</option>
						<option value="12:00">12:00</option>
						<option value="12:30">12:30</option>
						<option value="13:00">13:00</option>
						<option value="13:30">13:30</option>
						<option value="14:00">14:00</option>
						<option value="14:30">14:30</option>
						<option value="18:00">11:00</option>
						<option value="18:30">18:30</option>
						<option value="19:00">19:00</option>
						<option value="19:30">19:30</option>
						<option value="20:00">20:00</option>
						<option value="20:30">20:30</option>
					</select>
				</div>
			</div>	
		</div>
		<p>* Campi obbligatori</p>
		<p style="margin-top:20px;text-align:center;"><input type="submit" name="submit" value="Continua"></p>
	</div>
</form>
<?php
}

if($step == '3') {
session_start(order);
$name = $_POST['name'];
$address = $_POST['address'];
$phone = $_POST['phone'];
$email = $_POST['email'];
$orario = $_POST['orario'];
$_SESSION['arraydata'] = $_POST;
$tot=0;
$arrayitem = $_SESSION['arrayitem'];
$arrayprice = $_SESSION['arrayprice'];
$arrayqt = $_SESSION['arrayqt'];
$lim = count($arrayitem)-2;

echo <<<_END
<form method="post" action="ordina.php?step=4" class="default" id="order3">
_END;
echo "
<h2 style=\"clear:both;\">Men&ugrave; del Giorno</h2>
<p style=\"margin-bottom:25px;\">Riepilogo</p>
<div class=\"otherL\">
	<div class=\"productT\">";
echo "
		<div class=\"productR\" style=\"height:40px;\">
			<div class=\"productC itemP\"><strong>Piatti</strong></div>
			<div class=\"productC priceP\"  style=\"text-align:center;\"><strong>Quantit&agrave;</strong></div>
			<div class=\"productC priceP\" style=\"text-align:center;\"><strong>Prezzo</strong></div>
		</div>";
if ($arrayqt[mare]!='0') {
$item = $arrayitem [mare];
	$q = $arrayqt[mare];
	$p = $arrayprice[mare];
	$subt = $q*$p;
	$tot += $subt;    
echo <<<_END
		<div class="productR" style="height:40px;">
			<div class="productC itemP">$item</div>
			<div class="productC priceP" style="text-align:center;">x&emsp;$arrayqt[mare]</div>
			<div class="productC priceP" style="text-align:right;">&euro;&emsp;$subt</div>
		</div>
_END;
}
if ($arrayqt[terra]!='0') {
$item = $arrayitem [terra];
	$q = $arrayqt[terra];
	$p = $arrayprice[terra];
	$subt = $q*$p;
	$tot += $subt;    
echo <<<_END
		<div class="productR" style="height:40px;">
			<div class="productC itemP">$item</div>
			<div class="productC priceP" style="text-align:center;">x&emsp;$arrayqt[terra]</div>
			<div class="productC priceP" style="text-align:right;">&euro;&emsp;$subt</div>
		</div>
_END;
}
for ($j = 0; $j < $lim; ++$j) {
	$item = $arrayitem [$j];
	$q = $arrayqt[$j];
	$p = $arrayprice[$j];
	$subt = $q*$p;
	$tot += $subt;
        if ($q!='0') {
echo <<<_END
		<div class="productR" style="height:40px;">
			<div class="productC itemP">$item</div>
			<div class="productC priceP" style="text-align:center;">x&emsp;$q</div>
			<div class="productC priceP" style="text-align:right;">&euro;&emsp;$subt</div>
		</div>		
_END;
        }
}
echo "
		<div class=\"productR\" style=\"height:40px;\">
			<div class=\"productC itemP\" style=\"height: 30px;vertical-align: bottom;\"><strong>Totale</strong></div>
			<div class=\"productC priceP\" style=\"border-top: 1px solid rgb(0,159,238);height: 30px;vertical-align: bottom;\">&nbsp;</div>
			<div class=\"productC priceP\" style=\"text-align:right;border-top: 1px solid rgb(0,159,238);height: 30px;vertical-align: bottom;\"><strong>&euro;&emsp;$tot</strong></div>
		</div>
	</div>
</div>";


echo <<<_END
<div class=otherR>
	<div class="productT">
		<div class="productR" style="height:40px;">
			<div class="productC" style="padding:0 10px;">Nome e Cognome</div>
			<div class="productC">$name</div>
		</div>
		<div class="productR" style="height:40px;">
			<div class="productC" style="padding:0 10px;">Indirizzo</div>
			<div class="productC">$address</div>
		</div>
		<div class="productR" style="height:40px;">
			<div class="productC" style="padding:0 10px;">Telefono</div>
			<div class="productC">$phone</div>
		</div>
		<div class="productR" style="height:40px;">
			<div class="productC" style="padding:0 10px;">Email</div>
			<div class="productC">$email</div>
		</div>
		<div class="productR" style="height:40px;">
			<div class="productC" style="padding:0 10px;">Orario di consegna preferito</div>
			<div class="productC">$orario</div>
		</div>		
	</div>
</div>
_END;
echo "
<p style=\"margin-top:20px;text-align:center;clear:both;\"><input type=\"submit\" name=\"submit\" value=\"Conferma l'ordine\"></p>
</form>";
}
if($step == '4') {
session_start(order);
$arrayitem = $_SESSION['arrayitem'];
$arrayprice = $_SESSION['arrayprice'];
$arrayqt = $_SESSION['arrayqt'];
$arraydata = $_SESSION['arraydata'];
$lim = count($arrayitem)-2;

$email = $arraydata['email'];
$name = $arraydata['name'];
$address = $arraydata['address'];
$phone = $arraydata['phone'];
$orario = $arraydata['orario'];
$headers = "";
$headers .= "From: $email\n";
$headers .= "Reply-To: $email\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=utf-8\n";
$to = "info@delizieincucinasiena.it";
//$to = "skalkabox@gmail.com";
$subject = "Hai ricevuto un nuovo ordine!";
$message = "";
$message = "C'&egrave un nuovo ordine on line, ecco i dettagli:<br><br>
Nome: $name<br>
Indirizzo : $address<br>
Telefono: $phone<br>
E-mail: $email<br>
Orario di consegna preferito: $orario<br>
<br>
Dettagli ordine:<br><br>";
if($arrayqt[mare]!='0') {
	$item = $arrayitem [mare];
	$q = $arrayqt[mare];
	$p = $arrayprice[mare];
	$subt = $q*$p;
	$tot += $subt;
        $message .= $arrayitem [mare] . "&emsp; Quantit&agrave;: " . $arrayqt[mare] . "&emsp; Riepilogo costo: " . $subt . "&euro;.<br>";
}
if($arrayqt[terra]!='0') {
	$item = $arrayitem [terra];
	$q = $arrayqt[terra];
	$p = $arrayprice[terra];
	$subt = $q*$p;
	$tot += $subt;
        $message .= $arrayitem [terra] . "&emsp; Quantit&agrave;: " . $arrayqt[terra] . "&emsp; Riepilogo costo: " . $subt . "&euro;.<br>";
}
for ($j = 0; $j < $lim; ++$j) {

	$item = $arrayitem [$j];
	$q = $arrayqt[$j];
	$p = $arrayprice[$j];
	$subt = $q*$p;
	$tot += $subt;
	if ($arrayqt[$j] != "0"){
		$message .= $arrayitem [$j] . "&emsp; Quantit&agrave;: " . $arrayqt[$j] . "&emsp; Riepilogo costo: " . $subt . "&euro;.<br>";
	}


}
$message .= "<br>Il costo totale dell&apos;ordine $egrave di " . $tot . "&euro;.";
$message .= "<br><br><span style=\"color:red;\">L'ORDINE SI INTENDE CONFERMATO SOLTANTO DOPO AVER CONTATTATO TELEFONICAMENTE IL CLIENTE.</span>";
//echo $message;
if($tot != "0") {
mail ($to,$subject,$message,$headers);
}

?>


<?php
if ($tot == "0") {
?>
<h2 style="clear:both;">Errore!</h2>
<div class="otherL" style="border:0;margin:25px 260px;width:439px;">
<p>Non hai effettuato un ordine valido.</p>
<?php
}
else {
?>
<h2 style="clear:both;">Ordine completato!</h2>
<div class="otherL" style="border:0;margin:25px 260px;width:439px;">
	<p>Abbiamo ricevuto il tuo ordine e ti sarà inviato un riepilogo all'indirizzo email che ci hai fornito, 
		a breve sarai ricontattato telefonicamente e riceverai i dettagli sulla consegna.<br>
		La conferma dell'ordine ti sar&agrave; comunicata telefonicamente dal nostro staff nel minor tempo possibile.
		</p><p>&nbsp;</p>
	<p>Grazie per aver scelto Delizie in Cucina.</p>
<?php
}
?>
</div>
<?php
if ($email != "") {
$tot = 0;
$headers = "";
$headers .= "From: info@delizieincucinasiena.it\n";
$headers .= "Reply-To: info@delizieincucinasiena.it\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=iso-8859-1\n";
$to = $email;
$subject = "Delizie in Cucina - Riepilogo Ordine";
$message = "";
$message = "Ecco i dettagli dell'ordine che hai appena effettuato:<br><br>
Nome: $name<br>
Indirizzo : $address<br>
Telefono: $phone<br>
E-mail: $email<br>
Orario: $orario<br>
<br>
Dettagli ordine:<br><br>";
for ($j = 0; $j < $lim; ++$j) {

	$item = $arrayitem [$j];
	$q = $arrayqt[$j];
	$p = $arrayprice[$j];
	$subt = $q*$p;
	$tot += $subt;
	if ($arrayqt[$j] != "0"){
		$message .= $arrayitem [$j] . "&emsp; Quantit&agrave;: " . $arrayqt[$j] . "&emsp; Riepilogo costo: " . $subt . "&euro;.<br>";
	}

}
$message .= "<br>Il costo totale dell&apos;ordine $egrave di " . $tot . "&euro;.";
$message .= "<br><br><span style=\"color:red;\">TI RICORDIAMO CHE QUESTA EMAIL RAPPRESENTA SOLTANTO UN PROMEMORIA, A BREVE SARAI CONTATTATO DAL NOSTRO STAFF PER CONFERMARE IL TUO ORDINE E DEFINIRE I DETTAGLI DELLA CONSEGNA.</span>";

//echo $message;
mail ($email,$subject,$message,$headers);

}
}
?>
		</div>
	</div>


<?php
include_once 'footer.php';
?>