<?php
$pagename = "index";
$pagetitle = "Home";
$pagedesc = "Delizie in Cucina, ristorante da asporto e vendita al dettaglio di prodotti surgelati. La soluzione ideale per pasti veloci e gustosi, l'aiuto migliore per le vostre cene. A Siena in Via Roma 5, Ordina on line o chiama allo 0577 223667.";
include_once 'header.php';
include_once 'cdb.php';
setlocale(LC_TIME, 'it_IT');
$link = mysql_connect($db_hostname, $db_username, $db_password) or die("Couldn't make connection.");
$db = mysql_select_db($db_database, $link) or die("Couldn't select database");
?>

	<div id="main">
		<div class="mainContent" id="home">
			<div class="todayleft">
				<div class="tlTitle blue">
				<h1>Delizie in Cucina</h1>
				<h2>Fish Market</h2>
				</div>
				<div class="tlContent blue">
				<p>Delizie in Cucina, oltre ad essere un'attività di cucina da asporto, è anche un punto di vendita di 
				prodotti surgelati, grazie alla collaborazione con Delizie di Mare.</p>
				<p>Nel nostro punto vendita potrete trovare prodotti ittici, prodotti pastellati e da friggere, verdure 
				e tanto altro ancora per una dieta sana ed equilibrata. Tutti i nostri prodotti sono sfusi e in self service, 
				per permettervi di acquistare tutto ciò che vi serve nella quantità desiderata.</p>
				<p>I nostri prodotti sono resi speciali dal particolare processo di congelamento a bordo del pesce e dalla lavorazione 
				in stabilimenti ubicati in prossimità dell'area di pesca, il tutto pensato per offrirvi un prodotto dal gusto 
				e dalle proprietà nutritive equivalenti a quelle del pesce appena pescato.</p>
				</div>							
			</div>
			<div class="todayright">
				<div class="trTitle brown">
				<h1>Menù del Giorno</h1>
<?php
$sql1 = "SELECT date FROM daily_price WHERE id=1";
$result = mysql_query($sql1);
if (!$result) die ("Database access failed: " . mysql_error());
$row = mysql_fetch_row($result);
echo <<<_END
				<h2>$row[0]</h2>
_END;
?>
				</div>
				<div class="trContent brown">
				
<?php
$sql1 = "SELECT fish FROM daily_price WHERE id=1";
$result = mysql_query($sql1);
if (!$result) die ("Database access failed: " . mysql_error());
$row = mysql_fetch_row($result);
echo <<<_END
<h2><strong>Mare</strong> (&euro;&nbsp;$row[0])</h2>
_END;
$sql2 = "SELECT * FROM food_list WHERE active='1' AND dailytype!='1' ORDER BY type,genre,name ASC";
	$result = mysql_query($sql2);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
$sum = 0;
for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);
echo <<<_END

		<p>$row[0]</p>
_END;
}
$sql1 = "SELECT meat FROM daily_price WHERE id=1";
$result = mysql_query($sql1);
if (!$result) die ("Database access failed: " . mysql_error());
$row = mysql_fetch_row($result);
echo <<<_END
<h2><strong>Terra</strong> (&euro;&nbsp;$row[0])</h2>
_END;
$sql2 = "SELECT * FROM food_list WHERE active='1' AND dailytype!='0' ORDER BY type,genre,name ASC";
	$result = mysql_query($sql2);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
$sum = 0;
for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);
echo <<<_END

		<p>$row[0]</p>
_END;
}
?>					
				
					<h2 style="margin-top:25px;">
					 <a href="ordina.php?step=1" title="Ordina on line!"><strong>Ordina on line</strong></a> o chiama lo <strong>0577 223667</strong>!
					</h2>
				</div>							
			</div>		
		</div>
	</div>
	

<?php
include_once 'footer.php';
?>