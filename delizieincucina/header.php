<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="description" content="<?php echo $pagedesc ?>">
<meta name= "keywords" content= "Delizie in Cucina, pesce, carne, surgelati, cucina, asporto, siena, consegne a domicilio, primi, secondi, antipasti, contorni, piatto del giorno" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<!--[if !IE 7]>
	<style type="text/css">
		#wrap {display:table;height:100%}
	</style>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>
<title><?php echo $pagetitle ?> | Delizie in Cucina</title>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38338787-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div id="wrapper">

	<div id="header">
		<div id="headerContent">
			<div class="logo">
				<a href="index.php" title="Home - Delizie in Cucina" ><img src="images/delizielogo.png" alt="logo" ></a>
			</div>
			<div id="topmenu">
				<ul>
					<li><a href="index.php" title="Home" <?php if($pagename=="index") {echo "class=\"selected\"";} ?>>Home</a> <p>|</p></li>
					<li><a href="prodotti.php" title="Prodotti" <?php if($pagename=="prodotti") {echo "class=\"selected\"";} ?>>Prodotti</a> <p>|</p></li>
					<li><a href="gallery.php" title="Gallery" <?php if($pagename=="gallery") {echo "class=\"selected\"";} ?>>Gallery</a> <p>|</p></li>
					<li><a href="ordina.php?step=1" title="Ordina on Line" <?php if($pagename=="ordinaonline") {echo "class=\"selected\"";} ?>>Ordina On Line</a></li>
				</ul>
			</div>
		</div>	
	</div>
