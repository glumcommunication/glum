<?php
$pagename = "gallery";
$pagetitle = "Gallery";
$pagedesc = "Delizie in Cucina, ristorante da asporto e vendita al dettaglio di prodotti surgelati. Vieni a trovarci per uno spuntino o per scegliere gli ingredienti per una cena perfetta. Siamo a Siena, in Via Roma 5. Prenota on line o chiama allo 0577 223667.";
include_once 'header.php';
?>
	<div id="main">
		<div class="mainContent" id="gallery">
			<ul class="polaroids">
				<li>
					<img src="images/gallery/delizieincucina01.jpg" alt="Delizie in cucina" class="polaroid">			
				</li>
				<li>
					<img src="images/gallery/delizieincucina02.jpg" alt="Delizie in Cucina" class="polaroid">			
				</li>
				<li>
					<img src="images/gallery/delizieincucina03.jpg" alt="Delizie in Cucina" class="polaroid">			
				</li>
				<li>
					<img src="images/gallery/delizieincucina04.jpg" alt="Delizie in Cucina" class="polaroid">			
				</li>
				<li>
					<img src="images/gallery/delizieincucina05.jpg" alt="Delizie in Cucina" class="polaroid">			
				</li>
				<li>
					<img src="images/gallery/delizieincucina06.jpg" alt="Delizie in Cucina" class="polaroid">			
				</li>				
				<li>
					<img src="images/gallery/delizieincucina07.jpg" alt="Delizie in Cucina" class="polaroid">			
				</li>
				<li>
					<img src="images/gallery/delizieincucina08.jpg" alt="Delizie in Cucina" class="polaroid">			
				</li>
				<li>
					<img src="images/gallery/delizieincucina09.jpg" alt="Delizie in Cucina" class="polaroid">			
				</li>
				<li>
					<img src="images/gallery/delizieincucina10.jpg" alt="Delizie in Cucina" class="polaroid">			
				</li>					
			</ul>				
		</div>
	</div>
	

<?php
include_once 'footer.php';
?>