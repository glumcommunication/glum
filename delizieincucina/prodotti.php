<?php
$pagename = "prodotti";
$pagetitle = "Prodotti";
$pagedesc = "Delizie in Cucina, ristorante da asporto e vendita al dettaglio di prodotti surgelati. Tra i nostri prodotti puoi trovare le migliori specialità di pesce e tante altre gustose pietanze pronte da mangiare. Ordina on line o chiama allo 0577 223667.";
include_once 'cdb.php';
include_once 'header.php';


$link = mysql_connect($db_hostname, $db_username, $db_password) or die("Couldn't make connection.");
$db = mysql_select_db($db_database, $link) or die("Couldn't select database");
?>

	<div id="main">
		<div class="mainContent" id="other">
			<!--<h2 style="float: left;width:30%;margin-top: 50px;">Pesce</h2>-->
			<h1 style="float:left;width:910px;">PRODOTTI</h1>
			<!--<h2 style="float: left;width:30%;margin-top: 50px;">Carne</h2>-->
					
<?php


$sql = "SELECT * FROM food_list WHERE genre=0 AND type=0 ORDER BY future DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
$sum = 0;
echo "<h2 style=\"clear:both;\">Primi</h2><div class=\"otherL\"><div class=\"productT\">";
for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);

echo <<<_END
					<div class="productR">
						<div class="productC itemP">$row[0]
_END;
if($row[1] != "") {

echo " <em>($row[1])</em>";
}
echo <<<_END
						</div>
						<div class="productC priceP">&euro;&emsp;$row[2]</div>																
					</div>

_END;
$sum += $row[1];

}

echo "</div></div>";
$sql = "SELECT * FROM food_list WHERE genre=0 AND type=1 ORDER BY future DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
$sum = 0;
echo "<div class=\"otherR\"><div class=\"productT\">";


for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);

echo <<<_END
					<div class="productR">
						<div class="productC itemP">$row[0]
_END;
if($row[1] != "") {

echo " <em>($row[1])</em>";
}
echo <<<_END
						</div>
						<div class="productC priceP">&euro;&emsp;$row[2]</div>																
					</div>

_END;
$sum += $row[1];

}
echo "</div></div>";
$sql = "SELECT * FROM food_list WHERE genre=1 AND type=0 ORDER BY future DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
$sum = 0;
echo "<h2 style=\"clear:both;\">Secondi</h2><div class=\"otherL\"><div class=\"productT\">";
for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);

echo <<<_END
					<div class="productR">
						<div class="productC itemP">$row[0]
_END;
if($row[1] != "") {

echo " <em>($row[1])</em>";
}
echo <<<_END
						</div>
						<div class="productC priceP">&euro;&emsp;$row[2]</div>																
					</div>

_END;
$sum += $row[1];

}

echo "</div></div>";
$sql = "SELECT * FROM food_list WHERE genre=1 AND type=1 ORDER BY future DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
$sum = 0;
echo "<div class=\"otherR\"><div class=\"productT\">";


for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);

echo <<<_END
					<div class="productR">
						<div class="productC itemP">$row[0]
_END;
if($row[1] != "") {

echo " <em>($row[1])</em>";
}
echo <<<_END
						</div>
						<div class="productC priceP">&euro;&emsp;$row[2]</div>																
					</div>

_END;
$sum += $row[1];

}
echo "</div></div>";
$sql = "SELECT * FROM food_list WHERE genre=2 AND type=0 ORDER BY future DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
$sum = 0;
echo "<h2 style=\"clear:both;\">Antipasti e Contorni</h2><div class=\"otherL\"><div class=\"productT\">";
for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);

echo <<<_END
					<div class="productR">
						<div class="productC itemP">$row[0]
_END;
if($row[1] != "") {

echo " <em>($row[1])</em>";
}
echo <<<_END
						</div>
						<div class="productC priceP">&euro;&emsp;$row[2]</div>																
					</div>

_END;
$sum += $row[1];

}

echo "</div></div>";
$sql = "SELECT * FROM food_list WHERE genre=2 AND type=1 ORDER BY future DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
$sum = 0;
echo "<div class=\"otherR\"><div class=\"productT\">";


for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);

echo <<<_END
					<div class="productR">
						<div class="productC itemP">$row[0]
_END;
if($row[1] != "") {

echo " <em>($row[1])</em>";
}
echo <<<_END
						</div>
						<div class="productC priceP">&euro;&emsp;$row[2]</div>																
					</div>

_END;
$sum += $row[1];

}
echo "</div></div>";
$sql = "SELECT * FROM food_list WHERE type=2 ORDER BY future DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
$sum = 0;
echo "<h2 style=\"clear:both;\">Bevande</h2><div class=\"otherL\" style=\"border:none;\"><div class=\"productT\">";
for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);

echo <<<_END
					<div class="productR">
						<div class="productC itemP">$row[0]
_END;
if($row[1] != "") {

echo " <em>($row[1])</em>";
}
echo <<<_END
						</div>
						<div class="productC priceP">&euro;&emsp;$row[2]</div>																
					</div>

_END;
$sum += $row[1];

}

echo "</div></div>";

?>
			
		</div>
	</div>


<?php
include_once 'footer.php';
?>