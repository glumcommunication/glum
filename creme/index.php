<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <script src="js/modernizr.min.js"></script>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/style.css">
        <link id="page_favicon" href="/favicon.ico" rel="icon" type="image/x-icon" />
    </head>
    <body>


        <div class="container-fluid">
            <div class="bigbox cover">

            </div>



        </div>
        <div class="container">
            <div class="row">
                <p id="frase"><img src="img/logo.jpg"><br>IL SAPORE DELLA TRADIZIONE</p>

            </div>
            <div class="row">

                <div class="col-sm-4 col-sm-offset-0" ><img src="img/box1.jpg" class="img-responsive"><br/><p class="titolobox">INGREDIENTI AL PRIMO POSTO</p><p class="testobox text-justify"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt diam ut odio blandit, ac finibus turpis porttitor. Mauris consectetur metus vitae dolor tincidunt, ullamcorper eleifend tortor rutrum.</p></div>
                <div class="col-sm-4 col-sm-offset-0"><img src="img/box2.jpg" class="img-responsive"><br/><p class="titolobox">INGREDIENTI AL PRIMO POSTO</p><p class="testobox text-justify"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt diam ut odio blandit, ac finibus turpis porttitor. Mauris consectetur metus vitae dolor tincidunt, ullamcorper eleifend tortor rutrum.</p></div>
                <div class="col-sm-4 col-sm-offset-0"><img src="img/box3.jpg" class="img-responsive"><br/><p class="titolobox">INGREDIENTI AL PRIMO POSTO</p><p class="testobox text-justify"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt diam ut odio blandit, ac finibus turpis porttitor. Mauris consectetur metus vitae dolor tincidunt, ullamcorper eleifend tortor rutrum.</p></div>
            </div>
            <div class="row" style="margin-top: 40px;">

                <div class="col-sm-4 col-sm-offset-0" ><img src="img/box1.jpg" class="img-responsive"><br/><p class="titolobox">INGREDIENTI AL PRIMO POSTO</p><p class="testobox text-justify"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt diam ut odio blandit, ac finibus turpis porttitor. Mauris consectetur metus vitae dolor tincidunt, ullamcorper eleifend tortor rutrum.</p></div>
                <div class="col-sm-4 col-sm-offset-0"><img src="img/box2.jpg" class="img-responsive"><br/><p class="titolobox">INGREDIENTI AL PRIMO POSTO</p><p class="testobox text-justify"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt diam ut odio blandit, ac finibus turpis porttitor. Mauris consectetur metus vitae dolor tincidunt, ullamcorper eleifend tortor rutrum.</p></div>
                <div class="col-sm-4 col-sm-offset-0"><img src="img/box3.jpg" class="img-responsive"><br/><p class="titolobox">INGREDIENTI AL PRIMO POSTO</p><p class="testobox text-justify"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt diam ut odio blandit, ac finibus turpis porttitor. Mauris consectetur metus vitae dolor tincidunt, ullamcorper eleifend tortor rutrum.</p></div>
            </div>
        </div>

        <div class="container-fluid brown">
            <div class="row">
                <div class="text-center" id="titolofooter">
                    <div class="text-center"> <img src="img/contattaci.png" style="width: 20%;"/>
                </div>
                </div>
            </div>
            <div class="row">
                <div class="text-center" id="testofooter">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt diam ut odio blandit, ac finibus turpis porttitor. Mauris consectetur metus vitae dolor tincidunt, ullamcorper eleifend tortor rutrum. Nullam aliquet nunc metus, id pulvinar nulla ullamcorper vel. Curabitur sollicitudin tincidunt imperdiet. Phasellus feugiat condimentum massa eget aliquet. Sed tortor erat, lacinia ut luctus non, consectetur vitae quam. Cras eget tortor vitae metus varius porta id eget nibh. Ut sit amet justo at dui rhoncus ornare nec eu ex. Vivamus felis urna, tempus non ligula in, interdum hendrerit erat. In hac habitasse platea dictumst. Nulla tincidunt felis id ullamcorper faucibus. Duis ullamcorper mollis tortor, sed lobortis mi laoreet eu.
                    </p>
                    <div class="row">
                        <div class="boxhome col-sm-4 col-sm-offset-0">
                            <span class="glyphicon glyphicon-envelope"></span><a target="_blank" href="mailto:info@cremegelateria.it"> info@cremegelateria.it</a>

                        </div>
                        <div class="boxhome col-sm-4 col-sm-offset-0">
                            <span class="glyphicon glyphicon-phone"></span><a target="_blank" href="tel:0577204587"> 0577 xxxxxxx</a>

                        </div>
                        <div class="boxhome col-sm-4 col-sm-offset-0">
                            <span class="glyphicon glyphicon-map-marker"></span><a target="_blank" href="https://www.google.it/maps/place/VIa+Fiorentina,+151,+53100+Siena+SI/@43.3425018,11.3057114,19z/data=!4m2!3m1!1s0x132a2cfdaca5650b:0x6757673af8106d0a"> via Fiorentina 41, Siena.it</a>

                        </div>
                    </div>
                </div>
            </div>
            <div class="row text-center" id="chiaro">
                <p>Crafted by <a href="http://www.glumcommunication.it">GLuM Communication</a></p>
            </div>

        </div>



  <!--<script src="js/plugins.js"></script>
  <script src="js/main.js"></script>-->
        <script src="js/jquery-2.1.1.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-53491943-1', 'auto');
            ga('send', 'pageview');

        </script>
    </body>
</html>
