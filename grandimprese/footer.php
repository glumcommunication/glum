<div id="push"></div>
</div>	
        <div id="footer">
            <div class="container">
                <div class="upperF">
                    <div class="social">
                            <a href="https://www.facebook.com/GrandiImprese.si" title="Facebook"><img src="img/facebook.png" alt="Facebook"></a>
                            <a href="https://plus.google.com/u/0/111095599168838871583/posts" title="Google+"><img src="img/google+.png" alt="Google+"></a>
                            <a href="https://www.youtube.com/channel/UCx7iE0Ca7hmsNaAnC75XEOw" title="YouTube"><img src="img/youtube.png" alt="Youtube"></a>
                    </div>
                </div>
                <div class="lowerF">
                    <div class="col-md-6">
                        <p class="copyright">&copy; 2013 <a href="http://www.glumcommunication.it" target="_blank">GLuM Communication SRL</a></p>
                    </div>
                    <div class="col-md-6">
                        <p class="contact"><a href="mailto:info@glumcommunication.it">info@glumcommunication.it</a></p>
                    </div>
                </div>
            </div>
	</div>
<script src="http://code.jquery.com/jquery.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="js/bootstrap.min.js"></script>
        <script type="text/javascript">
          $(document).ready(function () {
            $("#privacy").tooltip({
              'selector': '',
              'placement': 'right'
            });
          });
        </script>
<script>
$(document).ready(function() {

	// validate signup form on keyup and submit
	var validator = $("#contatti").validate({
		rules: {
			name: "required",
            surname: "required",
			phone: {
                required: false,
                minlength: 9
            },
            message: "required",
            privacy: "required",
			email: {
				required: true,
                minlength: 7,
                email: true
			}
		},
		messages: {
			name: "Inserisci il tuo nome",
                        surname: "Inserisci il tuo cognome",
			phone: {
				
				minlength: "Il numero deve essere composto da almeno 9 cifre"
			},
            message: "Inserisci un messaggio",
            privacy: "Devi accettare l'",
			email: {
                required: "Inserisci un indirizzo email",
                minlength: "Inserisci un indirizzo email valido",
                email: "Inserisci un indirizzo email valido"
            }
			
		},
	});


});
</script>        
<?php
if ($pageName == "puntata") {
echo <<<_END
     
  <script src="js/jquery.collagePlus.min.js"></script>
  <script src="js/jquery.removeWhitespace.min.js"></script>
  <script src="js/jquery.collageCaption.min.js"></script>
  <script src="js/lightbox.min.js"></script>  
  <script type="text/javascript">
    $(window).load(function () {
        $(document).ready(function(){
            collage();
        });
    });
    function collage() {
        $('.Collage').removeWhitespace().collagePlus(
            {
                'allowPartialLastRow' : true,
                'effect' : 'effect-3',
                'fadeSpeed' : 2000,
                'targetHeight' : 200
            }
        ).collageCaption();
    };

        
     
    var resizeTimer = null;
    $(window).bind('resize', function() {
        // hide all the images until we resize them
        $('.Collage .Image_Wrapper').css("opacity", 0);
        // set a timer to re-apply the plugin
        if (resizeTimer) clearTimeout(resizeTimer);
        resizeTimer = setTimeout(collage, 200);
    });

  </script>
_END;
}
?>


</body>

</html>
