<?php
$pageTitle="Contatti";
$pageDesc="Contattaci per aderire al progetto o per maggiori informazioni su GrandImprese.si.";
$pageName="contatti";
include_once ('header.php');
?>
<?php

  ?>
<?php
if ($_POST['doSend']=='Invia') {
	  require_once('recaptchalib.php');
  $privatekey = "6LdeM-oSAAAAAJ4RuJxH82YrWp7QK1mnYFbnlrrX";
  $resp = recaptcha_check_answer ($privatekey,
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["recaptcha_challenge_field"],
                                $_POST["recaptcha_response_field"]);

  if (!$resp->is_valid) {
    // What happens when the CAPTCHA was entered incorrectly
    die ("<div class=\"alert alert-warning\" style=\"width: 50%;margin: 50px auto;\">Il codice inserito non è corretto. <a href=\"javascript:history.go(-1)\" style=\"text-decoration:underline;\">Torna indietro</a> e riprova!</div>");
        // "(reCAPTCHA said: " . $resp->error . ")");
  } else {
    // Your code here to handle a successful verification
  }
$name = $_POST[name];
$surname = $_POST[surname];
$phone = $_POST[phone];
$email = $_POST[email];
$message = $_POST[message];
$headers = "";
$headers .= "From: $email\n";
$headers .= "Reply-To: $email\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=utf-8\n";
$to = "skalkabox@gmail.com";
$subject = "Richiesta informazioni";
/*$message = $messaggio;*/
$messagebody = "Hai ricevuto una nuova email via grandimprese.si, ecco i dettagli:<br />
Nome e Cognome: $name $surname<br />
E-mail: $email<br />
Telefono: $phone<br />
Messaggio: $message<br />";

mail ($to,$subject,$messagebody,$headers);
$sent = "1";
}

?>
	<div id="main">
            <div class="container">
                <hr>
                <div class="row">
                    <div class="col-sm-6">
                        <p class="lfp">CONTATTACI</p>
                        <hr>
                        <div id="leftContact">
                            <p>
                                <a href="mailto:info@glumcommunication.it" title="Contattaci">info@glumcommunication.it</a>
                            </p>
                            <p>
                                Tel. +39 0577280213
                            </p>

                        </div>                        
                    </div>
                    <div class="col-sm-6" id="formContainer">
<?php
if ($sent=='1') {
echo <<<_END
<div class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>Messaggio inviato!</h4>
  Sarai ricontattato al pi&ugrave; presto.
</div>
_END;
}
?>
<script type="text/javascript">
 var RecaptchaOptions = {
    theme : 'clean'
 };
 </script>
                        <form id="contatti" role="form" action="contatti.php" method="post">
                            <div class="form-group">
                                <label for="name">Nome</label>
                                <input type="text" class="form-control" id="name" placeholder="Nome" name="name">
                            </div>
                            <div class="form-group">
                                <label for="surname">Cognome</label>
                                <input type="text" class="form-control" id="surname" placeholder="Cognome" name="surname">
                            </div>
                            <div class="form-group">
                                <label for="email">Indirizzo email</label>
                                <input type="email" class="form-control" id="email" placeholder="Email" name="email">
                            </div>
                            <div class="form-group">
                                <label for="phone">Numero di telefono</label>
                                <input type="text" class="form-control" id="phone" placeholder="Telefono" name="phone">
                            </div>
                            <div class="form-group">
                                <label for="message">Messaggio</label>
                                <textarea class="form-control" rows="5" id="message" name="message"></textarea>
                            </div>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox" value="Yep" name="privacy">
                                    <a id="privacy" href="#" rel="tooltip" data-toggle="tooltip" title="Nel inviare i miei dati dichiaro di aver letto l'informativa e sono consapevole che il trattamento degli stessi è necessario per ottenere il servizio proposto. A tal fine, nel dichiarare di essere maggiorenne, fornisco il mio consenso." data-placement="right" style="font-size:14px;cursor: help;">
                                        Informativa sulla Privacy ai sensi del D.Lgs 196/2003</a>
                                </label>
                            </div>
<?php
	require_once('recaptchalib.php');
	$publickey = "6LdeM-oSAAAAAExl4C-ElrimQgFRKHUZ1UdUZjyB"; // you got this from the signup page
	echo recaptcha_get_html($publickey);
?>
                            <button type="submit" class="btn btn-default" name="doSend" value="Invia" style="margin-top:15px;">Invia</button>

                        </form>
                    </div>
      
                </div>
            </div>
        </div>

<?php
include_once('footer.php');
?>
