<!DOCTYPE html>
<html lang="it">
<head>
<meta charset="utf-8" />
<!--<meta http-equiv="X-UA-Compatible" content="IE=edge">-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="<?php echo $pageDesc; ?>">
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/lightbox.css" rel="stylesheet" media="screen">
<link href="css/style.css" rel="stylesheet" media="screen">
<script src="js/respond.src.js"></script>
<link href='http://fonts.googleapis.com/css?family=Tinos:400,700,400italic,700italic|Open+Sans:400,700|Open+Sans+Condensed:300,700|Playball' rel='stylesheet' type='text/css'>
<link rel="icon" href="img/favicon.png" type="image/svg" />
<title><?php echo $pageTitle; ?> | Grandi Imprese punto SI</title>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42256773-1', 'grandimprese.si');
  ga('send', 'pageview');

</script>

</head>
<body>

<div id="wrapper">
	<div id="header">
            <div class="container">
                <div class="row">
                    <div class="logo col-md-3">
                            <a href="http://www.grandimprese.si" title="Home"><img src="img/logo-01.png" alt="Grandi Imprese"></a>
                    </div>
                    <div class="col-md-9">
                        <!--<nav class="navbar navbar-default" role="navigation">
                                <ul>
                                        <li <?php if($pageName=='index'){echo 'class="sel"';} ?>><a href="index.php" title="Home">HOME</a></li>
                                        <li <?php if($pageName=='progetto'){echo 'class="sel"';} ?>><a href="progetto.php" title="Il Progetto">PROGETTO</a></li>
                                        <li <?php if($pageName=='puntate'){echo 'class="sel"';} ?>><a href="puntate.php" title="Puntate">PUNTATE</a></li>
                                        <li <?php if($pageName=='contatti'){echo 'class="sel"';} ?>><a href="contatti.php" title="Contatti">CONTATTI</a></li>
                                </ul>
                        </nav>-->
                        <nav class="navbar navbar-default" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
        
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
                                        <li <?php if($pageName=='index'){echo 'class="sel"';} ?>><a href="index.php" title="Home">HOME</a></li>
                                        <li <?php if($pageName=='progetto'){echo 'class="sel"';} ?>><a href="progetto.php" title="Il Progetto">PROGETTO</a></li>
                                        <li <?php if($pageName=='puntate'){echo 'class="sel"';} ?>><a href="puntate.php" title="Puntate">PUNTATE</a></li>
                                        <li <?php if($pageName=='contatti'){echo 'class="sel"';} ?>><a href="contatti.php" title="Contatti">CONTATTI</a></li>
    </ul>


  </div><!-- /.navbar-collapse -->
</nav>
                    </div>
                </div>
            </div>

	</div>