<?php
$pageTitle="Puntate";
$pageDesc="La raccolta completa dei video di Grandi Imprese. Clicca su ogni puntata per seguire le tappe del nostro viaggio.";
$pageName="puntate";
include_once ('header.php');
?>
	<div id="main" style="padding-bottom: 25px;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 smallP">
                        <div class="smallscreen">
                            <img src="img/epsd/casone.jpg" style="vertical-align: middle;">
                            <a href="puntata.php?episode=casone" title="Guarda il video">
                                <img src="img/cinepresa.png" alt="PLAY" class="playbuttonS">
                            </a>
                        </div>
                         <div class="textscreenS col-md-12">
                            <h1>
                                <a href="puntata.php?episode=casone" title="Guarda il video">IMBALLAGGI IL CASONE</a>
                             </h1>
                            <p>
                                Grandi Imprese ha scelto per la quarta puntata Imballaggi il Casone, il più antico produttore di scatole 
                                per imballaggio industriale in provincia di Siena. 
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-4 smallP">
                        <div class="smallscreen">
                            <img src="img/epsd/luppolicase.jpg" style="vertical-align: middle;">
                            <a href="puntata.php?episode=luppolicase" title="Guarda il video">
                                <img src="img/cinepresa.png" alt="PLAY" class="playbuttonS">
                            </a>
                        </div>
                         <div class="textscreenS col-md-12">
                            <h1>
                                <a href="puntata.php?episode=luppolicase" title="Guarda il video">LUPPOLI CASE</a>
                             </h1>
                            <p>
                                Grandi Imprese ha scelto per la terza puntata Luppoli Case, un'azienda da oltre vent'anni ai vertici 
                                dell'intermediazione immobiliare senese. 
                            </p>
                        </div>
                    </div>                    
                    <div class="col-sm-4 smallP">
                        <div class="smallscreen">
                            <img src="img/epsd/thenewoxfordschool.jpg" style="vertical-align: middle;">
                            <a href="puntata.php?episode=thenewoxfordschool" title="Guarda il video">
                                <img src="img/cinepresa.png" alt="PLAY" class="playbuttonS">
                            </a>
                        </div>
                         <div class="textscreenS col-md-12">
                            <h1>
                                <a href="puntata.php?episode=thenewoxfordschool" title="Guarda il video">THE NEW OXFORD SCHOOL</a> 
                            </h1>
                            <p>
                                The New Oxford School è protagonista della seconda puntata di Grandi Imprese. La direttrice 
                                Orsola Maione ci guida alla scoperta della scuola.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-4 smallP">
                        <div class="smallscreen" style="background-image:none;">
                            <img src="img/epsd/gruppoarkell.jpg" style="vertical-align: middle;">
                            <a href="puntata.php?episode=gruppoarkell" title="Guarda il video">
                                <img src="img/cinepresa.png" alt="PLAY" class="playbuttonS">
                            </a>
                        </div>
                         <div class="textscreenS col-md-12">
                            <h1>
                                <a href="puntata.php?episode=gruppoarkell" title="Guarda il video">GRUPPO ARKELL</a>
                            </h1>
                            <p>
                                Protagonista della prima puntata di Grandi Imprese è il Gruppo Arkell. Laura Farasin, una dei 
                                soci, ci ha invitato nella sua casa per raccontarci l'azienda.
                            </p>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
        </div>

<?php
include_once('footer.php');
?>