<?php
$pageTitle="Home";
$pageDesc="Un viaggio attraverso le Grandi Imprese senesi. Scopri con noi chi sarà il prossimo protagonista della puntata.";
$pageName="index";
include_once ('header.php');
?>
        
            <div id="main">
                <div class="container">
                    <div class="row">
                    
                        <div class="col-md-12 bigH">
                            <div id="whitebg">
                            <div class="bigscreen col-md-9 col-sm-8">
                                <img src="img/epsd/casone.jpg" style="vertical-align: middle;">
                                <a href="puntata.php?episode=casone" title="Guarda il video"><img src="img/cinepresa.png" alt="PLAY" class="playbutton"></a>
                                    <!--<iframe width="620" height="349" src="//www.youtube.com/embed/_nAqQMNcAgs" frameborder="0" allowfullscreen></iframe>-->
                                <!--<div class="video-container">
                                    <iframe src="http://www.youtube.com/embed/oSI6zOp2KQo?rel=0" frameborder="0" width="560" height="315"></iframe>
                                </div>-->
                            </div>
                            <div class="textscreen col-md-3 col-sm-4">
                                <h1>
                                    <a href="puntata.php?episode=casone" title="Guarda il video">IMBALLAGGI IL CASONE</a>
                                </h1>
                                <h2>4^ puntata</h2>
                                <p>
                                    Grandi Imprese ha scelto per la quarta puntata Imballaggi il Casone, il più antico produttore di scatole per imballaggio 
                                    industriale in provincia di Siena che, da poco, realizza anche elementi d'arredo e di design con un nuovo brand “Carton 
                                    Factory”, sempre in cartone. Si è offerta di guidarci nel viaggio all'interno dell'azienda, Cinzia Gianni, seconda generazione 
                                    di una delle famiglie fondatrici. L'abbiamo intervistata nel Castello di Monteriggioni.
                                </p>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!--<div class="row">
                    
                        <div class="col-md-12 bigH">
                            <div id="whitebg">
                            <div class="bigscreen col-md-9 col-sm-8">
                                <img src="img/epsd/gruppoarkell.jpg" style="vertical-align: middle;">
                                <a href="puntata.php?episode=gruppoarkell" title="Guarda il video"><img src="img/cinepresa.png" alt="PLAY" class="playbutton"></a>

                            </div>
                            <div class="textscreen col-md-3 col-sm-4">
                                <h1>
                                    <a href="puntata.php?episode=gruppoarkell" title="Guarda il video">GRUPPO ARKELL</a>
                                </h1>
                                <h2>1^ puntata</h2>
                                <p>
                                    Per la prima puntata, Grandi Imprese ha scelto il Gruppo Arkell, leader nel mercato delle finiture e soluzioni d'interni 
                                    nella Valdelsa e nell'area senese. Laura Farasin, una dei quattro soci, ci ha invitato nella sua casa per raccontarci la 
                                    storia di come è nata l'azienda. Mentre Simone Boschini, David Campinoti e Paolo Posarelli ci hanno guidato all'interno 
                                    dei due negozi per raccontarci i loro inizi e il loro ruolo all'interno dell'impresa.
                                </p>
                            </div>
                            </div>
                        </div>
                    </div>-->
                    <div class="row">
                        <div class="col-sm-4 smallH">
                            <div class="smallscreen">
                                <img src="img/epsd/luppolicase.jpg" style="vertical-align: middle;">
                                <a href="puntata.php?episode=luppolicase" title="Guarda il video">
                                    <img src="img/cinepresa.png" alt="PLAY" class="playbuttonS">
                                </a>
                            </div>
                             <div class="textscreenS col-md-12">
                                <h1>LUPPOLI CASE</h1>
                                <p>
                                    Grandi Imprese ha scelto per la terza puntata Luppoli Case, un'azienda che, da oltre vent'anni, è ai vertici dell'intermediazione immobiliare senese.
                                </p>
                            </div>
                        </div>                        
                        <div class="col-sm-4 smallH">
                            <div class="smallscreen">
                                <img src="img/epsd/thenewoxfordschool.jpg" style="vertical-align: middle;">
                                <a href="puntata.php?episode=thenewoxfordschool" title="Guarda il video">
                                    <img src="img/cinepresa.png" alt="PLAY" class="playbuttonS">
                                </a>
                            </div>
                             <div class="textscreenS col-md-12">
                                <h1>THE NEW OXFORD SCHOOL</h1>
                                <p>
                                    The New Oxford School è la protagonista della seconda puntata di Grandi Imprese. La direttrice Orsola Maione ci ha 
                                    guidato nel viaggio all'interno della scuola.
                                </p>
                            </div>
                        </div>

                        <div class="col-sm-4 smallH">
                            <div class="smallscreen">
                                <img src="img/epsd/gruppoarkell.jpg" style="vertical-align: middle;">
                                <a href="puntata.php?episode=gruppoarkell" title="Guarda il video">
                                    <img src="img/cinepresa.png" alt="PLAY" class="playbuttonS">
                                </a>
                            </div>
                             <div class="textscreenS col-md-12">
                                <h1>GRUPPO ARKELL</h1>
                                <p>
                                    La protagonista della prima puntata di Grandi Imprese è il Gruppo Arkell. Laura Farasin, una dei quattro soci, ci ha 
                                    invitato nella sua casa per raccontarci l'azienda.
                                </p>
                            </div>
                        </div>

                        </div>
                    </div>
                </div>

        
<?php
include_once('footer.php');
?>
