<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<link href="pre/style.css" rel="stylesheet" media="screen">
<title>Grandimprese.si - Sito in costruzione</title>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42256773-1', 'grandimprese.si');
  ga('send', 'pageview');

</script>
</head>
<body>
    <div id="wrapper">
        <div id="container">
                <div id="fb">
                        <a href="https://www.facebook.com/GrandiImprese.si" title="Facebbok Page">
                                <img src="pre/fb.png" alt="Seguici su Facebook">
                        </a>		
                </div>
                <div id="yt">
                        <a href="https://www.youtube.com/channel/UCx7iE0Ca7hmsNaAnC75XEOw" title="Youtube Channel">
                                <img src="pre/yt.png" alt="Iscriviti al nostro canale Youtube">
                        </a>		
                </div>
        </div>
        <div id="footer">
            <div id="footLeft">
                <p>&copy; <?php echo date(Y); ?> GLuM Communication Srl</p>
            </div>
            <div id="footRight">
                <p><a href="mailto:info@grandimprese.si">info@grandimprese.si</a>
            </div>
        </div>
    </div>
</body>
</html>