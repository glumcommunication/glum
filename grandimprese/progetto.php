<?php
$pageTitle="Progetto";
$pageDesc="Il progetto, le sue origini, i suoi obbiettivi. Scopri di cosa si tratta.";
$pageName="progetto";
include_once ('header.php');
?>
	<div id="main">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="logoAbout">
                            <img src="img/logooff.png" alt="Logo Grandi Imprese">
                        </div>
                        <br clear="all">
                        <img src="img/foto1.jpg" alt="Screenshot mani" id="fotomani">
                    </div>

                    <div class="col-sm-8">
                        <div class="whiteBox textscreen">
                            <h1>PROGETTO</h1>
                            <p>
                                GrandImprese.si è un viaggio attraverso le esperienze di tutte le aziende operanti nel territorio senese 
                                che sono attive da tempo nel territorio e che hanno fatto la scelta di puntare sull’eccellenza e sulla qualità. 
                                Quelle aziende solide che, nel corso degli anni, invece di occuparsi di fare semplicemente profitto, hanno 
                                scelto di fare la differenza, le Grandi Imprese senesi appunto. Un’azienda infatti non è semplicemente un mezzo, 
                                un tramite tra un prodotto e un cliente, ma un insieme di valori, di persone che cooperano per soddisfare nel 
                                migliore dei modi il cliente. Un’azienda è fatta prima di tutto da coloro che ci lavorano. Per ogni impresa di 
                                successo in passato c’è stato qualcuno che si è messo in gioco, che ha preso una decisione coraggiosa.
                            </p>
                            <p>
                                GrandImprese.si è un viaggio condotto con un taccuino e una cartina geografica, attraverso le storie personali 
                                di tutti i titolari, l’ambiente in cui lavorano, ma anche quello in cui vivono, trascorrono il loro tempo fuori 
                                dall’azienda. Cosa li ha portati un giorno ad assumersi il rischio di aprire e portare avanti un’impresa. 
                                Un viaggio alla ricerca di storie, di sudori, fatiche, difficoltà e soddisfazioni dove ci condurranno delle guide 
                                d’eccellenza: i responsabili aziendali. Attraverso le puntate, una per ogni tappa del nostro viaggio, si avrà così 
                                l’impressione di entrare fisicamente dentro l’azienda.
                            </p>
                            <p>
                                Ma GrandImprese.si è anche molto altro: un punto di incontro tra le varie aziende, un luogo dove queste, unite dal 
                                fatto di essere delle eccellenze in ambito territoriale, possano incontrarsi, cooperare, supportarsi, organizzare 
                                degli incontri, stabilire rapporti sempre più stretti tra di loro, ma anche con il cliente. Perché fare impresa 
                                oggi non significa essere isolati.
                            </p>
                        </div>
                    </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="whiteBox sjobs">
                                <p>"Ho imparato che le grandi aziende hanno a cuore l'estetica perché trasmette un messaggio su come l'azienda percepisce se stessa, 
                                    sul senso di disciplina dei suoi progetti e su come è gestita"</p>
                                <p style="text-align: right;">(Steve Jobs)</p>
                            </div>
                        </div>
                     </div>
                </div>
            </div>
<?php
include_once('footer.php');
?>