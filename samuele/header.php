<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "secure/dbcon.php"; ?>
<head>
<meta charset="utf-8">
<title>Samuele Mancini Photographer</title>
<meta name="description" content="Samuele Mancini Fotografo">
<meta name="keywords" content="fotografo, siena,">
<meta name="viewport" content="width=device-width">
<script src="js/modernizr.min.js"></script>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link href='http://fonts.googleapis.com/css?family=Libre+Baskerville' rel='stylesheet' type='text/css'>
</head>
<body>
<header>
    <?php include "nav.php"; ?>
</header>