<?php session_start();
// verifico che esista la sessione di autenticazione
if (empty($_SESSION['userid'])) {
 header("location:main_login.php");
  
  exit;
}
include "dbcon.php";
$userid=$_SESSION['userid'];
$usertype=mysql_query("SELECT role FROM utenti WHERE id=$userid");
while($role=mysql_fetch_array($usertype)){
    $permit=$role['role'];

}
// gestisco la richiesta di logout
if (isset($_GET['logout'])) {
  session_destroy();
  echo "Sei uscito con successo<br/><a href='../index.php'>Torna al sito</a> ";
  exit;
}
?>
<html>
<head>
<title>Pannello Di Controllo</title>
  <!-- Inclusione fogli di stile-->
        <link rel="stylesheet" type="text/css" href="css/backend.css" media="screen" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/smoothness/jquery-ui-1.8.16.custom.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/ui.jqgrid.css" />
        <link rel="stylesheet" type="text/css" href="library/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
        <link href='http://fonts.googleapis.com/css?family=Rosario' rel='stylesheet' type='text/css'>
        <link rel="shortcut icon" href="favicon.ico" />
        <!-- Inclusione librerie js-->
        <script language="javascript" type="text/javascript" src="library/jquery.js"></script>
        <script language="javascript" type="text/javascript" src="js/i18n/grid.locale-it.js"></script>
        <script language="javascript" type="text/javascript" src="js/jquery.jqGrid.min.js" ></script>
        <script language="javascript" type="text/javascript" src="library/tiny_mce/tiny_mce.js"></script>
        <script language="javascript" type="text/javascript" src="js/jquery-ui-1.8.17.custom.min.js" ></script>
        <script language="javascript" type="text/javascript" src="library/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
        <script language="javascript" type="text/javascript" src="library/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
        <script language="javascript" type="text/javascript" src="library/modernizr.js"></script>
        <script type="text/javascript" src="js/highlight-active-input.js"></script>
        <link rel="stylesheet" type="text/css" href="css/cupertino/jquery-ui-1.10.3.custom.css"/>

 
    
</head>
<body>
    <div id="container">  
        <?php
        include "pannello.php";

        /*
         * To change this template, choose Tools | Templates
         * and open the template in the editor.
         */
        ?>
          <div id="footer">
                <div id="copy">Copy &copy;2013 <a href="mailto:bartrosso@gmail.com">Bartrosso</a></div>
                <div id="footertext"><div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'it', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script></div>
      <script type="text/javascript">

            initInputHighlightScript();
        </script>
  </div>
    </div>


</body>
</html>