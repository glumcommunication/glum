<script type="text/javascript">
    
    $(function(){ 
        $("#list").jqGrid({
            url:'recartxml.php',
            datatype: 'xml',
            mtype: 'GET',
            colNames:['Data Creazione','Data Modifica', 'Titolo','Pubblica','Ord','Posto in'],
            colModel :[ 
               
                {name:'create', index:'create',width:100,editable:true,sortable:true},
                {name:'edit', index:'edit', width:100,editable:true}, 
                {name:'titolo_it', index:'titolo_it', width:200, align:'left',editable:true},
                {name:'pubblica', index:'pubblica', width:70, align:'center',formatter: 'checkbox',editable:true}, 
                {name:'ord', index:'ord', width:30, align:'center',editable:true},
                {name:'subpage', index:'subpage', width:100,align:'left',editable:true}
      
            ],
            pager: '#pager',
            rowNum:20,
            rowList:[10,20,30],
            sortname: 'id',
            sortorder: 'asc',
            gridResize:true,
            
            forceFit:true,
            viewrecords: true,
            height:'auto',
            gridview: true,
            caption: 'Articoli',
            editurl:"editart.php?idart=id"
        }); 

jQuery("#list").dblclick( function() {
            var s;
            s = jQuery("#list").jqGrid('getGridParam','selrow');
            if( s != null ){
                s = jQuery("#list").jqGrid('getGridParam','selrow');
                var dest= "editart.php?idart="+s;

                $.fancybox({

                    'href' : dest,
                    'width'				: '90%',
                    'height'			: '98%',
                    'autoScale'			: true,
                    'transitionIn'		: 'none',
                    'transitionOut'		: 'none',
                    'hideOnOverlayClick': false,
                    'type'				: 'iframe',
                    'onClosed': function() {
                        parent.location.reload(true);
                    }
                });
            }else {alert('Selezionare una riga');
                window.location = "index.php";  }


        });


  
        jQuery("#list").jqGrid('navGrid','#pager',{add:false,del:false,edit:false,position:'right'});
        jQuery("#m1").click( function() {
            var s;
            s = jQuery("#list").jqGrid('getGridParam','selrow');
            if( s != null ){
                s = jQuery("#list").jqGrid('getGridParam','selrow');
                var dest= "editart.php?idart="+s;
          
                $.fancybox({
                
                    'href' : dest,
                    'width'				: '90%',
                    'height'			: '90%',
                    'autoScale'			: true,
                    'transitionIn'		: 'none',
                    'transitionOut'		: 'none',
                    'hideOnOverlayClick': false,
                    'type'				: 'iframe',
                    'onClosed': function() {
                        parent.location.reload(true);
                    }
                }); 
            }else {alert('Selezionare una riga');
                window.location = "index.php";  }
     
    
        });
        jQuery("#m2").click( function() {
            var s; s = jQuery("#list").jqGrid('getGridParam','selrow');
            if( s != null ){
                s = jQuery("#list").jqGrid('getGridParam','selrow');
              doIt=confirm("Attenzione! Si sta per eliminare la pagina! Proseguire con la cancellazione?");
 if(doIt){
            window.location = "delart.php?idart="+s; }else
            {
                window.location='index.php'; 
            }}else{
            alert('Selezionare una riga');
            }
    
        });
           jQuery("#m3").click( function() {
                           var s; s = jQuery("#list").jqGrid('getGridParam','selrow');
            if( s != null ){
                s = jQuery("#list").jqGrid('getGridParam','selrow');    
                    window.open("../index.php?p="+s,"_blank"); 
            }else {alert('Selezionare una riga');
               }
              
           }
       );
    }); 


    $(document).ready(function() {
        $("#new").fancybox({
            'width'				: '90%',
            'height'			: '90%',
            'autoScale'			: true,
            'transitionIn'		: 'none',
            'transitionOut'		: 'none',
            'hideOnOverlayClick': false,
            'type'				: 'iframe',
            'onClosed': function() {
                parent.location.reload(true);
            }
        });


    });

</script>
<div id="pulsantiera">
<button id="new" href="newart.php">
    <img src="images/plus_16.png">  
  <span class="textbutton">  Nuovo</span>
</button>
<button class="editbutton"  href="javascript:void(0)" id="m1">
    <img src="images/pencil_16.png"> 
    <span class="textbutton">   Modifica</span>
</button>
<button class="delbutton" href="javascript:void(0)" id="m2">
    <img src="images/delete_16.png">
    <span class="textbutton">   Elimina</span>
</button>
  <button class="preview" href="javascript:void(0)" id="m3">
    <img src="images/search_16.png">
    <span class="textbutton"> Anteprima</span>
</button>  
    
</div>
<table id="list"></table>
<div id="pager"></div>
