	<link href="css/cupertino/jquery-ui-1.10.3.custom.css" rel="stylesheet">
	<script src="js/jquery-1.9.1.js"></script>
	<script src="js/jquery-ui-1.10.3.custom.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="css/backend.css" />  


<script language="javascript" type="text/javascript" src="library/tiny_mce/tiny_mce.js"></script>
<script src="js/jquery.validate.js" type="text/javascript"></script> 
<script language="javascript" type="text/javascript">     
    
    $(function() {
        $( "#tabs" ).tabs();
    });
    function confirmUpdate() {
        res= confirm("Uscendo i dati non salvati saranno persi. Continuare?");
        if(res){
            function chiudi() {
                parent.jQuery.fancybox.close();   

            } 
            chiudi();
        }
    }         
        
      
    tinyMCE.init({
        mode : "textareas",
        elements : "content,descrizione",
        theme : "advanced",
        plugins : "advimage,advlink,media,contextmenu",
         theme_advanced_fonts : 
                 "Metamorphous=Metamorphous, sans-serif;"+
         "Andale Mono=andale mono,times;"+
                "Arial=arial,helvetica,sans-serif;"+
                "Arial Black=arial black,avant garde;"+
                "Book Antiqua=book antiqua,palatino;"+
                "Comic Sans MS=comic sans ms,sans-serif;"+
                "Courier New=courier new,courier;"+
                "Georgia=georgia,palatino;"+
                "Helvetica=helvetica;"+
                "Impact=impact,chicago;"+
                "Symbol=symbol;"+
                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
                "Terminal=terminal,monaco;"+
                "Times New Roman=times new roman,times;"+
                "Trebuchet MS=trebuchet ms,geneva;"+
                "Verdana=verdana,geneva;"+
                "Webdings=webdings;"+
                "Wingdings=wingdings,zapf dingbats",

  width: '800',
  height: '400',
  autoresize_min_height: 300,
  autoresize_max_height: 700,
        theme_advanced_buttons1_add_before : "newdocument,separator",
        theme_advanced_buttons1_add : "fontselect,fontsizeselect",
        theme_advanced_buttons2_add : "separator,forecolor,backcolor,liststyle",
        theme_advanced_buttons2_add_before: "cut,copy,separator,",
        theme_advanced_buttons3_add_before : "",
        theme_advanced_buttons3_add : "media",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        extended_valid_elements : "hr[class|width|size|noshade]",
        file_browser_callback : "ajaxfilemanager",
        paste_use_dialog : false,
        theme_advanced_resizing : false,
        theme_advanced_resize_horizontal : false,
        apply_source_formatting : true,
        force_br_newlines : true,
        force_p_newlines : false,	
        relative_urls : false
       
        

        
    }); 

    
    
    function ajaxfilemanager(field_name, url, type, win) {
        var ajaxfilemanagerurl = "../../../../library/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
        var view = 'detail';
        switch (type) {
            case "image":
                view = 'thumbnail';
                break;
            case "media":
                break;
            case "flash": 
                break;
            case "file":
                break;
            default:
                return false;
        }
        tinyMCE.activeEditor.windowManager.open({
            url: "../../../../library/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php?view=" + view,
            width: 800,
            height: 600,
            inline : "yes",
        
            close_previous : "no"
        },{
            window : win,
            input : field_name
        });
            
        /*            return false;			
              var fileBrowserWindow = new Array();
              fileBrowserWindow["file"] = ajaxfilemanagerurl;
              fileBrowserWindow["title"] = "Ajax File Manager";
              fileBrowserWindow["width"] = "782";
              fileBrowserWindow["height"] = "440";
              fileBrowserWindow["close_previous"] = "no";
              tinyMCE.openWindow(fileBrowserWindow, {
                window : win,
                input : field_name,
                resizable : "yes",
                inline : "yes",
                editor_id : tinyMCE.getWindowArg("editor_id")
              });
			
              return false;*/
    }
    
    $(function() {
        $("#datainizio").datepicker({dateFormat: 'dd/mm/yy'});
        $("#datafine").datepicker({dateFormat: 'dd/mm/yy'});
        $("#pubblicada").datepicker({dateFormat: 'dd/mm/yy'});
    
    });
</script>

<!--     $( "#datepicker" ).datepicker({dateFormat: 'dd/mm'});
        $( "#datepicker2" ).datepicker({dateFormat: 'dd/mm'});
-->