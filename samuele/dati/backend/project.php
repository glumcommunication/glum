<script type="text/javascript">

    $(function(){
        $("#list").jqGrid({
            url:'recgalxml.php',
            datatype: 'xml',
            mtype: 'GET',
            colNames:['Data Creazione', 'Titolo','Posta in','Pubblica','Ord'],
            colModel :[

                {name:'create', index:'create',width:100},
                {name:'titolo', index:'titolo', width:350, align:'left',editable:true,sortable:true},
                {name:'parent', index:'parent', width:350, align:'left',editable:true,sortable:true},
                {name:'pubblica', index:'pubblica', width:70, align:'center',formatter: 'checkbox',editable:true},
                {name:'ord', index:'ord', width:30, align:'center',editable:true,sortable:true},

            ],
            pager: '#pager',
            rowNum:10,
            rowList:[10,20,30],
            sortname: 'id',
            sortorder: 'asc',
            viewrecords: true,
            gridview: true,
            caption: 'PRogetti',
            editurl:"editgal.php",
            autowidth: true
        });

        jQuery("#list").dblclick( function() {
            var s;
            s = jQuery("#list").jqGrid('getGridParam','selrow');
            if( s != null ){
                s = jQuery("#list").jqGrid('getGridParam','selrow');
                var dest= "editgal.php?idgal="+s;

                $.fancybox({

                    'href' : dest,
                    'width'				: '90%',
                    'height'			: '98%',
                    'autoScale'			: true,
                    'transitionIn'		: 'none',
                    'transitionOut'		: 'none',
                    'hideOnOverlayClick': false,
                    'type'				: 'iframe',
                    'onClosed': function() {
                        parent.location.reload(true);
                    }
                });
            }else {alert('Selezionare una riga');
                window.location = "index.php";  }


        });

        jQuery("#list").jqGrid('navGrid','#pager',{add:false,del:false,edit:false,position:'right'});


        jQuery("#m1").click( function() {
            var s; s = jQuery("#list").jqGrid('getGridParam','selrow');
            if( s != null ){
                s = jQuery("#list").jqGrid('getGridParam','selrow');
                var dest= "editgal.php?idgal="+s;

                $.fancybox({

                    'href' : dest,
                    'width'				: '90%',
                    'height'			: '90%',
                    'autoScale'			: true,
                    'transitionIn'		: 'none',
                    'transitionOut'		: 'none',
                     'hideOnOverlayClick': false,
                    'type'				: 'iframe',
                    'onClosed': function() {
                        parent.location.reload(true);
                    }
                });
            }else {alert('Selezionare una riga');
                window.location = "?p=project.php";  }


        });
        jQuery("#m2").click( function() {
            var s; s = jQuery("#list").jqGrid('getGridParam','selrow');
            if( s != null ){
                s = jQuery("#list").jqGrid('getGridParam','selrow');
                doIt=confirm("Attenzione! Si sta per eliminare la galleria con tutte e foto che contiene! Proseguire con la cancellazione?");
                if(doIt){
                    window.location = "delgal.php?idgal="+s; }
                                   }else
                                   {
                   alert('Selezionare una riga');
                                   }

        });
    });


    $(document).ready(function() {
        $("#new").fancybox({
            'width'				: '90%',
            'height'			: '90%',
            'autoScale'			: true,
            'transitionIn'		: 'none',
            'transitionOut'		: 'none',
             'hideOnOverlayClick': false,
            'type'				: 'iframe',
            'onClosed': function() {
                parent.location.reload(true);
            }
        });


    });

</script>
<div id="pulsantiera">
    <button id="new" href="newgal.php">
        <img src="images/plus_16.png">
        <span class="textbutton">  Nuovo</span>
    </button>
    <button class="editbutton" id="m1">
        <img src="images/pencil_16.png">
        <span class="textbutton">   Modifica</span>
    </button>
    <button class="delbutton"  id="m2">
        <img src="images/delete_16.png">
        <span class="textbutton">   Elimina</span>
    </button>
</div>
<table id="list"></table>
<div id="pager"></div>
