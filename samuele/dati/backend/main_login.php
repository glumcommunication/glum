<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <title>
            Pannello di Controllo
        </title>

        <!-- Definizione meta tag, per il pannello il posizionamento non serve. Anzi.-->
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <META NAME="Language" CONTENT="it_IT"/>
        <META CONTENT="no-index, follow" NAME="Robots"/>


        <!-- Inclusione fogli di stile-->

        <link rel="stylesheet" type="text/css" href="css/stilelogin.css" />

        <!-- Inclusione librerie js-->
        <script type="text/javascript" src="library/jquery.js"></script>
        <script type="text/javascript" src="library/modernizr.js"></script>
        <script type="text/javascript" src="js/highlight-active-input.js"></script>
        <meta name="google-translate-customization" content="da935cd1bcd0717e-51518e36c8cf2fd6-ga07c7ebeec1250c0-f"></meta>
    </head>
    <body>
        <div class="wrapper">
            <div id="header">
                <img src="images/logo.png" alt="logo ditta" />
                <div class="sign">Accedi al Pannello</div>
            </div>
            <div class="main content">
                <div class="loginbox">
                    <p class="testo-box">
                        <p id="titolino">Login</p>
                        <img src="images/logo.png" alt="logo ditta" height="20px"/>

                        <form method="post" id="modulo_login" >
                            <div id="table">

                                <label class="label">Username:</label>
                                <div class="inputs"><input class="textInput"   name="username" type="text" id="username" maxlength="30" style="width: 100%"/></div>
                                <label class="label">Password:</label>
                                <div class="inputs"><input class="textInput"  name="password" type="password" id="password" maxlength="16" style="width: 100%"/></div>
                                <input type="submit" id="submit" value="Entra" />
                                <div id="messaggio"></div>

                            </div>
                        </form>
                    </p> 

                </div>
            </div>
            <script type="text/javascript">
                $("#modulo_login").submit(function() {
                    // passo i dati (via POST) al file PHP che effettua le verifiche 
                    $.post("login.php", { username: $('#username').val(), password: $('#password').val(), rand: Math.random() }, function(risposta) {
                        // se i dati sono corretti...
                        if (risposta == 1) {
                            
                            // applico l'effetto allo span con id "messaggio"
                            $("#messaggio").fadeTo(200, 0.1, function() {
                                // per prima cosa mostro, con effetto fade, un messaggio di attesa
                                $(this).removeClass().addClass('corretto').text('Login in corso...').fadeTo(900, 1, function() {
                                    // al termine effettuo il redirect alla pagina privata
                                    document.location = 'home.php';
                                });
                            });
                            <?php logs();?>
                            // se, invece, i dati non sono corretti...
                        }else{
                            // stampo un messaggio di errore
                            $("#messaggio").fadeTo(200, 0.1, function() {
                                $(this).removeClass().addClass('errore').text('Dati di login non corretti!').fadeTo(900,1);
                            });
                        }
                    });
                    // evito il submit del form (che deve essere gestito solo dalla funzione Javascript)
                    return false;
                });
            </script>

            <div id="footer">
                <div id="copy">Copy &copy;2012 <a href="mailto:bartrosso@gmail.com">Bartrosso</a></div>
                <div id="footertext"><div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'it', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script></div>
            </div>
        </div>
        <script type="text/javascript">

            initInputHighlightScript();
        </script> 
    </body>
</html>