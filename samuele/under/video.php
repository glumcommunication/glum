<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<title>Samuele Mancini Photographer</title>
<meta name="description" content="Il mi cognato">
<meta name="keywords" content="fotografo, siena,">
<meta name="viewport" content="width=device-width">
<script src="js/modernizr.min.js"></script>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link href='http://fonts.googleapis.com/css?family=Libre+Baskerville' rel='stylesheet' type='text/css'>
</head>
<body>
    <style>
        video#bgvid {
position: fixed; right: 0; bottom: 0;
min-width: 100%; min-height: 100%;
width: auto; height: auto; z-index: -100;
background: url(images/samuelemancini.jpg) no-repeat;
background-size: cover;
}
    </style>
<div role="main">
    <?php ?>
    <div class="container">
        <div class="col-sm-12">
            <video autoplay loop poster="images/samuelemancini.jpg" id="bgvid">
<source src="caffe.mp4" type="video/mp4">
</video>
            
        </div>
    </div>
</div>
<footer>
</footer>
<!--<script src="js/plugins.js"></script>
<script src="js/main.js"></script>-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53273033-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
