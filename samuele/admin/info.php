<?php
include_once "../secure/dbcon.php";

session_start();
// verifico che esista la sessione di autenticazione
if (empty($_SESSION['userid'])) {
    header("location:main_login.php");
    exit;
}

$idnews = '1';
$recinfo = mysql_query("SELECT * FROM contatti ")or die(mysql_error());
while ($info = mysql_fetch_array($recinfo)) {
$email=$info['email'];
$phone=$info['phone'];
$mobile=$info['mobile'];
$cf=$info['cf'];
$piva=$info['piva'];
$indirizzo=$info['indirizzo'];
$varie=$info['varie'];
    
    
    
    
}

include "header.php";
include "nav.php";
?>
<script src="ckeditor/ckeditor.js"></script>
<style>

    /* Style the CKEditor element to look like a textfield */
    .cke_textarea_inline
    {
        padding: 10px;
        height: 200px;
        overflow: auto;
        border: 1px solid lightseagreen;
        -webkit-appearance: textfield;
    }

</style>
<script type="text/javascript" src="js/bootstrap-datepicker.it.js" charset="UTF-8"></script>
<script>
    $(document).ready(function() {
        $('#formnews').bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            }
        });
    });
</script>
<div class="container">

    <div class="row text-center" >
        <p class="titolopagina">Modifica i dati dei contatti</p>
    </div>
    <div class="row">
<?php include "ytmenu.php"; ?>
        <div class="col-sm-8 col-sm-offset-0">
            <form class="form-horizontal" id="formnews" method="post"  enctype="multipart/form-data" action="saveinfo.php">
                <fieldset class="fieldset">
                    <input type="hidden" name="action" id="action" value="edit"/>
                   <div class="form-group">
                        <label for="email" class="col-sm-2 col-lg-2 control-label">Email</label>
                        <div class="col-sm-8">
                            <input type='email' class=" form-control  anagrafe "  id="email" name="email" value='<?php echo $email; ?>'/>
                        </div>
                    </div>
                      <div class="form-group">
                        <label for="cel" class="col-sm-2 col-lg-2 control-label">Telefono</label>
                        <div class="col-sm-8">
                            <input type='tel' class=" form-control  anagrafe " id="phone" name="phone" value='<?php echo $phone; ?>'/>
                        </div>
                    </div>
                      <div class="form-group">
                        <label for="mobile" class="col-sm-2 col-lg-2 control-label">Mobile</label>
                        <div class="col-sm-8">
                            <input type='tel' class=" form-control  anagrafe "  id="mobile" name="mobile" value='<?php echo $mobile; ?>'/>
                        </div>
                    </div>
                      <div class="form-group">
                        <label for="cf" class="col-sm-2 col-lg-2 control-label">Codice Fiscale</label>
                        <div class="col-sm-8">
                            <input type='text' class=" form-control  anagrafe " id="cf" name="cf" value='<?php echo $cf; ?>'/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="piva" class="col-sm-2 col-lg-2 control-label">Partita Iva</label>
                        <div class="col-sm-8">
                            <input type='text' class=" form-control  anagrafe " id="piva" name="piva" value='<?php echo $piva; ?>'/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="varie" class="col-sm-2 col-lg-2 control-label">Varie</label>
                        <div class="col-sm-8">
                            <textarea class=" form-control  anagrafe " type="text" id="varie" name="varie" ><?php echo $varie; ?></textarea>
                        </div>
                    </div>   
                   
                    <div class="form-group">
                        <div class="col-sm-8 col-lg-5 col-sm-offset-2">
                            <button type="submit" class="btn btn-default">Invia</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

</div>

<script>
    CKEDITOR.replace('varie');


</script>
<script src="js/ytmenu.js"></script>
