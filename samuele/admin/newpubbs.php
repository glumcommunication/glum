<?php

session_start();
// verifico che esista la sessione di autenticazione
if (empty($_SESSION['userid'])) {
    header("location:main_login.php");
    exit;
}
include "header.php";
include "nav.php";
?>
<script src="ckeditor/ckeditor.js"></script>
<style>

    /* Style the CKEditor element to look like a textfield */
    .cke_textarea_inline
    {
        padding: 10px;
        height: 200px;
        overflow: auto;
        border: 1px solid lightseagreen;
        -webkit-appearance: textfield;
    }

</style>
<script type="text/javascript" src="js/bootstrap-datepicker.it.js" charset="UTF-8"></script>
<script>
    $(function() {

        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#datepicker').datepicker({
            language: 'it',
            onRender: function(date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('#datepicker2')[0].focus();
        }).data('datepicker');
        var checkout = $('#datepicker2').datepicker({
            language: 'it',
            onRender: function(date) {
                return date.valueOf()+1 <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');

    });
    $(document).ready(function() {
        $('#formnews').bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            }
        });
    });
</script>
<div class="container">
    
    <div class="row text-center" >
        <p class="titolopagina">Inserisci una nuova pubblicazione</p>
    </div>
    <div class="row">
       <?php include "ytmenu.php";?>
        <div class="col-sm-8 col-sm-offset-0">
            <form class="form-horizontal" id="formnews" method="post"  enctype="multipart/form-data" action="savepub.php">
                <fieldset class="fieldset">
                    <input type="hidden" name="action" id="action" value="new"/>
                    <div class="form-group">
                        <label for="titolo" class="col-sm-2 col-lg-2 control-label">Titolo</label>
                        <div class="col-sm-8 col-lg-5">
                            <input type="text"  class="form-control required" id="titolo" name="titolo" placeholder="Inserisci il titolo...">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="corponews" class="col-sm-2 col-lg-2 control-label">Corpo</label>
                        <div class="col-sm-8">
                            <textarea class=" form-control  anagrafe " type="text" id="corponews" name="corponews" ></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 col-lg-2 control-label ">Data Pubblicazione</label>
                        <div class="col-sm-8 col-lg-5">
                            <input type="text" class="form-control" data-date-format="dd-mm-yyyy" id="datepicker" name="datepicker">
                        </div>
                    </div>
                
                   
                       <div class="form-group">
                        <label for="pubblica" class="col-sm-2 col-lg-2 control-label">Pubblica</label>
                        <div class="col-sm-8 col-lg-5">
                            <input type="checkbox" name="pubblica" id="pubblica" checked="checked">
                        </div>
                    </div>
                      <div class="form-group">
                        <label for="pubblica" class="col-sm-2 col-lg-2 control-label">Copertina</label>
                        <div class="col-sm-8 col-lg-5">
                    <input name="image" id="image" size="30" type="file">  

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-lg-5 col-sm-offset-2">
                            <button type="submit" class="btn btn-default">Invia</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

</div>

<script>
    CKEDITOR.replace('corponews');


</script>
<script src="js/ytmenu.js"></script>
