
<?php

function ripulisci($v) {

    if (is_array($v)) {

        foreach ($v as $k => $val) {

            $v[$k] = htmlentities(strip_tags(trim($val)), ENT_QUOTES);
        }
    } else {

        $v = htmlentities(strip_tags(trim($v)), ENT_QUOTES);
    }

    return $v;
}
?>