
<?php
include_once "../secure/dbcon.php";
$upload_dir = '../img/upload/';
if (isset($_FILES['file1']) && $_FILES['file1']['tmp_name'] != '') {

    $file_tmp = $_FILES['file1']['tmp_name'];

    if ($_FILES['file1']['size'] > '2000000') {
        $msg = "<p>Il file non deve superare i 2MB!!</p>";
    }
    list($width, $height, $type, $attr) = getimagesize($_FILES['file1']['tmp_name']);
    if (($type != 1) && ($type != 2) && ($type != 3)) {
        $msg = "<p>Formato non corretto!!</p>";
    }

    $nomeimg = time() . $_FILES['file1']['name'];
    $urlimg = $upload_dir . $nomeimg;
    $uploaded = "../img/upload/" . $nomeimg;
    $bip = time();
    $destinazione = "img/upload/$bip.jpg";
    $pathimg = "img/upload/$bip.jpg";
    $cropname = "img/upload/crop_$bip.jpg";
    mysql_query("INSERT home (`id`,`path`,`value`,`active`,`ord`) VALUES (NULL,'$cropname','1','on','1') ")or die(mysql_error());


    if ($_FILES['file1']['size'] <= '2097152') {
        move_uploaded_file($file_tmp, $urlimg);
    } else {
        echo "immagine troppo grande, caricare immagini 72dpi, max 2 MB, (Es.Max 2000*1500 px) ";
    }

    include "header.php";
    ?>

    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.Jcrop.min.js"></script>
    <link rel="stylesheet" href="css/jquery.Jcrop.css" type="text/css" />
    <script type="text/javascript">

        jQuery(function() {
            jQuery('#cropbox').Jcrop({
                onSelect: updateCoords,
                bgColor: 'black',
                bgOpacity: .4,
                setSelect: [0, 0, 300, 300],
                aspectRatio: 300 / 300,
                boxWidth: 814,
                boxHeight: 544
            });
        });



        function updateCoords(c)
        {
            $('#x').val(c.x);
            $('#y').val(c.y);
            $('#w').val(c.w);
            $('#h').val(c.h);
        }
        ;

        function checkCoords()
        {
            if (parseInt($('#w').val()))
                return true;
            alert('Please select a crop region then press submit.');
            return false;
        }
        ;
    </script>
    <?php
    include "nav.php";
    ?><div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <p>Ritaglia la foto 1x1</p>
                <img src="<?php echo $uploaded; ?>" id="cropbox" />

                <form action="crop2.php" method="post" onsubmit="return checkCoords();">
                    <input type="hidden" id="x" name="x" />
                    <input type="hidden" id="y" name="y" />
                    <input type="hidden" id="w" name="w" />
                    <input type="hidden" id="h" name="h" />
                    <input type="hidden" id="page" name="page" value="bio" />
                    <input type="hidden" name="uploaded" value="<?php echo $uploaded; ?>">
                    <input type="hidden" name="destinazione" value="<?php echo $destinazione; ?>">
                    <input type="hidden" name="cropname" value="<?php echo $cropname; ?>">
                    <input type="hidden" name="pathimage" value="<?php echo $pathimg; ?>">
                    <input type="submit" value="Crop Image" />
                </form>
            </div>
        </div>
    </div>
    <?php
    include "footer.php";
} elseif (isset($_FILES['file2']) && $_FILES['file2']['tmp_name'] != '') {

    $file_tmp = $_FILES['file2']['tmp_name'];

    if ($_FILES['file2']['size'] > '2000000') {
        $msg = "<p>Il file non deve superare i 2MB!!</p>";
    }
    list($width, $height, $type, $attr) = getimagesize($_FILES['file2']['tmp_name']);
    if (($type != 1) && ($type != 2) && ($type != 3)) {
        $msg = "<p>Formato non corretto!!</p>";
    }
    $nomeimg = time() . $_FILES['file2']['name'];
    $urlimg = $upload_dir . $nomeimg;
    $uploaded = "../img/upload/" . $nomeimg;
    $bip = time();
    $destinazione = "img/upload/$bip.jpg";
    $pathimg = "img/upload/$bip.jpg";
    $cropname = "img/upload/crop_$bip.jpg";
    mysql_query("INSERT home (`id`,`path`,`value`,`active`,`ord`) VALUES (NULL,'$cropname','4','on','1') ")or die(mysql_error());
    if ($_FILES['file2']['size'] <= '2097152') {
        move_uploaded_file($file_tmp, $urlimg);
    } else {
        echo "immagine troppo grande, caricare immagini 72dpi, max 2 MB, (Es.Max 2000*1500 px) ";
    }

    include "header.php";
    ?>

    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.Jcrop.min.js"></script>
    <link rel="stylesheet" href="css/jquery.Jcrop.css" type="text/css" />
    <script type="text/javascript">

                    jQuery(function() {
                        jQuery('#cropbox').Jcrop({
                            onSelect: updateCoords,
                            bgColor: 'black',
                            bgOpacity: .4,
                            setSelect: [0, 0, 300, 300],
                            aspectRatio: 600 / 600,
                            boxWidth: 814,
                            boxHeight: 544
                        });
                    });



                    function updateCoords(c)
                    {
                        $('#x').val(c.x);
                        $('#y').val(c.y);
                        $('#w').val(c.w);
                        $('#h').val(c.h);
                    }
                    ;

                    function checkCoords()
                    {
                        if (parseInt($('#w').val()))
                            return true;
                        alert('Please select a crop region then press submit.');
                        return false;
                    }
                    ;
    </script>
    <?php
    include "nav.php";
    ?><div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <p>Ritaglia la foto 2x2</p>
                <img src="<?php echo $uploaded; ?>" id="cropbox" />

                <form action="crop2.php" method="post" onsubmit="return checkCoords();">
                    <input type="hidden" id="x" name="x" />
                    <input type="hidden" id="y" name="y" />
                    <input type="hidden" id="w" name="w" />
                    <input type="hidden" id="h" name="h" />
                    <input type="hidden" id="page" name="page" value="bio" />
                    <input type="hidden" name="uploaded" value="<?php echo $uploaded; ?>">
                    <input type="hidden" name="destinazione" value="<?php echo $destinazione; ?>">
                    <input type="hidden" name="cropname" value="<?php echo $cropname; ?>">
                    <input type="hidden" name="pathimage" value="<?php echo $pathimg; ?>">


                    <input type="submit" value="Crop Image" />
                </form>
            </div>
        </div>
    </div>
    <?php
    include "footer.php";
} elseif (isset($_FILES['file3']) && $_FILES['file3']['tmp_name'] != '') {

    $file_tmp = $_FILES['file3']['tmp_name'];

    if ($_FILES['file3']['size'] > '2000000') {
        $msg = "<p>Il file non deve superare i 2MB!!</p>";
    }
    list($width, $height, $type, $attr) = getimagesize($_FILES['file3']['tmp_name']);
    if (($type != 1) && ($type != 2) && ($type != 3)) {
        $msg = "<p>Formato non corretto!!</p>";
    }

    $nomeimg = time() . $_FILES['file3']['name'];
    $urlimg = $upload_dir . $nomeimg;
    $uploaded = "../img/upload/" . $nomeimg;
    $bip = time();
    $destinazione = "img/upload/$bip.jpg";
    $pathimg = "img/upload/$bip.jpg";
    $cropname = "img/upload/crop_$bip.jpg";
    mysql_query("INSERT home (`id`,`path`,`value`,`active`,`ord`) VALUES (NULL,'$cropname','8','on','1') ")or die(mysql_error());


    if ($_FILES['file3']['size'] <= '2097152') {
        move_uploaded_file($file_tmp, $urlimg);
    } else {
        echo "immagine troppo grande, caricare immagini 72dpi, max 2 MB, (Es.Max 2000*1500 px) ";
    }

    include "header.php";
    ?>

    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.Jcrop.min.js"></script>
    <link rel="stylesheet" href="css/jquery.Jcrop.css" type="text/css" />
    <script type="text/javascript">

                    jQuery(function() {
                        jQuery('#cropbox').Jcrop({
                            onSelect: updateCoords,
                            bgColor: 'black',
                            bgOpacity: .4,
                            setSelect: [0, 0, 300, 300],
                            aspectRatio: 1200 / 600,
                            boxWidth: 814,
                            boxHeight: 544
                        });
                    });



                    function updateCoords(c)
                    {
                        $('#x').val(c.x);
                        $('#y').val(c.y);
                        $('#w').val(c.w);
                        $('#h').val(c.h);
                    }
                    ;

                    function checkCoords()
                    {
                        if (parseInt($('#w').val()))
                            return true;
                        alert('Please select a crop region then press submit.');
                        return false;
                    }
                    ;
    </script>
    <?php
    include "nav.php";
    ?><div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <p>Ritaglia la foto 2x4 </p>
                <img src="<?php echo $uploaded; ?>" id="cropbox" />

                <form action="crop2.php" method="post" onsubmit="return checkCoords();">
                    <input type="hidden" id="x" name="x" />
                    <input type="hidden" id="y" name="y" />
                    <input type="hidden" id="w" name="w" />
                    <input type="hidden" id="h" name="h" />
                    <input type="hidden" id="page" name="page" value="bio" />
                    <input type="hidden" name="uploaded" value="<?php echo $uploaded; ?>">
                    <input type="hidden" name="destinazione" value="<?php echo $destinazione; ?>">
                    <input type="hidden" name="cropname" value="<?php echo $cropname; ?>">
                    <input type="hidden" name="pathimage" value="<?php echo $pathimg; ?>">

                    <input type="submit" value="Crop Image" />
                </form>
            </div>
        </div>
    </div>
    <?php
    include "footer.php";
} else {
    header("location:home.php");
}