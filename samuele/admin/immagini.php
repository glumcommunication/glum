<?php
session_start();
// verifico che esista la sessione di autenticazione
if (empty($_SESSION['userid'])) {
    header("location:main_login.php");
    exit;
}

$idgal = $_GET['idgal'];
include "header.php";
include "nav.php";
?>


  <script type="text/javascript">
jQuery(document).ready(function($){
 $("#deleteArea").droppable({
 accept: '#ordgal > li',
 hoverClass: 'dropAreaHover',
 drop: function(event, ui) {
        var responce=  confirm("Si sta per eliminare la foto. Procedere?");
        if(responce===false){location.reload();
 exit;}
 deleteImage(ui.draggable,ui.helper);
 },
 activeClass: 'dropAreaHover'
 });
 function deleteImage($draggable,$helper){
 params = 'PID=' + $draggable.attr('num')+'&idgal='+$draggable.attr('idgal');
 $.ajax({
 url: 'deleteImage.php',
 type: 'POST',
 data: params
 });
 
 $helper.effect('transfer', { to: '#deleteArea', className: 'ui-effects-transfer' },500);
 $draggable.remove();
 }
});
</script>
<style type="text/css">
    li { cursor: move; }

</style>
<script type="text/javascript">

    $(function() {
        $("#ordgal").sortable({
            placeholder: 'ui-state-highlight',
            update: function(e, ui) {
                var order = $('#ordgal').sortable('serialize');
                $.post('reorderimg.php', order);
            }
        });
    });
</script> 
<style>

    .cellafoto{margin-bottom: 10px;border:1px solid #0099CC; text-align: center; -webkit-border-radius: 10px;
               -moz-border-radius: 10px;
               border-radius: 10px;}
    #containerpreview{background-color: white;
                      width: 100%;
                      margin:auto;

                      padding-bottom: 20px;
    }
    #ordgal{background-color: white;
            width:100%;               height: 200px;


    }

    #ordgal li{width: 200px; float:left;
               list-style: none;
               margin-right: 10px;
               height: 200px;
               padding: 0;
    }
    #ordgal li img{width: 180px; margin-top: 10px;}
    .ui-state-highlight{background-color: lightgrey;}
</style>S

<div class="container" style="margin-bottom:200px;">
    <div class="row text-center" >
        <p class="titolopagina">Ordina Immagini in Home  <a href="uploadimg.php?idgal=<?php echo $idgal; ?>"><span class="btn btn-default">AGGIUNGI FOTO</span></a>

 <div id="deleteArea" style="float: right;">
            <span class="glyphicon glyphicon-trash" style="font-size: 40px;padding: 40px; border:1px solid black;"></span>
        </div>
        </p> 
          
   </div>
    <div class="row">
        <?php include "ytmenu.php"; ?>
        <div class="col-sm-10 col-sm-offset-0">


            <ul id="ordgal">

                <?php
                $recuperafoto = mysql_query("SELECT * FROM image WHERE idgallery='$idgal' ORDER by ord")or die(mysql_error());

                while ($foto = mysql_fetch_array($recuperafoto)) {
                    $id = $foto['id'];
                    $ord = $foto['ord'];
                    $pathsmall = $foto['path'];
                    echo "<li class='cellafoto' num='$id' id ='photo_$id'><img src='$pathsmall' ></li>";
                }
                ?>
            </ul>
        </div>
    </div>
</div>

<?php
include "footer.php";
?>


