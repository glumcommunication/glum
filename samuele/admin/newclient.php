<?php

session_start();
// verifico che esista la sessione di autenticazione
if (empty($_SESSION['userid'])) {
    header("location:main_login.php");
    exit;
}
include "header.php";
include "nav.php";
?>
<script src="ckeditor/ckeditor.js"></script>
<style>

    /* Style the CKEditor element to look like a textfield */
    .cke_textarea_inline
    {
        padding: 10px;
        height: 200px;
        overflow: auto;
        border: 1px solid lightseagreen;
        -webkit-appearance: textfield;
    }

</style>
<script type="text/javascript" src="js/bootstrap-datepicker.it.js" charset="UTF-8"></script>
<script>
    $(function() {

        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

        var checkin = $('#datepicker').datepicker({
            language: 'it',
            onRender: function(date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            if (ev.date.valueOf() > checkout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            checkin.hide();
            $('#datepicker2')[0].focus();
        }).data('datepicker');
        var checkout = $('#datepicker2').datepicker({
            language: 'it',
            onRender: function(date) {
                return date.valueOf()+1 <= checkin.date.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');

    });
    $(document).ready(function() {
        $('#formnews').bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            }
        });
    });
</script>
<div class="container">
    
    <div class="row text-center" >
        <p class="titolopagina">Inserisci un nuovo Cliente</p>
    </div>
    <div class="row">
       <?php include "ytmenu.php";?>
        <div class="col-sm-8 col-sm-offset-0">
            <form class="form-horizontal" id="formnews" method="post"  enctype="multipart/form-data" action="saveclient.php">
                <fieldset class="fieldset">
                    <input type="hidden" name="action" id="action" value="new"/>
                   
                    <div class="form-group">
                        <label for="url" class="col-sm-2 col-lg-2 control-label">Link</label>
                        <div class="col-sm-8 col-lg-5">
                            <input type="url" 
                                   class="form-control" id="link" name='url' value="http://" placeholder="Inserisci un link con http..."/>
                        </div>
                    </div>
                       <div class="form-group">
                        <label for="pubblica" class="col-sm-2 col-lg-2 control-label">Pubblica</label>
                        <div class="col-sm-8 col-lg-5">
                            <input type="checkbox" name="pubblica" id="pubblica" checked="checked">
                        </div>
                    </div>
                      <div class="form-group">
                        <label for="pubblica" class="col-sm-2 col-lg-2 control-label">Copertina</label>
                        <div class="col-sm-8 col-lg-5">
                    <input name="image" id="image" size="30" type="file">  

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-lg-5 col-sm-offset-2">
                            <button type="submit" class="btn btn-default">Invia</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

</div>

<script>
    CKEDITOR.inline('corponews');


</script>
<script src="js/ytmenu.js"></script>
