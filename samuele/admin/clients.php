<?php
session_start();
// verifico che esista la sessione di autenticazione
if (empty($_SESSION['userid'])) {
    header("location:login/main_login.php");
    exit;
}

include "header.php";
include "nav.php";
?>
<div class="container">
    
    <div class="row text-center" >
        <p class="titolopagina">Elenco Clienti in Archivio</p>
    </div>
    <div class="row">
       <?php include "ytmenu.php";?>
        <div class="col-sm-8 col-sm-offset-0">
  

            <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Link</th>
                        <th>Logo</th>
                        <th>Creata il:</th>
                        <th>Edit il:</th>
                        <th>Attiva</th>
                        <th><span class="glyphicon glyphicon-list-alt"></span></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $recnews = mysql_query("SELECT * FROM clienti");
                    while ($news = mysql_fetch_array($recnews)) {
                        $id = $news['id'];
                        $create = $news['create'];
                        $edit = $news['edit'];
                        $pubblica = $news['pubblica'];
                        $url = $news['url'];
                        $img = $news['path'];
                        ?>
                        <tr>
                            <td><?php echo $id; ?></td>
                          <td><?php echo $url; ?></td>
                           <td><img src="../<?php echo $img; ?>" width='30px;'></td>
                            <td><?php echo $create; ?></td>
                            <td><?php echo $edit; ?></td>
                            <td class="text-center"><?php
                                if ($pubblica == 'on') {
                                    echo"<span class='glyphicon glyphicon-ok'></span>";
                                } else {
                                    echo"<span class='glyphicon glyphicon-lock'></span>";
                                }
                                ?></td>
                            <td><a href="editclient.php?id=<?php echo $id; ?>"><span class="glyphicon glyphicon-pencil"></span></a><a onClick="return confirm('Sicuro di cancellare questa news?')" href="delclient.php?id=<?php echo $id; ?>"><span class="glyphicon glyphicon-trash"></span></a></td>

                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <span class="label label-success" ><a href="newclient.php" style="color:white; font-weight: bold;">AGGIUNGI CLIENTE <span class="glyphicon glyphicon-plus" style="color:white;"></span></span></a></span>

        </div>

    </div>

</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').dataTable({
            "bProcessing": true,
            "oLanguage": {
                "sLengthMenu": "Mostra _MENU_ oggetti",
                "sSearch": "Cerca:",
                "sInfo": "Da _START_ a _END_ di _TOTAL_ oggetti",
                "sZeroRecords": "Nessun oggetto trovato",
                "sEmptyTable": "Nessun oggetto trovato",
                "oPaginate": {
                    "sFirst": "Prima",
                    "sLast": "Ultima",
                    "sNext": "Successiva",
                    "sPrevious": "Precedente"
                }
            },
            "bJQueryUI": true,
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false,
            "sPaginationType": "full_numbers"
        });
    });
</script>




<?php
include "footer.php";
?>
