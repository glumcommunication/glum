<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

    <?php include_once "../secure/dbcon.php";
    include_once"function.php"; ?>
    
    <head>

        <title>Samuele Mancini ADMIN CPanel</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta charset="utf-8">

        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="stylesheets/stilelogin.css" />
        <link rel="stylesheet" type="text/css" href="css/component.css" />
        <!--<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/flick/jquery-ui.css">    -->
        <link href='http://fonts.googleapis.com/css?family=Libre+Baskerville' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css"/>
        <link href="css/datepicker.css" rel="stylesheet">
        <link href="css/jquery.dataTables.min.css" media="screen" rel="stylesheet" type="text/css"/>
        
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
        <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/highlight-active-input.js"></script>
        <script src="../js/modernizr.min.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>

        <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.0/js/bootstrapValidator.min.js"></script>

    </head>
    <body>

