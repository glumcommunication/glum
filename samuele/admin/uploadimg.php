<?php
session_start();
// verifico che esista la sessione di autenticazione
if (empty($_SESSION['userid'])) {
    header("location:main_login.php");
    exit;
}

$idgal = $_GET['idgal'];
include "header.php";
include "nav.php";
?>


<style type="text/css">@import url(tools/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css);</style>

<script type="text/javascript" src="tools/plupload/js/plupload.full.js"></script>
<script type="text/javascript" src="tools/plupload/js/jquery.plupload.queue/jquery.plupload.queue.js"></script>

<script type="text/javascript">
// Convert divs to queue widgets when the DOM is ready
    $(function() {
        $("#uploader").pluploadQueue({
            // General settings
            runtimes: 'html5,flash,silverlight',
            url: 'upload.php?idgal=<?php echo $idgal; ?>',
            max_file_size: '10mb',
            chunk_size: '1mb',
            unique_names: true,
            // Resize images on clientside if we can
            resize: {width: 1022, height: 681, quality: 85},
            // Specify what files to browse for
            filters: [
                {title: "Image files", extensions: "jpg,gif,png"},
                {title: "Zip files", extensions: "zip"}
            ],
            // Flash settings
            flash_swf_url: 'tools/plupload/js/plupload.flash.swf',
            // Silverlight settings
            silverlight_xap_url: 'tools/plupload/js/plupload.silverlight.xap'
        });
        // Client side form validation
        $('form').submit(function(e) {
            var uploader = $('#uploader').pluploadQueue();

            // Files in queue upload them first
            if (uploader.files.length > 0) {
                // When all files are uploaded submit form
                uploader.bind('StateChanged', function() {
                    if (uploader.files.length === (uploader.total.uploaded + uploader.total.failed)) {
                        $('form')[0].submit();
                    }
                });
                uploader.start();
            } else {
                alert('You must queue at least one file.');
            }
            return false;
        });
        uploader.splice();

uploader.refresh();
    });
</script>   
        <?php

        function sanitizeString($var) {
            $var = strip_tags($var);
            $var = stripslashes($var);
            return htmlentities($var, ENT_COMPAT, "UTF-8");
        }

        function toAscii($str, $replace = array(), $delimiter = '-') {
            if (!empty($replace)) {
                $str = str_replace((array) $replace, ' ', $str);
            }

            $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
            $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
            $clean = strtolower(trim($clean, '-'));
            $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

            return $clean;
        }

        if ($_POST['doInsert'] == 'Carica i file' || $action == 'add') {

            $oggi = getdate();
            $progetto = addslashes($_POST['progetto']);
            $galleryowner = addslashes($_POST['responsabile']);
            $gallerytech = addslashes($_POST['tecnico']);
            $galleryyear = addslashes($_POST['anno']);
            $slug = toAscii($progetto);
            $uploader_count = $_POST['uploader_count'];
            $data = $oggi['year'] . "-" . $oggi['mon'] . "-" . $oggi['mday'];
            $ora = $oggi['hours'] . ":" . $oggi['minutes'] . ":" . $oggi['seconds'];


            $query_ins_gallery = "INSERT INTO gallery (Name, Date, Hour, Size, Slug, extra_1, extra_2, extra_3) VALUES" .
                    "('$progetto', '$data', '$ora', '$uploader_count', '$slug', '$galleryowner', '$gallerytech', '$galleryyear' )";
            $result = mysql_query($query_ins_gallery);
            $lastid = mysql_insert_id();

            if (!$result)
                die("Database access failed: " . mysql_error());
            $message = "Stai per pubblicare la fotogallery \"" . stripslashes($progetto) . "\"";
            $added = '1';
        }
        elseif ($_POST['doSave'] == 'Salva la fotogallery') {
            $cover = $_POST['cover'];
            $gallery_id = $_POST['id'];
            $query_reset = "UPDATE gallery_pics SET Cover='0' WHERE gallery_id='$gallery_id'";
            $result_reset = mysql_query($query_reset);
            $query_cover = "UPDATE gallery_pics SET Cover='1' WHERE pic_id='$cover'";
            $result_cover = mysql_query($query_cover);
            if (!$result_reset || !$result_cover)
                die("Database access failed: " . mysql_error());
            $sql = "SELECT * FROM gallery WHERE id='$gallery_id'";
            $result = mysql_query($sql);
            if (!$result)
                die("Database access failed: " . mysql_error());
            $editrow = mysql_fetch_row($result);
            $galleryname = stripslashes($editrow[0]);
            $message = "La fotogallery \"" . stripslashes($galleryname) . "\" &egrave; stata salvata correttamente.
	Clicca <a href=\"/progetto.php?id=" . $gallery_id . "\">qui</a> per visualizzarla.";
            $saved = '1';
        }
        elseif ($_POST['doEdit'] == 'Salva Modifiche') {

            $galleryname = $_POST['progetto'];
            $galleryowner = $_POST['responsabile'];
            $gallerytech = $_POST['tecnico'];
            $galleryyear = $_POST['anno'];
            $gallery_id = $_POST['id'];
            $cover = $_POST['cover'];
            $query_title = "UPDATE gallery SET Name='$galleryname' WHERE id='$gallery_id'";
            $result_title = mysql_query($query_title);
            $query_owner = "UPDATE gallery SET extra_1='$galleryowner' WHERE id='$gallery_id'";
            $result_owner = mysql_query($query_owner);
            $query_tech = "UPDATE gallery SET extra_2='$gallerytech' WHERE id='$gallery_id'";
            $result_tech = mysql_query($query_tech);
            $query_year = "UPDATE gallery SET extra_3='$galleryyear' WHERE id='$gallery_id'";
            $result_year = mysql_query($query_year);
            $query_reset = "UPDATE gallery_pics SET Cover='0' WHERE gallery_id='$gallery_id'";
            $result_reset = mysql_query($query_reset);
            $query_cover = "UPDATE gallery_pics SET Cover='1' WHERE pic_id='$cover'";
            $result_cover = mysql_query($query_cover);
            if (!$result_reset || !$result_cover || !$result_title)
                die("Database access failed: " . mysql_error());
            $message = "Hai modificato la fotogallery \"" . stripslashes($galleryname) . "\".";
            $edited = '1';
        }
        ?>
<div class="container" style="margin-bottom:200px;">
    <div class="row text-center" >
        <p class="titolopagina">Ordina Immagini in Home  <a href="immagini.php?idgal=<?php echo $idgal; ?>"><span class="btn btn-default">GESTISCI FOTO</span></a>
        </p> 
    </div>
    <div class="row">
        <?php include "ytmenu.php"; ?>
        <div class="col-sm-10 col-sm-offset-0">

<form method="post" action="immagini.php" class="default ">


                <div id="uploader" style="clear:both;">
                    <p>You browser doesn't have Flash, Silverlight, Gears, BrowserPlus or HTML5 support.</p>
                </div>
                <div class="idtable">
                    <div class="idrow">
                        <div class="idcell_1" style="text-align:center;">
                          
                        </div>
                    </div>
                </div>
            
</form>
        </div>
    </div>
</div>

<?php
include "footer.php";
?>

