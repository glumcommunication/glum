<?php

include "header.php";

?>


<div role="main">
    <div class="container">
        <div class="row" id="formlogin">
     
           
            <div class="col-xs-12 col-sm-4 col-sm-offset-4 text-center">
                <img src="../img/logo.png" alt="logo ditta" class="img-responsive left"/>

                <form method="post" id="modulo_login" >
                    <div id="table">
                        <div class="form-group"><input class="form-control"   name="username" type="text"     id="username" style="width: 100%" placeholder="Username"/></div>
                        <div class="form-group"><input class="form-control"   name="password" type="password" id="password" style="width: 100%" placeholder="Password"/></div>
                        
                        
                        <button type="submit" class="btn btn-default" name="doSend" value="Invia" id="accedi">Accedi</button>
                    </div>
                </form>
                
                <div id="messaggio"></div>
                
            </div>
            <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
            <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
            <script type="text/javascript">
                $(document).ready(function() {
                    if (Modernizr.touch) {
                        $("figButton").click(function() {
                            $(this).toggleClass("cap-left");
                        });
                        $("figClose").click(function() {
                            $(this).toggleClass("cap-left");
                        });
                    }
                    ;
                });

            </script>

            <script type="text/javascript">
                $("#modulo_login").submit(function() {
                    // passo i dati (via POST) al file PHP che effettua le verifiche 
                    $.post("login.php", {username: $('#username').val(), password: $('#password').val(), code: $('#code').val(), rand: Math.random()}, function(risposta) {
                        // se i dati sono corretti...
                        if (risposta == 1) {
                            // applico l'effetto allo span con id "messaggio"
                            $("#messaggio").fadeTo(200, 0.1, function() {
                                // per prima cosa mostro, con effetto fade, un messaggio di attesa
                                $(this).removeClass().addClass('corretto').text('Login Effettuato.').fadeTo(900, 1, function() {
                                    // al termine effettuo il redirect alla pagina privata
                                    document.location = 'main.php';
                                });
                            });
                            // se, invece, i dati non sono corretti...
                        } else {
                            // stampo un messaggio di errore
                            $("#messaggio").fadeTo(200, 0.1, function() {
                                $(this).removeClass().addClass('errore').text('Dati non corretti!').fadeTo(900, 1);
                            });
                        }
                    });
                    // evito il submit del form (che deve essere gestito solo dalla funzione Javascript)
                    return false;
                });</script>
        </div>


    </div>
</div>
<script type="text/javascript">

    initInputHighlightScript();
</script> 
<?php include "../footer.php"; ?>
