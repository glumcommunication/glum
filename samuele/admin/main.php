<?php
session_start();
// verifico che esista la sessione di autenticazione
if (empty($_SESSION['userid'])) {
    header("location:main_login.php");
    exit;
}
include "header.php";
include "nav.php";?>
<div class="container">
    <div class="row">
        <div class="text-left">
          
        </div>
    </div>
    <div class="row">
        
        <div class="box col-sm-2" >
            <div class="box2 nuovo col-sm-12 " >
                <a href="newnews.php">
                    <p class="titolobox">New News</p>
                    <span class="glyphiconhome glyphicon glyphicon-paperclip"></span>
                </a>
            </div>
        </div>
        <div class="box col-sm-2 " >
            <div class="box2 nuovo col-sm-12 " >
                <a href="newgals.php">
                       <p class="titolobox">New Gallery</p>
                    <span class="glyphicon glyphiconhome glyphicon-picture"></span>
                </a>
            </div>
        </div>
        <div class="box col-sm-2 " >
             <div class="box2  nuovo col-sm-12 " >
                <a href="newpubbs.php">
                       <p class="titolobox">New Pubb</p>
                    <span class="glyphicon glyphiconhome glyphicon-book"></span>
                </a>
            </div>
        </div>
        <div class="box col-sm-2 " >
             <div class="box2  nuovo col-sm-12 " >
                <a href="newclient.php">
                     <p class="titolobox">New Client</p>
                    <span class="glyphicon glyphiconhome glyphicon-user"></span>
                </a>
            </div>
        </div>
      
        
    </div>
     <div class="row">
        <div class="text-left">
          
        </div>
    </div>
     <div class="row" >
        <div class="box col-sm-2" >
            <div class="box2 edit col-sm-12 " >
                  <a href="news.php">
                                           <p class="titolobox">Edit News</p>

                    <span class="glyphicon glyphiconhome glyphicon-link"></span>
                </a>
            </div>
        </div>
        <div class="box  col-sm-2 " >
             <div class="box2 edit col-sm-12 " >
                  <a href="gals.php">
                                           <p class="titolobox">Edit Gallery</p>

                    <span class="glyphicon glyphiconhome glyphicon-th-large"></span>
                </a>
            </div>
        </div>
        <div class="box  col-sm-2 " >
            <div class="box2 edit col-sm-12 " >
                 <a href="pubbs.php">
                                           <p class="titolobox">Edit Pubbs</p>
                    <span class="glyphicon glyphiconhome glyphicon-list-alt"></span>
                </a>
            </div>
        </div>
        <div class="box  col-sm-2 " >
           <div class="box2 edit col-sm-12 " >
                    <a href="clients.php">
                                           <p class="titolobox">Edit Client</p>

              

                    <span class="glyphicon glyphiconhome glyphicon-eye-open"></span>
                </a>
            </div>
        </div>
           <div class="box  col-sm-2 " >
            <div class="box2 edit col-sm-12 " >
                  <a href="home.php">
                                           <p class="titolobox">Edit Home</p>

                    <span class="glyphicon glyphiconhome glyphicon-home"></span>
                </a>
            </div>
        </div>
      
      
        
    </div>
     <div class="row">
        <div class="text-left">
            
        </div>
    </div>
     <div class="row" >
        <div class="box  col-sm-2" >
            <div class="box2 admin col-sm-12 " >
                  <a href="contacts.php">
                                           <p class="titolobox">Contatti</p>

                    <span class="glyphicon glyphiconhome glyphicon-tasks"></span>
                </a>
            </div>
        </div>
        <div class="box  col-sm-2 " >
             <div class="box2 admin col-sm-12 " >
                  <a href="editbio.php">
                                           <p class="titolobox">Bio</p>

                    <span class="glyphicon glyphiconhome glyphicon-leaf"></span>
                </a>
            </div>
        </div>
        <div class="box  col-sm-2 " >
            <div class="box2 admin col-sm-12 " >
                  <a href="services.php">                     <p class="titolobox">Servizi</p>

                    <span class="glyphicon glyphiconhome glyphicon-info-sign"></span>
                </a>
            </div>
        </div>
       
      

    </div>
</div>



