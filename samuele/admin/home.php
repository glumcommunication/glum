<?php
session_start();
// verifico che esista la sessione di autenticazione
if (empty($_SESSION['userid'])) {
    header("location:login/main_login.php");
    exit;
}

include "header.php";
include "nav.php";
?>
<div class="container" style="margin-bottom:200px;">

    <div class="row text-center" >
        <p class="titolopagina">Elenco Immagini in Home -                 <a class="btn  btn-default" href="ordinahome.php">Ordina foto</a>
        </p> 
    </div>
    <div class="row">
        <?php include "ytmenu.php"; ?>
        <div class="col-sm-8 col-sm-offset-0">


            <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
                <thead>
                    <tr>
                        <th  class="text-center">Preview</th>
                        <th  class="text-center">Valore</th>
                        <th  class="text-center">Stato</th>
                        <th  class="text-center">Link </th>
                        <th  class="text-center">Azioni </th>
                        <th  class="text-center">Modifica </th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    $recnews = mysql_query("SELECT * FROM home");
                    while ($news = mysql_fetch_array($recnews)) {
                        $id = $news['id'];
                        $path = $news['path'];
                        $url=$news['link'];
                        $value = $news['value'];
                        $active = $news['active'];
                        if ($active == 'on') {
                            $totale = $totale + $value;
                        }
                        ?>
                        <tr>
                            <td class="text-center"><img src="../<?php echo $path; ?>" width="60px"></td>
                            <td  class="text-center"><?php echo $value; ?></td>
                            <td class="text-center"><?php
                                if ($active == 'on') {
                                    echo"<span class='glyphicon glyphicon-ok'></span>";
                                } else {
                                    echo"<span class='glyphicon glyphicon-lock'></span>";
                                }
                                ?></td>
<td  class="text-center"><?php echo $url;?></td>
                            <td  class="text-center"><a href="edithome.php?id=<?php echo $id; ?>"><span class="glyphicon glyphicon-random"></span></a></td>
                            <td  class="text-center"><a href="editlinkhome.php?id=<?php echo $id; ?>"><span class="glyphicon glyphicon-pencil"></span></a><a onClick="return confirm('Sicuro di cancellare questa foto?')" href="delhome.php?id=<?php echo $id; ?>"><span class="glyphicon glyphicon-trash"></span></a></td>

                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
            if ($totale != 8) {
                echo "<p>Attenzione! </p>";
            };
            ?>
            <br/><br/>
            <form method='post' enctype="multipart/form-data" action='caricahome.php' name='home' id='home'>
                <div class=" col-sm-4 form-group">
                    <label for="exampleInputFile">Immagine da UNA cella</label>
                    <input type="file" name="file1" id="file1">
                    <p class="help-block">Carica una foto piccola</p>
                </div>
                <div class=" col-sm-4 form-group">
                    <label for="exampleInputFile">Immagine da QUATTRO celle</label>
                    <input type="file" name="file2" id="file2">
                    <p class="help-block">Carica una foto media</p>
                </div>
                <div class="col-sm-4 form-group">
                    <label for="exampleInputFile">Immagine da OTTO cella</label>
                    <input type="file" name="file3" id="file3">
                    <p class="help-block">Carica una foto grande</p>
                </div>
                <input class="btn btn-default" type="submit" value="Submit">

            </form>

        </div>



    </div>

</div>


<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').dataTable({
            "bProcessing": true,
            "oLanguage": {
                "sLengthMenu": "Mostra _MENU_ oggetti",
                "sSearch": "Cerca:",
                "sInfo": "Da _START_ a _END_ di _TOTAL_ oggetti",
                "sZeroRecords": "Nessun oggetto trovato",
                "sEmptyTable": "Nessun oggetto trovato",
                "oPaginate": {
                    "sFirst": "Prima",
                    "sLast": "Ultima",
                    "sNext": "Successiva",
                    "sPrevious": "Precedente"
                }
            },
            "bJQueryUI": true,
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": true,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false,
            "sPaginationType": "full_numbers"
        });
    });
</script>



<script src="js/ytmenu.js"></script>
<?php
include "f\ooter.php";
?>
