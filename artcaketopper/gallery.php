<?php
$pagetitle = "Gallery";
$pagename = "gallery";
$pagedesc = "Le immagini delle creazioni di Art & Cake Topper, la galleria viene aggiornata con i lavori più recenti.";
include_once 'header.php';
include_once 'navigation.php';
?>


  <script type="text/javascript">
    $(window).load(function () {
        $(document).ready(function(){
            collage();
        });
    });
    
    function collage() {
        $('.Collage').removeWhitespace().collagePlus(
            {
                'fadeSpeed' : 2000,
                'targetHeight' : 200
            }
        ).collageCaption();
    };
     
    var resizeTimer = null;
    $(window).bind('resize', function() {
        // hide all the images until we resize them
        $('.Collage .Image_Wrapper').css("opacity", 0);
        // set a timer to re-apply the plugin
        if (resizeTimer) clearTimeout(resizeTimer);
        resizeTimer = setTimeout(collage, 200);
    });

  </script>  
<div class="container">
    <div class="row-fluid lastrow">
        <div class="span12 boxsh cake">
            <div class="container ribbon">
                <h2>Gallery</h2>
            </div>
<div class="Collage">
<?php
for ($i=1;$i<=9;$i++ ){
    echo '<div class="Image_Wrapper"><a href="images/gallery/0'.$i.'_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/0'.$i.'_actgal.jpg"></a></div>';
}
for ($i=10;$i<=84;$i++ ){
    echo '<div class="Image_Wrapper"><a href="images/gallery/'.$i.'_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/'.$i.'_actgal.jpg"></a></div>';
}
?>
    <!--<div class="Image_Wrapper"><a href="images/gallery/01_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/01_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/02_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/02_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/03_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/03_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/04_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/04_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/05_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/05_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/06_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/06_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/07_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/07_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/08_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/08_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/09_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/09_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/10_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/10_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/11_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/11_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/12_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/12_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/13_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/13_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/14_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/14_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/15_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/15_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/16_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/16_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/17_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/17_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/18_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/18_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/19_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/19_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/20_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/20_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/21_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/21_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/22_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/22_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/23_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/23_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/24_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/24_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/25_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/25_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/26_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/26_actgal.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/27_actgal.jpg" rel="lightbox[act]" title=""><img src="images/gallery/27_actgal.jpg"></a></div>-->
</div>
            
            
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>