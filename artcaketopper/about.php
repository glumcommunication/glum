<?php
$pagetitle = "Chi sono";
$pagename = "about";
$pagedesc = "Art & Cake Topper, il laboratorio di cake design di Simona Tamanti. Qui troverai tutte le informazioni sui corsi, sui miei lavori e le immagini delle mie creazioni.";
include_once 'header.php';
include_once 'navigation.php';
?>
<div class="container">
    <div class="row-fluid lastrow">
        <div class="span12 boxsh cake">
            <div class="container ribbon">
                <h2>Chi sono</h2>
            </div>
            
            <p class="justify">
                Mi chiamo Simona Tamanti e sono una maestra d’arte. È stato mio padre, artista anche lui, a trasmettermi 
                questa passione. Ho studiato presso l’Istituto Superiore per la Conservazione ed il Restauro di Roma dove 
                ho conseguito la mia specializzazione.  
            </p>
            <p class="justify">
                Ho girato l’Italia esercitando il mio mestiere. Prima a Torino, dove mi sono occupata della direzione tecnica 
                del restauro della facciata del Duomo, poi in Veneto, dove tra i tanti lavori ho restaurato un affresco di 
                Paolo Veronese. Poi mi sono trasferita a Siena.
            </p>
            <p class="justify">            
                Ho dedicato gran parte della mia vita professionale all’arte e alla conservazione dei beni culturali, unendo 
                alle mie esperienze, la mia passione e creatività.<br>
                <img src="images/5a.jpg" alt="Paesaggio" class="span5" style="float:right;">
                Ma le vie dell’arte sono infinite e piene di sorprese, così un giorno è scattata la scintilla con il mondo del Cake Design. 
                È stato amore a prima vista. Ho capito che potevo utilizzare le mie conoscenze artistiche applicandole alla decorazione 
                delle torte e trasmetterle agli altri, tramite dei corsi. Non sono una pasticcera, quello che faccio è insegnare a decorare, 
                a manipolare la pasta di zucchero, per dare la possibilità a tutti di ricreare i propri lavori a casa.<br>
                La mia maggiore predisposizione è il modelling, in particolare la riproduzione di figurine cartoon. Adoro creare fiori di 
                zucchero dall’aspetto realistico che cerco di abbinare armoniosamente con il tema che mi viene richiesto.

            </p>
            <p class="justify">
                L’amore per le arti creative, i colori e le forme pulite mi hanno portata in questi anni ad approfondire le mie conoscenze 
                facendo master con Cake Designer di prestigio internazionale. Il resto l’ha compiuto la straordinaria ricchezza della campagna 
                senese dove vivo con i miei quattro figli e da cui trovo l’ispirazione necessaria per produrre ogni giorno nuove idee artistiche. 
                Perché è vero che un dolce si gusta anche con gli occhi.
            </p>
            <!--<div class="container ribbon">
                <h2>I corsi</h2>
            </div>-->
            <p class="justify">

                I miei corsi sono aperti a tutti coloro che amano avvicinarsi al mondo del Cake Design. Sono divisi per tema e per grado di 
                difficoltà. Potrai imparare a realizzare colorati bouquet floreali in pasta di zucchero, simpatici cuccioli su Cup Cake, 
                dare un tocco creativo alla tua torta per festeggiare con dolcezza i tuoi momenti indimenticabili.<br>
                Per maggiori informazioni e per restare sempre aggiornato consulta la pagina delle <a href="news.php" title="News"><strong>News</strong></a>.
            </p>
            <!--<p class="justify">
                Scegli il corso che fa per te:
            </p>
            <ul>
                <li>Wedding Cake</li>
                <li>La Rosa ed il suo Bocciolo</li>
                <li>La Peonia</li>
                <li>Flowers Cup Cake</li>
                <li>Baby Cup Cake</li>
                <li>Cup Cake Cuccioli 3D</li>
                <li>Cup Cake Comunione</li>
                <li>Topper Compleanno a Tema</li>
                <li>Topper Romantic Cake</li>
            </ul>
            <p class="justify">  
                ...e se volete la torta la decoriamo insieme con la vostra Cake Designer a domicilio...
            </p>-->


        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>