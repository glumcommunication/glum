<!DOCTYPE html>
<html>
    <head>
  
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo $pagedesc ?>">
        <meta name="KEYWORDS" content="Cake design, Siena, Simona Tamanti, arte, fantasia, dolci, dolce, pasta, zucchero, torte di compleanno, pasticciere, decorazioni, decorazione, torte, torta, torte nuziali, corsi, designer, wedding, cucina, sugar art, scuola cucina, disegni, decoro, spongecake, restauro, feste, festa, corso base">
        <link rel="icon" href="images/lilcake.png" type="image/svg" />
        <meta property="og:image" content="http://www.artcaketopper.com/images/logo.png" />
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,900,700,600,800,500' rel='stylesheet' type='text/css'>

<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.1/jquery.min.js"></script>    

<?php
if ($pagename == "gallery") {
    echo <<<_END
    
    
  <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.1/jquery.min.js"></script>    -->
    <script src="js/jquery-1.7.2.min.js"></script>
  <script src="js/jquery.collagePlus.min.js"></script>
  <script src="js/jquery.removeWhitespace.min.js"></script>
  <script src="js/jquery.collageCaption.min.js"></script>
  <script src="js/lightbox.js"></script>  
_END;
}
if ($pagename == "contatti") {
    echo <<<_END
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    function initialize() {
        var latlng = new google.maps.LatLng(43.275612, 11.327248);
        var settings = {
            zoom: 15,
            center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
            navigationControl: true,
            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
            mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"), settings);

  var companyLogo = new google.maps.MarkerImage('images/positionshadow.png',
    new google.maps.Size(86,58),
    new google.maps.Point(0,0),
    new google.maps.Point(20,50)
);

var companyPos = new google.maps.LatLng(43.275612, 11.327248);
var companyMarker = new google.maps.Marker({
    position: companyPos,
    map: map,
    icon: companyLogo,
    //shadow: companyShadow,
    title:"Example SpA"
});
var contentString = '<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h1 id="firstHeading" class="firstHeading">Art & Cake Topper</h1>'+
    '<div id="bodyContent">'+
    '<p>Strada di Monsindoli 13, 53100 Siena<br>Telefono: +39 3388365543<br>Email: <a href="mailto:info@artcaketopper.com">info@artcaketopper.com</a><br>Website: <a href="http://www.artcaketopper.com" title="Art & Cake Topper">www.artcaketopper.com</a><br></p>'+
    '</div>'+
    '</div>';
 
var infowindow = new google.maps.InfoWindow({
    content: contentString
});
google.maps.event.addListener(companyMarker, 'click', function() {
  infowindow.open(map,companyMarker);
});
    }
    </script>
_END;
}

?>        
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet" media="screen">
        <link href="css/lightbox.css" rel="stylesheet" media="screen">
        <title>
            <?php echo $pagetitle; ?> | Cake design Siena ~ Art & Cake Topper - Il dolce mondo di Simona Tamanti
        </title>
    </head>
<body<? if ($pagename == "contatti") echo " onload=\"initialize()\"";?>>
<?php include_once("analyticstracking.php") ?>
<div class="wrap">    