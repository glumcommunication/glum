<?php
$pagetitle = "Contatti";
$pagename = "contatti";
$pagedesc = "Indirizzo, email e numero di telefono di Art & Cake Topper: contattami per informazioni sui corsi o per curiosità sul mondo del Cake Design.";
include_once 'header.php';
include_once 'navigation.php';
?>
<div class="container">
    <div class="row-fluid lastrow">
        <div class="span12 boxsh cake">
            <div class="container ribbon">
                <h2>Contatti</h2>                
            </div>

            <div id="map_canvas" style="width:100%; height:400px;">
            </div>
            <div class="row-fluid lastrow">
                <div class="span12 ribbon">
                    <h2>Art & Cake Topper di Simona Tamanti</h2><br>
                    <p style="clear:both;">
                        Strada di Monsindoli, 13 - 53100 Siena
                    </p>
                    <p>
                        Telefono: +39 338 8365543
                    </p>
                    <p>
                        Email: <a href="mailto:info@artcaketopper.com" title="Contattami">info@artcaketopper.com</a>
                    </p>
                </div>
        </div>
    </div>
</div>
</div>
<?php
include_once 'footer.php';
?>