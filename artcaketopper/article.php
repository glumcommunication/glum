<?php
$slug = $_GET["slug"];
include_once 'cbd.php';
$sql = "SELECT * FROM news WHERE slug='$slug'";
$result = mysql_query($sql);
if (!$result) die ("Database access failed: " . mysql_error());
$row = mysql_fetch_row($result);
$datetime = strtotime($row[3]);
$mysqldate = date("d/m/Y", $datetime);
$pagetitle = $row[0];
$pagename = "news";

$pagedesc = $row[0];
$pageurl = "news.php?slug=".$row[4];
include_once 'header.php';
include_once 'navigation.php';
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="container">
    <div class="row-fluid lastrow">
        <div class="span12 boxsh">
            <div class="container ribbon">
                <h2>News</h2>
            </div>
<?php
echo <<<_END
                    <div class="row-fluid">
                        <h3 class="newstitle">
                                $row[0]
                        </h3>
                        <div class="newsdetail">
                            Pubblicata il $mysqldate, $row[3] (<a href="article.php?slug=$row[4]">Link</a>)
                        </div>        


                        <div class="newsbody">$row[1]</div>
        
<div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-action="recommend"></div>
        <a href="https://twitter.com/share" class="twitter-share-button" data-lang="it" data-hashtags="artcaketopper">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        <div class="g-plusone" data-size="medium"></div>
        

                    </div>
_END;
?>
        </div>
    </div>
</div>
<script type="text/javascript">
  window.___gcfg = {lang: 'it'};

  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
<?php
include_once 'footer.php';
?>