    <div class="push"></div>
</div>
<div class="footer">
  <div class="container">
    <p class="credit">Art & Cake Topper di Simona Tamanti | P. Iva: 01357350526 | Strada di Monsindoli, 12 - 53100 Siena | +39 338 8365543 | 
        <a href="mailto:info@artcaketopper.com">info@artcaketopper.com</a></p>
        <p style="font-size:12px;" class="credit">Crafted by <a href="http://www.glumcommunication.it/" target="_BLANK">GLuM Communication</a></p>

  </div>
</div>


<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<?php
if ($pagename == 'home') {
    echo <<<_END
<script>
    $('.carousel').carousel({
        interval: 3000
    })
</script>


<script>
$(document).ready(function() {

	// validate signup form on keyup and submit
	var validator = $("#form").validate({
		rules: {
			name: "required",
			phone: {
                required: true,
                minlength: 9
            },
            message: "required",
			email: {
				required: true,
                minlength: 7,
                email: true
			}
		},
		messages: {
			name: "Inserisci nome e cognome",
			phone: {
				required: "Inserisci un numero di telefono",
				minlength: "Il numero deve essere composto da almeno 9 cifre"
			},
            message: "Inserisci un messaggio",
			email: {
                required: "Inserisci un indirizzo email",
                minlength: "Inserisci un indirizzo email valido",
                email: "Inserisci un indirizzo email valido"
            }
			
		},
	});


});
</script>
_END;
}
?>
<script>
  $(function() {
    $( "#accordion" ).accordion({
      collapsible: true,
      active: false,
      heightStyle: "content"
    });
  });
  </script>
        <script type="text/javascript">
          $(document).ready(function () {
            $("a").tooltip({
              'selector': ''
            });
          });
        </script>

  
</body>
</html>
