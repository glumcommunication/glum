<?php
include_once 'db.php';

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<link href="images/favicon.ico" rel="icon" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link href="css/flexslider.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Italianno' rel='stylesheet' type='text/css'>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>

<script src="js/loadNewPage.js"></script>
<script type="text/javascript">


$("document").ready(function() {
				$('a.centro').click(function(){
					$('html, body').animate(
					{scrollTop: $("#pageCentro").offset().top-146}, 1000, "easeInOutCirc");				   
				 });
				 $('a.servizi').click(function(){
					$('html, body').animate({
						scrollTop: $("#pageServizi").offset().top-146}, 1000, "easeInOutCirc");	
				 });
				 $('a.equipe').click(function(){

					$('html, body').animate({
						scrollTop: $("#pageEquipe").offset().top-146}, 1000, "easeInOutCirc");	
				 });
				 $('a.news').click(function(){
					$('html, body').animate({
						scrollTop: $("#pageNews").offset().top-146}, 1000, "easeInOutCirc");	
				 });
				 $('a.contatti').click(function(){
					$('html, body').animate({
						scrollTop: $("#pageContatti").offset().top-146}, 1000, "easeInOutCirc");	
				 });				 				 
});

</script>

<script src="js/jquery.flexslider.js"></script>

<script>
$(window).load(function() {
  // The slider being synced must be initialized first
  $('#carousel').flexslider({
	 useCSS: true,  	
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 120,
    itemMargin: 5,
    asNavFor: '#slider1'
  });
   
  $('#slider1').flexslider({
	 useCSS: true,  	  	
	 animation: "fade",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel"
  });
});
</script>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<title>Centro Dedalo</title>
</head>
<body>
<div id="wrapper">

	<div id="footer">
		<div id="info">
			<p>Centro Dedalo, Strada Massetana Romana 64, 53100 Siena. 
			Contatti - Telefono: +39 0577 236516 | Email: <a href="mailto:dedalocentro@gmail.com" style="color:white;">dedalocentro@gmail.com</a></p>
		</div>
		<div id="social">
				<a href="http://www.facebook.com/centro.dedalo.1?fref=ts" title="Facebook"><img src="images/facebook.png" alt="Facebook" /></a>
				<a href="http://centrodedalo.blogspot.it/" title="Blog"><img src="images/blogger.png" alt="Blog" /></a>
				<a href="mailto:dedalocentro@gmail.com"><img src="images/mail.png" alt="Email" /></a>										
		</div>	
	</div>
	<div id="header">
		<div id="logo">
				<h1>Centro Dedalo</h1>
		</div>
<!--		<h1>Centro Dedalo</h1>-->
		<div id="menu">
			<ul>
				<li><a href="home.php#anchorCentro" class="goto">Il Centro</a></li>
				<li><a href="home.php#anchorServizi" class="servizi"  onclick="loadNewServ('defaultservices.php'); return false;">Servizi</a></li>
				<li><a href="home.php#anchorEquipe" class="equipe" onclick="loadNewPage('defaultequipe.php'); return false;">Equipe</a></li>
				<li><a href="home.php#anchorNews" class="news">News</a></li>
				<li><a href="home.php#anchorContatti" class="contatti">Contatti</a></li>			
			</ul>
		</div>		
	</div>
	<div id="main">
		<div id="pageContatti">
			<div class="whitebox" style="width:50%; padding:20px; margin:30px 25%;">
			<p>Hai richiesto l'iscrizione alla Newsletter di Centro Dedalo, riceverai una email all'indirizzo che hai fornito con il link di attivazione.</p>
			<p>Usa il menu per tornare a navigare nel nostro sito e grazie per aver iniziato a seguirci.</p>
			</div>
		</div>
		
						
	</div>
		
		
</div>
</body>
</html>