<?php include('include/connection.php'); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Agenda Centro Dedalo</title>
        <?php include('include/meta.php'); ?>
    </head>
    <body>
        
        <div id="menu">
            <?php
                $page = 'addpaziente';
                include('include/menu.php');
            ?>
        </div>
        <div id="content">
            <h3 class="fg c lblok">Paziente inserito correttamente.</h3>
            <br /><br /><br />
            <p class="c fg">Cosa vuoi fare?</p>
            <br /><br />
            <table>
                <tr>
                    <td>
                        <div class="btn" onclick="location.href='addpaziente.php'">
                            <span class="icon-user-add fg" ></span>
                            <span class="vocebtn">Aggiungi nuovo paziente</span>
                        </div>
                    </td>
                    <td>&nbsp;</td>
                     <td>
                        <div class="btn" onclick="location.href='paziente.php?i=<?=($_GET["i"])?>'">
                            <span class="icon-vcard fg" ></span>
                            <span class="vocebtn">Apri scheda paziente</span>
                        </div>
                    </td>
                    <td>&nbsp;</td>
                     <td>
                        <div class="btn" onclick="location.href='home.php'">
                            <span class="icon-calendar fg" ></span>
                            <span class="vocebtn">Torna in agenda</span>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <br /><br />
        <div id="footer"><?php include('include/footer.php'); ?></div>

    </body>
</html>
<?php mysql_close(); ?>