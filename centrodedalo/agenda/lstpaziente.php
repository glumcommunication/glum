<?php include('include/connection.php'); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Agenda Centro Dedalo</title>
        <?php include('include/meta.php'); ?>
        
        <script type="text/javascript">
            function caricalista() {
                $.post('ajax/lstpz.php', {p : $('#nominativo').val()}, function(r) {
                    $('#listapazienti').html(r);
                }); 
            }
            
            function cancellapaziente(v) {
                if (confirm("Vuoi davvero eliminare questo paziente?")) {
                    $.post('ajax/dltpz.php', {p : v}, function() {
                    caricalista();
                });
                }
            }
            
        </script>
    </head>
    <body onload="caricalista();">
        
        <div id="menu">
            <?php
                $page = 'lstpazienti';
                include('include/menu.php');
            ?>
        </div>
        <div id="content">
            <table style="width:100%;"> 
                <tr>
                    <td>
                        <div class="btn <?=($page == 'addpaziente' ? 'btnse' : '' )?> " onclick="location.href='addpaziente.php'">
                            <span class="icon-user-add fm" ></span>
                            <span class="vocebtn fp">Aggiungi paziente</span>
                        </div>
                    </td>
                    <td class="r"><span class="icon-search fg" ></span> Cerca paziente <input type="text" class="textbox" id="nominativo" onkeyup="caricalista()"  /></td>
                </tr>
            </table>
            <br /><br />
            <div id="listapazienti">
            
            </div>
        </div>
        <br /><br />
        <div id="footer"><?php include('include/footer.php'); ?></div>

    </body>
</html>
<?php mysql_close(); ?>