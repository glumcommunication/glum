<?php include('include/connection.php'); ?>
<?php
    if (isset($_GET["op"]) && $_GET["op"] == "uan") {
       mysql_query("UPDATE pazienti SET cognome = '".replace($_POST["cognome"])."', nome= '".replace($_POST["nome"])."', datadinascita = '".replace($_POST["nascita"])."', sesso = '".replace($_POST["sesso"])."', comunedinascita = '".replace($_POST["comune"])."', codicefiscale = '".replace($_POST["codfiscale"])."', indirizzo = '".replace($_POST["indirizzo"])."', citta = '".replace($_POST["citta"])."', cap = '".replace($_POST["cap"])."', provincia = '".replace($_POST["provincia"])."', telefono1 = '".replace($_POST["tel1"])."', telefono2 = '".replace($_POST["tel2"])."', cellulare = '".replace($_POST["cellulare"])."', mail = '".replace($_POST["mail"])."' WHERE id = ".$_GET["i"]);         
    }
    if (isset($_GET["op"]) && $_GET["op"] == "dpz") {
        mysql_query("DELETE FROM pazienti WHERE id = ".$_GET["i"]);
        header('Location:lstpaziente.php');
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Agenda Centro Dedalo</title>
        <?php include('include/meta.php'); ?>
        <script type="text/javascript" src="js/tools.js" ></script>
        <link href="css/csstab.css" rel="stylesheet" />
        <script type="text/javascript">
            $(document).ready(function(){
                $("ul.tabs").tabs("div.panes > div", {initialIndex : <?=(isset($_GET["t"]) ? $_GET["t"] : 0 )?>});
            })
            
            function conferma() {
                if (confirm("Vuoi davvero eliminare questo paziente?")) {
                    location.href='paziente.php?op=dpz&i=<?=($_GET["i"])?>';
                }
            }
            
            function salvavalutazione() {
                $('#loadvalutazione').fadeIn();
                $('#loadvalutazione').fadeOut();
            }
        </script>
    </head>
    <body>
        <div id="menu">
            <?php
                $page = 'nessuna';
                include('include/menu.php');
            ?>
        </div>
        <div id="content">
            <ul class="tabs">
                <li><a href="anagrafica">Anagrafica</a></li>
                <li><a href="#">Appuntamenti</a></li>
                <li><a href="contabilita">Contabilità</a></li>
                <li><a href="#">Scheda valutazione</a></li>
                <li><a href="#">Tipo di terapia</a></li>
                <li><a href="#">Note</a></li>
                <li><a href="#">Comunicazioni</a></li>
            </ul>
            <div class="panes">
                <div class="childpanes">
                    <!-- ANAGRAFICA -->
                    <form action="paziente.php?i=<?=($_GET["i"])?>&op=uan#uan" method="post" id="addpaziente" name="addpaziente" >
                    <table border="0">
                        <td rowspan="17" class="t" style="width:250px;">
                            <img src="images/nofoto.png" alt="Nessuna foto profilo" title="Nessuna foto profilo" />
                        </td>
                        <?php
                            $anagrafica_query = mysql_query("SELECT * FROM pazienti WHERE id = ".$_GET["i"]);
                            $anagrafica = mysql_fetch_array($anagrafica_query, MYSQL_ASSOC);
                         ?>
                        <tr><td class="r">Cognome</td><td><input type="text" name="cognome" id="cognome" value="<?=($anagrafica["cognome"])?>" required class="textbox" /> </td></tr>
                        <tr><td class="r">Nome</td><td><input type="text" name="nome" id="nome" value="<?=($anagrafica["nome"])?>" required class="textbox" /></td></tr>
                        <tr><td class="r">Data di nascita</td><td><input type="text" name="nascita" value="<?=($anagrafica["datadinascita"])?>" id="nascita" required  class="textbox" maxlength="10"/> (dd/mm/aaaa)</td></tr>
                        <tr><td class="r">Sesso</td><td><input type="radio" value="M" name="sesso" <?=($anagrafica["sesso"] == "M" ? 'checked="checked"' : '') ?> />M &nbsp;&nbsp;<input type="radio" value="F" name="sesso" <?=($anagrafica["sesso"] == "F" ? 'checked="checked"' : '') ?>  />F</td></tr>
                        <tr><td class="r">Comune di nascita</td><td><input type="text" name="comune" value="<?=($anagrafica["comunedinascita"])?>" id="comune" required class="textbox" /></td></tr>
                        <tr><td class="r">Codice fiscale</td><td><input type="text" name="codfiscale" id="codfiscale" value="<?=($anagrafica["codicefiscale"])?>" required class="textbox" style="text-transform: uppercase;" maxlength="16" /></td></tr>
                        <tr><td class="r">Indirizzo</td><td><input type="text" name="indirizzo" id="indirizzo" required class="textbox"  value="<?=($anagrafica["indirizzo"])?>" style="width:350px;" /></td></tr>
                        <tr><td class="r">Comune</td><td><input type="text" name="citta" id="citta" required class="textbox" value="<?=($anagrafica["citta"])?>" /></td></tr>
                        <tr><td class="r">Provincia</td><td><input type="text" name="provincia" id="provincia" value="<?=($anagrafica["provincia"])?>" required class="textbox" style="width:40px;" maxlength="2" value="SI" /></td></tr>
                        <tr><td class="r">C.A.P.</td><td><input type="text" name="cap" id="cap" required  class="textbox" value="<?=($anagrafica["cap"])?>" maxlength="5" style="width:70px;" /></td></tr>
                        <tr><td class="r">Telefono #1</td><td><input type="number" name="tel1" id="tel1" required class="textbox" value="<?=($anagrafica["telefono1"])?>"  style="letter-spacing:2px;" /></td></tr>
                        <tr><td class="r">Telefono #2</td><td><input type="number" name="tel2" id="tel2" class="textbox" value="<?=($anagrafica["telefono2"])?>" style="letter-spacing:2px;" /></td></tr>
                        <tr><td class="r">Cellulare</td><td><input type="number" name="cellulare" id="cellulare" class="textbox" value="<?=($anagrafica["cellulare"])?>" style="letter-spacing:2px;" /></td></tr>
                        <tr><td class="r">E-mail</td><td><input type="email" name="mail" id="mail" class="textbox" style="width:350px;" value="<?=($anagrafica["mail"])?>" /></td></tr>
                        <tr><td class="r">Convenzione</td><td><select name="convenzione" id="convenzione" class="textbox" ><option value="-1" >Nessuna convenzione</option></select></td></tr>
                        <tr><td>&nbsp;</td><td><br />
                            <button type="submit" value="Salva" class="icon-disk btn fg" >                           
                             <span class="vocebtn">SALVA </span>                            
                            </button>
                            &nbsp;&nbsp;
                            <button type="button" value="Salva" class="icon-cross btn fg" onclick="conferma()" >                           
                             <span class="vocebtn">ELIMINA PAZIENTE </span>                            
                            </button>
                            <a name="uan" />
                        </td></tr>
                    </table>
                    </form>
                </div>
                <div class="childpanes">
                    <!-- APPUNTAMENTI -->
                    <p class="r"><i>Sedute da <b>45 minuti</b></i></p>
                    <p><b>Appuntamenti presi</b></p>
                    <table style="width:70%;" border="0">
                        <tr>
                            <td>27/01/2014</td>
                            <td>ore 16:00</td>
                            <td>Dott.ssa Campanella</td>
                            <td style="width:40px;">
                                <div class="btn" >
                                    <span class="icon-calendar fg" ></span>
                                </div>
                            </td>
                            <td style="width:40px;">
                                <div class="btn" >
                                    <span class="icon-cross fg" ></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>03/02/2014</td>
                            <td>ore 16:00</td>
                            <td>Dott.ssa Campanella</td>
                            <td>
                                <div class="btn" >
                                    <span class="icon-calendar fg" ></span>
                                </div>
                            </td>
                            <td>
                                <div class="btn" >
                                    <span class="icon-cross fg" ></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>27/01/2014</td>
                            <td>ore 16:00</td>
                            <td>Dott.ssa Campanella</td>
                            <td>
                                <div class="btn" >
                                    <span class="icon-calendar fg" ></span>
                                </div>
                            </td>
                            <td>
                                <div class="btn" >
                                    <span class="icon-cross fg" ></span>
                                </div>
                            </td>
                        </tr>
                         <tr>
                            <td>10/02/2014</td>
                            <td>ore 16:00</td>
                            <td>Dott.ssa Campanella</td>
                            <td>
                                <div class="btn" >
                                    <span class="icon-calendar fg" ></span>
                                </div>
                            </td>
                            <td>
                                <div class="btn" >
                                    <span class="icon-cross fg" ></span>
                                </div>
                            </td>
                        </tr>
                         <tr>
                            <td>17/01/2014</td>
                            <td>ore 16:00</td>
                            <td>Dott.ssa Campanella</td>
                            <td>
                                <div class="btn" >
                                    <span class="icon-calendar fg" ></span>
                                </div>
                            </td>
                            <td>
                                <div class="btn" >
                                    <span class="icon-cross fg" ></span>
                                </div>
                            </td>
                        </tr>
                    </table>
                                        
                    <br />
                    <p><b>Appuntamenti passati</b></p>
                     <table style="width:70%;" border="0">
                        <tr>
                            <td>20/12/2013</td>
                            <td>ore 16:00</td>
                            <td>Dott.ssa Campanella</td>
                            <td style="width:40px;">
                                <div class="btn" >
                                    <span class="icon-calendar fg" ></span>
                                </div>
                            </td>
                            <td style="width:40px;">
                                <div class="btn" >
                                    <span class="icon-cross fg" ></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>13/12/2013</td>
                            <td>ore 16:00</td>
                            <td>Dott.ssa Campanella</td>
                            <td>
                                <div class="btn" >
                                    <span class="icon-calendar fg" ></span>
                                </div>
                            </td>
                            <td>
                                <div class="btn" >
                                    <span class="icon-cross fg" ></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>02/12/2013</td>
                            <td>ore 16:00</td>
                            <td>Dott.ssa Campanella</td>
                            <td>
                                <div class="btn" >
                                    <span class="icon-calendar fg" ></span>
                                </div>
                            </td>
                            <td>
                                <div class="btn" >
                                    <span class="icon-cross fg" ></span>
                                </div>
                            </td>
                        </tr>
                         <tr>
                            <td>25/11/2013</td>
                            <td>ore 16:00</td>
                            <td>Dott.ssa Campanella</td>
                            <td>
                                <div class="btn" >
                                    <span class="icon-calendar fg" ></span>
                                </div>
                            </td>
                            <td>
                                <div class="btn" >
                                    <span class="icon-cross fg" ></span>
                                </div>
                            </td>
                        </tr>
                         <tr>
                            <td>20/11/2013</td>
                            <td>ore 16:00</td>
                            <td>Dott.ssa Campanella</td>
                            <td>
                                <div class="btn" >
                                    <span class="icon-calendar fg" ></span>
                                </div>
                            </td>
                            <td>
                                <div class="btn" >
                                    <span class="icon-cross fg" ></span>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <p class="r"><i>Sedute da <b>45 minuti</b></i></p>
                </div>
                <div class="childpanes" >
                    <!-- CONTABILITA -->
                    <p><b>Appuntamenti da saldare</b></p>
                         <table style="width:90%;" class="lh30" border="0" >
                            <tr>
                                <td style="width:270px;">
                                    Appuntamento del 22/01/2014
                                </td>
                                <td rowspan="3" class="c">
                                    <div class="btn" >
                                        <span class="icon-euro fg"></span>
                                        <span class="vocebtn">Crea nuova fattura</span>
                                    </div>
                                </td>
                               
                            </tr>
                            <tr>
                                <td>
                                    Appuntamento del 15/01/2014
                                </td>
                                
                            </tr>
                            <tr>
                                <td>
                                    Appuntamento del 08/01/2014
                                </td>
                                
                            </tr>
                        </table>
                    <br />
                    <p><b>Fatture emesse</b></p>
                    
                        <table style="width:90%" border="0">
                            <tr>
                                <td style="width:215px;">
                                    Fattura N. 552 del 20/12/2013
                                </td>
                                <td style="width:45px;">
                                    <div class="btn" ><span class="icon-printer fg" ></span></div>
                                </td>
                                <td rowspan="3">
                                    <p class="c">Costo di ogni sedura: <input type="number" class="textbox" style="width:40px;text-align:right;padding-right:5px;" value="25" /> Euro</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Fattura N. 470 del 24/11/2013
                                </td>
                                <td>
                                    <div class="btn" ><span class="icon-printer fg" ></span></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Fattura N. 340 del 27/10/2013
                                </td>
                                <td>
                                    <div class="btn" ><span class="icon-printer fg" ></span></div>
                                </td>
                            </tr>
                        </table>
                    
                </div>
                <div class="childpanes">
                    <!-- VALUTAZIONE -->
                    <p class="r fp lh25"><b><i>aggiornata al: 26/11/2013</i></b></p>
                    <textarea class="txtareavalutazione" >Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.
Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.
Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</textarea>
                    <br />
                    <p class="c"><button type="submit" value="Salva" class="icon-disk btn fg" onclick="salvavalutazione()" ><span class="vocebtn">SALVA </span></button>&nbsp;&nbsp;&nbsp; <img src="images/loading.gif" alt="loading" title="loading" id="loadvalutazione" style="display:none;" class="m"/></p>
                    
                </div>
                <div class="childpanes">
                    <!-- Tipo di terapia -->
                    <p class="r fp lh25"><b><i>aggiornata al: 16/10/2013</i></b></p>
                    <textarea class="txtareavalutazione" >Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.
Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.
</textarea>
                    <br />
                    <p class="c"><button type="submit" value="Salva" class="icon-disk btn fg" onclick="salvavalutazione()" ><span class="vocebtn">SALVA </span></button>&nbsp;&nbsp;&nbsp; <img src="images/loading.gif" alt="loading" title="loading" id="loadvalutazione" style="display:none;" class="m"/></p>
                    
                </div>
                <div class="childpanes">
                    <!-- NOTE-->
                    Qui tutto sulle note
                </div>
                <div class="childpanes">
                    <!-- COMUNICAZIONI -->
                     Qui possibilità di inviare e-mail o SMS
                </div>
            </div>
        </div>
        <br /><br />
        <div id="footer"><?php include('include/footer.php'); ?></div>

    </body>
</html>
<?php mysql_close(); ?>