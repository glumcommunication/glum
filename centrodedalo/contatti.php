
		<div id="anchorContatti"></div>
		
		<div id="pageContatti">
			<div class="lefty">
				<div class="hiddenbox">
					<p style="text-align:center;margin-top:150px;" class="italianno_title">Centro Dedalo</p>
					<p style="text-align:center;font-size:1.5em;">Via Massetana Romana, 64<br />53100 Siena (SI)</p>
					<p style="text-align:center;font-size:1.5em;">Telefono: +39 0577 236516<br />Email: <a href="mailto:dedalocentro@gmail.com">dedalocentro@gmail.com</a></p>				
					<p>&nbsp;</p>

					<div class="newsletter_form">
					<p style="text-align:center;margin-bottom:25px;" class="italianno_title">Iscriviti alla nostra Newsletters</p>					
					<div style="padding: 0 18%;">
<?php
include_once 'newsletter.php'; 
?>
<p style="font-size:10px;line-height:14px;margin-top:25px;">*INFORMATIVA per il trattamento dei dati personali. Ai sensi dell'art. 13 del D.Lgs. n. 196/2003 la raccolta dei suoi dati personali viene effettuata registrando i 
dati da lei stesso forniti, in qualità di interessato, al momento della richiesta di invio del suo messaggio. I dati personali sono trattati per le seguenti finalità: 
a) consentire l'invio del suo messaggio contenente i commenti e le opinioni che vorrà esprimere; b) realizzare indagini dirette a verificare il grado di soddisfazione 
degli utenti sui servizi offerti o richiesti. Per garantire l'efficienza del servizio, la informiamo inoltre che i dati potrebbero essere utilizzati per effettuare 
prove tecniche e di verifica.</p>
					</div>	
					</div>
				</div>

			</div>
			<div class="righty">
				<div class="whitebox" style="margin-top:180px;">

                    <div id="map_canvas" style="width:100%; min-height:500px;"></div>

				</div>
				<div class="titlerow">
					<h3>Contatti</h3>
				</div>					
			</div>
		</div>