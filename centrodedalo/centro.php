		<div id="anchorCentro"></div>
		<div id="pageCentro">
			<div class="lefty">
				<div class="hiddenbox">
				<p>
					Centro Dedalo è un centro specialistico multidisciplinare per il bambino, la persona, la famiglia. 
					Punto di forza è la rete di ben 16 professionisti del settore che lavorano su appuntamento.
				</p>
				<p>
					Il centro è il primo punto di riferimento di servizi integrati per la famiglia nel territorio senese.
				</p>
				<p>
					Oltre al sostegno neuropsichiatrico, psicologico, educativo ed a interventi nel settore dell’alimentazione, 
					del linguaggio e del versante psicomotorio risulta un importante riferimento nel settore dei Disturbi 
					dell’apprendimento.
				</p>
				<p>
					Il Centro vanta infatti una pluriennale esperienza nella diagnosi, riabilitazione e supporto extra 
					scolastico nell’ambito dell’apprendimento.
				</p>
				<p>
					Inoltre il Centro si avvale della collaborazione del Prof. Giacomo Stella psicologo clinico, 
					professore associato all’università di Modena Reggio Emilia.</p>
				</div>
			</div>
			<div class="righty">


				<div class="flexslider" id="slider1">
					<ul class="slides">
<?php
$db_server = mysql_connect($db_hostname, $db_username, $db_password);
if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());
mysql_select_db($db_database, $db_server) or die("Unable to select database: " . mysql_error());
$sql = "SELECT * FROM dedalo_hs";
$result = mysql_query($sql);
if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);
?>                        
						<li>
	  	    	    		<img src="/admin/modules/dedalohomeslider/uploads/<?php echo $row[1]; ?>" alt="Centro Dedalo - Il Centro" />
		  	    		</li>
<?php
}
?>
                        
		  	    	</ul>
		  	    </div>
				<div id="carousel" class="flexslider">
				  <ul class="slides">
<?php
$sql = "SELECT * FROM dedalo_hs";
$result = mysql_query($sql);
if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);
?>                        
						<li>
	  	    	    		<img src="/admin/modules/dedalohomeslider/uploads/<?php echo $row[1]; ?>" alt="Centro Dedalo - Il Centro" />
		  	    		</li>
<?php
}
?>
	  	    		
				  </ul>
				</div>
				</div>
				<div class="titlerow">
					<h3>Il Centro</h3>
				</div>				

		</div>