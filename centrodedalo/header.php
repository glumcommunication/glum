<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="description" content="Centro Dedalo &egrave; un centro specialistico multidisciplinare per il bambino, la persona, la famiglia. Il Centro offre sostegno neuropsichiatrico, psicologico, educativo ed interventi nel settore dell'alimentazione, del linguaggio e del versante psicomotorio e dell'apprendimento." />
<meta name="keywords" content="centro dedalo, diagnosi, consulenze, psicologo, neuropsichiatra, psicomotricista, dietista, educazione, bambini, infanzia, siena, toscana, specialisti" />
<link href="images/favicon.ico" rel="icon" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link href="css/flexslider.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Italianno' rel='stylesheet' type='text/css'>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>
<script src="js/resetPage.js"></script>
<script src="js/loadNewPage.js"></script>
<script type="text/javascript">


$("document").ready(function() {
				$('a.centro').click(function(){
					$('html, body').animate(
					{scrollTop: $("#pageCentro").offset().top-146}, 1000, "easeInOutCirc");				   
				 });
				 $('a.servizi').click(function(){
					$('html, body').animate({
						scrollTop: $("#pageServizi").offset().top-146}, 1000, "easeInOutCirc");	
				 });
				 $('a.equipe').click(function(){

					$('html, body').animate({
						scrollTop: $("#pageEquipe").offset().top-146}, 1000, "easeInOutCirc");	
				 });
				 $('a.news').click(function(){
					$('html, body').animate({
						scrollTop: $("#pageNews").offset().top-146}, 1000, "easeInOutCirc");	
				 });
				 $('a.contatti').click(function(){
					$('html, body').animate({
						scrollTop: $("#pageContatti").offset().top-146}, 1000, "easeInOutCirc");	
				 });				 				 
});

</script>

<script src="js/jquery.flexslider-min.js"></script>

<script>
$(window).load(function() {
  // The slider being synced must be initialized first
  $('#carousel').flexslider({
	 useCSS: true,  	
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 120,
    itemMargin: 5,
    asNavFor: '#slider1'
  });
   
  $('#slider1').flexslider({
	 useCSS: true,  	  	
	 animation: "fade",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel"
  });
});
</script>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<title>Centro Dedalo</title>
<?php
include_once 'dbc.php';
include_once 'map.php';
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48719076-1', 'centrodedalo.org');
  ga('send', 'pageview');

</script>
</head>
<body  onload="initialize()">
<div id="wrapper">

	<div id="footer">
		<div id="info">
			<p>Centro Dedalo, Strada Massetana Romana 64, 53100 Siena. 
			Contatti - Telefono: +39 0577 236516 | Email: <a href="mailto:dedalocentro@gmail.com" style="color:white;">dedalocentro@gmail.com</a> | 
Partita Iva: 01086520523</p>
		</div>
		<div id="social">
				<a href="http://www.facebook.com/centro.dedalo.1?fref=ts" title="Facebook"><img src="images/facebook.png" alt="Facebook" /></a>
				<a href="http://centrodedalo.blogspot.it/" title="Blog"><img src="images/blogger.png" alt="Blog" /></a>
				<a href="mailto:dedalocentro@gmail.com"><img src="images/mail.png" alt="Email" /></a>										
		</div>	
	</div>
	<div id="header">
		<div id="logo">
				<h1>Centro Dedalo</h1>
		</div>
<!--		<h1>Centro Dedalo</h1>-->
		<div id="menu">
			<ul>
				<li><a class="centro">Il Centro</a></li>
				<li><a class="servizi"  onclick="resetPage('defaultservices.php');  return false;">Servizi</a></li>
				<li><a class="equipe" onclick="resetPage('defaultequipe.php'); return false;">Equipe</a></li>
				<!-- <li><a class="news">News</a></li> -->
				<li><a class="news" onclick="resetPage('defaultnews.php'); return false;">News</a></li>
				<li><a class="contatti">Contatti</a></li>			
			</ul>
		</div>		
	</div>


