<?php
$pagename = 'contatti';
$pagetitle = 'Contatti';
$pagedesc = 'Visita il nostro stabilimento in Strada Provinciale Colligiana 29, 53035 Monteriggioni (SI). Tel. 0577-304104, info@cartonfactory.it.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
if ($_POST['doSend']=='Invia') {
$name = $_POST[name];
$email = $_POST[email];
$subject = $_POST[subject];
$message = $_POST[message];
$headers = "";
$headers .= "From: $email\n";
$headers .= "Reply-To: $email\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=utf-8\n";
$to = "info@cartonfactory.it";
/*$message = $messaggio;*/
$messagebody = "Hai ricevuto una nuova email via cartonfactory.it, ecco i dettagli:<br />
Nome: $name <br />

E-mail: $email<br />

Oggetto: $subject<br>

Messaggio: $message<br />";

mail ($to,$subject,$messagebody,$headers);
$sent = "1";
}
?>
<div id="main">
    <div class="container">

        <div class="col-sm-10 col-sm-offset-1">
            <div id="map_canvas" style="width:100%; height:400px;margin-bottom: 30px;">
            </div>
        </div>

        <div class="col-sm-12">
            <div class="col-sm-6 addressInfo">
                <h1>IMBALLAGGI IL CASONE SRL</h1>
                <h2>SEDE LEGALE</h2>
                <p>Via Cassia Nord, 39<br>
                Castellina Scalo<br>
                53032 Monteriggioni (SI)</p>
                
                <h2>STABILIMENTO</h2>
                <p>Strada Provinciale Colligiana, 29<br>
                53035 Monteriggioni (SI)</p>
                
                <p><strong>Telefono</strong>: +39 0577 304104<br>
                <strong>Fax</strong>: +39 0577 304722</p>
                
                <p><strong>Email</strong>: <a href="mailto: info@cartonfactory.it">info@cartonfactory.it</a></p>
            </div>
            <div class="col-sm-6">
<?php
if ($sent=='1') {
?>
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <h4>Messaggio inviato!</h4>
              Sarai ricontattato al pi&ugrave; presto.
            </div>
<?php
}
?>
            <form action="contatti.php" method="post" id="contatti">
                <div class="form-group">
                  <input type="text" class="form-control" id="name" name="name" placeholder="Inserisci il tuo nome">
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" id="email" name="email" placeholder="Inserisci la tua email">
                </div>
                <div class="form-group">
                  <input type="etext" class="form-control" id="subject" name="subject" placeholder="Oggetto del messaggio">
                </div>
                <div class="form-group">
                  <textarea class="form-control" rows="7" id="message" name="message"></textarea>
                </div>
                <div class="checkbox">
                    <label>
                      <input type="checkbox" value="Yep" name="privacy">
                        <a id="privacy" href="#" rel="tooltip" data-toggle="tooltip" title="Nel inviare i miei dati dichiaro di aver letto l'informativa e sono consapevole che il trattamento degli stessi è necessario per ottenere il servizio proposto. A tal fine, nel dichiarare di essere maggiorenne, fornisco il mio consenso." data-placement="right" style="font-size:14px;cursor: help;">
                            Informativa sulla Privacy ai sensi del D.Lgs 196/2003</a>
                    </label>
                </div>
                <button type="submit" class="btn btn-default" name="doSend" value="Invia">Invia</button>
              </form>
            </div>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>