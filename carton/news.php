<?php
$pagename = 'news';
$pagetitle = 'News';
$pagedesc = 'Seguite le ultime novità su Carton Factory: le news, gli eventi e le manifestazioni a cui partecipiamo.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
$type = '00';
if (isset($_GET[type])) {
    $type = $_GET[type];
}
$query = "SELECT * FROM products WHERE category='$type' ORDER BY id DESC";
$result = mysql_query($query);
if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);

?>
<div id="main">
    <div class="container">
<?php
for ($j = 0 ; $j < $rows ; ++$j) {
    $row = mysql_fetch_row($result);
    $datetime = strtotime($row[3]);
    $mysqldate = date("d/m/Y", $datetime);
?>
        <div class="row">
            <div class="newsH">
                <div class="col-sm-6 newsP">
                    <img src="<?php echo $row[2]; ?>" alt="Immagine News">
                </div>
                <div class="col-sm-6 newsT">
                    <img src="img/arrow.png" class="arrow">
                    <h1><a href="post.php?item=<?php echo $row[5]; ?>" title="<?php echo $row[0]; ?>"><?php echo $row[0]; ?></a></h1>
                    <h5>Pubblicata il <?php echo $mysqldate; ?> alle <?php echo $row[4]; ?> (<a href="post.php?item=<?php echo $row[5]; ?>" title="<?php echo $row[0]; ?>">Link</a>)</h5>
                    <p>
                        <?php echo $row[1]; ?>
                    </p>
                </div>
            </div>
        </div>
<?php
}
?>
        <!--
        <div class="row">
            <div class="newsH">
                <div class="col-sm-8 newsP">
                    <img src="http://placehold.it/1000x500" alt="">
                </div>
                <div class="col-sm-4 newsT">
                    <img src="img/arrow.png" class="arrow">
                    <h1>Ciao</h1>
                    <h5>Amico</h5>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quis tristique metus. Nunc non sem faucibus, consectetur ante in, vehicula nisl. Donec nec mauris venenatis, aliquam libero sed, ullamcorper tortor. 
                    </p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="newsH">
                <div class="col-sm-8 newsP">
                    <img src="http://placehold.it/1000x500" alt="">
                </div>
                <div class="col-sm-4 newsT">
                    <img src="img/arrow.png" class="arrow">
                    <h1>Ciao</h1>
                    <h5>Amico</h5>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras quis tristique metus. Nunc non sem faucibus, consectetur ante in, vehicula nisl. Donec nec mauris venenatis, aliquam libero sed, ullamcorper tortor. 
                    </p>
                </div>
            </div>
        </div>-->
    </div>
</div>
<?php
include_once 'footer.php';
?>        