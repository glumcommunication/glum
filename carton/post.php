<?php
$pagename = 'post';
$pagetitle = 'News';
$pagedesc = '';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
$item = '00';
if (isset($_GET[item])) {
    $item = $_GET[item];
}
$query = "SELECT * FROM products WHERE slug='$item'";
$result = mysql_query($query);
if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);

?>
<div id="main">
    <div class="container">
<?php
for ($j = 0 ; $j < $rows ; ++$j) {
    $row = mysql_fetch_row($result);
    $datetime = strtotime($row[3]);
    $mysqldate = date("d/m/Y", $datetime);
?>
        <div class="row">
            <div class="newsH">
                <div class="col-sm-6 newsP">
                    <img src="<?php echo $row[2]; ?>" alt="Immagine News">
                </div>
                <div class="col-sm-6 newsT">
                    <img src="img/arrow.png" class="arrow">
                    <h1><?php echo $row[0]; ?></h1>
                    <h5>Pubblicata il <?php echo $mysqldate; ?> alle <?php echo $row[4]; ?> (<a href="post.php?item=<?php echo $row[5]; ?>" title="<?php echo $row[0]; ?>">Link</a>)</h5>
                    <p>
                        <?php echo $row[1]; ?>
                    </p>
                    <!-- AddThis Button BEGIN -->
                    <div class="addthis_toolbox addthis_default_style" style="margin-top:50px;">
                    <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                    <a class="addthis_button_tweet"></a>
                    <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
                    <a class="addthis_button_pinterest_pinit" pi:pinit:layout="horizontal" pi:pinit:url="http://www.addthis.com/features/pinterest" pi:pinit:media="http://www.addthis.com/cms-content/images/features/pinterest-lg.png"></a>
                    </div>
                    <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
                    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4da305f241d8027b"></script>
                    <!-- AddThis Button END -->
                </div>
            </div>
        </div>
<?php
}
?>

    </div>
</div>
<?php
include_once 'footer.php';
?>        