<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>CARTON FACTORY</h1>
                <p>&copy; <?php echo date(Y); ?> Imballaggi Il Casone Srl - Partita Iva 0015952052</p>
            </div>
        </div>
        <div class="row" id="footerInfo">
            <div class="col-md-6 col-md-push-3" id="footerInfo1">
                <div class="col-xs-6 col-sm-6">
                    <p><strong>SEDE LEGALE</strong><br>
                    Via Cassia Nord, 30<br>Castellina Scalo<br>53035 Monteriggioni (SI)</p>
                </div>
                <div class="col-xs-6 col-sm-6" id="stab">
                    <p><strong>STABILIMENTO</strong><br>
                    Strada Provinciale Colligiana<br>53035 Monteriggioni (SI)</p>
                </div>
            </div>
            <div class="clearfix visible-sm"></div>
            <div class="col-md-3 col-md-pull-6 col-xs-6" id="footerInfo2">
                <p><strong>Email</strong>: <a href="info@cartonfactory.it">info@cartonfactory.it</a></p>
            </div>

            <div class="col-md-3 col-xs-6" id="footerInfo3">
                <p><strong>Telefono</strong>: +39 0577 304104<br><strong>Fax</strong>: +39 0577 304722</p>
            </div>
        </div>
        <div class="col-sm-12">
            <a href="http://www.cartonfactory.it/admin">
                <h6 style="color:#666;text-align:right;">Area amministrativa</h6>    
            </a>
        </div>
		<div class="col-sm-12">
			<p style="text-align:right;">Crafted by <a href="http://www.glumcommunication.it/" target="_BLANK">GLuM Communication</a></p>
		</div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>        
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="js/pup-slider.js"></script>
    <script type="text/javascript">
$(document).ready(function(){
   if( Modernizr.touch ) {
       $("figButton").click(function(){
        $(this).toggleClass("cap-left");
       });
        $("figClose").click(function(){
        $(this).toggleClass("cap-left");
       });
   }; 
})

            </script>
<?php
if ($pagename == 'about') {
?>    
<script>

$(document).ready(function() {

    $("#people01").pupslider({ stick: 'right', speed: 500, opacity: 1 });
    $("#people02").pupslider({ stick: 'right', speed: 500, opacity: 1 });
    $("#people03").pupslider({ stick: 'right', speed: 500, opacity: 1 });
    $("#people04").pupslider({ stick: 'right', speed: 500, opacity: 1 });
    $("#people05").pupslider({ stick: 'right', speed: 500, opacity: 1 });
    $("#people06").pupslider({ stick: 'right', speed: 500, opacity: 1 });

});
$(window).resize(function() {
    var newfw = $(".pushup-form").outerWidth();
    
//    var settings = {stick: 'right', speed: 1000, opacity: 1 };
$('.btn-close', wrapper).addClass('right');
//    
    $('.pushup-form', wrapper).addClass('right').css({ left: newfw, display: '' });
});

</script>
<?php
}
?>
<script>
$(document).ready(function() {

	// validate signup form on keyup and submit
	var validator = $("#contatti").validate({
		rules: {
			name: "required",
                        subject: "required",

                        message: "required",
                        privacy: "required",
			email: {
				required: true,
                minlength: 7,
                email: true
			}
		},
		messages: {
			name: "Campo obbligatorio - Inserisci il tuo nome",
                        surname: "Inserisci il tuo cognome",
            subject: "Scegli un oggetto per il messaggio",
			phone: {
				
				minlength: "Il numero deve essere composto da almeno 9 cifre"
			},
            message: "Inserisci un messaggio",
            privacy: "Accetazione informativa sulla privacy obbligatoria!<br> ",
			email: {
                required: "Campo obbligatorio - Inserisci un indirizzo email",
                minlength: "Inserisci un indirizzo email valido",
                email: "Inserisci un indirizzo email valido"
            }
			
		},
	});


});
</script>

<script src="js/jquery.flexslider-min.js"></script>
<script type="text/javascript">
    $(window).load(function(){
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: true,
        slideshow: false,
        itemWidth: 96,
        minItems: 3,
        itemMargin: 10,
        asNavFor: '#slider'
      });
      
      $('#slider').flexslider({
        animation: "slide",
        controlNav: true,
        animationLoop: true,
        slideshow: false,
        sync: "#carousel",
        start: function(slider){
        $('body').removeClass('loading');
        }
      });
    });
</script>
<?php
if ($pagename == "about") {
?>

<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script>
    (function($){
        $(document).ready(function(){
            $(".bioText").mCustomScrollbar({
                scrollButtons:{
				    enable:true
                },
				advanced:{
                    updateOnContentResize:true
                },
            });
        })
    })(jQuery);
	</script>
<?php
}
?>
        </div>
    </body>
</html>
