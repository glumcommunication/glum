<div id="header">
    <div class="container">

        <div class="row" id="headerLogo">
            <div id="logo" class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <a href="http://www.cartonfactory.it/" title="Carton Factory"><img src="/img/logo.png" alt="Logo"></a>
            </div>
            <div id="social" class="col-md-4 col-sm-3">
                <a href="https://www.facebook.com/pages/Carton-Factory-Arredi-in-cartone/565459623469948"><i class="fa fa-facebook"></i></a>
                <!--<a href="#"><i class="fa fa-youtube"></i></a>-->
                <!--<a href="facebbok"><img src="img/twitter-sm.png" alt="Twitter"></a>-->
            </div>
        </div>
        <div class="row" id="headerMenu">
            <div class="col-sm-12">
                <nav class="navbar navbar-default" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-carton">
                         <!--<a class="navbar-brand visible-xs" href="#">Menu</a>-->
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    
                            
    
    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-carton">                                    
                        <ul class="nav nav-justified">
                            <li <?php if($pageName=='index'){echo 'class="sel"';} ?>><a href="index.php" title="Home">HOME</a></li>
                            <li <?php if($pageName=='about'){echo 'class="sel"';} ?>><a href="about.php" title="Chi siamo">CHI SIAMO</a></li>
                            <li class="dropdown <?php if($pagename=='servizi'){echo ' sel';} ?>"><a href="#" title="Prodotti" class="dropdown-toggle" data-toggle="dropdown">PRODOTTI <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="prodotti.php?cat=01">Sedute</a></li>
                                    <li><a href="prodotti.php?cat=02">Tavoli e Scrivanie</a></li>
                                    <li><a href="prodotti.php?cat=03">Complementi d'Arredo</a></li>
                                    <li><a href="prodotti.php?cat=04">Oggettistica</a></li>
                                    <li><a href="prodotti.php?cat=00">Tutti i prodotti</a></li>
                                </ul>                            
                            </li>
                            <li class="dropdown <?php if($pageName=='news'){echo ' sel';} ?>"><a href="#" title="News" class="dropdown-toggle" data-toggle="dropdown">NEWS <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="news.php?type=01">Eventi</a></li>
                                    <li><a href="news.php?type=02">Altre notizie</a></li>
                                </ul>
                            </li>
                            <li <?php if($pageName=='contatti'){echo 'class="sel"';} ?>><a href="contatti.php" title="Contatti">CONTATTI</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>