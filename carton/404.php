<?php
$pagename = '404';
$pagetitle = '404 - Page not found';
$pagedesc = '';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';


?>
<div id="main">
    <div class="container">

        <p style="text-align:center;margin-top:30px;"><span class="glyphicon glyphicon-exclamation-sign"></span>&emsp;La pagina che stai cercando non esiste. Carton Factory ha un nuovo sito, <a href="/" title="Carton Factory" class="notfound">torna alla HOME</a> e inizia a esplorarlo, speriamo ti piaccia.</p>
    </div>
</div>
<?php
include_once 'footer.php';
?>        