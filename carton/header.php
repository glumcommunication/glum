<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <!--<meta http-equiv="X-UA-Compatible" content="IE=edge">-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link rel="icon" href="img/favicon.png" type="image/svg" />
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,600,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,600' rel='stylesheet' type='text/css'>
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
        
        <meta name="description" content="<?php echo $pagedesc; ?>">
        <link href="/css/normalize.css" rel="stylesheet" media="screen">
        <link href="/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="/css/style.css" rel="stylesheet" media="screen">
        <link href="/css/flexslider.css" rel="stylesheet" media="screen">
        <script src="/js/respond.js"></script>
        <script src="/js/selectivizr-min.js"></script>
        <!--<link href="css/lightbox.css" rel="stylesheet" />-->
        <meta name="keywords" content="Arredo negozi personalizzato,  mobili in cartone, sedie in cartone, stand in cartone, negozi in cartone,  sedie, personalizzato,  arredi ecologici, eco arredi, design di cartone, cardboard design, arredi in cartone, mobili cartone, mobili in cartone, arredi in cartone, mobili di cartone, arredi cartone, complementi di arredo in cartone,  allestimenti in cartone, allestimenti stand, design in cartone, design, poltrone, sedie, sgabelli, tavoli, librerie, cassettiere, mensole,  contenitori multiuso, architetti, cartone design,  cardboard, allestimenti in cartone, cartone riciclato, negozio fai da te, oggetti design, merchandising, reboard, riciclato, recycle, recycled design, Made in Italy, eco-design, ambiente, sostenibilità, allestimento stand fieristico, riciclabile, riciclabili, ecologia, creatività">

<?php

if ($pagename == "contatti") {
    echo <<<_END
        
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    function initialize() {
        var latlng = new google.maps.LatLng(43.393955, 11.18597);
        var settings = {
            zoom: 15,
            center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
            navigationControl: true,
            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
            mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"), settings);

  var companyLogo = new google.maps.MarkerImage('img/positionshadow.png',
    new google.maps.Size(86,60),
    new google.maps.Point(0,0)

);

var companyPos = new google.maps.LatLng(43.393955, 11.18597);
var companyMarker = new google.maps.Marker({
    position: companyPos,
    map: map,
    icon: companyLogo,
    //shadow: companyShadow,
    title:"Imballaggi Il Casone"
});
var contentString = '<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h1 id="firstHeading" class="firstHeading" style="font-size:14px;font-weight:bold;border-bottom: none;margin-bottom:5px;">Imballaggi Il Casone Srl</h1>'+
    '<div id="bodyContent">'+
    '<p style="font-size:12px;">'+
        '<strong>SEDE LEGALE</strong><br>Via Cassia Nord, 30<br>Castellina Scalo<br>53035 Monteriggioni (SI)<br>'+
        '<strong>STABILIMENTO</strong><br>Strada Provinciale Colligiana, 29<br>53035 Monteriggioni (SI)<br><br>'+
        'Tel.: +39 0577 304104<br>Fax: +39 0577 304722<br>Email: <a href="mailto:info@cartonfactory.it">info@cartonfactory.it</a></p>'+
    '</div>'+
    '</div>';
 
var infowindow = new google.maps.InfoWindow({
    content: contentString
});
google.maps.event.addListener(companyMarker, 'click', function() {
  infowindow.open(map,companyMarker);
});
    }
    </script>
_END;
}

?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46459531-1', 'cartonfactory.it');
  ga('send', 'pageview');

</script>
        <title><?php echo $pagetitle ?> | Carton Factory - Mobili in cartone, complementi d'arredo, design a Siena</title>
</head>
    <body <? if ($pagename == "contatti") echo " onload=\"initialize()\"";?>>

        <div id="wrapper">