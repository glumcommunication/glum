<?php
$pagename = 'about';
$pagetitle = 'Chi siamo';
$pagedesc = "La creatività di Architetti e designers unita alla professionalità di un'azienda storica.";
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
<div id="main">
    <div class="container">

        <div class="col-sm-12">
            <p>
                <span style="font-size:20px;">Carton Factory</span> è un brand di eco- design 100% italiano, nato dalla lunga esperienza 
                dell’azienda toscana Imballaggi Il Casone, che dal 1947 progetta e realizza scatole da imballaggio industriale.<br>
                Carton Factory  nasce da un processo creativo che si concretizza in molteplici linee di prodotto d’arredo. Il cartone,  
                si eleva al design ed all’ architettura dando vita a vere e proprie creazioni artistiche ed ecologiche di uso quotidiano.<br>
                Il cartone, materiale tra i più ecologici, connota positivamente il brand, posizionandolo sui temi ambientali. Il progetto, 
                includendo l’uso di materiali riciclati e riciclabili e una lavorazione a basso impatto ambientale, risponde efficacemente 
                all’evoluzione del mercato ed alle esigenze dei consumatori, sempre più interessati ad arredare la casa in modo ecologico, 
                senza rinunciare allo stile e al design.<br>
                La storia di Carton Factory è anche la storia di un’azienda e di una famiglia che da oltre 65 anni, opera sul mercato con 
                un legame a stretto contatto con il territorio.
            </p>
            <p>                
                Oltre ai complementi standard già sviluppati "da catalogo", Carton Factory, con un ottimo rapporto tra funzionalità qualità 
                e prezzo, è in grado di ideare, produrre e distribuire prodotti su misura o su progetto del cliente, dal piccolo oggetto 
                allo stand fieristico.
            </p>
        </div>
        <div class="col-sm-12">
            <div class="imgBorder">
                <img src="img/fotogruppo.jpg" alt="gruppo" class="img-thumbnail">
            </div>
        </div>
        <div class="aboutPeople">
            <div class="col-sm-12">
                <h1>I NOSTRI DESIGNER</h1>
            </div>
            <div class="col-sm-4">
                <figure class="cap-left">
                    <div class="contactSlide contents" id="people01">                    
                        <div class="btn-show">
                           Bio
                        </div>
                        <div class="pushup-form">
                            <div class="btn-close">
                                X
                            </div>
                            <div class="clear">
                            </div>
                            <div class="bioText">
                                <h1>MATTEO BARONI</h1>
                                <p>
                                    Matteo Baroni lavora per prestigiose industrie nel campo del design: Ideal Standard, Targetti, Ghidini, Effeti cucine, 
                                    Giemmegi Cucine, TLF Fantoni, ISA bagno, Buzzi&Buzzi, Ferlea, etc. etc.<br>
                                    Nel settore retail ha progettato in varie parti del mondo per: Ermanno Scervino, CarloBay, Davines, Toni&Guy, 
                                    0571 Garage realizzando inoltre numerosi retails di clienti privati.<br>
                                    Ha lavorato per COOP e per RAI TRADE progettando studi televisivi ed allestimenti fieristici.<br>
                                    Contemporaneamente ha progettato molte abitazioni private di prestigio  per una clientela internazionale.<br>
                                    Matteo Baroni ha ricevuto riconoscimenti internazionali per aver progettato il porta-biciclette per la città di New York. <br>
                                    Ha inoltre ricevuto premi nel campo del design per oggetti di uso comune.<br>
                                    Attualmente lo studio sta anche lavorando ad un importante lottizzazione a Los Angeles per la realizzazione di un complesso 
                                    immobiliare in stile italiano che ospiterà prestigiose aziende americane.
                                </p>
                            </div>
                        </div>
                    </div>
                    <img src="img/matteo1.jpg" alt="Matteo Baroni">
                    <!--<figcaption>
                        <h1>Ciao</h1>
                        <p>Ricchione</p>
                    </figcaption>-->
                </figure>
            </div>
            <div class="col-sm-4">
                <figure class="cap-left">
                    <div class="contactSlide contents" id="people02">                    
                        <div class="btn-show">
                           Bio
                        </div>
                        <div class="pushup-form">
                            <div class="btn-close">
                                X
                            </div>
                            <div class="clear">
                            </div>
                            <div class="bioText">
                                <h1>CHIARA BERTINI</h1>
                                <p>
                                    Chiara Bertini nasce a Firenze nel 1982. Dopo il diploma conseguito presso il Liceo Artistico “Leon Batista Alberti”, frequenta e 
                                    si forma presso il Corso di Laurea in Disegno Industriale dell’Ateneo Fiorentino.<br>
                                    Durante gli anni universitari partecipa al concorso promosso dall’azienda L’Orèal per la progettazione del flacone e del packaging 
                                    esterno, per gli stilisti Viktor&Rolf, classificandosi tra i primi 10 progetti esposti all’azienda.<br>
                                    Dopo la laurea frequenta corsi di modellazione e renderizzazione tridimensionale, raggiungendo ottimi risultati che le permetteranno di 
                                    analizzare l’oggetto nelle molteplici fasi della progettazione.<br>
                                    Dal 2008 collabora come libero professionista con lo studio MATTEOBARONIARCHITETTO occupandosi di architettura di interni,design e progetti di grafica.
                                </p>
                            </div>
                        </div>
                    </div>
                    <img src="img/chiara1.jpg" alt="Chiara Bertini">
                    <!--<figcaption>
                        <h1>Ciao</h1>
                        <p>Ricchione</p>
                    </figcaption>-->
                </figure>
            </div>
            <div class="col-sm-4">
                <figure class="cap-left">
                    <div class="contactSlide contents"  id="people03">                    
                        <div class="btn-show">
                           Bio
                        </div>
                        <div class="pushup-form">
                            <div class="btn-close">
                                X
                            </div>
                            <div class="clear">
                            </div>
                            <div class="bioText">
                                <h1>SARA CASATI</h1>
                                <p>
                                    Nata a Poggibonsi il 19/11/1983, attualmente vive tra le bellissime colline senesi.<br>
                                    Nel 2007 si laurea presso l’Istituto di Architettura di Firenze al Corso di Disegno Industriale.<br>
                                    Durante  importanti esperienze presso prestigiose aziende come "Effeti Cucine" e "Karol", ha anche realizzato i suoi primi 
                                    oggetti, provenienti da concorsi come “Oltre lo specchio” e "Young Design awards".<br>
                                    Si appassiona al mondo del Design, e le piace studiare e progettare nuove soluzioni per soddisfare i nostri stili di vita in continua evoluzione.
                                </p>
                            </div>
                        </div>
                    </div>
                    <img src="img/sara1.jpg" alt="Sara Casati">
                    <!--<figcaption>
                        <h1>Ciao</h1>
                        <p>Ricchione</p>
                    </figcaption>-->
                </figure>
            </div>
            <div class="col-sm-4">
                <figure class="cap-left">
                    <div class="contactSlide contents" id="people04">                    
                        <div class="btn-show">
                           Bio
                        </div>
                        <div class="pushup-form">
                            <div class="btn-close">
                                X
                            </div>
                            <div class="clear">
                            </div>
                            <div class="bioText">
                                <h1>CLAUDIO MACCARI</h1>
                                <p>
                                    Claudio Maccari si diploma all'Accademia di Belle Arti di Firenze. Da sempre, parallelemente all'attività artistica, 
                                    disegna oggetti d'uso, complementi di arredo e accessori moda.
                                </p>
                            </div>
                        </div>
                    </div>
                    <img src="img/claudio.jpg" alt="Claudio Maccari">
                </figure>
            </div>
            <div class="col-sm-4">
                <figure class="cap-left">
                    <div class="contactSlide contents" id="people05">                    
                        <div class="btn-show">
                           Bio
                        </div>
                        <div class="pushup-form">
                            <div class="btn-close">
                                X
                            </div>
                            <div class="clear">
                            </div>
                            <div class="bioText">
                                <h1>LORELLA GALLETTI</h1>
                                <p>
                                    Si laurea nel 1999 presso la Facoltà di Architettura di Firenze.<br>
                                    Collabora attualmente con uno studio di architettura di Colle di Val d'Elsa, predilige il restauto e la progettazione d'interni.
                                </p>
                            </div>
                        </div>
                    </div>
                    <img src="img/lorella.jpg" alt="Lorella Galletti">
                </figure>
            </div>
            <div class="col-sm-4">
                <figure class="cap-left">
                    <div class="contactSlide contents" id="people06">                    
                        <div class="btn-show">
                           Bio
                        </div>
                        <div class="pushup-form">
                            <div class="btn-close">
                                X
                            </div>
                            <div class="clear">
                            </div>
                            <div class="bioText">
                                <h1>DANIELE LANDI</h1>
                                <p>
                                    Laureato alla Facoltà di Architettura di Firenze.<br>
                                    Nel 1989 fonda il proprio studio professionale a Colle di Val d'Elsa occupandosi da subito di architettura d'interni.<br>
                                    Partecipa a concorsi di architettura e collabora con varie ditte nel settore design, arredamenti e contract bancario.<br>
                                    Successivamente indirizza lo studio verso la progettazione e ristrutturazione nel campo residenziale per poi rituffarsi in 
                                    quello che è stato il primo amore, il design industriale.
                                </p>
                            </div>
                        </div>
                    </div>
                    <img src="img/daniele.jpg" alt="Daniele Landi">
                </figure>
            </div>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>