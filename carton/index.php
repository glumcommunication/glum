<?php
$pagename = 'servizi';
$pagetitle = 'Home';
$pagedesc = 'Carton Factory - Arredamento e complementi in cartone, design Made in Italy.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 badgeContainer">
                <div class="col-sm-3 col-xs-6 homeBadge">
                    <img src="img/badge1.png" alt="badge 1">
                    <h1>L'Azienda</h1>
                    <p>
                        Dall'esperienza di oltre 65 anni nasce un progetto audace e moderno.
                    </p>
                </div>
                <div class="col-sm-3 col-xs-6 homeBadge">
                    <img src="img/badge2.png" alt="badge 2">
                    <h1>La Filosofia</h1>
                    <p>
                        L'attenzione all'ambiente ci guida nel lavoro di tutti i giorni.
                    </p>
                </div>
                <div class="clearfix visible-xs"></div>
                <div class="col-sm-3 col-xs-6 homeBadge">
                    <img src="img/badge3.png" alt="badge 3">
                    <h1>Il Cartone</h1>
                    <p>
                        Da un materiale povero possono nascere oggetti sorprendenti.
                    </p>
                </div>
                <div class="col-sm-3 col-xs-6 homeBadge">
                    <img src="img/badge4.png" alt="badge 4">
                    <h1>Il Design</h1>
                    <p>
                        Made in Italy è sinonimo di qualità in tutto il mondo.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div id="homeSlider" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#homeSlider" data-slide-to="0" class="active"></li>
                        <li data-target="#homeSlider" data-slide-to="1"></li>
                        <li data-target="#homeSlider" data-slide-to="2"></li>
                        <li data-target="#homeSlider" data-slide-to="3"></li>
                        <li data-target="#homeSlider" data-slide-to="4"></li>
                        <li data-target="#homeSlider" data-slide-to="5"></li>
                    </ol>
        
          <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="img/imgh/home01.jpg" alt="homeslide" class="img-rounded">
                            <div class="carousel-caption">
                            
                            </div>
                        </div>
                        <div class="item">
                            <img src="img/imgh/home02.jpg" alt="homeslide" class="img-rounded">
                            <div class="carousel-caption">
                            
                            </div>
                        </div>
                        <div class="item">
                            <img src="img/imgh/home03.jpg" alt="homeslide" class="img-rounded">
                            <div class="carousel-caption">
                            
                            </div>
                        </div>
                        <div class="item">
                            <img src="img/imgh/home04.jpg" alt="homeslide" class="img-rounded">
                            <div class="carousel-caption">
                            
                            </div>
                        </div>
                        <div class="item">
                            <img src="img/imgh/home05.jpg" alt="homeslide" class="img-rounded">
                            <div class="carousel-caption">
                            
                            </div>
                        </div>
                        <div class="item">
                            <img src="img/imgh/home06.jpg" alt="homeslide" class="img-rounded">
                            <div class="carousel-caption">
                            
                            </div>
                        </div>                        
                    </div>
        
          <!-- Controls -->
                    <a class="left carousel-control" href="#homeSlider" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#homeSlider" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>
