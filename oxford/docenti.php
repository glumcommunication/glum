<?php
$thisPage = "Docenti";
$description = "Scuola di inglese, francese, spagnolo, tedesco, russo, arabo, cinese, italiano per stranieri, a Siena in Toscana, Italia. Scuola di lingue straniere a Siena.";
include("header.php");
include("menu.php");
?>

	<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>Staff</h1>
		<img src="images/fotogruppo.jpg" alt="" />			
	</div>
</div>


	
<div class="rightSide">

	<div id="lbox">

			<div class="hbox">
			<h1 style="font-size:1em;">Orsola Maione</h1>
				<img src="images/people/orsola.jpg" alt="Orsola" />
				<p><span class="redp">Direzione</span></p>
			</div>
		
			<div class="hbox">
			<h1 style="font-size:1em;">Lucia Morrone</h1>
				<img src="images/people/lucia.jpg" alt="Lucia" />
			<p><span class="redp">Segreteria</span></p>
			</div>
		
		<br clear="all">
		<h2>I Docenti</h2>
		<p>I nostri insegnanti, tutti  madrelingua, vengono selezionati in base al loro curriculum; prerogativa 
		necessaria per i docenti di lingua inglese, per esempio, è essere in possesso di un’abilitazione 
		all’insegnamento, quindi di una certificazione T.E.F.L. (Teaching English as a foreign language ), o 
		C.E.L.T.A.(Certificate in English Language Teaching to Adults) o altre certificazioni cher attestino 
		l'abilitazione all'insegnamento. Stesso criterio vale per i docenti di tutte le altre lingue.<br />
		Gli insegnanti vengono valutati anche in base alla loro esperienza professionale e alla loro capacità 
		di lavorare in gruppo perché è proprio il corpo docente professionale, motivato e sereno ad essere 
		una delle caratteristiche che contribuisce da sempre al successo della New Oxford School.</p>
		
		<h3>Metodo d’insegnamento</h3>
		
		<p>La metodologia d’insegnamento è basata sulla conversazione, il programma grammaticale di ogni livello viene 
		svolto interamente durante il corso ma è di supporto alla conversazione, il metodo non è noioso, lento bensì 
		fin dalla prima lezione stimolante, divertente; i processi d’apprendimento sono molto piu’ veloci e riscontrabili 
		già dopo poche lezioni.<br />
		Durante le lezioni s’impara a pensare in lingua e di conseguenza ad esprimersi senza far riferimento alla nostra 
		lingua madre; vengono riproposte situazioni di vita quotidiana e lavorative in base al livello di conoscenza degli 
		studenti.</p>
<div class="ltable">		
<div class="lrow">
	<div class="hbox">
	<h1 style="font-size:1em;">Silia</h1>
		<img src="images/people/silia.jpg" alt="Silia" />
		<p>Referente Trinity</p>
	</div>

	<div class="hbox">
	<h1 style="font-size:1em;">Jeremy</h1>
		<img src="images/people/jeremy.jpg" alt="Jeremy" />
	<p>Docente di Inglese e responsabile viaggi studio all'estero</p>
	</div>

	<div class="hbox">
	<h1 style="font-size:1em;">Alex</h1>
		<img src="images/people/alex.jpg" alt="Alex" />
		<p>Docente di Inglese</p>			
	</div>
	
	<div class="hboxR">
	<h1 style="font-size:1em;">Luisa</h1>
		<img src="images/people/luisa.jpg" alt="Luisa" />
		<p>Docente di Inglese</p>			
	</div>
</div>
<div class="lrow">
	<div class="hbox">
	<h1 style="font-size:1em;">Laura</h1>
		<img src="images/people/laura.jpg" alt="Laura" />	
		<p>Docente di Inglese</p>
	</div>
	
	<div class="hbox">
	<h1 style="font-size:1em;">Rob</h1>
		<img src="images/people/rob.jpg" alt="Rob" />
		<p>Docente di Inglese</p>			
	</div>
	<div class="hbox">
	<h1 style="font-size:1em;">Chia</h1>
		<img src="images/people/chia.jpg" alt="Chia" />
		<p>Docente di Inglese</p>			
	</div>
	<div class="hboxR">
	<h1 style="font-size:1em;">Laurène</h1>
		<img src="images/people/laurene.jpg" alt="Laurène" />
		<p>Docente di Francese</p>			
	</div>
</div>
<div class="lrow">
	<div class="hbox">
	<h1 style="font-size:1em;">Maria</h1>
		<img src="images/people/maria.jpg" alt="Maria" />	
		<p>Docente di Spagnolo</p>
	</div>
	
	<div class="hbox">
	<h1 style="font-size:1em;">Romina</h1>
		<img src="images/people/romina.jpg" alt="Romina" />
		<p>Docente di Tedesco</p>			
	</div>
	<div class="hbox">
	<h1 style="font-size:1em;">Monica</h1>
		<img src="images/people/monica.jpg" alt="Monica" />
		<p>Docente di Italiano per Stranieri</p>			
	</div>
	
	<div class="hboxR">
	<h1 style="font-size:1em;">Vittoria</h1>
		<img src="images/people/vittoria.jpg" alt="Vittoria" />
		<p>Docente di Cinese</p>			
	</div>
	</div>
	</div>
</div>	
</div>
</div>


<?php
include("footer.php");
?>