<?php
$thisPage = "Cours d'italien pour étrangers";
$description = "COURS D’ITALIEN POUR ETRANGERS, INFORMATIONS GENERALES, DUREE DES LECONS, COURS EN MINI-GROUPE, COURS INDIVIDUEL, COURS MIXTE.";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>Cours d'italien pour étrangers</h1>
		<img src="../images/english-student.jpg" alt="" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>The New Oxford School COURS D'ITALIEN POUR ETRANGERS</h2> 

		<p>Nos cours collectifs d'Italien sont dédiés à tous ceux qui désirent apprendre ou améliorer leur langue italienne, avec des enseignants qualifiés et expérimentés. A la suite d'un test d'aptitude oral et écrit pour déterminer votre niveau d'italien, vous serez placés en groupes d'étude de minimum 3 personnes jusqu'à 10 étudiants maximum. L'emploi du temps et la durée des cours seront organisés en accord avec les préférences des participants de chaque groupe, du lundi au vendredi durant la matinée, mais également le samedi toute la journée. Les cours d'italien sont basés sur un programme de grammaire correspondant aux six niveaux principaux de compétence linguistique (Débutant, Elèmentaire, Intermédiaire 1, Intermédiaire 2, Avancé, Expert), tout en laissant un large espace à la conversation.</p>
		 
		<h3>COURS D’ITALIEN POUR ETRANGERS<br />
		INFORMATIONS GENERALES</h3> 
		
		<p><strong>DURÉE DES LEÇONS</strong><br />
		Veuillez considérer que les leçons ont une durée de 60 minutes.</p> 
		
		<p><strong>NOMBRE D’ÉTUDIANTS PAR CLASSE</strong><br />
		Le nombre minimum de participants pour former un mini-groupe est de 3 à 6 personnes maximum.</p>
		
		<p><strong>NIVEAU DE COMPETENCE<br />
		Après un test initial d’évaluation, les étudiants seront placés dans un des groupes suivants:</strong>
			<ul class="oxlist">		
				<li>Débutant</li>
				<li>Elémentaire</li>
				<li>Intermédiaire 1</li>
				<li>Intermédiaire 2</li>
				<li>Avancé</li>
				<li>Supérieur</li>
			</ul>
		</p>
		<p>Nos programmes sont basés sur  les exigeances du Common European Framework.</p>
		
		<p><strong> INCLUS DANS LE PRIX DU COURS:</strong>
			<ul class="oxlist">		
				<li>tout le matériel nécessaire</li>
				<li>certificat de présence</li> 
				<li>activités sociales</li>
			</ul>		
		</p> 
		
		<p><strong>SERVICES EXTRA:</strong>
			<ul class="oxlist">
				<li>accès à internet</li>
				<li>visionnage de films italiens en version originale</li>
			</ul>		
		</p>
		
		<p><strong>COURS EN MINI-GROUPE</strong></p>
		<div class="redp">Cours Standard</div>
		
		<p><strong>Début du cours</strong>:  tous le lundi <br />
		<strong>Durée</strong>: tous les lundi<br />
		<strong>Leçons</strong>: 3 par jour = 15 heures par semaine<br />
		<strong>Niveau</strong>: tous niveaux<br /> 
		<strong>Class Size</strong>: de 3 à 8 participants<br />
		<span class="redp"><a href="../pdf/price_list.pdf" title="Download">LISTE DES PRIX</a>&emsp;<a href="booking_form.php" title="Booking">S'INSCRIRE MAINTENANT!</a></span></p>				
		<p>Un cours standard consiste en un paquet de 15 heures de leçons par semaine, nombre d’heures minimum que nous offrons pour un cours collectif. 
		Ce cours dure minimum 2 semaines.Les leçons se déroulent de 10h00 à 13h30. Cela est idéal pour les étudiants qui désirent avoir du temps libre.</p>
		
		<div class="redp">Cours Intensif</div>
		<p><strong>Début du cours</strong>: tous le lundi<br /> 
		<strong>Durée</strong>: minimum une semaine<br />
		<strong>Leçons</strong>: 4 par jour = 20 heures par semaine<br />
		<strong>Niveau</strong>: tous niveaux<br />
		<strong>Effectifs</strong>: de 3 à 6 participants</p>
		<p><span class="redp"><a href="../pdf/price_list.pdf" title="Download">LISTE DES PRIX</a>&emsp;<a href="booking_form.php" title="Booking">S'INSCRIRE MAINTENANT!</a></span></p>				
		<p>Le cours intensif consiste en un paquet de 20 heures de leçons par semaine. Sont admis 6 étudiants maximum. Les leçons commencent à 
		9h00 et finissent à 13h30.</p>
		<p><strong>COURS INDIVIDUEL</strong></p>
		<p>
		<strong>Début du cours</strong>: tous le jours<br />
		<strong>Durée</strong>: minimum 3 heures<br />
		<strong>Niveau</strong>: tous niveaux<br />
		<strong>Effectifs</strong>: un seul participant<br />
		<span class="redp"><a href="../pdf/price_list.pdf" title="Download">LISTE DES PRIX</a>&emsp;<a href="booking_form.php" title="Booking">S'INSCRIRE MAINTENANT!</a></span></p>
		<p>Nos cours individuels sont adaptés spécifiquement à la demande de l’étudiant. Le contenu des leçons est planifié selon les besoins du participant afin qu’il puisse 
		apprendre en toute tranquilité avec l’attention de l’enseignant qui lui sera entièrement dédiée.</p>
		<p><strong>COURS MIXTE</strong></p>
		<p>
		<strong>Début du cours</strong>:             tous le lundi<br />
		<strong>Durée</strong>:                     minimum une semaine<br />
		<strong>Leçons de groupe</strong>:             3 par jour = 15 heures par semaine<br />
		<strong>Effectifs</strong>:           de 3 à 6 participants<br /> 
		<strong>Individual Leçons</strong>:        4 par semaine<br />
		<strong>Niveau</strong>: tous niveaux<br />                                                
		<span class="redp"><a href="../pdf/price_list.pdf" title="Download">LISTE DES PRIX</a>&emsp;<a href="booking_form.php" title="Booking">S'INSCRIRE MAINTENANT!</a></span></p>
		<p>Le cours mixte, qui dure minimum une semaine, allie les leçons collectives du matin aux leçons individuelles de l’après-midi (de 13h à 17h). Cette option donne aux étudiants 
		l’opportunité d’amplifier leur connaissance de l’Italien durant les leçons collectives matinales et la possibilité d’étudier des sujets spécifiques intensivement dans 
		l’après-midi, durant leur séjour à Sienne.<br />
		Nos cours collectifs d’Italien sont dédiés à ceux qui désirent apprendre ou améliorer leur Italien grâce à des enseignants qualifiés et expérimentés.<br />
		Après un examen d’entrée écrit et oral pour déterminer votre niveau d’Italien, vous serez placés dans des groupes d’étude composés de 3 à 10 personnes maximum.<br />
		L’emploi du temps et la durée des cours seront organisés selon les préférences des participants de chaque groupe, du lundi au vendredi, mais également 
		le samedi matin et après-midi.<br />
		Les cours d’Italien sont basés sur les programmes de grammaire liés aux six niveaux principaux de compétence linguistique (Débutant, Elémentaire, Intermédiaire 1, 
		Intermédiaire 2, Avancé, Supérieur) et laisse également un large espace à la conversation.</p> 
		
	
		<p class="redp"><a href="../pdf/Metodi_pagamento.pdf" title="Download">
		<span><img src="../images/pdfdownload.png" width="32" height="32" alt="Pdf" style="height:32px;width:32px;vertical-align: middle;" /></span>
		 Modes de paiement</a></p>
		<p class="redp"><a href="../pdf/terms.pdf" title="Download">
		<span><img src="../images/pdfdownload.png" width="32" height="32" alt="Pdf" style="height:32px;width:32px;vertical-align: middle;" /></span>
		 TERMES ET CONDITIONS</a></p>
		<p class="redp"><a href="../pdf/Request_information.pdf" title="Download"> 
		<span><img src="../images/pdfdownload.png" width="32" height="32" alt="Pdf" style="height:32px;width:32px;vertical-align: middle;" /></span>
		 Formulaire de domande d'informations</a></p>		
	</div>
</div>
</div>

<?php
include("footer.php");
?>