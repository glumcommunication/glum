<?php
include '../../dbc.php';
page_protect();
$tab = $_GET['tab'];
$action = $_GET['action'];

if ($_POST['doInsert']=='Aggiungi') {
	function sanitizeString($var) {
		$var = strip_tags($var);
		$var = stripslashes($var);
		return htmlentities($var,ENT_COMPAT, "UTF-8");
	}

	function toAscii($str, $replace=array(), $delimiter='-') {
		if( !empty($replace) ) {
			$str = str_replace((array)$replace, ' ', $str);
		}

		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
		return $clean;
	}
	include '../../tools/picupl.php';
        
	$oggi = getdate();
	$newsTitle = addslashes($_POST['newsTitle']);
	$category = $_POST[category];
	$slug = toAscii($newsTitle);
	$editor1 = addslashes($_POST['editor1']);
	$data = $oggi['year']."-".$oggi['mon']."-".$oggi['mday'];
	$ora = $oggi['hours'].":".$oggi['minutes'].":".$oggi['seconds'];
	print_r($username);
	$link = mysql_connect(DB_HOST, DB_USER, DB_PASS) or die("Couldn't make connection.");
	$db = mysql_select_db(DB_NAME, $link) or die("Couldn't select database");
	$query_ins_news = "INSERT INTO products VALUES" .
	"('$newsTitle', '$category', '$editor1', '$imgurl', '$data', '$ora', '$slug', '')";
	$result = mysql_query($query_ins_news);
	$lastid = mysql_insert_id();

	if (!$result) die ("Database access failed: " . mysql_error());
	$newsitem = "Hai aggiunto l'articolo '" . stripslashes($newsTitle) . "'.";
	$added = '1';
}
if($_POST['doEdit']=='Salva Modifiche') {
	$id = $_POST['id'];
	$newimg =$_POST['newimg'];
	if ($newimg == '1') {
		include '../../tools/picupl.php';
		$query="UPDATE products SET Image='$imgurl' WHERE id='$id'";
		$result = mysql_query($query);
		if (!$result) die ("Database access failed: " . mysql_error());
	}
	$newsTitle = addslashes($_POST['newsTitle']);
	$editor1 = addslashes($_POST['editor1']);
	$category = $_POST[category];
	$query="UPDATE products SET Titolo='$newsTitle', category='$category', Testo='$editor1' WHERE id='$id'";
	$result = mysql_query($query);
	if (!$result) die ("Database access failed: " . mysql_error());
	$newsitem = "Hai modificato l'articolo '" . stripslashes($newsTitle) . "'.";
	$edited = '1';
}
if($action == 'delete') {
	$id = $_GET['id'];
	$query_purge = "DELETE FROM products WHERE id='$id'";
	$result = mysql_query($query_purge);
	if (!$result) die ("Database access failed: " . mysql_error());
	$newsitem = "L'articolo &egrave; stato cancellato.";
	$deleteditem = '1';
}
include_once '../../header.php';
include_once '../../menu.php';
?>
<div id="main">
	<div class="tabscontainer">
		<ul class="tabs">
			<li <?php if($tab == '1'){echo "class=\"selected\"";} ?>>
				<a href="index.php?tab=1" title="Aggiungi una notizia">
					<div class="choicebuttons">Aggiungi promozioni e news</div>
				</a>
			</li>
			<li <?php if($tab == '2'){echo "class=\"selected\"";} ?>>
				<a href="index.php?tab=2" title="Gestisci notizie">
					<div class="choicebuttons">Modifica o cancella promozioni e news</div>
				</a>
			</li>
		</ul>
		<div class="formcontainer">
<script src="ckeditor.js"></script>
<?php
if($added=='1' || $edited=='1' || $deleteditem=='1') {
?>
			<div class="highlight">
				<p><?php echo $newsitem; ?></p>
			</div>
<?php
}
if ($tab == '1') {
?>
			<form method="post" action="index.php?tab=2"  enctype="multipart/form-data" class="default">
				<h3>Inserisci una nuova news o un nuovo evento</h3>
				<div class="idtable">
					<div class="idrow">
						<div class="idcell_1" style="width:75%;">
							<fieldset>
								<label for="newsTitle">Titolo</label>
								<input type="text" name="newsTitle" size="50" />
							</fieldset>
						</div>
						<div class="idcell_4">
							<fieldset>
								<label for="category">Categoria</label>
								<select name="category" id="category">
									<option value="01">Promozioni</option>
									<option value="02">News</option>
								</select>
							</fieldset>
						</div>
					</div>
					<div class="idrow">
						<div class="idcell_1">
							<fieldset>
								<!--<label for="editor1">Versione Italiana</label>-->
								<textarea cols="80" id="editor1" name="editor1" rows="10"></textarea>
<script>
CKEDITOR.replace( 'editor1', {
	filebrowserBrowseUrl: '../../tools/kcfinder/browse.php',
	filebrowserUploadUrl : '../../tools/kcfinder/upload.php',
	filebrowserImageBrowseUrl : '../../tools/kcfinder/browse.php',
	filebrowserImageUploadUrl : '../../tools/kcfinder/upload.php',
	filebrowserWindowWidth  : 800,
	filebrowserWindowHeight : 500
});
</script>
							</fieldset>
						</div>
					</div>

					<div class="idrow">
						<div class="idcell_1">
							<fieldset>
								<label for="fileup">Immagine Promozione/News</label>
								<input type="file" name="fileup" />
							</fieldset>
						</div>
					</div>
					<div class="idrow">
						<div class="idcell_1" style="text-align: center;">
							<fieldset>
								<input type="hidden" name="added" value="1" /><br />
							</fieldset>
							<fieldset>
								<input type="submit" name="doInsert" value="Aggiungi" />
							</fieldset>
						</div>
					</div>
				</div>
			</form>

<?php
}
elseif($tab == '2') {
	$sql = "SELECT * FROM products ORDER BY Data DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
	$rows = mysql_num_rows($result);
	$sum = 0;
?>
			<div class="listtable">
<?php
	for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);
	$datasql = "SELECT DATE_FORMAT(Data, '%d/%m/%Y')  as data FROM products WHERE id='$row[5]'";
	$newsBody = substr($row[1],0,50) . "...";
?>
				<div class="idrow"
<?php
if ($j % 2 == '0'){echo " style=\"background-color: #fff;\"";}
?>
				>
					<div class="idcell_2" style="width:10%;">
						<a href="index.php?id=<?php echo $row[7]; ?>&action=edit" title="Modifica"><img src="../../images/pencil.png" alt="Modifica"/></a>&emsp;
						<a href="index.php?id=<?php echo $row[7]; ?>&action=delete&tab=2" onClick="return confirmDelete()" title="Cancella"><img src="../../images/delete.png" alt="Cancella"/></a>
					</div>
					<div class="idcell_2" style="width:100px;margin-right:25px;"><img src="<?php echo $row[3]; ?>" alt="<?php echo $row[0]; ?>"></div>
					<div class="idcell_2" style="width:80%;"><?php echo $row[4] . '&emsp;' . $row[0]; ?></div>
				</div>
<?php
	}
?>
			</div>
<?php
}
elseif ($action == 'edit') {
	$id = $_GET['id'];
	$sql = "SELECT * FROM products WHERE id='$id'";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
	$editrow = mysql_fetch_row($result);
	$newsTitle = stripslashes($editrow[0]);
	$category = $editrow[1];
	$editor1 = stripslashes($editrow[2]);
	$imgurl = $editrow[3];
	$old_imgurl = $imgurl;
?>
<script>
function showDiv(div_id)
{
	var style_sheet = getStyleObject(div_id);
	if (style_sheet) {
		hideDiv();
		changeObjectVisibility(div_id, "visible");
	}
	else {
		alert("sorry, this only works in browsers that do Dynamic HTML");
	}
}
function getStyleObject(objectId) {
	if(document.getElementById && document.getElementById(objectId)) {
		return document.getElementById(objectId).style;
	}
	else if (document.all && document.all(objectId)) {
		return document.all(objectId).style;
	}
	else if (document.layers && document.layers[objectId]) {
		return document.layers[objectId];
	} else {
		return false;
	}
}
function changeObjectVisibility(objectId, newVisibility) {
	var styleObject = getStyleObject(objectId);
	if (styleObject) {
		styleObject.visibility = newVisibility;
		return true;
	} else {
		return false;
	}
}
function hideDiv(div_id)
{
	changeObjectVisibility(div_id,"hidden");
}
</script>
			<form method="post" action="index.php?tab=2" enctype="multipart/form-data" class="default">
				<h3>Modifica la promozione o la news selezionata</h3>
				<div class="idtable">
					<div class="idrow">
						<div class="idcell_1" style="width:75%;">
							<fieldset>
								<label for="newsTitle">Titolo</label>
								<input type="text" name="newsTitle" size="50" value="<?php echo $newsTitle; ?>" />
							</fieldset>
						</div>
						<div class="idcell_4">
							<fieldset>
								<label for="category">Categoria</label>
								<select name="category" id="category">
									<option value="01"
<?php
if ($category == '01') echo ' selected';
?>
									>Promozioni</option>
									<option value="02"
<?php
if ($category == '02') echo ' selected';
?>
									>Eventi</option>
								</select>
							</fieldset>
						</div>
					</div>
					<div class="idrow">
						<div class="idcell_1">
							<fieldset>
								<!--<label for="editor1">Versione Italiana</label>-->
								<textarea cols="80" id="editor1" name="editor1" rows="10"><?php echo $editor1; ?></textarea>
<script>
CKEDITOR.replace( 'editor1', {
	filebrowserBrowseUrl: '../../tools/kcfinder/browse.php',
	filebrowserUploadUrl : '../../tools/kcfinder/upload.php',
	filebrowserImageBrowseUrl : '../../tools/kcfinder/browse.php',
	filebrowserImageUploadUrl : '../../tools/kcfinder/upload.php',
	filebrowserWindowWidth  : 800,
	filebrowserWindowHeight : 500
});
</script>
							</fieldset>
						</div>
					</div>

					<div class="idrow">
						<div class="idcell_1">
							<fieldset>
								<div style="width:150px;float:left;">Immagine promozione/evento: </div>
								<div style="width:100px;float:left;"><img src="<?php echo $old_imgurl; ?>" alt="newsTitle"></div>
							</fieldset>
							<fieldset>
								<div style="clear:both;">Vuoi caricare una nuova immagine?
									<input type="radio" name="newimg" value="0" checked="checked" onclick="hideDiv('uploadpic');">No&emsp;
									<input type="radio" name="newimg" value="1"  onclick="showDiv('uploadpic');" >Sì
								</div>
								<div id="uploadpic" style="clear:both;visibility:hidden;">Carica un&apos;immagine differente:
									<input type="file" name="fileup"/><br/>
								</div>
							</fieldset>
						</div>
					</div>
					<div class="idrow">
						<div class="idcell_1" style="text-align: center;">
							<fieldset>
								<input type="hidden" name="id" value="<?php echo $id; ?>" /><br />
								<input type="hidden" name="old_imgurl" value="<?php $old_imgurl; ?>" /><br />
								<input type="hidden" name="edited" value="1" /><br />
							</fieldset>
							<fieldset>
								<input type="submit" name="doEdit" value="Salva Modifiche" />
							</fieldset>
						</div>
					</div>
				</div>
			</form>
<?php
}
else {
		echo "<img src=\"".LOGOURL."\" alt=\"".SITENAME."\" >";	
}
?>
		</div>
	</div>
</div>
<?php
include_once '../../footer.php';
?>
