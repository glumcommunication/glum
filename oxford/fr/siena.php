<?php
$thisPage = "Sienne et la Toscane";
$description = "Sienne est une commune de la Toscane centrale. La ville est universellement connue pour son patrimoine artistique et pour la substantielle unité stylistique de son 
ornement urbain médiéval. Elle a été déclarée patrimoine de l’humanité par l’UNESCO.";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>Sienne et la Toscane</h1>
		<img src="../images/sienapiazza.jpg" alt="Siena" />
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>Toscane</h2>
		<p>Around the eighth century before Christ, in the whole territory of Central Italy, the first pieces of evidence were found of the presence of a mysterious 
		and extraordinary people: The Etruscans.  It was from this very people that the region took the name Etruria, Tuscia for the Romans and finally Tuscania and 
		Toscana (Tuscany in English).  After the Etruscans and the Roman Empire, through Ostrogoth, Byzantine and Lombard domination, first with Dante Alighieri and 
		with Giotto in the 14th century, then in the 15th with other great artists, Tuscany, and in particular Florence gave a determining contribution to the Italian 
		Renaissance.  In the Renaissance period the Medici family predominated Florence, and after their fall, the then Grand Duchy of Tuscany passed under the dominion  
		of an Austrian family, the Lorena, liberal princes who, among other things, reclaimed a great part of the Maremma and contributed to making Tuscany of of the 
		richest regions of Italy.  In the second half of the 19th century, the Grand Duchy came under the Kingdom of Italy.</p>
		<p>Tuscany is one of the major regions for historical, artistic, cultural and geographical patrimony.  It faces the Tyrrhenian with its around 400 kilometres of 
		coast and takes in also the islands of the Tuscan archipelago, including the island of Elba.  The Tuscan territory is predominantly hilly, especially in the central-south part. 
		Tuscany is universally noted for its rich heritage of monuments and works of art:  The cities of Florence, Pisa and Siena are world famous, not to leave out the many 
		smaller towns, some of which are true historical cities, perfectly preserved in their original state, custodians of many works of art of inestimable value, of which the 
		province of Siena can boast of San Gimignano (medieval historical centre and renaissance monuments), Pienza (historical centre and ideal city of the Renaissance, with 
		pre-existing medieval remains), Montepulciano (medieval, Renaissance and Baroque monuments), Montalcino (medieval historical centre famous for its highly valued wines), 
		Chiusi (Etruscan remains, early Christian and medieval monuments) and Monteriggioni (fortified medieval village).</p>
		<h2>Sienne</h2>
		<p>Siena is a municipality of central Tuscany.  The city is universally known for its artistic patrimony and for the substantial stylistic unity of its medieval 
		urban architecture.  It has been declared a UNESCO World Heritage site.</p>
		<p>Siena was founded as a Roman colony in the time of Emperor Augustus, and took the name of Saena Julia.  The scarse yet reliable evidence of a preceeding period 
		suggests  the existence of an Etruscan community on which the Roman military colony settled during the time of Augustus.  In the 10th century, Siena found itself 
		at the centre of important commercial roadways that lead to Rome and, thanks to this, became and important medieval city.  In the 12th century the city formed 
		comunal consular ordinances, and began to expand its territory and form its first alliances.  This political and economic situation led Siena to battle with 
		Florence over the dominion of northern provinces of Tuscany.  From the first half of the 12th century, Siena prospered and became an important commercial centre, 
		holding a good relationship with the Church State; the Sienese bankers were a point of reference the authorities in Rome, to whom they gave loans or financing. 
		At the end of the 12th century Siena supported the Ghibelline cause and found herself again against Florence, which at first suffered the worst, but then the 
		Sienese lost the war at the battle of Colle Val d’Elsa, that brought in 1287 the government of the Nove (Nine).  Under this new government, Siena reached her greatest 
		splendor, both economic and cultural.  After the Black Death of 1348 , the Sienese Republic began its slow decline, that reached its epilogue in 1555, the year in 
		which the city had to surrender to the Florentine surpremacy.</p>
		<p>Siena has a long culinary tradition, thanks perhaps to the wealth of the medieval period and the presence of numerous inns and points of hospitality along the 
		Via Francigena.  The principle activities are tourism, services, agriculture and handicrafts.  In the service industry, the more important activities are those 
		linked to the Monte dei Paschi Bank of Siena.  There are also important sections of the University of Siena and of the Public Health Services, which employ 
		thousands of people and serve an extensive market of users of the large provincial territory.  Their presence is important also from a scientific and medical 
		research point of view.  Tourism is surely a compelling activity, given the fame of Siena and the mumber of tourists that it attracts.  On the 2nd of July and the 
		16th of August in the Piazza del Campo (Campo Square) in Siena the traditional Palio is held, a race where the horses are ridden without saddles by jockeys sponsored 
		by the different ‘contrade’ (quarters) of Siena, a race which monopolises the attention of the city for quite some days; this because of the fact that the Palio is 
		not exclusively an historical display or a revisitation of an old medieval game, but it is the expression of the ancient and rooted Sienese tradition.  The Palio is 
		fare from being a display that you could pass off in only a few days, but it is the fruit of an accurate and maniac organisation on the part of the inhabitants of e
		ach quarter of the city, that conducts an intense social life during the whole year.  The Palio attracts many tourists also, and is followed live by many watching on TV.</p>
		<p>The Sienese territory is magnificent in every season.  The past and the present, living nature, springs of beauty and health, traditions... 
		a land between art and nature!  A land where history mixed with legend.  The cities and villages bordering on the surrounding hills emerge intact from the 
		long ago Medieval Age.  The art  in the cathedrals, in the churches, and in the mansions, the magic harmony in the little side-streets of the city and the 
		vibrations of antique melodies in the country lanes, between fertile fields rich with olive groves and vineyards.  The cypress keeps vigil on the hills, the last 
		defender of the unpretentious Sienese landscape.  The Sienese territory is rich with natural, incontaminated panoramas, perfectly preserved medieval villages, 
		archaeological sites, cities of art like Siena, San Gimignano, Montalcino, Pienza, Montepulciano.</p>
	</div>
</div>
</div>

<?php
include("footer.php");
?>