<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META NAME="description" CONTENT="<?php echo $description ?>">
<META NAME="KEYWORDS" CONTENT="italian language schools, learn italian in italy, italian schools in tuscany, italian schools in siena italy, study italian in Italy, 
italian language courses,  italian cultural courses, language study in siena, italian language school siena, italiano courses in Tuscany, italian for foreigners in Siena, 
italian schools in Siena , italian language school, ">
<link rel="icon" href="../images/favicon_o.png" type="image/svg"/> 
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php

	if ($thisPage == "Study Holidays") {
?>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
	<script>
  		$(function() {
    		$( "#tabs" ).tabs();
  		});
  	</script>
<?php
	}
?>
<title>The New Oxford School - <? echo $thisPage ?></title>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-35054570-1']);
  _gaq.push(['_trackPageview']);

  (function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<!--<img src="images/backgroundflag.png" width="1169" height="590" alt="background" class="bg" />-->
<div id="container">
	   
            <div id="header">
                <img src="../images/logotrinity.jpg" class="logoheader trinity">
                <img src="../images/logoeda.jpg" class="logoheader eda">
                <a href="index.php"><img src="../images/logom.png" alt="Oxford School" /></a>
                <img src="../images/logoaisli.jpg" class="logoheader aisli">
                <img src="../images/logoruxum.jpg" class="logoheader ielts"> 
            </div>