<?php
$thisPage = "Study Holidays";
$description = "Scuola di inglese, francese, spagnolo, tedesco, russo, arabo, cinese, italiano per stranieri, a Siena in Toscana, Italia. Scuola di lingue straniere a Siena. Viaggi studio per famiglie, ragazzi e adulti.";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">
	<div class="sbox">
	<h1>Viaggi studio</h1>
		<img src="../images/viaggistudio.jpg" alt="Viaggi studio" />
	</div>
</div>
<div class="rightSide">
	<div id="lbox">

		<p>At the New Oxford School we ogranize study holidays to destinations
throughout the world where you can learn, improve and perfect your
English, French, German, Spanish, or Chinese.</p>
		<div id="tabs">
			<ul>
				<li style="border-left:none;"><a href="#tab1">Study holidays</a></li>
				<li><a href="#tab2">Summer camps</a></li>
				<li><a href="#tab3" style="font-size:10px;">Family summer courses</a></li>
				<li><a href="#tab4">30+ course</a></li>
				<li><a href="#tab5">50+ course</a></li>
				<li><a href="#tab6" style="font-size:10px;">Teen summer courses</a></li>
				<li><a href="#tab7">Paid work</a></li>
				<li style="border-right:none;"><a href="#tab8">Unpaid internship</a></li>
			</ul>
			<div id="tab1">
		<table class="oxtable">
			<!-- Table header -->
			<thead>
				<tr>
					<th style="width:30%;">Study holidays</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Location</td>
					<td>UK, USA, Malta, Australia, Ireland, New Zealand, Canada, Germany, Spain, France, Taiwan</td>
				</tr>
				<tr>
					<td>Ages</td>
					<td>16 and up</td>
				</tr>
				<tr>
					<td>Length</td>
					<td>From one week to one year</td>
				</tr>
				<tr>
					<td>Dates</td>
					<td>Any Monday</td>
				</tr>
				<tr>
					<td>English level</td>
					<td>From beginner to advanced</td>
				</tr>
				<tr>
					<td>Prices</td>
					<td>Starting from &euro; 125 per week</td>
				</tr>
				<tr>
					<td style="border: 0;">Course type</td>
					<td style="border: 0;">General English, Business English, Intensive English, Preparation for IELTS, TOEFL, FCE, CAE, CPE exams</td>
				</tr>
			</tbody>
		</table>
			</div>
			<div id="tab2">
		<table class="oxtable">
			<!-- Table header -->
			<thead>
				 <tr>
					 <th style="width:30%;">Summer camps with teacher</th>
					 <th>&nbsp;</th>
				 </tr>
			</thead>
			<tbody>
				<tr>
					<td>Location</td>
					<td>England</td>
				</tr>
				<tr>
					<td>Ages</td>
					<td>From 8 to 16</td>
				</tr>
				<tr>
					<td>Length</td>
					<td>From one to three weeks</td>
				</tr>
				<tr>
					<td>Dates</td>
					<td>Late July - Early August</td>
				</tr>
				<tr>
					<td>English level</td>
					<td>All</td>
				</tr>
				<tr>
					<td>Prices</td>
					<td>Starting from &euro; 1000 per week</td>
				</tr>
				<tr>
					<td style="border: 0;">Course type</td>
					<td style="border: 0;">Adventure camp for kids and teens</td>
				</tr>
			</tbody>
		</table>
			</div>
			<div id="tab3">
		<table class="oxtable">
			<!-- Table header -->
			<thead>
				 <tr>
					 <th style="width:30%;">Summer courses for families</th>
					 <th>&nbsp;</th>
				 </tr>
			</thead>
			<tbody>
				<tr>
					<td>Location</td>
					<td>UK, Malta</td>
				</tr>
				<tr>
					<td>Ages</td>
					<td>Children 5 and up with parents</td>
				</tr>
				<tr>
					<td>Length</td>
					<td>From one week to one month</td>
				</tr>
				<tr>
					<td>Dates</td>
					<td>From June to September</td>
				</tr>
				<tr>
					<td>English level</td>
					<td>All</td>
				</tr>
				<tr>
					<td>Prices</td>
					<td>Starting from &euro; 200 per week</td>
				</tr>
				<tr>
					<td style="border: 0;">Course type</td>
					<td style="border: 0;">Play and learn English for children, General English for parents</td>
				</tr>
			</tbody>
		</table>
			</div>
			<div id="tab4">
		<table class="oxtable">
			<!-- Table header -->
			<thead>
				 <tr>
					 <th style="width:30%;">30+ course</th>
					 <th>&nbsp;</th>
				 </tr>
			</thead>
			<tbody>
				<tr>
					<td>Location</td>
					<td>London</td>
				</tr>
				<tr>
					<td>Ages</td>
					<td>30 and up</td>
				</tr>
				<tr>
					<td>Length</td>
					<td>From one week to six months</td>
				</tr>
				<tr>
					<td>Dates</td>
					<td>Year-round</td>
				</tr>
				<tr>
					<td>English level</td>
					<td>From beginner to advanced</td>
				</tr>
				<tr>
					<td>Prices</td>
					<td>Starting from &euro; 250 per week</td>
				</tr>
				<tr>
					<td style="border: 0;">Course type</td>
					<td style="border: 0;">General English, Business English, Intensive English</td>
				</tr>
			</tbody>
		</table>
			</div>
			<div id="tab5">
		<table class="oxtable">
			<!-- Table header -->
			<thead>
				 <tr>
					 <th style="width:30%;">50+ course</th>
					 <th>&nbsp;</th>
				 </tr>
			</thead>
			<tbody>
				<tr>
					<td>Location</td>
					<td>UK, USA</td>
				</tr>
				<tr>
					<td>Ages</td>
					<td>50 and up</td>
				</tr>
				<tr>
					<td>Length</td>
					<td>From one week to three months</td>
				</tr>
				<tr>
					<td>Dates</td>
					<td>Year-round</td>
				</tr>
				<tr>
					<td>English level</td>
					<td>From beginner to advanced</td>
				</tr>
				<tr>
					<td>Prices</td>
					<td>Starting from &euro; 300 per week</td>
				</tr>
				<tr>
					<td style="border: 0;">Course type</td>
					<td style="border: 0;">General English</td>
				</tr>
			</tbody>
		</table>
			</div>
			<div id="tab6">
		<table class="oxtable">
			<!-- Table header -->
			<thead>
				 <tr>
					 <th style="width:30%;">Summer course for teens</th>
					 <th>&nbsp;</th>
				 </tr>
			</thead>
			<tbody>
				<tr>
					<td>Location</td>
					<td>UK</td>
				</tr>
				<tr>
					<td>Ages</td>
					<td>From 14 to 18</td>
				</tr>
				<tr>
					<td>Length</td>
					<td>From one week to one month</td>
				</tr>
				<tr>
					<td>Dates</td>
					<td>Late June - Early August</td>
				</tr>
				<tr>
					<td>English level</td>
					<td>All</td>
				</tr>
				<tr>
					<td>Prices</td>
					<td>Starting from &euro; 700 per week</td>
				</tr>
				<tr>
					<td style="border: 0;">Course type</td>
					<td style="border: 0;">Intensive English with activities and trips</td>
				</tr>
			</tbody>
		</table>
			</div>
			<div id="tab7">
		<table class="oxtable">
			<!-- Table header -->
			<thead>
				 <tr>
					 <th style="width:30%;">Paid work</th>
					 <th>&nbsp;</th>
				 </tr>
			</thead>
			<tbody>
				<tr>
					<td>Location</td>
					<td>UK, Australia</td>
				</tr>
				<tr>
					<td>Ages</td>
					<td>18 and up</td>
				</tr>
				<tr>
					<td>Length</td>
					<td>From 12 weeks to one year</td>
				</tr>
				<tr>
					<td>Dates</td>
					<td>Year-round</td>
				</tr>
				<tr>
					<td>English level</td>
					<td>Minimum Intermediate (B2)</td>
				</tr>
				<tr>
					<td>Prices</td>
					<td>Starting from &euro; 450</td>
				</tr>
				<tr>
					<td style="border: 0;">Field</td>
					<td style="border: 0;">Restaurant and Hotel</td>
				</tr>
			</tbody>
		</table>
			</div>
			<div id="tab8">
		<table class="oxtable">
			<!-- Table header -->
			<thead>
				 <tr>
					 <th style="width:30%;">Unpaid internship</th>
					 <th>&nbsp;</th>
				 </tr>
			</thead>
			<tbody>
				<tr>
					<td>Location</td>
					<td>UK, Australia</td>
				</tr>
				<tr>
					<td>Ages</td>
					<td>18 and up</td>
				</tr>
				<tr>
					<td>Length</td>
					<td>From six weeks to one year</td>
				</tr>
				<tr>
					<td>Dates</td>
					<td>Year-round</td>
				</tr>
				<tr>
					<td>English level</td>
					<td>Minimum Low Intermediate (B1)</td>
				</tr>
				<tr>
					<td>Prices</td>
					<td>Starting from &euro; 550 course fee</td>
				</tr>
				<tr>
					<td style="border: 0;">Field</td>
					<td style="border: 0;">Various</td>
				</tr>
			</tbody>
		</table>
			</div>
		</div>
		<p style="float:left;">For information please contact: <a href="mailto:jeremy@thenewoxfordschool.it" style="text-decoration:underline;">jeremy@thenewoxfordschool.it</a>
		</p>
	</div>
</div>
</div>

<?php
include("footer.php");
?>
