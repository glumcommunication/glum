<?php
$thisPage = "Home";
$description = "School of world languages and of Italian for foreigners based in Siena Tuscany Italy - The New Oxford Schol is a Italian language school to learn italian in Italy";
include("header.php");
include("menu.php");
?>

<div id="main">
<?php include("../flags_int.php"); ?>
    <div id="mainbox">
        <img src="../images/oxfordhome.jpg" alt="" />
    </div>

    <div class="rightHome">

        <div class="boxhomeRight1">
            <h1>Trinity</h1>
            <a href="http://www.trinitycollege.it/" target="_blank"><img src="../images/trinity.png" alt="Trinity" /></a>
        </div>	
        <div class="boxhomeRight2">
            <h1>Opening hours</h1>
            <div class="mtext">
                <h3>Offices</h3>
                <p>From Monday to Friday 9:00am - 8:00pm<br />
                    Saturday 09:30am - 1:00pm</p>
                <p style="color:#cf142b;font-weight:bold;">During the months of July and August, the secretariat will be closed on Saturday</p>

                <h3>Classes</h3>
                <p>From Monday to Friday 9:00am - 10:00pm<br />
                    Saturday 9:00am - 6:00pm</p>
            </div>
        </div>

    </div>

    <div class="leftHome">
        <div class="boxhomeLeft">
            <h1>Diagnostic Test</h1>
            <img src="../images/test.png" alt="" />
        </div>
    </div>
</div>



<?php
include("footer.php");
?>