<?php
$thisPage = "Gallery";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>Gallery</h1>
		<img src="images/fotogallery.jpg" alt="" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">


			<div class="mbox">
				<h1>Il team</h1>
				<a href="images/gallery/original/persone_12.jpg" rel="lightbox[persone]" title="Team">
				<img src="images/gallery/thumb/persone_thumb.jpg" alt="Il team" />
				</a>
				<div style="visibility: hidden;">				
					<a href="images/gallery/original/persone_12.jpg" rel="lightbox[persone]" title="Team"></a>					
					<a href="images/gallery/original/persone_03.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_04.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_05.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_06.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_07.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_08.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_09.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_10.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_11.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_01.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_13.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_14.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_15.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_16.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_17.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_18.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_19.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_20.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_21.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_22.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_23.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_24.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_25.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_26.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_27.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_28.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_29.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_30.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_31.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_32.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_33.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_34.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_35.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_36.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_37.jpg" rel="lightbox[persone]" title="Team"></a>
					<a href="images/gallery/original/persone_38.jpg" rel="lightbox[persone]" title="Team"></a>													
				</div>			
			</div>


			<div class="mbox">
				<h1>La scuola</h1>
				<a href="images/gallery/original/scuola_01.jpg" rel="lightbox[scuola]" title="Scuola">
				<img src="images/gallery/thumb/scuola_thumb.jpg" alt="La Scuola" />
				</a>
				<div style="visibility: hidden;">				
					<a href="images/gallery/original/scuola_02.jpg" rel="lightbox[scuola]" title="Scuola"></a>					
					<a href="images/gallery/original/scuola_03.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_04.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_05.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_06.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_07.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_08.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_09.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_10.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_11.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_12.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_13.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_14.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_15.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_16.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_17.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_18.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_19.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_20.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_21.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_22.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_23.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_24.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_25.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_26.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_27.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_28.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_29.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_30.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_31.jpg" rel="lightbox[scuola]" title="Scuola"></a>
					<a href="images/gallery/original/scuola_32.jpg" rel="lightbox[scuola]" title="Scuola"></a>
				</div>			
			</div>					



			<div class="mbox">
				<h1>I corsi</h1>
				<a href="images/gallery/original/corsi_01.jpg" rel="lightbox[Corsi]" title="Corsi">
				<img src="images/gallery/thumb/corsi_thumb.jpg" alt="I corsi" />			
				<div style="visibility: hidden;">				
					<a href="images/gallery/original/corsi_02.jpg" rel="lightbox[Corsi]" title="Corsi"></a>					
					<a href="images/gallery/original/corsi_03.jpg" rel="lightbox[Corsi]" title="Corsi"></a>
					<a href="images/gallery/original/corsi_04.jpg" rel="lightbox[Corsi]" title="Corsi"></a>
					<a href="images/gallery/original/corsi_05.jpg" rel="lightbox[Corsi]" title="Corsi"></a>
					<a href="images/gallery/original/corsi_06.jpg" rel="lightbox[Corsi]" title="Corsi"></a>
					<a href="images/gallery/original/corsi_07.jpg" rel="lightbox[Corsi]" title="Corsi"></a>
					<a href="images/gallery/original/corsi_08.jpg" rel="lightbox[Corsi]" title="Corsi"></a>
				</div>
			</div>


			<div class="mbox">
				<h1>I corsi per bambini</h1>
				<a href="images/gallery/original/corsi_bambini_01.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini">
				<img src="images/gallery/thumb/bambini_thumb.jpg" alt="I corsi per bambini" />
				</a>
				<div style="visibility: hidden;">
					<a href="images/gallery/original/corsi_bambini_02.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_03.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_04.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_05.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_06.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_07.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_08.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_09.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_10.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_11.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_12.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_13.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_14.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_15.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_16.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_17.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_18.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_19.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_20.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_21.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_22.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_23.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_24.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_25.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_26.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_27.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_28.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_29.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_30.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_31.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_32.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_33.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_34.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_35.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_36.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>
					<a href="images/gallery/original/corsi_bambini_37.jpg" rel="lightbox[corsibambini]" title="Corsi per bambini"></a>						
				</div>				
			</div>


			<div class="mbox">
				<h1>English Happy Hour 2012</h1>
				<a href="images/gallery/original/happyhour2012_01.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012">				
				<img src="images/gallery/thumb/happyhour2012_thumb.jpg" alt="English Happy Hour" />

				<div style="visibility: hidden;">				
					<a href="images/gallery/original/happyhour2012_02.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>					
					<a href="images/gallery/original/happyhour2012_03.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_04.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_05.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_06.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_07.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_08.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_09.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
<!--					<a href="images/gallery/original/happyhour2012_10.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_11.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_12.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>-->
					<a href="images/gallery/original/happyhour2012_13.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_14.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_15.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_16.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_17.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_18.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_19.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_20.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_21.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_22.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_23.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_24.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_25.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_26.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_27.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_28.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_29.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_30.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_31.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_32.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_33.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_34.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_35.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_36.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_37.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_38.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_39.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>					
					<a href="images/gallery/original/happyhour2012_40.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_41.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_42.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_43.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_44.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_45.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_46.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_47.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_48.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_49.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_50.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_51.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_52.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_53.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_54.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_55.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_56.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_57.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_58.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>
					<a href="images/gallery/original/happyhour2012_59.jpg" rel="lightbox[happyhour2012]" title="English Happy Hour 2012"></a>																		
				</div>			
			</div>	
            <div class="mbox">
				<h1>Summer Club 2014 </h1>
				<a href="images/gallery/original/summercamp_1.jpg" rel="lightbox[summercamp]" title="Summer Club 2014">				
				<img src="images/gallery/thumb/summercamp_thumb.jpg" alt="Summer Camp 2014" />

				<div style="visibility: hidden;">				
					<a href="images/gallery/original/summercamp_2.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>					
					<a href="images/gallery/original/summercamp_3.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_4.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_5.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_6.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_7.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_8.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_9.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
<!--					<a href="images/gallery/original/summercamp_10.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_11.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_12.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>-->
					<a href="images/gallery/original/summercamp_13.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_14.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_15.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_16.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_17.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_18.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_19.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_20.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_21.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_22.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_23.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_24.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_25.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_26.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_27.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_28.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_29.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_30.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_31.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_32.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_33.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_34.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_35.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_36.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_37.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_38.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_39.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>					
					<a href="images/gallery/original/summercamp_40.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_41.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_42.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_43.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_44.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_45.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_46.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_47.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_48.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_49.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_50.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_51.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_52.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_53.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_54.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_55.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_56.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_57.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_58.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_59.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>	
                                        <a href="images/gallery/original/summercamp_60.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_61.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>	
                                        <a href="images/gallery/original/summercamp_62.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>
					<a href="images/gallery/original/summercamp_63.jpg" rel="lightbox[summercamp]" title="Summer Club 2014"></a>	
				</div>			
			</div>
			</div>				
			</div>


</div>


<?php
include("footer.php");
?>