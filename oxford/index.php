<?php
$thisPage = "Home";
$description = "Scuola di inglese, francese, spagnolo, tedesco, russo, arabo, cinese, italiano per stranieri, a Siena in Toscana, Italia. Scuola di lingue straniere a Siena.";
include("header.php");
include("menu.php");
include('db.php');
?>

<div id="main">
    <?php include("flags.php"); ?>
    <div id="mainboxa">
            <!--<img src="images/oxfordhome.jpg" alt="" />-->
        <div class="flexslider">
            <ul class="slides">
             
                  <li>
                    <img src="images/slide2.jpg" />
                </li>
             
                <li>
                    <img src="images/oxfordhome2.jpg" />
                </li>
                <li>
                    <a href="aziende.php">
                        <img src="images/oxfordhome4.jpg" />
                    </a>
                </li>

            </ul>
        </div>
    </div>

    <div class="rightHome">
        <!--<div class="lrow">-->

        <div class="boxhomeRight1 rosso">
            <h1>News ed Eventi</h1>
            <div id="scrollbar1">

                <?php
                $db_server = mysql_connect($db_hostname, $db_username, $db_password);

                if (!$db_server)
                    die("Unable to connect to MySQL: " . mysql_error());

                mysql_select_db($db_database, $db_server)
                        or die("Unable to select database: " . mysql_error());
                $sql = "SELECT * FROM news ORDER BY Data DESC,Ora DESC LIMIT 3";
                $result = mysql_query($sql);
                if (!$result)
                    die("Database access failed: " . mysql_error());
                $rows = mysql_num_rows($result);
                for ($j = 0; $j < $rows; ++$j) {
                    $row = mysql_fetch_row($result);
                    $newsTitle = stripslashes($row[0]);
                    $elm1 = stripslashes($row[1]);
                    $datasql = "SELECT DATE_FORMAT(Data, '%d/%m/%Y')  as data FROM news WHERE id='$row[5]'";

                    $rowdata = mysql_fetch_assoc(mysql_query($datasql));


                    echo <<<_END
		<div class="mtext">
		<h3>$newsTitle</h3>
		<h5>$rowdata[data], $row[3]</h5>
		$elm1

		</div>
_END;
                }
                ?>
            </div>
            <div class="more"><a href="news.php" title="News">Altre news...</a>
            </div>
        </div>
        <div class="boxhomeRight2 giallo">
            <h1>Orari</h1>
            <div class="mtext">
                <h3>Orari di segreteria di luglio e agosto</h3>
                <p>Dal lunedì al venerdì dalle 9,00 alle 20,00<br /><br/>	
                    DAL 28 LUGLIO al 29 AGOSTO
                <p>Dal lunedì al venerdì orario 9-13 / 15-19<br /></p>
                <p style="font-weight: bold;">Nei mesi di luglio e agosto la segreteria rimarr&agrave; chiusa il sabato.</p>
                <!--Il sabato <strong>dalle 9,30 ale 13,00</strong></p>-->
                <h3>Orari delle lezioni</h3>
                <p>Dal lunedì al venerdì dalle 9,00 alle 22,00
                </p>
                <!--<h3 style="font-size: 12px;">La scuola resterà aperta anche nei mesi di giugno, luglio e agosto osservando i soliti orari, 
                a luglio e agosto il sabato resteremo chiusi.</h3>-->
            </div>
        </div>
        <!--	</div>-->
        <!--	<div class="lrow">-->
        <div class="boxhomeRight1 verde">
            <h1>Aisli</h1>
            <a href="http://www.aisli.it/" target="_blank"><img src="http://www.aisli.it/images/logo.png" style="width: auto !important" alt="Aisli" /></a>
        </div>
        <div class="boxhomeRight2 blu">
            <h1>EQA</h1>
            <a href="http://www.eqanetwork.com/" target="_blank"><img src="images/eqacert.png" style="border: 0; width:auto; max-width: 250px; max-height: 150px" alt="eqa" /></a>

            </div>
        <!--		</div>-->
    </div>
    <div class="leftHome">
        <div class="boxhomeLeft blu">
            <h1>Diagnostic Test</h1>
            <div class="frameBox">
                <a href="test.php" title="Diagnostic Test"><img src="images/test.png" alt="Diagnostic Test" /></a></div>
        </div>
        <div class="boxhomeLeft nero" style="background-color: transparent;">
            <h1>Trinity</h1>
            <a href="http://www.trinitycollege.it/" target="_blank"><img src="images/trinity.png" alt="Trinity" /></a>        <div class="frameBox">
            </div>
        </div>
    </div>
    <div class="rightHome">
        <!--<div class="lrow">-->

        <div class="boxhomeRight1 rosso">
            <h1>Viaggi Studio</h1>
            <a href="http://thenewoxfordschool.it/viaggistudio.php"><img height="200px" src="images/viaggistudio.png" ></a>
            </div>
        <div class="boxhomeRight2 nero">
           <h1>Offerte</h1>
	<div id="scrollbar2">
		<div class="mtext">
		<h3>Le nostre convenzioni</h3>
		<h5>11/10/2012, 13:01:32</h5>
		<p style="text-align: justify;">Convenzione CRAL MPS</p>
<p style="text-align: justify;">Convenzione CRAS NOVARTIS</p>
<p style="text-align: justify;">Convenzione con forze armate</p>

		</div></div>
<div class="more"><a href="offerte.php" title="Offerte">Altre offerte...</a>
            </div>
        </div>
        <!--	</div>-->
        <!--	<div class="lrow">-->
        
        <!--		</div>-->
    </div>
    <div class="leftHome">
        <div class="boxhomeLeft giallo">
         
           <h1>In evidenza</h1>
            <div class="frameBox">
                <img src="images/in-evidenza.jpg" height="200px" alt="Certificazione Internazionale" /></div>
        </div>
        
    </div>
</div>




<?php
include("footer.php");
?>
