<?php
$thisPage = "Italienischkurse ";
$description = "Unsere Italienischkurse, ausgearbeitet für Sie, ob Sie Italienisch lernen, oder Ihr gelerntes Italienisch verbessern wollen. 
Wir bieten Ihnen qualifiziertes und erfahrenes Lehrpersonal.";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>Italienischkurse </h1>
		<img src="../images/english-student.jpg" alt="" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>The New Oxford School ITALIENISCHKURSE</h2>

		<p>Unsere Italienischkurse, ausgearbeitet für Sie, ob Sie Italienisch lernen, oder Ihr gelerntes Italienisch verbessern wollen. 
		Wir bieten Ihnen qualifiziertes und erfahrenes Lehrpersonal. Nach einem Eingangstest werden Sie in eine Gruppe von 3 - 8 Teilnehmern, 
		eingeteilt. Der Zeitplan wird auf die Bedürfnisse der Teilnehmer abgestimmt, jeweils von Montag bis Samstag vormittags und nachmittags.<br />
		Die Kursniveaus haben 6 verschiedene Stufen: (Beginner - Elementary - Intermediate 1- Intermediate 2 - Advanced - Proficiency) und lassen 
		viel Platz für Konversation.</p>
		 
		<h3>ITALIENISCHKURSE ALLGEMEINE INFORMATIONEN</h3>
		
		<p><strong>LÄNGE DER LEKTIONEN</strong><br />
		Bitte beachten Sie, dass die Länge der Lektionen 60 Minuten ist.</p>
		
		<p><strong>ANZAHL DER STUDENTEN PER KURS</strong><br />
		Für Minigruppen, Teilnehmerzahl von 3 - 8 </p>
		
		<p><strong>KURSEINTEILUNGEN
		Nach dem Eingangstest, werden die Studenten in einen der folgenden Kurse eingeteilt:</strong>
		<ul class="oxlist">
		<li>Beginner</li>
		<li>Elementary</li>
		<li>Intermediate 1</li>
		<li>Intermediate 2</li>
		<li>Advanced</li>
		<li>Superior</li>
		</ul></p>
		<p>Unser Programm basiert auf den Anforderunden des “Common European Framework“.</p>
		
		<p><strong>DIE KURSKOSTEN BEINHALTEN:</strong>
		<ul class="oxlist">
		<li>alle Kursmaterialien</li>
		<li>Bescheinigung über die Teilnahme</li>
		<li>weitere soziale Aktivitäten</li>
		</ul></p>
		
		<p><strong>WEITERE SERVICES:</strong>
		<ul class="oxlist">
		<li>Intenetzugang</li>
		<li>Filmvorführung in Italienischer Sprache</li>
		</ul></p>

		<p><strong>MINIGRUPPEN-KURSE</strong></p>
		<div class="redp">Standardkurs</div>

		<p><strong>Beginn</strong>: jeden Montag<br /> 
		<strong>Dauer</strong>: mindestens 2 Wochen<br />
		<strong>Lektionen</strong>: drei pro Tag = 15 Stunden pro Woche<br />
		<strong>Niveau</strong>: alle Niveaus<br /> 
		<strong>Klassengrösse</strong>: von 3 - 8 Teilnehmer<br />
		<span class="redp"><a href="../pdf/price_list.pdf" title="Download">PREISLISTE</a>&emsp;<a href="booking_form.php" title="Booking">BUCHEN SIE JEZT!</a></span></p>
		<p>Der Standardkurs beinhaltet 15 Gruppenstunden pro Woche, das ist die Mindeststundenzahl bei Gruppenkursen. Diese Gruppenkurse haben eine Mindestdauer von 2 Wochen. 
		Die Lektionen finden von 10:00 - 13:30 Uhr statt, ideal für Studenten die neben den Kursen viel freie Zeit wollen.</p>
		
		<div class="redp">Intensivkurse</div>
		<p><strong>Beginn</strong>: jeden Montag<br />
		<strong>Dauer</strong>: mindestens 1 Wochen<br />
		<strong>Lektionen</strong>: vier pro Tag = 20 Stunden pro Woche<br />
		<strong>Niveau</strong>: alle Niveaus<br />
		<strong>Klassengrösse</strong>: von 3 - 8 Teilnehmer<br />
		<span class="redp"><a href="../pdf/price_list.pdf" title="Download">PREISLISTE</a>&emsp;<a href="booking_form.php" title="Booking">BUCHEN SIE JEZT!</a></span></p>
		<p>Der Intensivkurs besteht aus 20 Gruppenstunden pro Woche. Maximale Teilnehmerzahl ist 8 Studenten. Die Lektionen beginnen um 9:00 Uhr und enden um 13:00 Uhr.</p>

		<p><strong>INDIVIDUALKURSE</strong></p>
		<p><strong>Beginn</strong>: jeden Tag<br />
		<strong>Dauer</strong>: mindestens 3 Stunden<br />
		<strong>Niveau</strong>: alle Niveaus<br />
		<strong>Klassengrösse</strong>: 1 Teilnehmer <br />
		<span class="redp"><a href="../pdf/price_list.pdf" title="Download">PREISLISTE</a>&emsp;<a href="booking_form.php" title="Booking">BUCHEN SIE JEZT!</a></span></p>
		<p>Unsere Individualkurse sind angepasst auf die spezifischen Bedürfnisse des Studenten. Der Inhalt der jeweiligen Lektionen kann individuell auf den Studenten 
		ausgerichtet werden, sodass der Student in einer entspannten Atmosphäre und mit uneingeschränkter Aufmerksamkeit durch den Lehrer studieren kann.</p>
		
		<p><strong>KOMBINIERTE KURSE</strong></p>
		<p><strong>Beginn</strong>: jeden Montag<br />
		<strong>Dauer</strong>: mindestens 1 Woche<br />
		<strong>Gruppenkurse</strong>: drei pro Tag = 15 pro Woche<br />
		<strong>Gruppenkurse Teilnehmer</strong>: von 3 - 8 Teilnehmer<br />
		<strong>Individualkurse</strong>: vier pro Woche<br />
		<strong>Niveau</strong>: alle Niveaus<br />
		<span class="redp"><a href="../pdf/price_list.pdf" title="Download">PREISLISTE</a>&emsp;<a href="booking_form.php" title="Booking">BUCHEN SIE JEZT!</a></span></p>
		<p>Die kombinierten Kurse dauern mindestens 1 Woche. Kombinierte Kurse beinhalten vormittags einen Gruppenkurs und Nachmittags (13:00-17:00 Uhr) einen Einzelkurs. 
		Dieses Angebot bietet die Möglichkeit, morgens in der Gruppe das allgemeine Italienisch auszubauen und nachmittags im Einzelkurs, spezielle Inhalte zu bearbeiten. 
		Sodass auch bei kurzem Aufenthalt in Siena ein guter Lernerfolg gewährleistet ist.</p>

		<p><strong>DREIMONATSKURSE</strong></p>
		<p><strong>Beginn</strong>: jeden Montag<br />
		<strong>Dauer</strong>: 12 Wochen<br />
		<strong>Lektionen</strong>: drei pro Tag = 15 Stunden pro Woche<br />
		<strong>Niveaus</strong>: alle Niveaus<br />
		<strong>Klassengrösse</strong>:  von 3 - 8 Teilnehmern<br /> 
		<span class="redp"><a href="../pdf/price_list.pdf" title="Download">PREISLISTE</a>&emsp;<a href="booking_form.php" title="Booking">BUCHEN SIE JEZT!</a></span></p>
		<p>Die Dreimonatskurse dauern 12 Wochen und sind für diejenigen Studenten die sich ganz in die Italienische Sprache “stürzen“ möchten mit intensivem Sprechen / Zuhören 
		und intensivem Lesen / Schreiben.<br />
		Unsere Italienischkurse, ausgearbeitet für Sie, ob Sie Italienisch lernen, oder Ihr gelerntes Italienisch  verbessern wollen. Wir bieten Ihnen qualifiziertes und 
		erfahrenes Lehrpersonal. Nach einem Eingangstest werden Sie in eine Gruppe von 3 - 8 Teilnehmern, eingeteilt. Der Zeitplan wird auf die Bedürfnisse der Teilnehmer 
		abgestimmt, jeweils von Montag bis Samstag vormittags und nachmittags. Die Kursniveaus haben 6 verschiedene Stufen: (Beginner - Elementary - Intermediate 1- 
		Intermediate 2 - Advanced - Proficiency) und lassen viel Platz für Konversation.</p>

		<p class="redp"><a href="../pdf/Metodi_pagamento.pdf" title="Download">
		<span><img src="../images/pdfdownload.png" width="32" height="32" alt="Pdf" style="height:32px;width:32px;vertical-align: middle;" /></span>
		Zahlungsmethoden</a></p>
		<p class="redp"><a href="../pdf/terms.pdf" title="Download">
		<span><img src="../images/pdfdownload.png" width="32" height="32" alt="Pdf" style="height:32px;width:32px;vertical-align: middle;" /></span>
		Bedingungen</a></p>
		<p class="redp"><a href="../pdf/Request_information.pdf" title="Download"> 
		<span><img src="../images/pdfdownload.png" width="32" height="32" alt="Pdf" style="height:32px;width:32px;vertical-align: middle;" /></span>
		Anforderung von Infomaterial</a></p>		
	</div>
</div>
</div>

<?php
include("footer.php");
?>