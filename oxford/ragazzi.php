<?php
$thisPage = "Corsi per Ragazzi";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
<div class="sbox">
	<h1>Corsi per Ragazzi</h1>
		<img src="images/english-children.jpg" alt="" />			
	</div>
</div>
<div class="rightSide">	
	<div id="lbox">
		<h2>DESCRIZIONE DEL CORSO</h2>
		<p>Corsi di lingua inglese, francese, spagnola e tedesca: questi sono i corsi piu’ 
		richiesti per bambini dai 3 anni fino ai ragazzi di 18 anni.<br />
		La metodologia d’insegnamento rimane la stessa di quella dei corsi per adulti, cioè 
		basata sulla conversazione, ma si differenzia in base alle necessità di tutte le  fasce d’età.</p>
		<h2>Corsi di lingua Inglese</h2>
		<h3>Play Group</h3>
		<p>Corso dedicato a bambini in età pre-scolare (dai 3 ai 5 anni).<br />
		Insegnanti qualificati e con esperienza nell’insegnamento ai bambini, parlando in lingua madre, 
		con l’ausilio di letture, giochi, canzoni insegnano i primi vocaboli dell’inglese e avvicinano i 
		bambini ai suoni nuovi della lingua straniera.</p>
		<h3>Corsi per bambini</h3>
		<p>Corsi dedicati a bambini in età scolare  (dai 6 agli 13 anni).<br />
		Caratteristica fondamentale di questi corsi dedicati ai piu’ piccoli è l’insegnamento della lingua 
		straniera attraverso il gioco, in modo naturale e spontaneo, divertendosi e appassionandosi al nuovo 
		modo di comunicare in modo spontaneo.<br />
		Impareranno velocemente e senza alcun sacrificio grazie al metodo e grazie agli insegnanti che 
		riescono già dopo poche lezioni a far amare loro la nuova lingua.
		La Scuola possiede un’aula interamente dedicata a loro con giochi, filmati, cartoni animati, 
		CD con canzoni, storie e grazie alla fantasia e alla creatività dei nostri insegnanti e all’atmosfera 
		rassicurante e divertente il bambino apprende velocemente senza alcun sacrificio.<br />
		Per la lingua inglese, i programmi didattici sono il linea con i programmi grammaticali del Trinity 
		College di Londra, dopo un breve test con l’insegnante il bambino o il ragazzo viene inserito in un 
		gruppo di studio di massimo 6 partecipanti.<br />
		I corsi hanno una durata di 50 ore durante le quali svolgono un intero programma grammaticale del 
		loro livello.<br />
		La frequenza per loro è di una lezione a settimana di due ore ciascuna.<br />
		Il materiale didattico scelto tra le migliori case editrici di testi per bambini viene fornito dalla Scuola.<br />
		A fine corso una breve verifica e il rilascio dell’attestato di frequenza da parte della scuola. 
		A partire dal settimo anno di età, inoltre, la possibilità di sostenere un esame orale con un esaminatore del 
		Trinity College London e quindi, in caso di esito positivo, il rilascio di una certificazione internazionale, 
		valida anche per i crediti scolastici.</p>
		<h3>Livelli</h3>

		<p>
			I nostri corsi di lingua inglese sono modulati sull’acquisizione delle nozioni grammaticali e lessicali e
			sullo sviluppo progressivo delle abilità comunicative del parlare, capire, leggere e scrivere,  in linea
			con la progressione dei livelli del Quadro Comune di Riferimento Europeo. A vari livelli è possibile
			sostenere gli esami degli Enti certificatori riconosciuti dal MIUR, Trinity College London e Cambridge English.
		</p>
		<table id="oxtable">
		<!-- Table header -->
			<thead>
				<tr>
					<th class="lfive">The New Oxford School</th>
					<th class="lfive">Numero ore</th>
					<th class="lfive">QCER</th>
					<th class="lfive">Cambridge English</th>
					<th class="lfive">Trinity College</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Grade 1</td>
					<td>50</td>
					<td></td>
					<td></td>
					<td>GESE 1</td>
				</tr>
				<tr>
					<td>Grade 2</td>
					<td>50</td>
					<td></td>
					<td></td>
					<td>GESE 2</td>
				</tr>
				<tr>
					<td>Grade 3</td>
					<td>50</td>
					<td></td>
					<td></td>
					<td>GESE 3</td>
				</tr>
				<tr>
					<td>Grade 4</td>
					<td>50 + 50</td>
					<td>A2</td>
					<td>KET</td>
					<td>GESE 4 - ISE 0</td>
				</tr>
				<tr>
					<td>Grade 5</td>
					<td>50</td>
					<td></td>
					<td></td>
					<td>GESE 5</td>
				</tr>
				<tr>
					<td style="border: 0;">Grade 6</td>
					<td style="border: 0;">50 + 50</td>
					<td style="border: 0;">B1</td>
					<td style="border: 0;">PET</td>
					<td style="border: 0;">GESE 6 - ISE I</td>
				</tr>
			</tbody>
		</table>
		<h3>Corsi per adolescenti</h3>
		<p>Corsi dedicati ai ragazzi dai 14 ai 18 anni che frequentano le scuole superiori.<br />
		Anche per loro corsi in linea con i programmi grammaticali del Trinity College London e, oltre al 
		materiale didattico, tante attività, films, canzoni adatte alla fascia d’età.<br />
		A fine corso anche per loro un attestato di frequenza e possibilità di sostenere un esame in sede 
		con un esaminatore del Trinty College London.</p>
		<h3>Recupero scolastico</h3>
		<p>The New Oxford School organizza programmi di studio personalizzati per bambini di scuole elementari, 
		medie e superiori che incontrano difficoltà con la lingua inglese o altre lingue (tedesco, spagnolo, 
		francese) durante l’anno scolastico sia nella grammatica che nella letteratura.<br />
		In seguito ad un test scritto e orale con il nostro referente didattico, viene studiato un piano di 
		recupero per aiutare il bambino o il ragazzo a colmare le lacune  e a superare le difficoltà che 
		incontra con la lingua straniera.</p>

		<h3>METODI DI PAGAMENTO</h3>
		<p>I pagamenti di tutti i nostri corsi di lingue, sia per adulti che per bambini, possono avvenire in
			un'unica soluzione o dilazionati in pi&ugrave; mesi.</p>
	</div>
</div>
</div>
<?php
include("footer.php");
?>
