<?php
$thisPage = "Viaggi studio";
$description = "Scuola di inglese, francese, spagnolo, tedesco, russo, arabo, cinese, italiano per stranieri, a Siena in Toscana, Italia. Scuola di lingue straniere a Siena. Viaggi studio per famiglie, ragazzi e adulti.";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>Viaggi studio</h1>
		<img src="images/viaggistudio.jpg" alt="Viaggi studio" />
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
	
		<p>Alla New Oxford School organizziamo viaggi studio all'estero al fine di apprendere o perfezionare le 
		lingue straniere principalmente per la lingua inglese ma anche per il francese, il tedesco, lo spagnolo e il cinese.</p>
		
		<!--<p>Di seguito le diverse possibilità:</p>
		
		<h3>Viaggi studio per adulti (dai 18 anni in su):</h3> 
		<p>Il classico viaggio studio all'estero. In qualsiasi giorno dell’anno potrai partire per fare un'esperienza 
		che sicuramente segnerà la tua vita.<br>
		Offriamo un’ampia varietà di corsi  fra cui General English, Business English, Corsi di preparazione per 
		esami Cambridge, IELTS o TOEFL, ed anche corsi più particolari come, per esempio, inglese e vela a San Diego. 
		Le località possibili sono Gran Bretagna, Stati Uniti, Canada, Malta, Irlanda, Australia, e Nuova Zelanda.
		</p>
		<h3>Viaggi con accompagnatore ad un campo estivo per ragazzi (dai 7 anni in su):</h3>  
		<p>Ogni estate i nostri insegnanti accompagnano un gruppo di ragazzi ad un campo estivo, un Adventure Camp 
		in Inghilterra. Si parte da un aeroporto toscano per arrivare a Londra dove incontriamo lo staff del campo 
		estivo prescelto che ci accompagana alla sede.<br>
		I ragazzi vivranno in un ambiente inglese insieme ad altri ragazzi coetanei che provengono da tutta Europa 
		e anche dal resto del mondo. Le giornate sono piene d’attività di tutti tipi fra cui sport, giochi di squadra, 
		scherma, tiro con l’arco, zip wire, Go Kart, Quad, laser, arrampicamento, discoteche, film in inglese. Sono 
		previste ogni settimana anche delle gite presso città o attrazioni vicine.</p>
		
		<h3>Viaggi studio per famiglie (con bambini dai 5 anni in su):</h3>  
		<p>Una soluzione interessante per unire l’utile al dilettevole: le famiglie con bambini piccoli, dai 5 anni 
		in su vengono ospitate a casa da una famiglia del luogo, in un appartamento o in albergo vicino alla scuola 
		che frequentano. Ci sono vari tipi di corsi tra cui quello più scelto che  prevede lezioni di mattina sia per 
		i genitori che per i figli così da avere i pomeriggi liberi per godersi la vacanza<br>
		Questa possibilità è disponibile da giugno ad agosto in Inghilterra e a Malta.</p>-->
		<div id="tabs">
			<ul>
				<li style="border-left:none;"><a href="#tab1">Viaggi per Adulti</a></li>
				<li><a href="#tab2">Viaggi per ragazzi</a></li>
				<li><a href="#tab3">Viaggi per famiglie</a></li>
				<li><a href="#tab4">Corso 30+</a></li>
				<li><a href="#tab5">Corso 50+</a></li>
				<li><a href="#tab6">College</a></li>
				<li><a href="#tab7">Lavoro retribuito</a></li>
				<li style="border-right:none;"><a href="#tab8">Stage professionale</a></li>
			</ul>
			<div id="tab1">
		<table class="oxtable">
			<!-- Table header -->
			<thead>
				<tr>
					<th style="width:30%;">Viaggi Studio per Adulti</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Luogo</td>
					<td>Inghilterra, Stati Uniti, Malta, Australia, Irlanda, Nuova Zelanda, Canada, Germania, Spagna, Francia, Taiwan</td>
				</tr>
				<tr>
					<td>Et&agrave;</td>
					<td>Dai 16 anni in su</td>
				</tr>
				<tr>
					<td>Durata</td>
					<td>Da una settimana a un anno</td>
				</tr>
				<tr>
					<td>Data</td>
					<td>Qualsiasi luned&igrave; dell'anno</td>
				</tr>
				<tr>
					<td>Livello</td>
					<td>Da principiante all'avanzato</td>
				</tr>
				<tr>
					<td>Costi</td>
					<td>Programmi a partire da &euro; 125 a settimana</td>
				</tr>
				<tr>
					<td style="border: 0;">Tipo di corsi</td>
					<td style="border: 0;">General English, Business English, Intensive English, Preparazione per esami IELTS, TOEFL, FCE, CAE; CPE </td>
				</tr>
			</tbody>
		</table>
			</div>
			<div id="tab2">
		<table class="oxtable">
			<!-- Table header -->
			<thead>
				 <tr>
					 <th style="width:30%;">Viaggi per ragazzi (con accompagnatore)</th>
					 <th>&nbsp;</th>
				 </tr>
			</thead>
			<tbody>
				<tr>
					<td>Luogo</td>
					<td>Inghilterra</td>
				</tr>
				<tr>
					<td>Et&agrave;</td>
					<td>Dagli 8 ai 16 anni</td>
				</tr>
				<tr>
					<td>Durata</td>
					<td>Da una a tre settimane</td>
				</tr>
				<tr>
					<td>Data</td>
					<td>Fine luglio inizio agosto 2014</td>
				</tr>
				<tr>
					<td>Livello</td>
					<td>Tutti</td>
				</tr>
				<tr>
					<td>Costi</td>
					<td>A partire da &euro; 1000 a settimana</td>
				</tr>
				<tr>
					<td style="border: 0;">Tipo di corsi</td>
					<td style="border: 0;">Campe avventura per ragazzi</td>
				</tr>
			</tbody>
		</table>
			</div>
			<div id="tab3">
		<table class="oxtable">
			<!-- Table header -->
			<thead>
				 <tr>
					 <th style="width:30%;">Viaggi per famiglie (con accompagnatore)</th>
					 <th>&nbsp;</th>
				 </tr>
			</thead>
			<tbody>
				<tr>
					<td>Luogo</td>
					<td>Inghilterra, Malta</td>
				</tr>
				<tr>
					<td>Et&agrave;</td>
					<td>Bambini dai 5 anni in su con i genitori</td>
				</tr>
				<tr>
					<td>Durata</td>
					<td>Da una settimana a tre mesi</td>
				</tr>
				<tr>
					<td>Data</td>
					<td>Da giugno a settembre</td>
				</tr>
				<tr>
					<td>Livello</td>
					<td>Tutti</td>
				</tr>
				<tr>
					<td>Costi</td>
					<td>A partire da &euro; 200 a settimana</td>
				</tr>
				<tr>
					<td style="border: 0;">Tipo di corsi</td>
					<td style="border: 0;">Corso basato sui giochi per bambini e corso di General English per adulti</td>
				</tr>
			</tbody>
		</table>
			</div>
			<div id="tab4">
		<table class="oxtable">
			<!-- Table header -->
			<thead>
				 <tr>
					 <th style="width:30%;">Corso 30+</th>
					 <th>&nbsp;</th>
				 </tr>
			</thead>
			<tbody>
				<tr>
					<td>Luogo</td>
					<td>Londra</td>
				</tr>
				<tr>
					<td>Et&agrave;</td>
					<td>Dai 30 in su</td>
				</tr>
				<tr>
					<td>Durata</td>
					<td>Da una settimana a sei mesi</td>
				</tr>
				<tr>
					<td>Data</td>
					<td>Tutto l'anno</td>
				</tr>
				<tr>
					<td>Livello</td>
					<td>Da principiante all'avanzato</td>
				</tr>
				<tr>
					<td>Costi</td>
					<td>A partire da &euro; 250 a settimana</td>
				</tr>
				<tr>
					<td style="border: 0;">Tipo di corsi</td>
					<td style="border: 0;">General English, Business English, Intensive English</td>
				</tr>
			</tbody>
		</table>
			</div>
			<div id="tab5">
		<table class="oxtable">
			<!-- Table header -->
			<thead>
				 <tr>
					 <th style="width:30%;">Corso 50+</th>
					 <th>&nbsp;</th>
				 </tr>
			</thead>
			<tbody>
				<tr>
					<td>Luogo</td>
					<td>Inghilterra, Stati Uniti</td>
				</tr>
				<tr>
					<td>Et&agrave;</td>
					<td>Dai 50 anni in su</td>
				</tr>
				<tr>
					<td>Durata</td>
					<td>Da una settimana a tre mesi</td>
				</tr>
				<tr>
					<td>Data</td>
					<td>Tutto l'anno</td>
				</tr>
				<tr>
					<td>Livello</td>
					<td>Da principiante all'avanzato</td>
				</tr>
				<tr>
					<td>Costi</td>
					<td>A partire da &euro; 300 a settimana</td>
				</tr>
				<tr>
					<td style="border: 0;">Tipo di corsi</td>
					<td style="border: 0;">General English</td>
				</tr>
			</tbody>
		</table>
			</div>
			<div id="tab6">
		<table class="oxtable">
			<!-- Table header -->
			<thead>
				 <tr>
					 <th style="width:30%;">College</th>
					 <th>&nbsp;</th>
				 </tr>
			</thead>
			<tbody>
				<tr>
					<td>Luogo</td>
					<td>Inghilterra</td>
				</tr>
				<tr>
					<td>Et&agrave;</td>
					<td>Dai 14 ai 18 anni</td>
				</tr>
				<tr>
					<td>Durata</td>
					<td>Da una settimana a un mese</td>
				</tr>
				<tr>
					<td>Data</td>
					<td>Da fine giugno ad inizio agosto</td>
				</tr>
				<tr>
					<td>Livello</td>
					<td>Tutti</td>
				</tr>
				<tr>
					<td>Costi</td>
					<td>A partire da &euro; 700 a settimana</td>
				</tr>
				<tr>
					<td style="border: 0;">Tipo di corsi</td>
					<td style="border: 0;">Intensive English con gite ed attivit&agrave;</td>
				</tr>
			</tbody>
		</table>
			</div>
			<div id="tab7">
		<table class="oxtable">
			<!-- Table header -->
			<thead>
				 <tr>
					 <th style="width:30%;">Lavoro retribuito</th>
					 <th>&nbsp;</th>
				 </tr>
			</thead>
			<tbody>
				<tr>
					<td>Luogo</td>
					<td>Inghilterra, Australia</td>
				</tr>
				<tr>
					<td>Et&agrave;</td>
					<td>Dai 18 anni in su</td>
				</tr>
				<tr>
					<td>Durata</td>
					<td>Da 12 settimane ad un anno</td>
				</tr>
				<tr>
					<td>Data</td>
					<td>Tutto l'anno</td>
				</tr>
				<tr>
					<td>Livello</td>
					<td>Minimo Intermediate (B2)</td>
				</tr>
				<tr>
					<td>Costi</td>
					<td>Circa &euro; 450 in totale</td>
				</tr>
				<tr>
					<td style="border: 0;">Settore</td>
					<td style="border: 0;">Ristorazione alberghiere</td>
				</tr>
			</tbody>
		</table>
			</div>
			<div id="tab8">
		<table class="oxtable">
			<!-- Table header -->
			<thead>
				 <tr>
					 <th style="width:30%;">Stage professionale</th>
					 <th>&nbsp;</th>
				 </tr>
			</thead>
			<tbody>
				<tr>
					<td>Luogo</td>
					<td>Inghilterra, Australia</td>
				</tr>
				<tr>
					<td>Et&agrave;</td>
					<td>Dai 18 anni in su</td>
				</tr>
				<tr>
					<td>Durata</td>
					<td>Da 6 a 16 settimane</td>
				</tr>
				<tr>
					<td>Data</td>
					<td>Tutto l'anno</td>
				</tr>
				<tr>
					<td>Livello</td>
					<td>Minimo Intermediate (B2)</td>
				</tr>
				<tr>
					<td>Costi</td>
					<td>Circa &euro; 550</td>
				</tr>
				<tr>
					<td style="border: 0;">Settore</td>
					<td style="border: 0;">Vari</td>
				</tr>
			</tbody>
		</table>
			</div>
		</div>
		<p style="float:left;">Per informazioni contattare <a href="mailto:jeremy@thenewoxfordschool.it" style="text-decoration:underline;">jeremy@thenewoxfordschool.it</a>
		</p>
	</div>
</div>
</div>

<?php
include("footer.php");
?>
