-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Ott 12, 2012 alle 17:08
-- Versione del server: 5.0.95
-- Versione PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `thenewox_admin`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `Titolo` varchar(50) NOT NULL,
  `Testo` varchar(2000) NOT NULL,
  `Data` date NOT NULL,
  `Ora` time NOT NULL,
  `slug` varchar(200) character set utf8 collate utf8_unicode_ci NOT NULL,
  `id` int(10) NOT NULL auto_increment,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dump dei dati per la tabella `news`
--

INSERT INTO `news` (`Titolo`, `Testo`, `Data`, `Ora`, `slug`, `id`) VALUES
('Nuovo sito', '<p>&Egrave; online il nuovo sito di THE NEW OXFORD SCHOOL!</p>', '2012-09-21', '17:32:35', 'nuovo-sito', 2),
('English Happy Hour', '<p>THE NEW OXFORD SCHOOL vi invita tutti al primo "English Happy Hour" che si terr&agrave; presso la nostra sede venerd&igrave; 28 settembre&nbsp;a partire dalle 18:00.<br />Potrai partecipare alla presentazione dei corsi e dei servizi offerti dalla scuola, inoltre potrai valutare gratuitamente il livello del tuo inglese.</p>', '2012-10-11', '12:52:43', 'english-happy-hour', 4);

-- --------------------------------------------------------

--
-- Struttura della tabella `offerte`
--

CREATE TABLE IF NOT EXISTS `offerte` (
  `Titolo` varchar(100) character set utf8 collate utf8_unicode_ci NOT NULL,
  `Testo` varchar(2000) character set utf8 collate utf8_unicode_ci NOT NULL,
  `Data` date NOT NULL,
  `Ora` time NOT NULL,
  `slug` varchar(200) character set utf8 collate utf8_unicode_ci NOT NULL,
  `id` int(10) NOT NULL auto_increment,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dump dei dati per la tabella `offerte`
--

INSERT INTO `offerte` (`Titolo`, `Testo`, `Data`, `Ora`, `slug`, `id`) VALUES
('Le nostre convenzioni', '<p style="text-align: justify;">Convenzione CRAL MPS</p>\r\n<p style="text-align: justify;">Convenzione CRAS NOVARTIS</p>\r\n<p style="text-align: justify;">Convenzione con forze armate</p>', '2012-10-11', '13:01:32', 'le-nostre-convenzioni', 7);

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL auto_increment,
  `md5_id` varchar(200) collate latin1_general_ci NOT NULL default '',
  `full_name` tinytext collate latin1_general_ci NOT NULL,
  `user_name` varchar(200) collate latin1_general_ci NOT NULL default '',
  `user_email` varchar(220) collate latin1_general_ci NOT NULL default '',
  `user_level` tinyint(4) NOT NULL default '1',
  `pwd` varchar(220) collate latin1_general_ci NOT NULL default '',
  `address` text collate latin1_general_ci NOT NULL,
  `country` varchar(200) collate latin1_general_ci NOT NULL default '',
  `tel` varchar(200) collate latin1_general_ci NOT NULL default '',
  `fax` varchar(200) collate latin1_general_ci NOT NULL default '',
  `website` text collate latin1_general_ci NOT NULL,
  `date` date NOT NULL default '0000-00-00',
  `users_ip` varchar(200) collate latin1_general_ci NOT NULL default '',
  `approved` int(1) NOT NULL default '0',
  `activation_code` int(10) NOT NULL default '0',
  `banned` int(1) NOT NULL default '0',
  `ckey` varchar(220) collate latin1_general_ci NOT NULL default '',
  `ctime` varchar(220) collate latin1_general_ci NOT NULL default '',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `user_email` (`user_email`),
  FULLTEXT KEY `idx_search` (`full_name`,`address`,`user_email`,`user_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=55 ;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `md5_id`, `full_name`, `user_name`, `user_email`, `user_level`, `pwd`, `address`, `country`, `tel`, `fax`, `website`, `date`, `users_ip`, `approved`, `activation_code`, `banned`, `ckey`, `ctime`) VALUES
(54, '', 'admin', 'admin', 'admin@localhost', 5, '4c09e75fa6fe36038ac240e9e4e0126cedef6d8c85cf0a1ae', 'admin', 'Switzerland', '4433093999', '', '', '2010-05-04', '', 1, 0, 0, 't0agnms', '1350053953');

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE IF NOT EXISTS `utenti` (
  `nome` varchar(20) character set utf8 collate utf8_unicode_ci NOT NULL,
  `cognome` varchar(20) character set utf8 collate utf8_unicode_ci NOT NULL,
  `username` varchar(20) character set utf8 collate utf8_unicode_ci NOT NULL,
  `password` varchar(50) character set utf8 collate utf8_unicode_ci NOT NULL,
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `utenti`
--

INSERT INTO `utenti` (`nome`, `cognome`, `username`, `password`) VALUES
('Colin', 'Donati', 'cezza', '7815696ecbf1c96e6894b779456d330e');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
