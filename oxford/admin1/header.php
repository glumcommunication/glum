<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="images/favicon_o.png" type="image/svg"/> 
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<script type="text/javascript" src="js/confirmDelete.js" ></script>
<script language="JavaScript" type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script language="JavaScript" type="text/javascript" src="js/jquery.validate.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">

<script type="text/javascript" src="js/tiny_mce/tiny_mce.js"></script>
  <script>
  $(document).ready(function(){
    $("#actForm").validate();
  });
  </script>
<title>The New Oxford School</title>
</head>
<body>


<!--<img src="images/backgroundflag.png" width="1169" height="590" alt="background" class="bg" />-->
<div id="container">
	<div id="header">
		<a href="login.php"><img src="../images/logom.png" alt="Oxford School" /></a>
	</div>
