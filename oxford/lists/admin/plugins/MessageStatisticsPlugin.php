<?php
/**
 * MessageStatisticsPlugin for phplist
 * 
 * This file is a part of MessageStatisticsPlugin.
 *
 * This plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * @category  phplist
 * @package   MessageStatisticsPlugin
 * @author    Duncan Cameron
 * @copyright 2011-2012 Duncan Cameron
 * @license   http://www.gnu.org/licenses/gpl.html GNU General Public License, Version 3
 * @version   SVN: $Id: MessageStatisticsPlugin.php 988 2012-08-19 19:00:28Z Duncan $
 * @link      http://forums.phplist.com/viewtopic.php?f=7&t=35427
 */


/**
 * Registers the plugin with phplist
 * 
 * @category  phplist
 * @package   MessageStatisticsPlugin
 */
 
 class MessageStatisticsPlugin extends phplistPlugin {
  public $name = 'Message Statistics';
  public $coderoot;

  public function adminmenu() {
    return array(
      'main' => 'Message Statistics'
    );
  }
  public function __construct() {
    $this->coderoot = dirname(__FILE__) . '/MessageStatisticsPlugin/';
  }
}
