<?php
/**
 * CommonPlugin for phplist
 * 
 * This file is a part of CommonPlugin.
 *
 * @category  phplist
 * @package   CommonPlugin
 * @author    Duncan Cameron
 * @copyright 2011-2012 Duncan Cameron
 * @license   http://www.gnu.org/licenses/gpl.html GNU General Public License, Version 3
 * @version   SVN: $Id: en.php 694 2012-03-21 12:18:58Z Duncan $
 * @link      http://forums.phplist.com/viewtopic.php?f=7&t=35427
 */

/**
 * This file contains the English text
 * 
*/$lan = array(
//	Pager.php
	'Showing %d to %d of %d' => 'Showing %d to %d of %d',
	'Show' => 'Show',
	'no_results' => 'No results to display',
//	Widget.php
	'download to Excel' => 'download',
	'help' => 'help',
	'about' => 'about',
//	General
	'top' => 'top',
	'closewindow' => 'close this window',
//	widget_attributeform.tpl.php
	'Search for' => 'Search for',
	'unconfirmed_caption' => 'Show only unconfirmed',
	'blacklisted_caption' => 'Show only blacklisted',
//	GoogleChart.php
	'http_proxy_options_error' => 'Unable to set http proxy options',
	'chart_error' => 'Unable to create chart'
);
