<?php
/**
 * CommonPlugin for phplist
 * 
 * This file is a part of CommonPlugin.
 *
 * @category  phplist
 * @package   CommonPlugin
 * @author    Duncan Cameron
 * @copyright 2011-2012 Duncan Cameron
 * @license   http://www.gnu.org/licenses/gpl.html GNU General Public License, Version 3
 * @version   SVN: $Id: es.php 857 2012-08-16 17:08:43Z Duncan $
 * @link      http://forums.phplist.com/viewtopic.php?f=7&t=35427
 */

/**
 * This file contains the English text
 * 
*/$lan = array(
//	Pager.php
	'Showing %d to %d of %d' => 'Mostrando %d al %d de %d',
	'Show' => 'Mostrar',
	'no_results' => 'No hay resultados para mostrar',
//	Widget.php
	'download to Excel' => 'descargar',
	'help' => 'ayuda',
	'about' => 'acerca de',
//	General
	'top' => 'arriba',
	'closewindow' => 'cerrar esta ventana',
//	widget_attributeform.tpl.php
	'Search for' => 'Buscar',
	'unconfirmed_caption' => 'Mostrar s&oacute;lo no confirmados',
	'blacklisted_caption' => 'Mostrar s&oacute;lo de la lista negra',
//	GoogleChart.php
	'http_proxy_options_error' => 'No se pueden establecer las opciones de proxy http',
	'chart_error' => 'No se puede crear el gr&aacute;fico'
);
