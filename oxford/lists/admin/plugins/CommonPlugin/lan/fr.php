<?php
/**
 * CommonPlugin for phplist
 * 
 * This file is a part of CommonPlugin.
 *
 * @category  phplist
 * @package   CommonPlugin
 * @author    Duncan Cameron
 * @copyright 2011-2012 Duncan Cameron
 * @license   http://www.gnu.org/licenses/gpl.html GNU General Public License, Version 3
 * @version   SVN: $Id: fr.php 857 2012-08-16 17:08:43Z Duncan $
 * @link      http://forums.phplist.com/viewtopic.php?f=7&t=35427
 */

/**
 * This file contains the French translations
 *
 * Important - this file must be saved in ISO-8859-1 encoding
 *
 */

$lan = array(
//	Pager.php
	'Showing %d to %d of %d' => 'Afficher de %d � %d sur %d',
	'Show' => 'Afficher',
	'no_results' => 'Aucun r�sultat',
//	Widget.php
	'download to Excel' => 'T�l�charger',
	'help' => 'Aide',
	'about' => '� propos',
//	General
	'top' => 'haut',
	'closewindow' => 'Fermer cette fen�tre',
//	widget_attributeform.tpl.php
	'Search for' => 'Rechercher',
	'unconfirmed_caption' => 'Utilisateurs non confirm�s',
	'blacklisted_caption' => 'Utilisateurs sur liste noire',
//	GoogleChart.php
	'http_proxy_options_error' => 'Impossible de param�trer les options de proxy',
	'chart_error' => 'Impossible de cr�er des graphiques'
);
