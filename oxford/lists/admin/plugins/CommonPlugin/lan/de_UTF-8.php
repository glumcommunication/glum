<?php
/**
 * CommonPlugin for phplist
 * 
 * This file is a part of CommonPlugin.
 *
 * @category  phplist
 * @package   CommonPlugin
 * @author    Duncan Cameron
 * @copyright 2011-2012 Duncan Cameron
 * @license   http://www.gnu.org/licenses/gpl.html GNU General Public License, Version 3
 * @version   SVN: $Id: de_UTF-8.php 857 2012-08-16 17:08:43Z Duncan $
 * @link      http://forums.phplist.com/viewtopic.php?f=7&t=35427
 */

/**
 * This file contains the German translation of the English text
 *
 * Important - this file must be saved in UTF-8 encoding
 *
 */
 
$lan = array(
//	Pager.php
	'Showing %d to %d of %d' => 'Zeige %d bis %d von %d',
	'Show' => 'Show',
	'no_results' => 'Nichts anzuzeigen.',
//	Widget.php
	'download to Excel' => 'Download',
	'help' => 'hilfe',
	'about' => 'über',
//	General
	'top' => 'hoch',
	'closewindow' => 'Fenster schliessen',
//	widget_attributeform.tpl.php
	'Search for' => 'Suche nach',
	'unconfirmed_caption' => 'Nur unbestätigte Abonnenten',
	'blacklisted_caption' => 'Nur Abonnenten auf Blacklist',
//	GoogleChart.php
	'http_proxy_options_error' => 'Fehler bei der Verbindung zum Proxyserver, bitte überprüfen Sie Ihre Einstellungen',
	'chart_error' => 'Fehler bei der Anzeige der Grafikübersicht.'
);
