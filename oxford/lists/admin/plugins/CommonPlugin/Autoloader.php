<?php
/**
 * CommonPlugin for phplist
 * 
 * This file is a part of CommonPlugin.
 *
 * @category  phplist
 * @package   CommonPlugin
 * @author    Duncan Cameron
 * @copyright 2011-2012 Duncan Cameron
 * @license   http://www.gnu.org/licenses/gpl.html GNU General Public License, Version 3
 * @version   SVN: $Id: Autoloader.php 1078 2012-11-30 14:52:23Z Duncan $
 * @link      http://forums.phplist.com/viewtopic.php?f=7&t=35427
 */


/**
 * Convenience function to create and register the class loader
 * 
 */
include dirname(__FILE__) . '/ClassLoader.php';

function CommonPlugin_Autoloader_main()
{
	$loader = new CommonPlugin_ClassLoader();
	$loader->addBasePath(PLUGIN_ROOTDIR);
    $iterator = new DirectoryIterator(PLUGIN_ROOTDIR . '/CommonPlugin/ext');
    
    foreach ($iterator as $file) {
        if ($file->isDir()) {
            $loader->addBasePath($file->getPathname());
        }
    }
	$loader->register();
}
CommonPlugin_Autoloader_main();
