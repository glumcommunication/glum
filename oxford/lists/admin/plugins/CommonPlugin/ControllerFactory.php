<?php
/**
 * CommonPlugin for phplist
 * 
 * This file is a part of CommonPlugin.
 *
 * @category  phplist
 * @package   CommonPlugin
 * @author    Duncan Cameron
 * @copyright 2011-2012 Duncan Cameron
 * @license   http://www.gnu.org/licenses/gpl.html GNU General Public License, Version 3
 * @version   SVN: $Id: ControllerFactory.php 683 2012-03-20 17:30:58Z Duncan $
 * @link      http://forums.phplist.com/viewtopic.php?f=7&t=35427
 */

/**
 * A concrete implementation of CommonPlugin_ControllerFactoryBase using the default methods
 * 
 * @category  phplist
 * @package   CommonPlugin
 */
class CommonPlugin_ControllerFactory extends CommonPlugin_ControllerFactoryBase
{
}
