<?php 
/**
 * MessageStatisticsPlugin for phplist
 * 
 * This file is a part of MessageStatisticsPlugin.
 *
 * This plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * @category  phplist
 * @package   MessageStatisticsPlugin
 * @author    Duncan Cameron
 * @copyright 2011-2012 Duncan Cameron
 * @license   http://www.gnu.org/licenses/gpl.html GNU General Public License, Version 3
 * @version   SVN: $Id: en.php 1062 2012-11-13 15:27:56Z Duncan $
 * @link      http://forums.phplist.com/viewtopic.php?f=7&t=35427
 */

/**
 * This file contains the English text
 * 
 * @category  phplist
 * @package   MessageStatisticsPlugin
 */
$lan = array(
//	Controller.php
	'tab_settings' => 'Settings',
	'tab_lists' => 'Lists',
	'tab_messages' => 'Messages',
	'tab_opened' => 'Opened',
	'tab_unopened' => 'Unopened',
	'tab_clicked' => 'Clicked',
	'tab_bounced' => 'Bounced',
	'tab_forwarded' => 'Forwarded',
	'tab_domains' => 'Domains',
	'tab_links' => 'Links',
	'tab_linkclicks' => 'Link Clicks',
	'User email' => 'User email',
	'message %s sent to %s' => 'Message %s sent to %s',
//	Opened.php
	'last viewed' => 'Last viewed',
//	Unopened.php
//	Bounced.php
	'Bounce ID' => 'Bounce ID',
	'email' => 'email',
//	Clicked.php
	'count' => 'Count',
	'links clicked' => 'Links clicked',
	'clicks_total' => 'Total clicks',
	'user_not_exist' => 'User does not exist now',
//	Domain.php
	'Domain' => 'Domain',
	'sent' => 'Sent',
	'opened' => 'Opened',
	'clicked' => 'Clicked',
//	Forwarded.php
//	Lists.php
	'ID' => 'ID',
	'All lists' => 'All lists',
	'name' => 'Name',
	'active' => 'Active',
	'total sent' => 'Total sent',
	'latest' => 'Latest',
//	Messages.php
	'Messages sent to %s' => 'Messages sent to %s',
	'All sent messages' => 'All sent messages',
	'ID' => 'ID',
	'Subject' => 'Subject',
	'date' => 'Date',
	'bounced' => 'Bounced',
	'views' => 'Views',
	'print' => 'Print',
	'print to PDF' => 'print to PDF',
	'Message ID' => 'Message ID',
	'Users' => 'Users',
//	Links
	'Link URL' => 'Link URL',
	'clicks' => 'Clicks',
	'users' => 'Users',
	'users%' => 'Users %',
	'firstclick' => 'First',
	'latestclick' => 'Latest',
//	LinkClicks
	'Link "%s"' => 'Link "%s"',
//	Settings
	'caption' => 'Select any of the user attributes to be displayed as separate columns in addition to the user email',
	'no_attrs' => 'There are no user attributes to select',
//	exceptions
	'You are not authorised to view message %d' => 'You are not authorised to view message %d.',
	'Message %d does not exist' => 'Message %d does not exist.',
	'no_messages' => 'No messages found for the current user.',
	'no_access' => 'You do not have access to this page.',
//	view.tpl.php
	'plugin_title' => 'Message Statistics Plugin',
);
