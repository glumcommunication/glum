------------------------------------------------------------------------
r1058 | Duncan | 2012-11-13 15:22:53 +0000 (Tue, 13 Nov 2012) | 3 lines
Changed paths:
   M /phplist/trunk/MessageStatisticsPlugin/MessageStatisticsPlugin/DAO/Message.php

Additions for printing message
Performance improvment for clicks
Capitalise all keywords
------------------------------------------------------------------------
r1059 | Duncan | 2012-11-13 15:23:37 +0000 (Tue, 13 Nov 2012) | 1 line
Changed paths:
   A /phplist/trunk/MessageStatisticsPlugin/MessageStatisticsPlugin/print.tpl.php
   A /phplist/trunk/MessageStatisticsPlugin/MessageStatisticsPlugin/printfooter.tpl.php
   A /phplist/trunk/MessageStatisticsPlugin/MessageStatisticsPlugin/printheader.tpl.php

Templates for message print
------------------------------------------------------------------------
r1060 | Duncan | 2012-11-13 15:24:06 +0000 (Tue, 13 Nov 2012) | 1 line
Changed paths:
   M /phplist/trunk/MessageStatisticsPlugin/MessageStatisticsPlugin/Model.php

Additional methods
------------------------------------------------------------------------
r1061 | Duncan | 2012-11-13 15:24:49 +0000 (Tue, 13 Nov 2012) | 1 line
Changed paths:
   M /phplist/trunk/MessageStatisticsPlugin/MessageStatisticsPlugin/Controller/Messages.php

Additions and changes for message print
------------------------------------------------------------------------
r1062 | Duncan | 2012-11-13 15:27:56 +0000 (Tue, 13 Nov 2012) | 1 line
Changed paths:
   M /phplist/trunk/MessageStatisticsPlugin/MessageStatisticsPlugin/help/en/messages.php
   M /phplist/trunk/MessageStatisticsPlugin/MessageStatisticsPlugin/lan/en.php
   M /phplist/trunk/MessageStatisticsPlugin/MessageStatisticsPlugin/lan/fr.php
   M /phplist/trunk/MessageStatisticsPlugin/MessageStatisticsPlugin/lan/fr_UTF-8.php

Minor changes
------------------------------------------------------------------------
r1063 | Duncan | 2012-11-14 16:59:10 +0000 (Wed, 14 Nov 2012) | 2 lines
Changed paths:
   M /phplist/trunk/MessageStatisticsPlugin/MessageStatisticsPlugin/DAO/Message.php

Performance improvement for link clicks

------------------------------------------------------------------------
r1066 | Duncan | 2012-11-17 18:05:49 +0000 (Sat, 17 Nov 2012) | 1 line
Changed paths:
   M /phplist/trunk/MessageStatisticsPlugin/MessageStatisticsPlugin/Controller/Messages.php

Add total clicks to report
------------------------------------------------------------------------
r1067 | Duncan | 2012-11-17 18:06:44 +0000 (Sat, 17 Nov 2012) | 2 lines
Changed paths:
   M /phplist/trunk/MessageStatisticsPlugin/MessageStatisticsPlugin/print.tpl.php

Add total clicks to report
Simplify table layout
------------------------------------------------------------------------
r1086 | Duncan | 2012-11-30 16:21:10 +0000 (Fri, 30 Nov 2012) | 1 line
Changed paths:
   M /phplist/trunk/MessageStatisticsPlugin/MessageStatisticsPlugin/Controller/Messages.php

Now uses WkHtmlToPdf class
------------------------------------------------------------------------
r1090 | Duncan | 2012-12-09 22:57:11 +0000 (Sun, 09 Dec 2012) | 1 line
Changed paths:
   M /phplist/trunk/MessageStatisticsPlugin/MessageStatisticsPlugin/view.tpl.php

Removed width so that the div fits within the enclosing table
------------------------------------------------------------------------
