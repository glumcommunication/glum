<?php 
/**
 * MessageStatisticsPlugin for phplist
 * 
 * This file is a part of MessageStatisticsPlugin.
 *
 * This plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * @category  phplist
 * @package   MessageStatisticsPlugin
 * @author    Duncan Cameron
 * @copyright 2011-2012 Duncan Cameron
 * @license   http://www.gnu.org/licenses/gpl.html GNU General Public License, Version 3
 * @version   SVN: $Id: Message.php 1063 2012-11-14 16:59:10Z Duncan $
 * @link      http://forums.phplist.com/viewtopic.php?f=7&t=35427
 */

/**
 * This class provides database access to the message, usermessage and related tables
 * 
 * @category  phplist
 * @package   MessageStatisticsPlugin
 */
 
class MessageStatisticsPlugin_DAO_Message extends CommonPlugin_DAO_Message
{
	/**
	 * Private methods
	 */

	private function xx_lu_exists($field, $listid)
	{
		return $listid
			? "AND EXISTS (
				SELECT 1 FROM {$this->tables['listuser']} lu
				WHERE $field = lu.userid AND lu.listid = $listid)"	
			: '';
	}
	
	private function u_lu_join($listid)
	{
		return $listid
			? "JOIN {$this->tables['listuser']} lu ON lu.userid = u.id AND lu.listid = $listid"	
			: '';
	}
	
	private function limitClause($start, $limit)
	{
		return is_null($start) ? ''	: "LIMIT $start, $limit";
	}
	
	private function userAttributeJoin($attributes, $searchTerm, $searchAttr)
	{
		global $tables;
		global $table_prefix;

		$searchTerm = sql_escape($searchTerm);
        $attr_fields = '';
        $attr_join = '';

		foreach ($attributes as $attr) {
			$id = $attr['id'];
			$tableName = "{$table_prefix}listattr_{$attr['tablename']}";

			$joinType = ($searchTerm && $searchAttr == $id) ? 'JOIN' : 'LEFT JOIN';
			$thisJoin = "
				$joinType {$this->tables['user_attribute']} ua{$id} ON ua{$id}.userid = u.id AND ua{$id}.attributeid = {$id} ";
			
			switch ($attr['type']) {
			case 'radio':
			case 'select':
				$thisJoin .= "
				$joinType {$tableName} la{$id} ON la{$id}.id = ua{$id}.value ";
				
				if ($searchTerm && $searchAttr == $id) {
					$thisJoin .= "AND la{$id}.name LIKE '%$searchTerm%' ";
				}
				$attr_fields .= ", la{$id}.name AS attr{$id}";
				break;
			default:
				if ($searchTerm && $searchAttr == $id) {
					$thisJoin .= "AND ua{$id}.value LIKE '%$searchTerm%' ";
				}
				$attr_fields .= ", ua{$id}.value AS attr{$id}";
				break;
			}
			$attr_join .= $thisJoin;
		}
		return array($attr_join, $attr_fields);
	}

	/**
	 * Public methods
	 */
	/*
	 * Methods for messages
	 */
	public function latestMessage($loginid, $listid)
	{
		$owner = $loginid ? "AND m.owner = $loginid" : '';
		$list = $listid ? "AND lm.listid = $listid" : '';
		$sql = 
			"SELECT MAX(lm.messageid) AS msgid
			FROM {$this->tables['listmessage']} lm
			JOIN {$this->tables['message']} m ON lm.messageid = m.id
			JOIN {$this->tables['usermessage']} um ON um.messageid = m.id 
			WHERE m.status = 'sent' $owner $list";

		return $this->dbCommand->queryOne($sql, 'msgid');
	}

	public function prevNextMessage($listId, $msgID, $loginid)
	{
		$owner_and = $loginid ? "AND owner = $loginid" : '';
		$m_lm_exists = $listId
			? "AND EXISTS (
				SELECT 1 FROM {$this->tables['listmessage']} lm
				WHERE m.id = lm.messageid AND lm.listid = $listId)"
			: "";

		$sql = 
			"SELECT MAX(a.id) AS prev
			FROM (
				SELECT DISTINCT id
				FROM {$this->tables['message']} m
				WHERE m.status = 'sent'
				AND id < $msgID
				$m_lm_exists
				$owner_and
			) AS a";

		$prev = $this->dbCommand->queryOne($sql, 'prev');

		$sql = 
			"SELECT MIN(a.id) AS next
			FROM (
				SELECT DISTINCT id
				FROM {$this->tables['message']} m
				WHERE m.status = 'sent'
				AND id > $msgID
				$m_lm_exists
				$owner_and
			) AS a";

		$next = $this->dbCommand->queryOne($sql, 'next');

		return array($prev, $next);
	}

	public function fetchMessages($listId, $loginid, $ascOrder = false, $start = null, $limit = null)
	{
		$order = $ascOrder ? 'ASC' : 'DESC';
		$owner_and = $loginid ? "AND owner = $loginid" : '';
		$limitClause = is_null($start) ? ''	: "LIMIT $start, $limit";
		
		$m_lm_exists = $listId
			? "AND EXISTS (
				SELECT 1 FROM {$this->tables['listmessage']} lm
				WHERE m.id = lm.messageid AND lm.listid = $listId)"
			: "";

		$um_lu_exists = $this->xx_lu_exists('um.userid', $listId);
		$ltuc_lu_exists = $this->xx_lu_exists('ltuc.userid', $listId);
		$umb_lu_exists = $this->xx_lu_exists('umb.user', $listId);
		$umf_lu_exists = $this->xx_lu_exists('umf.user', $listId);

		$sql =
			"SELECT m.id, fromfield AS 'from', subject, viewed, owner,
            date_format(m.sent,'%e %b %Y') AS end, date_format(m.sendstart,'%e %b %Y') AS start,
			(SELECT COUNT(viewed)
				FROM {$this->tables['usermessage']} um
				WHERE messageid = m.id 
				$um_lu_exists
			) AS openUsers,
			(SELECT COUNT(status)
				FROM {$this->tables['usermessage']} um
				WHERE messageid = m.id 
				$um_lu_exists
			) AS sent,

			(SELECT COUNT(DISTINCT ltuc.userid)
				FROM {$this->tables['linktrack_userclick']} ltuc
				WHERE ltuc.messageid = m.id
				AND EXISTS (
					SELECT * FROM {$this->tables['usermessage']} um
					WHERE ltuc.userid = um.userid AND ltuc.messageid = um.messageid
				)
				$ltuc_lu_exists
			) AS clickUsers,
			(SELECT COUNT(*)
				FROM {$this->tables['linktrack_userclick']} ltuc
				WHERE ltuc.messageid = m.id AND ltuc.name = 'Message Type'
				AND EXISTS (
					SELECT * FROM {$this->tables['usermessage']} um
					WHERE ltuc.userid = um.userid AND ltuc.messageid = um.messageid
				)
				$ltuc_lu_exists
			) AS totalClicks,
			(SELECT COUNT(DISTINCT umb.user)
				FROM {$this->tables['user_message_bounce']} umb
				WHERE umb.message = m.id
				AND EXISTS (
					SELECT 1 FROM {$this->tables['usermessage']} um
					WHERE umb.user = um.userid AND umb.message = um.messageid
				)
				$umb_lu_exists
			) AS bouncecount,
		   (SELECT COUNT(DISTINCT umf.user)
                FROM {$this->tables['user_message_forward']} AS umf
                WHERE umf.message = m.id
                AND EXISTS (
                    SELECT 1 FROM {$this->tables['usermessage']} um 
                    WHERE um.userid = umf.user AND umf.message = um.messageid
                )
                $umf_lu_exists
            ) AS forwardcount
			FROM {$this->tables['message']} m
			WHERE m.status = 'sent'
			$m_lm_exists
			$owner_and
			ORDER BY id $order
			$limitClause";

		return $this->dbCommand->queryAll($sql);
	}

	public function fetchMessage($msgId, $listId)
	{
	
		$m_lm_exists = $listId
			? "AND EXISTS (
				SELECT 1 FROM {$this->tables['listmessage']} lm
				WHERE m.id = lm.messageid AND lm.listid = $listId)"
			: "";

		$um_lu_exists = $this->xx_lu_exists('um.userid', $listId);
		$ltuc_lu_exists = $this->xx_lu_exists('ltuc.userid', $listId);
		$umb_lu_exists = $this->xx_lu_exists('umb.user', $listId);
		$umf_lu_exists = $this->xx_lu_exists('umf.user', $listId);

		$sql =
			"SELECT m.id, fromfield AS 'from', subject, viewed, owner,
            date_format(m.sent,'%e %b %Y') AS end, date_format(m.sendstart,'%e %b %Y') AS start,
			(SELECT COUNT(viewed)
				FROM {$this->tables['usermessage']} um
				WHERE messageid = m.id 
				$um_lu_exists
			) AS openUsers,

			(SELECT COUNT(status)
				FROM {$this->tables['usermessage']} um
				WHERE messageid = m.id 
				$um_lu_exists
			) AS sent,

			(SELECT COUNT(DISTINCT ltuc.userid)
				FROM {$this->tables['linktrack_userclick']} ltuc
				WHERE ltuc.messageid = m.id
				AND EXISTS (
					SELECT * FROM {$this->tables['usermessage']} um
					WHERE ltuc.userid = um.userid AND ltuc.messageid = um.messageid
				)
				$ltuc_lu_exists
			) AS clickUsers,

			(SELECT COUNT(*)
				FROM {$this->tables['linktrack_userclick']} ltuc
				WHERE ltuc.messageid = m.id AND ltuc.name = 'Message Type'
				AND EXISTS (
					SELECT * FROM {$this->tables['usermessage']} um
					WHERE ltuc.userid = um.userid AND ltuc.messageid = um.messageid
				)
				$ltuc_lu_exists
			) AS totalClicks,

			(SELECT COUNT(DISTINCT umb.user)
				FROM {$this->tables['user_message_bounce']} umb
				WHERE umb.message = m.id
				AND EXISTS (
					SELECT 1 FROM {$this->tables['usermessage']} um
					WHERE umb.user = um.userid AND umb.message = um.messageid
				)
				$umb_lu_exists
			) AS bouncecount,

		   (SELECT COUNT(DISTINCT umf.user)
                FROM {$this->tables['user_message_forward']} AS umf
                WHERE umf.message = m.id
                AND EXISTS (
                    SELECT 1 FROM {$this->tables['usermessage']} um 
                    WHERE um.userid = umf.user AND umf.message = um.messageid
                )
                $umf_lu_exists
            ) AS forwardcount

			FROM {$this->tables['message']} m
			WHERE m.status = 'sent'
            AND m.id = $msgId
			$m_lm_exists";

		return $this->dbCommand->queryRow($sql);
	}

	public function totalMessages($listId, $loginid)
	{
		$owner_and = $loginid ? "AND owner = $loginid" : '';
		$lm_exists = $listId
			? "AND EXISTS (
				SELECT * FROM {$this->tables['listmessage']} lm
				WHERE lm.messageid =  m.id AND lm.listid = $listId)"
			: "";

		$sql = 
			"SELECT COUNT(m.id) AS t
			FROM {$this->tables['message']} m
			WHERE m.status = 'sent' 
			$lm_exists
			$owner_and";

		return $this->dbCommand->queryOne($sql, 't');
	}
	/*
	 * Methods for message views
	 */
	public function fetchMessageOpens($opened, $msgid, $listid, 
		$attributes, $searchTerm, $searchAttr,
		$start = null, $limit = null)
	{
		list($attr_join, $attr_fields) = $this->userAttributeJoin($attributes, $searchTerm, $searchAttr);
		$limitClause = $this->limitClause($start, $limit);
		$u_lu_exists = $this->xx_lu_exists('u.id', $listid);

		if ($opened) {
			$isOpened = 'NOT NULL';
			$order = 'um.viewed';
		} else {
			$isOpened = 'NULL';
			$order = 'u.email';
		}

		$sql = 
			"SELECT u.email, um.userid, um.entered, um.viewed $attr_fields
			FROM {$this->tables['usermessage']} um
			JOIN {$this->tables['user']} u ON um.userid = u.id
			$attr_join
			WHERE um.messageid = $msgid
			AND um.viewed IS $isOpened
			$u_lu_exists
			ORDER BY $order
			$limitClause";
		return $this->dbCommand->queryAll($sql);
	}

	public function totalMessageOpens($opened, $msgid, $listid,	$attributes, $searchTerm, $searchAttr)
	{
		if ($opened) {
			$isOpened = 'NOT NULL';
		} else {
			$isOpened = 'NULL';
		}

		if ($searchTerm) {
			list($attr_join) = $this->userAttributeJoin($attributes, $searchTerm, $searchAttr);
		} else {
			$attr_join = '';
		}

		$u_lu_exists = $this->xx_lu_exists('u.id', $listid);
		$sql = 
			"SELECT COUNT(*) AS t
			FROM {$this->tables['usermessage']} um
			JOIN {$this->tables['user']} u ON um.userid = u.id
			$attr_join
			WHERE um.messageid = $msgid
			AND um.viewed IS $isOpened
			$u_lu_exists
            ";
		return $this->dbCommand->queryOne($sql, 't');
	}
	/*
	 * Methods for message clicks
	 */
	public function fetchMessageClicks($msgid, $listid, $attributes, $searchTerm, $searchAttr, $start = null, $limit = null)
	{
		list($attr_join, $attr_fields) = $this->userAttributeJoin($attributes, $searchTerm, $searchAttr);
		$ltuc_lu_exists = $this->xx_lu_exists('ltuc.userid', $listid);
		$limitClause = $this->limitClause($start, $limit);

		$sql =
			"SELECT ltuc.userid AS userid, COUNT(DISTINCT ltuc.linkid) AS links, COUNT(*) AS clicks,
            u.email AS email
            $attr_fields
			FROM {$this->tables['linktrack_userclick']} ltuc
			LEFT JOIN {$this->tables['user']} u ON ltuc.userid = u.id
			$attr_join
			WHERE ltuc.messageid = $msgid AND ltuc.name = 'Message Type'
			AND EXISTS (
				SELECT 1 FROM {$this->tables['usermessage']} um 
				WHERE um.userid = ltuc.userid AND ltuc.messageid = um.messageid
			)
			$ltuc_lu_exists
			GROUP BY ltuc.userid
			ORDER BY u.email
			$limitClause";

		return $this->dbCommand->queryAll($sql);
	}
	public function totalMessageClicks($msgid, $listid, $attributes, $searchTerm, $searchAttr)
	{
		$ltuc_lu_exists = $this->xx_lu_exists('ltuc.userid', $listid);

		if ($searchTerm) {
			list($attr_join) = $this->userAttributeJoin($attributes, $searchTerm, $searchAttr);
		} else {
			$attr_join = '';
		}

		$sql =
			"SELECT COUNT(DISTINCT ltuc.userid) AS t
			FROM {$this->tables['linktrack_userclick']} ltuc
			WHERE ltuc.messageid = $msgid AND ltuc.name = 'Message Type'
			AND EXISTS (
				SELECT 1 FROM {$this->tables['usermessage']} um 
				WHERE um.userid = ltuc.userid AND ltuc.messageid = um.messageid
			)
			$ltuc_lu_exists";
		return $this->dbCommand->queryOne($sql, 't');
	}
	/*
	 * Methods for message bounces
	 */
	 public function fetchMessageBounces($mid, $listid, $attributes, $searchTerm, $searchAttr, $start = null, $limit = null)
	 {
		list($attr_join, $attr_fields) = $this->userAttributeJoin($attributes, $searchTerm, $searchAttr);
		$umb_lu_exists = $this->xx_lu_exists('umb.user', $listid);
		$limitClause = $this->limitClause($start, $limit);

		$sql = 
		   "SELECT u.email, umb.user, umb.bounce $attr_fields
			FROM {$this->tables['user_message_bounce']} AS umb
			JOIN {$this->tables['user']} AS u ON umb.user = u.id
			$attr_join
			WHERE umb.message = $mid
			AND EXISTS (
				SELECT 1 FROM {$this->tables['usermessage']} um 
				WHERE um.userid = umb.user AND umb.message = um.messageid
			)
			$umb_lu_exists
			$limitClause";
		return $this->dbCommand->queryAll($sql);
	}

	public function totalMessageBounces($mid, $listid, $attributes, $searchTerm, $searchAttr)
	{
		$umb_lu_exists = $this->xx_lu_exists('umb.user', $listid);

		if ($searchTerm) {
			list($attr_join) = $this->userAttributeJoin($attributes, $searchTerm, $searchAttr);
		} else {
			$attr_join = '';
		}

		$sql = 
		   "SELECT COUNT(umb.user) AS t
			FROM {$this->tables['user_message_bounce']} AS umb
			JOIN {$this->tables['user']} AS u ON umb.user = u.id
			$attr_join
			WHERE umb.message = $mid
			AND EXISTS (
				SELECT 1 FROM {$this->tables['usermessage']} um 
				WHERE um.userid = umb.user AND umb.message = um.messageid
			)
			$umb_lu_exists";

		return $this->dbCommand->queryOne($sql, 't');
	}
	/*
	 * Methods for message forwards
	 */
	 public function fetchMessageForwards($mid, $listid, $attributes, $searchTerm, $searchAttr, $start = null, $limit = null)
	 {
		list($attr_join, $attr_fields) = $this->userAttributeJoin($attributes, $searchTerm, $searchAttr);
		$u_lu_exists = $this->xx_lu_exists('u.id', $listid);
		$limitClause = $this->limitClause($start, $limit);

		$sql = 
		   "SELECT u.email, u.id, COUNT(umf.id) AS count $attr_fields
			FROM {$this->tables['user_message_forward']} AS umf
			JOIN {$this->tables['user']} AS u ON umf.user = u.id
			$attr_join
			WHERE umf.message = $mid
			AND EXISTS (
				SELECT 1 FROM {$this->tables['usermessage']} um 
				WHERE um.userid = umf.user AND umf.message = um.messageid
			)
			$u_lu_exists
			GROUP BY umf.user
			$limitClause";
		return $this->dbCommand->queryAll($sql);
	}

	public function totalMessageForwards($mid, $listid, $attributes, $searchTerm, $searchAttr)
	{
		$u_lu_exists = $this->xx_lu_exists('u.id', $listid);

		if ($searchTerm) {
			list($attr_join) = $this->userAttributeJoin($attributes, $searchTerm, $searchAttr);
		} else {
			$attr_join = '';
		}

		$sql = 
		   "SELECT COUNT(DISTINCT umf.user) AS t
			FROM {$this->tables['user_message_forward']} AS umf
			JOIN {$this->tables['user']} AS u ON umf.user = u.id
			$attr_join
			WHERE umf.message = $mid
			AND EXISTS (
				SELECT 1 FROM {$this->tables['usermessage']} um 
				WHERE um.userid = umf.user AND umf.message = um.messageid
			)
			$u_lu_exists";

		return $this->dbCommand->queryOne($sql, 't');
	}
	/*
	 * Methods for domains
	 */
	public function messageByDomain($msgID, $listid, $start = null, $limit = null)
	{
		$listuser_join = $this->u_lu_join($listid);
		$limitClause = $this->limitClause($start, $limit);

		$sql =
			"SELECT SUBSTRING_INDEX(u.email, '@', -1) AS domain,
				COUNT(um.viewed) AS opened, COUNT(um.status) AS sent, COUNT(lt.userid) AS clicked
			FROM {$this->tables['user']} u 
			JOIN {$this->tables['usermessage']} um ON u.id = um.userid AND um.messageid = $msgID
			$listuser_join
			LEFT OUTER JOIN 
				(SELECT DISTINCT userid
				FROM {$this->tables['linktrack_userclick']}
				WHERE messageid = $msgID ) AS lt ON u.id = lt.userid
			GROUP BY domain
			$limitClause";

		return $this->dbCommand->queryAll($sql);
	}

	public function totalMessageByDomain($msgID, $listid)
	{
		$listuser_join = $this->u_lu_join($listid);
		$sql =
			"SELECT COUNT(*) AS t 
			FROM (
				SELECT SUBSTRING_INDEX(u.email, '@', -1) AS domain
				FROM {$this->tables['user']} u 
				JOIN {$this->tables['usermessage']} um ON u.id = um.userid AND um.messageid = $msgID
				$listuser_join
				GROUP BY domain
			) AS domain
			";

		return $this->dbCommand->queryOne($sql, 't');
	}
	/*
	 * Methods for links
	 */
	public function prevNextUrl($msgID, $url)
	{
		$sql = 
			"SELECT MAX(a.url) AS prev
			FROM (
				SELECT DISTINCT url
				FROM {$this->tables['linktrack']}
				WHERE messageid = $msgID
				AND url < '$url'
				ORDER BY url
			) AS a";

		$prev = $this->dbCommand->queryOne($sql, 'prev');

		$sql = 
			"SELECT MIN(a.url) AS next
			FROM (
				SELECT DISTINCT url
				FROM {$this->tables['linktrack']}
				WHERE messageid = $msgID
				AND url > '$url'
				ORDER BY url
			) AS a";

		$next = $this->dbCommand->queryOne($sql, 'next');

		return array($prev, $next);
	}

	public function links($msgID, $listid, $start = null, $limit = null)
	{
		$lt_lu_exists = $this->xx_lu_exists('lt.userid', $listid);
		$limitClause = $this->limitClause($start, $limit);

		$sql = 
			"SELECT
				lt.url,
				DATE_FORMAT(MIN(lt.firstclick), '%Y-%m-%d %H:%i') AS firstclick,
				DATE_FORMAT(MAX(IF(lt.clicked > 0, lt.latestclick, NULL)), '%Y-%m-%d %H:%i') AS latestclick,
				SUM(lt.clicked) AS numclicks,
				COUNT(*) AS totalsent,
				COUNT(lt.firstclick) AS usersclicked
			FROM {$this->tables['linktrack']} lt
			WHERE lt.messageid = $msgID 
			$lt_lu_exists
			GROUP BY lt.url
			ORDER BY lt.url
			$limitClause";

		return $this->dbCommand->queryAll($sql);
	}

	public function totalLinks($msgID, $listid)
	{
		$listuser_join = $this->u_lu_join($listid);
		$lt_lu_exists = $this->xx_lu_exists('lt.userid', $listid);
		$sql =
			"SELECT COUNT(DISTINCT lt.url) AS t
			FROM {$this->tables['linktrack']} lt
			WHERE lt.messageid = $msgID
			$lt_lu_exists
			";

		return $this->dbCommand->queryOne($sql, 't');
	}

	/*
	 * Methods for link clicks
	 */
	public function linkClicks($url, $msgID, $listid, $attributes, $searchTerm, $searchAttr, $start = null, $limit = null)
	{
		list($attr_join, $attr_fields) = $this->userAttributeJoin($attributes, $searchTerm, $searchAttr);
		$ltuc_lu_exists = $this->xx_lu_exists('ltuc.userid', $listid);
		$limitClause = $this->limitClause($start, $limit);

		$sql = 
			"SELECT u.email, u.id,
			DATE_FORMAT(lt.firstclick, '%Y-%m-%d %H:%i') AS firstclick,
			DATE_FORMAT(lt.latestclick, '%Y-%m-%d %H:%i') AS latestclick,
			lt.clicked
			$attr_fields
			FROM {$this->tables['linktrack_userclick']}  ltuc
            JOIN {$this->tables['linktrack']} lt ON ltuc.linkid = lt.linkid AND lt.url = '$url'
			JOIN {$this->tables['user']} AS u ON lt.userid = u.id
			$attr_join
			WHERE ltuc.messageid = $msgID AND ltuc.name = 'Message Type'
            $ltuc_lu_exists
            GROUP BY ltuc.linkid
			$limitClause
			";

		return $this->dbCommand->queryAll($sql);
	}

	public function totalLinkClicks($url, $msgID, $listid, $attributes, $searchTerm, $searchAttr)
	{
		if ($searchTerm) {
			list($attr_join) = $this->userAttributeJoin($attributes, $searchTerm, $searchAttr);
		} else {
			$attr_join = '';
		}
		$ltuc_lu_exists = $this->xx_lu_exists('ltuc.userid', $listid);
		$sql =
			"SELECT COUNT(DISTINCT ltuc.userid) AS t
			FROM {$this->tables['linktrack_userclick']} ltuc
            JOIN {$this->tables['linktrack']} lt ON ltuc.linkid = lt.linkid AND lt.url = '$url'
			$attr_join
			WHERE ltuc.messageid = $msgID AND ltuc.name = 'Message Type'
            $ltuc_lu_exists
			";

		return $this->dbCommand->queryOne($sql, 't');
	}

}
