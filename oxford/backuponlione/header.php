<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <?php
        $description = "Scuola di inglese, francese, spagnolo, tedesco, russo, arabo, cinese, italiano per stranieri, a Siena in Toscana, Italia. Scuola di lingue straniere a Siena.";
        ?>
        <META NAME="description" CONTENT="<?php echo $description ?>">
        <META NAME="KEYWORDS" CONTENT="scuola di inglese a siena, scuola di francese a siena, scuola di tedesco a siena, scuola di spagnolo a siena, scuola di cinese a siena, scuola di russo a siena, scuola di arabo a siena, scuola di italiano per stranieri a siena, lezioni di inglese a siena, lezioni di francese a siena, lezioni di spagnolo a siena, lezioni di tedesco a siena, lezioni di cinese a siena, lezioni di russo a siena, lezioni di arabo a siena, imparare l'inglese a siena, imparare il francese a siena, imparare il tedesco a siena, imparare lo spagnolo a siena, corsi di inglese a siena, corsi di francese a siena, corsi di tedesco a siena, corsi di spagnolo a siena, corsi di cinese a siena, corsi di russo a siena, corsi di arabo a siena">
        <link rel="icon" href="images/favicon_o.png" type="image/svg"/> 
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link href="css/lightbox.css" rel="stylesheet" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
        <title>The New Oxford School - <?php echo $thisPage ?></title>

        <?php
        if ($thisPage == "Home") {
            ?>
            <link rel="stylesheet" href="css/flexslider.css" type="text/css">

            <script src="js/jquery.flexslider.js"></script>
            <script type="text/javascript" charset="utf-8">
                $(window).load(function() {
                    $('.flexslider').flexslider({
                        animation: "slide",
                        animationDuration: 0,
                        animationLoop: true,
                        slideshowSpeed: 5000,
                        pauseOnHover: false,
                        touch: true,
                        controlNav: false
                    });
                });
            </script>
            <?php
        }
        if ($thisPage == "Viaggi studio") {
            ?>
            <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
            <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
            <script>
                $(function() {
                    $("#tabs").tabs();
                });
            </script>
            <?php
        }
        ?>
        <!-- SCROLLBAR -->

        <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
        <!-- mousewheel plugin -->
        <script src="js/jquery.mousewheel.min.js"></script>
        <!-- custom scrollbars plugin -->
        <script src="js/jquery.mCustomScrollbar.js"></script>
        <script>
            (function($) {


                $(document).ready(function() {
                    /* custom scrollbar fn call */
                    $("#scrollbar1").mCustomScrollbar({
                        scrollButtons: {
                            enable: true

                        },
                        scrollInertia: 0,
                        advanced: {
                            updateOnContentResize: true
                        },
                    });
                }),
                        $(document).ready(function() {
                    /* custom scrollbar fn call */
                    $("#scrollbar2").mCustomScrollbar({
                        scrollButtons: {
                            enable: true

                        },
                        advanced: {
                            updateOnContentResize: true
                        },
                    });
                })
            })(jQuery);
        </script>


        <script src="js/lightbox.js"></script>
        <!--	<script type="text/javascript" src="js/jquery.tinyscrollbar.min.js"></script>
                <script type="text/javascript">
                        $(document).ready(function(){
                                $('#scrollbar1').tinyscrollbar();	
                        });
                </script>
                <script type="text/javascript">
                        $(document).ready(function(){
                                $('#scrollbar2').tinyscrollbar();	
                        });
                </script>-->
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-35054570-1']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>

    </head>
    <body>
    <!--<img src="images/backgroundflag.png" width="1169" height="590" alt="background" class="bg" />-->
        <div id="container">
            <div id="header">
                <img src="images/logotrinity.jpg" class="logoheader trinity">
                <img src="images/logoeda.jpg" class="logoheader eda">
                <a href="index.php"><img src="images/logom.png" alt="Oxford School" /></a>
                <img src="images/logoaisli.jpg" class="logoheader aisli">
                <img src="images/logoruxum.jpg" class="logoheader ielts"> 
            </div>
