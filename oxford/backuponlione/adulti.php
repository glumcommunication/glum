<?php
$thisPage = "Corsi per Adulti";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
<div class="sbox">
	<h1>Corsi per Adulti</h1>
		<img src="images/english-adult.jpg" alt="" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>DESCRIZIONE DEL CORSO</h2>
			<h3>Corso di gruppo</h3>
			<p>I nostri corsi di gruppo sono a numero chiuso di massimo 6 partecipanti per 
			consentire il massimo apprendimento e la garanzia di essere seguiti attentamente dall’insegnante.<br />
			I corsi di gruppo sono organizzati in  54 ore, la frequenza è di due lezioni a settimana di due ore 
			ciascuna per una durata totale del corso di 3 mesi circa.<br />
			Sono corsi intensivi studiati apposta per chi vuole imparare e/o perfezionare la lingua 
			in tempi brevi.</p>
			<!--<p><strong>Garanzia di apprendimento</strong><br />
			A fine corso l'allievo che, pur avendo frequentato il corso per intero, sostenendo l'esame 
			finale non riesce a superare il 50% delle risposte della prova scritta, ha diritto a ripetere il 
			corso dello stesso livello <strong>gratuitamente</strong> entro l'anno scolastico in corso.</p> -->
			<h3>Corso individuale</h3>
			<p>I corsi individuali (one to one) si svolgono in 30 ore totali, da organizzare con l’insegnante 
			in 20 lezioni di un’ora e mezza con una frequenza di una o due lezioni a settimana a seconda delle 
			proprie esigenze personali e/o professionali.<br />
			Caratteristica fondamentale di questi corsi è la flessibilità della frequenza poichè l’allievo 
			comunicando in anticipo eventuali cambi di orari o giorni svolge tutte le ore di corso senza mai 
			fare assenze.<br />
			Proprio per questo motivo sono corsi indicati a chi ha poco tempo libero per lavoro e quindi 
			grandi difficoltà a seguire un gruppo di lavoro.<br />
			Inoltre questo tipo di corso dà la possibilità di richiedere una terminologia mirata alle proprie 
			esigenze professionali e così anche un aiuto concreto.</p>
			<h3>Materiale didattico</h3>
			<p>Il materiale didattico è interamente fornito dalla Scuola, viene scelto tra le più note case 
			editrici per le scuole tra le quali Oxford University Press e di volta in volta vengono selezionate 
			dal nostro referente didattico le versioni più aggiornate e il materiale più adatto ad ogni fascia d'età 
			e ad ogni tipologia di corso.</p>
			<p>Compresi nel corso:
			<ul class="oxlist">
			<li>Materiale didattico originale, student's book, che rimane di proprietà dell'allievo</li>
			<li>Laboratorio linguistico</li>
			<li>Internet</li>
			<li>Biblioteca in lingua originale</li>
			<li>Proiezione di films in lingua originale sottotitolati con la presenza di un insegnante</li>
			<li>Scambi culturali</li>
			</ul>
			</p>
			<h3>Esami a fine corso</h3>
			<p>A fine di ogni corso, sia di gruppo sia individuale per adulti, c'è la possibilità presso la 
			nostra scuola di sostenere un test finale, scritto e orale e di ottenere, una volta superato, un 
			attestato di frequenza che riporta le ore di corso frequentate e il livello conseguito.<br />
			Inoltre, per chi lo desidera, c'è anche la possibilità di sostenere un esame di Certificazione 
			Internazionale <strong>Trinity College London</strong> presso la nostra sede e nelle frequenti sessioni d'esame. 
			Possono sostenere gli esami <strong>Trinity College London</strong> adulti, studenti universitari, bambini dai 7 anni.</p>		
		
		<h3>METODI DI PAGAMENTO</h3>
		<p>I pagamenti di tutti i nostri corsi di lingue, sia per adulti che per bambini, possono avvenire in 
			un'unica soluzione o dilazionati in pi&ugrave; mesi.</p>
		
	</div>
</div>
</div>

<?php
include("footer.php");
?>