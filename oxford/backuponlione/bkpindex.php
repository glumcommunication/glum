<?php
$thisPage = "Home";
$description = "Scuola di inglese, francese, spagnolo, tedesco, russo, arabo, cinese, italiano per stranieri, a Siena in Toscana, Italia. Scuola di lingue straniere a Siena.";
include("header.php");
include("menu.php");
include('db.php');

?>

	<div id="main">
<?php include("flags.php");	?>
		<div id="mainboxa">
			<!--<img src="images/oxfordhome.jpg" alt="" />-->
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <img src="images/oxfordhome.jpg" />
                    </li>
                    <li>
                        <img src="images/oxfordhome2.jpg" />
                    </li>
                    <li>
                        <a href="aziende.php">
                            <img src="images/oxfordhome4.jpg" />
                        </a>
                    </li>
					<li>
                        <img src="images/oxfordhome5.jpg" />
                    </li>
					<li>
                        <img src="images/oxfordhome6.jpg" />
                    </li>
                </ul>
            </div>
		</div>

	<div class="rightHome">
<!--<div class="lrow">-->
	
		<div class="boxhomeRight1">
		<h1>News ed Eventi</h1>
	<div id="scrollbar1">

<?php
$db_server = mysql_connect($db_hostname, $db_username, $db_password);

if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());

mysql_select_db($db_database, $db_server)
	or die("Unable to select database: " . mysql_error());
$sql = "SELECT * FROM news ORDER BY Data DESC,Ora DESC LIMIT 3";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);
$newsTitle = stripslashes($row[0]);
$elm1 = stripslashes($row[1]);
$datasql = "SELECT DATE_FORMAT(Data, '%d/%m/%Y')  as data FROM news WHERE id='$row[5]'";

$rowdata = mysql_fetch_assoc(mysql_query($datasql));


echo <<<_END
		<div class="mtext">
		<h3>$newsTitle</h3>
		<h5>$rowdata[data], $row[3]</h5>
		$elm1

		</div>
_END;
}
?>
</div>
<div class="more"><a href="news.php" title="News">Altre news...</a>
</div>
		</div>
		<div class="boxhomeRight2">
		<h1>Orari</h1>
		<div class="mtext">
			<h3>Orari di segreteria</h3>
			<p>Dal lunedì al venerdì dalle 9,00 alle 20,00<br />
			Il sabato dalle 9,00 alle 13,00</p>
			<p style="color:#cf142b;font-weight:bold;">Nei mesi di luglio e agosto la segreteria rimarrà chiusa il sabato</p>
			<!--Il sabato <strong>dalle 9,30 ale 13,00</strong></p>-->
			<h3>Orari delle lezioni</h3>
			<p>Dal lunedì al venerdì dalle 9,00 alle 22,00<br />
			Il sabato dalle 9,00 alle 13,00</p>
            <!--<h3 style="font-size: 12px;">La scuola resterà aperta anche nei mesi di giugno, luglio e agosto osservando i soliti orari, 
            a luglio e agosto il sabato resteremo chiusi.</h3>-->
		</div>
		</div>
<!--	</div>-->
<!--	<div class="lrow">-->
		<div class="boxhomeRight1">
		<h1>Trinity</h1>
			<a href="http://www.trinitycollege.it/" target="_blank"><img src="images/trinity.png" alt="Trinity" /></a>
		</div>
		<div class="boxhomeRight2">
		<h1>Offerte</h1>
	<div id="scrollbar2">

<?php
$db_server = mysql_connect($db_hostname, $db_username, $db_password);

if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());

mysql_select_db($db_database, $db_server)
	or die("Unable to select database: " . mysql_error());
$sql = "SELECT * FROM offerte ORDER BY Data DESC,Ora DESC LIMIT 3";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);
$newsTitle = stripslashes($row[0]);
$elm1 = stripslashes($row[1]);
$datasql = "SELECT DATE_FORMAT(Data, '%d/%m/%Y')  as data FROM offerte WHERE id='$row[5]'";

$rowdata = mysql_fetch_assoc(mysql_query($datasql));
echo <<<_END
		<div class="mtext">
		<h3>$newsTitle</h3>
		<h5>$rowdata[data], $row[3]</h5>
		$elm1

		</div>
_END;
}
?>
</div>
<div class="more"><a href="offerte.php" title="Offerte">Altre offerte...</a>
</div>
		</div>
<!--		</div>-->
	</div>
	<div class="leftHome">
	<div class="boxhomeLeft">
		<h1>Diagnostic Test</h1>
		<div class="frameBox">
			<a href="test.php" title="Diagnostic Test"><img src="images/test.png" alt="Diagnostic Test" /></a></div>
		</div>
    <div class="boxhomeLeft" style="background-color: transparent;">
        <h1>In evidenza</h1>
        <div class="frameBox">
            <img src="images/certInter.jpg" alt="Certificazione Internazionale" /></div>
        </div>
	</div>
</div>




<?php
include("footer.php");
?>
