<?php
$thisPage = "Biblioteca";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
<div class="sbox">
	<h1>Biblioteca</h1>
		<img src="images/biblioteca.jpg" alt="" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>SERVIZIO PRENOTAZIONE LIBRI</h2>
        <table id="oxtable">

      <thead>
      <tr>
        <th width="51"><strong>Numero</strong></th>
        <th width="406"><strong>Titolo</strong></th>
        <th width="222"><strong>Autore</strong></th>
        <th width="71"><div align="right"><strong>Categoria</strong></div></th>
      </tr>
      </thead>
      <tr>
        <td height="17" align="right"><div align="center">1</div></td>
        <td>FACES OF THE PAST</td>
        <td>B.M.WALKER</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">2</div></td>
        <td>WHITETHORN WOODS</td>
        <td>MAEVE BINCHJ</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">3</div></td>
        <td>INSIDERS</td>
        <td>OLIVIA GOLDSMITH</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">4</div></td>
        <td>MIDDLESEX</td>
        <td>JEFFREY EUGENIDES</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">5</div></td>
        <td>BLACK WILL SHOOT&nbsp;</td>
        <td>JESSE WASHINGTON</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">6</div></td>
        <td>FROST NIXON</td>
        <td>DAVID FROST</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">7</div></td>
        <td>THE LIFE AND OPINIONS OF TRISTRAM</td>
        <td>LAURENCE STERNE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">8</div></td>
        <td>THE FRENCH LIEUTENANT'S WOMAN&nbsp;</td>
        <td>JOHN FOWLES</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">9</div></td>
        <td>THE BATTLE OF BRITAIN&nbsp;</td>
        <td>MATTHEW PARKER</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">10</div></td>
        <td>DICTIONARY OF DREAMS&nbsp;</td>
        <td>GEDDES .GROSSET</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">11</div></td>
        <td>THE ROAD&nbsp;&nbsp;</td>
        <td>CORMAC Mc CARTHY</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">12</div></td>
        <td>THE MILL ON THE FLOSS</td>
        <td>GEORGE ELIOT&nbsp;</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">13</div></td>
        <td>THE PICKWICK PAPERS</td>
        <td>CHARLES DICKENS</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">14</div></td>
        <td>VANILLA BEANS &amp; BRODO&nbsp;</td>
        <td>ISABELLA DUSI</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">15</div></td>
        <td>GREAT EXPECTATIONS</td>
        <td>CHARLES DICKENS</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">16</div></td>
        <td>THE HOUSE IN SOUTH ROAD</td>
        <td>JOYCE STOREY</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">17</div></td>
        <td>PRIDE AND PREJUDICE</td>
        <td>JANE AUSTEN</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">18</div></td>
        <td>THE SHAKESPEARE CURSE</td>
        <td>J.L.CARRELL</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">19</div></td>
        <td>VOICES FROM THE WORLD OF JANE&hellip;.</td>
        <td>MALCOLM DAY</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">20</div></td>
        <td>SIZE 14 IS NOT FAT EITHER</td>
        <td>MEG CABOT</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">21</div></td>
        <td>SURGICALLY ENHANCED</td>
        <td>PAM AYRES</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">22</div></td>
        <td>WITH THESE HANDS</td>
        <td>PAM AYRES</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">23</div></td>
        <td>ON CHESIL BEACH</td>
        <td>IAN MCEWAN</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">24</div></td>
        <td>ORLANDO</td>
        <td>&nbsp;VIRGINIA WOOLF</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">25</div></td>
        <td>THE GREAT GATSBY</td>
        <td>SCOTT FITZGERALD&nbsp;</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">26</div></td>
        <td>ANTONY AND CLEOPATRA</td>
        <td>WILLIAM SHAKESPEARE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">27</div></td>
        <td>ROBINSON CRUSOE</td>
        <td>DANIEL DEFOE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">28</div></td>
        <td>THE WINTER'S TALE</td>
        <td>WILLIAM SHAKESPEARE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">29</div></td>
        <td>THE TAMING OF THE SHREW</td>
        <td>WILLIAM SHAKESPEARE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">30</div></td>
        <td>A PORTRAIT OF THE ARTIST AS A &hellip;.</td>
        <td>JAMES JOYCE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">31</div></td>
        <td>A MIDSUMMER NIGHT'S DREAM</td>
        <td>WILLIAM SHAKESPEARE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">32</div></td>
        <td>ROMEO AND JULIET</td>
        <td>WILLIAM SHAKESPEARE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">33</div></td>
        <td>THE CASTLE OF OTRANTO</td>
        <td>&nbsp;HORACE WALPOLE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">34</div></td>
        <td>SECRET OF THE MORNING</td>
        <td>VIRGINIA ANDREWS</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">35</div></td>
        <td>THE BODY FARM</td>
        <td>PATRICIA CORNWELL</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">36</div></td>
        <td>SOPHIE'S WORLD</td>
        <td>JOSTEIN GAARDER</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">37</div></td>
        <td>JANE EYRE</td>
        <td>CHARLOTTE BRONTE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">38</div></td>
        <td>ESSAYS IN LOVE&nbsp;</td>
        <td>ALAIN DE BOTTON</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">39</div></td>
        <td>DADDI'S LITTLE GIRL</td>
        <td>MARY HIGGINS CLARK</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">40</div></td>
        <td>THE SECRET HISTORY</td>
        <td>DONNA TARTT</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">41</div></td>
        <td>I AM CHARLOTTE SIMMONS</td>
        <td>TOM WOLFE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">42</div></td>
        <td>REMEMBER ME&nbsp;</td>
        <td>MARGARET THORNTON</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">43</div></td>
        <td>ANGELS IN THE GLOOM</td>
        <td>ANNE PERRY</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">44</div></td>
        <td>OPERATION OCEAN EMERALD</td>
        <td>ILKKA REMES</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">45</div></td>
        <td>THE HUMMINGBIRD'S DAUGHTER</td>
        <td>LUIS ALBERTO URREA</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">46</div></td>
        <td>THE GLASS DEMON&nbsp;</td>
        <td>HELEN GRANT</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">47</div></td>
        <td>UNTIL I FIND YOU</td>
        <td>JOHN IRVING&nbsp;</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">48</div></td>
        <td>SWITCHED,BOTHERED AND BEWILDERED</td>
        <td> SUZANNE MACPHERSON</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">49</div></td>
        <td>TALKING TO ADDISON</td>
        <td>JENNY COLGAN</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">50</div></td>
        <td>WILLIAM SHAKESPEARE</td>
        <td>PETER HOLLAND</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">51</div></td>
        <td>HALF A SISTER&nbsp;</td>
        <td>KELLY MCKAIN</td>
        <td><div align="right">RAGAZZI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">52</div></td>
        <td>HETTY FEATHER</td>
        <td>JACQUELINE WILSON&nbsp;</td>
        <td><div align="right">RAGAZZI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">53</div></td>
        <td>RONIA THE ROBBERS DAUGHTER</td>
        <td>ASTRIO LINDGREN</td>
        <td><div align="right">RAGAZZI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">54</div></td>
        <td>THE SUMMER I TURNED PRETTY</td>
        <td>JENNY HAN</td>
        <td><div align="right">RAGAZZI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">55</div></td>
        <td>IT'S NOT ROCKET SCIENCE &hellip;&hellip;</td>
        <td>CLIVE WHICHELOW&hellip;</td>
        <td><div align="right">RAGAZZI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">56</div></td>
        <td>RALPH'S PARTY&nbsp;</td>
        <td>LISA JEWELL</td>
        <td><div align="right">RAGAZZI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">57</div></td>
        <td>TUSCAN SOUP&nbsp;</td>
        <td>LOU WAKEFIELD</td>
        <td><div align="right">RAGAZZI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">58</div></td>
        <td>SORROWS AND SMILES</td>
        <td>DEE WILLIAMS&nbsp;</td>
        <td><div align="right">RAGAZZI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">59</div></td>
        <td>ALICE'S ADVENTURES IN WONDERLAND</td>
        <td>LEWIS CARROLL</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">60</div></td>
        <td>A TALE OF TWO CITIES</td>
        <td>CHARLES DICKENS</td>
        <td><div align="right">ADULTI&nbsp;</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">61</div></td>
        <td>THE PICTURE OF DORIAN GRAY</td>
        <td>OSCAR WILDE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">62</div></td>
        <td>THROUGH THE LOOKING GLASS</td>
        <td>LEWIS CARROLL</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">63</div></td>
        <td>ROBINSON CRUSOE</td>
        <td>DANIEL DEFOE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">64</div></td>
        <td>MOLL FLANDERS</td>
        <td>DANIEL DEFOE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">65</div></td>
        <td>SENSE AND SENSIBILITY</td>
        <td>JANE AUSTEN</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">66</div></td>
        <td>TWILIGHT</td>
        <td>STEPHENIE MEYER</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">67</div></td>
        <td>KIM</td>
        <td>RUDYARD KIPLING</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">68</div></td>
        <td>GULLIVER'S TRAVELS</td>
        <td>JONATHAN SWIFT</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">69</div></td>
        <td>THE DEVIL WEARS PRADA</td>
        <td>LAUREN WEISBERGER</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">70</div></td>
        <td>THE GREAT GATSBY</td>
        <td>F.SCOTT FITZGERALD&nbsp;</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">71</div></td>
        <td>MIDDLESEX</td>
        <td>JEFFREY EUGENIDES</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">72</div></td>
        <td>THE GIRL WHITH THE DRAGON TATTOO</td>
        <td>STIEG LARSSON</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">73</div></td>
        <td>WUTHERING HEIGHTS</td>
        <td>EMILY BRONTE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">74</div></td>
        <td>THE SCARLET LETTER</td>
        <td>NATHANIEL HAWTHORNE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">75</div></td>
        <td>THE ADVENTURES OF SHERLOCK HO..</td>
        <td>&nbsp;ARTHUR CONAN DOYLE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">76</div></td>
        <td>DRACULA</td>
        <td>BRAM STOKER</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">77</div></td>
        <td>GEISHA</td>
        <td>ARTUR GOLDEN</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">78</div></td>
        <td>GREAT EXPECTATIONS</td>
        <td>CHARLES DICKENS</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">79</div></td>
        <td>GULLIVER' S&nbsp; TRAWELS&nbsp;</td>
        <td>JONATHAN SWIFT</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">80</div></td>
        <td>TENDER IS THE NIGHT</td>
        <td>F.SCOTT FITZGERALD&nbsp;</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">81</div></td>
        <td>A THOUSAND SPLENDID SUNS</td>
        <td>KHALED HOSSEINI</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">82</div></td>
        <td>THE ENCHANTRESS OF FLORENCE</td>
        <td>SALMANRUSHDIE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">83</div></td>
        <td>MADAME BOVARY</td>
        <td>FLAUBERT</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">84</div></td>
        <td>THE KITE RUNNEV</td>
        <td>KHALED HOSSEINI</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">85</div></td>
        <td>BRIDGET JONES'S DIARY</td>
        <td>HELEN FIELDING</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">86</div></td>
        <td>L ' EDUCATION SENTIMENTALE</td>
        <td>FLAUBERT</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">87</div></td>
        <td>LITTLE WOMEN</td>
        <td>LOUISA MAY ALCOTT</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">88</div></td>
        <td>THE HAPPY PRINCE AND OTHER&hellip;&hellip;</td>
        <td>OSCAR WILDE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">89</div></td>
        <td>THE GREAT GATSBY</td>
        <td>F.SCOTT FITZGERALD&nbsp;</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">90</div></td>
        <td>DRESS YOUR FAMILY IN CORDUROY..</td>
        <td>&nbsp;DAVID SEDARIS</td>
        <td><div align="right">ADULTI&nbsp;</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">91</div></td>
        <td>DR JEKYLL AND MR HYDE</td>
        <td>ROBERT LOUIS STEVENSON&nbsp;</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">92</div></td>
        <td>PETIT DEJEUNER AU CREPUSCULE</td>
        <td>DICK</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">93</div></td>
        <td>ABOUT A BOY</td>
        <td>NICK HORNBY</td>
        <td><div align="right">RAGAZZI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">94</div></td>
        <td>LE PETIT PRINCE</td>
        <td>ANTOINE DE SAINT EXUPERY</td>
        <td><div align="right">RAGAZZI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">95</div></td>
        <td>THE WONDERFUL WIZARD OF OZ</td>
        <td>L.FRANK BAUM</td>
        <td><div align="right">RAGAZZI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">96</div></td>
        <td>THE BIBLE CODE</td>
        <td>MICHAEL DROSNIN</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">97</div></td>
        <td>GHOST SOLDIERS</td>
        <td>HAMPTON SIDES&nbsp;</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">98</div></td>
        <td>L'HOMME DE JERUSALEM</td>
        <td>DAVID GEMMELL</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">99</div></td>
        <td>ALL THE FINEST GIRLS</td>
        <td>ALEXANDRA STYRON</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">100</div></td>
        <td>LIVES OF THE SAINTS</td>
        <td>NINO RICCI</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">101</div></td>
        <td>DREAMER'S DICTIOMARY</td>
        <td>GARUDA</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">102</div></td>
        <td>TROPIC OF CAPRICORN</td>
        <td>HENRY MILLER</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">103</div></td>
        <td>HOLD ON&nbsp;</td>
        <td>ALAN GIBBONS</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">104</div></td>
        <td>MERCHANT OF VENICE&nbsp;</td>
        <td>WILLIAM SHAKESPEARE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">105</div></td>
        <td>A FAREWELL TO ARMS</td>
        <td>ERNEST HEMINGWAY</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">106</div></td>
        <td>BLU IRIS&nbsp;</td>
        <td>MARY OLIVER</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">107</div></td>
        <td>&nbsp;LITTLE BEAR'S FRIEND</td>
        <td>ELSE HOLMELUND MINARIK</td>
        <td><div align="right">RAGAZZI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">108</div></td>
        <td>LA FEMME FARD&egrave;E</td>
        <td>FRANCOISE SAGAN</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">109</div></td>
        <td>LEE CHILD</td>
        <td>ECHO BURNING</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">110</div></td>
        <td>A DAY IN APRIL</td>
        <td>BAXTER</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">111</div></td>
        <td>THE MONK&nbsp;</td>
        <td>MATTHEW LEWIS</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">112</div></td>
        <td>BAD TIMING</td>
        <td>KATE LE VANN</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">113</div></td>
        <td>THE RESURRECTION OF JOSEPH B&hellip;..</td>
        <td>JACK HODGINS</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">114</div></td>
        <td>CATCH ME WHEN I FALL</td>
        <td>NICCI&nbsp; FRENCH</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">115</div></td>
        <td>THE CITY OF FALLING ANGELS</td>
        <td>JOHN BERENDT</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">116</div></td>
        <td>CHARITY GIRL</td>
        <td>GEORGETTE HEYER</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">117</div></td>
        <td>&nbsp;SECRET BOOK OF GRAZIA DEI ROSSI</td>
        <td>JACQUELINE PARK</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">118</div></td>
        <td>ONE ROOM IN A CASTEL</td>
        <td>KAREN CONNELLY</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">119</div></td>
        <td>THE SCARLET LETTER</td>
        <td>NATHANIEL HAWTHORNE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">120</div></td>
        <td>THE ISLAND</td>
        <td>VICTORIA HISLOP</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">121</div></td>
        <td>A CAB AT THE DOOR</td>
        <td>V.S PRITCHETT</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">122</div></td>
        <td>THE BLACK DAHLIA</td>
        <td>JAMES ELLROY</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">123</div></td>
        <td>ROAD RAGE</td>
        <td>RUTH RENDELL</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">124</div></td>
        <td>A SUITABLE VENGEANCE</td>
        <td>ELIZABETH GEOGE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">125</div></td>
        <td>THRE GOODBYE SUMMER</td>
        <td>PATRICIA GAFFNEY</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">126</div></td>
        <td>ANGELS DEMONS</td>
        <td>DAN BROWN</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">127</div></td>
        <td>LE MATRIMOINE</td>
        <td>HERV&egrave; BAZIN</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">128</div></td>
        <td>THE PLAGUE DOGS</td>
        <td>RICHARD ADAMS</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">129</div></td>
        <td>THE PILGRIMS AND PLYMOUTH..</td>
        <td></td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">130</div></td>
        <td>THE PICTURE OF DORIAN GRAY</td>
        <td>OSCAR WILDE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">131</div></td>
        <td>OCEAN SEA&nbsp;</td>
        <td>ALESSANDRO BARICCO</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">132</div></td>
        <td>THE POISONWOOD BIBLE</td>
        <td>BARBARA KINGSOLVER</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">133</div></td>
        <td>THE SIXTENN PLEASURES</td>
        <td>ROBERT HELLENGA</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">134</div></td>
        <td>EUROPE TRAVELBOOK</td>
        <td>&nbsp;</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">135</div></td>
        <td>ANGELA'S ASHES</td>
        <td>FRANK MCCOURT</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">136</div></td>
        <td>APOLOGIZING TO DOGS</td>
        <td>JOE COOMER</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">137</div></td>
        <td>CRIMINAL INTENT</td>
        <td>WILLIAM BERNHARDT</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">138</div></td>
        <td>MIDNIGHT BAYOU</td>
        <td>NORA ROBERTS</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">139</div></td>
        <td>PIRANA TO&nbsp; SCURFY</td>
        <td>RUTH RENDELL</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">140</div></td>
        <td>SECRET PREY</td>
        <td>JOHN SANDFORD</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">141</div></td>
        <td>HARRY POTTER</td>
        <td>J.K.ROWLING</td>
        <td><div align="right">RAGAZZI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">142</div></td>
        <td>INDELIBLE</td>
        <td>KARIN SLAUGHTER</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">143</div></td>
        <td>KISSCUT</td>
        <td>KARIN SLAUGHTER</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">144</div></td>
        <td>ABAUT A BOY</td>
        <td>NIK HORNBY</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">145</div></td>
        <td>MURDER ON THE ORIENT EXPRESS&nbsp;</td>
        <td>AGATHA CHRISTIEN</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">146</div></td>
        <td>AHAB'S WIFE</td>
        <td>SENA JETER NASLUND</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">147</div></td>
        <td>NOVELLE ITALIANE</td>
        <td>ROBERT A.HALL JR</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">148</div></td>
        <td>EXTREMELY LOUD E INCREDIBLI CLOSE</td>
        <td>J.SAFRAN FOER</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">149</div></td>
        <td>LITTLE ALTARS EVERYWHERE</td>
        <td>REBECCA WELLS</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">150</div></td>
        <td>MIDWIVES</td>
        <td>BOHJALIAN</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">151</div></td>
        <td>ANIMAL DREAMS</td>
        <td>KINGSOLVER</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">152</div></td>
        <td>WHILE WAS GONE</td>
        <td>SUE MILLER</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">153</div></td>
        <td>PARALLEL TEXT</td>
        <td>TREVELYAN</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">154</div></td>
        <td>&nbsp;TUSCANY</td>
        <td>GUIDE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">155</div></td>
        <td>THE TORTILLA CURTAIN</td>
        <td>BOYLE&nbsp;</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">156</div></td>
        <td>THE HAPPY PRINCE&nbsp;</td>
        <td>OSCAR WILDE</td>
        <td><div align="right">RAGAZZI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">157</div></td>
        <td>FLORENCE</td>
        <td>GUIDE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">158</div></td>
        <td>WUTHERING HEIGHTS</td>
        <td>EMILY BRONTE</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">159</div></td>
        <td>ANIMAL FARM</td>
        <td>ORWELL</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">160</div></td>
        <td>FLORENCE</td>
        <td></td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">161</div></td>
        <td>ORLANDO BLOOM</td>
        <td>BIOGRAFY</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">162</div></td>
        <td>DRAWN QUARTERLY</td>
        <td>BLUTCH</td>
        <td><div align="right">RAGAZZI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">163</div></td>
        <td>L' HERITIER</td>
        <td>&nbsp;WINCH</td>
        <td><div align="right">ADULTI</div></td>
      </tr>
      <tr>
        <td height="17" align="right"><div align="center">164</div></td>
        <td>L HOMME DE GLACE&nbsp;</td>
        <td>RIC HOCHET</td>
        <td><div align="right">RAGAZZI</div></td>
      </tr>
    </table>
    <p><br />
      Per prenotare, compilare il modulo sottostante:<br />
    </p>
    <form  action="send1.php"  method="post" id="form">
    <table id="oxtable" style="width:47.9166666%; margin: 0 auto;">

                <tr >
                  <td><label >
                      <div align="left">Nome e cognome *:</div>
                      </label>
                  </td>
                  <td><input type="text" name="nome"  value="" id="q1" maxlength="20" size="10" />
                  </td>
                </tr>
                <tr >
                  <td><label >
                      <div align="left">Telefono* :</div>
                    </label>
                  </td>
                  <td><input type="text" name="telefono"  value="" id="q2" maxlength="20" size="10" /></td>
                </tr>
                <tr >
                  <td><label >
                      <div align="left">E-mail* :</div>
                    </label>
                  </td>
                  <td><input type="text" name="email"  value="" id="q3"  size="3" /></td>
                </tr>
                <tr >
                  <td><label >
                      <div align="left">Numero del libro* :</div>
                    </label>
                  </td>
                  <td><input type="text" size="10" name="numero"  value="" id="q4"  maxlength="10" maxsize="10" /></td>
                </tr>
                 <tr >
                  <td><label >
                      <div align="left">Titolo del libro* :</div>
                      </label>
                  </td>
                  <td><input type="text" name="titolo"  value="" id="q7"  maxlength="20" size="10" /></td>
                </tr>
                 <tr >
                  <td><label >
                      <div align="left">Da quale data (GG/MM/AAAA):</div>
                      </label>
                  </td>
                  <td><input type="text" name="giorno"  value="" id="q6"  maxlength="20" size="10" /></td>
                </tr>
                <tr >
                  <td height="50" valign="top" ><label>
                      <div align="left"><br />
                        Messaggio :</div>
                    </label>
                  </td>
                  <td height="50"><textarea cols="20" rows="3" name="messaggio" class="text" id="q5"></textarea>
                  </td>
                </tr>
                <tr >
                  <td><label>
                      <div align="left">privacy*</div>
                    </label></td>
                  <td><input type="checkbox"  name="privacy" class="other" id="q14_" value="accetto" />
                      <label>accetto </label>


                    
                    <br />
                  </td>
                </tr>

              <tr>
              <td>* Campi obbligatori</td>
              <td>&nbsp;</td>
              </tr>
              <tr>
              <td colspan="2" align="center">
				<input type="submit" class="btn" value="Invia" />          
              </td>
              
              </tr>              
            </table>
         </form>
        
    
    </p>
    
  <br />

	</div>
</div>
</div>
<?php
include("footer.php");
?>