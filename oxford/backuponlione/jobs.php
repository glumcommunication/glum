<?php
$thisPage = "Lavora con noi / Jobs";
$description = "Scuola di inglese, francese, spagnolo, tedesco, russo, arabo, cinese, italiano per stranieri, a Siena in Toscana, Italia. Scuola di lingue straniere a Siena. Viaggi studio per famiglie, ragazzi e adulti.";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>Lavora con noi / Jobs</h1>
		<img src="images/careers.jpg" alt="Lavora con noi" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
        <h2>
            LAVORA CON NOI / JOBS
        </h2>
        <p>    
            Do I have the right profile and qualifications to work with <strong>The New Oxford School</strong>?<br> 
            Please look at the following questionnaire and if you can answer yes to all the questions, feel free to <strong>send us your CV</strong>. 
        </p>
        <ul class="oxlist" style="list-style-type: decimal;">
            <li>I...
                <ul>
                    <li>
                        am native speaker OR
                    </li>
                    <li>
                        am bilingual with one parent who is an English native speaker OR
                    </li>
                    <li>
                        was educated in an English speaking country OR
                    </li>
                    <li>
                        have taken and hold an IELTS exam band 8 or 9 / Cambridge CPE.
                    </li>
                </ul>
            </li>
            <li>
                I have a college degree (preferred in the field of languages but not mandatory).
            </li>
            <li>
                I hold a CELTA, TESOL or equivalent certificate.
            </li>
            <li>
                I have a minimum of 2 years’ full time experience teaching English.
            </li>
            <li>
                I’m interested in teaching because I consider it a career and not just a hobby.
            </li>
            <li>
                I'm legally qualified to work in the EU
            </li>
            <li>
                I...
                <ul>
                    <li>
                        have experience working with children OR
                    </li>
                    <li>
                        am motivated to teach children.
                    </li>
                </ul>
            </li>
        </ul>
        <p>
            If you answered <strong>YES</strong> to ALL the above questions, please send us your CV to 
            <a href="mailto:dos@thenewoxfordschool.it">dos@thenewoxfordschool.it</a>
        </p>

	</div>
</div>
</div>

<?php
include("footer.php");
?>