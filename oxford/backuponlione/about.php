<?php
$thisPage = "Chi siamo";
$description = "Scuola di inglese, francese, spagnolo, tedesco, russo, arabo, cinese, italiano per stranieri, a Siena in Toscana, Italia. Scuola di lingue straniere a Siena.";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>Chi siamo</h1>
		<img src="images/about.jpg" alt="About" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>The New Oxford School World Languages</h2>
		
		<p>E' una scuola di lingue straniere, e <span class="redp">Agenzia Formativa Accreditata dalla Regione Toscana, in possesso di certificazione UNI EN ISO 9001:2008, sede e Centro di Supporto 
		per la Toscana del TRINITY COLLEGE LONDON</span>, accreditata presso <span class="redp">FORMA.TEMP</span> con certificato n. 0741.1180, registrata  all’ ECM, Educazione Continua in Medicina presso 
		il Ministero della Salute.</p>
		
		<p>Grazie all’Accreditamento da parte della Regione Toscana, c’è la possibilità  di utilizzare presso il nostro Istituto voucher 
		formativi ottenuti a seguito di pubblicazione di bandi della Regione toscana e/o della Provincia e partecipare a corsi formativi 
		riconosciuti organizzati dalla Scuola.</p>
		
		<p>The New Oxford School nasce nel 2001 ed opera con successo nel settore della formazione linguistica grazie ad innovative 
		formule di apprendimento basate sulla comunicazione.</p>
		
		<p>The New Oxford School si occupa da sempre dell’insegnamento della lingua inglese, ormai diventata esigenza fondamentale per 
		chi vuole vivere seguendo  ritmi della vita contemporanea: parlare la lingua inglese e le altre lingue del mondo è oggi la 
		chiave d’accesso in tutti i settori.</p>
		
		<p>Parlare non significa solamente comunicare ma anche conoscersi a livello personale e quindi sentirsi cittadini del mondo. 
		Negli ultimi anni la New Oxford School si è specializzata nell’insegnamento dell’inglese, tedesco, francese, spagnolo e da poco 
		è possibile imparare anche il russo, il cinese, l'arabo, il giapponese il portoghese e l’italiano per chi è straniero.</p>
		
		
		<p>I nostri servizi:
			<ul class="oxlist">
			<li>Corsi individuali e di gruppo per tutti i livelli di conoscenza di ogni lingua</li>
			<li>Corsi per bambini</li>
			<li>Corsi di business english, preparazione a certificazione BEC</li>
			<li>Corsi di inglese giuridico</li>
			<li>Preparazione per esami TRINITY e CAMBRIDGE</li>
			<li>Corsi estivi e viaggi studio all’estero</li>
			<li>Corsi di italiano per stranieri</li>
			<li>Corsi per aziende</li>
			<li>Servizio traduzioni</li>
			<li>Corsi di preparazione per esami riconosciuti</li>
		</ul></p>
		
		<h3>Viaggi studio all'estero</h3>
		 
		<p>Dal 2008 la New Oxford School offre la possibilità agli allievi adulti di studiare l'inglese all'estero e dall'estate 2009 
		anche ai ragazzi dai 7 anni in su in Inghilterra, Stati Uniti, Irlanda, Malta Scozia.<br />
		Per informazioni contattare la Scuola in orario 9,00 - 20,00 dal lunedì al venerdì oppure contattare direttamente il responsabile 
		viaggi studio all'estero al seguente indirizzo e-mail: <a href="mailto:jeremy@thenewoxfordschool.it">jeremy@thenewoxfordschool.it</a>.</p>
		<h3>Lo staff</h3>
		
		<p>Lo Staff della New Oxford School è disponibile negli orari di ufficio a dare tutte le informazioni e a  rispondere ad ogni 
		tipo di esigenza.<br />
		Esperienza, professionalità, serietà e disponibilità sono le carte vincenti del team della Scuola.<br />
		I docenti sono tutti madrelingua, qualificati all’insegnamento e provenienti da diverse e molteplici esperienze.<br />
		Il clima sereno e il forte entusiasmo dell’intero Staff contraddistinguono la T.N.O.S..</p>		
		
	</div>
</div>
</div>

<?php
include("footer.php");
?>