<?php
$thisPage = "About us";
$description = "Our Italian group courses are designed for anyone who wants to learn or improve their Italian language with qualified and experienced teachers.";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>Italian Courses for Foreigners</h1>
		<img src="../images/english-student.jpg" alt="" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>The New Oxford School ITALIAN COURSES FOR FOREIGNERS</h2> 

		<p>Our Italian group courses are designed for anyone who wants to learn or improve their Italian language with qualified and experienced teachers.<br />
		After sitting an oral and written entrance exam to determine your Italian level, you will be placed in study groups with a minimum of 3 and a maximum of 10 other students.<br />
		The timetable and length of the course will be organised according to the preferences of the participants of each group within the morning hours of Monday to Friday, 
		but also Saturday morning and afternoon as well.<br />
		The Italian courses are based on a grammatical programme related to the six principal linguistic competence levels (Beginner - Elementary - Intermediatel1 - Intermediate2 - 
		Advanced-Superior) yet also gives plenty of space to conversation.</p>
		 
		<h3>GENERAL INFORMATION</h3> 
		
		<p><strong>LESSON LENGTH</strong><br />
		Please note that one lesson is considered to be 60 minutes in length.</p> 
		
		<p><strong>NUMBER OF STUDENTS PER CLASS</strong><br />
		For mini-group courses the minimum number of participants is 3 and the maximum is 6.</p>
		
		<p><strong>LEVELS OF ABILITY<br />
		Students are placed in one of the following levels following the entrance test:</strong>
			<ul class="oxlist">		
				<li>Beginner</li>
				<li>Elementary</li>
				<li>Intermediate 1</li>
				<li>Intermediate 2</li>
				<li>Advanced</li>
				<li>Superior</li>
			</ul>
		</p>
		<p>Our programs are based on the requirements of the Common European Framework.</p>
		
		<p><strong>COURSE COSTS INCLUDE:</strong>
			<ul class="oxlist">		
				<li>all course materials</li>
				<li>certificate of attendance</li> 
				<li>social activities</li>
			</ul>		
		</p> 
		
		<p><strong>ADDITIONAL SERVICES:</strong>
			<ul class="oxlist">
				<li>internet access</li>
				<li>screening of Italian films in original language</li>
			</ul>		
		</p>
		
		<p><strong>MINI-GROUP COURSE</strong></p>
		<div class="redp">Standard Course</div>
		
		<p><strong>Duration</strong>: minimum 2 weeks<br />
		<strong>Lessons</strong>: 3/day = 15 hours/week<br />
		<strong>Level</strong>: all levels<br /> 
		<strong>Class Size</strong>: from 3 to 8 participants<br />
		<span class="redp"><a href="../pdf/price_list.pdf" title="Download">PRICE LIST</a>&emsp;<a href="booking_form.php" title="Booking">BOOK IT NOW!</a></span></p>				
		<p>The Standard Course consists of 15 group lesson hours per week, the minimum number of hours we offer for a group course. This course 
		lasts for a minimum of two weeks. Lessons are from 10:00am to 1:30pm. It is ideal for those students who wish to have a lot of free time</p>
		
		<div class="redp">Intensive Course</div>
		<p>Starting Dates: every Monday<br /> 
		Duration: minimum one week<br />
		Lessons: 4/day = 20 hours/week<br />
		Level: all levels<br />
		Class: from 3 to 6 participants</p>
		<p><span class="redp"><a href="../pdf/price_list.pdf" title="Download">PRICE LIST</a>&emsp;<a href="booking_form.php" title="Booking">BOOK IT NOW!</a></span></p>				
		<p>The Intensive Course consists of 20 group lesson hours per week. There is a maximum of six students. Lessons start at 09:00am and 
		finish at 1:30pm.</p>
		<p><strong>INDIVIDUAL LESSONS</strong></p>
		<p>
		<strong>Starting Dates</strong>: every day<br />
		<strong>Duration</strong>: minimum 3 hours<br />
		<strong>Level</strong>: all levels<br />
		<strong>Class Size</strong>: one participant<br />
		<span class="redp"><a href="../pdf/price_list.pdf" title="Download">PRICE LIST</a>&emsp;<a href="booking_form.php" title="Booking">BOOK IT NOW!</a></span></p>
		<p>Our individual courses are adapted to the specific requirements of the student. The content of the lessons is planned on the needs 
		of the individual participant so that he or she may learn in a relaxed atmosphere with the undivided attention of the teacher.</p>
		<p><strong>COMBINED COURSE</strong></p>
		<p>
		<strong>Starting Dates</strong>:             every Monday<br />
		<strong>Duration</strong>:                     minimum one week<br />
		<strong>Group Lessons</strong>:             3/day = 15 hours/week<br />
		<strong>Group Class size</strong>:           from 3 to 6 participants<br /> 
		<b>Individual Lessons</b>:        4/week<br />
		<strong>Level</strong>: all levels<br />                                                
		<span class="redp"><a href="../pdf/price_list.pdf" title="Download">PRICE LIST</a>&emsp;<a href="booking_form.php" title="Booking">BOOK IT NOW!</a></span></p>
		<p>The Combined Course, which can be taken for a minimum of one week, combines group lessons in the morning with individual 
		lessons in the afternoon (from 13.00 to17.00).  This option gives students the opportunity to broaden their knowledge of 
		Italian during morning group lessons and the chance to study specific topics intensively in the afternoon during their 
		short stay in Siena.</p> 
		
		<p><strong>TRIMESTRAL  COURSE</strong>
		<p>
		<strong>Starting dates</strong>:             every Monday<br /> 
		<strong>Duration</strong>:                     12 weeks<br />
		<strong>Lesson</strong>:                        3/day = 15 hours/week<br />
		<strong>Level</strong>:                                   all levels<br />
		<strong>Class size</strong>:                    from three to eight participants<br /> 
		<span class="redp"><a href="../pdf/price_list.pdf" title="Download">PRICE LIST</a>&emsp;<a href="booking_form.php" title="Booking">BOOK IT NOW!</a></span></p>
		<p>The Trimestral Course, lasting 12 weeks, is suitable for students who would like to be fully immersed in the language 
		with intensive speaking, listening, reading and writing practice.</p>
		
		<p class="redp"><a href="../pdf/Metodi_pagamento.pdf" title="Download">
		<span><img src="../images/pdfdownload.png" width="32" height="32" alt="Pdf" style="height:32px;width:32px;vertical-align: middle;" /></span>
		 Payment details</a></p>
		<p class="redp"><a href="../pdf/terms.pdf" title="Download">
		<span><img src="../images/pdfdownload.png" width="32" height="32" alt="Pdf" style="height:32px;width:32px;vertical-align: middle;" /></span>
		 TERMS AND CONDITIONS</a></p>
		<p class="redp"><a href="../pdf/Request_information.pdf" title="Download"> 
		<span><img src="../images/pdfdownload.png" width="32" height="32" alt="Pdf" style="height:32px;width:32px;vertical-align: middle;" /></span>
		Information request form</a></p>		
	</div>
</div>
</div>

<?php
include("footer.php");
?>