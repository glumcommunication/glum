<?php
$thisPage = "About us";
$description = "School of world languages and of Italian for foreigners based in Siena Tuscany Italy - The New Oxford Schol is a Italian language school to learn italian in Italy";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>About us</h1>
		<img src="../images/about.jpg" alt="About" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>Italian language school in Siena to study italian in Italy</h2> 


			<p>Is a foreign language school and formative agency accredited with the Tuscan Region, a Support Centre of the Trinity College of London.<br />  
			Thanks to the accreditation by the Tuscan Region, there is the possibility to utilize formative vouchers obtained from the Tuscan Region and/or the 
			Province of Siena in order to participate in recognised formative courses run by our institute.<br />
			The New Oxford School began in 2001 and has had great success in the language education sector due to its innovative teaching system based on conversation. <br />
			The New Oxford School has always offered English language courses, by now an fundamental for anyone who lives a busy modern life: being able to speak 
			English and other world languages is a key to success in every sector.<br />
			To speak doesn’t mean only communication, but also getting to know each other on a personal level and therefore becoming a true ‘world’ citizen.  In recent years, 
			The New Oxford School has become specialised in the teaching of English, German, French, Spanish, and recently has added Russian, Chinese and Italian for foreigners 
			to its list of world languages.</p>

			<p>Our services: 
				<ul class="oxlist">
					<li>Individual and group courses for all levels of every language</li> 
					<li>Children’s courses</li>
					<li>Business English, Preparation for the BEC certificate</li> 
					<li>Legal English</li>
					<li>Summer Courses and Overseas Study Tours</li> 
					<li>Italian for Foreigners</li>
					<li>Courses organised for companies, big and small</li> 
					<li>Translation service</li>
					<li>Recognised exam preparation courses</li>
				</ul>
			</p>
					
			<h3>The Staff</h3>
			
			<p>The staff of The New Oxford School are available during office hours to give any information and to respond to every need. Experience, professionalism, and helpfulness: 
			this is what makes up our winning team at The New Oxford School. All our teachers are native speakers, fully certified in teaching and have varying teaching 
			backgrounds and specialities. The easy-going atmosphere and the enthusiasm of the entire staff are what sets The New Oxford School apart.</p>
		
	</div>
</div>
</div>

<?php
include("footer.php");
?>