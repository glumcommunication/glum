<?php
$thisPage = "Home";
$description = "ist eine von der Region Toskana anerkannte Sprachschule und Weiterbildungseinrichtung. Weiter ist die Schule Supportcentrum des Trinity 
College London und registriert beim Gesundheitsministerium Italiens für das Programm ECM (Weiterbildung für Mediziner)";
include("header.php");
include("menu.php");
?>

	<div id="main">
<?php include("../flags_int.php"); ?>	
		<div id="mainbox">
			<img src="../images/oxfordhome.jpg" alt="Home" />
		</div>

	<div class="rightHome">

		<div class="boxhomeRight1">
		<h1>Trinity</h1>
			<a href="http://www.trinitycollege.it/" target="_blank"><img src="../images/trinity.png" alt="Trinity" /></a>
		</div>	
		<div class="boxhomeRight2">
		<h1>Öffnungszeiten</h1>
		<div class="mtext">
			<h3>Bürozeiten</h3>
			<p>Von Montag bis Freitag 9:00 – 13:00 Uhr /15:00 - 19:00<br />
			</p>
                        			<p style="color:#cf142b;font-weight:bold;">In den Monaten Juli und August wird das Sekretariat am Samstag geschlossen</p>

			<h3>Unterrichtszeiten</span></h3>
			<p>Von Montag bis Freitag 9:00 – 22:00 Uhr<br />
			Samstags von 9:00 bis 18:00 Uhr</p>
		</div>
		</div>


</div>
	<div class="leftHome">
	<div class="boxhomeLeft">
		<h1>Diagnostic Test</h1>
			<img src="../images/test.png" alt="Diagnostic Test" />
		</div>
	</div>
</div>




<?php
include("footer.php");
?>