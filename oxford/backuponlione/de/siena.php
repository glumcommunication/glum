<?php
$thisPage = "Siena und die Toskana";
$description = "Berühmt ist die Toskana auch für ihre zahlreichen Monumente und gut erhaltenen Altstädte. Hervorzuheben sind besonders Städte wie Florenz, Siena, Pisa und Lucca.";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>Siena und die Toskana</h1>
		<img src="../images/sienapiazza.jpg" alt="Siena" />
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>Toskana</h2>
		
		<p>800 Jahre vor Christus werden die ersten Zeugnisse einer Kultur im Territorium Centro Italia, dem zentralitalienischem Gebiet, bekannt. 
		Die Zeugnisse stammen von dem mysteriöse Volk der Etrusker und die Region hat danach ihre Namen Etruria, Tuscia für die Römer und später Tuscania und Toscana. 
		Nach den Etruskern und Römern, der Herrschaft der Ostgoten, Byzantinern und Langobarden hat die Toskana bedeutendes zum Italienischen Rinascimento 
		beigetragen, zuerst mit Dante Alighieri dann mit Giotto und weiteren Künstlern. Während des Rinascimento dominierte zuerst in Florenz die Familie 
		der Medici das Gebiet, dann, nach deren Auslöschung, die Familie der Lorena aus Österreich. Die Familie Lorena hat unter anderem zur Sanierung der Gegend 
		der Maremma beigetragen und so den Aufstieg der Toskana zu einer der reichsten Gebiete Italiens unterstützt. In der zweiten Hälfte des 19 Jahrhunderts ging 
		die Toskana an das Königreich Italien.</p>
		<p>Die Toskana ist eine der wichtigsten Regionen hinsichtlich historischem, künstlerischen und landschaftlichem Erbe. Die Toskana hat ca. 400Km Küstenlinie 
		inkl. derer der Inseln des Arcipelago Toscano , darunter Elba. Die Toskana besteht hauptsächlich aus Hügelland, wie der berühmten Crete Senese bei Siena.</p>
		<h2>Siena</h2>
		<p>Berühmt ist die Toskana auch für ihre zahlreichen Monumente und gut erhaltenen Altstädte. Hervorzuheben sind besonders Städte wie Florenz, Siena, Pisa und Lucca. 
		Auch kleinere Städte wie San Gimignano mit seinen einzigartigen mittelalterlichen Hochhäusern oder Pienza, der Stadt vom Reissbrett sind unbedingt einen Besuch wert. 
		&#8232;Siena ist eine Stadt in der Toscana Centrale und berühmt für ihr künstlerisches Erbe und ihre architektonische Einheit. Siena ist auch Weltkulturerbe der Unesco.</p> 
		<p>Siena wurde als Römische Kolonie zur Zeit des Imperatore Augusto gegründet und erhielt den Namen Siena Julia . Die wenigen Informationen die von der Gründung erhalten sind, 
		deuten auf eine bestehende Etruskische Siedlung hin, in der sich dann später Römische Truppen niederliessen. Im 10 Jahrhundert befindet sich Siena entlang wichtiger 
		Handels und Pilgerrouten die nach Rom führen und wird dadurch eine sehr bedeutende mittelalterliche Stadt. Im 13 Jahrhundert organisiert sich die Stadt in einer so 
		genannten consularen Komunalverwaltung und beginnt ihr Herrschaftsgebiet auszubauen und Allianzen zu schmieden. Dieser Aufschwung und Ausbau des Herrschaftsanspruches 
		führt zu einer Konkurrenzsituation zu Florenz. Seit der ersten Hälfte des 13 Jahrhunderts prosperiert Siena und wird ein bedeutendes Handelszentrum mit guten 
		Beziehungen zum Kirchenstaat. Die Geldverleiher von Siena waren wichtige Partner von Rom welches Anleihen bei diesen ersten Bankern aufnahm. Nach der Pest von 1348 begann 
		für Siena ein Abschwung, der in der Herrschaft von Florenz über Siena mündete.</p>
		<p>Siena hat eine lange kulinarische Tradition, wohl auch durch seine grosse Anzahl an Gasthäusern entlang der Via Francigena , der Pilgerstrasse nach Rom. Die 
		Hauptaktivitäten heute sind der Dienstleistungssektor, der Tourismus und die Landwirtschaft.</p>

	</div>
</div>
</div>

<?php
include("footer.php");
?>