<?php
$thisPage = "Wir über uns";
$description = "ist eine von der Region Toskana anerkannte Sprachschule und Weiterbildungseinrichtung. Weiter ist die Schule Supportcentrum des Trinity 
College London und registriert beim Gesundheitsministerium Italiens für das Programm ECM (Weiterbildung für Mediziner)";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>Wir über uns</h1>
		<img src="../images/about.jpg" alt="about" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>Die New Oxford School World Languages</h2> 
		<p>Die New Oxford School World Languages ist eine von der Region Toskana anerkannte Sprachschule und Weiterbildungseinrichtung. Weiter ist 
		die Schule Supportcentrum des Trinity College London und registriert beim Gesundheitsministerium Italiens für das Programm ECM (Weiterbildung für Mediziner).<br />
		Die Zusammenarbeit mit der Region Toskana bietet die Möglichkeit in unseren Räumlichkeiten Listen mit Förderprogrammen der Region einzusehen. <br />
		Die New Oxford School ist 2001 gegründet und arbeitet seither mit Erfolg in der Sprachvermittlung, was den innovativen Lehrmethoden bei denen die 
		Kommunikation im Vordergrund steht zu verdanken ist.<br />
		Die New Oxford School bietet sein Beginn Englischunterricht, unverzichtbar für alle die heute Erfolg im Beruf haben wollen. Englisch als 
		Fremdsprache zu beherrschen und darüber hinaus weiter Sprachen zu lernen ist heutzutage der Schlüssel für Erfolg in allen Bereichen.<br />
		Fremdsprachen sprechen bedeutet nicht nur Kommunizieren, sondern auch „sich zu kennen“ und sich so als Mittglied der Weltgemeinschaft zu fühlen. 
		Die New Oxford School bietet aktuell folgende Sprachen an: Englisch, Deutsch, Französisch, Spanisch, Russisch, Chinesisch und Italienisch für Ausländer.</p> 
		<p>Unserer Service:
		<ul class="oxlist">
			<li>Individualkurse und Gruppenkurse in allen Stufen und jeder Sprache</li>
			<li>Sprachkurse für Kinder</li>
			<li>Geschäftsenglisch, Ausbildung und Zertifikat nach BEC</li>
			<li>Juristenenglisch</li>
			<li>Ferienkurse und Studienreisen</li>
			<li>Italienischkurse für Ausländer</li>
			<li>Sprachkurse für Firmen</li>
			<li>Übersetzungen</li>
			<li>Examensvorbereitung</li>
		</ul></p>
		<h3>Das Team:</h3>
		<p>Das Team der New Oxford School steht Ihnen zu den Bürozeiten für alle Ihre Fragen gerne zur Verfügung.<br />
		Erfahrung, Professionalität, Seriösität und Verfügbarkeit sind die Eigenschaften des Lehrerteams.<br />
		Alle Sprachlehrer sind in ihrem Unterrichtsfach Muttersprachler und als Lehrkraft ausgebildet und können auf verschiedenste Qualifikationen verweisen.</p>
	</div>
</div>
</div>

<?php
include("footer.php");
?>