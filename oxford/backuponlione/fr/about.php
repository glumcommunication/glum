<?php
$thisPage = "Qui sommes-nous?";
$description = "Est une école de langues étrangères et Agence de formation aggrégée par la Région de Tosacane, siège et Centre de Support du Trinity College de Londres, et inscrite à l’ECM, Education Continue en Médecine, auprès du Ministère de la Santé.";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>Qui sommes-nous?</h1>
		<img src="../images/about.jpg" alt="" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>The New Oxford School écoles de langue italienne</h2> 


			<p>Est une école de langues étrangères et Agence de formation aggrégée par la Région de Tosacane, siège et Centre de Support du Trinity College de Londres, 
			et inscrite à l’ECM, Education Continue en Médecine, auprès du Ministère de la Santé.<br />
			Grâce au crédit de la Région Toscane, il est possible d’utiliser dans notre Institut les vouchers de formation obtenus à la suite de la publication des 
			avis de la Région Toscane et/ou de la Province et de participer aux cours de formation reconnus organisés par l’Institut.<br />
			La New Oxford School nait en 2001 et opère avec succès dans le secteur de la formation linguistique grâce à des formules innovatives d’enseignement 
			basés sur la communication.<br />
			La New Oxford School s’est toujours occupée de l’enseignement de la langue anglaise, étant devenue une exigeance fondamentale pour ceux qui veulent 
			vivre en suivant des rythmes tout à fait contemporains: parler la langue anglaise et les autres langues du monde est aujourd’hui la clé d’accès à tous les secteurs.<br />
			Parler ne signifie pas uniquement communiquer, mais également se connaitre au plan personnel et donc se sentire citadin du monde. Durant ces dernières 
			années la New Oxford School s’est spécialisée dans l’enseignement de l’anglais, de l’allemand, du français, de l’espagnol et depuis peu il est possible 
			d’appprendre également le russe, le chinois et l’italien pour les étrangers.</p>

			<p>Nos services: 
				<ul class="oxlist">
					<li>Cours individuels et de groupe pour tous les niveaux de connaissance de chaque langue</li> 
					<li>Cours pour enfants</li>
					<li>Cours de business anglais, préparation à la certification BEC</li> 
					<li>Cours d’anglais juridique</li>
					<li>Cours d’été et voyages d’étude à l’étranger</li> 
					<li>Cours d’italien pour étrangers</li>
					<li>Cours pour les entreprises</li> 
					<li>Services de traduction</li>
					<li>Cours de préparation pour les examens officiels</li>
				</ul>
			</p>
					
			<h3>L'équipe</h3>
			
			<p>L’équipe de la New Oxford School reste disponible aux horaires de bureau pour vous donner toutes les informations et répondre à toutes vos exigeances.<br />
			Expérience, professionnalité, sérieux et disponibilité sont les cartes gagnantes de l’équipe de notre école. Les enseignants sont tous de langue maternelle, 
			qualifiés pour le professorat et issus de multiples et diverses expériences.<br />
			Le climat serein et le fort enthousiasme de toute l’équipe: voilà ce qui distingue la New oxford School.</p>
		
	</div>
</div>
</div>

<?php
include("footer.php");
?>