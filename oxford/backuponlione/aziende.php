<?php
$thisPage = "Corsi per Aziende";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
<div class="sbox">
	<h1>Corsi per Aziende</h1>
		<img src="images/english-company.jpg" alt="" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>DESCRIZIONE DEL CORSO</h2>
		<p>The New Oxford School organizza corsi di tutti i livelli di lingue inglese, tedesca, spagnola, 
		francese, russa e cinese presso la nostra sede e presso le sedi delle aziende, sia corsi di gruppo 
		che individuali.</p> 
		<p>I corsi hanno l’obiettivo di dare un concreto ausilio nelle situazioni reali di lavoro, di arricchire 
		il vocabolario necessario alle esigenze professionali, di dare una maggior sicurezza ai dipendenti nel 
		minor tempo possibile.</p> 
		<p>Orari e giorni da concordare, ovviamente, in base agli impegni e orari di lavoro.</p> 
		<p>Da anni la New Oxford School organizza corsi, soprattutto di lingua inglese, per aziende che 
		operano sul territorio senese.</p>
		<p>È possibile seguire le lezioni presso la nostra sede o direttamente in azienda concordando giorni e 
		orari delle lezioni in base alle esigenze lavorative ed è possibile strutturare il corso in base alle 
		proprie esigenze professionali.</p>
		<p>I corsi per le aziende che lo richiedono possono essere organizzati in gruppi o individuali e con 
		frequenza da stabilire.</p>
	</div>
</div>
</div>
<?php
include("footer.php");
?>