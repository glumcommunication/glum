<?php
$thisPage = "Corsi di Inglese";
$description = "Scuola di inglese, francese, spagnolo, tedesco, russo, arabo, cinese, italiano per stranieri, a Siena in Toscana, Italia. Scuola di lingue straniere a Siena.";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>Corsi di Inglese</h1>
		<img src="images/english-student.jpg" alt="" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>DESCRIZIONE DEL CORSO</h2>
		
			<p>I corsi di lingua inglese soddisfano le più svariate esigenze.<br />
			Sono indirizzati ai bambini in età pre-scolare, studenti di età scolare, 
			adolescenti, studenti universitari, adulti che lavorano.<br />
			Dopo un test d’accesso obbligatorio, sia scritto che orale, gratuito e senza 
			impegno, vengono organizzati i gruppi in base alle preferenze di giorni ed orari 
			di frequenza di ogni partecipante.<br />
			A seconda delle richieste, la New Oxford School organizza anche corsi di lingua 
			inglese in alcuni comuni limitrofi a Siena.</p>

		<h3>Insegnanti</h3>

			<p>I nostri insegnanti, tutti  madrelingua, vengono selezionati in base al loro 
			curriculum; prerogativa necessaria per i docenti di lingua inglese, per esempio, 
			è essere in possesso di un’abilitazione all’insegnamento, quindi di una 
			certificazione T.E.F.L. (Teaching English as a Foreign Language), o C.E.L.T.A. 
			(Certificate in English Language Teaching to Adults) o altre certificazioni che 
			attestino l'abilitazione all'insegnamento. Stesso criterio vale per i docenti di 
			tutte le altre lingue.<br />
			Gli insegnanti vengono valutati anche in base alla loro esperienza professionale 
			e alla loro capacità di lavorare in gruppo perché è proprio il corpo docente 
			professionale, motivato e sereno ad essere una delle caratteristiche che 
			contribuisce da sempre al successo della New Oxford School.</p>

		<h3>Metodo d’insegnamento</h3>

			<p>La metodologia d’insegnamento è basata sulla conversazione, il programma 
			grammaticale di ogni livello viene svolto interamente durante il corso ma è di 
			supporto alla conversazione, il metodo non è noioso, lento bensì fin dalla prima 
			lezione stimolante, divertente; i processi d’apprendimento sono molto piu’ veloci 
			e riscontrabili già dopo poche lezioni.<br />
			Durante le lezioni s’impara a pensare in lingua e di conseguenza ad esprimersi 
			senza far riferimento alla nostra lingua madre; vengono riproposte situazioni di 
			vita quotidiana e lavorative in base al livello di conoscenza degli studenti.</p>
			
		<h3>Livelli</h3>

			<p>I nostri corsi di lingua inglese, basati sulla conversazione, prevedono allo stesso 
			tempo lo svolgimento per ciascun livello del programma grammaticale in linea sia con 
			quanto previsto dal Quadro Comune di Riferimento Europeo sia con gli esami GESE del 
			Trinity College London.</p>
			<table id="oxtable">
			<!-- Table header -->
				<thead>
					<tr>
						<th class="lsix">The New Oxford School</th>
						<th class="lsix">Numero ore</th>
						<th class="lsix">QCER</th>
						<th class="lsix">Cambridge English</th>
						<th class="lsix">Trinity College ISE</th>
						<th class="lsix">Trinity College GESE</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Beginner</td>
						<td>54</td>
						<td>A1</td>
						<td></td>
						<td></td>
						<td>Grade 1-2</td>
					</tr>
					<tr>
						<td>Elementary</td>
						<td>54 + 50</td>
						<td>A2</td>
						<td>KET</td>
						<td>ISE 0</td>
						<td>Grade 3</td>
					</tr>
					<tr>
						<td>Upper - Elementary</td>
						<td>54</td>
						<td>A2+</td>
						<td></td>
						<td></td>
						<td>Grade 4</td>
					</tr>
					<tr>
						<td>Pre - Intermediate</td>
						<td>54 + 50</td>
						<td>B1</td>
						<td>PET</td>
						<td>ISE I</td>
						<td>Grade 5-6</td>
					</tr>
					<tr>
						<td>Intermediate</td>
						<td>54</td>
						<td>B1+</td>
						<td></td>
						<td></td>
						<td>Grade 7</td>
					</tr>
					<tr>
						<td>Upper - Intermediate</td>
						<td>54 + 50</td>
						<td>B2</td>
						<td>FCE</td>
						<td>ISE II</td>
						<td>Grade 8-9</td>
					</tr>
					<tr>
						<td>Pre - Advanced</td>
						<td>54</td>
						<td>B2+</td>
						<td></td>
						<td></td>
						<td>Grade 10</td>
					</tr>
					<tr>
						<td>Advanced</td>
						<td></td>
						<td>C1</td>
						<td>CAE</td>
						<td>ISE III</td>
						<td>Grade 11</td>
					</tr>
					<tr>
						<td style="border: 0;">Proficiency</td>
						<td style="border: 0;">54 + 50</td>
						<td style="border: 0;">C2</td>
						<td style="border: 0;">CPE</td>
						<td style="border: 0;">ISE IV</td>
						<td style="border: 0;">Grade 12</td>
					</tr>
				</tbody>
			</table>
			
		<h3>Giorni e orari di frequenza</h3>

			<p>Gli allievi possono concordare con noi i giorni e le fasce orarie di frequenza a 
			loro piu’ consoni, pur rispettando le esigenze organizzative della Scuola.<br />
			E’ possibile fare lezione in sede dal lunedì al venerdì dalle ore 9,00 fino alle ore 
			22,00 e il sabato dalle ore 9,30 alle ore 13,00.<br />
			Per quanto riguarda i corsi individuali, indicati proprio per chi per motivi personali o 
			professionali non riesce a seguire un gruppo di lavoro, l’allievo può concordare di volta 
			in volta il giorno e l’orario di lezione con il proprio insegnante, senza mai perdere 
			nessuna lezione.<br />
			Per i corsi di gruppo vengono concordati due giorni alterni a settimana e una fascia oraria, 
			piu’ richiesta pomeridiana e serale, che consente soprattutto a persone adulte che lavorano, 
			di poter seguire senza difficoltà tutte le lezioni.<br />
			La Scuola inoltre dà la possibilità di poter recuperare le lezioni che si sa in anticipo 
			di dover perdere, in altri gruppi dello stesso livello in altri giorni e orari.</p>

		<h3>Attestato di frequenza</h3>

			<p>A fine corso la Scuola dà la possibilità a tutti gli allievi che lo desiderano di poter 
			sostenere un esame finale, scritto e orale che consente,se superato, di ottenere un attestato di 
			frequenza al corso su riportate le ore totali di corso e il livello conseguito.</p>		
		
		<h3>METODI DI PAGAMENTO</h3>
		<p>I pagamenti di tutti i nostri corsi di lingue, sia per adulti che per bambini, possono avvenire in 
			un'unica soluzione o dilazionati in pi&ugrave; mesi.</p>
	</div>
</div>
</div>

<?php
include("footer.php");
?>