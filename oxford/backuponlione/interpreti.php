<?php
$thisPage = "Interpretariato";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
<div class="sbox">
	<h1>Interpretariato</h1>
		<img src="images/interprete.jpg" alt="" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>SERVIZIO INTERPRETARIATO</h2>
		
		<p>
		La New Oxford School offre anche servizi di interpretariato delle seguenti tipologie:
		</p>
			<ul class="oxlist">
				<li>di compagnia</li>
			   <li>tecnico trattativa</li>
			   <li>consecutiva</li>
			   <li>simultanea</li>
    		</ul>
    	<p>
    	È possibile offrire il servizio di interpretariato in lingua inglese, tedesca, francese, spagnola e russa.
    	</p>
	</div>
</div>
</div>
<?php
include("footer.php");
?>