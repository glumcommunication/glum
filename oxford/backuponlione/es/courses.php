<?php
$thisPage = "Cursos de italiano para extranjeros";
$description = "Nuestro cursos de italiano en grupo están dirigidos a todos aquellos que quieran aprender o mejorar su nivel de 
italiano con profesores expertos y cualificados. Después de realizar un examen de ingreso oral y escrito para determinar su nivel 
de italiano, será situado en un grupo de estudio de 3 alumnos como mínimo y 10 como máximo.";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>Cursos de italiano para extranjeros</h1>
		<img src="../images/english-student.jpg" alt="" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>The New Oxford School CURSOS DE ITALIANO PARA EXTRANJEROS</h2> 

		<p>Nuestro cursos de italiano en grupo están dirigidos a todos aquellos que quieran aprender o mejorar su nivel de italiano 
		con profesores expertos y cualificados.<br />
		Después de realizar un examen de ingreso oral y escrito para determinar su nivel de italiano, será situado en un grupo 
		de estudio de 3 alumnos como mínimo y 10 como máximo.<br />
		El horario y la duración del curso se organizará de acuerdo a las preferencias de los participantes de cada grupo de 
		lunes a viernes durante las horas de la mañana, sino también todas las tardes y las mañanas de los sábados.<br />
		Los cursos de italiano se basan en programas de carácter gramatical relacionados con los seis principales niveles de 
		competencia lingüística (inicial, elemental, intermedio 1, intermedio 2, avanzado y superior) y, además da mucho 
		espacio a la conversación.</p>
		 
		<h3>CURSOS DE ITALIANO PARA EXTRANJEROS<br />
		INFORMACIÓN GENERAL</h3> 
		
		<p><strong>Duración de la clase.</strong><br />
		Por favor, recuerde que cada clase se considera como 60 minutos de duración.</p> 
		
		<p><strong>NÚMERO DE ESTUDIANTES POR CLASE</strong><br />
		En los cursos con mini-grupos el número mínimo de alumnos es de 3 y el máximo de 6.</p>
		
		<p><strong>NIVELES DE HABILIDAD<br />
		Los estudiantes se insertan en uno de los siguientes niveles después de haber realizado el test de ingreso:</strong>
			<ul class="oxlist">		
				<li>Inicial</li>
				<li>Elemental</li>
				<li>Intermedio 1</li>
				<li>Intermedio 2</li>
				<li>Avanzado</li>
				<li>Superior</li>
			</ul>
		</p>
		<p>Nuestros programas se basan en los requisitos del Marco Común Europeo.</p>
		
		<p><strong>EL PRECIO DEL CURSO INCLUYE:</strong>
			<ul class="oxlist">		
				<li>todo el material para el curso</li>
				<li>certificado de asistencia</li> 
				<li>actividades sociales</li>
			</ul>		
		</p> 
		
		<p><strong>Servicios adicionales:</strong>
			<ul class="oxlist">
				<li>acceso a internet</li>
				<li>visión de películas italianas en lengua original</li>
			</ul>		
		</p>
		
		<p><strong>CURSOS MINI-GRUPO</strong></p>
		<div class="redp">Curso estándar</div>
		<strong>Fecha de inicio</strong>: cada lunes<br />		
		<p><strong>Duración</strong>: mínimo 2 semanas<br />
		<strong>Clases</strong>: 3/día = 15 horas/semana<br />
		<strong>Nivel</strong>: todos los niveles<br /> 
		<strong>Dimensión de la clase</strong>: de 3 a 8 alumnes<br />
		<span class="redp"><a href="../pdf/price_list.pdf" title="Download">LISTA DE PRECIOS</a>&emsp;<a href="booking_form.php" title="Booking">¡RESERVA AHORA!</a></span></p>				
		<p>El Curso Estándar consiste en un total de 15 horas de clase en grupo por semana, el número mínimo de horas que ofrecemos por cada curso de grupo. Este curso dura 
		un mínimo de dos semanas. Las clases son de 10:00 a 13:30. Es ideal para estudiantes que desean tener mucho tiempo libre.</p>
		
		<div class="redp">Curso Intensivo</div>
		<p><strong>Fecha de inicio</strong>: every Monday<br /> 
		<strong>Duración</strong>: mínimo 1 semana<br />
		<strong>Clases</strong>: 4/día = 20 horas/semana<br />
		<strong>Nivel</strong>: todos los niveles<br />
		<strong>Dimensión de la clase</strong>: de 3 a 6 alumnos</p>
		<p><span class="redp"><a href="../pdf/price_list.pdf" title="Download">LISTA DE PRECIOS</a>&emsp;<a href="booking_form.php" title="Booking">¡RESERVA AHORA!</a></span></p>				
		<p>El Curso Intensivo consiste en un total de 20 horas de clase a la semana. Hay un máximo de seis estudiantes. Las 
		clases empiezan a las 09:00 y terminan a las 13:00.</p>
		<p><strong>CLASES INDIVIDUALES</strong></p>
		<p>
		<strong>Fecha de inicio</strong>: cada día<br />
		<strong>Duración</strong>: mínimo 3 horas<br />
		<strong>Nivel</strong>: todos niveles<br />
		<strong>Dimensión de la clase</strong>: un alumno<br />
		<span class="redp"><a href="../pdf/price_list.pdf" title="Download">LISTA DE PRECIOS</a>&emsp;<a href="booking_form.php" title="Booking">¡RESERVA AHORA!</a></span></p>
		<p>Nuestros cursos individuales se adaptan a los requisitos específicos del estudiante. El contenido de las clases está programado según las necesidades del alumno 
		de forma que pueda aprender en una atmósfera relajada con la atención exclusiva del profesor.</p>
		<p><strong>CURSO COMBINADO</strong></p>
		<p>
		<strong>Fecha de inicio</strong>: cada lunes<br />
		<strong>Duración</strong>:                     mínimo one week<br />
		<strong>Clases</strong>: 3/día = 15 horas/semana<br />
		<strong>Dimensión de la clase</strong>: de 3 a 6 alumnos<br /> 
		<b>Clases individuales</b>:  4/semana<br />
		<strong>Nivel</strong>: todos niveles<br />                                                
		<span class="redp"><a href="../pdf/price_list.pdf" title="Download">LISTA DE PRECIOS</a>&emsp;<a href="booking_form.php" title="Booking">¡RESERVA AHORA!</a></span></p>
		<p>El Curso Combinado, que puede desarrollarse por un mínimo de una semana, combina clases en grupo por la mañana 
		con clases individuales por la tarde (de 13:00 a 17:00). Esta opción da a los estudiantes la oportunidad de ampliar 
		su conocimiento de Italiano durante las clases de grupo de la mañana y la oportunidad de estudiar intensivamente 
		argumentos específicos por la tarde durante su breve permanencia en Siena.</p> 
		
		<p><strong>CURSO TRIMESTRAL</strong>
		<p>
		<strong>Fecha de inicio</strong>: cada lunes<br /> 
		<strong>Duración</strong>: 12 semanas<br />
		<strong>Lesson</strong>: 3/día = 15 horas/semana<br />
		<strong>Nivel</strong>: todos los niveles<br />
		<strong>Dimensión de la clase</strong>: de 3 a 8 alumnos<br /> 
		<span class="redp"><a href="../pdf/price_list.pdf" title="Download">LISTA DE PRECIOS</a>&emsp;<a href="booking_form.php" title="Booking">¡RESERVA AHORA!</a></span></p>
		
		
		<p class="redp"><a href="../pdf/Metodi_pagamento.pdf" title="Download">
		<span><img src="../images/pdfdownload.png" width="32" height="32" alt="Pdf" style="height:32px;width:32px;vertical-align: middle;" /></span>
		 Formas de pago</a></p>
		<p class="redp"><a href="../pdf/terms.pdf" title="Download">
		<span><img src="../images/pdfdownload.png" width="32" height="32" alt="Pdf" style="height:32px;width:32px;vertical-align: middle;" /></span>
		 Términos y condiciones</a></p>
		<p class="redp"><a href="../pdf/Request_information.pdf" title="Download"> 
		<span><img src="../images/pdfdownload.png" width="32" height="32" alt="Pdf" style="height:32px;width:32px;vertical-align: middle;" /></span>
		Impreso de solicitud de informacion</a></p>		
	</div>
</div>
</div>

<?php
include("footer.php");
?>