<?php
$thisPage = "Traduzioni";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
<div class="sbox">
	<h1>Traduzioni</h1>
		<img src="images/dizionario.jpg" alt="" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>SERVIZIO TRADUZIONI</h2>
		
		<p>La New Oxford School offre servizi di traduzione capaci di soddisfare vari tipi di esigenze per quanto riguarda 
		la traduzione di testi in lingua inglese, tedesca, francese, spagnola e russa, avvalendoci di collaboratori madrelingua qualificati.</p>
		
		<p>Offriamo servizio traduzione documenti, testi tecnici e scientifici, testi di diverso tipo.</p>
		
		<p>La nostra Scuola è in grado di offrirvi un servizio professionale di qualità per traduzioni, revisione di testi, correzione bozze e asseverazioni.</p>
	</div>
</div>
</div>
<?php
include("footer.php");
?>