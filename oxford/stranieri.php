<?php
$thisPage = "Italiano per Stranieri";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
<div class="sbox">
	<h1>Italiano per Stranieri</h1>
		<img src="images/english-foreigners.jpg" alt="" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>DESCRIZIONE DEL CORSO</h2>
		<h3>Informazioni generali</h3>
		<p>Durata delle lezioni:<br />
		Ogni lezione avrà la durata di 60 minuti.<br />
		Numero di studenti nella classe:<br />
		Per i gruppi ‘mini’, ci sarà un minimo di 3 ed un massimo di 6 partecipanti.</p>
		 
		<h3>I livelli:</h3>
		 
		<p>Secondo i risultati dell’esame d’ammissione, i studenti verranno suddivisi nei seguenti livelli:
			<ul class="oxlist">
	         <li>Beginner</li>
	         <li>Elementary</li>
	         <li>Intermediate 1</li>            
	         <li>Intermediate 2</li>
	         <li>Advanced</li>
	         <li>Superior</li>
	      </ul></p>
		<p>I nostri programmi sono in linea con i programmi  stabiliti dal <strong>Common European Framework</strong>.</p>
		<p>Inclusi nel prezzo:</p>
			<ul class="oxlist">
	         <li>il materiale del corso</li>
	         <li>attestazione di frequenza</li>
	         <li>attività sociali</li>
	      </ul>
		<p>Servizi extra:</p>
			<ul class="oxlist">
				<li>accesso ad Internet</li>
		      <li>proiezione di film italiani in lingua originale</li>
			</ul>		      
		
		 
		<h3>Corso gruppo ‘mini’</h3>
		            
		<p><strong>Corso standard</strong></p>
		            
		<p>Date di inizio: ogni lunedì<br />
		Livello: tutti i livelli<br />
		Numero di studenti: da 3 a 6 partecipanti</p>
		            
		<p>Il corso standard consiste di 15 ore di lezioni di gruppo ogni settimana, il minimo numero di 
		ore offerto per un corso di gruppo. Questo corso sarà svolto per un periodo minimo di 2 settimane. 
		Le lezioni saranno dalle ore 10,00 alle 13,30. Queste lezioni sono ideali per quegli studenti che 
		vogliono godere di più tempo libero.</p>
		 
		<p><strong>Corso intensivo</strong></p>
		 
		<p>Date di inizio: ogni lunedì<br />
		Durata: una settimana (il minimo)<br />
		Lezioni: 4 ore al giorno / 20 ore alla settimana<br />
		Livello: tutti i livelli<br />
		Numero di studenti: da 3 a 6 partecipanti</p>
		<p>Il corso intensivo consiste in 20 ore di lezioni di gruppo ogni settimana.  C’è un massimo di 
		6 studenti. Le lezioni iniziano alle ore 9,00 e terminano alle ore 13,30.</p>
		 
		<h3>Corsi individuali</h3>
		 
      <p>Date di inizio: ogni giorno<br />
      Durata: da 3 ore (minimo)<br />
      Livello: tutti i livelli<br />      
      Numero di studenti: 1 partecipante</p>
		            
		<p>I nostri corsi individuali si adattano alle esigenze  dello studente. Il contenuto delle lezioni è 
		programmato sulle esigenze del partecipante per agevolare il suo apprendimento. L’atmosfera rilassata 
		e l’attenzione esclusiva dell’insegnante favoriscono un progresso rapido.</p>
		 
		<h3>Corso Combinato</h3>
		 
		<p>Date di inizio: ogni lunedì<br />
      Durata: una settimana (minimo)<br />
      Lezioni di gruppo: 3 al giorno / 15 ore alla settimana<br />
      Lezioni individuali: 4 lezioni alla settimana<br />
      Numero di studenti: da 3 a 6 partecipanti<br />
      Livello: tutti i livelli</p>
		<p>Il corso combinato unisce le lezioni di gruppo alla mattina con delle lezioni individuali il 
		pomeriggio (dalle ore 13,00 alle 17,00).  Quest’alternativa dà agli studenti l’opportunità di allargare 
		la propria conoscenza della lingua italiana durante le lezioni di gruppo la mattina e poi di approfondire 
		degli argomenti specifici il pomeriggio durante la loro permanenza a Siena.</p>

		<h3>METODI DI PAGAMENTO</h3>
		<p>I pagamenti di tutti i nostri corsi di lingue, sia per adulti che per bambini, possono avvenire in
			un'unica soluzione o dilazionati in pi&ugrave; mesi.</p>
	</div>
</div>
</div>
<?php
include("footer.php");
?>
