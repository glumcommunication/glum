<div id="menu" >
	<ul class="topmenu">
		<li>
		<a href="index.php">Home</a>
		</li>
		<li>
		<a href="about.php">Chi siamo</a>
		</li>
		<li>
		<a href="corsi.php">Corsi</a>
		</li>
		<li>
		<a href="viaggistudio.php">Viaggi studio</a>
		</li>		
		<li>
		<a href="docenti.php">Staff</a>
		</li>
		<li>
		<a href="servizi.php">Servizi</a>
		</li>
		<li>
		<a href="convenzioni.php">Convenzioni</a>
		</li>
		<li>
		<a href="gallery.php">Gallery</a>
		</li>
		<li>
		<a href="jobs.php">Lavora con noi</a>
		</li>        
		<li>
		<a href="contatti.php">Contatti</a>
		</li>
        <li>
            <a href="http://thenewoxfordschool.it/lists/?p=subscribe&id=1" title="Newsletter" style="text-decoration: underline;">
            Newsletter
            </a> 
        </li>
	</ul>
	<div class="social">

	<!--Seguici su&emsp;--><a href="http://www.facebook.com/pages/The-New-Oxford-School-Siena/172918636151645" target="_blank">
	<img src="images/facebook.png" alt="Facebook"></a>
	<a href="http://twitter.com/thenewoxford1" target="_blank">
	<img src="images/twitter.png" alt="Twitter" /></a>
    <a href="http://www.youtube.com/channel/UCdu9HD04NJIYp5otzdP2pmA" target="_blank">
	<img src="images/youtube.png" alt="YouTube" /></a>
	</div>
</div>