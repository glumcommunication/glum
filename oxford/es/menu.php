<div id="menu">
	<ul class="topmenu">
		<li>
		<a href="index.php">Home</a>
		</li>
		<li>
		<a href="about.php">¿Quienes somos?</a>
		</li>
		<li>
		<a href="where.php">Donde estamos</a>
		</li>
		<li>
		<a href="courses.php">Cursos de italiano para extranjeros</a>
		</li>
		<li>
		<a href="siena.php">Siena y la Toscana</a>
		</li>
	</ul>
	<div class="social">
	<!--Seguici su&emsp;--><a href="http://www.facebook.com/pages/The-New-Oxford-School-Siena/172918636151645" target="_blank">
	<img src="../images/facebook.png" /></a>
	<a href="http://twitter.com/thenewoxford1" target="_blank">
	<img src="../images/twitter.png" alt="" /></a>
	</div>
</div>