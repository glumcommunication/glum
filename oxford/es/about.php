<?php
$thisPage = "¿Quienes somos?";
$description = "Es un escuela de lenguas extranjeras y Empresa de formación acreditada por la Región Toscana, sede y Centro de Apoyo del Trinity College de Londres y registrada en la ECM, Educación Continua y Medicina del Ministerio de la Salud.";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>¿Quienes somos?</h1>
		<img src="../images/about.jpg" alt="" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>The New Oxford School escuela de lenguas extranjeras</h2>
		<p>Es un escuela de lenguas extranjeras y Empresa de formación acreditada por la Región Toscana, sede y Centro de Apoyo del Trinity College de Londres y registrada 
		en la ECM, Educación Continua y Medicina del Ministerio de la Salud.<br />
		Gracias a la Acreditación por parte de la Región Toscana, existe la posibilidad de usar en nuestro Instituto vales formativos obtenidos a partir de la publicación 
		de convocatorias de la Región toscana y/o de la Provincia y participar en cursos formativos reconocidos organizados por nuestra academia.<br />
		The New Oxford School nace en 2001 y actúa con éxito en el sector de la formación lingüística gracias a innovativas fórmulas de aprendizaje basadas en la comunicación.<br />
		The New Oxford School se ocupa desde siempre de la enseñanza del inglés, que se ha convertido en una exigencia fundamental para quien quiere vivir siguiendo los ritmos 
		de la realidad contemporánea: hablar inglés y otros idiomas es actualmente la clave de acceso en cualquier sector.<br />
		Hablar no significa solamente comunicar sino también conocerse a nivel personal y, por tanto, sentirse ciudadanos del mundo. En los últimos años, 
		la New Oxford School se ha especializado en la enseñanza del inglés, alemán, francés, español y desde hace poco tiempo, es también posible aprender el ruso, el chino y 
		el italiano para quien sea extranjero.</p>


		<p>Nuestros servicios:
			<ul class="oxlist">
				<li>Cursos individuales y de grupo para todos los niveles de conocimiento de cada lengua.</li>
				<li>Cursos para niños </li>
				<li>Curso de inglés para los negocios, preparación para el certificado BEC</li>
				<li>Cursos de inglés jurídico</li>
				<li>Cursos de verano y viajes-estudio al extranjero</li> 
				<li>Cursos de italiano para extranjeros</li>
				<li>Cursos para empresas</li>
				<li>Servicio de traducción</li>
				<li>Cursos de preparación para exámenes de reconocimiento internacional</li>
			</ul>
		</p>


		<h3>El PERSONAL:</h3>

		<p>El personal de la New Oxford School se encuentra disponible en los horarios de oficina para dar toda la información y para responder a cada tipo de exigencia.<br />
		Experiencia, profesionalidad, seriedad y disponibilidad son las cartar ganadoras del equipo de la Escuela. Los docentes son todos nativos, cualificados para la 
		enseñanza y procedentes de múltiples experiencias.<br />
		El clima sereno y el fuerte entusiamo de todo el personal caracterizan la T.N.O.S.</p>
	
	</div>
</div>
</div>

<?php
include("footer.php");
?>