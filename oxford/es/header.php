<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META NAME="description" CONTENT="<?php echo $description ?>">
<META NAME="KEYWORDS" CONTENT="accademia de lengua italiana, aprender italiano en italia, accademia de italiano en toscana, academias de italiano en siena, escuela de italiano para extranjeros, cursos de italiano para extranjeros en Siena, cursos de italiano para extranjeros en toscana, ">
<link rel="icon" href="../images/favicon_o.png" type="image/svg"/> 
<link rel="stylesheet" type="text/css" href="../css/style.css" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>The New Oxford School - <? echo $thisPage ?></title>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-35054570-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<!--<img src="images/backgroundflag.png" width="1169" height="590" alt="background" class="bg" />-->
<div id="container">
	    <div id="header">
                <img src="../images/logotrinity.jpg" class="logoheader trinity">
                <img src="../images/logoeda.jpg" class="logoheader eda">
                <a href="index.php"><img src="../images/logom.png" alt="Oxford School" /></a>
                <img src="../images/logoaisli.jpg" class="logoheader aisli">
                <img src="../images/logoruxum.jpg" class="logoheader ielts"> 
            </div>
