<?php
$thisPage = "Home";
$description = "Es un escuela de lenguas extranjeras y Empresa de formación acreditada por la Región Toscana, sede y Centro de Apoyo del Trinity College de Londres y registrada en la ECM, Educación Continua y Medicina del Ministerio de la Salud.";
include("header.php");
include("menu.php");
?>

	<div id="main">
<?php include("../flags_int.php"); ?>	
		<div id="mainbox">
			<img src="../images/oxfordhome.jpg" alt="Home" />
		</div>

	<div class="rightHome">

		<div class="boxhomeRight1">
		<h1>Trinity</h1>
			<a href="http://www.trinitycollege.it/" target="_blank"><img src="../images/trinity.png" alt="Trinity" /></a>
		</div>	
		<div class="boxhomeRight2">
		<h1>Horarios</h1>
		<div class="mtext">
			<h3>Oficinas</h3>
			<p>De lunes a viernes de 9,00 a 20,00<br />
			Sábado de 09,30 a 13,00</p>
                        <p style="color:#cf142b;font-weight:bold;">Durante los meses de julio y agosto, la secretar&iacute;a estar&aacute; cerrada el s&aacute;bado</p>

			<h3>Clases</span></h3>
			<p>De lunes a viernes de 9,00 a 22,00<br />
			Sábado de 9,00 a 18,00</p>
		</div>
		</div>


</div>
	<div class="leftHome">
	<div class="boxhomeLeft">
		<h1>Diagnostic Test</h1>
			<img src="../images/test.png" alt="" />
		</div>
	</div>
</div>



<?php
include("footer.php");
?>