<?php
$thisPage = "Siena y la Toscana";
$description = "Siena es un municipio de la Toscana central. La ciudad es mundialmente conocida por su patrimonio artístico y por la 
esencial unidad estilística de su mobiliario urbano medieval. Ha sido declarada por la UNESCO patrimonio de la Humanidad.";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>Siena y la Toscana</h1>
		<img src="../images/sienapiazza.jpg" alt="Siena" />
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>Toscana</h2>
		<p>Hacia el s. VIII a.C. aparecen los primeros testimonios de la presencia en todo el territorio de la Italia Central de un 
		pueblo misterioso y extraordinario: los Etruscos. De ellos la región tomó el nombre de Etruria, Tuscia por los romanos y, 
		posteriormente, Tuscania y Toscana. Después de los etruscos y del Imperio Romano, a través de las dominaciones ostrogodas, 
		bizantinas y longobardas, primero con Dante Alighieri y con Giotto en el s. XIV y luego, en el s. XV, con otros grandes artistas, 
		la Toscana y, concretamente, Florencia, contribuyó de forma determinante en el Renacimiento Italiano. En el periodo renacentista 
		predomina en Florencia la familia de los Medici y, después de su desaparición, el entonces Gran Ducado de la Toscana pasó bajo 
		el dominio de una familia austríaca, los Lorena, príncipes liberales que entre otras cosas hicieron sanear gran parte de la Maremma 
		y contribuyeron a hacer de la Toscana una de las regiones más ricas de Italia. En la segunda mitad de 1800, el Gran Ducado pasó al 
		Reino de Italia.</p> 
		<p>La Toscana es una de las mayores regiones italianas por patrimonio histórico artístico, cultural y paisajístico. Se asoma al mar 
		Tirreno con sus aproximadamente 400 km de costa y comprende también las islas del Archipiélago Toscano, entre las cuales se encuentra 
		la Isla de Elba. En el territorio toscano predominan las colinas sobre todo en la parte centro-meridional. La Toscana es universalmente 
		conocida por su grandísima riqueza de monumentos y obras de arte: célebres en todo el mundo son las ciudades de Florencia, Lucca, Pisa 
		y Siena: no son de menor importancia los numerosos centros menores, algunos de los cuales son verdaderas ciudades históricas perfectamente 
		conservadas, guardianas de obras de arte de inestimable valor, entre las cuales -en la provincia de Siena- podemos citar San Gimignano 
		(centro histórico medieval y monumentos renacentistas), Pienza (centro histórico y ciudad ideal del Renacimiento con preexistentes 
		testimonios medievales), Montepulciano (monumentos medievales, renacentistas y barrocos), Montalcino (centro histórico medieval y 
		territorio famoso por sus preciadísimos vinos), Chiusi (testimonios etrusco-romanos, monumentos paleocristianos y medievales) y Monteriggioni 
		(burgo medieval fortificado).</p> 
		<h2>Siena</h2>
		<p>Siena es un municipio de la Toscana central. La ciudad es mundialmente conocida por su patrimonio artístico y por la esencial unidad 
		estilística de su mobiliario urbano medieval. Ha sido declarada por la UNESCO patrimonio de la Humanidad.</p> 
		<p>Siena fue fundada como colonia romana en la época del Emperador Augusto y tomó el nombre de Saena Julia. Las escasas noticias de buena 
		fuente anteriores a la fundación, sugieren la existencia de una comunidad etrusca sobre la cual se estableció la colonia militar romana en 
		la época de Augusto. Siena se vuelve a encontrar en el s. X en el centro de importantes vías comerciales que conducían a Roma y, gracias a 
		ello, se convirtió en una importante ciudad medieval. En el siglo XII la ciudad se dota de ordenanzas municipales de tipo consular, empieza 
		a extender su territorio y estrecha la primeras alianzas. Esta situación de relevancia tanto política como económica, llevan a Siena a luchar 
		por los dominios septentrionales de la Toscana, contra Florencia. A partir de la primera mitad del s. XII Siena prospera y se convierte en un 
		importante centro comercial, manteniendo buenas relaciones con el Estado de la Iglesia; los banqueros sieneses eran un punto de referencia para 
		las autoridades de Roma, a los cuales se dirigían para préstamos o financiaciones. A finales del s. XII Siena sosteniendo la causa gibelina, se 
		encontró de nuevo contra Florencia, la cual en un primer momento se llevó las de perder pero luego los sieneses perdieron la guerra en la batalla 
		de Colle Val d’Elsa, que llevó en el 1287 la ascensión al poder del Gobierno de los Nueve. Bajo este nuevo gobierno, Siena logró su máximo esplendor, 
		tanto económico como cultural. Después de la peste de 1348, comenzó la lenta decadencia de la República de Siena, que alcanzó el epílogo en el 
		1555, año en el que la ciudad tuvo que rendirse a la supremacía florentina.</p> 
		<p>Siena tiene una larga tradición culinaria, quizás debida también a la riqueza del periodo medieval y a la presencia de numerosas tascas y 
		puntos de hospitalidad a lo largo de la vía Francigena. Las actividades principales son el turismo, los servicios, la agricultura y la artesanía. 
		En el sector de los servicios, las actividades más importantes son las ligadas al banco Monte dei Paschi de Siena. Tiene además importante presencia 
		la Universidad y la Empresa hospitalaria, que emplean miles de personas y sirven un distrito de usuarios mucho más extenso de su ya amplio 
		territorio provincial. Su presencia es importante también desde el punto de vista de la investigación científica y médica. El turismo es, sin duda 
		alguna, una actividad próspera, dada la fama de Siena y el número de turistas que atrae. El 2 de julio y el 16 de agosto en Siena, en la Plaza del 
		Campo, se desarrolla el tradicional Palio, una carrera de caballos montados a pelo (es decir, sin silla) entre los distintos barrios de Siena que 
		monopoliza la atención de la ciudad durante varios días; esto se debe a que el Palio no es exclusivamente un espectáculo histórico o la reexaminación de 
		un antiguo torneo medieval sino que es la expresión de la antiquísima y arraigada tradición sienesa. El Palio no tiene la menor intención de ser un 
		espectáculo que se pueda pasar en “pocos días” sino que es el fruto de una cuidadosa y maniática organización por parte de los barrios de la ciudad, 
		que conllevan una intensa vida social y asociativa durante todo el año. El Palio atrae también muchos turistas y se sigue en directo desde muchas televisiones.</p> 
		<p>El territorio sienés es magnífico en todas las estaciones. Pasado y presente, naturaleza por vivir, manantial de salud y belleza, tradición… ¡una 
		tierra entre arte y naturaleza! Tierra donde la historia se mezcla con la leyenda. Las ciudades y los burgos enmarcados en los collados emergen intactos 
		de la lejana Edad Media. Grande es el arte en las catedrales, en las iglesias y en los palacios, mágica armonía en los callejones de la ciudad y 
		vibraciones de antiguas melodías en las carreteras de campiña, entre fértiles campos ricos de olivos y viñedos. Vela en las colinas el ciprés, extremo 
		defensor del sobrio paisaje sienés. El territorio sienés es rico de paisajes naturales incontaminados, burgos medievales perfectamente conservados, 
		lugares arqueológicos, ciudades de arte como Siena, San Gimignano, Montalcino, Pienza, Montepulciano, etc.</p> 
	</div>
</div>
</div>

<?php
include("footer.php");
?>