<?php
$thisPage = "Donde estamos";
$description = "Es un escuela de lenguas extranjeras y Empresa de formación acreditada por la Región Toscana, sede y Centro de Apoyo del Trinity College de Londres y registrada en la ECM, Educación Continua y Medicina del Ministerio de la Salud.";
include("header.php");
include("menu.php");
?>
<div id="main">
	<div class="leftSide">		
		<div class="sbox">
			<h1>Donde estamos</h1>
				<img src="../images/contatti.jpg" alt="" />			
		</div>
	</div>
	<div class="rightSide">
		<div id="lbox">
			<!--<h2>School of Italian for foreigners</h2>-->
				<p>THE NEW OXFORD SCHOOL Di Maione Orsola<br />
				Via Cesare Battisti 4 - 53100 SIENA</p>
				<p><strong>Tel.</strong>: 0577 281907 <strong>Fax</strong>: 0577 217074<br />
				<strong>Mail</strong>: <a href="mailto:info@thenewoxfordschool.it">info@thenewoxfordschool.it</a><br />
				<strong>P.Iva</strong> 01189500521</p>
				<div class="gmaps">
				<table><tr><td><p>
				<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
				src="https://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=The+New+Oxford+School,+Siena,+SI&amp;aq=0&amp;oq=the+new+oxford+school&amp;sll=43.322309,11.320875&amp;sspn=0.002396,0.004823&amp;t=h&amp;ie=UTF8&amp;hq=The+New+Oxford+School,&amp;hnear=Siena,+Toscana&amp;cid=12961991591500064237&amp;ll=43.326458,11.321454&amp;spn=0.010927,0.018239&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe><br />
				</p></td></tr></table></div>
				<small><a href="https://maps.google.it/maps?f=q&amp;source=embed&amp;hl=it&amp;geocode=&amp;q=The+New+Oxford+School,+Siena,+SI&amp;aq=0&amp;oq=the+new+oxford+school&amp;sll=43.322309,11.320875&amp;sspn=0.002396,0.004823&amp;t=h&amp;ie=UTF8&amp;hq=The+New+Oxford+School,&amp;hnear=Siena,+Toscana&amp;cid=12961991591500064237&amp;ll=43.326458,11.321454&amp;spn=0.010927,0.018239&amp;z=15&amp;iwloc=A" 
				style="color:#0000FF;text-align:left">Ver mapa más grande</a></small>
			
			<h3>Oficinas</h3>	
			<p>De lunes a viernes de 9,00 a 20,00<br />
			Sábado de 09,30 a 13,00</p>
			<h3>Clases</span></h3>
			<p>De lunes a viernes de 9,00 a 22,00<br />
			Sábado de 9,00 a 18,00</p>
		</div>
	</div>
</div>

<?php
include("footer.php");
?>