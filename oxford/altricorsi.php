<?php
$thisPage = "Altri Corsi";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">	
<div class="sbox">
	<h1>Altri Corsi</h1>
		<img src="images/flags.jpg" alt="" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>DESCRIZIONE DEI CORSI</h2>
		<p>Con noi è possibile imparare il tedesco, il francese, lo spagnolo, il portoghese, il russo, il cinese, il giapponese e l'arabo.</p> 
		<p>Tutti gli insegnanti, madrelingua e qualificati all’insegnamento, tengono corsi individuali e di 
		gruppo a seconda delle richieste.</p> 
		<p>La metodologia d’insegnamento basata sulla conversazione, l’organizzazione dei corsi e la frequenza sono i 
		medesimi dei corsi di gruppo e individuali per la lingua inglese.</p>
		<p>Test d’ accesso e organizzazione del corso avvengono in base alle esigenze dell’allievo: personali, 
		di lavoro, di studio.</p>
		<p>Esame finale e attestato di frequenza con numero di ore di corso frequentate a livello conseguito.</p>
		<p>Anche preparazione a certificazioni internazionali per il francese, il tedesco, lo spagnolo e il portoghese.</p>

		<h3>METODI DI PAGAMENTO</h3>
		<p>I pagamenti di tutti i nostri corsi di lingue, sia per adulti che per bambini, possono avvenire in
			un'unica soluzione o dilazionati in pi&ugrave; mesi.</p>
	</div>
</div>
</div>
<?php
include("footer.php");
?>
