<?php
$thisPage = "Corsi di Inglese";
$description = "Scuola di inglese, francese, spagnolo, tedesco, russo, arabo, cinese, italiano per stranieri, a Siena in Toscana, Italia. Scuola di lingue straniere a Siena.";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>404 - File not found!</h1>
		<img src="images/error404.png" alt="404" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>OPS! Questa pagina non esiste più...</h2>
		
			<p>La pagina che stai cercando non esiste, <strong>The New Oxford School</strong> ha un nuovo sito e forse stai
			facendo riferimento ad una vecchia pagina.<br />
			Ci scusiamo per il disagio.<br />
			Scopri il nuovo sito partendo dalla <a href="index.php" title="Home"><strong><span class="redp">HOME</span></strong></a></p>

	
	</div>
</div>
</div>

<?php
include("footer.php");
?>