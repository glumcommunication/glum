<?php
$thisPage = "Corsi";
$description = "Scuola di inglese, francese, spagnolo, tedesco, russo, arabo, cinese, italiano per stranieri, a Siena in Toscana, Italia. Scuola di lingue straniere a Siena.";
include("header.php");
include("menu.php");
?>

<div id="main">

		<a href="inglese.php">
			<div class="mbox">
				<h1>Corsi di Inglese</h1>
				<img src="images/english-student.jpg" alt="Corsi di Inglese" />
			</div>
		</a>
		<a href="adulti.php">
			<div class="mbox">
				<h1>Corsi per Adulti</h1>
				<img src="images/english-adult.jpg" alt="Corsi per Adulti" />
			</div>
		</a>
		<a href="ragazzi.php">
			<div class="mbox">
				<h1>Corsi per Bambini</h1>
				<img src="images/english-children.jpg" alt="Corsi per Bambini" />
			</div>
		</a>

		<a href="aziende.php">
			<div class="mbox">
				<h1>Corsi per Aziende</h1>
				<img src="images/english-company.jpg" alt="Corsi per Aziende" />
			</div>
		</a>
		<a href="stranieri.php">
			<div class="mbox">
				<h1>Italiano per Stranieri</h1>
				<img src="images/english-foreigners.jpg" alt="Italiano per Stranieri" />
			</div>
		</a>
		<a href="altricorsi.php">
			<div class="mbox">
				<h1>Altri Corsi</h1>
				<img src="images/flags.jpg" alt="Altri Corsi" />
			</div>
		</a>

</div>



<?php
include("footer.php");
?>
