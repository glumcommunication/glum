<?php
$thisPage = "Home";
include("header.php");
include("menu.php");
$notrobot1 = rand(0,9);
$notrobot2 = rand(0,9);

?>
<!--<script type="text/javascript">
 var RecaptchaOptions = {
    theme : 'clean'
 };
 </script>-->
	<div id="main">
		<div class="leftSide">		
			<div class="sbox">
				<h1>Diagnostic Test</h1>
				<img src="images/test.png" alt="test" />			
			</div>
		</div>
		<div class="rightSide">
			<div id="lbox">
				<h2>Take your test</h2> 
			   <p><strong>Nota bene</strong>: Nella risoluzione del test va <strong>SEMPRE</strong> riportata la lettera corrispondente alla risposta (A,B,C o D), 
			   <strong>NON</strong> il relativo termine inglese.</p>
        
        		<p>
				<form method="POST" action="invia_test.php">
				<table width="80%" align="center" cellpadding="5" cellspacing="0" id="testTable">
				<tr>
				<td colspan="2">
				<strong>Question 1 - 5</strong><br>
				Where you can see this notice?<br>
				For questions 1 to 5, mark one letter A, B or C
				</td>
				</tr>

				<tr>
				<td>Please leave your room key at Reception.</td>
				<td>
				A &nbsp; <input type="checkbox" name="1" value="A"> &nbsp; in a shop<br>
				B &nbsp; <input type="checkbox" name="1" value="B"> &nbsp; in a hotel<br>
				C &nbsp; <input type="checkbox" name="1" value="C"> &nbsp; in a taxi<br>
				</td>
				</tr>
				
				<tr>
				<td>Foreign money changed here.</td>
				<td>
				A &nbsp; <input type="checkbox" name="2" value="A"> &nbsp; in a library<br>
				B &nbsp; <input type="checkbox" name="2" value="B"> &nbsp; in a bank<br>
				C &nbsp; <input type="checkbox" name="2" value="C"> &nbsp; in a police station<br>
				</td>
				</tr>
				
				<tr>
				<td>AFTERNOON SHOW BEGINS AT 2PM</td>
				<td>
				A &nbsp; <input type="checkbox" name="3" value="A"> &nbsp; outside a theatre<br>
				B &nbsp; <input type="checkbox" name="3" value="B"> &nbsp; outside a supermarket<br>
				C &nbsp; <input type="checkbox" name="3" value="C"> &nbsp; outside a restaurant<br>
				</td>
				</tr>
				
				<tr>
				<td>CLOSED FOR HOLIDAYS
				<br>
				Lessons start again on the 8 th January
				</td>
				<td>
				A &nbsp; <input type="checkbox" name="4" value="A"> &nbsp; at a travel agent's<br>
				B &nbsp; <input type="checkbox" name="4" value="B"> &nbsp; at a music school<br>
				C &nbsp; <input type="checkbox" name="4" value="C"> &nbsp; at a restaurant<br>
				</td>
				</tr>
				
				<tr>
				<td>
				Price per night:
				<br>
				&pound; 10 a tent<br>
				&pound; 5 a person
				</td>
				<td>
				A &nbsp; <input type="checkbox" name="5" value="A"> &nbsp; at a cinema<br>
				B &nbsp; <input type="checkbox" name="5" value="B"> &nbsp; in a hotel<br>
				C &nbsp; <input type="checkbox" name="5" value="C"> &nbsp; on a camp-site<br>
				</td>
				</tr>
				<tr>
				<td colspan="2">
				<strong>Question 6 - 10</strong><br>
				In this section you must choose the word which best fits each space in the text below<br>
				For questions 6 to 10, mark one letter A, B or C
				</td>
				</tr>
				<tr>
				<td colspan="2">
				<div align="center"><strong>Scotland</strong></div><br>
				Scotland is the north part of the island of Great Britain. The Atlantic Ocean is on the west and the 
				North Sea on the east. Some people <strong>(6)</strong> <input type="text" size="1" maxlength="1" name="6" > Scotland speak a different language called Gaelic. 
				There are <strong>(7)</strong> <input type="text" size="1" maxlength="1" name="7" > five million people in Scotland, and Edinburgh is <strong>(8)</strong> <input type="text" size="1" maxlength="1" name="8" > most 
				famous city.
				Scotland has many mountains; the highest one is called ‘Ben Nevis’. In the south of Scotland, there are 
				a lot of sheep. A long time ago, there <strong>(9)</strong> <input type="text" size="1" maxlength="1" name="9" > many forests, but now there are only a 
				<strong>(10)</strong> <input type="text" size="1" maxlength="1" name="10" >
				Scotland is only a small country, but it is quite beautiful.
				
				</td>
				</tr>
				<tr>
				<td colspan="2">
				<table width="100%" cellpadding="5" cellspacing="0" border="0">
				<tr>
				<td>6. <strong>A</strong> on</td><td><strong>B</strong> in</td><td><strong>C</strong> at</td>
				</tr>
				<tr>
				<td>7. <strong>A</strong> about</td><td><strong>B</strong> between</td><td><strong>C</strong> among</td>
				</tr>
				<tr>
				<td>8. <strong>A</strong> his</td><td><strong>B</strong> your</td><td><strong>C</strong> its</td>
				</tr>
				<tr>
				<td>9. <strong>A</strong> is</td><td><strong>B</strong> were</td><td><strong>C</strong> was</td>
				</tr>
				<tr>
				<td>10. <strong>A</strong> few</td><td><strong>B</strong> little</td><td><strong>C</strong> lot</td>
				</tr>
				</table>
				</td>
				</tr>
				
				<tr>
				<td colspan="2">
				<strong>Question 11 - 20</strong><br>
				In this section you must choose the word which best fits each space in the texts<br>
				For questions 11 to 20, mark one letter A, B, C or D
				</td>
				</tr>
				<tr>
				<td colspan="2">
				<div align="center"><strong>Alice Guy Blaché</strong></div><br>
				Alice Guy Blaché was the first female film director.  She first became involved in cinema whilst 
				working for the Gaumont Film Company in the late 1890s.  This was a period  of great change in 
				the cinema and Alice was the first to use many new inventions, <strong>(11)</strong> <input type="text" size="1" maxlength="1" name="11" > sound and colour.
				<br><br>
				In 1907 Alice <strong>(12)</strong> <input type="text" size="1" maxlength="1" name="12" > to New York where she started her own film company. She was  
				<strong>(13)</strong> <input type="text" size="1" maxlength="1" name="13" > successful, but, when Hollywood became the centre of the film world, the best 
				days of the independent New York film companies were <strong>(14)</strong> <input type="text" size="1" maxlength="1" name="14" > When Alice died in 
				1968, hardly anybody <strong>(15)</strong> <input type="text" size="1" maxlength="1" name="15" >her name.
				</td>
				</tr>
				<tr>
				<td colspan="2">
				<table width="100%" cellpadding="5" cellspacing="0" border="0">
				<tr>
				<td>11. <strong>A</strong> bringing</td><td><strong>B</strong> including</td><td><strong>C</strong> containing</td><td><strong>D</strong> supporting</td>
				</tr>
				<tr>
				<td>12. <strong>A</strong> moved</td><td><strong>B</strong> ran</td><td><strong>C</strong> entered</td><td><strong>D</strong> transported</td>
				</tr>
				<tr>
				<td>13. <strong>A</strong> next</td><td><strong>B</strong> once</td><td><strong>C</strong> immediately</td><td><strong>D</strong> recently</td>
				</tr>
				<tr>
				<td>14. <strong>A</strong> after</td><td><strong>B</strong> down</td><td><strong>C</strong> behind</td><td><strong>D</strong> over</td>
				</tr>
				<tr>
				<td>15. <strong>A</strong> remembered</td><td><strong>B</strong> realised</td><td><strong>C</strong> reminded</td><td><strong>D</strong> repeated</td>
				</tr>
				</table>
				</td>
				</tr>
				
				<tr>
				<td colspan="2">
				<div align="center"><strong>UFOs – do they exist?</strong></div><br>
				UFO is short for ‘unidentified flying object’. UFOs are popularly known as flying saucers, 
				<strong>(16)</strong> <input type="text" size="1" maxlength="1" name="16" > that is often the <strong>(17)</strong> <input type="text" size="1" maxlength="1" name="17" > they are reported to be.  The <strong>(18)</strong> <input type="text" size="1" maxlength="1" name="18" > 
				"flying saucers" were seen in 1947 by an American pilot, but experts who studied his claim 
				decided it had been a trick of the light.
				Even people experienced at watching the sky, <strong>(19)</strong> <input type="text" size="1" maxlength="1" name="19" > as pilots, report seeing UFOs. In 
				1978 a pilot reported a collection of UFOs off the coast of New Zealand.  A television 
				<strong>(20)</strong> <input type="text" size="1" maxlength="1" name="20" > went up with the pilot and filmed the UFOs. Scientists studying this 
				phenomenon later discovered that in this case they were simply lights on boats out fishing.
				
				</td>
				</tr>
				<tr>
				<td colspan="2">
				<table width="100%" cellpadding="5" cellspacing="0" border="0">
				<tr>
				<td>16. <strong>A</strong> because</td><td><strong>B</strong> therefore</td><td><strong>C</strong> although</td><td><strong>D</strong> so</td>
				</tr>
				<tr>
				<td>17. <strong>A</strong> look</td><td><strong>B</strong> shape</td><td><strong>C</strong> size</td><td><strong>D</strong> type</td>
				</tr>
				<tr>
				<td>18. <strong>A</strong> last</td><td><strong>B</strong> next</td><td><strong>C</strong> first</td><td><strong>D</strong> oldest</td>
				</tr>
				<tr>
				<td>19. <strong>A</strong> liker</td><td><strong>B</strong> that</td><td><strong>C</strong> so</td><td><strong>D</strong> such</td>
				</tr>
				<tr>
				<td>20. <strong>A</strong> cameraman</td><td><strong>B</strong> director</td><td><strong>C</strong> actor</td><td><strong>D</strong> announcer</td>
				</tr>
				</table>
				</td>
				</tr>
				
				<tr>
				<td colspan="2">
				<strong>Question 21 - 40</strong><br>
				In this section you must choose the word or phrase which best completes each sentence.<br>
				For questions 21 to 40, mark one letter A, B, C or D
				</td>
				</tr>
				<tr>
				<td colspan="2">
				<table width="100%" cellpadding="5" cellspacing="0">
				<tr>
				<td><strong>21.</strong></td><td colspan="4">The teacher encouraged her students <input type="text" size="1" maxlength="1" name="21" > to an English pen-friend.
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>should write</td><td><strong>B </strong>write</td><td><strong>C </strong>wrote</td><td><strong>D </strong>to write</td>
				</tr>
				
				<tr>
				<td><strong>22.</strong></td><td colspan="4">They spent a lot of time <input type="text" size="1" maxlength="1" name="22" > at the pictures in the museum.
				
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>looking</td><td><strong>B </strong>for looking</td><td><strong>C </strong>to look</td><td><strong>D </strong>to looking</td>
				</tr>
				
				<tr>
				<td><strong>23.</strong></td><td colspan="4">Shirley enjoys science lessons, but all her experiments seem to <input type="text" size="1" maxlength="1" name="23" > wrong.
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>turn</td><td><strong>B </strong>come</td><td><strong>C </strong>end</td><td><strong>D </strong>go</td>
				</tr>
				
				<tr>
				<td><strong>24.</strong></td><td colspan="4"><input type="text" size="1" maxlength="1" name="24" > from Michael, all the group arrived on time.
				
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>Except</td><td><strong>B </strong>Other</td><td><strong>C </strong>Besides
				</td><td><strong>D </strong>Apart</td>
				</tr>
				
				<tr>
				<td><strong>25.</strong></td><td colspan="4">She <input type="text" size="1" maxlength="1" name="25" >her neighbour's children for the broken window.
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>accused</td><td><strong>B </strong>complained</td><td><strong>C </strong>blamed</td><td><strong>D </strong>denied</td>
				</tr>
				
				<tr>
				<td><strong>26.</strong></td><td colspan="4">As I had missed the history lesson, my friend went <input type="text" size="1" maxlength="1" name="26" > the homework with me.
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>by</td><td><strong>B </strong>after</td><td><strong>C </strong>over</td><td><strong>D </strong>on</td>
				</tr>
				
				<tr>
				<td><strong>27.</strong></td><td colspan="4">Whether shes a good actress or not is a <input type="text" size="1" maxlength="1" name="27" > of opinion.
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>matter</td><td><strong>B </strong>subject</td><td><strong>C </strong>point</td><td><strong>D </strong>case</td>
				</tr>
				
				<tr>
				<td><strong>28.</strong></td><td colspan="4">The decorated roof of the ancient palace was <input type="text" size="1" maxlength="1" name="28" > up by four thin columns.
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>built</td><td><strong>B </strong>carried</td><td><strong>C </strong>held</td><td><strong>D </strong>supported</td>
				</tr>
				<tr>
				<td><strong>29.</strong></td><td colspan="4">Would it <input type="text" size="1" maxlength="1" name="29" > you if we came on Thursday
				
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>agree</td><td><strong>B </strong>suit</td><td><strong>C </strong>like</td><td><strong>D </strong>fit</td>
				</tr>
				<tr>
				<td><strong>30.</strong></td><td colspan="4">This form <input type="text" size="1" maxlength="1" name="30" > be handed in until the end of the week.
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>doesn't need</td><td><strong>B </strong>doesn't have</td><td><strong>C </strong>needn't</td><td><strong>D </strong>hasn't got</td>
				</tr>
				
				<tr>
				<td><strong>31.</strong></td><td colspan="4">If you make a mistake when you are writing, just <input type="text" size="1" maxlength="1" name="31" >. it out with your pen.
				
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>cross</td><td><strong>B </strong>clear</td><td><strong>C </strong>do</td><td><strong>D </strong>wipe</td>
				</tr>
				
				<tr>
				<td><strong>32.</strong></td><td colspan="4">Although our opinions on many things <input type="text" size="1" maxlength="1" name="32" > , we're good friends.
				
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>differ</td><td><strong>B </strong>oppose</td><td><strong>C </strong>disagree</td><td><strong>D </strong>divide</td>
				</tr>
				
				<tr>
				<td><strong>33.</strong></td><td colspan="4">This product must be eaten <input type="text" size="1" maxlength="1" name="33" > two days of purchase.
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>by</td><td><strong>B </strong>before</td><td><strong>C </strong>within</td><td><strong>D </strong>under</td>
				</tr>
				
				<tr>
				<td><strong>34.</strong></td><td colspan="4">The newspaper report contained <input type="text" size="1" maxlength="1" name="34" > important information.
				
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>many</td><td><strong>B </strong>another</td><td><strong>C </strong>an</td><td><strong>D </strong>a lot of</td>
				</tr>
				
				<tr>
				<td><strong>35.</strong></td><td colspan="4">Have you considered <input type="text" size="1" maxlength="1" name="35" > to London?
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>move</td><td><strong>B </strong>to move</td><td><strong>C </strong>to be moving</td><td><strong>D </strong>moving</td>
				</tr>
				
				<tr>
				<td><strong>36.</strong></td><td colspan="4">It can be a good idea for people who lead an active life to increase their <input type="text" size="1" maxlength="1" name="36" > of vitamins
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>upturn</td><td><strong>B </strong>input</td><td><strong>C </strong>upkeep moving</td><td><strong>D </strong>intake</td>
				</tr>
				
				<tr>
				<td><strong>37.</strong></td><td colspan="4">I thought there was a <input type="text" size="1" maxlength="1" name="37" > of jealousy in his reaction to my good fortune.
				
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>piece</td><td><strong>B </strong>part</td><td><strong>C </strong>shadow moving</td><td><strong>D </strong>touch</td>
				</tr>
				
				<tr>
				<td><strong>38.</strong></td><td colspan="4">Why didn't you <input type="text" size="1" maxlength="1" name="38" > that you were feeling ill?
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>advice</td><td><strong>B </strong>mention</td><td><strong>C </strong>remark</td><td><strong>D </strong>tell</td>
				</tr>
				
				<tr>
				<td><strong>39.</strong></td><td colspan="4">James was not sure exactly where his best interests <input type="text" size="1" maxlength="1" name="39" >
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>stood</td><td><strong>B </strong>rested</td><td><strong>C </strong>lay</td><td><strong>D </strong>centred</td>
				</tr>
				
				<tr>
				<td><strong>40.</strong></td><td colspan="4">He's still getting <input type="text" size="1" maxlength="1" name="40" > the shock of losing his job.
				
				</td>
				</tr>
				<tr>
				<td>&nbsp;</td><td><strong>A </strong>across</td><td><strong>B </strong>by</td><td><strong>C </strong>over</td><td><strong>D </strong>through</td>
				</tr>
				</table>
				</td>
				</tr>
				<tr style="background-color: #cad8f7;">
				<td style="width:40%;">Name *</td>
				<td>
				<input type="text" name="nome" required>
				</td>
				</tr>
				<tr style="background-color: #cad8f7;">
				<td style="width:40%;">Surname *</td>
				<td>
				<input type="text" name="cognome" required>
				</td>
				</tr>
				<tr style="background-color: #cad8f7;">
				<td style="width:40%;">E-mail *</td>
				<td>
				<input type="text" name="email" required>
				</td>
				</tr>
                <tr style="background-color: #cad8f7;">
                    <td style="width:40%;">Quanto fa <strong><?php echo $notrobot1 . "</strong> pi&ugrave; <strong>" . $notrobot2 ?></strong> *</td>
                    <td>
                     <input type="text" name="notrobot_check" required>   
                    </td>
                </tr>
                <tr>
                    <td colspan="2">* I campi contrassegnati dall'asterisco sono obbligatori.</td>
                </tr>
                <tr>
                    <td colspan="2">
<?php
//require_once('recaptchalib.php');
//$publickey = "6LdeM-oSAAAAAExl4C-ElrimQgFRKHUZ1UdUZjyB"; // you got this from the signup page
//echo recaptcha_get_html($publickey);
?>         
                    </td>
                </tr>    
				<tr>
				<td colspan="2" align="center">
                <input type="hidden" <?php echo "value=\"$notrobot1\""; ?> name="notrobot1">
                <input type="hidden" <?php echo "value=\"$notrobot2\""; ?> name="notrobot2">
				<input type="submit" name="INVIA" value="INVIA">
				</td>
				</tr>
				</table>
				</form></p>
			</div>
		</div>
	</div>

<?php
include("footer.php");
?>