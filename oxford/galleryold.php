<?php
$thisPage = "Gallery";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>Gallery</h1>
		<img src="images/fotogallery.jpg" alt="" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
		<h2>LE PERSONE</h2>
		<div class="gallery">
				<a href="images/gallery/original/DSC_2925.jpg" rel="lightbox[persone]"><img src="images/gallery/thumb/DSC_2925.jpg" alt="Persone" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_3082.jpg" rel="lightbox[persone]"><img src="images/gallery/thumb/DSC_3082.jpg" alt="Persone" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_3104.jpg" rel="lightbox[persone]"><img src="images/gallery/thumb/DSC_3104.jpg" alt="Persone" ></a>			
			</div>
			<div class="gallerylast">
				<a href="images/gallery/original/DSC_3216.jpg" rel="lightbox[persone]"><img src="images/gallery/thumb/DSC_3216.jpg" alt="Persone" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_2932.jpg" rel="lightbox[persone]"><img src="images/gallery/thumb/DSC_2932.jpg" alt="Persone" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_2934.jpg" rel="lightbox[persone]"><img src="images/gallery/thumb/DSC_2934.jpg" alt="Persone" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_2963.jpg" rel="lightbox[persone]"><img src="images/gallery/thumb/DSC_2963.jpg" alt="Persone" ></a>			
			</div>
			<div class="gallerylast">
				<a href="images/gallery/original/DSC_3209.jpg" rel="lightbox[persone]"><img src="images/gallery/thumb/DSC_3209.jpg" alt="Persone" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_3009.jpg" rel="lightbox[persone]"><img src="images/gallery/thumb/DSC_3009.jpg" alt="Persone" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_3048.jpg" rel="lightbox[persone]"><img src="images/gallery/thumb/DSC_3048.jpg" alt="Persone" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_3069.jpg" rel="lightbox[persone]"><img src="images/gallery/thumb/DSC_3069.jpg" alt="Persone" ></a>			
			</div>
			<div class="gallerylast">
				<a href="images/gallery/original/DSC_3072.jpg" rel="lightbox[persone]"><img src="images/gallery/thumb/DSC_3072.jpg" alt="Persone" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_3075.jpg" rel="lightbox[persone]"><img src="images/gallery/thumb/DSC_3075.jpg" alt="Persone" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_3188.jpg" rel="lightbox[persone]"><img src="images/gallery/thumb/DSC_3188.jpg" alt="Persone" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_3192.jpg" rel="lightbox[persone]"><img src="images/gallery/thumb/DSC_3192.jpg" alt="Persone" ></a>			
			</div>
<br clear="all">
	
		<h2>LA SCUOLA</h2>
			<div class="gallery">
				<a href="images/gallery/original/DSC_0610.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_0610.jpg" alt="Scuola" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_0639.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_0639.jpg" alt="Scuola" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_0645.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_0648.jpg" alt="Scuola" ></a>			
			</div>
			<div class="gallerylast">
				<a href="images/gallery/original/DSC_0648.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_0648.jpg" alt="Scuola" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_0651.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_0651.jpg" alt="Scuola" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_0704.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_0704.jpg" alt="Scuola" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_0732.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_0732.jpg" alt="Scuola" ></a>			
			</div>
			<div class="gallerylast">
				<a href="images/gallery/original/DSC_0742.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_0742.jpg" alt="Scuola" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_0743.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_0743.jpg" alt="Scuola" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_0752.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_0752.jpg" alt="Scuola" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_2952.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_2952.jpg" alt="Scuola" ></a>			
			</div>
			<div class="gallerylast">
				<a href="images/gallery/original/DSC_3015.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_3015.jpg" alt="Scuola" ></a>			
			</div>
			<h2>I CORSI</h2>
			<div class="gallery">
				<a href="images/gallery/original/DSC_2986.jpg" rel="lightbox[corsi]"><img src="images/gallery/thumb/DSC_2986.jpg" alt="Corsi" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_2991.jpg" rel="lightbox[corsi]"><img src="images/gallery/thumb/DSC_2991.jpg" alt="Corsi" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_2992.jpg" rel="lightbox[corsi]"><img src="images/gallery/thumb/DSC_2992.jpg" alt="Corsi" ></a>			
			</div>
			<div class="gallerylast">
				<a href="images/gallery/original/DSC_2993.jpg" rel="lightbox[corsi]"><img src="images/gallery/thumb/DSC_2993.jpg" alt="Corsi" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_2994.jpg" rel="lightbox[corsi]"><img src="images/gallery/thumb/DSC_2994.jpg" alt="Corsi" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_3222.jpg" rel="lightbox[corsi]"><img src="images/gallery/thumb/DSC_3222.jpg" alt="Corsi" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_3224.jpg" rel="lightbox[corsi]"><img src="images/gallery/thumb/DSC_3224.jpg" alt="Corsi" ></a>			
			</div>
			<div class="gallerylast">
				<a href="images/gallery/original/DSC_3225.jpg" rel="lightbox[corsi]"><img src="images/gallery/thumb/DSC_3225.jpg" alt="Corsi" ></a>			
			</div>			
<!--			<div class="gallery">
				<a href="images/gallery/original/DSC_0628.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_0628.jpg" alt="Scuola" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_0679.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_0679.jpg" alt="Scuola" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_0713.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_0713.jpg" alt="Scuola" ></a>			
			</div>
			<div class="gallerylast">
				<a href="images/gallery/original/DSC_0724.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_0724.jpg" alt="Scuola" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_0741.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_0741.jpg" alt="Scuola" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_2961.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_2961.jpg" alt="Scuola" ></a>			
			</div>
			<div class="gallery">
				<a href="images/gallery/original/DSC_3004.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_3004.jpg" alt="Scuola" ></a>			
			</div>
			<div class="gallerylast">
				<a href="images/gallery/original/DSC_3013.jpg" rel="lightbox[scuola]"><img src="images/gallery/thumb/DSC_3013.jpg" alt="Scuola" ></a>			
			</div>		
-->
		</div>
</div>
</div>

<?php
include("footer.php");
?>