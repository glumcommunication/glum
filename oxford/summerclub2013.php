<?php
$thisPage = "Summer Club 2013";
$description = "Scuola di inglese, francese, spagnolo, tedesco, russo, arabo, cinese, italiano per stranieri, a Siena in Toscana, Italia. Scuola di lingue straniere a Siena. Viaggi studio per famiglie, ragazzi e adulti.";
include("header.php");
include("menu.php");
?>

<div id="main">
<div class="leftSide">		
	<div class="sbox">
	<h1>Summer Club</h1>
		<img src="images/summerclub_2013.jpg" alt="Summer Club" />			
	</div>
</div>
<div class="rightSide">
	<div id="lbox">
        <h2>
            SUMMER CLUB 2013 - ORTO DE’ PECCI</br>
            …Per imparare l'inglese divertendosi anche in estate quando la scuola è finita!
        </h2>
        <p>    
            I nostri <strong>Summer Club</strong> nascono dal crescente interesse da parte dei genitori affinché i ragazzi trascorrano 
            anche in estate dei momenti piacevoli immergendosi sempre piu' nella lingua inglese.<br>
            I  Summer Club sono dei corsi per ragazzi dai 6 ai 16 anni costituiti da una serie di attività 
            coinvolgenti per imparare in modo spontaneo l'inglese, divertendosi insieme ai nostri insegnanti 
            madrelingua e qualificati.<br>
            I  Summer Club della New Oxford School  non sono dei semplici campi solari ma dei veri e propri corsi 
            di lingua inglese, proprio come quelli che la Scuola organizza durante l’anno scolastico ma organizzati 
            all’aria aperta, a contatto con la natura e con una formula  full immersion. Tale organizzazione permetterà  
            ai ragazzi di vivere un’esperienza simile al soggiorno studio estivo all’estero rimanendo nella propria città, 
            quindi potremmo considerarla una valida alternativa.<br>
            Il Summer Club  sarà organizzato in moduli della durata di una settimana, per un totale di quattro settimane.
            Ogni giorno, dal lunedì al venerdì, si svolgerà una lezione di inglese strutturata secondo il metodo The New 
            Oxford School, sede <strong>Trinity College London</strong>, ente certificatore internazionale per l’inglese.<br>
            Il docente madrelingua è sempre a contatto diretto con i ragazzi e li stimola a comunicare solo in lingua inglese, 
            incentivando la capacità di apprendimento e di partecipazione. I corsi per bambini e ragazzi sono studiati proprio 
            per suscitare interesse e curiosità verso la lingua straniera e l'apprendimento è veloce proprio perché diventa 
            l’unico modo per comunicare con l’insegnante. Ogni giorno tutte le attività saranno svolte in inglese, compresi  
            i momenti di gioco, sport e relax.<br>
            Il venerdì di ogni settimana è prevista la visione di un film o un cartone animato per bambini e ragazzi in 
            lingua inglese e durante l’ultima ora verranno consegnati gli attestati di partecipazione al Summer Club con 
            riportato il livello di inglese.<br>
            L’obiettivo di questi corsi è quello di stimolare i ragazzi all’apprendimento della lingua inglese affiancando 
            alla lezione tradizionale una serie di attività ludiche e ricreative anche attraverso il gioco di squadra, il tutto 
            sotto la guida costante dei nostri docenti madrelingua.<br>
            I ragazzi che vi parteciperanno, se non sono già allievi della Scuola, effettueranno un breve test d’accesso per 
            valutare il livello di conoscenza della lingua così da poter essere inseriti in mini gruppi omogenei in quanto a 
            età e livello.<br>
            Le lezioni saranno svolte esclusivamente in lingua inglese cosicchè i ragazzi saranno a contatto diretto con la 
            lingua durante tutta la giornata imparando e migliorando il proprio inglese in maniera spontanea e naturale. 
            I ragazzi avranno l’opportunità di socializzare e soprattutto esprimere la loro creatività in lingua inglese.
        </p>
        <h3>    
            Alcuni esempi di attività:
        </h3>
        <p>
            <strong>Consolidamento grammatica inglese attraverso varie attività e giochi:</strong><br>
            lettura, storie e comprensione, ore di conversazione in lingua, teatro, quiz, attività sportive, caccia al tesoro, 
            karaoke, proiezioni film, pittura, cartoni animati per bambini canzoni, arti e mestieri, ruba bandiera, bingo 
            (fun e game, sing a long song, story time, arts e crafts, cool conversation, play theatre, net rasure hunt, 
            grammar gang, sharades, puzzles, canzoni, arti e mestieri, ruba bandiera, bingo, go fish) ecc. 
            e molto altro ancora, soprattutto quest’estate grazie ai nuovi orari e alla nuova location.
        </p>
        <p>
            <strong>Sport</strong><br>
            Volleyball, frisbee, football americano, calcio ecc.
        </p>
        <h3>
            Materiali
        </h3>
        <p>
            La Scuola fornirà per ciascun partecipante il materiale didattico, materiale artistico e tutti gli equipaggiamenti sportivi.
        </p>
        <p>
            Cosa devono portare i ragazzi
        </p>
        <ul class="oxlist">
            <li>Crema solare</li>
            <li>Scarpe da ginnastica</li>
            <li>Asciugamano</li>
            <li>Un quaderno</li>
            <li>Un cappellino</li>
            <li>Fazzoletti</li>
        </ul>
        <h3>
            Periodo
        </h3>
        <p>
            I genitori possono scegliere una o piu’ settimane a partire da lunedì 11 giugno 2013, dal lunedì al venerdì dalle ore 9 alle ore 15 o dalle 
            ore 9 alle ore 17 nei mesi di giugno luglio e agosto.
        </p>
        <h3>
            Luogo di svolgimento
        </h3>
        <p>               
            <strong>Orto De’ Pecci</strong><br>
            Via di Porta Giustizia, 39 a Siena, a Porta Romana.
        </p>
        <p>    
            A tutti i ragazzi sarà offerta una colazione a metà mattina e il pranzo presso il ristorante “All’ Orto dè Pecci” con prodotti locali e genuini.<br>
            Per i ragazzi che si trattengono fino alle 17,00 anche la merenda.
        </p>
        <p>
            La quota di iscrizione al Summer Club 2013 comprende:
        </p>
        <ul class="oxlist">
            <li>Corso di inglese con attestato di partecipazione</li>
            <li>Attività all’aperto, sport e giochi di squadra</li>
            <li>Attività e materiale didattico</li>
            <li>Colazione, pranzo (e merenda)</li>
            <li>Assicurazione</li>
        </ul>
        <p>
            Per i genitori interessati, le iscrizioni vanno effettuate entro il <strong>15 GIUGNO 2013</strong> così da poter avere le settimane di 
            interesse e poter procedere all’organizzazione dei mini gruppi e relativa definizione degli orari e del programma.
        </p>
        <h3>    
            I nostri riferimenti:
        </h3>
        <p>
            La <strong>New Oxford School</strong> si trova in via Cesare Battisti, 4 a Siena, zona S. Prospero.<br>
            La Segreteria  è a vostra disposizione dal lunedì al venerdì dalle ore 9.00 alle 20.00 e il sabato mattina, 
            dalle 9.30 alle 13.00. (il sabato rimarrà chiusa nei mesi di giugno, luglio e agosto)
        </p>

	</div>
</div>
</div>

<?php
include("footer.php");
?>