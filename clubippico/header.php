<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="description" content="<?php echo $pagedesc ?>">
<meta name= "keywords" content= "Club Ippico Senese, cavalli, scuola di equitazione, Siena, Pian del Lago, Monteriggioni, Pony club, campi estivi, Pany games, poniadi, ippoterapia, corsi, stage, eventi" />
<meta name="author" content="Cesare Rinaldi">
<link rel="stylesheet" type="text/css" href="css/style.css" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>

	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<!-- SCROLLBAR -->
	<link rel="icon" href="images/favicon.png" type="image/svg"/>
	<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" />
	<!-- mousewheel plugin -->
	<script src="js/jquery.mousewheel.min.js"></script>
	<!-- custom scrollbars plugin -->
	<script src="js/jquery.mCustomScrollbar.js"></script>
<link href="css/flexslider.css" rel="stylesheet" type="text/css" />
<script src="js/jquery.flexslider.js"></script>
<link href="css/lightbox.css" rel="stylesheet" />
<script src="js/lightbox.js"></script>
		<script>
 (function($){


			$(document).ready(function(){
				/* custom scrollbar fn call */
				$("#scroll1").mCustomScrollbar({
					scrollButtons:{
						enable:true

					},
					advanced:{
    updateOnContentResize:true
},


				});
				$("#scroll2").mCustomScrollbar({
					scrollButtons:{
						enable:true

					},
					advanced:{
    updateOnContentResize:true
},


				});
				$("#scroll3").mCustomScrollbar({
					scrollButtons:{
						enable:true

					},
					advanced:{
    updateOnContentResize:true
},


				});
			})

				 })(jQuery);
	</script>
<script>
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    itemWidth: 240,
    itemMargin: 20,
    controlNav: false,
    slideshow: false,
    move: 1
  });
});
</script>
<title><? echo $pagetitle; ?> | Club Ippico Senese - Strada di Pian del Lago, 53035 Monteriggioni - Siena | Scuola equitazione a Siena</title>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40926783-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>

<div id="wrapper">
	<div id="header"><!--<img src="images/headerlogo3.png" width="975" height="53" alt="Club Ippico Senese">--></div>
	<div id="container">
<?php include_once 'menu.php'; ?>
