<?php
$pagename = 'news';
$pagetitle = 'News';
include_once 'header.php';
include_once('cbd.php');

	/*
		Beginning of pagination
	*/


	$tbl_name="news";		//your table name
	// How many adjacent pages should be shown on each side?
	$adjacents = 3;
	
	/* 
	   First get total number of rows in data table. 
	   If you have a WHERE clause in your query, make sure you mirror it here.
	*/
	$query = "SELECT COUNT(*) as num FROM $tbl_name";
	$total_pages = mysql_fetch_array(mysql_query($query));
	$total_pages = $total_pages[num];
//echo $total_pages;
//	$total_pages = $total_pages[num];
	
	/* Setup vars for query. */
	$targetpage = "news.php"; 	//your file name  (the name of this file)
	$limit = 3; 								//how many items to show per page
	$page = $_GET['page'];
	if($page) 
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;								//if no page var is given, set start to 0
	
	/* Get data. */
	$sql = "SELECT * FROM $tbl_name ORDER BY id DESC LIMIT $start, $limit";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);

//if($total_pages==0){echo "<p>Nel database non sono presenti schede paziente.</p>";}

//elseif($total_pages==1) {echo "<p>Nel database &egrave; presente una scheda paziente.</p>";}

//else {echo "<p>Nel database sono presenti " . $total_pages . " schede paziente.</p>";}

//echo "<p>Nel database sono presenti " . $rows . " schede paziente.</p>";

for ($j = 0 ; $j < $rows ; ++$j)

	
	/* Setup page vars for display. */
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1
	
	/* 
		Now we apply our rules and draw the pagination object. 
		We're actually saving the code to a variable in case we want to draw it more than once.
	*/
	$pagination = "";
	if($lastpage > 1)
	{	
		$pagination .= "<div class=\"pagination\">";
		//previous button
		if ($page > 1) 
			$pagination.= "<a href=\"$targetpage?page=$prev\">&laquo; Pi&ugrave; nuove</a>";
		else
			$pagination.= "<span class=\"disabled\">&laquo; Pi&ugrave; nuove</span>";	
		
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<span class=\"current\">$counter</span>";
				else
					$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
				$pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<a href=\"$targetpage?page=1\">1</a>";
				$pagination.= "<a href=\"$targetpage?page=2\">2</a>";
				$pagination.= "...";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
				$pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<a href=\"$targetpage?page=1\">1</a>";
				$pagination.= "<a href=\"$targetpage?page=2\">2</a>";
				$pagination.= "...";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
				}
			}
		}
		
		//next button
		if ($page < $counter - 1) 
			$pagination.= "<a href=\"$targetpage?page=$next\">Pi&ugrave; vecchie &raquo;</a>";
		else
			$pagination.= "<span class=\"disabled\">Pi&ugrave; vecchie &raquo;</span>";
		$pagination.= "</div>\n";		
	}
?>

		<div id="main">

				<div class="horizontalcontent" id="scroll1">
<?php

for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);
$datasql = "SELECT DATE_FORMAT(Data, '%d/%m/%Y')  as data FROM news WHERE id='$row[5]'";

$rowdata = mysql_fetch_assoc(mysql_query($datasql));
//$newsBody = substr($row[1],0,50) . "...";
echo <<<_END
            <div class="horizontaltext">
			<h2 class="news"><a href="shownews.php?slug=$row[4]" title="$row[0]">$row[0]</a></h2>
            <h5 class="news">$rowdata[data], $row[3] (<a href="shownews.php?slug=$row[4]" title="$row[0]">Link</a>)</h5>
            $row[1]
            </div>
         
_END;
}
?>	  
				<!--<div class="newsleft">
					<img src="images/test_cavallodondolo.jpg" width="220" height="155" alt="test image">
				</div>-->
					<!--<div class="horizontaltext" id="scroll1">
					<h1>1Stage</h1>
					<p>1Progetto complicità dai 3 ai 6 
anni. Obiettivo favorire l'avvi-
cinamento al mondo del pony
attraverso la scoperta del pro-
prio corpo e la complicità del
genitore. Il progetto prevede,
infatti, un iniziale coinvolgi-
mento dei genitori per arrivare
poi all'autonomia del bambino.</p>
					<p>Progetto complicità dai 3 ai 6
anni. Obiettivo favorire l'avvi-
cinamento al mondo del pony
attraverso la scoperta del pro-
prio corpo e la complicità del
genitore. Il progetto prevede,
infatti, un iniziale coinvolgi-
mento dei genitori per arrivare
poi all'autonomia del bambino.</p>	
					<p>Progetto complicità dai 3 ai 6
anni. Obiettivo favorire l'avvi-
cinamento al mondo del pony
attraverso la scoperta del pro-
prio corpo e la complicità del
genitore. Il progetto prevede,
infatti, un iniziale coinvolgi-
mento dei genitori per arrivare
poi all'autonomia del bambino.</p>	
					<p>Progetto complicità dai 3 ai 6
anni. Obiettivo favorire l'avvi-
cinamento al mondo del pony
attraverso la scoperta del pro-
prio corpo e la complicità del
genitore. Il progetto prevede,
infatti, un iniziale coinvolgi-
mento dei genitori per arrivare
poi all'autonomia del bambino.</p>	
				</div>
			</div>-->

			</div>
			<div class="more">
				<?=$pagination?>			
			</div>
		

<?php
include_once 'footer.php';
?>