<?php
$pagename = 'index';
$pagetitle = 'Home';
include_once 'header.php';
?>


		<div id="main">
			<div class="hometop">
				<!--<img src="images/homepic.jpg" width="240" height="160" alt="Club Ippico Senese">-->
				<div class="hometext" id="scroll1">
                    <p>
                        Il <strong>Club Ippico Senese - Associazione Sportiva Dilettantistica</strong>, è una scuola di equitazione affiliata 
                        alla Federazione Italiana Sport Equestri dal 1966.
                    </p>
                    <p>
                        Ubicato ai piedi della Montagnola, in una posizione suggestiva e circondata dal verde della campagna senese, il Club fu 
                        fondato nel 1966 da un gruppo di amatori senesi che iniziarono la divulgazione dell'equitazione allo scopo di trasmettere 
                        lo spirito di condivisione e di puro divertimento unita alla passione per il cavallo. Fu un successo tanto che, dato il 
                        numero sempre crescente di persone che si avvicinarono a questo mondo, si rese necessario dare al Club un'impronta un po' 
                        meno "casalinga" guardando verso progetti più definiti e consistenti. Da qui nacque l'esigenza di dar vita a una vera e 
                        propria scuola che fu affidata all'Istruttore federale Giuseppe Ramirez, che ha seguito allievi e agonisti fino al 2002, 
                        anno del suo pensionamento. Da sempre il Club Ippico Senese è stato il punto di riferimento di quanti, a Siena e provincia, 
                        hanno praticato gli sport equestri, essendo una delle prime istituzioni del settore. Attualmente la nostra scuola, diretta 
                        dall'Istruttore federale Lorenzo Ginanneschi, è una delle prime in Toscana. Molte sono le attività portate avanti dal Club 
                        Ippico Senese, rivolte in particolare ai ragazzi, prima tra tutte la Scuola Pony. Annualmente, nel periodo giugno - settembre, 
                        vengono organizzati, da oltre 10 anni, i Campi solari estivi settimanali.
                    </p>
                    <p>
                        Il club frequenta e ha anche ospitato i corsi di formazione Fise per tecnici addestrativi ludici a livello regionale. Gli 
                        stessi istruttori del Club seguono regolarmente corsi di aggiornamento.
                    </p>
                    <p>
                        Il Club Ippico Senese dispone adesso di una tensostruttura di 22mX60m grazie al contributo della Fondazione Monte dei Paschi di Siena.
                    </p>
				</div>
			</div>
			<div class="homebottom"><h2>MULTIMEDIA</h2>
				<div class="flexslider">
					<ul class="slides">
            			<li><a href="images/galleries/scuola_01.jpg" rel="lightbox[scuola]" title=""><img src="images/galleries/scuola_01.jpg" width="240" height="192" alt="Esempio" /><div class="label">La scuola</div></a>
							<div style="visibility: hidden;">
                                <a href="images/galleries/scuola_15.jpg" rel="lightbox[scuola]" title=""></a>
                                <a href="images/galleries/scuola_16.jpg" rel="lightbox[scuola]" title=""></a>
                                <a href="images/galleries/scuola_17.jpg" rel="lightbox[scuola]" title=""></a>
    							<a href="images/galleries/scuola_02.jpg" rel="lightbox[scuola]" title=""></a>
    							<a href="images/galleries/scuola_04.jpg" rel="lightbox[scuola]" title=""></a>
    							<a href="images/galleries/scuola_06.jpg" rel="lightbox[scuola]" title=""></a>
								<a href="images/galleries/scuola_07.jpg" rel="lightbox[scuola]" title=""></a>
    							<a href="images/galleries/scuola_08.jpg" rel="lightbox[scuola]" title=""></a>
								<a href="images/galleries/scuola_09.jpg" rel="lightbox[scuola]" title=""></a>
    							<a href="images/galleries/scuola_10.jpg" rel="lightbox[scuola]" title=""></a>
								<a href="images/galleries/scuola_11.jpg" rel="lightbox[scuola]" title=""></a>
    							<a href="images/galleries/scuola_12.jpg" rel="lightbox[scuola]" title=""></a>
								<a href="images/galleries/scuola_13.jpg" rel="lightbox[scuola]" title=""></a>
    							<a href="images/galleries/scuola_14.jpg" rel="lightbox[scuola]" title=""></a>
							</div>
						</li>
        				<li><a href="images/galleries/gare_01.jpg" rel="lightbox[gare]" title=""><img src="images/galleries/gare_01.jpg" width="240" height="192" alt="gare" /><div class="label">Le gare</div></a>
							<div style="visibility: hidden;">
                                <a href="images/galleries/gare_43.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_44.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_45.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_46.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_47.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_48.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_49.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_50.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_51.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_52.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_53.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_54.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_55.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_56.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_57.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_58.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_59.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_60.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_61.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_62.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_63.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_64.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_65.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_66.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_67.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_68.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_69.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_70.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_71.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_72.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_73.jpg" rel="lightbox[gare]" title=""></a>
    							<a href="images/galleries/gare_04.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_05.jpg" rel="lightbox[gare]" title=""></a>
								<a href="images/galleries/gare_06.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_07.jpg" rel="lightbox[gare]" title=""></a>
								<a href="images/galleries/gare_08.jpg" rel="lightbox[gare]" title=""></a>
								<a href="images/galleries/gare_09.jpg" rel="lightbox[gare]" title=""></a>
    							<a href="images/galleries/gare_10.jpg" rel="lightbox[gare]" title=""></a>
								<a href="images/galleries/gare_11.jpg" rel="lightbox[gare]" title=""></a>
    							<a href="images/galleries/gare_12.jpg" rel="lightbox[gare]" title=""></a>
								<a href="images/galleries/gare_13.jpg" rel="lightbox[gare]" title=""></a>
    							<a href="images/galleries/gare_14.jpg" rel="lightbox[gare]" title=""></a>
								<a href="images/galleries/gare_15.jpg" rel="lightbox[gare]" title=""></a>
    							<a href="images/galleries/gare_16.jpg" rel="lightbox[gare]" title=""></a>
								<a href="images/galleries/gare_17.jpg" rel="lightbox[gare]" title=""></a>
    							<a href="images/galleries/gare_23.jpg" rel="lightbox[gare]" title=""></a>
								<a href="images/galleries/gare_32.jpg" rel="lightbox[gare]" title=""></a>
								<a href="images/galleries/gare_34.jpg" rel="lightbox[gare]" title=""></a>
    							<a href="images/galleries/gare_35.jpg" rel="lightbox[gare]" title=""></a>
								<a href="images/galleries/gare_36.jpg" rel="lightbox[gare]" title=""></a>
								<a href="images/galleries/gare_38.jpg" rel="lightbox[gare]" title=""></a>
                                <a href="images/galleries/gare_39.jpg" rel="lightbox[gare]" title=""></a>
							</div>
						</li>
    					<li><a href="images/galleries/corsi_01.jpg" rel="lightbox[corsi]" title=""><img src="images/galleries/corsi_01.jpg" width="240" height="192" alt="Corsi" /><div class="label">I nostri corsi</div></a>
							<div style="visibility: hidden;">
                                <a href="images/galleries/corsi_29.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_30.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_31.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_32.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_33.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_34.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_35.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_36.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_37.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_38.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_39.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_40.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_41.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_42.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_43.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_44.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_45.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_46.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_47.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_48.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_49.jpg" rel="lightbox[corsi]" title=""></a>
                                <a href="images/galleries/corsi_50.jpg" rel="lightbox[corsi]" title=""></a>
    							<a href="images/galleries/corsi_02.jpg" rel="lightbox[corsi]" title=""></a>
								<a href="images/galleries/corsi_03.jpg" rel="lightbox[corsi]" title=""></a>
								<a href="images/galleries/corsi_05.jpg" rel="lightbox[corsi]" title=""></a>
    							<a href="images/galleries/corsi_06.jpg" rel="lightbox[corsi]" title=""></a>
								<a href="images/galleries/corsi_09.jpg" rel="lightbox[corsi]" title=""></a>
    							<a href="images/galleries/corsi_12.jpg" rel="lightbox[corsi]" title=""></a>
								<a href="images/galleries/corsi_13.jpg" rel="lightbox[corsi]" title=""></a>
    							<a href="images/galleries/corsi_14.jpg" rel="lightbox[corsi]" title=""></a>
    							<a href="images/galleries/corsi_22.jpg" rel="lightbox[corsi]" title=""></a>
        						<a href="images/galleries/corsi_27.jpg" rel="lightbox[corsi]" title=""></a>
								<a href="images/galleries/corsi_28.jpg" rel="lightbox[corsi]" title=""></a>
							</div>
						</li>
    					<li><a href="images/galleries/eventi_19.jpg" rel="lightbox[eventi]" title=""><img src="images/galleries/eventi_01.jpg" width="240" height="192" alt="Esempio" /><div class="label">Gli eventi</div></a>
							<div style="visibility: hidden;">
                                <a href="images/galleries/eventi_20.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_21.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_22.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_23.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_24.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_25.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_26.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_27.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_28.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_29.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_30.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_31.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_32.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_33.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_34.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_35.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_36.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_37.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_38.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_39.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_40.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_41.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_42.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_43.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_44.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_45.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_46.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_47.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_48.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_49.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_50.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_51.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_52.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_53.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_54.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_55.jpg" rel="lightbox[eventi]" title=""></a>
                                <a href="images/galleries/eventi_56.jpg" rel="lightbox[eventi]" title=""></a>
    							<a href="images/galleries/eventi_06.jpg" rel="lightbox[eventi]" title=""></a>
    							<a href="images/galleries/eventi_16.jpg" rel="lightbox[eventi]" title=""></a>
        						<a href="images/galleries/eventi_18.jpg" rel="lightbox[eventi]" title=""></a>
							</div>
						</li>
						<li><a href="images/galleries/cavalli_01.jpg" rel="lightbox[cavalli]" title=""><img src="images/galleries/cavalli_01.jpg" width="240" height="192" alt="Esempio" /><div class="label">I nostri cavalli</div></a>
							<div style="visibility: hidden;">
                                <a href="images/galleries/corsi_26.jpg" rel="lightbox[cavalli]" title=""></a>
                                <a href="images/galleries/cavalli_20.jpg" rel="lightbox[cavalli]" title=""></a>
                                <a href="images/galleries/cavalli_21.jpg" rel="lightbox[cavalli]" title=""></a>
                                <a href="images/galleries/cavalli_22.jpg" rel="lightbox[cavalli]" title=""></a>
                                <a href="images/galleries/cavalli_23.jpg" rel="lightbox[cavalli]" title=""></a>
                                <a href="images/galleries/cavalli_24.jpg" rel="lightbox[cavalli]" title=""></a>
                                <a href="images/galleries/cavalli_25.jpg" rel="lightbox[cavalli]" title=""></a>
                                <a href="images/galleries/cavalli_26.jpg" rel="lightbox[cavalli]" title=""></a>
                                <a href="images/galleries/cavalli_27.jpg" rel="lightbox[cavalli]" title=""></a>
                                <a href="images/galleries/cavalli_28.jpg" rel="lightbox[cavalli]" title=""></a>
                                <a href="images/galleries/cavalli_29.jpg" rel="lightbox[cavalli]" title=""></a>
                                <a href="images/galleries/cavalli_30.jpg" rel="lightbox[cavalli]" title=""></a>
                                <a href="images/galleries/cavalli_31.jpg" rel="lightbox[cavalli]" title=""></a>
                                <a href="images/galleries/cavalli_32.jpg" rel="lightbox[cavalli]" title=""></a>
                                <a href="images/galleries/cavalli_33.jpg" rel="lightbox[cavalli]" title=""></a>
                                <a href="images/galleries/cavalli_34.jpg" rel="lightbox[cavalli]" title=""></a>
								<a href="images/galleries/cavalli_03.jpg" rel="lightbox[cavalli]" title=""></a>
    							<a href="images/galleries/cavalli_04.jpg" rel="lightbox[cavalli]" title=""></a>
    							<a href="images/galleries/cavalli_06.jpg" rel="lightbox[cavalli]" title=""></a>
								<a href="images/galleries/cavalli_07.jpg" rel="lightbox[cavalli]" title=""></a>
								<a href="images/galleries/cavalli_09.jpg" rel="lightbox[cavalli]" title=""></a>
    							<a href="images/galleries/cavalli_10.jpg" rel="lightbox[cavalli]" title=""></a>
								<a href="images/galleries/cavalli_11.jpg" rel="lightbox[cavalli]" title=""></a>
    							<a href="images/galleries/cavalli_12.jpg" rel="lightbox[cavalli]" title=""></a>
								<a href="images/galleries/cavalli_13.jpg" rel="lightbox[cavalli]" title=""></a>
								<a href="images/galleries/cavalli_15.jpg" rel="lightbox[cavalli]" title=""></a>
								<a href="images/galleries/cavalli_17.jpg" rel="lightbox[cavalli]" title=""></a>
								<a href="images/galleries/cavalli_19.jpg" rel="lightbox[cavalli]" title=""></a>
							</div>
						</li>
                        <li style="width: 240px;height:192px;float: left;box-shadow: 0 0 10px #333;display: block;border-radius: 0;overflow:hidden;">
                            <iframe src="https://www.facebook.com/video/embed?video_id=117260681768715" width="240" height="192" frameborder="0"></iframe>
                            <!--<object width="240" height="192">
                                <param name="allowfullscreen" value="true"></param>
                                <param name="movie" value="http://www.facebook.com/v/117260681768715"></param>
                                <embed src="http://www.facebook.com/v/117260681768715" type="application/x-shockwave-flash" allowfullscreen="1" width="240" height="192"></embed>
                            </object>-->
                        </li>


					</ul>
				</div>
				<!--<div class="homebottomcontent">
					<ul class="medialist">
						<li><object width="175" height="140"><param name="allowfullscreen" value="true"></param><param name="movie" value="http://www.facebook.com/v/117260681768715"></param><embed src="http://www.facebook.com/v/117260681768715" type="application/x-shockwave-flash" allowfullscreen="1" width="175" height="140"></embed></object></li>
						<li><iframe width="175" height="140" src="http://www.youtube.com/embed/trAIzB2qTRQ" frameborder="0" allowfullscreen></iframe></li>
					</ul>
				</div>-->

			</div>
		</div>


<?php
include_once 'footer.php';
?>