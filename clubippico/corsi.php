<?php
$pagename = 'corsi';
$pagetitle = 'Corsi';
include_once 'header.php';
?>

		<div id="main">
			<div class="verticalcontainer">
				<div class="verticalcontent">
					<h1>Pony Club</h1>
					<img src="images/ponyclub.jpg" width="220" height="155" alt="Pony Club">
					<div class="verticaltext" id="scroll1">
					<p>
                        Attivi da circa dieci anni i Pony Club sono rivolti ai giovani dai 3 ai 16 anni. I corsi si svolgono 
                        durante tutto l'anno, dal lunedì alla domenica, e sono suddivisi per età e competenza. Grazie a 20 pony 
                        di varia altezza i ragazzi potranno imparare, attraverso dei giochi, a lavorare di equilibrio per la 
                        coordinazione e il giusto schema corporeo, aspetti fondamentali per integrare le capacità tecniche. I 
                        giochi di squadra, a carattere ludico e pedagogico, avranno benefici anche sull’emotività del bambino 
                        rafforzandone il carattere. Queste attività aiutano a collaborare con i propri compagni, stimolano la 
                        comunicazione e insegnano la disciplina. Ogni allievo infatti riuscirà a tirare fuori la grinta e la 
                        determinazione necessarie per gestire l’animale autonomamente favorendo il processo di autostima necessario 
                        per aprire la personalità dei più timidi ma anche a smussare l’esuberanza dei giovani più vivaci. Le 
                        attività si suddividono in Pony Games, Gimkana e Carosello e saranno seguite dal tecnico addestrativo 
                        ludico Simona Monciatti. Su richiesta si possono effettuare anche lezioni tradizionali. I Pony Club 
                        rappresentano anche una opportunità di formazione per chi vuole entrare nel mondo dell’agonismo.
                    </p>
				</div>
			</div>
				<div class="verticalcontent" id="vcmiddle">
					<h1>Corsi per Adulti e Ragazzi</h1>				
					<img src="images/corsiadultiragazzi.jpg" width="220" height="155" alt="Corsi per adulti e ragazzi">							
					<div class="verticaltext" id="scroll2">
					<p>
                        Strutturati per vari livelli di preparazione dell’allievo, si svolgono dal lunedì alla domenica durante 
                        tutto l’anno. Le lezioni usufruiscono del metodo tradizionale e grazie all’esperienza dei tecnici federali 
                        sono adatti per coloro che vogliono muovere i primi passi nella disciplina ma anche per chi desidera ottenere 
                        una adeguata preparazione agonistica.
                    </p>	
				</div>						
				</div>
				<div class="verticalcontent">
					<h1>Corsi Estivi</h1>				
					<img src="images/campiestivi.jpg" width="220" height="155" alt="Campi estivi">
					<div class="verticaltext" id="scroll3">
					<p>
                        Organizzati con cadenza settimanale da giugno a settembre (dal lunedì al venerdì con possibilità di ripetere la 
                        settimana) i ragazzi dai 4 ai 16 anni potranno avvicinarsi alla disciplina grazie a un doppio approccio tecnico 
                        ed educativo. Verranno spiegate le mansioni di sellaggio, pulizia e alimentazione oltre alle procedure di montaggio 
                        che si avvarranno anche del metodo psicologico dell’ippoterapia. I ragazzi, in proporzione all’età e alla propria 
                        struttura fisica, impareranno a cavalcare pony di varie dimensioni attraverso dei giochi mirati a ottenere il giusto 
                        equilibrio e un’efficace schema corporeo. Non solo, durante la fase d’apprendimento verrà curata la gestione e il 
                        controllo delle emozioni per favorire il processo di crescita e autostima personale. Ogni venerdì, alla presenza dei 
                        genitori, sarà realizzata un’esibizione dove sarà presentato quanto appreso durante le lezioni.
                    </p>	
				</div>													
				</div>
			</div>
		</div>

<?php
include_once 'footer.php';
?>