<?php
$pagename = 'eventi';
$pagetitle = 'Eventi';
include_once 'header.php';
?>

		<div id="main">
			<div class="verticalcontainer">
				<div class="verticalcontent">
					<h1>Stage</h1>
					<img src="images/stage.jpg" width="220" height="155" alt="Stage">
					<div class="verticaltext" id="scroll1">
					<p>
                        Gli <strong>stage pony</strong> si avvalgono della collaborazione del Tecnico federale nazionale Jacques Cavè e del 
                        tecnico addestrativo ludico e psicologa <strong>D.ssa Marie Catherine Boibien</strong>. Vengono organizzati anche gli 
                        <strong>stage cavalli</strong> seguiti periodicamente da vari tecnici specializzati.
                    </p>	
				</div>
			</div>
				<div class="verticalcontent" id="vcmiddle">
					<h1>Eventi Sociali</h1>				
					<img src="images/eventisociali.jpg" width="220" height="155" alt="Eventi Sociali">							
					<div class="verticaltext" id="scroll2">
					<p>
                        Per preparare al meglio le gare esterne vengono organizzate le “giornate pony” mirate per gli allievi che 
                        svolgono attività agonistica. Inoltre, sempre allo stesso scopo, sono in programma anche i concorsi sociali 
                        di salto ostacoli (con percorsi da 60 e 1,35 m). Per tutto l’anno celebriamo eventi (compleanni, cene sociali, 
                        piccoli rinfreschi) per stare in compagnia e passare serate all’insegna dell’allegria. 
                    </p>	
				</div>						
				</div>
				<div class="verticalcontent">
					<h1>Gare</h1>				
					<img src="images/gare.jpg" width="220" height="155" alt="Gare">
					<div class="verticaltext" id="scroll3">
					<!--<p>Progetto complicità dai 3 ai 6
anni. Obiettivo favorire l'avvi-
cinamento al mondo del pony
attraverso la scoperta del pro-
prio corpo e la complicità del
genitore. Il progetto prevede,
infatti, un iniziale coinvolgi-
mento dei genitori per arrivare
poi all'autonomia del bambino.</p>
					<p>Progetto complicità dai 3 ai 6
anni. Obiettivo favorire l'avvi-
cinamento al mondo del pony
attraverso la scoperta del pro-
prio corpo e la complicità del
genitore. Il progetto prevede,
infatti, un iniziale coinvolgi-
mento dei genitori per arrivare
poi all'autonomia del bambino.</p>	
					<p>Progetto complicità dai 3 ai 6
anni. Obiettivo favorire l'avvi-
cinamento al mondo del pony
attraverso la scoperta del pro-
prio corpo e la complicità del
genitore. Il progetto prevede,
infatti, un iniziale coinvolgi-
mento dei genitori per arrivare
poi all'autonomia del bambino.</p>	
					<p>Progetto complicità dai 3 ai 6
anni. Obiettivo favorire l'avvi-
cinamento al mondo del pony
attraverso la scoperta del pro-
prio corpo e la complicità del
genitore. Il progetto prevede,
infatti, un iniziale coinvolgi-
mento dei genitori per arrivare
poi all'autonomia del bambino.</p>-->
				</div>													
				</div>
			</div>
		</div>

<?php
include_once 'footer.php';
?>