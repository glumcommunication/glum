<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name= "keywords" content= "agenzia, aziende, privati, consulenze, siena, vodaofone, partita iva, offerte, promoter, piani telefonici"  />
<?php 
if ($pagename == "contatti") {
    echo <<<_END
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    function initialize() {
        var latlng = new google.maps.LatLng(43.318278, 11.333535);
        var settings = {
            zoom: 15,
            center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
            navigationControl: true,
            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
            mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"), settings);

  var companyLogo = new google.maps.MarkerImage('images/cursorshadow.png',
    new google.maps.Size(86,58),
    new google.maps.Point(0,0),
    new google.maps.Point(20,50)
);

var companyPos = new google.maps.LatLng(43.318278, 11.333535);
var companyMarker = new google.maps.Marker({
    position: companyPos,
    map: map,
    icon: companyLogo,
    //shadow: companyShadow,
    title:"F@M Group - Siena"
});
var contentString = '<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h1 id="firstHeading" class="firstHeading">F@M Group - Siena</h1>'+
    '<div id="bodyContent">'+
    '<p>Via del Porrione, 50 - 53100 Siena<br>Telefono: +39 0577  221710<br>Email: <a href="mailto:info@vodafonesiena.it">info@vodafonesiena.it</a><br>Website: <a href="http://www.vodafonesiena.it" title="F@M Group Siena - Vodafone">www.vodafonesiena.it</a><br></p>'+
    '</div>'+
    '</div>';
 
var infowindow = new google.maps.InfoWindow({
    content: contentString
});
google.maps.event.addListener(companyMarker, 'click', function() {
  infowindow.open(map,companyMarker);
});
    }
    </script>
_END;
}

?>        
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet" media="screen">
        <title>
            <? echo $pagetitle; ?> | F@M Group Siena - Vodafone | Via del Porrione 50, 53100 Siena
        </title>
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-40343924-1']);
            _gaq.push(['_setDomainName', 'vodafonesiena.it']);
            _gaq.push(['_setAllowLinker', true]);
            _gaq.push(['_trackPageview']);
            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>
    </head>
<body<? if ($pagename == "contatti") echo " onload=\"initialize()\"";?>>
<div class="wrap">    