<?php
$pagetitle = "Offerte";
$pagename = "offerte";
include_once 'cbd.php';
include_once 'header.php';
include_once 'navigation.php';
?>
        <div class="container">
            <div class="row-fluid lastrow">
                <div class="resizer span12 shadowed">
                    <div class="container ribbon">
                        <div class="ribbon_l"></div>
                        <div class="ribbon_m"><h2>Offerte</h2></div>
                        <div class="ribbon_r"></div>
                    </div>
<?php
	$sql = "SELECT * FROM products ORDER BY Data DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
	$rows = mysql_num_rows($result);
	$sum = 0;
    if ($rows==0) {
        echo "Al momento non &egrave; presente nessuna offerta.";
    }
	for ($j = 0 ; $j < $rows ; ++$j) {
		$row = mysql_fetch_row($result);
echo <<<_END
                    <div class="row-fluid">
                        <div class="span12">
                            <h4 class="newstitle">
                                $row[0]
                            </h4>
                        </div>
                    </div>
                    <div class="row-fluid lastrow redline">
                        <div class="span8">
                            <img src="$row[2]" alt="$row[0]">
                        </div>
                        <div class="span4 justify">
                            
                                $row[1]
                            
                        </div>
                    </div>
_END;
}
?>

                </div>
            </div>
        </div>

<?php
include_once 'footer.php';
?>