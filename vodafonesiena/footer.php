    <div class="push"></div>
</div>
<div class="footer">
  <div class="container">
    <p class="credit">F@M Group Siena | P. Iva: 01335380521 | Via del Porrione 50, 53100 Siena | +39 0577 221710 | 
        <a href="mailto:info@vodafonesiena.it">info@vodafonesiena.it</a></p>
	  <p class="credit">Crafted by <a href="http://www.glumcommunication.it/" target="_BLANK">GLuM Communication</a></p>
  </div>
</div>
<script src="http://code.jquery.com/jquery.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
    $('.carousel').carousel({
        interval: 3000
    })
</script>

        <script type="text/javascript">
          $(document).ready(function () {
            $("a").tooltip({
              'selector': ''
            });
          });
        </script>
<script>
$(document).ready(function() {

	// validate signup form on keyup and submit
	var validator = $("#form").validate({
		rules: {
			name: "required",
			phone: {
                required: true,
                minlength: 9
            },
            message: "required",
			email: {
				required: true,
                minlength: 7,
                email: true
			}
		},
		messages: {
			name: "Inserisci nome e cognome",
			phone: {
				required: "Inserisci un numero di telefono",
				minlength: "Il numero deve essere composto da almeno 9 cifre"
			},
            message: "Inserisci un messaggio",
			email: {
                required: "Inserisci un indirizzo email",
                minlength: "Inserisci un indirizzo email valido",
                email: "Inserisci un indirizzo email valido"
            }
			
		},
	});


});
</script>

</body>
</html>
