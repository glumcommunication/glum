<?php
$pagetitle = "Agenzia";
$pagename = "agenzia";
include_once 'header.php';
include_once 'navigation.php';
?>
<div class="container">
    <div class="row-fluid lastrow">
        <div class="resizer span12 shadowed">
            <div class="container ribbon">
                <div class="ribbon_l"></div>
                <div class="ribbon_m"><h2>Agenzia</h2></div>
                <div class="ribbon_r"></div>
            </div>
            <p class="justify">
                La storia di <strong>F@M GROUP SIENA</strong>, e del suo titolare Ivan Lubrano,inizia nel Febbraio 2013, dopo una 
                proficua esperienza come agente/promoter presso varie agenzie Vodafone
            </p>
            <p class="justify">
                A febbraio la voglia di mettere a frutto tutta l'esperienza nel settore, e lo  stimolo di creare un'azienda propria 
                di successo nel settore,si arriva alla decisione di creare un'agenzia che possa essere una vera e propria struttura 
                di consulenza per tutti i suoi clienti, in un territorio molto ostico e competitivo come Siena.
            </p>
            <p class="justify">
                Oggi <strong>F@M GROUP SIENA</strong> sta pian piano occupando una posizione d'interesse vista la continua 
                disponibilit&agrave;,professionalit&agrave; e correttezza dei suoi agenti. 
            </p>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>