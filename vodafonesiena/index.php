<?php
$pagetitle = "Home";
$pagename = "home";
include_once 'cbd.php';
include_once 'header.php';
include_once 'navigation.php';
?>

<div class="container">

<?php
	$sql = "SELECT * FROM products ORDER BY Data DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
	$rows = mysql_num_rows($result);
    if ($rows!=0) {
        include_once 'carousel.php';
    }
    else {
        echo <<<_END
    <div class="row-fluid" style="margin-bottom: 15px;">
        <img src="images/picturehome.jpg" alt="home" class="span12">
    </div>
_END;
    }
?>

<?php
if ($_POST['doSend']=='Invia') {
$name = $_POST[name];
$address = $_POST[address];
$phone = $_POST[phone];
$email = $_POST[email];
$message = $_POST[message];
$headers = "";
$headers .= "From: $email\n";
$headers .= "Reply-To: $email\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=utf-8\n";
$to = "info@vodafonesiena.it";
$subject = "Nuova candidatura";
/*$message = $messaggio;*/
$messagebody = "Hai ricevuto una nuova candidatura, ecco i dettagli:<br />
Nome e Cognome: $name<br />
Indirizzo completo: $address<br />
E-mail: $email<br />
Telefono: $phone<br />
Messaggio: $message<br />";

mail ($to,$subject,$messagebody,$headers);
$sent = "1";
}

?>
    <div class="row-fluid lastrow">
        <div class="span6 shadowed">
            <div class="container ribbon">
                <div class="ribbon_l"></div>
                <div class="ribbon_m"><h2>Lavora con noi</h2></div>
                <div class="ribbon_r"></div>
            </div>
            <p>
                Se sei una persona a cui piace il mondo della tecnologia e sei interessato ad intraprendere 
                un percorso nel campo delle telecomunicazioni, potresti fare al caso nostro.<br>
                Compila il form sottostante con i tuoi dati e sarai contattato nel pi&ugrave; breve tempo possibile.
            </p>
            <form class="form-horizontal" action="index.php"  method="post" id="form">
              <fieldset>
                <legend>Contattaci</legend>
<?php
if ($sent=='1') {
echo <<<_END
<div class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>Messaggio inviato!</h4>
  Sarai ricontattato al pi&ugrave; presto.
</div>
_END;
}
?>
                <p class="alert alert-block">I campi contrassegnati dall'asterisco sono obbligatori.</p>
                <div class="control-group">
                    <label class="control-label" for="name">Nome e Cognome *</label>
                    <div class="controls">
                        <input type="text" name="name" id="name" placeholder="Es. Mario Rossi">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="address">Indirizzo completo</label>
                    <div class="controls">
                        <input type="text" name="address" id="address" placeholder="Es. Via Roma 10, 53100 Siena">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="phone">Telefono *</label>
                    <div class="controls">
                        <input type="text" name="phone" id="phone" placeholder="Es. 3331234567, 057712345...">
                    </div>
                </div>  
                <div class="control-group">
                    <label class="control-label" for="email">Email *</label>
                    <div class="controls">
                        <input type="email" name="email" id="email" placeholder="Es. mail@example.com">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="message">Messaggio *</label>
                    <div class="controls">
                        <textarea rows="3" name="message" id="message"></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                      <button type="submit" class="btn btn-danger" name="doSend" value="Invia" id="submitbtn">Invia</button>
                    </div>
                </div>
                    
                    <a href="#" rel="tooltip" data-toggle="tooltip" 
                    title="Nel inviare i miei dati dichiaro di aver letto l'informativa e sono 
                    consapevole che il trattamento degli stessi è necessario per ottenere 
                    il servizio proposto. A tal fine, nel dichiarare di essere maggiorenne, 
                    fornisco il mio consenso." 
                    data-placement="right" style="font-size:12px; cursor: help;">Informativa sulla Privacy ai sensi del D.Lgs 196/2003</a>
                </fieldset>
            </form>
                    </div>

        
        <div class="span6 shadowed">
            <div class="container ribbon">
                <div class="ribbon_l"></div>
                <div class="ribbon_m"><h2>Orari</h2></div>
                <div class="ribbon_r"></div>
            </div>
            <p>
                La nostra agenzia osserva i seguenti orari:
            </p>
            <p>
                Dal <strong>luned&igrave;</strong> al <strong>venerd&igrave;</strong> siamo aperti dalle 9:00 alle 13:00 e dalle 14:00 alle 18:00.
            </p>
            <p>
                Il <strong>sabato</strong> e la <strong>domenica</strong> siamo chiusti.
            </p>
            <p>
                Vieni a trovarci in Via del Porrione 50, ti aspettiamo!
            </p>
        </div>
        <div class="span6 shadowed">
            <div class="container ribbon">
                <div class="ribbon_l"></div>
                <div class="ribbon_m"><h2>Seguici su Facebook</h2></div>
                <div class="ribbon_r"></div>
            </div>
<div id="fb-root" class="span12"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class=".stretch">
<div class="fb-like-box" data-href="http://www.facebook.com/FamGroupSiena" data-width="300" data-height="350" data-show-faces="true" data-stream="false" data-show-border="false" data-header="false"></div>
</div>

        </div>        
    </div>
</div>

<?php
include_once 'footer.php';
?>