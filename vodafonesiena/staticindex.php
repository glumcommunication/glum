<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
            F@M Group Siena - Vodafone | Via del Porrione 50, 53100 Siena
        </title>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40343924-1']);
  _gaq.push(['_setDomainName', 'vodafonesiena.it']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
    </head>
    <body>
        <div id="wrapper" style="width:1024px;margin: 0 auto; overflow: hidden;"
             <p><img src="images/vodafonesiena_banner.jpg" alt="Vodafone Siena Banner"></p>
             <p style="text-align:center;">
                 <a href="https://www.facebook.com/pages/Fam-Group-Siena/516454581734289?ref=br_rs" title="Facebook">
                     <img src="images/facebook_logo.png" alt="F@M Group siena | Facebook Fan Page" style="width:32px;">
                 </a>
             </p>
        </div>
    </body>
</html>
