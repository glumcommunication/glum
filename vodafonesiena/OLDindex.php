<?php
$pagetitle = "Home";
$pagename = "home";
include_once 'cbd.php';
include_once 'header.php';
include_once 'navigation.php';
?>

<div class="container">
    <div id="myCarousel" class="carousel slide" data-interval=2000>
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>
        <!-- Carousel items -->
   
        <div class="carousel-inner">
            <div class="active item">
                <img src="../examples/1.JPG" alt="1" class="">
            </div>
            <div class="item">
                <img src="../examples/2.JPG" alt="2">
            </div>
            <div class="item">
                <img src="../examples/3.JPG" alt="3">
            </div>
            <div class="item">
                <img src="../examples/4.JPG" alt="4">
            </div>
        </div>

        <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div>
    <div class="row-fluid lastrow">
        <div class="span6 shadowed">
            <div class="container ribbon">
                <div class="ribbon_l"></div>
                <div class="ribbon_m"><h2>Secondo</h2></div>
                <div class="ribbon_r"></div>
            </div>
            <p>
                Il Bayern Monaco ha umiliato il Barcellona sotto gli occhi dei tifosi dell’Allianz Arena. 
                Un 4-0 difficile da prevedere alla vigilia perché ottenuto contro la squadra che negli 
                ultimi anni ha segnato il limite superiore da provare a raggiungere
            </p>

                <div class="btn btn-danger">Maggiori Dettagli</div>

        </div>
        <div class="span6 shadowed">
            <div class="container ribbon">
                <div class="ribbon_l"></div>
                <div class="ribbon_m"><h2>Terzo</h2></div>
                <div class="ribbon_r"></div>
            </div>
            <p>
                Il Bayern Monaco ha umiliato il Barcellona sotto gli occhi dei tifosi dell’Allianz Arena. 
                Un 4-0 difficile da prevedere alla vigilia perché ottenuto contro la squadra che negli 
                ultimi anni ha segnato il limite superiore da provare a raggiungere
            </p>

                <div class="btn btn-danger">Download</div>

        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>