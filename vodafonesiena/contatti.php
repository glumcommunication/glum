<?php
$pagetitle = "Contatti";
$pagename = "contatti";
include_once 'header.php';
include_once 'navigation.php';
?>
<div class="container">
    <div class="row-fluid lastrow">
        <div class="span12 shadowed">
            <div class="container ribbon">
                <div class="ribbon_l"></div>
                <div class="ribbon_m"><h2>Contatti</h2></div>
                <div class="ribbon_r"></div>
            </div>
            <div class="row-fluid">
            <div class="span4">
                <p>
                    <strong>F@M Group Siena</strong>
                </p>
                <p>
                    <img src="img/maps.png" alt="Indirizzo" style="vertical-align: bottom;">&emsp;Via del Porrione 50, 53100 Siena
                </p>
                <p>
                    <img src="img/phone.png" alt="Telefono" style="vertical-align: bottom;">&emsp;+39 0577 221710
                </p>
                <p>
                    <img src="img/mail.png" alt="Email" style="vertical-align: bottom;">&emsp;<a href="mailto:info@vodafonesiena.it" title="Contattaci">info@vodafonesiena.it</a>
                </p>
                <!--<p>
                    <strong>Partita Iva</strong>: 01335380521
                </p>-->
            </div>
            <div class="span4">
                
                <div style="float:left;">
                    <img src="img/man.png" alt="User" style="vertical-align: bottom;margin:10px 10px 10px 0;">
                </div>
                <p>
                <strong>Ivan Lubrano</strong><br>Titolare
                
                </p>
                <p>
                    <img src="img/phone.png" alt="Telefono" style="vertical-align: bottom;">&emsp;+39 347 6919128
                </p>
                <p>
                    <img src="img/mail.png" alt="Email" style="vertical-align: bottom;">&emsp;<a href="mailto:ivanlubrano@vodafonesiena.it" title="Contattaci">ivanlubrano@vodafonesiena.it</a>
                </p>
            </div>
           <div class="span4">
                
                <div style="float:left;">
                    <img src="img/man.png" alt="User" style="vertical-align: bottom;margin:10px 10px 10px 0;">
                </div>
                <p>
                <strong>Emanuele Bimbi</strong><br>Direttore Commerciale
                
                </p>
                <p>
                    <img src="img/phone.png" alt="Telefono" style="vertical-align: bottom;">&emsp;+39 346 3318978
                </p>
                <p>
                    <img src="img/mail.png" alt="Email" style="vertical-align: bottom;">&emsp;<a href="mailto:emanuelebimbi@vodafonesiena.it" title="Contattaci">emanuelebimbi@vodafonesiena.it</a>
                </p>
            </div>
            </div>
            <div id="map_canvas" style="width:100%; height:400px;">
            </div>

        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>