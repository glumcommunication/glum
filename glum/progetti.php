<?php
$pageName = "ten";
$pageTitle = "Progetti";
$pageDesc = "I nostri progetti";
include_once 'header.php';
?>

<nav id="navigationMap">
    <ul>
        <li class="return"><a class="ascensorLink ascensorLink0"></a>
        <li class="return"><a class="ascensorLink ascensorLink1"></a>
          
    </ul>
</nav>
<div id="backhome">
    <a href="/" title="Torna alla home">HOME</a>
</div>
<div id="sectionName">
    <p>PROGETTI</p>
</div>

<div id="ascensorTwelve" class="resizeMe">
    <div>
        <div class="content" id="progetto1">
            <div class="progettoLogo">
                <img src="img/progettograndiimprese.jpg" alt="Grandi Imprese - Logo">
            </div>
            <div class="progettoUrl">
                <a href="http://www.grandimprese.si" title="Grandi Imprese">www.grandimprese.si</a>
            </div>
            <div class="progettoMain">
                <p>
                    Un viaggio alla scoperta di storie, sudori, difficoltà e soddisfazioni attraverso quelle aziende operanti 
                    nel territorio senese che hanno fatto la scelta di non occuparsi semplicemente di fare profitto, ma di 
                    puntare all’eccellenza e alla qualità, le Grandi Imprese Senesi appunto. Un viaggio condotto da delle guide 
                    d’eccellenza: i titolari stessi che ci racconteranno la loro storia personale, l’ambiente in cui lavorano, 
                    ma anche quello in cui vivono e come trascorrono il loro tempo fuori dall’azienda. Segui le tappe del nostro 
                    viaggio:
                </p>
                <p>
                    <a href="http://www.grandimprese.si" title="Grandimprese.si">www.grandimprese.si</a><br>
                    <a href="https://www.facebook.com/GrandiImprese.si" title="Grandimprese.si su Facebook">https://www.facebook.com/GrandiImprese.si</a>
                </p>

            </div>
        </div>
    </div>
    <div>
        <div class="content" id="progetto2">
            <div class="progettoLogo">
                <img src="img/logo.png" style="width: 250px;" alt="SienaSiamoNoi - Logo">
            </div>
            <div class="progettoUrl">
                <a href="http://www.sienasiamonoi.it" title="SienaSiamoNoi">www.sienasiamonoi.it</a>
            </div>
              <div class="progettoMain">
            <p>Siena Siamo Noi &ndash; Volti di senesi per scelta e per amore&rdquo; &egrave; un progetto ideato da Samuele Mancini e GLuM Communication che vuole raccontare con i ritratti fotografici gli abitanti di Siena. Non solo senesi ma tutti coloro che &ldquo;per scelta e per amore&rdquo; oggi vivono nella nostra citt&agrave;. Pensiamo agli studenti ad esempio, che scelgono di venire a studiare a Siena perch&eacute; credono ancora nella qualit&agrave; dell&#39;offerta formativa dell&#39;ateneo senese. Pensiamo a tutti coloro che &ldquo;per amore&rdquo; di questa citt&agrave; decidono di restare a vivere qui nonostante tutto. L&#39;idea parte dalla consapevolezza che oggi pi&ugrave; che mai sia necessario stare dalla parte di chi crede ancora nella citt&agrave; e di chi, spesso silenziosamente e nell&#39;ombra, continua a mostrare sul suo volto la voglia di fare e di non cedere di fronte al difficile momento di crisi.</p>

            <p>&ldquo;Siena Siamo Noi&rdquo; vuole mostrare il volto di chi Siena la vive quotidianamente, di chi lotta per un suo rilancio, di chi la ama.</p>
            <p>
                <a href="http://www.sienasiamonoi.it" title="SienaSiamoNoi">www.sienasiamonoi.it</a><br>
                <a href="https://www.facebook.com/sienasiamonoi" title="sienasiamonoi su Facebook">https://www.facebook.com/sienasiamonoi</a>
            </p>
              </div>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>