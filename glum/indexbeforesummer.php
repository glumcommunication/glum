<?php
$pagename = "index";
$pagetitle = "Home";
include_once 'header.php';
?>
    <div class="container main">

        <div class="container-fluid">
          <div class="row-fluid">
 <?php
 include_once 'sidenav.php';
 ?>
            <div class="span10">
                <div class="row-fluid">
                    <div id="myCarousel" class="carousel slide">
                      <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="4"></li>
                      </ol>
                      <!-- Carousel items -->
                      <div class="carousel-inner">
                        <div class="active item">
                            <img src="../examples/6.png" alt="1">
                            <div class="carousel-caption">
                              <h4>First Thumbnail label</h4>
                              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                            </div>    
                        </div>
                        <div class="item">
                            <img src="../examples/2.JPG" alt="2" class="img-rounded">
                            <div class="carousel-caption">
                              <h4>Second Thumbnail label</h4>
                              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="../examples/3.JPG" alt="3" class="img-rounded">
                            <div class="carousel-caption">
                              <h4>Third Thumbnail label</h4>
                              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="../examples/4.JPG" alt="4" class="img-rounded">
                            <div class="carousel-caption">
                              <h4>Fourth Thumbnail label</h4>
                              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                            </div>
                        </div>
                      </div>
                      <!-- Carousel nav -->
                      <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                      <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span6 titleBox">
                        <h2>Uno</h2>
                        <div style="background-color: #ff8c00;margin: 15px;">
                            <div style="background-color:rgba(255,255,255,0.8);padding: 5px;">
                        <p>
                            Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                        </p>
                        <p>
                            Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                        </p>
                        <p>
                            Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                        </p>        
                            </div>
                        </div>
                    </div>
                    <div class="span6 titleBox">
                        <h2>Due</h2>
                        <p>
                            Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                        </p>
                    </div>
                    <div class="span4 titleBox">
                        <h2>tre</h2>
                        <p>
                            Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                        </p>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12 titleBox">
                        <h2>
                            Let's get social
                        </h2>                            
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span6 titleBox facebook">
                        <img src="img/facebookhome.png" alt="Facebook">
<div id="fb-root span12"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-like-box" data-href="http://www.facebook.com/glumcommunication" data-width="444" data-height="293" data-show-faces="true" data-stream="false" data-show-border="false" data-header="false"></div>

                        
                    </div>
                    <div class="span6 titleBox twitter">
                    <img src="img/twitterhome.png" alt="Twitter">
                    <a class="twitter-timeline span12" href="https://twitter.com/AgenziaGlum" data-tweet-limit="3" data-chrome="noborders noscrollbar nofooter transparent" data-widget-id="336750877585068032">Tweets di @AgenziaGlum</a>
                    <script>
                        !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
                    </script>

                    </div>
                    <!--<div class="span4 titleBox">
                        <img src="img/youtubehome.png" alt="YouTube">
<script src="js/yunero.min.js"></script>
<script type="text/javascript">
 //youtube URL (mandatory) -----------------------------
 var youTubeURL = "http://www.youtube.com/user/googlechrome"; 
 //optional parameters ---------------------------------
 //width and height of your widget
 var height = 130;
 var yuneroWidgetHeight = 400; 
 var yuneroWidgetWidth = 300; 
 var yuneroVideoHeight = 200; //works with plugin in download 2
</script>
<div id="yunero" style="border: 0;"></div>

                    </div>-->
                </div>
            </div>
          </div>
            
        </div>

<?php
include_once 'footer.php';
?>
    </div>