<?php
$pagename = "index";
$pagetitle = "Home";
include_once 'header.php';
?>
<div class="container main">
    <div class="container-fluid">
        <div class="row-fluid">
 <?php
 include_once 'sidenav.php';
 ?>

            <div class="span10">
                <div class="row-fluid">
                    <div id="columnsP">
                        <div class="pinP">
                            <img src="images/pfl/pfl_01.png" alt="Luppoli Case">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_02.png" alt="The New Oxford School">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_03.png" alt="Club Ippico Senese">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_04.png" alt="Art Immobiliare">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_05.png" alt="Case di Siena">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_06.png" alt="FTSA">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_07.png" alt="Gelateria Il Masgalano">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_08.png" alt="Girogustando">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_09.png" alt="Gruppo Arkell">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_10.png" alt="Meeting Sushi Wok">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_11.png" alt="Brillo Parlante">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_12.png" alt="La Taverna di San Pietro">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_13.png" alt="Studio Tecnico Marco Grandi">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_14.png" alt="Tosoni Auto">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_24.png" alt="Sofia Montalcino">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_16.png" alt="Fratelli Festa">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_17.png" alt="FòM Group Siena">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_18.png" alt="Ristorante Il Feudo">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_19.png" alt="La Diana - Birra Artigianale">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_20.png" alt="Art & Cake Topper">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_21.png" alt="Betty & Wilma">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_22.png" alt="Gelato Ti Amo">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_23.png" alt="Oliviera">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_15.png" alt="Siena Motori">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_25.png" alt="Bar San Domenico">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_26.png" alt="Bagoga">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_27.png" alt="Tipica Macelleria Rapaccini">
                        </div>
                        <div class="pinP">
                            <img src="images/pfl/pfl_28.png" alt="Delizie in Cucina">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/toucheffects.js"></script>
        <div class="push"></div>
    </div>
</div>

<?php
include_once 'footer.php';
?>