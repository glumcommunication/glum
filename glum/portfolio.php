<?php
$pageName = "one";
$pageTitle = "Pubblicità";
$pageDesc = "Cosa facciamo";
include_once 'header.php';
?>

<nav id="navigationMap">
    <ul class="links-to-floor">
        <li><a class="ascensorLink ascensorLink0"></a></li>
        <li><a class="ascensorLink ascensorLink1"></a></li>
        <li><a class="ascensorLink ascensorLink2"></a></li>
        <li><a class="ascensorLink ascensorLink3"></a></li>
        <li><a class="ascensorLink ascensorLink4"></a></li>
        <li><a class="ascensorLink ascensorLink5"></a></li>
        <li><a class="ascensorLink ascensorLink6"></a></li>
        <li><a class="ascensorLink ascensorLink7"></a></li>
        <li><a class="ascensorLink ascensorLink8"></a></li>
        <li><a class="ascensorLink ascensorLink9"></a></li>
        <li><a class="ascensorLink ascensorLink10"></a></li>      
         <li><a class="ascensorLink ascensorLink11"></a></li>
        <li><a class="ascensorLink ascensorLink12"></a></li>        
        <li class="return"><a class="ascensorLink ascensorLink13"></a></li>
        <li><a class="ascensorLink ascensorLink14"></a></li>
        <li><a class="ascensorLink ascensorLink15"></a></li>
        <li><a class="ascensorLink ascensorLink16"></a></li>
        <li class="return"><a class="ascensorLink ascensorLink17"></a></li>
        <li><a class="ascensorLink ascensorLink18"></a></li>
        <li><a class="ascensorLink ascensorLink19"></a></li>
        <li class="return"><a class="ascensorLink ascensorLink20"></a></li>
        <li><a class="ascensorLink ascensorLink21"></a></li>
        <li class="return"><a class="ascensorLink ascensorLink22"></a></li>
        <li class="return"><a class="ascensorLink ascensorLink23"></a></li>
        <li><a class="ascensorLink ascensorLink24"></a></li>
        <li class="return"><a class="ascensorLink ascensorLink25"></a></li>
        <li><a class="ascensorLink ascensorLink26"></a></li>
        <li class="return"><a class="ascensorLink ascensorLink27"></a></li>



    </ul>
</nav>
<div id="backhome">
    <a href="/" title="Torna alla home">HOME</a>
</div>
<div id="sectionName">
    <p>PORTFOLIO</p>
</div>
<div id="ascensorFour" class="resizeMe">
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio_web-01.jpg" alt="Web">
            </div>
            <h1 class="mediatype">WEB</h1>
            <h2 class="mediaurl"><a href="http://www.cartonfactory.it" target="_blank" title="Carton Factory">Visita il sito</a></h2>
        </div>
    </div>           
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio_web-02.jpg" alt="Web">
            </div>
            <h1 class="mediatype">WEB</h1>
            <h2 class="mediaurl"><a href="http://www.carroccioviaggi.com" target="_blank" title="Carroccio Viaggi">Visita il sito</a></h2>
        </div>
    </div>  
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio_web-03.jpg" alt="Web">
            </div>
            <h1 class="mediatype">WEB</h1>
            <h2 class="mediaurl"><a href="http://www.esteticaego.it" target="_blank" title="Estetica Ego">Visita il sito</a></h2>
        </div>
    </div>          
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio_web-04.jpg" alt="Web">
            </div>
            <h1 class="mediatype">WEB</h1>
            <h2 class="mediaurl"><a href="http://www.ristoranteosaka.com" target="_blank" title="Ristoranti Osaka e Minohana">Visita il sito</a></h2>
        </div>
    </div>
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio_web-05.jpg" alt="Web">
            </div>
            <h1 class="mediatype">WEB</h1>
            <h2 class="mediaurl"><a href="http://www.grandimprese.si" target="_blank" title="Grandimprese.SI">Visita il sito</a></h2>
        </div>
    </div>           
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio_web-06.jpg" alt="Web">
            </div>
            <h1 class="mediatype">WEB</h1>
            <h2 class="mediaurl"><a href="http://www.fuoriskema.net" target="_blank" title="Fuoriskema - Parrucchieri Estetica">Visita il sito</a></h2>
        </div>
    </div>  
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio_web-07.jpg" alt="Web">
            </div>
            <h1 class="mediatype">WEB</h1>
            <h2 class="mediaurl"><a href="http://www.casedisiena.it" target="_blank" title="Case di Siena">Visita il sito</a></h2>
        </div>
    </div>          
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio_web-08.jpg" alt="Web">
            </div>
            <h1 class="mediatype">WEB</h1>
            <h2 class="mediaurl"><a href="http://www.sofiamontalcino.it" target="_blank" title="Sofia Montalcino">Visita il sito</a></h2>
        </div>
    </div>
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio_web-09.jpg" alt="Web">
            </div>
            <h1 class="mediatype">WEB</h1>
            <h2 class="mediaurl"><a href="http://www.luppolicase.it" target="_blank" title="Luppoli Case">Visita il sito</a></h2>
        </div>
    </div>          
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio_web-10.jpg" alt="Web">
            </div>
            <h1 class="mediatype">WEB</h1>
            <h2 class="mediaurl"><a href="http://www.thenewoxfordschool.it" target="_blank" title="The New Oxford School">Visita il sito</a></h2>
        </div>
    </div>
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio_web-11.jpg" alt="Web">
            </div>
            <h1 class="mediatype">WEB</h1>
            <h2 class="mediaurl"><a href="http://www.gruppoarkell.it" target="_blank" title="Gruppo Arkell">Visita il sito</a></h2>
        </div>
    </div>
   <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio_web-12.jpg" alt="Web">
            </div>
            <h1 class="mediatype">WEB</h1>
            <h2 class="mediaurl"><a href="http://www.ilcaggio.it" target="_blank" title="Agriturismo il caggio">Visita il sito</a></h2>
        </div>
    </div>
     <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio_web-13.jpg" alt="Web">
            </div>
            <h1 class="mediatype">WEB</h1>
            <h2 class="mediaurl"><a href="http://www.venerenuevo.it" target="_blank" title="Centro Estetico VenereNuevo">Visita il sito</a></h2>
        </div>
    </div>
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio-volantini-01.jpg" alt="Volantino">
            </div>
            <h1 class="mediatype">VOLANTINI</h1>
        </div>
    </div>
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio-volantini-02.jpg" alt="Volantino">
            </div>
            <h1 class="mediatype">VOLANTINI</h1>
        </div>
    </div>
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio-volantini-03.jpg" alt="Volantino">
            </div>
            <h1 class="mediatype">VOLANTINI</h1>
        </div>
    </div>
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio-volantini-04.jpg" alt="Volantino">
            </div>
            <h1 class="mediatype">VOLANTINI</h1>
        </div>
    </div>
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio-pieghevoli-01.jpg" alt="Pieghevole">
            </div>
            <h1 class="mediatype">BROCHURE</h1>
        </div>
    </div>
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio-pieghevoli-02.jpg" alt="Pieghevole">
            </div>
            <h1 class="mediatype">BROCHURE</h1>
        </div>
    </div>
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio-pieghevoli-03.jpg" alt="Pieghevole">
            </div>
            <h1 class="mediatype">BROCHURE</h1>
        </div>
    </div>
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfoliologodesign-01.jpg" alt="Logo">
            </div>
            <h1 class="mediatype">LOGO</h1>
        </div>
    </div>
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfoliologodesign-02.jpg" alt="Logo">
            </div>
            <h1 class="mediatype">LOGO</h1>
        </div>
    </div>
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio-gadget-01.jpg" alt="Gadget">
            </div>
            <h1 class="mediatype">GADGET</h1>
        </div>
    </div>
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio-biglietti-01.jpg" alt="Biglietti da visita">
            </div>
            <h1 class="mediatype">BIGLIETTI DA VISITA</h1>
        </div>
    </div>
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio-biglietti-02.jpg" alt="Biglietti da visita">
            </div>
            <h1 class="mediatype">BIGLIETTI DA VISITA</h1>
        </div>
    </div>
    <div>           
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio-6x3-01.jpg" alt="Affissioni">
            </div>
            <h1 class="mediatype">AFFISSIONI</h1>
        </div>
    </div>
    <div>
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio-6x3-02.jpg" alt="Affissioni">
            </div>
            <h1 class="mediatype">AFFISSIONI</h1>
        </div>
    </div>
    <div>
        <div class="content">
            <div class="imgContainer">
                <img src="img/pfl/portfolio_packaging_01.jpg" alt="Packaging">
            </div>
            <h1 class="mediatype">PACKAGING</h1>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>
