<?php
$pageName = "eleven";
$pageTitle = "People";
$pageDesc = "Chi siamo. Le persone di GluM.";
include_once 'header.php';
?>

<nav id="navigationMap">
	<ul>
		<li class="return"><a class="ascensorLink ascensorLink0"></a>
		<li class="return"><a class="ascensorLink ascensorLink1"></a>
<!--		<li class="return"><a class="ascensorLink ascensorLink2"></a>
		<li class="return"><a class="ascensorLink ascensorLink3"></a>
		<li class="return"><a class="ascensorLink ascensorLink4"></a>
		<li class="return"><a class="ascensorLink ascensorLink5"></a>-->
	</ul>
</nav>
	<div id="backhome">
		<a href="/" title="Torna alla home">HOME</a>
	</div>
	<div id="sectionName">
		<p>CHI SIAMO</p>
	</div>
	<div id="ascensorEleven" class="resizeMe">

		<div>
			<ul class="grid cs-style-4">
				<li>
					<figure>
						<div>
							<div><img src="img/ppl/gennaro1.jpg" alt="Gennaro Di Domenico"></div>
						</div>
						<figcaption>
								<h3>Gennaro<br><span style="white-space:nowrap">Di Domenico</span></h3>
							<p class="role">CEO / Account Director</p>

						</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<div>
							<div><img src="img/ppl/oriana1.jpg" alt="Oriana Bottini"></div>
						</div>
						<figcaption>
								<h3>Oriana<br>Bottini</h3>
							<p class="role">Sales Account</p>

						</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<div>
							<div><img src="img/ppl/nicola1.jpg" alt="Nicola Panzieri"></div>
						</div>
						<figcaption>
								<h3>Nicola<br>Panzieri</h3>
							<p class="role">Sales Account</p>

						</figcaption>
					</figure>
				</li>
			</ul>
		</div>
		<div>
			<ul class="grid cs-style-4">
				<li>
					<figure>
						<div>
							<div><img src="img/ppl/alessandra1.jpg" alt="Alessandra Santomarco"></div>
						</div>
						<figcaption>
								<h3>Alessandra<br>Santomarco</h3>
							<p class="role">Social Media Strategist</p>

						</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<div>
							<div><img src="img/ppl/irene1.jpg" alt="Irene Vannucci"></div>
						</div>
						<figcaption>
								<h3>Irene<br>Vannucci</h3>
							<p class="role">Graphic Designer</p>

						</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<div>
							<div><img src="img/ppl/cesare1.jpg" alt="Colin 'Cezza' Donati"></div>
						</div>
						<figcaption>
								<h3>Cesare<br>Rinaldi</h3>
							<p class="role">Web Developer</p>

						</figcaption>
					</figure>
				</li>
			</ul>
		</div>
		<!--<div>
			<ul class="grid cs-style-4">
				<li>
					<figure>
						<div>
							<div><img src="img/ppl/david.jpg" alt="img06"></div>
						</div>
						<figcaption>

							<h3>David<br>Vagellini</h3>
							<p class="role">Sales Account</p>


						</figcaption>
					</figure>
				</li>
			</ul>
		</div>
		<div>
			<ul class="grid cs-style-4">
				<li>
					<figure>
						<div>
							<div><img src="img/ppl/gennaro.jpg" alt="img06"></div>
						</div>
						<figcaption>
								<h3>Gennaro<br>Di Domenico</h3>
							<p class="role">CEO - Account Director</p>

						</figcaption>
					</figure>
				</li>
			</ul>
		</div>
		<div>
			<ul class="grid cs-style-4">
				<li>
					<figure>
						<div>
							<div><img src="img/ppl/irene.jpg" alt="img06"></div>
						</div>
						<figcaption>
								<h3>Irene<br>Vannucci</h3>
							<p class="role">Graphic Designer</p>

						</figcaption>
					</figure>
				</li>
			</ul>
		</div>
		<div>
			<ul class="grid cs-style-4">
				<li>
					<figure>
						<div>
							<div><img src="img/ppl/nicola.jpg" alt="img06"></div>
						</div>
						<figcaption>
								<h3>Nicola<br>Panzieri</h3>
							<p class="role">Sales Account</p>

						</figcaption>
					</figure>
				</li>
			</ul>
		</div>-->

			
	</div>
<?php
include_once 'footer.php';
?>
