<?php
$pagename = "index";
$pagetitle = "Home";
include_once 'header.php';
?>
    <div class="container main">

      <div class="container-fluid">
          <div class="row-fluid">
 <?php
 include_once 'sidenav.php';
 ?>
            <div class="span10">
                <p>
                    Comunicare è un istinto primordiale di sopravvivenza. GLuM Communication aggiunge a questo istinto 
                    la professionalità di un gruppo di giovani provenienti dalla ‘giungla’ del mondo della comunicazione 
                    che mette a disposizione del cliente creatività, professionalità e mestiere.
                </p>
                <p>
                    GLuM Communication valorizza le aziende attraverso una comunicazione mirata e studiata, dando 
                    visibilità e forza comunicativa ad ogni tipologia di impresa, regalando un’immagine efficace, 
                    coerente e calibrata, attraverso l’uso della PUBBLICITÀ CARTACEA:
                </p>
                <h3>
                    volantini, locandine, biglietti da visita, brochures, cataloghi, gadget, cartellonistica, manifesti, totem, stendardi, camion vela;
                </h3>
            </div>
        </div>
    </div>

<?php
include_once 'footer.php';
?>
</div>