<?php
$pagename = "index";
$pagetitle = "Home";
include_once 'header.php';
?>
    <div class="container main">

      <div class="container-fluid">
          <div class="row-fluid">
 <?php
 include_once 'sidenav.php';
 ?>
            <div class="span10">
                <h1>L'Agenzia</h1>
                <p>
                    Comunicare è un istinto primordiale di sopravvivenza. GLuM Communication aggiunge a questo istinto 
                    la professionalità di un gruppo di giovani provenienti dalla ‘giungla’ del mondo della comunicazione 
                    che mette a disposizione del cliente creatività, professionalità e mestiere.
                </p>
                <p>
                    GLuM Communication valorizza le aziende attraverso una comunicazione mirata e studiata, dando 
                    visibilità e forza comunicativa ad ogni tipologia di impresa, regalando un’immagine efficace, 
                    coerente e calibrata, attraverso l’uso della <strong>PUBBLICITÀ CARTACEA</strong>.
                </p>
                <h4 style="text-align:center;padding: 5px 2px;color: #ffffff;background-color: #ff8c00;">
                    volantini, locandine, biglietti da visita, brochures, cataloghi, gadget, cartellonistica, manifesti, totem, stendardi, camion vela
                </h4>
                <p>
                    GluM Communication è intraprendenza, competitività, voglia di fare e di crescere. Studiamo le strategie migliori per ampliare 
                    le possibilità di sviluppo attraverso operazioni di web marketing progettate per coinvolgere sempre più persone nell’offerta di 
                    prodotti e servizi, pianificando strategie multicanale, attraverso la <strong>PUBBLICITÀ MULTIMEDIALE</strong>.
                </p>
                <h4 style="text-align:center;padding: 5px 2px;color: #ffffff;background-color: #0088cc;float:left;">
                    siti internet, portali, e-commerce, newsletter, app ios e android, analisi web, ottimizzazione e posizionamento siti web, banner pubblicitari online
                </h4>
                <p>
                    GLuM è fedeltà al cliente che non abbandona mai, seguendolo dall’inizio, ma anche durante la propria attività. Nel mercato di oggi un’attività 
                    commerciale si impone soprattutto affermando una forte identità, in grado di trasmettere chiaramente i propri valori. Ecco che GLuM si 
                    occupa di <strong>CREATIVITÀ</strong>.
                </p>
                <h4 style="text-align:center;padding: 5px 2px;color: #ffffff;background-color: #ff8c00;">progettazione loghi, immagine coordinata, ideazione del naming</h4>
                <p>
                    La visibilità è un fattore determinante per uscire vincenti nel mercato concorrenziale ed esibire la propria qualità e professionalità. Le 
                    <strong>PUBLIC RELATIONS</strong> sono funzionali sia agli obiettivi di immagine che alle strategie commerciali perché consentono 
                    un contatto diretto con clienti e partner professionali. 
                </p>
                <h4 style="text-align:center;padding: 5px 2px;color: #ffffff;background-color: #ff8c00;">ufficio stampa, organizzazione eventi</h4>
                <p>
                    GLuM è al passo con i tempi. Attraverso il <STRONG>SOCIAL MARKETING</STRONG> aumentiamo il potenziale di vendita di un’azienda. I social network 
                    negli ultimi anni hanno rivoluzionato le modalità di condivisione delle informazioni e ampliato notevolmente le opportunità di 
                    partecipazione. Noi utilizziamo questi spazi virtuali per promuovere prodotti o servizi, lanciare campagne pubblicitarie, fidelizzare i 
                    clienti, monitorare il livello di popolarità del brand.
                </p>
                <h4 style="text-align:center;padding: 5px 2px;color: #ffffff;background-color: #ff8c00;">Facebook, Twitter, Google+, Youtube, Pinterest, LinkedIn</h4>
            </div>
        </div>
    </div>

<?php
include_once 'footer.php';
?>
</div>