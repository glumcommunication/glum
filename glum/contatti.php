<?php
$pageName = "three";
$pageTitle = "Contatti";
$pageDesc = "I contatti";
include_once 'header.php';
?>

    <div id="backhome">
        <a href="/" title="Torna alla home">HOME</a>
    </div>
    <div id="sectionName">
        <p>CONTATTI</p>
    </div>

    <div id="ascensorThree" class="resizeMe">
        <div>
          
            <div class="content" id='contatti'>
                <h1>CONTATTI</h1>
                <div class="contactmap">
                    <div id="contactSlide" class="contents">                    
                        <div class="btn-show">
                            <i class="icon-info-sign icon-white" style="margin:6px;"></i> Info
                        </div>


                        <div class="pushup-form">
                            <div class="btn-close">
                                <i class="icon-remove icon-white" style="margin:6px;"></i> Chiudi
                            </div>
                            <div class="clear">
                            </div>
                            <div class="contacttext" id="scontents">
                                <p>GLuM Communication Srl</p>
                                <p>Strada Massetana Romana 64</p>
                                <p>53100 Siena</p>
                                <p>+39 0577 280213</p>
                                <p><a href="mailto:info@glumcommunication.it">info@glumcommunication.it</a></p>
                            </div>
                        </div>
                    </div>
                    <!--<div class='contactimage'>
                        <img src='img/mappaglum.jpg'>
                    </div>-->
                </div>
                <div class="contacthours">
                    <h2>ORARI</h2>
                    <p>
                        L'AGENZIA E' APERTA TUTTI I GIORNI DALLE 9 ALLE 13 E DALLE 14 ALLE 18.<br>
                        IL SABATO E LA DOMENICA SIAMO CHIUSI.
                    </p>
                    <div id='check'></div>
                </div>
                <div class="contactdir">
                    <h2>DA ROMA</h2>
                    <p>
                        Prendere l'autostrada A1 direzione Nord, uscire al casello Valdichiana. Proseguire in 
                        direzione Siena e imboccare il raccordo autostradale Siena - Bettolle. Al termine del 
                        raccordo, proseguire in direzione Firenze - Tangenziale Ovest di Siena e scegliere 
                        l'uscita Siena Sud. Alla rotonda prendere la seconda uscita, siamo sopra il negozio Euronics.
                    </p>
                </div>
                <div class="contactdir">
                    <h2>DA FIRENZE</h2>
                    <p>
                        Dirigersi verso il raccordo autostradale Firenze - Siena (uscita A1 Firenze Impruneta), 
                        proseguire in direzione Siena/Colle val D'Elsa/Poggibonsi/Grosseto-Fi-Si. Alla tangenziale 
                        Ovest di Siena uscire Siena Sud. Allo Stop voltare a sinistra, alla rotonda 
                        prendere la prima uscita, siamo sopra il negozio Euronics.
                    </p>
                </div>
            </div>
        </div>
    </div>
    
<?php
include_once 'footer.php';
?>