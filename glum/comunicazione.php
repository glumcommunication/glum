<?php
$pageName = "eight";
$pageTitle = "Comunicazione";
$pageDesc = "Filosofia aziendale";
include_once 'header.php';
?>

    <div id="backhome">
        <a href="/" title="Torna alla home">HOME</a>
    </div>
    <div id="sectionName">
        <p>AGENZIA</p>
    </div>

    <div id="ascensorEight" class="resizeMe">
        <div>
          
            <div class="content">
                <h1>FILOSOFIA<br>AZIENDALE</h1>
                <p>
                    Al centro della filosofia di GLuM c’è il cliente. Ce ne occupiamo con trasparenza, 
                    prendendocene cura,  dedicando all’ascolto e alla conoscenza dell’azienda tutto il 
                    tempo necessario. Creiamo per ogni cliente, di volta in volta, un team ad hoc. In 
                    base ai bisogni e alle aspettative costruiamo una campagna mirata con lo scopo, non 
                    di vendere semplicemente un prodotto, ma di rafforzare l’identità e i profitti.
                </p>
            </div>
        </div>
    </div>
<?php
include_once 'footer.php';
?>