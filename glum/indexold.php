<?php
include_once 'header.php';
?>
        <div id="main">
            <div class="left" id="homeleft">
                <div style="width:80%;margin:0 10%;"><img src="images/homebg.jpg" alt="Puffin"></div>

            </div>
            <div class="right">
                <p class="ptophome">
                    <strong>GLuM</strong> è <strong>intraprendenza</strong>,<br>
                    voglia di <strong>fare</strong> e di <strong>crescere</strong>.<br>
                    Il <strong>sogno</strong> di un gruppo di ragazzi<br>
                    che provengono tutti dal mondo della<br>
                    comunicazione e mettono a disposizione<br>
                    dei clienti tutta la loro <strong>professionalità</strong>.
                </p>
                <p class="ptophome">
                    GLuM si propone di svolgere un ruolo di supporto<br>
                    per le aziende. Il gruppo di lavoro di GLuM si<br>
                    distingue per un versatile reparto creativo che<br>
                    opera sui più vari canali della comunicazione.
                </p>
                <p class="pbottomhome">
                    <strong>I principali mezzi su cui lavora GLuM sono</strong>:
                </p>
                <p class="pbottomhome">
                    <strong>Pubblicità Cartacea</strong>:<br>
                    volantini, locandine, biglietti da visita, brochures, cataloghi,
                    gadget, cartellonistica, manifesti, totem, stendardi, camion vela;
                </p>
                <p class="pbottomhome">
                    <strong>Pubblicità Multimediale</strong>:<br>
                    siti internet, portali, e-commerce, newsletter, app ios e android,
                    analisi web, ottimizzazione e posizionamento siti web, banner
                    pubblicitari online.
                </p>
                <p class="pbottomhome">
                    <strong>Social Marketing</strong>:<br>
                    Facebook, Twitter, Google+, Youtube.
                </p>
                <p class="pbottomhome">
                    <strong>Creatività</strong>:<br>
                    progettazione loghi, immagine coordinata.
                </p>
                <p class="pbottomhome">
                    <strong>Public Relations</strong>:<br>
                    ufficio stampa, organizzazione eventi.
                </p>
            </div>
        </div>
            <div style="margin: 20px auto; text-align:center;float:left;width:100%;">
            <a onclick="var that=this;_gaq.push(['_trackEvent','Download','PDF',this.href]);setTimeout(function(){location.href=that.href;},400);return false;" href="public/GLuM_presentazione_web_2013.pdf" class="btn btn-warning btn-large" style="text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);text-decoration: none;">
                    Scarica la nostra brochure
                <i class="icon-white  icon-download-alt"></i>
            </a>
<!--            <a href="public/GLuM_presentazione_web_2013.pdf" class="btn btn-warning btn-large" style="text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);text-decoration: none;">
                Scarica la nostra brochure 
                <i class="icon-white  icon-download-alt"></i>
            </a>-->
            </div>
<?php
include_once 'footer.php';
?>

