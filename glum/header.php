<?php
session_name("fancyform");
session_start();


$_SESSION['n1'] = rand(1,20);
$_SESSION['n2'] = rand(1,20);
$_SESSION['expect'] = $_SESSION['n1']+$_SESSION['n2'];


$str='';
if($_SESSION['errStr'])
{
	$str='<div class="error">'.$_SESSION['errStr'].'</div>';
	unset($_SESSION['errStr']);
}

$success='';
if($_SESSION['sent'])
{
	$success='<h1>Thank you!</h1>';
	
	$css='<style type="text/css">#contact-form{display:none;}</style>';
	
	unset($_SESSION['sent']);
}
?>
<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<meta name = "viewport" content = "user-scalable=0,width=device-width,initial-scale=1,maximum-scale=1" />
	<meta name="description" content="<?php echo $pagedesc ?>">
	<meta name= "keywords" content= "Pubblicità, Advertising, Web Marketing, Social Media, Comunicazione, consulenza, volantini, flyer, manifesti, locandine, Siena, marketing, image, brand, internet, sito, website, ufficio stampa, press office, organizzazione eventi, promozione, biglietti da visita, indicizzazione SEO, banner, design, grafica, pagina aziendale Facebook twitter youtube, video, spot radio televisione, google+,  pieghevole, brochure, gadget, fund raising" />
	<link rel="shortcut icon" href="img/favicon.png">
	<link href='http://fonts.googleapis.com/css?family=Merriweather+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
	<link href="css/extra.min.css" rel="stylesheet" />
	<link href="css/style.min.css" rel="stylesheet" />

<?php
if ($pageName === "three") echo "<link href=\"css/pup-slider.min.css\" rel=\"stylesheet\" media=\"screen\">"; 
?>

	<link href="css/validationEngine.jquery.min.css" rel="stylesheet" />
	<title><?php echo $pageTitle; ?> | GLuM Communication - Agenzia di comunicazione a Siena - Pubblicità, siti web, social media.</title>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40022944-1']);
  _gaq.push(['_trackPageview']);

  (function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script src="js/respond.min.js"></script>
<script src="js/selectivizr-min.js"></script>
</head>
<body <?php if($pagename=='index'){echo ' class="homegrey"';}?>>
	<div id="main">
