<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
                <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/stylebis.css" rel="stylesheet" media="screen">
        <title>
            GLuM Communication
        </title>
    </head>
<body>
<div class="wrap">
    <div class="header">
        <div class="container">
            <div class="row-fluid">            
                <div class="span3 brand">
                    <img src="images/glumlogopadding.png" alt="GLuM Logo">
                </div>
                <div class="span9 orange">
                    <div class="row-fluid">

                        <div class="navbar">
                            <ul class="nav">
                                <li<? if ($pagename == "home") echo " class=\"active\"";?>><a href="index.php">HOME</a></li>

                                <li<? if ($pagename == "agenzia") echo " class=\"active\"";?>><a href="agenzia.php">AGENZIA</a></li>

                                <li<? if ($pagename == "contatti") echo " class=\"active\"";?>><a href="contatti.php">CONTATTI</a></li>

                                <li<? if ($pagename == "offerte") echo " class=\"active\"";?>><a href="offerte.php">OFFERTE</a></li>
                            </ul>

                                <!--</div>-->
                        </div>
                    </div>
                </div>
                    
            </div>

        </div>
    </div>
</div>
    <style>
        #map{
	z-index: 2000;
	top: 20px;
	position: fixed;
	right: 20px;
	display: none;
}

.works_NavigationButton{
	width: 10px;
	height: 10px;
	background-color: #f2f2f2;
	margin: 0px;
	-webkit-border-radius: 2px;
	-moz-border-radius: 2px;
	border-radius: 2px;
	float: left;
	margin-right: 1px;
	margin-top: 1px;
	z-index:2600;

}

.works_NavigationButton:first-child{
	background: #000;
}

    </style>
    		<div id="map">
			<dl id="works_Navigation">
				<dt class="works_NavigationButton" ><a href="#" title="Link for Content" id="works_NavigationButton0"></a></dt>
				<dt class="works_NavigationButton" ><a href="#" title="Link for Content" id="works_NavigationButton1"></a></dt>
				<dt style="margin-top: 1px" class="works_NavigationButton clear" ><a href="#" title="Link for Content" id="works_NavigationButton2"></a></dt>
				<dt style="margin-top: 1px" class="works_NavigationButton clear" ><a href="#" title="Link for Content" id="works_NavigationButton3"></a></dt>
				<dt class="works_NavigationButton" ><a href="#" title="Link for Content" id="works_NavigationButton4"></a></dt>
				<dt style="margin-top: 1px" class="works_NavigationButton clear" ><a href="#" title="Link for Content" id="works_NavigationButton5"></a></dt>
				<dt class="works_NavigationButton" ><a href="#" title="Link for Content" id="works_NavigationButton6"></a></dt>
				<dt style="margin-top: 1px" class="works_NavigationButton clear" ><a href="#" title="Link for Content" id="works_NavigationButton7"></a></dt>
				<dt class="works_NavigationButton" ><a href="#" title="Link for Content" id="works_NavigationButton8"></a></dt>
				<dt style="margin-top: 1px" class="works_NavigationButton clear" ><a href="#" title="Link for Content" id="works_NavigationButton9"></a></dt>
				<dt style="margin-top: 1px" class="works_NavigationButton clear" ><a href="#" title="Link for Content" id="works_NavigationButton10"></a></dt>
				<dt class="works_NavigationButton" ><a href="#" title="Link for Content" id="works_NavigationButton11"></a></dt>
				<dt class="works_NavigationButton" ><a href="#" title="Link for Content" id="works_NavigationButton12"></a></dt>
				<dt class="works_NavigationButton" ><a href="#" title="Link for Content" id="works_NavigationButton13"></a></dt>
				<dt style="margin-top: 1px" class="works_NavigationButton clear" ><a href="#" title="Link for Content" id="works_NavigationButton14"></a></dt>
				<dt style="margin-top: 1px" class="works_NavigationButton clear" ><a href="#" title="Link for Content" id="works_NavigationButton15"></a></dt>
				<dt class="works_NavigationButton" ><a href="#" title="Link for Content" id="works_NavigationButton16"></a></dt>
				<dt class="works_NavigationButton" ><a href="#" title="Link for Content" id="works_NavigationButton17"></a></dt>
				<dt style="margin-top: 1px" class="works_NavigationButton clear" ><a href="#" title="Link for Content" id="works_NavigationButton18"></a></dt>
				<dt style="margin-top: 1px" class="works_NavigationButton clear" ><a href="#" title="Link for Content" id="works_NavigationButton19"></a></dt>
				<dt class="works_NavigationButton" ><a href="#" title="Link for Content" id="works_NavigationButton20"></a></dt>
				<dt style="margin-top: 1px" class="works_NavigationButton clear" ><a href="#" title="Link for Content" id="works_NavigationButton21"></a></dt>
				<dt class="works_NavigationButton" ><a href="#" title="Link for Content" id="works_NavigationButton22"></a></dt>
				<dt style="margin-top: 1px" class="works_NavigationButton clear" ><a href="#" title="Link for Content" id="works_NavigationButton23"></a></dt>
				<dt class="works_NavigationButton" ><a href="#" title="Link for Content" id="works_NavigationButton24"></a></dt>
				<dt class="works_NavigationButton" ><a href="#" title="Link for Content" id="works_NavigationButton25"></a></dt>
				<dt style="margin-top: 1px" class="works_NavigationButton clear" ><a href="#" title="Link for Content" id="works_NavigationButton26"></a></dt>
				<dt class="works_NavigationButton" ><a href="#" title="Link for Content" id="works_NavigationButton27"></a></dt>
				<dt style="margin-top: 1px" class="works_NavigationButton clear" ><a href="#" title="Link for Content" id="works_NavigationButton28"></a></dt>
			</dl>
		</div>
</body>
</html>