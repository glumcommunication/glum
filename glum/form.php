
<?php


?>
<?=$css?>
<div id="form-container" style="display: none;">	<!-- The form container -->

<form id="contact-form" name="contact-form" method="post" action="submit.php">	<!-- The form, sent to submit.php -->


    <div class="formRow">
        <div class="formLabel">
            <label for="name">Nome</label>
        </div>
        <div class="formField">
            <input type="text" class="validate[required,custom[onlyLetter]]" name="name" id="name" value="<?=$_SESSION['post']['name']?>" />
        </div><div id="errOffset"></div>
    </div>
    <div class="formRow">
        <div class="formLabel">
            <label for="email">Email</label>
        </div>
        <div class="formField">
            <input type="text" class="validate[required,custom[email]]" name="email" id="email" value="<?=$_SESSION['post']['email']?>" />
        </div>
    </div>
    <div class="formRow">
        <div class="formLabel">
            <label for="message">Messaggio</label>
        </div>
        <div class="formField">
            <textarea name="message" id="message" class="validate[required]" rows="5"><?=$_SESSION['post']['message']?></textarea>
        </div>
    </div>
    <div class="formRow">
        <div class="formLabel">
            <label><?=$_SESSION['n1']?> + <?=$_SESSION['n2']?> =</label>
        </div>
        <div class="formField">
            <input type="text" class="validate[required,custom[onlyNumber]]" name="captcha" id="captcha" />
        </div>
    </div>
    <div class="formRow">
        <div class="formLabel">
            <label>&nbsp;</label>
        </div>
        <div class="formField">
            <input type="submit" name="button" id="button" value="Invio" class="button button-flat button-rounded" />
            <input type="reset" name="button2" id="button2" value="Azzera" class="button button-flat button-rounded" />
        </div>
<?=$str?>

<!-- $str contains the error string if the form is used with JS disabled -->

<img id="loading" src="img/loading.gif" width="16" height="16" alt="loading" />
<!-- the rotating gif animation, hidden by default -->        
    </div>

    


</form>

<?=$success?>
<!-- The $success variable contains the message that is shown if JS is disabled and the form is submitted successfully -->

</div>
