$(document).ready(function () {


	$("#button").click(function () {
        if ($(".formError").size() === 0){
            $(".formError").hide();
        }

	});

	var use_ajax=true;
	$.validationEngine.settings={};

	$("#contact-form").validationEngine({
		inlineValidation: false,
		promptPosition: "centerRight",
		success :  function(){use_ajax=true;},
		failure : function(){use_ajax=false;}
    });

	$("#contact-form").submit(function(e){

			if(!$('#button2').val().length)
			{
				//$.validationEngine.buildPrompt(".formError","* This field is required","error")
				return false;
			}
			
			if(use_ajax)
			{
				$('#loading').css('visibility','visible');
				$.post('submit.php',$(this).serialize()+'&ajax=1',
				
					function(data){
						if(parseInt(data)==-1)
							$.validationEngine.buildPrompt("#captcha","* Wrong verification number!","error");
							
						else
						{
							$("#contact-form").hide('slow').after('<h1 class="thanks">Thank you!</h1>');
						}
						
						$('#loading').css('visibility','hidden');
					}
				
				);
			}
			e.preventDefault();
	});

});

/* Logo resizing */
   
$(document).ready(function(){
   var slwidth = $(".sociallogo").width();
   $(".sociallogo").css({"height" : slwidth});
   $(".sociallogo>a>img").css({"height" : slwidth+1, "width" : slwidth+1});
   var nineWidth = $("#ninein > .hb_content").width();
   var nineHeight = $("#ninein > .hb_content").height();
   var socialHeight = $(".socialbox").height();
   socialTop = (nineHeight-socialHeight)/2;
   $(".socialbox").css({"top" : socialTop});
   return null;
});
$(window).resize(function() {
   var slwidthR = $(".sociallogo").width();
   $(".sociallogo").css({"height" : slwidthR});
   $(".sociallogo>a>img").css({"height" : slwidthR+1, "width" : slwidthR+1});
   var nineWidth = $("#ninein > .hb_content").width();
   var nineHeight = $("#ninein > .hb_content").height();
   var socialHeight = $(".socialbox").height();
   socialTop = (nineHeight-socialHeight)/2;
   $(".socialbox").css({"top" : socialTop});
   return null;
});

/* Effects for the home page form */
function showSendForm() {
    $("div.homebox").css("display","none");
    $("#two").css("display","block");
    $("#form-container").css("display","block");
    $("#send").removeClass("hl");
    $("#twoin").css({"display":"block", "border-radius":"0", "background-image":"none", "overflow-y":"auto"});
    $("#twoin > .hb_title").css("display","none");
    $("#backhome").css("display","block");
    $("#sectionName").css("display","block");
    $("#two").css("padding","0");
    $("#two").animate({width: "100%", height: "100%", top: "0", left: "0"}, 300, "linear" );
    $( "#twoin > .hb_content").unbind( "click" )
    return false;
}
function hideSendForm() {
    $("#backhome").css("display","none");
    $("#sectionName").css("display","none");
    $("#send").addClass("hl");
    $("#twoin > .hb_title").css("display","block");
    $("#twoin").css({"border-radius":"7px", "background-image":"url('img/send2.jpg')","overflow-y":"hidden"});
    $("#two").css({"padding":"6px 3px 3px 3px"});
    $("#two").animate({width: "25%", height: "33.333333%", left: "25%"}, 300, "linear" );
    $("#form-container").css("display","none");
    $("div.homebox").css("display","block");
    return  false;
}

$("#twoin > .hb_content").click(function() {
    $("div.homebox").css("display","none");
    $("#two").css("display","block");
    $("#form-container").css("display","block");
    $("#send").removeClass("hl");
    $("#twoin").css({"display":"block", "border-radius":"0", "background-image":"none", "overflow-y":"auto"});
    $("#twoin > .hb_title").css("display","none");
    $("#backhome").css("display","block");
    $("#sectionName").css("display","block");
    $("#two").css("padding","0");
    $("#two").animate({width: "100%", height: "100%", top: "0", left: "0"}, 300, "linear" );
    $( "#twoin > .hb_content").unbind( "click" )
    return false;
});
$("#twoin > #backhome").click(function(){
    $("#backhome").css("display","none");
    $("#sectionName").css("display","none");
    $("#send").addClass("hl");
    $("#twoin > .hb_title").css("display","block");
    $("#twoin").css({"border-radius":"7px", "background-image":"url('img/send2.jpg')","overflow-y":"hidden"});
    $("#two").css({"padding":"6px 3px 3px 3px"});
    $("#two").animate({width: "25%", height: "33.333333%", left: "25%"}, 300, "linear" );
    $("#form-container").css("display","none");
    $("div.homebox").css("display","block");
    $("#twoin > .hb_content").bind("click",showSendForm);
    return  false;
});

/* Click support on People page */

$(document).ready(function(){
   if( Modernizr.touch ) {
       $("figure").click(function(){
        $(this).toggleClass("cs-hover");
       });
   }
});

/* Ascensor resizer */

$(window).resize(function() {
   var winWidth = $(window).width();
   $(".resizeMe").css({"width" : winWidth});
});

/* Map Info slider */

$(document).ready(function() {
    if ($("#contatti").size()){
            $('#contactSlide').pupslider({ stick: 'right', speed: 500, opacity: 0.9 });
    }
});

/* Map resizing */


    $(document).ready(function(){
        var mapWidth = $(".contactmap").width();
    var mapHeight =  Math.ceil(mapWidth / 1.975308642);
    $(".contactmap").css({"height" : mapHeight});
});
$(window).resize(function() {
        var mapWidth = $(".contactmap").width();
        var mapHeight =  Math.ceil(mapWidth / 1.975308642);
        $(".contactmap").css({"height" : mapHeight});
        //$('#log').append('<div>Handler for .resize() called.</div>');

});

/* Font size che passione */

$(document).ready(function(){
    var vpSize = $("body").width();
    var fontSize = 16;
    if (vpSize <= 1200 && vpSize >= 320) {
        fontSize = (((Math.round(vpSize/100))*100)+400)/100;
    
    
    }
    else if (vpSize < 320) {
        fontSize = 6;
    }
    $("body").css({"font-size" : fontSize});
});
    $(window).resize(function() {
    var vpSize = $("body").width();
    if (vpSize <= 1200 && vpSize >= 320) {
    var fontSize = (((Math.round(vpSize/100))*100)+400)/100;
    
    $("body").css({"font-size" : fontSize});  
    }
});
