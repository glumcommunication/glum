$('#ascensorOne').ascensor({
        ascensorName: 'ascensor',
        childType: 'div',
        ascensorFloorName: ['Pubblicità Multimediale','Creatività','Social Marketing','Public Relations','Pubblicità cartacea','Consulenza aziendale'],
        time: 1000,
        windowsOn: 0,
        direction: "chocolate",
        ascensorMap: [[1,0],[2,0],[3,0],[4,0],[5,0],[6,0]],
        easing: 'easeInOutQuad',
        keyNavigation: true,
        queued: true,
        queuedDirection: "y",
        overflow:"hidden"
});
$('#ascensorThree').ascensor({
        ascensorName: 'ascensor',
        childType: 'div',
        ascensorFloorName: ['Contatti'],
        time: 1000,
        windowsOn: 0,
        direction: "chocolate",
        ascensorMap: [[1,0]],
        easing: 'easeInOutQuad',
        keyNavigation: true,
        queued: false,
        queuedDirection: "y",
        overflow:"hidden"
});
$('#ascensorFour').ascensor({
        ascensorName: 'ascensor',
        childType: 'div',
        ascensorFloorName: ['Carton Factory','Carroccio Viaggi','Estetica Ego','Minohana - Osaka','Grandimprese.si','FuoriSkema','Case di Siena','Sofia Montalcino','Luppoli Case','The New Oxford School','Gruppo Arkell','Agriturismo il Caggio','Centro Estetico VenereNuevo','Volantini1','Volantini3','Volantini3','Volantini4','Pieghevoli1','Pieghevoli2','Pieghevoli3','Logo1','Logo2','gadget','biglietti1','biglietti2','affissioni','affissioni2','packaging'],
        time: 1000,
        windowsOn: 0,
        direction: "chocolate",
        ascensorMap: [[1,10],[1,9],[1,8],[1,7],[1,6],[1,5],[1,4],[1,3],[1,2],[1,1],[1,0],[2,10],[2,9],[2,8],[2,7],[3,10],[3,9],[3,8],[4,10],[4,9],[5,10],[6,10],[6,9],[7,10],[7,9],[8,10]],
        easing: 'easeInOutQuad',
        keyNavigation: true,
        queued: true,
        queuedDirection: "y",
        overflow:"hidden",
        touchSwipeIntegration: true
});
$('#ascensorEight').ascensor({
        ascensorName: 'ascensor',
        childType: 'div',
        ascensorFloorName: ['Comunicazione'],
        time: 1000,
        windowsOn: 0,
        direction: "chocolate",
        ascensorMap: [[1,0]],
        easing: 'easeInOutQuad',
        keyNavigation: true,
        queued: false,
        queuedDirection: "y",
        overflow:"hidden"
});
$('#ascensorTen').ascensor({
        ascensorName: 'ascensor',
        childType: 'div',
        ascensorFloorName: ['Comunicazione'],
        time: 1000,
        windowsOn: 0,
        direction: "chocolate",
        ascensorMap: [[1,0]],
        easing: 'easeInOutQuad',
        keyNavigation: true,
        queued: false,
        queuedDirection: "y",
        overflow:"hidden"
});
$('#ascensorEleven').ascensor({
        ascensorName: 'ascensor',
        childType: 'div',
        ascensorFloorName: ['One','Two'],
        time: 1000,
        windowsOn: 0,
        direction: "chocolate",
        ascensorMap: [[1,0],[2,0]],
        easing: 'easeInOutQuad',
        keyNavigation: true,
        queued: false,
        queuedDirection: "y",
        overflow:"hidden"
});
$('#ascensorEleven').ascensor({
        ascensorName: 'ascensor',
        childType: 'div',
        ascensorFloorName: ['One','Two'],
        time: 1000,
        windowsOn: 0,
        direction: "chocolate",
        ascensorMap: [[1,0],[2,0]],
        easing: 'easeInOutQuad',
        keyNavigation: true,
        queued: false,
        queuedDirection: "y",
        overflow:"hidden"
});
