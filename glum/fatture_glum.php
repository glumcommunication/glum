<?php
session_start();
require_once("../../includes/constants.php");
require_once("../../includes/connect.php");
require_once("../../includes/functions.php");
require_once("../../includes/tcpdf/tcpdf.php");
confirm_login();


// create new PDF document
$pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('WitICT System');
$pdf->SetTitle('Fattura');
$pdf->SetSubject('Stampa fattura');
$pdf->SetKeywords('contabilita, fatture, stampa, clienti');

// set default header data
//$pdf->SetHeaderData('', 160, 'Stampa', 'Stampa eseguita da: '.$_SESSION['nome']);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(10, 20, 10);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
//$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('helvetica', '', 10, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// prima pagina
$id=intval(trim($_GET['id']));

$html = '
<style type="text/css">
	*{font-family: Arial, Helvetica, sans-serif; font-size: 1em}
	h1{font-size: 0.8em}
	.title{text-align:center;font-size:1em}
	p{line-height: 0.2em;font-size:0.6em}
	.br_left{border-left:1px solid #F59331;font-size:0.7em}
	.br_left_bottom{border-left:1px solid #F59331;border-bottom:1px solid #F59331}
	.br_bottom{border-bottom:1px solid #F59331}
	.br_left_top_bottom{border-left:1px solid #F59331;border-top:1px solid #F59331;border-bottom:1px solid #F59331}
	.br_top_bottom{border-top:1px solid #F59331;border-bottom:1px solid #F59331}
	.sedi{font-size:0.4em}
	.spett{font-size:1.2em}
	.iva{font-size:1em}
	.clieiva{font-size:0.8em}
	table{border:1px solid #F59331}
</style>
	<table width="100%" cellpadding="3" cellspacing="0" border="0">
		<tr>
			<td rowspan="3" width="30%"><img src="../../img/logo_print_idpromoter.jpg" /></td>';
				//seleziona nome azienda e sito
				$read=read_data('des_ragsoc,des_www','anagrafiche','id_tipo=1','','1');
				$anas=mysql_fetch_array($read);
			$html .= '
			<td colspan="2" width="40%"><h1>'.$anas['des_ragsoc'].'</h1></td>
			<td width="30%"><h1 style="color: #F59331;text-align:right">'.$anas['des_www'].'</h1></td>
		</tr>
		<tr>
			<td class="sedi">Sede legale:</td>
			<td class="sedi">Sedi operative:</td>
		</tr>
		<tr>';
			//seleziona indirizzi sede legale e filiali
			$read=read_data('des_indirizzo,des_nc,num_cap,des_comune,des_provincia,num_tel','anagrafiche','id_tipo=1','','');
			$rows=mysql_num_rows($read);
			while($anas=mysql_fetch_array($read)){

					$html .= '<td class="br_left">
									'.$anas['des_indirizzo'].', '.$anas['des_nc'].'<br />
									'.$anas['num_cap'].' '.$anas['des_comune'].' ('.$anas['des_provincia'].')<br />
									Tel.: '.$anas['num_tel'].'
								  </td>';
			}
		$html .= '
		</tr>
		<tr>
			<td colspan="4" style="font-size:0.5em">&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align:center" colspan="2">
				<p class="iva">&nbsp;</p>';
				//seleziona sito, e-mail, codice fiscale e altro registro
				$read=read_data('des_www,des_email,des_iva,des_cf,des_other','anagrafiche','id_tipo=1','','1');
				$anas=mysql_fetch_array($read);
				$html .= '
				<p style="font-weight:bold" class="iva">'.$anas['des_www'].'</p>
				<p class="iva">'.$anas['des_email'].'</p>';
				if(!empty($anas['des_cf'])){
					if($anas['des_cf']==$anas['des_iva']){
						$html .= '<p class="iva">C.F / P.I. '.$anas['des_iva'].'</p>';
					}else{
						$html .= '<p class="iva">P.IVA '.$anas['des_iva'].'</p>';
						$html .= '<p class="iva">C.F '.$anas['des_cf'].'</p>';
					}
				}else{
					$html .= '<p class="iva">C.F / P.I. '.$anas['des_iva'].'</p>';
				}
				if(!empty($anas['des_other'])){
					$html .= '<p class="iva">'.$anas['des_other'].'</p>';
				}
			$html .= '
			</td>
			<td colspan="2" class="spett" height="150px" style="border-left:1px solid #F59331">
				<br />Spett.le<br />';
				//seleziona fattura
				$read=read_data('id_anagrafica','idp_contratti','id='.$id,'','');
				$anas=mysql_fetch_array($read);
				//seleziona dati anagrafica cliente
				$read=read_data('des_ragsoc,des_indirizzo,des_indirizzo1,des_nc,num_cap,des_comune,des_provincia,des_cf,des_iva','anagrafiche','id='.$anas['id_anagrafica'],'','');
				$cliente=mysql_fetch_array($read);
				$html .= 
					'<strong>'.$cliente['des_ragsoc'].'</strong><br /><br />
					'.$cliente['des_indirizzo'].', '.$cliente['des_nc'].'<br />';
					if(!empty($cliente['des_indirizzo1'])){ $html .= $cliente['des_indirizzo1'].'<br />';}
					$html .= $cliente['num_cap'].' '.$cliente['des_comune'].' ('.$cliente['des_provincia'].')';
					$html .= '<br /><br /><br />';
					
					if(!empty($cliente['des_cf'])){
						if($cliente['des_cf']==$cliente['des_iva']){
							$html .= '<p class="clieiva">P.IVA '.$cliente['des_iva'].'</p>';
						}else{
							$html .= '<p class="clieiva">P.IVA '.$cliente['des_iva'].' ';
							$html .= 'C.F. '.$cliente['des_cf'].'</p>';
						}
					}else{
						$html .= '<p class="clieiva">C.F. / P.I. '.$cliente['des_iva'].'</p>';
					}
				$html .= '
			</td>
		</tr>
		<tr>
			<td colspan="4" style="border-bottom: 1px solid #F59331;font-size:0.5em">&nbsp;</td>
		</tr>
	</table>
';
//fine intestazione

// inizio corpo fattura
$html .= '
	<table width="100%" cellpadding="3" cellspacing="0" border="0">
		<tr>';
		$read=read_data('num_fat,dat_data','idp_contratti','id='.$id,'','');
		$anas=mysql_fetch_array($read);
		$html .= '
			<td class="br_bottom"><h1>Fattura n. <br />'.$anas['num_fat'].'/'.date("Y",$anas['dat_data']).'</h1></td>
			<td class="br_left_bottom"><h1>Del <br />'.date("d/m/Y",$anas['dat_data']).'</h1></td>
			<td class="br_left_bottom" colspan="2"><h1>Riferimento contratto <br />'.$id.'</h1></td>
		</tr>
		<tr><td colspan="4" style="font-size:0.5em">&nbsp;</td></tr>
		<tr>
			<td colspan="4" class="title br_top_bottom"><h1>Descrizione</h1></td>
		</tr>';
		//items
		$html .= '
		<tr>
			<td colspan="4" class="br_top_bottom">';
		$item=read_data('*','idp_contratti_voci','id_contratto='.$id,'','');
		while($items=mysql_fetch_array($item)){
			$html.=$items['des_des'].'<br />';
		}
		$html.= '</td>
		</tr>';
		
		$read=read_data('num_impo,num_iva','idp_contratti','id='.$id,'','');
		$array=mysql_fetch_array($read);
			$html .= '<tr>
				<td colspan="2">&nbsp;</td>
				<td class="br_left_bottom">imponibile</td>
				<td class="br_left_bottom">&euro; '.$array['num_impo'].'</td>
			</tr>';
			$base=read_data('*','basefiscale','flg_attivo=1','','');
			$bf=mysql_fetch_array($base);
			$html .= '<tr>
				<td colspan="2">&nbsp;</td>
				<td class="br_left_bottom">Spese bancarie</td>
				<td class="br_left_bottom">&euro; '.$bf['num_bollo'].'</td>
			</tr>';
			$iva=$array['num_iva']-$array['num_impo'];
			$html .= '<tr>
				<td colspan="2">&nbsp;</td>
				<td class="br_left_bottom">IVA '.$bf['num_iva'].'%</td>
				<td class="br_left_bottom">&euro; '.number_format($iva,2,',','.').'</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
				<td class="br_left_bottom">Totale:</td>
				<td class="br_left_bottom">&euro; '.$array['num_iva'].'</td>
			</tr>
			<tr><td colspan="4">&nbsp;</td></tr>
			
			<tr>
				<td colspan="4" class="title br_top_bottom"><h1>Pagamenti</h1></td>
			</tr>
			<tr>
				<td class="title br_top_bottom"><h1>Scadenza</h1></td>
				<td class="title br_left_bottom"><h1>Valore</h1></td>
				<td class="title br_left_bottom" colspan="2"><h1>Metodo di pagamento</h1></td>
			</tr>';
			$read=read_data('*','idp_contratti_scadenze','id_contratto='.$id,'dat_data ASC','');
			while($scad=mysql_fetch_array($read)){
			$html.='<tr>
				<td class="br_top_bottom" style="text-align:center">'.date("d/m/Y",$scad['dat_data']).'</td>
				<td class="br_left_bottom" style="text-align:center">&euro; '.number_format($scad['num_val'],2,',','.').'</td>';
			$sel=read_data('num_fat,des_paga','idp_entrate','dat_data='.$scad['dat_data'].' AND num_impo='.$scad['num_val'],'','');
			$ty=mysql_fetch_array($sel);
			$html.='
				<td class="br_left_bottom" colspan="2" style="text-align:center">'.$ty['des_paga'].'</td>
			</tr>';
			}
			$html.='
			<tr><td colspan="4" style="font-size:0.5em">&nbsp;</td></tr>
			
			';
			if(!empty($array['flg_esente'])){
				$html .= '
					<tr>
						<td colspan="4">
							'.$bf['des_esente'].'
						</td>
					</tr>
				';
			  }
			  if(!empty($bf['des_note'])){
				$html .= '
					<tr>
						<td colspan="4">
							<p>&nbsp;</p>
							<p style="text-align:center">'.$bf['des_note'].'</p>
						</td>
					</tr>
				';
			  }
			  if(!empty($array['des_note'])){
				$html .= '
					<tr>
						<td colspan="4" class="br_top_bottom"><h1>Note:</h1>
							'.$array['des_note'].'
						</td>
					</tr>
				';
			  }
			$html .= '
	</table>';







// seconda pagina

$outhtml = <<<EOD
$html
EOD;
// Print text using writeHTMLCell()
$pdf->writeHTMLCell($w=0, $h=0, $x='', $y='', $outhtml, $border=0, $ln=1, $fill=0, $reseth=true, $align='', $autopadding=true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('fattura_'.date('d_m_Y_H:i:s').'.pdf', 'I');
 if(isset($conn)) { mysql_close($conn); }
//============================================================+
// END OF FILE
//============================================================+
 ?>
