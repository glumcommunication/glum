<?php
$pagename = "index";
$pagetitle = "Home";
include_once 'header.php';
?>
<div class="container main">
    <div class="container-fluid">
        <div class="row-fluid">
 <?php
 include_once 'sidenav.php';
 ?>

            <div class="span10">
                <div class="row-fluid">
                   <div id="columns">
                        <div class="pin"><div id="alessandra"></div>

                            <h1>
                                Alessandra Santomarco
                            </h1>
                            <p>
                                Ciao ciao ciao ciao ciao ciao.Ciao ciao ciao ciao ciao ciaoCiao ciao ciao ciao ciao ciaoCiao ciao ciao ciao ciao ciaoCiao ciao ciao ciao ciao ciao
                            </p>
                        </div>
                      
                        <div class="pin"><div id="cesare"></div>

                            <h1>Cesare Rinaldi</h1>
                            <p>
                                Ciao ciao ciao ciao ciao ciao.Ciao </p><p>ciao ciao ciao ciao ciaoCiao ciao ciao ciao ciao</p><p> ciaoCiao ciao ciao ciao ciao ciaoCiao ciao ciao ciao ciao ciao
                            </p>
                        </div>
                        <div class="pin"><div id="gennaro"></div>

                            <h1>Gennaro Di Domenico</h1>
                            <p>
                                Ciao ciao ciao ciao ciao ciao.Ciao ciao ciao ciao ciao ciaoCiao ciao ciao ciao ciao ciaoCiao ciao ciao ciao ciao ciaoCiao ciao ciao ciao ciao ciao
                            </p>
                        </div>
                        <div class="pin"><div id="michele"></div>

                            <h1>Michele Sestini</h1>
                            <p>
                                MICHELECiao ciao ciao ciao ciao ciao.Ciao </p><p>ciao ciao ciao ciao ciaoCiao ciao ciao ciao ciao</p><p> ciaoCiao ciao ciao ciao ciao ciaoCiao ciao ciao ciao ciao ciao
                            </p>
                        </div>
                        <div class="pin"><div id="nicola"></div>

                            <h1>Nicola Panzieri</h1>
                            <p>
                                NICOLACiao ciao ciao ciao ciao ciao.Ciao ciao ciao ciao ciao ciaoCiao ciao ciao ciao ciao ciaoCiao ciao ciao ciao ciao ciaoCiao ciao ciao ciao ciao ciao
                            </p>
                        </div>
                        <div class="pin"><div id="david"></div>

                            <h1>David Vagellini</h1>
                            <p>
                                Ciao ciao ciao ciao ciao ciao.Ciao </p><p>ciao ciao ciao ciao ciaoCiao ciao ciao ciao ciao</p><p> ciaoCiao ciao ciao ciao ciao ciaoCiao ciao ciao ciao ciao ciao
                            </p>
                        </div>

                    </div>
                </div>
			<ul class="grid cs-style-4">
                
				<li class="span4">
					<figure>
                        <div><div><img src="images/ppl/david.jpg" alt="img06"></div></div>
						<figcaption>
                            <!--<div style="-webkit-transform: rotate(-90deg);-webkit-transform-origin: 75% 100%; float:left;">-->
							<h3>David Vagellini</h3>
                            <p>Email: david@glumcommunication.it</p>
							<!--<a href="http://dribbble.com/shots/1116775-Safari">Take a look</a>-->
						</figcaption>
					</figure>
				</li>
				<li class="span4">
					<figure>
						<div><div><img src="images/ppl/cesare.jpg" alt="img06"></div></div>
						<figcaption>
							<h3>Cesare<br>Rinaldi</h3>
							<span>Jacob Cummings</span>
							<a href="http://dribbble.com/shots/1116775-Safari">Take a look</a>
						</figcaption>
					</figure>
				</li>                
				<li class="span4">
					<figure>
                        <div><div><img src="images/ppl/nicola.jpg" alt="img06"></div></div>
						<figcaption>
							<h3>Nicola<br>Panzieri</h3>
							<span>Jacob Cummings</span>
							<a href="http://dribbble.com/shots/1116775-Safari">Take a look</a>
						</figcaption>
					</figure>
				</li>
				<li class="span4">
					<figure>
						<div><div><img src="images/ppl/gennaro.jpg" alt="img06"></div></div>
						<figcaption>
							<h3>Gennaro<br>Di Domenico</h3>
							<span>Jacob Cummings</span>
							<a href="http://dribbble.com/shots/1116775-Safari">Take a look</a>
						</figcaption>
					</figure>
				</li>
				<li class="span4">
					<figure>
						<div><div><img src="images/ppl/alessandra.jpg" alt="img06"></div></div>
						<figcaption>
							<h3>Alessandra<br>Santomarco</h3>
							<span>Jacob Cummings</span>
							<a href="http://dribbble.com/shots/1116775-Safari">Take a look</a>
						</figcaption>
					</figure>
				</li>
				<li class="span4">
					<figure>
						<div><div><img src="images/ppl/irene.jpg" alt="img06"></div></div>
						<figcaption>
							<h3>Irene<br>Vannucci</h3>
							<span>Jacob Cummings</span>
							<a href="http://dribbble.com/shots/1116775-Safari">Take a look</a>
						</figcaption>
					</figure>
				</li>
			</ul>
            </div>
        </div>
        <script src="js/toucheffects.js"></script>
        <div class="push"></div>
    </div>
</div>

<?php
include_once 'footer.php';
?>