<?php
$pagename = "index";
$pagetitle = "Home";
include_once 'header.php';
?>
  
  <script src="js/pup-slider.js"></script>
<script type="text/javascript">
        $(document).ready(function() {
            $('#left-demo').pupslider({ stick: 'right', speed: 500, opacity: 0.9 });
        });
</script>

    <div class="container main">

      <div class="container-fluid">
          <div class="row-fluid">
 <?php
 include_once 'sidenav.php';
 ?>
              
            <div class="span10">
                <div class="row-fluid">
                    <h1>Contatti</h1>
                    <div class="contactmap">
                    
            
                        <div id="left-demo" class="contents">                    
                            <div class="btn-show">
                                <i class="icon-info-sign icon-white" style="margin:6px;"></i> Info
                            </div>


                            <div class="pushup-form">
                                <div class="btn-close">
                                    <i class="icon-remove icon-white" style="margin:6px;"></i> Chiudi
                                </div>
                                <div class="clear">
                                </div>

                                    <div class="contacttext" id="scontents">
                                        <p>GLuM Communication Srl</p>
                                        <p>Strada Massetana Romana 64</p>
                                        <p>53100 Siena</p>
                                        <p>+39 0577 280213</p>
                                        <p><a href="mailto:info@glumcommunication.it">info@glumcommunication.it</a></p>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12 titleBox">
                        <h2>Come raggiungerci</h2>
                        <div class="container-fluid">
                        <div class="span6">
                            <h4>
                                Da Roma
                            </h4>
                            <p>
                                Prendere l'autostrada A1 direzione Nord, uscire al casello Valdichiana. Proseguire in direzione Siena e imboccare il raccordo autostradale Siena - Bettole.
                                Al termine del raccordo, proseguire in direzione Firenze - Tangenziale Ovest di Siena e scegliere l'uscita Siena Sud.
                                Alla rotonda prendere la seconda uscita, siamo sopra il negozio Euronics.
                            </p>
                        </div>
                        <div class="span6">
                            <h4>
                                Da Firenze
                            </h4>
                            <p>
                                Dirigersi verso il raccordo autostradale Firenze - Siena (uscita A1 a Firenze Impruneta), proseguire in direzione Siena/Colle val D'Elsa/Poggibonsi/Grossero-Fi-Si.
                                Dalla tangenziale Ovest di Siena scegliere l'uscita Siena Sud. Allo Stop voltare a sinistra, alla rotonda prendere la prima uscita, siamo sopra il negozio Euronics.
                            </p>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12 titleBox">
                        <h2>Orari</h2>
                        <p>
                            L'agenzia è aperta tuttii giorni dalle 9 alle 13 e dalle 14 alle 18.<br>
                            Il sabato e la domenica siamo chiusi.
                        </p>
                    </div>
               </div>
                </div>
            </div>
        </div>
    </div>

<?php
include_once 'footer.php';
?>
</div>