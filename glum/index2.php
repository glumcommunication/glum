<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href='http://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Average+Sans' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Gentium+Book+Basic' rel='stylesheet' type='text/css'>
                <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/stylebis.css" rel="stylesheet" media="screen">

        <title>
            GLuM Communication
        </title>
    </head>
<body>
<div class="wrap">
    <div class="container main">

        <div class="container-fluid">
          <div class="row-fluid">
            <div class="span2">
                
              <img src="images/glumlogosmall.png" alt="GLuM Logo">
              <hr>
                                                      
              <div class="navbar">
                                                    <a class="btn btn-block btn-navbar" data-toggle="collapse" data-target=".nav-collapse" style="margin-bottom: 5px;float:left;">
                                                        Menu <i class="icon-white icon-align-justify"></i>
                                                    </a>   
              </div>
              <div class="nav-collapse">
              <ul class ="nav nav-list sidemenu">
                  
                  <li><a href="#" title="Agenzia"><img src="images/comment.png" alt="">Agenzia</a></li>
                  <li><a href="#" title="Agenzia"><img src="images/file.png" alt="">Portfolio</a></li>
                  <li><a href="#" title="Agenzia"><img src="images/mail.png" alt="">Contatti</a></li>
                  <li><a href="#" title="Agenzia"><img src="images/users.png" alt="">Team</a></li>
                  <li><a href="#" title="Agenzia"><img src="images/earth.png" alt="">Partner</a></li>
                  <li><a href="#" title="Agenzia"><img src="images/locked.png" alt="">Area riservata</a></li>
              </ul>
                  <hr>
              <ul class ="nav nav-list sidemenu">
                  <li class="nav-header">Social</li>
              </ul>
              </div>

                                            
              <!--<h3>Social</h3>-->
              <hr>
            </div>
            <div class="span10">
                <div class="row-fluid">
                    <div id="myCarousel" class="carousel slide">
                      <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="4"></li>
                      </ol>
                      <!-- Carousel items -->
                      <div class="carousel-inner">
                        <div class="active item">
                            <img src="../examples/1.JPG" alt="1" class="img-rounded">
                            <div class="carousel-caption">
                              <h4>First Thumbnail label</h4>
                              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                            </div>    
                        </div>
                        <div class="item">
                            <img src="../examples/2.JPG" alt="2" class="img-rounded">
                            <div class="carousel-caption">
                              <h4>Second Thumbnail label</h4>
                              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="../examples/3.JPG" alt="3" class="img-rounded">
                            <div class="carousel-caption">
                              <h4>Third Thumbnail label</h4>
                              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="../examples/4.JPG" alt="4" class="img-rounded">
                            <div class="carousel-caption">
                              <h4>Fourth Thumbnail label</h4>
                              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                            </div>
                        </div>
                      </div>
                      <!-- Carousel nav -->
                      <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                      <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4 titleBox">
                        <h2>Uno</h2>
                        <p>
                            Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                        </p>
                    </div>
                    <div class="span4 titleBox">
                        <h2>Due</h2>
                        <p>
                            Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                        </p>
                    </div>
                    <div class="span4 titleBox">
                        <h2>tre</h2>
                        <p>
                            Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                        </p>
                    </div>
                </div>
            </div>
          </div>
            <div class="push"></div>
        </div>
        <div class="footer">
            <p>Ciao</p>
        </div>
    </div>
</div>
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script>
            $('.carousel').carousel({
                interval: 3000
            })
        </script>
</body>
</html>