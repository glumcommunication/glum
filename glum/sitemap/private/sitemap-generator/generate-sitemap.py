#!/usr/bin/python

from ymcore import logger
from ymcore.logger import log
from luppoli.sitemap_generator import SiteMapGenerator


''' Log to stdout, 'cause register.it catch all the stdout
activity and return it back through emails '''
logger.init_log("stdout")

log("Activating sitemap generator script")

oImporter = SiteMapGenerator()

oImporter.mysqlHost = "hostingmysql262.register.it"
oImporter.mysqlDatabase = "luppolicase_it_main"
oImporter.mysqlUser = "ML12800_luppoli"
oImporter.mysqlPassword = "luppolicase88"

if oImporter.start():
	log("Done")
else:
	log("Finishing with errors. See previous line for more detailed error messages")

logger.close_log()

