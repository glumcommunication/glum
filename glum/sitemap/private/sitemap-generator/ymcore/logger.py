''' Logger module.
Helper methods to facilitate the logging action

Always call init_log() to prepare the logging framework
and close_log() to allow the framework to dispose all
opened objects
'''

import sys
from datetime import datetime

_eLogType = None
_oLogFile = None
_bAutoFlush = False

''' logtype can be:
		"file"   - the log is stored into a file, specified by logfilename
		"stdout" - the log is redirected to stdout
		"stderr" - the log is redirected to stderr
	
Return True if the logger is successfully initialized, False otherwise
'''
def init_log(logtype, logfilename=None, autoflush=True):
	global _eLogType
	global _oLogFile
	global _bAutoFlush

	_bAutoFlush = autoflush

	_eLogType = logtype
	if _oLogFile != None:
		_oLogFile.close()
		_oLogFile = None

	if logtype == "stdout":
		_oLogFile = sys.stdout
	elif logtype == "stderr":
		_oLogFile = sys.stderr
	elif logtype == "file":
		_oLogFile = open(logfilename, "w")
	else:
		_eLogType = None
		_oLogFile = None
		return False

	return True


''' Print some data in the log file 
	
The log is in the format:
[local_datetime] data
'''
def log(data):
	global _oLogFile
	global _bAutoFlush
	
	if _oLogFile == None:
		return
	
	print >> _oLogFile, "[" + str(datetime.today()) + "]", data

	if _bAutoFlush:
		_oLogFile.flush()


''' Dispose any opened object (ie. if a log file has been
	opened, it is flushed and closed now) 
'''
def close_log():
	global _eLogType
	global _oLogFile

	if _oLogFile == None:
		return
	
	if _eLogType == "file":
		_oLogFile.flush()
		_oLogFile.close()

