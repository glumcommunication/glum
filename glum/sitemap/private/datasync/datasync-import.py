#!/usr/bin/python

from lib.ymcore import logger
from lib.ymcore.logger import log
from lib.luppoli.importer import Importer


''' Log to stdout, 'cause register.it catch all the stdout
activity and return it back through emails '''
logger.init_log("stdout")

log("Activating datasync IMPORTER script")

oImporter = Importer()

oImporter.mysqlHost = "hostingmysql262.register.it"
oImporter.mysqlDatabase = "luppolicase_it_main"
oImporter.mysqlUser = "ML12800_luppoli"
oImporter.mysqlPassword = "luppolicase88"

oImporter.imageStoragePath = "../../public/www/images/photos"
oImporter.imageBaseURL = "http://gestionale.luppolicase.it/luppolinews/Agenzie/file"
 
if oImporter.start():
	log("Done")
else:
	log("Finishing with errors. See previous line for more detailed error messages")

logger.close_log()

