import sys
import os
import json
import math
import urllib
import zipfile
import MySQLdb
from datetime import datetime
from lib.ymcore import logger
from lib.ymcore.logger import log

''' SiteMapGenerator module.

The SiteMapGenerator class is used to create a sitemap that conforms
to the current standards

This module use the ymcore.logger facility. No initialization
or closing action are performed on the logger. Is up to the
caller to prepare the logger.
'''

class SiteMapGenerator(object):

	def __init__(self):
		self._sTemplateFilename = os.path.basename(sys.argv[0]).replace(".py", "")
		self._sMysqlHost = ""
		self._sMysqlDatabase = ""
		self._sMysqlUser = ""
		self._sMysqlPassword = ""
		self._oConnection = None
		self._oSitemapFile = None
	
	@property
	def mysqlHost(self):
		return self._sMysqlHost
	@mysqlHost.setter
	def mysqlHost(self, value):
		self._sMysqlHost = value

	@property
	def mysqlDatabase(self):
		return self._sMysqlDatabase
	@mysqlDatabase.setter
	def mysqlDatabase(self, value):
		self._sMysqlDatabase = value
	
	@property
	def mysqlUser(self):
		return self._sMysqlUser
	@mysqlUser.setter
	def mysqlUser(self, value):
		self._sMysqlUser = value

	@property
	def mysqlPassword(self):
		return self._sMysqlPassword
	@mysqlPassword.setter
	def mysqlPassword(self, value):
		self._sMysqlPassword = value

	@property
	def sitemapFilename(self):
		return "sitemap.xml"
	
	def _initMysqlConnection(self):
		if self._oConnection != None:
			return
		self._oConnection = MySQLdb.connect(
				host=self._sMysqlHost,
				db=self._sMysqlDatabase,
				user=self._sMysqlUser,
				passwd=self._sMysqlPassword,
				charset="utf8")

	def _closeMysqlConnection(self):
		if self._oConnection == None:
			return
		self._oConnection.commit()
		self._oConnection.close()

	def _getCursor(self):
		if self._oConnection == None:
			return None
		return self._oConnection.cursor()
		
	def _openSitemapFile(self):
		if self._oSitemapFile != None:
			return
		self._oSitemapFile = open(self.sitemapFilename, "w")
	
	def _closeSitemapFile(self):
		if self._oSitemapFile == None:
			return			
		self._oSitemapFile.flush()
		self._oSitemapFile.close()



	def _printHeader(self):
		print >> self._oSitemapFile, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
		print >> self._oSitemapFile, "<urlset"
		print >> self._oSitemapFile, "      xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\""
		print >> self._oSitemapFile, "      xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
		print >> self._oSitemapFile, "      xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9"
		print >> self._oSitemapFile, "                          http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">"

	def _printFooter(self):
		print >> self._oSitemapFile, "</urlset>"

	def _printPage(self, url, lastmod, changeFreq):
		print >> self._oSitemapFile, "<url>"
		print >> self._oSitemapFile, "   <loc>%s</loc>" % url
		print >> self._oSitemapFile, "   <lastmod>%s</lastmod>" % lastmod
		print >> self._oSitemapFile, "   <changefreq>%s</changefreq>" % changeFreq
		print >> self._oSitemapFile, "</url>"



	''' Create the sitemap
	'''
	def _createSitemap(self):
		log("Start generating the sitemap")
		sCurrentDate = str(datetime.today().strftime("%Y-%m-%d"))

		''' pyodbc works great also with Mysql, but 
		the host (Register.it) doesn't support that library '''

		log("	Opening Mysql Server connection")
		self._initMysqlConnection()
		oCursor = self._getCursor()
		oCursor.execute("SELECT C_Riferimento FROM V_Case")
		
		log("	Opening sitemap file at: %s" % self.sitemapFilename)
		self._openSitemapFile()

		log("	Adding the header")
		self._printHeader()
		
		log("	Adding the static content")
		self._printPage(("http://www.luppolicase.it/"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/vendita.php?mode=abitazione"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/vendita.php?mode=ufficio"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/vendita.php?mode=fondo"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/vendita.php?mode=commerciale"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/vendita.php?mode=garage"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/vendita.php?mode=garage"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/affitto.php?mode=abitazione"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/affitto.php?mode=ufficio"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/affitto.php?mode=fondo"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/affitto.php?mode=garage"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/search.php?centro_storico"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/search.php?periferia_siena"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/search.php?comuni_di_siena"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/search.php?province_toscana"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/box_preferiti.php"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/special_privilege.php"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/stima_gratuita.php"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/tutti_immobili.php?mode=centro"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/tutti_immobili.php?mode=periferia"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/tutti_immobili.php?mode=provincia"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/tutti_immobili.php?mode=regione"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/rete_luppoli.php"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/blog.php"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/partners.php"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/a.php"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/siena.php"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/chianti.php"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/crete.php"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/montagnola.php"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/condomini.php"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/chi_siamo.php"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/dovesiamo.php"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/servizi.php"), sCurrentDate, "monthly")
		self._printPage(("http://www.luppolicase.it/contatti.php"), sCurrentDate, "monthly")

		log("	Adding the dynamic content")
		iCount = 0
		for oRow in oCursor.fetchall():
			iCount += 1
			if not oRow[0]: # both None and empty string are evaluated as False
				continue
			self._printPage(("http://www.luppolicase.it/details.php?rif=%s" % oRow[0]), sCurrentDate, "daily")

		iNPages = int(math.ceil(iCount/5.0))
		for i in range(1, iNPages):
			self._printPage(("http://www.luppolicase.it/search_results.php?page=%s" % i), sCurrentDate, "daily")
			
		
		log("	Adding the footer")
		self._printFooter()

		log("	Closing the sitemap file")
		self._closeSitemapFile()
		
		log("	Closing Mysql Server connection")
		oCursor.close()
		self._closeMysqlConnection()
		
		log("Sitemap generated")



	'''
	Return True if all the jobs are correctly executed and finished,
	False otherwise
	'''
	def start(self):
		try:
			self._createSitemap()
		except:
			log("A blocking error occurred: %s" % str(sys.exc_info()))
			return False

		return True

