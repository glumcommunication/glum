import sys
import os
import os.path
from subprocess import call

''' Video generator module.

'''

class SlideshowMaker(object):
	
	def __init__(self):
		self._title_fadein_length = 0
		self._closing_fadeout_length = 0
		self._switch_image_fade_length = 0

		self._title_image_length = 0
		self._closing_image_length = 0
		self._content_image_length = 0
		
		self._video_size = "320x240"
		self._video_filename = "video.mp4"
		self._framerate = 25

		self._images_base_path = None
		self._temp_base_path = None
		
		self._title_image_filename = None
		self._closing_image_filename = None

		self._audio_filename = None

		self._images = []

		self._codec_name = "qtrle"

		self._ffmpeg_path = "ffmpeg"
		self._error_message = ""

		self.m_bTitleVideoCreated = False
		self.m_bContentImageVideoCreate = False
		self.m_bClosingVideoCreate = False


	@property
	def title_fadein_length(self):
		return self._title_fadein_length
	@title_fadein_length.setter
	def title_fadein_length(self, value):
		self._title_fadein_length = value
	
	@property
	def title_image_length(self):
		return self._title_image_length
	@title_image_length.setter
	def title_image_length(self, value):
		self._title_image_length = value

	
	@property
	def closing_fadeout_length(self):
		return self._closing_fadeout_length
	@closing_fadeout_length.setter
	def closing_fadeout_length(self, value):
		self._closing_fadeout_length = value

	@property
	def closing_image_length(self):
		return self._closing_image_length
	@closing_image_length.setter
	def closing_image_length(self, value):
		self._closing_image_length = value


	@property
	def switch_image_fade_length(self):
		return self._switch_image_fade_length
	@switch_image_fade_length.setter
	def switch_image_fade_length(self, value):
		self._switch_image_fade_length = value
	
	@property
	def content_image_length(self):
		return self._content_image_length
	@content_image_length.setter
	def content_image_length(self, value):
		self._content_image_length = value


	@property
	def video_size(self):
		return self._video_size
	@video_size.setter
	def video_size(self, value):
		self._video_size = value
	
	@property
	def video_filename(self):
		return self._video_filename
	@video_filename.setter
	def video_filename(self, value):
		self._video_filename = value


	@property
	def framerate(self):
		return self._framerate
	@framerate.setter
	def framerate(self, value):
		self._framerate = value


	@property
	def images_base_path(self):
		return self._images_base_path
	@images_base_path.setter
	def images_base_path(self, value):
		self._images_base_path = value

	@property
	def tmp_base_path(self):
		return self._tmp_base_path
	@tmp_base_path.setter
	def tmp_base_path(self, value):
		self._tmp_base_path = value


	@property
	def title_image_filename(self):
		return self._title_image_filename
	@title_image_filename.setter
	def title_image_filename(self, value):
		self._title_image_filename = value

	@property
	def closing_image_filename(self):
		return self._closing_image_filename
	@closing_image_filename.setter
	def closing_image_filename(self, value):
		self._closing_image_filename = value
	
	@property
	def images(self):
		return self._images
	@images.setter
	def images(self, value):
		self._images = value


	@property
	def audio_filename(self):
		return self._audio_filename
	@audio_filename.setter
	def audio_filename(self, value):
		self._audio_filename = value


	@property
	def ffmpeg_path(self):
		return self._ffmpeg_path
	@ffmpeg_path.setter
	def ffmpeg_path(self, value):
		self._ffmpeg_path = value
	
	@property
	def error_message(self):
		return self._error_message


	''' Prepare and clean the temp file
	'''
	def _prepareTmpFile(self, filename):
		if filename is None or len(filename) == 0:
			return None

		tmp_filename = filename
		if self._tmp_base_path is not None:
			tmp_filename = self._tmp_base_path + os.sep + filename

		if os.path.exists(tmp_filename):
			os.remove(tmp_filename)

		return tmp_filename


	''' Return the total video length
		FIXME : add a brief description of how this length is computed
	'''
	def _getTotalVideoLength(self):
		return  self._getTitleVideoLength() - self._switch_image_fade_length + \
				(self._getContentImageVideoLength() - self._switch_image_fade_length) * len(self._images) + \
				self._getClosingVideoLength()


	''' FIXME : add description
	'''
	def _getContentImageVideoLength(self):
		return (self._switch_image_fade_length * 2) + self._content_image_length


	''' FIXME : add description
	'''
	def _getTitleVideoLength(self):
		return self._title_fadein_length + self._title_image_length + self._switch_image_fade_length


	''' FIXME : add description
	'''
	def _getClosingVideoLength(self):
		return self._switch_image_fade_length + self._closing_image_length + self._closing_fadeout_length + 1 # +1 for the allowing a good fading out to black


	''' FIXME : add description
	'''
	def _createEmptyVideo(self):
		rv = call([
			self._ffmpeg_path, 
			"-y", 
			"-filter_complex", ("color=black:%s:%s" % (self._video_size, self._framerate)), 
			"-c:v", self._codec_name, 
			"-t", str(self._getTotalVideoLength()),
			self._prepareTmpFile("out_tmp_base.mov")])

		if rv == 0:
			return True

		return False


	''' FIXME : add description
	'''

	def _createTitleImageVideo(self):
		if self._title_image_filename is None:
			return True

		sBasePath = ""
		if self._images_base_path is not None:
			sBasePath = self._images_base_path + os.sep
		
		sFilterComplex = "[0:v] format=rgb32 [VIN]"

		sFadeInEndFrame = self._framerate * self._title_fadein_length
		sFadeOutFrameCount = self._framerate * self._switch_image_fade_length
		sFadeOutStartFrame = self._framerate * (self._title_fadein_length + self._title_image_length)

		bWantsFadeIn = (self._title_fadein_length is not None and self._title_fadein_length > 0)
		bWantsFadeOut = (self._switch_image_fade_length is not None and self._switch_image_fade_length > 0)

		if bWantsFadeIn and not bWantsFadeOut:
			sFilterComplex = sFilterComplex + \
					("; [VIN] fade=in:%s:%s:alpha=0" % (
						"0", 
						sFadeInEndFrame))
		
		elif not bWantsFadeIn and bWantsFadeOut:
			sFilterComplex = sFilterComplex + \
					("; [VIN] fade=out:%s:%s:alpha=1" % (
						sFadeOutStartFrame,
						sFadeOutFrameCount))

		elif bWantsFadeIn and bWantsFadeIn:
			sFilterComplex = sFilterComplex + \
					("; [VIN] fade=in:%s:%s:alpha=0 [VOUT]; [VOUT] fade=out:%s:%s:alpha=1" % (
						"0", 
						sFadeInEndFrame,
						sFadeOutStartFrame,
						sFadeOutFrameCount))
		
		rv = call([
			self._ffmpeg_path, 
			"-y", 
			"-loop", "1",
			"-f", "image2",
			"-i", sBasePath + self._title_image_filename,
			"-filter_complex", sFilterComplex,
			"-c:v", self._codec_name, 
			"-t", str(self._getTitleVideoLength()),
			"-r", str(self._framerate),
			"-s", self._video_size,
			self._prepareTmpFile("out_tmp_title.mov")])

		if rv != 0:
			return False

		m_bTitleVideoCreated = True
		return True
	
	
	''' FIXME : add description
	'''
	def _createClosingImageVideo(self):
		if self._closing_image_filename is None:
			return True

		sBasePath = ""
		if self._images_base_path is not None:
			sBasePath = self._images_base_path + os.sep
		sTmpPath = ""
		if self._tmp_base_path is not None:
			sTmpPath = self._tmp_base_path + os.sep


		''' Phase 1 '''


		sFilterComplex = ""

		if self._closing_fadeout_length is not None and self._closing_fadeout_length > 0:
			sFadeOutFrameCount = self._framerate * self._closing_fadeout_length
			sFadeOutStartFrame = self._framerate * (self._switch_image_fade_length + self._closing_image_length)
			sFilterComplex = sFilterComplex + \
					("fade=out:%s:%s:alpha=0" % (
						sFadeOutStartFrame, 
						sFadeOutFrameCount))
		
		rv = call([
			self._ffmpeg_path, 
			"-y", 
			"-loop", "1",
			"-f", "image2",
			"-i", sBasePath + self._closing_image_filename,
			"-filter_complex", sFilterComplex,
			"-c:v", self._codec_name, 
			"-t", str(self._getClosingVideoLength()),
			"-r", str(self._framerate),
			"-s", self._video_size,
			self._prepareTmpFile("out_tmp_closing_1.mov")])

		if rv != 0:
			return False
		

		''' Phase 2 '''


		sFilterComplex = ""

		if self._switch_image_fade_length is not None and self._switch_image_fade_length > 0:
			sFadeInFrameCount = self._framerate * self._switch_image_fade_length
			sFilterComplex = sFilterComplex + \
					("[0:v] format=rgb32 [VIN]; [VIN] fade=in:%s:%s:alpha=1" % (
						"0", 
						sFadeInFrameCount))
		
		rv = call([
			self._ffmpeg_path, 
			"-y", 
			"-i", sTmpPath + "out_tmp_closing_1.mov",
			"-filter_complex", sFilterComplex,
			"-c:v", self._codec_name, 
			self._prepareTmpFile("out_tmp_closing_2.mov")])

		if rv != 0:
			return False

		m_bClosingVideoCreate = True
		return True


	''' FIXME : add description
	'''
	def _createContentImageVideo(self):
		iCounter = 1
		sBasePath = ""
		if self._images_base_path is not None:
			sBasePath = self._images_base_path + os.sep
		
		sFilterComplex = "[0:v] format=rgb32 [VIN]"
		if self._switch_image_fade_length is not None and self._switch_image_fade_length > 0:
			sFadeFrameCount = self._framerate * self._switch_image_fade_length
			sFadeOutStartFrame = self._framerate * (self._switch_image_fade_length + self._content_image_length)
			
			sFilterComplex = sFilterComplex + \
					("; [VIN] fade=in:%s:%s:alpha=1 [VOUT]; [VOUT] fade=out:%s:%s:alpha=1" % (
						"0", 
						sFadeFrameCount,
						sFadeOutStartFrame,
						sFadeFrameCount))
		
		for sImageName in self._images:
			rv = call([
				self._ffmpeg_path, 
				"-y", 
				"-loop", "1",
				"-f", "image2",
				"-i", sBasePath + sImageName,
				"-filter_complex", sFilterComplex,
				"-c:v", self._codec_name, 
				"-t", str(self._getContentImageVideoLength()),
				"-r", str(self._framerate),
				"-s", self._video_size,
				self._prepareTmpFile("out_tmp_content_%s.mov" % str(iCounter))])

			if rv != 0:
				return False
			
			iCounter = iCounter + 1

		m_bContentImageVideoCreate = True
		return True


	''' FIXME : add description
	'''
	def _mergeVideos(self):
		sTmpPath = ""
		if self._tmp_base_path is not None:
			sTmpPath = self._tmp_base_path + os.sep

		iOffset = 0

		rv = call([
			self._ffmpeg_path, 
			"-y", 
			"-i", sTmpPath + "out_tmp_base.mov",
			"-itsoffset", str(iOffset),
			"-i", sTmpPath + "out_tmp_title.mov",
			"-filter_complex", "[0:v] format=argb [VIN1]; [1:v] format=argb [VIN2]; [VIN1][VIN2] overlay",
			"-c:v", self._codec_name, 
			self._prepareTmpFile("out_final_1.mov")])

		if rv != 0:
			return False

		iOffset = iOffset + (self._getTitleVideoLength() - 1)

		for i in range(1, len(self._images)+1): # loop 1-based
			rv = call([
				self._ffmpeg_path, 
				"-y", 
				"-i", sTmpPath + ("out_final_%s.mov" % str(i)),
				"-itsoffset", str(iOffset),
				"-i", sTmpPath + ("out_tmp_content_%s.mov" % str(i)),
				"-filter_complex", "[0:v] format=argb [VIN1]; [1:v] format=argb [VIN2]; [VIN1][VIN2] overlay",
				"-c:v", self._codec_name, 
				self._prepareTmpFile("out_final_%s.mov" % str(i+1))])

			if rv != 0:
				return False

			iOffset = iOffset + (self._getContentImageVideoLength() - 1)

		rv = call([
			self._ffmpeg_path, 
			"-y", 
			"-i", sTmpPath + ("out_final_%s.mov" % str(len(self._images)+1)),
			"-itsoffset", str(iOffset),
			"-i", sTmpPath + "out_tmp_closing_2.mov",
			"-filter_complex", "[0:v] format=argb [VIN1]; [1:v] format=argb [VIN2]; [VIN1][VIN2] overlay",
			"-c:v", self._codec_name, 
			self._prepareTmpFile("out_final.mov")])

		if rv != 0:
			return False
		
		return True


	'''
	'''
	def _addMusicBackground(self):
		if self._audio_filename is None:
			return True
		
		sTmpPath = ""
		if self._tmp_base_path is not None:
			sTmpPath = self._tmp_base_path + os.sep
		
		rv = call([
			self._ffmpeg_path, 
			"-y", 
			"-i", sTmpPath + "out_final.mov",
			"-i", self._audio_filename,
			"-c:v", "libx264", 
			"-b:v", "5000k",
			"-c:a", "aac",
			"-b:a", "192k",
			"-strict", "experimental",
			"-shortest",
			self._prepareTmpFile(self._video_filename)])

		if rv != 0:
			return False
		
		return True

	'''
	'''
	def makeSlideshowVideo(self):

		''' FIXME : test for ffmpeg existence
		'''

		if len(self._images) == 0:
			self._error_message = "No content images provided"
			return False

		if not self._createEmptyVideo():
			return False
		if not self._createTitleImageVideo():
			return False
		if not self._createClosingImageVideo():
			return False
		if not self._createContentImageVideo():
			return False
		if not self._mergeVideos():
			return False
		if not self._addMusicBackground():
			return False

		return True

