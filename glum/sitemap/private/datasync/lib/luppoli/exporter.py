import sys
import os
import os.path
import json
import ftplib
import urllib
import zipfile
import pyodbc
from lib.ymcore import logger
from lib.ymcore.logger import log

''' Exporter module.

The Exporter class is used to perform all the export task,
such as fetching current data and packing it into a zip file
that is uploaded into the server

This module use the ymcore.logger facility. No initialization
or closing action are performed on the logger. Is up to the
caller to prepare the logger.
'''

class Exporter(object):

	def __init__(self):
		self._sTemplateFilename = os.path.basename(sys.argv[0]).replace(".py", "")
		self._sSqlServerHost = ""
		self._sSqlServerDatabase = ""
		self._sSqlServerUser = ""
		self._sSqlServerPassword = ""
		self._oConnection = None
	
	@property
	def sqlServerHost(self):
		return self._sSqlServerHost
	@sqlServerHost.setter
	def sqlServerHost(self, value):
		self._sSqlServerHost = value

	@property
	def sqlServerDatabase(self):
		return self._sSqlServerDatabase
	@sqlServerDatabase.setter
	def sqlServerDatabase(self, value):
		self._sSqlServerDatabase = value
	
	@property
	def sqlServerUser(self):
		return self._sSqlServerUser
	@sqlServerUser.setter
	def sqlServerUser(self, value):
		self._sSqlServerUser = value

	@property
	def sqlServerPassword(self):
		return self._sSqlServerPassword
	@sqlServerPassword.setter
	def sqlServerPassword(self, value):
		self._sSqlServerPassword = value

	@property
	def ftpHost(self):
		return self._sFtpHost
	@ftpHost.setter
	def ftpHost(self, value):
		self._sFtpHost = value

	@property
	def ftpUser(self):
		return self._sFtpUser
	@ftpUser.setter
	def ftpUser(self, value):
		self._sFtpUser = value

	@property
	def ftpPassword(self):
		return self._sFtpPassword
	@ftpPassword.setter
	def ftpPassword(self, value):
		self._sFtpPassword = value
	
	@property
	def ftpBasePath(self):
		return self._sFtpBasePath
	@ftpBasePath.setter
	def ftpBasePath(self, value):
		self._sFtpBasePath = value
	
	@property
	def jsonFilename(self):
		return "datasync-export.json"
	
	@property
	def zipFilename(self):
		return "datasync-export.zip"
	

	def _getConnectionString(self):
		sConnectionString = "SERVER=%s;DATABASE=%s;UID=%s;PWD=%s;" % (
				self._sSqlServerHost,
				self._sSqlServerDatabase,
				self._sSqlServerUser,
				self._sSqlServerPassword)
		
		if sys.platform == "linux2":
			sConnectionString = "DRIVER={FreeTDS};TDS_Version=8.0;Port=1433;" + sConnectionString
		elif sys.platform == "win32":
			sConnectionString = "DRIVER={SQL Server Native Client 10.0};" + sConnectionString
		else:
			return None

		return sConnectionString


	''' Fetch the data from the Sql Server database and pack it
	into a json file
	'''
	def _dumpData(self):
		log("Start dumping data")

		log("	Opening SqlServer connection")
		with pyodbc.connect(self._getConnectionString()) as oConn:
			oCursor = oConn.cursor()
			hJsonContent = {}
			hTableNames = {}

			''' Fetch the list of view have to be exported 
			'''
			for oRow in oCursor.execute("SELECT * FROM dbo.fn_sitopubblico_export_table_list()"):
				hTableNames[oRow.view_name] = oRow.table_name

			''' Scroll the dataset 
			'''
			for sKey in hTableNames:
				aColumnNames = []
				aValues = []
				oCursor.execute("SELECT * FROM " + sKey)

				for oColumnDescription in oCursor.description:
					aColumnNames.append(oColumnDescription[0])

				aRows = oCursor.fetchall()
				log("	Fetched %s rows for view: %s" % (len(aRows), sKey))

				for oRow in aRows:
					hRow = {}
					for (i, sColumnName) in enumerate(aColumnNames):
						''' Send to the json encoder only unicode data or
						the encoder can fail to dump the data '''
						if type(oRow[i]) is str:
							hRow[sColumnName] = oRow[i].decode("latin1")
						else:
							hRow[sColumnName] = oRow[i]
					aValues.append(hRow)

				if len(aValues) > 0:
					hJsonContent[hTableNames[sKey]] = aValues


			# Write the json content to file
			with open(self.jsonFilename, "w") as oFile:
				json.dump(hJsonContent, oFile)

			oCursor.close()

		log("	Closing SqlServer connection")
		log("Data dump'd")

		return True


	''' Create the zip file with the content of the data to export
	'''
	def _createZipfile(self):
		if not os.path.exists(self.jsonFilename):
			log("JSON file doesn't exist.")
			return False
		
		try:
			log("Adding %s to zip file" % (self.jsonFilename))
			oZipFile = zipfile.ZipFile(self.zipFilename, "w", zipfile.ZIP_DEFLATED)
			oZipFile.write(self.jsonFilename)
			oZipFile.close()
		except:
			log("Zip file creation failed!")
			return False

		return True


	''' Upload the zipped file with the content of the data to export
	to the production server
	'''
	def _ftpUpload(self):
		try:
			oFtp = ftplib.FTP(
					self.ftpHost,
					self.ftpUser,
					self.ftpPassword
				)
			
			log("Uploading %s to %s" % (self.zipFilename, self.ftpHost))
			oZipFile = open(self.zipFilename, "rb")
			oFtp.cwd(self.ftpBasePath)
			oFtp.storbinary("STOR " + self.zipFilename, oZipFile)
			
			oZipFile.close()
			oFtp.close()

		except:
			log("Zip file upload failed!")
			log("Error: %s" % str(sys.exc_info()))
			return False

		return True


	''' Perform a cleanup of the temporary files created
	'''
	def _cleanup(self):
		if os.path.exists(self.jsonFilename):
			os.remove(self.jsonFilename)
			log("Removing cache file: " + self.jsonFilename)
		if os.path.exists(self.zipFilename):
			os.remove(self.zipFilename)
			log("Removing cache file: " + self.zipFilename)


	''' All the jobs are executed in this method, with right order
	and with the right checks.

	Return True if all the jobs are correctly executed and finished,
	False otherwise
	'''
	def start(self):
		try:
			''' dump the data '''
			if not self._dumpData():
				return False
			''' create the zipfile '''
			if not self._createZipfile():
				return False
			''' upload the zipfile '''
			if not self._ftpUpload():
				return False
			''' clean up temporary files '''
			self._cleanup()
		except:
			log("A blocking error occurred: %s" % str(sys.exc_info()))
			return False

		return True

