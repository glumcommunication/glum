import sys
import os
import json
import urllib
import zipfile
import MySQLdb
from lib.ymcore import logger
from lib.ymcore.logger import log

''' Importer module.

The Importer class is used to perform all the import task,
such as syncing data and photo.

This module use the ymcore.logger facility. No initialization
or closing action are performed on the logger. Is up to the
caller to prepare the logger.
'''

class Importer(object):

	def __init__(self):
		self._sTemplateFilename = os.path.basename(sys.argv[0]).replace(".py", "")
		self._sMysqlHost = ""
		self._sMysqlDatabase = ""
		self._sMysqlUser = ""
		self._sMysqlPassword = ""
		self._oConnection = None
		self._aImageNames = []
	
	@property
	def mysqlHost(self):
		return self._sMysqlHost
	@mysqlHost.setter
	def mysqlHost(self, value):
		self._sMysqlHost = value

	@property
	def mysqlDatabase(self):
		return self._sMysqlDatabase
	@mysqlDatabase.setter
	def mysqlDatabase(self, value):
		self._sMysqlDatabase = value
	
	@property
	def mysqlUser(self):
		return self._sMysqlUser
	@mysqlUser.setter
	def mysqlUser(self, value):
		self._sMysqlUser = value

	@property
	def mysqlPassword(self):
		return self._sMysqlPassword
	@mysqlPassword.setter
	def mysqlPassword(self, value):
		self._sMysqlPassword = value

	@property
	def imageBaseURL(self):
		return self._sImagebaseURL
	@imageBaseURL.setter
	def imageBaseURL(self, value):
		self._sImagebaseURL = value if value.endswith("/") else (value + "/")

	@property
	def imageStoragePath(self):
		return self._sImageStoragePath
	@imageStoragePath.setter
	def imageStoragePath(self, value):
		self._sImageStoragePath = value if value.endswith("/") else (value + "/")

	@property
	def jsonFilename(self):
		return "datasync-export.json"
	
	@property
	def zipFilename(self):
		return "datasync-export.zip"


	def _getImageFileRemoteURL(self, sImageName):
		return self.imageBaseURL + sImageName

	def _getImageFileLocalPath(self, sImageName):
		return self.imageStoragePath + sImageName


	def _initMysqlConnection(self):
		if self._oConnection != None:
			return
		self._oConnection = MySQLdb.connect(
				host=self._sMysqlHost,
				db=self._sMysqlDatabase,
				user=self._sMysqlUser,
				passwd=self._sMysqlPassword,
				charset="utf8")

	def _closeMysqlConnection(self):
		if self._oConnection == None:
			return
		self._oConnection.commit()
		self._oConnection.close()

	def _getCursor(self):
		if self._oConnection == None:
			return None
		return self._oConnection.cursor()


	''' Restore the data from the JSON content to
	the Mysql server database
	'''
	def _restoreData(self):
		log("Start restoring data")
		log("	Opening Mysql Server connection")

		''' pyodbc works great also with Mysql, but 
		the host (Register.it) doesn't support that library '''

		self._initMysqlConnection()
		oCursor = self._getCursor()

		hJsonContent = {}
		bTempTableFailed = False
		
		log("	Parsing json data")
		with open(self.jsonFilename, "r") as oFile:
			hJsonContent = json.load(oFile)

		''' Perform all insert queries in temporary tables 
		with the same name of the original table and a "_temp" suffix '''

		for sKey in hJsonContent:
			if len(hJsonContent[sKey]) == 0:
				continue

			sTableName = sKey
			sTempTableName = sKey + "_temp"
			
			log("")

			log("	Creating temporary table %s" % sTempTableName)
			oCursor.execute("CREATE TEMPORARY TABLE %s LIKE %s" % (sTempTableName, sTableName))

			aColumnNames = hJsonContent[sKey][0].keys()
			iParamCount = len(aColumnNames)
			sQuery = "INSERT INTO %s (%s) VALUES(%s)" % (sTempTableName, ",".join(aColumnNames), ("%s,"*iParamCount)[:-1])

			aAllParams = []

			for hRow in hJsonContent[sKey]:
				aParams = []
				for sField in aColumnNames:
					''' Unicode string should be encoded with the wanted
					encoding or the MySQLdb can fail to insert the data '''
					if type(hRow[sField]) is unicode:
						aParams.append(hRow[sField].encode("utf8"))
					else:
						aParams.append(hRow[sField])
				aAllParams.append(aParams)

			iRecordCount = len(aAllParams)
			log("	Executing insert query on temporary table for %s records" % iRecordCount)
			oCursor.executemany(sQuery, aAllParams)
			
			''' Count how many records have been inserted into the temporary table and check
			how many records are provided by the json file. If the values are equal continue 
			with the next temporary table '''

			oCursor.execute("SELECT COUNT(*) FROM %s" % sTempTableName)
			oRow = oCursor.fetchone()
			if oRow == None or oRow[0] != iRecordCount:
				log("	Table %s has a different record count from the json data." % sTempTableName)
				bTempTableFailed = True
				break;


		if bTempTableFailed:
			log("	Aborting execution. No working tables have been touched.")
			return False

		''' All temporary tables have been prepared and have correct data.
		Copy the data to the production tables (the one without the "_temp" suffix '''

		for sKey in hJsonContent:
			if len(hJsonContent[sKey]) == 0:
				continue

			sTableName = sKey
			sTempTableName = sKey + "_temp"

			log("")

			log("	Flushing out all data from table %s" % sTableName)
			oCursor.execute("DELETE FROM %s" % sTableName)

			log("	Copying all new data from %s to %s" % (sTempTableName, sTableName))
			oCursor.execute("INSERT INTO %s SELECT * FROM %s" % (sTableName, sTempTableName))

			oCursor.execute("SELECT COUNT(*) FROM %s" % sTempTableName)
			oRow = oCursor.fetchone()
			log("	Table %s has %s records." % (sTableName, oRow[0]))
			
			log("	Table %s updated" % sTableName)


		log("")


		''' Prepare all the derived data. Register.it mysql's database doesn't
		support the CREATE TRIGGER statement. Post processing operation must be
		done here... '''
		
		log("	Post processing data")
		log("		Preparing post-processed data")
		
		aAffittiTypes = []
		aAttivitaTypes = []
		oCursor.execute("SELECT C_DescrizioneAffitto, C_Attivita FROM T_Case WHERE C_DescrizioneAffitto IS NOT NULL OR C_DescrizioneAffitto != '' OR C_Attivita IS NOT NULL or C_Attivita != ''")
		
		for oRow in oCursor.fetchall():
			if oRow[0]: # both None and empty string are evaluated as False
				aTokens = oRow[0].split(",")
				for sToken in aTokens:
					sToken = sToken.strip()
					if not sToken in aAffittiTypes:
						aAffittiTypes.append(sToken)

			if oRow[1] and not oRow[1] in aAttivitaTypes:
				aAttivitaTypes.append(oRow[1])

		''' Perform the sql insert '''
		
		log("		Adding post-processed data")
		
		oCursor.execute("DELETE FROM TD_TipologieAffitti")
		oCursor.execute("DELETE FROM TD_TipologieAttivitaCommerciali")
		''' don't use the executemany statement here, seems to be buggy... '''
		for sItem in aAffittiTypes:
			oCursor.execute("INSERT INTO TD_TipologieAffitti VALUES(SHA1(%s), %s)", (sItem, sItem))
		for sItem in aAttivitaTypes:
			oCursor.execute("INSERT INTO TD_TipologieAttivitaCommerciali VALUES(SHA1(%s), %s)", (sItem, sItem))
		
		log("	Data post processed correctly")
		log("")
		
		
		''' Populate the aImageNames array with all the images that should be
		sync'd in the local storage '''

		oCursor.execute("SELECT MC_PathMedia as imageName FROM T_MediaCase")
		for oRow in oCursor.fetchall():
			self._aImageNames.append(oRow[0])
		oCursor.execute("SELECT SP_ImgName as imageName FROM T_SpecialPrivilege")
		for oRow in oCursor.fetchall():
			self._aImageNames.append(oRow[0])
		oCursor.execute("SELECT RL_ImgName as imageName FROM T_ReteLuppoli")
		for oRow in oCursor.fetchall():
			self._aImageNames.append(oRow[0])
		oCursor.execute("SELECT B_ImgName as imageName FROM T_Blog")
		for oRow in oCursor.fetchall():
			self._aImageNames.append(oRow[0])

		log("	Closing Mysql Server connection")
		oCursor.close()
		self._closeMysqlConnection()
		log("Data restored")

		return True


	''' Extract the content of the zip file in the current working directory
	'''
	def _extractZipfile(self):
		if not os.path.exists(self.zipFilename):
			log("Zip file doesn't exist.")
			return False

		try:
			log("Extracting %s to %s" % (self.zipFilename, os.getcwd()))
			oZipFile = zipfile.ZipFile(self.zipFilename, "r", zipfile.ZIP_DEFLATED)
			oZipFile.extractall()
			oZipFile.close()
		except:
			log("Zip file extraction failed!")
			log("Error: %s" % str(sys.exc_info()))
			return False

		return True


	''' Sync the photos to the local storage
	'''
	def _syncPhoto(self):
		log("Start sync'ing photos")

		if not os.path.exists(self.imageStoragePath):
			try:
				log("Creating the image storage directory: %s" % self.imageStoragePath)
				os.makedirs(self.imageStoragePath)
			except:
				log("Can't create the image storage directory.")
				log("Error: %s" % str(sys.exc_info()))
				return False

		log("	Syncing the image storage content")
		for sFilename in self._aImageNames:
			if not os.path.exists(self._getImageFileLocalPath(sFilename)):
				log("		- Missing %s, downloading from: %s" % (sFilename, self._getImageFileRemoteURL(sFilename)))
				urllib.urlretrieve(
						self._getImageFileRemoteURL(sFilename),
						self._getImageFileLocalPath(sFilename)
					)

		for sFilename in os.listdir(self.imageStoragePath):
			if sFilename not in self._aImageNames and self._isMediaPhoto(sFilename):
				log("		- Image %s is not referenced in the database. Removing it." % sFilename)
				os.remove(self._getImageFileLocalPath(sFilename))
		log("	Image storage content sync'd")

		log("Photo sync'd")
		return True

	''' Return True if the photo has a compatible reference code as 
	prefix in the image filename 
	'''
	def _isMediaPhoto(self, sImageName):
		iPrefixEnd = sImageName.find("_")

		if iPrefixEnd == 6:
			sPrefix = sImageName[0:6]
			if sPrefix[0:1].isalpha() and sPrefix[0:1].isupper() and sPrefix[1:6].isdigit():
				return True
		elif iPrefixEnd == 7:
			sPrefix = sImageName[0:7]
			if sPrefix[0:2].isalpha() and sPrefix[0:2].isupper() and sPrefix[2:7].isdigit():
				return True

		return False

	''' Perform a cleanup of the temporary files created
	'''
	def _cleanup(self):
		if os.path.exists(self.jsonFilename):
			os.remove(self.jsonFilename)
			log("Removing cache file: " + self.jsonFilename)
		if os.path.exists(self.zipFilename):
			os.remove(self.zipFilename)
			log("Removing cache file: " + self.zipFilename)


	''' All the jobs are executed in this method, with right order
	and with the right checks.

	Return True if all the jobs are correctly executed and finished,
	False otherwise
	'''
	def start(self):
		try:
			''' extract the current zip file '''
			if not self._extractZipfile():
				return False
			''' restore the data from the json file '''
			if not self._restoreData():
				return False
			''' sync the photos '''
			if not self._syncPhoto():
				return False
			''' clean up temporary files '''
			self._cleanup()
		except:
			log("A blocking error occurred: %s" % str(sys.exc_info()))
			return False

		return True

