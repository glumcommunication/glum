<?php
$pageName = "one";
$pageTitle = "Pubblicità";
$pageDesc = "Cosa facciamo";
include_once 'header.php';
?>
<nav id="navigationMap">
	<ul>
		<li class="return"><a class="ascensorLink ascensorLink0"></a></li>
		<li class="return"><a class="ascensorLink ascensorLink1"></a></li>
		<li class="return"><a class="ascensorLink ascensorLink2"></a></li>
		<li class="return"><a class="ascensorLink ascensorLink3"></a></li>
		<li class="return"><a class="ascensorLink ascensorLink4"></a></li>
                <li class="return"><a class="ascensorLink ascensorLink5"></a></li>
        </ul>
</nav>
    <div id="backhome">
        <a href="/" title="Torna alla home">HOME</a>
    </div>
    <div id="sectionName">
        <p>COSA FACCIAMO</p>
    </div>
    <div id="ascensorOne" class="resizeMe">
        <div>           
            <div class="content">
                <h1>PUBBLICITA'<br>MULTIMEDIALE</h1>
                <p>
                    Progettazione e realizzazione siti internet, portali, e-commerce, newsletter, app IOS e Android, analisi web, 
                    ottimizzazione e posizionamento sui motori di ricerca, banner pubblicitari on line, digital design.
                </p>
            </div>
        </div>           
        <div>           
            <div class="content">
                <h1>CREATIVITA'</h1>
                <p>
                    Progettazione loghi, immagine coordinata, ideazione del naming, graphic design, branding, exhibit design, packaging.

                </p>
            </div>
        </div>  
        <div>           
            <div class="content">
                <h1>SOCIAL MARKETING</h1>
                <p>
                    Consulenza strategica e gestione pagine aziendali Facebook, Twitter, Google+, gestione canali aziendali Youtube.
                </p>
            </div>
        </div>          
        <div>           
            <div class="content">
                <h1>PUBLIC RELATIONS</h1>
                <p>
                    Ufficio stampa, organizzazione eventi, progettazione e coordinamento attività.

                </p>
            </div>
        </div>    
        <div>           
            <div class="content">
                <h1>PUBBLICITA' CARTACEA</h1>
                <p>
                    Realizzazione grafica e stampa su vari supporti di volantini, locandine, biglietti da visita, brochures, cataloghi, 
                    gadget, cartellonistica, manifesti, totem, stendardi.
                </p>
            </div>
        </div>
        <div>           
            <div class="content">
                <h1>CONSULENZA AZIENDALE</h1>
                <p>
                    Consulenza strategica per comunicazione e marketing, analisi strutturale, pianificazione, 
                    riorganizzazione interna, progettazione e fund raising, pianificazione delle strategie di marketing e 
                    promozione a diversi livelli.
                </p>
            </div>
        </div>   
    </div>
<?php
include_once 'footer.php';
?>