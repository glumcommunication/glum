<?php
$pagename="index";
$pagedesc="GLuM Communication, agenzia di comunicazione con sede a Siena. Si occupa di pubblicità, web ed email marketing, social media marketing, immagine coordinata.";
$pageTitle="Home";
include_once 'header.php';
?>

    <a href="pubblicita.php" class="hl">
        <div class="homebox" id="one">
            <div id="onein" class="homeboxin">
                <div class="hb_content">
    
                </div>
                <div class="hb_title">
                    <p>
                        COSA FACCIAMO
                    </p>
                </div>
            </div>
        </div>
    </a>
    <div class="homebox" id="two">
        <div id="twoin" class="homeboxin">
            <div id="backhome" style="display:none;color: #fff;border-color:#fff;">
                HOME
            </div>
            <div id="sectionName" style="display:none;color:#fff;border-top-color:#fff;">
                <p>EMAIL</p>
            </div>        
            <div class="hb_content">
                <?php include_once("form.php"); ?>
            </div>
            <div class="hb_title">
                <p>
                    EMAIL
                </p>
            </div>
        </div>
    </div>

    <a href="contatti.php" class="hl">
        <div class="homebox" id ="three">
            <div id="threein" class="homeboxin">
                <div class="hb_content">
                    <p>GLuM Communication Srl</p>
                    <p>Strada Massetana Romana 64</p>
                    <p>53100 Siena</p>
                    <p>+39 0577 280213</p>
                    <p>info@glumcommunication.it</p>
                </div>
                <div class="hb_title">
                    <p>
                        CONTATTI
                    </p>
                </div>
            </div>
        </div>
    </a>
    <a href="portfolio.php" class="hl">
        <div class="homebox" id="four">
            <div id="fourin" class="homeboxin">
               <div class="hb_content">
                </div>
                <div class="hb_title">
                    <p>
                        PORTFOLIO
                    </p>
                </div> 
            </div>
        </div>
    </a>
    <div class="homebox" id="five">
        <div id="fivein" class="homeboxin">
        <div class="hb_content">
        </div>
        <div class="hb_title">
            <p>
                INSPIRATION
            </p>
        </div>
        </div>
    </div>
    <div class="homebox" id="six">
        <div id="sixin" class="homeboxin">
        <div class="hb_content">
            
<!--            <img src="img/glumlogo.jpg">-->
        </div>
        <div class="hb_title">
            <p id="iva">P. Iva: 01354790527</p>
        </div>
        </div>
    </div>
    <div class="homebox" id="seven">
        <div id="sevenin" class="homeboxin">
           
            <a href="backend/"><img src="img/lock.png"></a>
           
        </div>        
    </div>
    <a href="comunicazione.php" class="hl">
        <div class="homebox" id="eight">
            <div id="eightin" class="homeboxin">
                <div class="hb_content">
                    
                </div>
                <div class="hb_title">
                    <p>
                        AGENZIA
                    </p>
                </div>
            </div>
        </div>
    </a>
    <div class="homebox" id="nine">
        <div id="ninein" class="homeboxin">
            <div class="hb_content">
                <div class="socialbox">
                    <div class="sociallogo fb">
                        <a href="https://www.facebook.com/glumcommunication" target="_BLANK" class="hl"><img src="img/fb.jpg" alt="Facebook"></a>
                    </div>
                    <div class="sociallogo gp">
                        <a href="https://plus.google.com/101949524368702286587" target="_BLANK" class="hl"><img src="img/gp.jpg" alt="Google+"></a>
                    </div>
                    <div class="sociallogo yt">
                        <a href="http://www.youtube.com/user/GLuMCommunication" target="_BLANK" class="hl"><img src="img/yt.jpg" alt="Youtube"></a>
                    </div>
                    <div class="sociallogo tw">
                        <a href="https://twitter.com/AgenziaGlum" target="_BLANK"  class="hl"><img src="img/tw.jpg" alt="Twitter"></a>
                    </div>                
                </div>
            </div>
            <div class="hb_title">
                <p>
                    SOCIAL
                </p>
            </div>
        </div>
    </div>
    <a href="progetti.php" class="hl">
        <div class="homebox" id="ten">
            <div id="tenin" class="homeboxin">
                <div class="hb_content">
                </div>
                <div class="hb_title">
                    <p>
                        PROGETTI
                    </p>
                </div>
            </div>
        </div>
    </a>
    <a href="faces.php"  class="hl">
        <div class="homebox"  id="eleven">
            <div id="elevenin" class="homeboxin">
                <div class="hb_content">
                </div>
                <div class="hb_title">
                    <p>
                        CHI SIAMO
                    </p>
                </div>
            </div>
        </div>
    </a>
    <div class="homebox" id="twelve">
        <div id="twelvein" class="homeboxin">
            <div class="hb_content">
            </div>
            <div class="hb_title">
                <!--<p>
                    ISTINTO DI COMUNICARE
                </p>-->
            </div>
        </div>
    </div>
<?php
    include_once 'footer.php';
?>
