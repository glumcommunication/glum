<?php
$thisPage = "richiediinformazioni";
include("header.php");
?>
<body>

<?php

echo <<<_END
<div id="main" class="contatti">
        <div class="opacizeme">
        <div class="mainleft">
            <h1>Contatti</h1>
            <p style="padding-top: 0;">
            Via del Chianti, 70 - 53019 - Castelnuovo Berardenga (SI)
            </p>
            <p>
            Sulla Siena-Bettolle uscire per Castelnuovo Berardenga e entrare nel paese direzione San Gusmè.<br>
            In via del Chianti, poco prima della fine di Castelnuovo, c&apos;è un ampio parcheggio a pochi metri dal nostro ristorante.
            </p>
            <p>
            Se si arriva dal Chianti appena entrati nel centro abitato la Taverna si trova subito sulla sinistra.
            </p>
            <p style="font-size: 1.5em;">
            <img src="images/telefono.png" alt="Telefona!" style="vertical-align: middle;"> 0577 355547
            </p>
        </div>
        <div class="maincenter">
            <div class="bigpiccontainer" style="height:510px;">
                <div style="background-color: #F9F2E3;border: 5px solid #732726;box-shadow: 0 0 10px #000000;">
                <iframe width="555" height="476" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=taverna+della+berardenga&amp;aq=&amp;sll=41.29085,12.71216&amp;sspn=29.441464,67.631836&amp;t=h&amp;ie=UTF8&amp;hq=taverna+della+berardenga&amp;hnear=&amp;cid=6484612953957846267&amp;ll=43.349633,11.504719&amp;spn=0.001794,0.002999&amp;z=18&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="https://maps.google.it/maps?f=q&amp;source=embed&amp;hl=it&amp;geocode=&amp;q=taverna+della+berardenga&amp;aq=&amp;sll=41.29085,12.71216&amp;sspn=29.441464,67.631836&amp;t=h&amp;ie=UTF8&amp;hq=taverna+della+berardenga&amp;hnear=&amp;cid=6484612953957846267&amp;ll=43.349633,11.504719&amp;spn=0.001794,0.002999&amp;z=18&amp;iwloc=A" style="background-color: transparent;color:#0000FF;text-align:left">Visualizzazione ingrandita della mappa</a></small>
                </div>
            </div>
            <div class="maintext">
                <p><a href="mailto:info@tavernadellaberardenga.it">info@tavernadellaberardenga.it</a></p>
            </div>
        </div>
_END;
if ($_POST['doBook']=='Invia') {
$nome = $cognome = $email = $messaggio = '0';
$nome = $_POST[nome];
$cognome = $_POST[cognome];
$email = $_POST[email];
$messaggio = $_POST[messaggio];

if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
$controllo = 0;
} else {
$controllo = 1;
}
if ($nome!=false && $cognome!=false && $email!=false && $messaggio!=false)
{
    if ($controllo!=1)
	{
echo <<<_END

        <div class="mainright" id="scrollbarright">
        <form  action="send.php"  method="post" id="form">

            <div class="formbox">
            <table id="cForm">
                <tr>
                    <td>Nome*</td>
                    <td>$nome</td>
                </tr>
                <tr>
                    <td>Cognome*</td>
                    <td>$cognome</td>
                </tr>
                <tr>
                    <td>Email*</td>
                    <td>$email</td>
                </tr>    
                <tr>
                    <td>Telefono</td>
                    <td>$telefono</td>
                </tr>
                <tr>
                    <td>Richiesta*</td>
                    <td>$messaggio</td>
                </tr>
                <tr>

                <tr>
                    <td colspan="2" align="center">
                    Se i dati inseriti sono corretti conferma la tua richiesta di informazioni.          
                    </td>
                </tr>    
                <tr>
                    <td colspan="2" align="center">
                    <input type="hidden" name="nome" value="$nome" />
                    <input type="hidden" name="cognome" value="$cognome" />
                    <input type="hidden" name="email" value="$email" />
                    <input type="hidden" name="messaggio" value="$messaggio" />
                    <input type="submit" name="doSend" class="btn" value="Invia" />          
                    </td>
                </tr>  
            </table>
            </div>
        </form>


_END;
	}
	else
    {
echo <<<_END
        <div class="mainright" id="scrollbarright">
            <div class="formbox">

<p>Spiacente <span style="color:#db2326">indirizzo email non valido</span>.</p>
<p><a href='javascript:window.history.back();' style='color: #222222;font-weight:bold;'>Torna indietro</a> e inserisci un indirizzo email valido.</p>
</div>
_END;
}
}
else
{
echo <<<_END
        <div class="mainright" id="scrollbarright">
            <div class="formbox">
<p>Non sono stati riempiti tutti i campi obbligatori</p>
<p>I seguenti campi sono <span style="color:#db2326">obbligatori</span>:
<ul class="customlist">
_END;
if ($nome==false) {echo "<li style=\"color: #db2326;\">Nome</li>";}
if ($cognome==false) {echo "<li style=\"color: #db2326;\">Cognome</li>";}
if ($email==false) {echo "<li style=\"color: #db2326;\">Email</li>";}
if ($messaggio==false) {echo "<li style=\"color: #db2326;\">Richiesta</li>";}
echo <<<_END
</ul>
<p><a href='javascript:window.history.back();' style='color: #222222;font-weight:bold;'>Torna indietro</a> per completare correttamente il form.</p>
</div>
_END;
}
}

if ($_POST['doSend']=='Invia') {


$nome = $_POST[nome];
$cognome = $_POST[cognome];
$email = $_POST[email];
$messaggio = $_POST[messaggio];
$headers = "";
$headers .= "From: $email\n";
$headers .= "Reply-To: $email\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=utf-8\n";
$to = "info@tavernadellaberardenga.it";
$subject = "NEW - Richiesta informazioni";
/*$message = $messaggio;*/
$message = "Hai ricevuto una nuova richiesta di informazioni, ecco i dettagli:<br />
Nome: $nome<br />
Cognome: $cognome<br />
E-mail: $email<br />
Messaggio: $messaggio<br />";

mail ($to,$subject,$message,$headers);

echo <<<_END

        <div class="mainright" id="scrollbarright">
            <div class="formbox">
        <p>La Vostra richiesta &egrave; stata inoltrata, riceverete al più presto una risposta. Grazie per averci contattato.</p>
        <p><a href='index.php' style="color:#000000;">Torna alla Pagina iniziale</a></p>
        </div>

_END;
}


?>
    </div>
</div>
</div>
<?php
include_once('footer.php');
?>
