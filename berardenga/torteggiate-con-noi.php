<?php
$pagename = "torteggiate-con-noi";
$pagetitle = "News | Torteggiate con noi";
include_once('header.php');
?>

<div id="main" class="news">
    <div class="opacizeme">
        <h1 class="newsheader">News</h1>
        <div class="newscontainer" id="scrollbarright">
        <div class="bignewsbox">
            <h1>Torteggiate con noi</h1>
            <h5>Lunedì 3 dicembre 2012, 12:30 (<a href="torteggiate-con-noi.php" title="Torteggiate con noi">Link</a>)</h5>
            <p>Alla Taverna si realizzano su commissione torte di ogni genere e anche decorate con pasta di zucchero con soggetti da voi scelti.</p>
            <p>Per qualsiasi info contattateci la nostra pasticcera Francesca sarà lieta di rispondervi e farvi trovare la torta che sognate.</p>
        </div>
        </div>
    </div>
</div>

<?php
include_once('footer.php');
?>