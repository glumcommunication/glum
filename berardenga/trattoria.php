<?php
$pagename = "trattoria";
$pagetitle = "La Trattoria";
include_once('header.php');
?>
<script type="text/javascript" >
function showDiv(div_id) {
	
                        document.getElementById(div_id).style.display = "block";
	
	}
function hideDiv(div_id)
{
	document.getElementById(div_id).style.display = "none";
}
</script>
<div id="main" class="trattoria">
	<div class="opacizeme">
		<div class="mainleft">
            <h1 style="padding-bottom: 0;">La Trattoria</h1>
             <p style="margin:0 0 10px;padding:0;text-align:center;">
                 <a href="#" onClick="showDiv('scrollbarleft');hideDiv('scrollbarleft_en')"><img src="images/it.png" alt="Italian"></a>&emsp;
                 <a href="#" onClick="showDiv('scrollbarleft_en');hideDiv('scrollbarleft')"><img src="images/gb.png" alt="English"></a>
             </p>            
            <div id="scrollbarleft" style="height: 500px;display:block;">
            <p style="padding-top: 0;">La Taverna della Berardenga nasce nel 1986 come bar e tabacchi, l’anno successivo si
            amplia e diventa anche ristorante. Adriana Dragoni gestisce dalla creazione la Taverna e tratta i propri clienti
            come ospiti nella propria casa. Osteria tipica chiantigiana, dove il menù e la carta dei vini sono fortemente legati
            al territorio, con pasta e dolci fatti in casa tutti i giorni. La scelta dei prodotti e la preparazione dei piatti
            cambia a seconda della stagionalità delle materie prime.
            </p>
            <p>Il menù varia ogni giorno, in base ai prodotti che nel mercato sono più freschi, cercando di offrire ai nostri
            clienti una buona varietà di piatti. Cesare cura la sala e la selezione dei vini, mettendo in pratica la sua esperienza
            e professionalità. Francesca e Stefania, figlie di Adriana, seguono la cucina e l’accoglienza sia in sala che al bar.
            Piatti forti della Taverna sono l’Anatra, cucinata sia al forno, in porchetta; sia come sugo nei Pici fatti in casa.
            Famoso è anche il Fritto Misto alla Taverna con carni bianche e verdure miste. Immancabile nella cucina chiantigiana è
            la carne di vitello che trova grande spazio nel menù della Taverna, cotta alla brace, con la classica fiorentina, la
            tagliata, e al forno. Quando la stagione lo permette, potete trovare piatti a base di porcini e tartufi, sempre freschissimi.
            </p>
		</div>
            <div id="scrollbarleft_en" style="height: 500px;display:none;">
            <p style="padding-top: 0;">
                La Taverna della Berardenga has been founded in 1986 as a bar and tobacco, the following year grows and becomes 
                also restaurant. Adriana Dragoni manages the Taverna from his creation and she treats her customers like guests of 
                her home. The Taverna is a Typical osteria of Chiantishine and the menu and the wine list are strongly linked to the 
                territory, with pasta and homemade desserts prepared every day. The choice of products and the preparation of 
                dishes varies depending on the seasonality of raw materials.
            </p>
            <p>
                The menu changes daily, depending on the products that are the fresher in the market, trying to offer our customers a 
                good variety of dishes. Cesare, the sommelier, takes care of the dining room and of wines selection, putting into practice 
                his experience and professionalism. Francesca and Stefania, Adriana’s daughters, take care of the kitchen and the reception of 
                clients. Tipical Taverna’s dishes  are the duck, roasted or stuffed in a tasty ragù in order to enrich the homemade Pici . Famous 
                is also the Taverna’s style Fritto Misto composed by white meats (rabbit and chicken) and mixed vegetables. Inevitable in Chianti 
                cuisine is veal that has a large space in the menu of Taverna, cooked on the grill, like the classic Fiorentina, the Tagliata 
                (thin slices), and baked veal. When the season permits, you can find dishes based on fresh mushrooms and truffles.
            </p>
		</div>
		</div>
		<div class="maincenter">
		 <div class="bigpiccontainer">
		 	<img src="images/trattoria/trattoria_01.jpg" alt="test">
		 </div>
         <div class="maintext">
             <p>Scarica la carta dei vini <a href="doc/cartavini.pdf" alt="Carta dei vini"><img src="images/pdfdownload.png" alt="Download PDF" class="pdfdl" /></a><br>
             Scarica la carta delle birre artigianali <a href="doc/cartabirre.pdf" alt="Carta delle birre artigianali"><img src="images/pdfdownload.png" alt="Download PDF" class="pdfdl" /></a></p>
        </div>
		</div>
		<div class="mainright" id="scrollbarright">
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_01.jpg'); return false;"><img src="images/trattoria/trattoria_01.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_02.jpg'); return false;"><img src="images/trattoria/trattoria_02.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_03.jpg'); return false;"><img src="images/trattoria/trattoria_03.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_04.jpg'); return false;"><img src="images/trattoria/trattoria_04.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_05.jpg'); return false;"><img src="images/trattoria/trattoria_05.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_06.jpg'); return false;"><img src="images/trattoria/trattoria_06.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_07.jpg'); return false;"><img src="images/trattoria/trattoria_07.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_08.jpg'); return false;"><img src="images/trattoria/trattoria_08.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_09.jpg'); return false;"><img src="images/trattoria/trattoria_09.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_10.jpg'); return false;"><img src="images/trattoria/trattoria_10.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_11.jpg'); return false;"><img src="images/trattoria/trattoria_11.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_12.jpg'); return false;"><img src="images/trattoria/trattoria_12.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_13.jpg'); return false;"><img src="images/trattoria/trattoria_13.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_14.jpg'); return false;"><img src="images/trattoria/trattoria_14.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_15.jpg'); return false;"><img src="images/trattoria/trattoria_15.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_16.jpg'); return false;"><img src="images/trattoria/trattoria_16.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_17.jpg'); return false;"><img src="images/trattoria/trattoria_17.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_18.jpg'); return false;"><img src="images/trattoria/trattoria_18.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_21.jpg'); return false;"><img src="images/trattoria/trattoria_21.jpg" alt="test"></a></div>
            <div class="thumb" style="height: 56px;"><a href="" onclick="loadpic('images/trattoria/trattoria_22.jpg'); return false;"><img src="images/trattoria/trattoria_22.jpg" alt="test"></a></div>
            <div class="thumb" style="clear:both;"><a href="" onclick="loadpic('images/trattoria/trattoria_31.jpg'); return false;"><img src="images/trattoria/trattoria_31.jpg" alt="test"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/trattoria/trattoria_32.jpg'); return false;"><img src="images/trattoria/trattoria_32.jpg" alt="test"></a></div>

		</div>
	</div>
</div>

<?php
include_once('footer.php');
?>