<?php
$pagename = "menu-degustazione-al-tartufo";
$pagetitle = "Menù Degustazione al Tartufo";
include_once('header.php');
?>

<div id="main" class="news">
    <div class="opacizeme">
        <h1 class="newsheader">News</h1>
        <div class="newscontainer" id="scrollbarright">
        <div class="bignewsbox">
            <h1>Menù Degustazione al Tartufo</h1>
            <h5>Mercoledì 23 gennaio 2013, 12:00 (<a href="menu-degustazione-al-tartufo.php" title="Menù Degustazione al Tartufo">Link</a>)</h5>
            <p>
            Da oggi e per tutto il mese di febbraio (fino a che il prodotto fresco sarà disponibile), La Taverna vi propone un menù 
            tutto a base di Tartufo bianco Marzuolo…venite a degustare questa delizia del nostro territorio...<br>
            Vi aspettiamo!</p>

            <h2>Menù Degustazione al Tartufo</h2>
				<h2>Tris di antipasti</h2>
				<ul>
					<li>Crostone caldo al lardo e tartufo</li>
					<li>Uovo con tartufo</li>
					<li>Insalatina di carciofi, grana e tartufo</li>
				</ul>
				
				<h2>Il Primo</h2>
				<ul>
					<li>Tagliolini al tartufo</li>
				</ul>
				
				<h2>Il Secondo e il suo contorno</h2>
				
					<li>Tagliata al tartufo</li>
					<li>Spicchi di patate in padella al sale grosso</li>
				
				<h2>Il Dolce</h2>
				<ul>
					<li>Bigné caramellati all’arancio</li>
				</ul>
            <h2>€ 35,00 a persona</h2>
            <p>Il menù comprende ½ L di acqua e ¼ L di vino della casa ma se scegliendo questo menù, deciderete di aprire una bottiglia di vino dalla 
            nostra carta, ve lo sconteremo del 10%.</p>

        </div>
        </div>
    </div>
</div>

<?php
include_once('footer.php');
?>