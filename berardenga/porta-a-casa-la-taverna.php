<?php
$pagename = "porta-a-casa-la-taverna";
$pagetitle = "News | Porta a casa la Taverna";
include_once('header.php');
?>

<div id="main" class="news">
    <div class="opacizeme">
        <h1 class="newsheader">News</h1>
        <div class="newscontainer" id="scrollbarright">
        <div class="bignewsbox">
            <h1>Porta a casa la Taverna</h1>
            <h5>Mercoledì 28 novembre 2012, 12:30 (<a href="porta-a-casa-la-taverna.php" title="Porta a casa la Taverna">Link</a>)</h5>
            <p>
            Ricciarelli, panforte, cavallucci, pasta fresca, cene e pranzi potranno essere acquistati alla Taverna e gustati tranquillamente a casa vostra.
            </p>
            <p>
            Se volete i dolci e la pasta vi consigliamo di ordinarli con leggero anticipo, per i pranzi e le cene basta telefonare o passare da noi per sapere
            il menù del giorno, scegliere e aspettare 10-15 minuti per portare a casa il mangiare.
            </p>
            <p>
            Per prenotare il pranzo di Natale (totale o parziale) da asporto vi preghiamo di prenotarlo qualche giorno prima e sarà pronto il 25 dicembre alle
            12.00 – 12.30.
            </p>
            <p>
            Ricordiamo inoltre che alla Taverna trovate anche una cantina molto fornita di ottimi vini del territorio che potete utilizzare per fare qualche
            regalo o perché no, bere anche nelle vostre tavole per coccolarvi un po’.
            </p>
        </div>
        </div>
    </div>
</div>

<?php
include_once('footer.php');
?>