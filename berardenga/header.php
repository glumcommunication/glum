<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="author" content="Cesare Rinaldi">
<meta name="description" content="<?php echo $pagedesc ?>">
<meta name= "keywords" content= "La Taverna della Berardenga, Castlenuovo Berardenga, pizzeria, bar, panini, pizze, cucina toscana, siena, chianti, carta dei vini, gestione familiare, trattoria" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/loadpic.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>

<!-- mousewheel plugin -->
<script src="js/jquery.mousewheel.min.js"></script>
<!-- custom scrollbars plugin -->
<script src="js/jquery.mCustomScrollbar.js"></script>
<script>
(function($){
    $(document).ready(function(){
        $("#scrollbarleft").mCustomScrollbar({
            scrollButtons:{
                enable:false
            },
            scrollInertia:0,
            advanced:{
                updateOnContentResize:true
            },
        });
    }),
    $(document).ready(function(){
        $("#scrollbarright").mCustomScrollbar({
            scrollButtons:{
                enable:false
            },
            scrollInertia:0,
            advanced:{
                updateOnContentResize:true
            },
        });
    })
})(jQuery);
</script>
<script>
(function($){
    $(document).ready(function(){
        $("#scrollbarleft_en").mCustomScrollbar({
            scrollButtons:{
                enable:false
            },
            scrollInertia:0,
            advanced:{
                updateOnContentResize:true
            },
        });
    }),
    $(document).ready(function(){
        $("#scrollbarright_en").mCustomScrollbar({
            scrollButtons:{
                enable:false
            },
            scrollInertia:0,
            advanced:{
                updateOnContentResize:true
            },
        });
    })
})(jQuery);
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37220714-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<title>La Taverna della Berardenga | <? echo $pagetitle ?></title>
</head>
<body>
<div id="wrapper">

<!-- #################### -->

<div id="header">
    <div id="headerimage">
        <!--<img src="images/tappo.png" alt="">-->
    </div>
    <div id="name">
        <h1><a href="index.php" alt="Home" title="Home">La Taverna della Berardenga</a></h1>
        <h2><a href="index.php" alt="Home" title="Home"><span style="color:#762825;">Specialità Toscane</span></a></h2>
    <div id="nav">
        <ul>
            <li><a href="bar.php">Il Bar</a></li>
            <li><a href="trattoria.php">La Trattoria</a></li>
            <li><a href="foto.php">La Cucina</a></li>
            <li><a href="news.php">News</a></li>
            <li><a href="contatti.php">Contatti</a></li>
        </ul>
    </div>
    </div>
</div>

<!-- #################### -->