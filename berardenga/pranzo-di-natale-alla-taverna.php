<?php
$pagename = "pranzo-di-natale-alla-taverna";
$pagetitle = "Pranzo di Natale alla Taverna";
include_once('header.php');
?>

<div id="main" class="news">
    <div class="opacizeme">
        <h1 class="newsheader">News</h1>
        <div class="newscontainer" id="scrollbarright">
        <div class="bignewsbox">
            <h1>Pranzo di Natale alla Taverna</h1>
            <h5>Domenica 25 novembre 2012, 12:30 (<a href="pranzo-di-natale-alla-taverna.php" title="Pranzo di Natale alla Taverna">Link</a>)</h5>
            <p>
            Se volete stare da noi per il tradizionale pranzo natalizio ecco il menù e vi ricordiamo di prenotare quanto prima poiché i posti a disposizione sono limitati.</p>

            <h2>Menù</h2>
            <ul>
                <li>Torta salata pecorino e pere</li>
                <li>Crostino burro e acciuga</li>
                <li>Tonno tiepido del Chianti con fagioli cannellini cotti nel forno a legna</li>
                <li>Crostino nero di polenta fritta</li>
                <li>Crostone al cavolo nero e lardo</li>
                <li>Prosciutto toscano</li>
                <li>Carpaccio di vitello con insalatina di carciofi freschi</li>
                <li>Frittata in trippa</li>
            </ul>

            <h2>Primi piatti</h2>
            <ul>
                <li>Pappardelle fatte in casa alla lepre</li>
                <li>Tortelloni di zucca gialla fatti in casa con scaglie di pecorino, pepe e olio extravergine d'oliva</li>
            </ul>

            <h2>Secondi piatti</h2>
            <ul>
                <li>Stinco di maiale al forno con patate risaltate in padella</li>
                <li>Cappone in umido con sformato di gobbi</li>
            </ul>
            <h2>Dolci</h2>
            <ul>
                <ul>Selezione di dolci natalizi fatti in casa (panforte, ricciarelli, cavallucci)</ul>
                <ul>Torta ricotta e pere</ul>
            </ul>
            <p>
            Vino dolce<br>
            Caffè Amari Grappe<br>
            <br>
            Acqua<br>
            <br>
            Vino:<br>
            Chianti Classico d.o.c.g. Il Palei 2007 – Fattoria Villa a Sesta
            </p>
            <h2>€ 35,00 a persona</h2>

        </div>
        </div>
    </div>
</div>

<?php
include_once('footer.php');
?>