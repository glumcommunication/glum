<?php
$pagename = "bar";
$pagetitle = "Il Bar";
include_once('header.php');
?>
<script type="text/javascript" >
function showDiv(div_id) {
	
                        document.getElementById(div_id).style.display = "block";
	
	}
function hideDiv(div_id)
{
	document.getElementById(div_id).style.display = "none";
}
</script>
<div id="main" class="bar">
	<div class="opacizeme">
		<div class="mainleft">
			<h1 style="padding-bottom: 0;">Il Bar</h1>
             <p style="margin:0 0 10px;padding:0;text-align:center;">
                 <a href="#" onClick="showDiv('scrollbarleft');hideDiv('scrollbarleft_en')"><img src="images/it.png" alt="Italian"></a>&emsp;
                 <a href="#" onClick="showDiv('scrollbarleft_en');hideDiv('scrollbarleft')"><img src="images/gb.png" alt="English"></a>
             </p>
            <div id="scrollbarleft" style="height: 500px;display:block;">
            <p style="padding-top: 0;">
            All'ingresso della Taverna si trova il locale Bar dove è possibile trovare anche la rivendita di Tabacchi (Riv. 14).
            Caffè, cappuccino, birra, panini, caramelle etc...  si possono trovare dalle 6.00 del mattino fino a tarda notte.
            </p>
            <p>
            Il bancone e i tavoli sono realizzati artigianalmente con tappi di sughero di bottiglie di vino stappate.
            </p>
            <p>
            Il bar ospita clienti che consumano la colazione o la merenda ma anche accese partite di carte tipiche della campagna Toscana.
            </p>
            <p>
            Tutti i giorni intorno alle ore 15 viene sfornata una gustosissima pizza. Dolci fatti in casa trovano sempre
            spazio nel banco insieme a ottime paste fresche di pasticceria.
            </p>
            </div>
            <div id="scrollbarleft_en" style="height: 500px;display:none;">
            <p style="padding-top: 0;">
                At the entrance of the Taverna you enter in the Bar where you can find also the resale of tobacco (Rev. 14). Coffee, 
                cappuccino, beers, sandwiches, candies, etc. can be found from 6:00 a.m. until late at night.
            </p>
            <p>
                The counter and the tables are handcrafted with corks from uncorked wine bottles.
            </p>
            <p>
                Our customers can have breakfast or a snack and sometimes you can find some people who play cards…a typical scene of Tuscan life.
            </p>
            <p>
                Every day , at about three p.m., a fresh pizza is taken out from the oven and homemade cakes and fresh pastry have always an honour place in the cabinet.
            </p>
            </div>                        
		</div>
		<div class="maincenter">
    	 <div class="bigpiccontainer">
		 	<img src="images/bar/bar_01.jpg" alt="bar">
		 </div>
         <div class="maintext"">
             <p>Quotazioni Sisal <a href="http://www.sisal.it/online/Scommesse-Matchpoint/Downloads" alt="Sisal"><img src="images/pdfdownload.png" alt="Download PDF" class="pdfdl" /></a></p>
        </div>
		</div>
    	<div class="mainright" id="scrollbarright">
            <div class="thumb"><a href="" onclick="loadpic('images/bar/bar_01.jpg'); return false;"><img src="images/bar/bar_01.jpg" alt="bar"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/bar/bar_02.jpg'); return false;"><img src="images/bar/bar_02.jpg" alt="bar"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/bar/bar_03.jpg'); return false;"><img src="images/bar/bar_03.jpg" alt="bar"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/bar/bar_04.jpg'); return false;"><img src="images/bar/bar_04.jpg" alt="bar"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/bar/bar_05.jpg'); return false;"><img src="images/bar/bar_05.jpg" alt="bar"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/bar/bar_07.jpg'); return false;"><img src="images/bar/bar_07.jpg" alt="bar"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/bar/bar_08.jpg'); return false;"><img src="images/bar/bar_08.jpg" alt="bar"></a></div>
            <div class="thumb" style="height: 74px;"><a href="" onclick="loadpic('images/bar/bar_06.jpg'); return false;"><img src="images/bar/bar_06.jpg" alt="bar"></a></div>
		</div>
	</div>
</div>

<?php
include_once('footer.php');
?>