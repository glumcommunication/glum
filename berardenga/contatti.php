<?php
$pagename = "contatti";
$pagetitle = "Contatti";
include_once('header.php');
?>
<script type="text/javascript" >
function showDiv(div_id) {
	
                        document.getElementById(div_id).style.display = "block";
	
	}
function hideDiv(div_id)
{
	document.getElementById(div_id).style.display = "none";
}
</script>
<div id="main" class="contatti">
    <div class="opacizeme">
        <div class="mainleft">
            <h1 style="padding-bottom: 0;">Contatti</h1>
             <p style="margin:0 0 10px;padding:0;text-align:center;">
                 <a href="#" onClick="showDiv('contactbox');hideDiv('contactbox_en')"><img src="images/it.png" alt="Italian"></a>&emsp;
                 <a href="#" onClick="showDiv('contactbox_en');hideDiv('contactbox')"><img src="images/gb.png" alt="English"></a>
             </p>                
            <p style="padding-top: 0;">
            Via del Chianti, 70 - 53019 - Castelnuovo Berardenga (SI)
            </p>
            <div id="contactbox" style="display:block;">
            <p>
            Sulla Siena-Bettolle uscire per Castelnuovo Berardenga e entrare nel paese direzione San Gusmè.<br>
            In via del Chianti, poco prima della fine di Castelnuovo, c'è un ampio parcheggio a pochi metri dal nostro ristorante.
            </p>
            <p>
            Se si arriva dal Chianti appena entrati nel centro abitato la Taverna si trova subito sulla sinistra.
            </p>
            </div>
            <div id="contactbox_en" style="display:none;">
            <p>
                From Siena - Exit the Siena-Bettolle road (signs for A1 and Arezzo or Perugia) at Castelnuovo Berardenga and enter 
                in the town following the signs for San Gusmé.<br>
                In Via del Chianti, just before the end of Castelnuovo, there is a large parking just near from our restaurant.
             </p>
            <p>
                From the Chianti - As you enter the town of Castelnuovo Berardenga the Taverna is the first building on the left, and on the right 
                there is a large parking.
            </p>
            </div>            
            <p style="font-size: 1.5em;">
            <img src="images/telefono.png" alt="Telefona!" style="vertical-align: middle;"> 0577 355547
            </p>
        </div>
        <div class="maincenter">
            <div class="bigpiccontainer" style="height:510px;">
                <div style="background-color: #F9F2E3;border: 5px solid #732726;box-shadow: 0 0 10px #000000;">
                <iframe width="555" height="476" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=taverna+della+berardenga&amp;aq=&amp;sll=41.29085,12.71216&amp;sspn=29.441464,67.631836&amp;t=h&amp;ie=UTF8&amp;hq=taverna+della+berardenga&amp;hnear=&amp;cid=6484612953957846267&amp;ll=43.349633,11.504719&amp;spn=0.001794,0.002999&amp;z=18&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="https://maps.google.it/maps?f=q&amp;source=embed&amp;hl=it&amp;geocode=&amp;q=taverna+della+berardenga&amp;aq=&amp;sll=41.29085,12.71216&amp;sspn=29.441464,67.631836&amp;t=h&amp;ie=UTF8&amp;hq=taverna+della+berardenga&amp;hnear=&amp;cid=6484612953957846267&amp;ll=43.349633,11.504719&amp;spn=0.001794,0.002999&amp;z=18&amp;iwloc=A" style="background-color: transparent;color:#0000FF;text-align:left">Visualizzazione ingrandita della mappa</a></small>
                </div>
            </div>
            <div class="maintext">
                <p><a href="mailto:info@tavernadellaberardenga.it">info@tavernadellaberardenga.it</a></p>
            </div>
        </div>
        <div class="mainright" id="scrollbarright">
        <form  action="send.php"  method="post" id="form">

            <div class="formbox">
            <table id="cForm">
                <tr>
                    <td>Nome*</td>
                    <td><input type="text" name="nome"  value="" id="q1" maxlength="20" size="30" required="required" style="width:150px;height:20px;border-radius:5px;margin-bottom:10px;padding: 5px 3px 5px 2px;"/></td>
                </tr>
                <tr>
                    <td>Cognome*</td>
                    <td><input type="text" name="cognome"  value="" id="q2" maxlength="20" size="30" required="required" style="width:150px;height:20px;border-radius:5px;margin-bottom:10px;padding: 5px 3px 5px 2px;"/></td>
                </tr>
                <tr>
                    <td>Email*</td>
                    <td><input type="text" name="email"  value="" id="q3" maxlength="20" size="30" required="required" style="width:150px;height:20px;border-radius:5px;margin-bottom:10px;padding: 5px 3px 5px 2px;"/></td>
                </tr>

                <tr>
                    <td>Richiesta*</td>
                    <td><textarea cols="25" rows="3" name="messaggio" class="text" id="q5" style="width:150px;height:60px;border-radius:5px;margin-bottom:10px;padding: 5px 3px 5px 2px;"></textarea></td>
                </tr>
                <tr>
                    <td></td><td style="font-size:0.7em;text-align: center;">* Campi obbligatori.</td>
                </tr>
                <tr>
                    <td></td>
                    <td style="text-align:center;"><input type="submit" name="doBook" class="btn" value="Invia" style="border:0;font-family: 'Julius Sans One', sans-serif;" /></td>
                </tr>
            </table>
            </div>
        </form>
        </div>
    </div>
</div>

<?php
include_once('footer.php');
?>