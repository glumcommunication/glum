<!-- #################### -->

<div id="footer">
    <div style="width:16.6666667%;margin-right:1.25%;float:left; opacity: 1;">
        <div id="CDSWIDWRM" style="margin:0; padding:0; width:100%; border:none; background-color:#589442; overflow:hidden; height:130px; position:relative; ">
            <div style="padding: 5px 11px;background-color:#fff;  border: 1px solid #589442; position:relative; ">
                <div style="margin:3px 0 3px; padding:0; overflow:hidden; border-bottom:1px solid #CCCCCC; border-top:none; border-left:none; border-right:none; text-align:center;">
                <a target="_blank" style="border:none; background:transparent;" href="http://www.tripadvisor.it"> <script type="text/javascript" src="http://www.jscache.com/weimg?itype=img2/branding/medium-logo-12096-2.png&amp;lang=it"></script></a>
            </div>
            <div style="margin:0; padding:3px 0 6px; border:none; font:bold 12px Arial,Verdana,Tahoma,'Bitstream Vera Sans',sans-serif; color:#2c2c2c; text-align:center; line-height:normal; letter-spacing:0">
            Review <a target="_blank" style="margin:0;padding:0;border:none;background:transparent;text-decoration:none;outline:none;font-weight:bold;font-size:14px;font-family:Arial,Verdana,Tahoma;color:#2c2c2c;text-align:center;line-height:normal;letter-spacing:0" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" href="http://www.tripadvisor.it/Restaurant_Review-g652035-d2020424-Reviews-La_Taverna_della_Berardenga-Castelnuovo_Berardenga_Tuscany.html">La Taverna della Berardenga</a>
            </div>
            <div style="margin:0px 0 0 0; padding:6px 0; border:none; background-color:none; white-space:nowrap;  text-align:center; margin-left:auto; margin-right:auto; position:relative; ">
            <input type="button" onclick="window.open('http://www.tripadvisor.it/UserReview-g652035-d2020424-m12096-La_Taverna_della_Berardenga-Castelnuovo_Berardenga_Tuscany.html')" style="border:1px solid #EA9523; border:active:border:none;background:url(http://c1.tacdn.com/img2/sprites/yellow-button.png) 0 0 repeat-x #EA9523;  cursor:pointer; text-decoration:none; outline:none; font: bold 13px Arial,Tahoma,'Bitstream Vera Sans',sans-serif; color:#000; letter-spacing:0; vertical-align:center; text-align:center; width:auto; height:27px; position:relative;" value="Write Review"/> </div>
            </div>
        </div>
    </div>
    <div class="logos">
        <div class="singlelogo"><a href="http://www.2spaghi.it/ristoranti/toscana/si/castelnuovo-berardenga/trattoria-della-berardenga/" alt="2 Spaghi" target="_blank"><img src="images/logos/2spaghi_logo.png" alt=""></a></div>
        <div class="singlelogo"><a href="http://livre.fnac.com/a3777894/Philippe-Gloaguen-Guide-du-Routard-Toscane-Ombrie?orign=1&Origin=ROUTARD_GUIDE#ficheDt" alt="Routard" target="_blank"><img src="images/logos/routard_logo.png" alt=""></a></div>
        <div class="singlelogo"><a href="http://www.alice.tv/ristoranti/trattoria-della-berardenga_castelnuovo-b" alt="Alice Tv"><img src="images/logos/alice_logo.png" alt="Alice Tv" target="_blank"></a></div>
        <div class="singlelogo"><a href="#" alt="Original Italy"><img src="images/logos/originalitaly.gif" alt="Original Italy" target="_blank"></a></div>
    </div>
    <div class="info">
        <span style="font-size:1.1em">
        La Taverna della Berardenga
        </span>
        <br>
        Via del Chianti 70/74<br>
        53019 Castelnuovo Berardenga (SI)<br>
        Partita Iva 00386850523<br>
        <div style="width:48px;margin:0;padding:0;margin:7px auto;" id="facebooklogo">
            <a href="http://www.facebook.com/pages/La-Taverna-della-Berardenga/155263227409" alt="Facebook"><img src="images/facebook_logo.png" alt=""></a>
        </div>
    </div>
    <div class="services">
        <div class="singlelogo" style="width: 38.4545455%;margin: 0 4.5454545% 15px 0;">
            <a href="http://www.sisal.it/" alt="Sisal" target="_blank"><img src="images/logos/sisal_logo.png" alt="Sisal"></a>
        </div>
        <div class="singlelogo" style="width: 23.2727273%;margin: 0 4.5454545% 15px 0; opacity: 1;">
            <img src="images/logos/free_wi_fi_spot.png" alt="WiFi">
        </div>
        <div class="singlelogo" style="width: 28.2727273%;margin: 0 0 15px 0; opacity: 1;">
            <a href="http://www.girogustando.tv/" alt="Girogustando" target="_blank"><img src="images/logos/girogustando.png" alt="WiFi"></a>
        </div>        
    </div>    
</div>

<!-- #################### -->

</div>
<p style="text-align: center; font-size:0.75em; color: #000000;">Realizzato da&emsp; <a href="http://www.idpromoter.it/"><img src="images/idpromoter.png" width="87" height="21" alt="idpromoter"></a></p>
</body>
</html>