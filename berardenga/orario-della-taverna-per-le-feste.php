<?php
$pagename = "orario-della-taverna-per-le-feste";
$pagetitle = "Orario della Taverna per le feste";
include_once('header.php');
?>

<div id="main" class="news">
    <div class="opacizeme">
        <h1 class="newsheader">News</h1>
        <div class="newscontainer" id="scrollbarright">
        <div class="bignewsbox">
            <h1>Orario della Taverna per le feste</h1>
            <h5>Sabato 1 dicembre 2012, 12:30 (<a href="orario-della-taverna-per-le-feste.php" title="Orario della Taverna per le feste">Link</a>)</h5>
            <p>Nei giorni delle festività la Taverna effettuerà il seguente orario:<br>
            <br>
            Lunedì 24 Dicembre APERTI dalle 6.00 alle 00.00<br>
            Martedì 25 Dicembre APERTI dalle 6.00 alle 17.00<br>
            Mercoledì 26 Dicembre APERTI dalle 6.00 alle 17.00<br>
            Giovedì 27 APERTI dalle 6.00 alle 00.00<br>
            Venerdì 28 APERTI dalle 6.00 alle 00.00<br>
            Sabato 29 APERTI dalle 6.00 alle 00.00<br>
            Domenica 30 APERTI dalle 6.00 alle 17.00<br>
            Lunedì 31 CHIUSI<br>
            Martedì 1 CHIUSI<br>
            Da Mercoledì 2 in poi APERTI dalle 6.00 alle 00.00
            </p>
        </div>

        </div>
    </div>
</div>

<?php
include_once('footer.php');
?>