<?php
$pagename = "siamo-on-line";
$pagetitle = "News | Siamo on line!";
include_once('header.php');
?>

<div id="main" class="news">
    <div class="opacizeme">
        <h1 class="newsheader">News</h1>
        <div class="newscontainer" id="scrollbarright">
        <div class="bignewsbox">
            <h1>Siamo on line!</h1>
            <h5>Venerdì 21 dicembre 2012, 12:30 (<a href="siamo-on-line.php" title="Siamo on line">Link</a>)</h5>
            <p>ECCOCI!!!!! Finalmente siamo on line con il nostro nuovo sito internet curato in ogni dettaglio da noi e dall'agenzia IdPromoter.
            Sperando di farvi cosa gradita e che sopratutto vi piaccia vogliamo presentarci con il nostro vestito migliore. Fotografie accattivanti
            dei nostri migliori prodotti, delle nostre sale e soprattutto del nostro bellissimo territorio. Un modo nuovo di proporre chi siamo,
            quello che facciamo e ciò che vogliamo sempre con un occhio verso di voi che tramite il sito potrete vedere la Taverna da dentro e
            chiederci qualsiasi informazione con un CLIC.</p>
            <p>Solo per i nostri clienti ONLINE dal 26 dicembre al 15 gennaio a chi porta questa pagina stampata sarà fatto uno sconto del 10%
            su tutti i pranzi e tutte le cene.</p>
        </div>
        </div>
    </div>
</div>

<?php
include_once('footer.php');
?>