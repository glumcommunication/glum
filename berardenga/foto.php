<?php
$pagename = "foto";
$pagetitle = "La Cucina";
include_once('header.php');
?>

<div id="main" class="foto">
    <div class="opacizeme">
        <div class="maincenter" style="width:75%;">
            <div class="bigpiccontainer" id="dishcontainer">
                <img src="images/piatti/piatti_01.jpg" alt="bar">
            </div>
            <div class="maintext" style="top: -50px;">

            </div>
        </div>
        <div class="mainright" id="scrollbarright">
            <div class="thumb"><a href="" onclick="loadpic('images/piatti/piatti_01.jpg'); return false;"><img src="images/piatti/piatti_01.jpg" alt="Baccalà al pomodoro" title="Baccalà al pomodoro"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/piatti/piatti_02.jpg'); return false;"><img src="images/piatti/piatti_02.jpg" alt="Cappella di porcino alla brace" title="Cappella di porcino alla brace"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/piatti/piatti_03.jpg'); return false;"><img src="images/piatti/piatti_03.jpg" alt="Cinghiale alla cacciatora" title="Cinghiale alla cacciatora"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/piatti/piatti_04.jpg'); return false;"><img src="images/piatti/piatti_04.jpg" alt="Coniglio in carciofaia" title="Coniglio in carciofaia"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/piatti/piatti_05.jpg'); return false;"><img src="images/piatti/piatti_05.jpg" alt="Costole di agnello allo scottadito" title="Costole di agnello allo scottadito"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/piatti/piatti_06.jpg'); return false;"><img src="images/piatti/piatti_06.jpg" alt="Cotolette d'agnello fritte con carciofi" title="Cotolette d'agnello fritte con carciofi"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/piatti/piatti_07.jpg'); return false;"><img src="images/piatti/piatti_07.jpg" alt="Crostini misti" title="Crostini misti"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/piatti/piatti_08.jpg'); return false;"><img src="images/piatti/piatti_08.jpg" alt="Fritto misto della Taverna" title="Fritto misto della Taverna"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/piatti/piatti_09.jpg'); return false;"><img src="images/piatti/piatti_09.jpg" alt="Ossobuco" title="Ossobuco"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/piatti/piatti_10.jpg'); return false;"><img src="images/piatti/piatti_10.jpg" alt="Pici all'anatra" title="Pici all'anatra"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/piatti/piatti_11.jpg'); return false;"><img src="images/piatti/piatti_11.jpg" alt="Porcini fritti" title="Porcini fritti"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/piatti/piatti_12.jpg'); return false;"><img src="images/piatti/piatti_12.jpg" alt="Reginette fatte in casa al sugo di cinghiale" title="Reginette fatte in casa al sugo di cinghiale"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/piatti/piatti_13.jpg'); return false;"><img src="images/piatti/piatti_13.jpg" alt="Ribollita" title="Ribollita"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/piatti/piatti_14.jpg'); return false;"><img src="images/piatti/piatti_14.jpg" alt="Tacchino allo zafferano e pomodorini" title="Tacchino allo zafferano e pomodorini"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/piatti/piatti_15.jpg'); return false;"><img src="images/piatti/piatti_15.jpg" alt="Tagliata alla rucola" title="Tagliata alla rucola"></a></div>
            <div class="thumb"><a href="" onclick="loadpic('images/piatti/piatti_16.jpg'); return false;"><img src="images/piatti/piatti_16.jpg" alt="Tagliolini al sugo di funghi" title="Tagliolini al sugo di funghi"></a></div>
        </div>
    </div>
</div>

<?php
include_once('footer.php');
?>