<?php
$id = $_GET[id];
$pagename = 'read';
$pagetitle = 'Home';
$pagedesc = 'Fuoriskema è un parrucchiere ed un centro estetico a Castellina Scalo. Da noi potete trovare uno staff solare e preparato, pronto a prendersi cura di voi e a soddisfare ogni vostra esigenza.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
<?php
$db_server = mysql_connect($db_hostname, $db_username, $db_password);
if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());

mysql_select_db($db_database, $db_server)
	or die("Unable to select database: " . mysql_error());
$sql = "SELECT * FROM fuoriskema WHERE category='01' ORDER BY Data DESC,Ora DESC";
$result = mysql_query($sql);
if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);

?>
<div class="bigcont" id="news">
    <div class="bleather topp">
        <div class="container">
            <h1 class="bigtitle">NEWS</h1>
            <div class="row">
<?php
for ($j = 0 ; $j < $rows ; ++$j) {
	$row = mysql_fetch_row($result);
    $title = stripslashes($row[0]);
    $body = stripslashes($row[1]);
    $body = strip_tags($body, '<p><a>');
    $datasql = "SELECT DATE_FORMAT(Data, '%d/%m/%Y')  as data FROM fuoriskema WHERE id='$row[7]'";
    $rowdata = mysql_fetch_assoc(mysql_query($datasql));

?>

                <div class="col-sm-4 newsitem">
                    <h1><a href="read.php?id=<?php echo $row[7]; ?>" title="Leggi"><?php echo $title; ?></a></h1>
                    <h5><?php echo $rowdata[data]; ?></h5>
                    <?php echo $body; ?>
                </div>
<?php
}
?>
            </div>
            <div id="allnews" style="text-align:left;"><a href="index.php#news"><span class="glyphicon glyphicon-arrow-left"></span>&emsp;Torna indietro</a></div>
        </div>
        
    </div>
</div>
<div id="push">

</div>
</div>    
<div id="footer">
    <div class="container">
        <div class="col-sm-12">
            <p>&copy; <?php echo date(Y); ?> Fuoriskema Snc - Via G. di Vittorio 5/9 - Castellina Scalo  (SI) - Telefono: +39 0577 304390 - Email: <a href="mailto:fuoriskemasnc@alice.it">fuoriskemasnc@alice.it</a> - P. Iva: 00840040521 | <a href="/admin/">Area amministrativa</a></p>
        </div>
        <div class="col-sm-12">
            <p>Crafted by <a href="http://www.glumcommunication.it/" title="GLuM Communication" target="_blank">GLuM</a></p>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>
