<?php
$id = $_GET[id];
$pagename = 'read';
$pagetitle = 'Home';
$pagedesc = 'Fuoriskema è un parrucchiere ed un centro estetico a Castellina Scalo. Da noi potete trovare uno staff solare e preparato, pronto a prendersi cura di voi e a soddisfare ogni vostra esigenza.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
<?php
$db_server = mysql_connect($db_hostname, $db_username, $db_password);
if (!$db_server) die("Unable to connect to MySQL: " . mysql_error());

mysql_select_db($db_database, $db_server)
	or die("Unable to select database: " . mysql_error());
$sql = "SELECT * FROM fuoriskema WHERE Id=$id";
$result = mysql_query($sql);
if (!$result) die ("Database access failed: " . mysql_error());

?>
            
<?php

	$row = mysql_fetch_row($result);
    $title = stripslashes($row[0]);
    $body = stripslashes($row[1]);
    //$body = strip_tags($body, '<p><a>');
    $datasql = "SELECT DATE_FORMAT(Data, '%d/%m/%Y')  as data FROM fuoriskema WHERE id='$row[7]'";
    $rowdata = mysql_fetch_assoc(mysql_query($datasql));

?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="bigcont" id="news">
    <div class="bleather topp">
        <div class="container">
            <h1 class="singleitem"><?php echo $title; ?></h1>    
            <div class="col-sm-12" style="border-bottom: 2px solid white;margin-bottom: 15px;">
                        
                        <div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-action="recommend"></div>
        <a href="https://twitter.com/share" class="twitter-share-button" data-lang="it" data-hashtags="fuoriskema">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        <div class="g-plusone" data-size="medium"></div>
                    </div>
            <div class="col-sm-12 singleitem ">
                
                <h5 style="padding:0 15px 5px 0; float:left; margin-bottom:15px;font-family: 'Quattrocento Sans', sans-serif;font-size:0.875em;"><?php echo $rowdata[data]; ?></h5>
                <div style="clear:both;">
                    <?php echo $body; ?>
                </div>
            </div>
<?php

?>
        </div>

    </div>
</div>
<div id="push">

</div>
</div>    
<div id="footer">
    <div class="container">
        <div class="col-sm-12">
            <p>&copy; <?php echo date(Y); ?> Fuoriskema Snc - Via G. di Vittorio 5/9 - Castellina Scalo  (SI) - Telefono: +39 0577 304390 - Email: <a href="mailto:fuoriskemasnc@alice.it">fuoriskemasnc@alice.it</a> - P. Iva: 00840040521 | <a href="/admin/">Area amministrativa</a></p>
        </div>
        <div class="col-sm-12">
            <p>Crafted by <a href="http://www.glumcommunication.it/" title="GLuM Communication" target="_blank">GLuM</a></p>
        </div>
    </div>
</div>
<script type="text/javascript">
  window.___gcfg = {lang: 'it'};

  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
<?php
include_once 'footer.php';
?>
