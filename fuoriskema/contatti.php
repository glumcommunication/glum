<div class="bigcont" id="contatti">
    
    <div class="bleather topp" style="padding-bottom:90px;">
        <div class="container">
            <h1 class="bigtitle">CONTATTI</h1>
            <div class="row centerize">
                <img src="img/parrucchieri2.png" alt="Parrucchieri">
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="one">
                        <p>Via G. Di Vittorio 9<br>Castellina Scalo (SI)</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="two">
                        <p>+39 0577 304390</p>        
                    </div>
                </div>
                <div class="col-sm-4" >
                    <div class="three">
                        <p><a href="mailto:fuoriskemasnc@alice.it">fuoriskemasnc@alice.it</a></p>    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wleather" style="padding-bottom:128px;padding-top:90px;">
        <div class="container">
            <div class="row centerize">
                <img src="img/estetica.png" alt="Estetica">
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="four">
                        <p>Via G. Di Vittorio 5<br>Castellina Scalo (SI)</p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="five">
                        <p>+39 0577 304252</p>        
                    </div>
                </div>
                <div class="col-sm-4" >
                    <div class="six">
                        <p><a href="mailto:fuoriskemasnc@alice.it">fuoriskemasnc@alice.it</a></p>    
                    </div>
                </div>
            </div>
            <div class="row" style="padding-top:30px;">
                <p><a href="https://www.facebook.com/fuoriskemacastellina" alt="Pagine Facebook"><img src="img/fb.png"></a></p>
            </div>
        </div>
    </div>
    <div id="footer">
        <div class="container">
            <div class="col-sm-12">
                <p>&copy; <?php echo date(Y); ?> Fuoriskema Snc - Via G. di Vittorio 5/9 - Castellina Scalo  (SI) - Telefono: +39 0577 304390 - Email: <a href="mailto:fuoriskemasnc@alice.it">fuoriskemasnc@alice.it</a> - P. Iva: 00840040521 | <a href="/admin/">Area amministrativa</a></p>
            </div>
            <div class="col-sm-12">
                <p>Crafted by <a href="http://www.glumcommunication.it/" title="GLuM Communication" target="_blank">GLuM Communication</a></p>
            </div>
        </div>
    </div>
</div>
