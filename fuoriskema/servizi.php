<div class="bigcont" id="servizi">
    <div class="bleather topp">
        <div class="container">
            <h1 class="bigtitle">Servizi</h1>
            <div class="row service">
                <div class="col-sm-4">
                    <div class="col-sm-5">
                        <img src="img/serv/taglio.png" alt="Taglio" class="img-circle">
                    </div>
                    
                    <div class="col-sm-7">
                        <h1>TAGLIO</h1>
                        <p>
                            Taglio uomo e donna
                        </p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="col-sm-5">
                        <img src="img/serv/piega3.png" alt="Piega" class="img-circle">
                    </div>
                    
                    <div class="col-sm-7">
                        <h1>PIEGA E ACCONCIATURE</h1>
                        <p>
                            Acconciatura sposa<br>Acconciatura sera
                        </p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="col-sm-5">
                        <img src="img/serv/stiraggio.png" alt="Permanente e stiraggio" class="img-circle">
                    </div>
                    
                    <div class="col-sm-7">
                        <h1>PERMANENTE E STIRAGGIO</h1>
                        <p>
                            Ondulazione<br>Permanente<br>Relax cheratina<br>Stiraggio<br>Kerato Reforming
                        </p>
                    </div>
                </div>
            </div>
            <div class="row service">
                <div class="col-sm-4">
                    <div class="col-sm-5">
                        <img src="img/serv/colore2.png" alt="Colore" class="img-circle">
                    </div>
                    
                    <div class="col-sm-7">
                        <h1>COLORE</h1>
                        <p>
                            Colore senza ammoniaca<br>Colpi di sole<br>Colorazioni moda personalizzate
                        </p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="col-sm-5">
                        <img src="img/serv/consulenza.png" alt="Consulenza" class="img-circle">
                    </div>
                    
                    <div class="col-sm-7">
                        <h1>CONSULENZA</h1>
                        <p>
                            Consulenza d'immagine<br>Consulenza tricologica
                        </p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="col-sm-5">
                        <img src="img/serv/trattamenti-cute.png" alt="Trattamenti" class="img-circle">
                    </div>
                    
                    <div class="col-sm-7">
                        <h1>TRATTAMENTI</h1>
                        <p>
                            Trattamenti cute<br>Trattamenti capelli
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wleather">
        <div class="container">
            <div class="row service">
                <div class="col-sm-4">
                    <div class="col-sm-5">
                        <img src="img/serv/trattamenti-viso.png" alt="Trattamenti viso" class="img-circle">
                    </div>
                    
                    <div class="col-sm-7">
                        <h1>TRATTAMENTI VISO</h1>
                        <p>
                            Pulizia viso<br>Trattamenti viso personalizzati con prodotti naturali e nuove biotecnologie<br>
                            Trattamento esfoliante con ultrasuoni<br>Trattamento ossigenante, rassodante, antirughe con 
                            radiofrequenza + infrarossi
                        </p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="col-sm-5">
                        <img src="img/serv/trattamenti-corpo.png" alt="Trattamenti corpo" class="img-circle">
                    </div>
                    
                    <div class="col-sm-7">
                        <h1>TRATTAMENTI CORPO</h1>
                        <p>
                            Greco-Romano<br>Linfodrenaggio Cranio sacrale<br>Hammam Relax distensivo<br>
                            Pressoterapia<br>Modellamento con radiofrequenza + infrarossi<br>Trattamenti rassodanti, 
                            drenanti, anticelullite<br>Trattamenti esfolianti e nutrienti 
                        </p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="col-sm-5">
                        <img src="img/serv/depilazione.png" alt="Depilazione" class="img-circle">
                    </div>
                    
                    <div class="col-sm-7">
                        <h1>EPILAZIONE</h1>
                        <p>
                            Elettrodepilazione ad ago<br>Epilazione tradizionale con cera al biossido di titanio<br>
                            Epilazione a sfoltimento progressivo con resina naturale<br>Trattamento peeling pre depilazione
                        </p>
                    </div>
                </div>
            </div>
            <div class="row service">
                <div class="col-sm-4">
                    <div class="col-sm-5">
                        <img src="img/serv/manicure.png" alt="Manicure e pedicure" class="img-circle">
                    </div>
                    
                    <div class="col-sm-7">
                        <h1>MANICURE E PEDICURE</h1>
                        <p>
                            Smalto semipermanente
                        </p>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="col-sm-5">
                        <img src="img/serv/solarium.png" alt="Solarium" class="img-circle">
                    </div>
                    
                    <div class="col-sm-7">
                        <h1>DOCCIA SOLARIUM</h1>
                        <p>
                            Conforme alle nuove normative riguardo la salute della pelle
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>