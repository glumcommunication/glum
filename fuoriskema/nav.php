<div id="header">
    <div class="container">
        <div class="row">
            <div id="logo" class="col-sm-3 col-xs-9">
                <a href="/" id="logourl">                
                    <img src="img/fslogo.png">
                </a>
            </div>
            <div class="col-sm-9">
                <nav class="navbar navbar-default" role="navigation">
<!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <!--<span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>-->MENU
                        </button>
                    </div>
                            

<!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">                                    
                        <ul class="nav navbar-nav">
                            <li <?php if($pageName=='index'){echo 'class="sel"';} ?>><a href="index.php#index" title="Home" class="index">Home</a></li>
                            <li <?php if($pageName=='about'){echo 'class="sel"';} ?>><a href="index.php#about" title="Chi siamo" class="about">Chi siamo</a></li>
                            <li <?php if($pageName=='servizi'){echo 'class="sel"';} ?>><a href="index.php#servizi" title="Servizi" class="servizi">Servizi</a></li>
                            <li <?php if($pageName=='news'){echo 'class="sel"';} ?>><a href="index.php#news" title="News" class="news">News</a></li>
                            <li <?php if($pageName=='offerte'){echo 'class="sel"';} ?>><a href="index.php#offerte" title="Offerte" class="offerte">Offerte</a></li>
                            <li <?php if($pageName=='gallery'){echo 'class="sel"';} ?>><a href="index.php#gallery" title="Gallery" class="gallery">Gallery</a></li>                            
                            <li <?php if($pageName=='contatti'){echo 'class="sel"';} ?>><a href="index.php#contatti" title="Contatti" class="contatti">Contatti</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>