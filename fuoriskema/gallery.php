<div class="bigcont" id="gallery">
    <div id="slidesgal">
        <ul class="slides-container">
            <li>
                
                <img src="img/galsample.jpg" alt="Gallery background">
                <h1 class="claimgal" style="float: right;right:3%;top:80%;" id="showgallery">GUARDA LA GALLERY</h1>
            </li>
        </ul>
    </div>
    <div id="picwall" style="display:none;" class="wleather topp">
        <div class="container">
        <h1 class="bigtitle" style="color:#000;">GALLERY</h1>
        <div class="row">


<?php
for ($i=1;$i<=20;$i++) {
?>    
        <div class="col-sm-2 col-xs-4 imgal
<?php if (($i%5)==1){echo ' firstimg';} ?>
                        ">
            <a href="img/gallery/fuoriskema<?php echo $i; ?>.jpg" data-lightbox="fuoriskema">
                <img src="img/gallery/thmb/fuoriskema<?php echo $i; ?>.jpg" alt="Parrucchieri Estetetica Fuoriskema" class="img-thumbnail">
            </a>
        </div>
<?php
}
?>
            </div>
            <div class="row">
                <div id="closegallery"><p>Torna indietro</p></div>
            </div>
        
        </div>
    </div>
</div>
