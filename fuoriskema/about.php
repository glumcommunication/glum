<div class="bigcont" id="about">
    
    <div class="bleather topp">
        <div class="container">
            <h1 class="bigtitle">Chi siamo</h1>
            <p style="text-align: justify;margin-bottom:15px;">
                Non servono concetti complicati per far conoscere lo staff di FuoriSkema. Il gruppo di professioniste che lavorano nel salone di parrucchiere e nel centro estetico si descrive con parole semplici. Parole come gentilezza, solarità, cortesia, dinamicità, creatività e innovazione. Sono parole strettamente legate al lavoro che viene svolto quotidianamente a FuoriSkema. Quali altre caratteristiche potreste ricercare in chi si impegna a prendersi cura della vostra bellezza?
            </p>
            <div class="col-sm-2 people">
                <img src="img/ppl/francesco-migliorini.png" class="img-circle">
                <h1>FRANCESCO MIGLIORINI</h1>
            </div>
            <div class="col-sm-2 people">
                <img src="img/ppl/jennifer-landi.png" class="img-circle">
                <h1>JENNIFER LANDI</h1>
            </div>
            <div class="col-sm-2 people">
                <img src="img/ppl/stefania-bettini.png" class="img-circle">
                <h1>STEFANIA BETTINI</h1>
            </div>
            <div class="col-sm-2 people">
                <img src="img/ppl/monica-calamassi.png" class="img-circle">
                <h1>MONICA CALAMASSI</h1>
            </div>
            <div class="col-sm-2 people">
                <img src="img/ppl/carmela-galiano.png" class="img-circle">
                <h1>CARMELA GALIANO</h1>
            </div>
            <div class="col-sm-2 people">
                <img src="img/ppl/barbara-cubattoli.png" class="img-circle">
                <h1>BARBARA CUBATTOLI</h1>
            </div>
        </div>
    </div>
    <div class="wleather">
        <div class="container">
            <div class="col-sm-2 col-sm-offset-3 people">
                <img src="img/ppl/deborah-barcelli.png" class="img-circle">
                <h1>DEBORAH BARCELLI</h1>
            </div>
            <div class="col-sm-2 people">
                <img src="img/ppl/manila-agnorelli.png" class="img-circle">
                <h1>MANILA AGNORELLI</h1>
            </div>
            <div class="col-sm-2 people">
                <img src="img/ppl/silvia-donzelli.png" class="img-circle">
                <h1>SILVIA DONZELLI</h1>
            </div>
        </div>
    </div>
</div>

