<!doctype html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="img/favicon.png" type="image/svg" />
        <link href='http://fonts.googleapis.com/css?family=Amatic+SC:400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Voltaire' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Quattrocento+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <meta name="description" content="<?php echo $pagedesc ?>">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
        <link href="css/lightbox.css" rel="stylesheet" />
        <link href="css/normalize.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/superslides.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet" media="screen">

        <meta name="keywords" content="parrucchiere uomo donna coiffeur taglio capelli colore tintura permanente shampoo meches cheratina piastra ricci acconciatura hair style shatush pettinatura barba moda fashion salone centro estetico trattamenti viso corpo ultrasuoni esfoliante antirughe anti-age infrarossi radiofrequenza rassodante ossigenante pressoterapia elettrostimolazione idratante cellulite benessere hammam depilazione elettrodepilazione epilazione cera resina naturale manicure smalto semipermanente pedicure doccia solarium pelle corpo">        
        <title>Fuoriskema - Parrucchieri Estetica | Via G. di Vittorio 5/9 - Castellina Scalo (SI) - Telefono: +39 0577 304390</title>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47602254-1', 'fuoriskema.net');
  ga('send', 'pageview');

</script>
<script src="js/respond.js"></script>
<script src="js/selectivizr-min.js"></script>
    </head>
    <body 
<?php 
    if ($pagename == 'read') {echo 'style="background-image:url(img/bleather.jpg);background-repeat: repeat;"';}
?>
          >
    

        <div id="wrapper">
