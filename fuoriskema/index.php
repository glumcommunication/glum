<?php
$pagename = 'index';
$pagetitle = 'Home';
$pagedesc = 'Fuoriskema è un parrucchiere ed un centro estetico a Castellina Scalo. Da noi potete trovare uno staff solare e preparato, pronto a prendersi cura di voi e a soddisfare ogni vostra esigenza.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
        <div id="index">
            <div id="slides">
               <ul class="slides-container">
                    <li>
                        <h1 class="claiml" style="float: left;left: 10%;">LA MODA VA FUORI MODA,<br>LO STILE MAI...</h1>
                        <img src="img/modella.jpg" alt="Home">
                    </li>
                    <li>
                        <h1 class="claiml" style="float:left;left: 10%;">LA BELLEZZA NON E' SOLO<br>QUESTIONE DI DONNE...</h1>
                        <img src="img/modello03.jpg" alt="Home">
                    </li>
                    <li>
                        <h1 class="claiml" style="float:left;left: 10%;">IL BELLO PIACE A TUTTI.</h1>
                        <img src="img/estetica.jpg" alt="Home">
                    </li>
                </ul>
    
                <!--<nav class="slides-navigation">
                    <a href="#" class="next">Next</a>
                    <a href="#" class="prev">Previous</a>
                </nav>-->
            </div>
        </div>
<?php
include_once 'about.php';
include_once 'servizi.php';
include_once 'news.php';
include_once 'offerte.php';
include_once 'gallery.php';
include_once 'contatti.php';
?>

<?php
include_once 'footer.php';
?>
