<?php
$pagename = "laboratorio";
$pagetitle = "Laboratorio riparazioni";
$description= "La gioielleria Rabatti è espressione di alta professionalità: l’esperienza nelle riparazioni artigianali, unita alla strumentazione d’avanguardia garantiscono un risultato ottimale su orologi e gioielli di ogni tipologia, forma e fattura. 
Il laboratorio di Orologeria è specializzato nelle riparazioni di orologi, revisioni complete e rimesse a nuovo rispettando le norme di qualità più esigenti. Offriamo servizi di assistenza e consulenza tecnica qualificata.";
include_once('header.php');
?>

<div id="main">
    <div class="boxproduct leftmargin" style="height: 406px;">
<script>
$(window).load(function() {
  $('#sliderlab').flexslider({
    animation: "slide",
    slideshow: false,

    controlNav: "thumbnails"
  });
});
</script>
        <div class="flexslider" id="sliderlab" style="background-color: #3A342E;z-index: 100;">
          <ul class="slides">
            <li data-thumb="images/thn-labslide_01.jpg">
              <img src="images/labslide_01.jpg" />
            </li>
            <li data-thumb="images/thn-labslide_02.jpg">
              <img src="images/labslide_02.jpg" />
            </li>
            <li data-thumb="images/thn-labslide_03.jpg">
              <img src="images/labslide_03.jpg" />
            </li>  
            <li data-thumb="images/thn-labslide_04.jpg">
              <img src="images/labslide_04.jpg" />
            </li>
            <li data-thumb="images/thn-labslide_05.jpg">
              <img src="images/labslide_05.jpg" />
            </li>
            <li data-thumb="images/thn-labslide_06.jpg">
              <img src="images/labslide_06.jpg" />
            </li>      
          </ul>
        </div>
        <!--<img src="images/laboratorio.jpg" alt="" />-->
    </div>
    <div class="boxproduct right" style="height: 408px; background-color: #ffffff;">
        <div class="producttext" style="height:368px; width: 467px; padding: 20px;">
            <p>
            La gioielleria Rabatti è espressione di alta professionalità: l’esperienza nelle riparazioni artigianali, unita alla strumentazione d’avanguardia garantiscono un risultato ottimale su orologi e gioielli di ogni tipologia, forma e fattura.<br>
            Il laboratorio di Orologeria è specializzato nelle riparazioni di orologi, revisioni complete e rimesse a nuovo rispettando le norme di qualità più esigenti.<br>
            Offriamo servizi di assistenza e consulenza tecnica qualificata.
            </p>
            <div id="name" style="width:487px; margin-top: 20px;">
                <span style="font-size:4.8125em;">Rabatti</span><br>
                <span style="font-size:1.25em;">OROLOGERIA GIOIELLERIA</span><br>
                <span style="font-size:1.0625em; font-family: 'Comfortaa', cursive;">di Bini Andrea</span>
            </div>
        </div>
    </div>

    <div class="boxproduct" style="width:1024px;border-top: 2px solid #ffffff;margin-top:0px;padding-top:10px;">
        <img src="images/linkfull.png" alt="Rabatti">
    </div>
<?php
include_once ('brands.php');
?>
</div>

<?php
include_once('footer.php');
?>