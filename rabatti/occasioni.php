<?php
$pagename = "occasioni";
$pagetitle = "Occasioni";
$description ="La pagina delle nostre occasioni, qui troverai tutte le nostre offerte speciali: articoli prestigiosi e di qualità a prezzi imbattibili.";
include_once 'dbc.php';
include_once('header.php');
?>

<div id="main">
<!--<p style="font-family: 'Playball', cursive;font-size: 1.3em;text-align:center;padding: 150px 0;">Tutte le nostre offerte saranno presto on line</p>-->
<?php
	$sql = "SELECT * FROM products ORDER BY Data DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
	$rows = mysql_num_rows($result);
	$sum = 0;
	for ($j = 0 ; $j < $rows ; ++$j) {
		$row = mysql_fetch_row($result);
echo <<<_END
	 <div class="boxproduct">
        <div class="productpic">
            <img src="$row[2]" alt="Row[0]" />
        </div>
_END;
}
?>
	 <div class="boxproduct">
        <div class="productpic">
            <img src="public/corumcoinwatch.jpg" alt="Corum" />
        </div>
        <div class="producttext offer">
            <h1>CORUM COIN WATCH TEN DOLLARS</h1>
            <p>
            Referenza: 5514756<br>
            Carica: Manuale<br>
            Cinturino: Pelle di coccodrillo<br>
            Collezione: Coin Watch<br>
            Descrizione: Cassa in due parti, un’autentica moneta da 10 dollari in oro giallo 18K, tagliata a metà funge da cassa e da quadrante
            </p>
            <h2 class="marca"></h2>
            <h2 class="prezzo"></h2>
        </div>
    </div>
    <div class="boxproduct">    
        <div class="productpic">
            <img src="public/perseosettebello.jpg" alt="Perseo" />
        </div>
        <div class="producttext offer">
            <h1>PERSEO SETTEBELLO</h1>
            <p>
            Referenza: 740.501<br>
            Carica: Manuale U.T. 6376 rielaborato<br>
            Cassa: Oro giallo 18K<br>
            Cinturino: Pelle di coccodrillo<br>
            Collezione: Coin Watch<br>
            Descrizione: Serie limitata n°082 su 100
            </p>
            <h2 class="marca"></h2>
            <h2 class="prezzo"></h2>
        </div>
    </div>
    <div class="boxproduct">    
        <div class="productpic">
            <img src="public/rolexsubmariner.jpg" alt="Rolex" />
        </div>
        <div class="producttext offer">
            <h1>ROLEX SUB MARINER</h1>
            <p>
            Referenza: 16613<br>
            Misura: 40mm<br>
            Cassa/Bracciale: Acciaio/Oro<br>
            Movimento: automatico<br>
            Vetro: Zaffiro<br>
            Quadrante e ghiera blu<br>            
            Impermeabile 300 MT 
            </p>
            <h2 class="marca"></h2>
            <h2 class="prezzo"></h2>
        </div>
    </div>
  
    <div class="boxproduct">    
        <div class="productpic">
            <img src="public/zenithelprimero.jpg" alt="Zenith" />
        </div>
        <div class="producttext offer">
            <h1>ZENITH EL PRIMERO PORT ROYAL V</h1>
            <p>
            Referenza: 01/02.0450.400<br>
            Cassa: Acciaio<br>
            Condizioni: Nuovo di vetrina<br>
            Diametro: 40mm<br>
            Vetro: Zaffiro<br>
            Quadrante nero, numeri arabi<br>
            Bracciale: Acciaio con deplayant 
            </p>
            <h2 class="marca"></h2>
            <h2 class="prezzo"></h2>
        </div>
    </div>
    <div class="boxproduct">    
        <div class="productpic">
            <img src="public/zenithelite.jpg" alt="Zenith" />
        </div>
        <div class="producttext offer">
            <h1>ZENITH  ELITE  HW</h1>
            <p>
            Referenza: 01/1125.650<br>
            Carica: Manuale<br>
            Cassa: Acciaio<br>
            Condizioni: Nuovo di vetrina<br>
            Diametro: 37mm<br>
            Vetro: Zaffiro<br>
            Quadrante: Bianco, numeri arabi, piccoli secondi al 9<br>
            Cinturino: Pelle
            </p>
            <h2 class="marca"></h2>
            <h2 class="prezzo"></h2>
        </div>
    </div>            

 
 

    <!--    
    <div class="boxproduct right">
        <div class="productpic">
            <img src="images/orologio_esempio.jpg" alt="" />
        </div>
        <div class="producttext">
            <h1>Orologio di marca</h1>
            <p>
            Cronografo automatico Valjoux 7750, con funzione di GMT doppia (2 fusi orari). 
            </p>
            <h2 class="marca">Marca</h2>
            <h2 class="prezzo">Prezzo</h2>
        </div>    
    </div>
    <div class="boxproduct leftmargin">
        <div class="productpic">
            <img src="images/orologio_esempio.jpg" alt="" />
        </div>
        <div class="producttext">
            <h1>Orologio di marca</h1>
            <p>
            Cronografo automatico Valjoux 7750, con funzione di GMT doppia (2 fusi orari). 
            </p>
            <h2 class="marca">Marca</h2>
            <h2 class="prezzo">Prezzo</h2>
        </div>
    </div>
    <div class="boxproduct right">
        <div class="productpic">
            <img src="images/orologio_esempio.jpg" alt="" />
        </div>
        <div class="producttext">
            <h1>Orologio di marca</h1>
            <p>
            Cronografo automatico Valjoux 7750, con funzione di GMT doppia (2 fusi orari). 
            </p>
            <h2 class="marca">Marca</h2>
            <h2 class="prezzo">Prezzo</h2>
        </div>    
    </div>
    <div class="boxproduct leftmargin">
        <div class="productpic">
            <img src="images/orologio_esempio.jpg" alt="" />
        </div>
        <div class="producttext">
            <h1>Orologio di marca</h1>
            <p>
            Cronografo automatico Valjoux 7750, con funzione di GMT doppia (2 fusi orari). 
            </p>
            <h2 class="marca">Marca</h2>
            <h2 class="prezzo">Prezzo</h2>
        </div>
    </div>
    <div class="boxproduct right">
        <div class="productpic">
            <img src="images/orologio_esempio.jpg" alt="" />
        </div>
        <div class="producttext">
            <h1>Orologio di marca</h1>
            <p>
            Cronografo automatico Valjoux 7750, con funzione di GMT doppia (2 fusi orari). 
            </p>
            <h2 class="marca">Marca</h2>
            <h2 class="prezzo">Prezzo</h2>
        </div>    
    </div>-->

<?php
include_once ('brands.php');
?>

</div>
    

    

<?php
include_once('footer.php');
?>