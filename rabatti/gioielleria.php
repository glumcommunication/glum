<?php
$pagename = "gioielleria";
$pagetitle = "Gioielleria";
$description ="La gioielleria Rabatti è sinonimo di una tradizione nata nel 1950 e continuata oggi con amore e dedizione da Andrea Bini. Attenzione alle tendenze, una vasta scelta di articoli che spaziano da manufatti artigianali di grande 
pregio alle migliori firme di gioielleria e argenteria.";
include_once('header.php');
?>

<div id="main">
    <div class="introtext">
        <p>
            La gioielleria Rabatti è sinonimo di una tradizione nata nel 1950 e continuata oggi con amore e dedizione da Andrea Bini.<br>
            La gioielleria Rabatti vuol dire attenzione alle tendenze: una vasta scelta di articoli che spaziano da manufatti artigianali di grande
            pregio alle migliori firme di gioielleria e argenteria, che potrete anche scoprire visitando la sezione dedicata.
        </p>
    </div>
    <div class="brandlist" style="width: 660px;padding: 10px 182px 0;">
        <a href="http://www.salvini.it/" alt="Salvini" target="_blank"><img src="images/logob_12.png" alt="" /></a>
        <a href="http://www.annamariacammilli.com/" alt="Annamaria Camilli" target="_blank"><img src="images/logob_01.png" alt="" /></a>
        <a href="http://www.pandora.net/it-IT" alt="Pandora" target="_blank"><img src="images/logob_10.png" alt="" /></a>
        <a href="http://www.morellato.com/morellato_it/" alt="Morellato" target="_blank"><img src="images/logob_08.png" alt="" /></a>
        <a href="http://www.nomination.it/" alt="Nomination" target="_blank"><img src="images/logob_09.png" alt="" /></a>
        <a href="http://www.genesiaperle.it/" alt="Genesia" target="_blank"><img src="images/logob_16.png" alt="" /></a>
        
    </div>
    <p style="margin-bottom:0;">Argenteria</p>
    
    <div class="brandlist" style="width: 660px;padding: 10px 182px 0;border-bottom: 2px solid #ffffff; border-top: 2px solid #ffffff;">
        <a href="http://www.operacollection.it/" alt="Opera" target="_blank"><img src="images/logob_15.png" alt="" /></a>
        <a href="http://www.greggio.it/" alt="Greggio" target="_blank"><img src="images/logob_04.png" alt="" /></a>
        <a href="http://www.midaargento.it/" alt="Mida" target="_blank"><img src="images/logob_14.png" alt="" /></a>

    </div>    
    <div class="boxproduct leftmargin">
        <a href="occasioni.php" alt="Occasioni"><img src="images/linkoccasioni.png" alt="Occasioni"></a>
    </div>
    <div class="boxproduct right">
        <a href="laboratorio.php" alt="Laboratorio"><img src="images/linklaboratorio.png" alt="Laboratorio"></a>
    </div>
<?php
include_once ('brands.php');
?> 
</div>

<?php
include_once('footer.php');
?>