<?php
$pagename = "index";
$pagetitle = "Home";
$description = "Gioielleria, Orologeria Rabatti di Andrea Bini a Castelfranco di Sopra (Arezzo). Tradizione, attenzione alle tendenze e alta professionalità.";
include_once('header.php');
?>
<script>   
$(document).ready(function() {
  $('#bighome').flexslider({
    animation: "slide",
    animationLoop: true,
    itemWidth: 1024,
    itemMargin: 0,
    controlNav: false,
    directionNav: true, 
    slideshow: true,
    move: 1

  });
});
</script>
<div id="main">
    <div class="mainTop">
    <div class="flexslider" id="bighome">
        <ul class="slides">
        <li><img src="images/homebig_01.jpg" alt="Home - 1" /></li>
        <li><img src="images/homebig_02.jpg" alt="Home - 2" /></li>
        <li><img src="images/homebig_03.jpg" alt="Home - 3" /></li>
        <li><img src="images/homebig_04.jpg" alt="Home - 4" /></li>
        </ul>
    </div>
    </div>
    <div class="mainBottom">
    <div class="homecarousel">
        <ul>
            <li><a href="occasioni.php" alt="Occasioni"><img src="images/carousel_1.jpg" /></a></li>
            <li><a href="orologeria.php" alt="Orologeria"><img src="images/carousel_2.jpg" /></a></li>
            <li><a href="gioielleria.php" alt="Gioielleria"><img src="images/carousel_3.jpg" /></a></li>
            <li><a href="laboratorio.php" alt="Laboratorio"><img src="images/carousel_4.jpg" /></a></li>         
        </ul>
    </div>
    </div>
<?php
include_once ('brands.php');
?>
</div>

<?php
include_once('footer.php');
?>