<?php
$pagename = "orologeria";
$pagetitle = "Orologeria";
$description = "La gioielleria Rabatti vuol dire regalare un tempo di qualità. Orologi esclusivi dalle marche più prestigiose, precisi strumenti di eccellenza estetica e meccanica. 
Potrai trovare gli articoli più esclusivi e ricercati, accompagnati dalla guida esperta dello staff di Andrea Bini.";
include_once('header.php');
?>

<div id="main">
    <div class="introtext">
        <p>
            La gioielleria Rabatti vuol dire regalare un tempo di qualità.<br>
            Orologi esclusivi dalle marche più prestigiose, precisi strumenti di eccellenza estetica e meccanica.<br>
            In negozio potrai trovare gli articoli più esclusivi e ricercati, accompagnati dalla guida esperta dello staff di Andrea Bini.
        </p>
    </div>
    <div class="brandlist" style="border-bottom: 2px solid #ffffff;">
        <a href="http://www.glycine-watch.ch/" alt="Glycine" target="_blank"><img src="images/logob_03.png" alt="" /></a>
        <a href="http://www.perseo-watches.it/" alt="Perseo" target="_blank"><img src="images/logob_11.png" alt="" /></a>
        <a href="http://www.citizen.it/" alt="Citizen" target="_blank"><img src="images/logob_02.png" alt="" /></a>
        <a href="http://www.lorenz.it/" alt="Lorenz" target="_blank"><img src="images/logob_06.png" alt="" /></a>
        <a href="http://www.kienzle.it/" alt="Kienzle" target="_blank"><img src="images/logob_05.png" alt="" /></a>
        <a href="http://www.morellato.com/morellato_it/" alt="Morellato" target="_blank"><img src="images/logob_08.png" alt="" /></a>
        <a href="http://www.lowell.it/" alt="Lowell" target="_blank"><img src="images/logob_07.png" alt="" /></a> 
        <a href="http://orologigalileo.com/" alt="Galileo" target="_blank"><img src="images/logob_13.png" alt="" /></a>
    </div>

    <div class="boxproduct leftmargin">
        <a href="gioielleria.php" alt="Gioielleria"><img src="images/linkgioielleria.png" alt="Gioielleria"></a>
    </div>
    <div class="boxproduct right">
        <a href="occasioni.php" alt="Occasioni"><img src="images/linkoccasioni.png" alt="Occasioni"></a>
    </div>
<?php
include_once ('brands.php');
?>
</div>

<?php
include_once('footer.php');
?>