<?php
$pagename = "Temp";
$pagetitle = "Temp";
include_once('header.php');
?>

<div id="main">
    <div class="boxproduct leftmargin" style="height: 389px; background-color: #ffffff;">
        <div class="producttext" style="height:369px; width: 487px;">

            <p style="font-family: 'Comfortaa', cursive;text-align:center;font-size: 1.5em;">
            Per Informazioni e assistenza
            </p>
        <form  action="send.php"  method="post" id="form">
            
            <div class="formbox">
            <table id="cForm">
                <tr>
                    <td>Nome*</td>
                    <td><input type="text" name="nome"  value="" id="q1" maxlength="20" size="30" required="required" style="width:300px;height:20px;border-radius:5px;margin-bottom:10px;"/></td>
                </tr>
                <tr>
                    <td>Cognome*</td>
                    <td><input type="text" name="cognome"  value="" id="q2" maxlength="20" size="30" required="required" style="width:300px;height:20px;border-radius:5px;margin-bottom:10px;"/></td>
                </tr>
                <tr>
                    <td>Email*</td>
                    <td><input type="text" name="email"  value="" id="q3" maxlength="20" size="30" required="required" style="width:300px;height:20px;border-radius:5px;margin-bottom:10px;"/></td>
                </tr>    
                <tr>
                    <td>Telefono</td>
                    <td><input type="text" name="telefono"  value="" id="q4" maxlength="20" size="30" style="width:300px;height:20px;border-radius:5px;margin-bottom:10px;"/></td>
                </tr>
                <tr>
                    <td>Richiesta*</td>
                    <td><textarea name="messaggio" class="text" id="q5" style="width:295px;height:60px;border-radius:5px;margin-bottom:10px;"></textarea></td>
                </tr>
                <tr>
                    <td></td><td style="font-family: 'Comfortaa', cursive; font-size:0.7em;text-align: center;">I campi segnati con l'asterisco sono obbligatori.</td>
                </tr>                
                <tr>
                    <td></td>
                    <td style="text-align:center;"><input type="submit" name="doBook" class="btn" value="Invia" style="border:0;font-family: 'Julius Sans One', sans-serif;" /></td>
                </tr>

            </table>
            </div>
        </form>
        </div>

    </div>
    <div class="boxproduct right" style="height: 389px; background-color: #ffffff;">
        <div class="producttext" style="height:369px; width: 487px;">
            <p style="font-family: 'Comfortaa', cursive;text-align:center;font-size: 1.5em;">
                Rabatti Orologeria Gioielleria<br>
                Castelfranco di Sopra<br>
                Via Alcide de Gasperi 32<br>
                055 9149596
            </p>
            <div class="social" style="width:48px; margin: 0 auto; ">
            <p><a href="http://www.facebook.com/RabattiGioielleriaOrologeriaDiBiniAndrea"alt="Facebook"><img src="images/facebook.png" alt="Facebook" style="box-shadow: 0 0 0 #ffffff;"/></a></p>
            </div>
            <div id="name" style="width:487px; margin-top: 30px;">
                <span style="font-size:4.8125em;">Rabatti</span><br>
                <span style="font-size:1.25em;">OROLOGERIA GIOIELLERIA</span><br>
                <span style="font-size:1.0625em; font-family: 'Comfortaa', cursive;">di Bini Andrea</span>
            </div>
        </div>
    </div>

    <div class="boxproduct" style="width:1024px;border-top: 2px solid #ffffff;margin-top:0px;padding-top:10px;">
        <img src="images/linkfull.png" alt="Rabatti">
    </div>
<?php
include_once ('brands.php');
?>
</div>

<?php
include_once('footer.php');
?>