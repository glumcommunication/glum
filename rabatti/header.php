<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name= "keywords" content= "Rabatti, Gioielleria, Orologeria, Oreficeria, Andra Bini, Castelfranco di Sopra, Morellato, Salvini, Annamaria Camilli, Riparazione orologi, laboratorio riparazioni, Pandora, citizen, perseo, Nomination, Longines"  />
<meta name="description" content="<?php echo $description ?>">
<link rel="icon" href="images/favicon.png" type="image/svg" /> 
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<link href='http://fonts.googleapis.com/css?family=Comfortaa:400,300,700' rel='stylesheet' type='text/css'>
<title><? echo $pagetitle; ?> | Rabatti Gioielleria Orologeria di Bini Andrea - Castelfranco di Sopra (AR) </title>
<link href='http://fonts.googleapis.com/css?family=Julius+Sans+One' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
<link href="css/flexslider.css" rel="stylesheet" type="text/css" />    
<script src="js/jquery.flexslider.js"></script>
<script>   
$(window).load(function() {
  $('#sliderbrands').flexslider({
    animation: "fade",
    animationLoop: true,
    itemWidth: 1024,
    itemMargin: 0,
    controlNav: false,
    directionNav: false, 
    slideshow: true,
    move: 1
  });
});

</script>
<script src="js/jquery.mousewheel.min.js"></script>
<!-- custom scrollbars plugin -->
<script src="js/jquery.mCustomScrollbar.js"></script>
<script>
(function($){
    $(document).ready(function(){
        $(".offer").mCustomScrollbar({
            scrollButtons:{
                enable:false
            },
            scrollInertia:0,
            advanced:{
                updateOnContentResize:true
            },
        });
    })

})(jQuery);
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-37021895-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div id="wrapper">

<!-- #################### -->

<div id="header">
    <a href="index.php" alt="home">
    <div id="name">
        <span style="font-size:4em;">Rabatti</span><br>
        <span style="font-size:1.25em;">OROLOGERIA GIOIELLERIA</span><br>
        <span style="font-size:1.0625em; font-family: 'Comfortaa', cursive;">di Bini Andrea</span><br><br>
                <span style="font-family: 'Playball', cursive;font-size: 1.3em;">Tradizione, attenzione alle tendenze e alta professionalità</span>

    </div>
    </a>

    <div id="nav">
        <ul>
            <li><a href="occasioni.php">Occasioni</a></li>
            <li><a href="gioielleria.php">Gioielleria</a></li>
            <li><a href="orologeria.php">Orologeria</a></li>
            <li><a href="laboratorio.php">Laboratorio</a></li>
            <li><a href="contatti.php">Contatti</a></li>
        </ul>
    </div>
</div>

<!-- #################### -->
