<?php
$slug = $_GET["slug"];
include_once 'dbc.php';
$sql = "SELECT * FROM products WHERE slug='$slug'";
$result = mysql_query($sql);
if (!$result) die ("Database access failed: " . mysql_error());
$row = mysql_fetch_row($result);
$datetime = strtotime($row[3]);
$mysqldate = date("d/m/Y", $datetime);
$pagetitle = $row[0];
$pagename = "news";

$pagedesc = $row[0];
$pageurl = "news.php?slug=".$row[5];
include_once 'header.php';
include_once 'nav.php';
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/it_IT/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="main">
    <div class="container">
        <div class="maincontent">
<?php            
echo <<<_END
            <div class="row news" style="border-top:none;">
_END;
    if($row[2]!="") {
echo <<<_END
                <div class="col-sm-3"><img src="$row[2]" alt="$row[0]"></div>
                <div class="col-sm-9">
                    <h1 class="newstitle"><a href="$pageurl" title="Leggi la news completa">$row[0]</a></h1>
                    <div class="newsdetail"><p>Pubblicata il $rowdata[data], $row[3] (<a href="article.php?slug=$row[5]">Link</a>)</p></div>
                    <div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-action="recommend" style="margin-right:5px;"></div>
        <a href="https://twitter.com/share" class="twitter-share-button" data-lang="it" data-hashtags="esteticaego">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        <div class="g-plusone" data-size="medium"></div>
                    <div class="newsbody">$row[1]</div>

                </div>
            </div>
                
_END;
}
else {
echo <<<_END
                <div class="col-sm-12">
                    <h1 class="newstitle"><a href="$pageurl" title="Leggi la news completa">$row[0]</a></h1>
                    <div class="newsdetail"><p>Pubblicata il $rowdata[data], $row[3] (<a href="article.php?slug=$row[5]">Link</a>)</p></div>
                    <div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false" data-action="recommend"></div>
        <a href="https://twitter.com/share" class="twitter-share-button" data-lang="it" data-hashtags="esteticaego">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
        <div class="g-plusone" data-size="medium"></div>
                    <div class="newsbody">$row[1]</div>

                </div>
            </div>
                
_END;
}
?>

        </div>
    </div>
</div>
<script type="text/javascript">
  window.___gcfg = {lang: 'it'};

  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
<?php
include_once 'footer.php';
?>