<?php
$pagename = 'servizi';
$pagetitle = 'I nostri Servizi';
$pagedesc = 'Trattamenti corpo e viso; decottopia e dimagrimento; pedicure e manicure; epilazione tradizionale, con metodo SUGAR PAST e laser; massaggi e trattamenti anti-aging.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<div id="main">
    <div class="container">
        <div class="maincontent">
            
            <div class="row">
                <div class="col-sm-4 servicepic">
                    <div class="servicebox" style="background-color: #e9ebec;">
                        <h1 style="color:#27829d;border-color:#27829d;">DIMAGRIMENTO</h1>
                        <p style="color:#27829d;">
                            Centro Specializzato in decottopia.<br>
                            Dimagrire nel rispetto del benessere di Corpo e Mente.<br>
                            Un valido aiuto con l'utilizzo della decottopia e degli integratori amminoacidi.
                        </p>
                    </div>
                </div>
                <div class="col-sm-4 servicepic">
                    <div class="servicebox"  style="background-color: #33b2b1;">
                        <h1>TRATTAMENTI CORPO</h1>
                        <p>
                            Lipomassage con L.P.G. sistema Endermologie.<br>
                            Contrastare le adiposit&agrave; pi&ugrave; resistenti, rassodare il tessuto, drenare eliminando la ritenzione idrica.<br>
                            Tecnologia Med Contour (ultrasuoni a bassa frequenza).<br>
                            Trattare i pannicoli adiposi e la cellulite pi&ugrave; compatta.<br>
                            Manualit&agrave; Connettivali e Linfodrenanti.<br>
                            Azione riequilibrante e anti-edema dei tessuti.
                        </p>
                    </div>
                </div>
                <div class="col-sm-4 servicepic">
                    <div class="servicebox"  style="background-color: #2c5160;">
                        <h1>TECNOLOGIA BIODERMOGENESI</h1>
                        <p>
                            Per il progressivo riempimento delle smagliature, nessun effetto collaterale e risultati duraturi. Corpo pi&ugrave; tonificato e giovane.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 servicepic">
                    <img src="img/servizi_01.jpg" alt="Immagine servizi">
                </div>
                <div class="col-sm-4 servicepic">
                    <div class="servicebox"  style="background-color: #27829d;">
                        <h1>EPILAZIONE LASER</h1>
                        <p>
                            Nuova tecnologia: macchinario SC YOU.
                        </p>
                    </div>
                </div>
                <div class="col-sm-4 servicepic">
                    <div class="servicebox" style="background-color: #bbe0e4;">
                        <h1 style="color:#27829d;border-color:#27829d;">TRATTAMENTI VISO</h1>
                        <p style="color:#27829d;">
                            Dal 2005 associati A.M.I.A. (Ass. Medici Italiani AntiAging)<br>
                            Trattamento specialistico idratazione trifasica.<br>
                            Trattamento specialistico per pelli acneiche.<br>
                            Trattamento specialistico per foto e crono aging (invecchiamento cutaneo).<br>
                            Trattamento specialistico liftante.<br>
                            Trattamento per pelli sensibili, rossori e couperose.<br>
                            Trattamento specialistico schiarente per pelli discromiche.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 servicepic">
                    <div class="servicebox"  style="background-color: #4ba5a6;">
                        <h1>EPILAZIONE</h1>
                        <p>
                            Rivoluzionario metodo SUGAR PAST a base di zucchero, 100% biodegradabile, previene la follicolite, riduce la ricrescita, nessun disagio per la pelle.
                        </p>
                    </div>
                </div>
                <div class="col-sm-4 servicepic">
                    <div class="servicebox"  style="background-color: #215e6d;">
                        <h1>PEDICURE E MANICURE</h1>
                        <p>
                            Con smalto semipermanente. Utilizzo di prodotti e smalti privi di DBP, toluene, formaleide e canfora. Gli unici sicuri anche per donne in gravidanza.
                        </p>
                    </div>
                </div>
                <div class="col-sm-4 servicepic">
                    <img src="img/servizi_02.jpg" alt="Immagine servizi">
                </div>
            </div>
            
            
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
$(document).ready(function(){
   var swidth = $(".servicebox").width();
   var sheight = swidth/1.6;
   $(".servicebox").css({"height" : sheight});
   return null;    
});
$(window).resize(function() {
   var swidth = $(".servicebox").width();
   var sheight = swidth/1.6;
   $(".servicebox").css({"height" : sheight});
   return null;
});
</script>
<?php
include_once 'footer.php';
?>