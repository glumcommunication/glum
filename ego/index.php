<?php
$pagename = 'index';
$pagetitle = 'Home';
$pagedesc = 'Estetica Ego è a Siena dal 1997, in Via del Paradiso 27. Il centro è un punto di riferimento per la bellezza, la cura della persona e il relax.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
            <div id="main">
                <div class="rowpic">
                    <div class="col-xs-4 homepic">
                        <img src="img/home01.jpg" alt="home">
                    </div>
                    <div class="col-xs-4 homepic">
                        <img src="img/home02.jpg" alt="home">
                    </div>
                    <div class="col-xs-4 homepic">
                        <img src="img/home03.jpg" alt="home">
                    </div>

                </div>
                <div class="container">
                    <div class="maincontent">
                    <div class="col-md-4 col-sm-6">
                        <p>
                            Da sempre EGO ha l'obiettivo di prendersi cura della persona. In continua evoluzione, ricerca sempre nuovi servizi, 
                            senza inseguire le mode, ma ricercando solo ciò che veramente è funzionale ed in armonia con il proprio corpo.
                        </p>
                        <p>
                            Offrire un momento per rigenerarsi, ritrovare se stessi e ripartire con nuova energia per affrontare lo stress quotidiano. 
                            Perché la bellezza è un equilibrio tra corpo e mente e l'equilibrio non è né scontato né gratuito... è una conquista!
                        </p>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <h1>DOVE SIAMO</h1>
                        <div id="map_canvas" style="width:100%; height:150px;">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <h1>NEWS</h1>
<?php
	$sql = "SELECT * FROM products ORDER BY Data DESC LIMIT 1";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
	$rows = mysql_num_rows($result);
	$sum = 0;
    if ($rows==0) {
        echo "<p>Al momento non &egrave; presente nessuna notizia.</p>";
    }
	for ($j = 0 ; $j < $rows ; ++$j) {
		$row = mysql_fetch_row($result);
        $pageurl = "item.php?slug=".$row[5];
        $datasql = "SELECT DATE_FORMAT(Data, '%d/%m/%Y')  as data FROM news WHERE id='$row[5]'";
        $rowdata = mysql_fetch_assoc(mysql_query($datasql));
        $row[1] = strip_tags($row[1], '<p><a>');
    if (strlen($row[1])>=199) {
        $nlenght = 1;
        $row[1]= substr($row[1], 0, 199);
        $row[1] .= "...";

    }        
echo <<<_END
                    <div class="homenews">
                        <h1 class="newstitle"><a href="$pageurl" title="Leggi la news completa">$row[0]</a></h1>
                        <div class="newsdetail">Pubblicata il $rowdata[data], $row[3] (<a href="item.php?slug=$row[5]">Link</a>)</div>
                        <div class="newsbody">$row[1]</div>
_END;
    if ($nlenght == 1) {
echo <<<_END
        <p><a href="item.php?slug=$row[5]">Leggi la news completa...</a></p>
_END;
    }
echo <<<_END
                        
                        

                    </div>

_END;
}
?>
                        <p style="margin-top:15px;text-align: right;"><strong><a href="news.php">Leggi tutte le altre news...</a></strong><!--&nbsp;<span class=".glyphicon .glyphicon-arrow-right"></span>--></p>
                    </div>
                    </div>
                </div>
            </div>

<?php
include_once 'footer.php';
?>