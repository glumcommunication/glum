<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="img/favicon.png" type="image/svg" />
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,600,800' rel='stylesheet' type='text/css'>
        <meta name="description" content="<?php echo $pagedesc ?>">
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet" media="screen">
        <link href="css/lightbox.css" rel="stylesheet" />
        <script src="js/respond.src.js"></script>
        <meta name="keywords" content="Estetica Ego, Via del Paradiso, Siena, estetica, lipomassage, viso, labbra, esperienza, professionalità, relax, massaggio, maschera, anti-age, benessere, tonificazione, dimagrimento, epilazione laser, rassodamento, pedana vibrante, estetista">
<?php
if ($pagename == "contatti" || $pagename == "index") {
    echo <<<_END
        
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    function initialize() {
        var latlng = new google.maps.LatLng(43.321064, 11.32802);
        var settings = {
            zoom: 15,
            center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
            navigationControl: true,
            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
            mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"), settings);

  var companyLogo = new google.maps.MarkerImage('img/positionshadow.png',
    new google.maps.Size(86,58),
    new google.maps.Point(0,0),
    new google.maps.Point(20,50)
);

var companyPos = new google.maps.LatLng(43.321064, 11.32802);
var companyMarker = new google.maps.Marker({
    position: companyPos,
    map: map,
    icon: companyLogo,
    //shadow: companyShadow,
    title:"Centro Estetica Ego"
});
var contentString = '<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h1 id="firstHeading" class="firstHeading" style="font-size:14px;font-weight:bold;border-bottom: none;margin-bottom:5px;">Centro Estetica Ego</h1>'+
    '<div id="bodyContent">'+
    '<p style="font-size:12px;">Via del Paradiso 27, 53100 Siena<br>Telefono: +39 0577 221011<br>Email: <a href="mailto:info@esteticaego.it">info@esteticaego.it</a><br>Website: <a href="http://www.esteticaego.it" title="Centro Estetica Ego">www.esteticaego.it</a><br></p>'+
    '</div>'+
    '</div>';
 
var infowindow = new google.maps.InfoWindow({
    content: contentString
});
google.maps.event.addListener(companyMarker, 'click', function() {
  infowindow.open(map,companyMarker);
});
    }
    </script>
_END;
}

?>        
        <title><?php echo $pagetitle ?> | Centro Estetica Ego Siena - Trattamenti di bellezza, massaggi, cura del corpo, wellness, relax</title>
<?php include_once("analyticstracking.php") ?>        
    </head>
    <body <? if ($pagename == "contatti" || $pagename == "index") echo " onload=\"initialize()\"";?>>

        <div id="wrapper">