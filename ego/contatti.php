<?php
$pagename = 'contatti';
$pagetitle = 'Contatti';
$pagedesc = 'Il centro Estetica Ego è in Via del Paradiso 27, nel centro storico di Siena. Vi aspetta dal martedì al venerdì dalle ore 10 alle 19:30 e il sabato dalle ore 10 alle 17. Telefono: 0577 221011 - Email: info@esteticaego.it';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';

if ($_POST['doSend']=='Invia') {
$name = $_POST[name];
$email = $_POST[email];
$message = $_POST[message];
$headers = "";
$headers .= "From: $email\n";
$headers .= "Reply-To: $email\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=utf-8\n";
$to = "info@esteticaego.it";
$subject = "Richiesta informazioni";
/*$message = $messaggio;*/
$messagebody = "Hai ricevuto una nuova email via esteticaego.it, ecco i dettagli:<br />
Nome: $name <br />

E-mail: $email<br />

Messaggio: $message<br />";

mail ($to,$subject,$messagebody,$headers);
$sent = "1";
}

?>
<div id="main">
    <div class="container">
        <div class="maincontent">
            <!--<div class="col-sm-4">
                <img src="img/contatti.jpg" alt="foto contatti">
            </div>-->
            <div class="col-sm-6 addressInfo">
            <div id="map_canvas" style="width:100%; height:400px;margin-bottom: 15px;">
            </div>
                <p>
                    Via del Paradiso, 27 - 53100 Siena
                </p>
                <p>
                    Telefono: +390577221011
                </p>
                <p>
                    Email: <a href="mailto:info@esteticaego.it">info@esteticaego.it</a>
                </p>                
                <p>
                    <a href="http://www.esteticaego.it/" title="Estetica Ego">www.esteticaego.it</a>
                </p>

            </div>
            <div class="col-sm-6">
                <h1>CONTATTACI</h1>
<?php
if ($sent=='1') {
echo <<<_END
<div class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>Messaggio inviato!</h4>
  Sarai ricontattato al pi&ugrave; presto.
</div>
_END;
}
?>
                <form action="contatti.php" method="post" id="contatti">
                    <div class="form-group">
                      <label for="name">Nome</label>
                      <input type="text" class="form-control" id="name" name="name" placeholder="Inserisci il tuo nome">
                    </div>
                    <div class="form-group">
                      <label for="email">Email</label>
                      <input type="email" class="form-control" id="email" name="email" placeholder="Inserisci la tua email">
                    </div>
                    <div class="form-group">
                      <label for="message">Messaggio</label>
                      <textarea class="form-control" rows="7" id="message" name="message"></textarea>
                    </div>
                    <div class="checkbox">
                        <label>
                          <input type="checkbox" value="Yep" name="privacy">
                            <a id="privacy" href="#" rel="tooltip" data-toggle="tooltip" title="Nel inviare i miei dati dichiaro di aver letto l'informativa e sono consapevole che il trattamento degli stessi è necessario per ottenere il servizio proposto. A tal fine, nel dichiarare di essere maggiorenne, fornisco il mio consenso." data-placement="right" style="font-size:14px;cursor: help;">
                                Informativa sulla Privacy ai sensi del D.Lgs 196/2003</a>
                        </label>
                    </div>
                    <button type="submit" class="btn btn-default" name="doSend" value="Invia">Invia</button>
                  </form>
            </div>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>