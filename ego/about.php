<?php
$pagename = 'about';
$pagetitle = 'Chi siamo';
$pagedesc = 'Vieni a farci visita, troverai uno staff preparato e disponibile che con la sua esperienza saprà accompagnarti nella ricerca del benessere.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<div id="main">
    <div class="container">
        <div class="maincontent">
            <div class="row">
                <div class="col-md-6 col-sm-5">
                    <img src="img/about.jpg" alt="Chi siamo">
                </div>
                <div class="col-md-6 col-sm-7">
                    <h1>CHI SIAMO</h1>
                    <p>
                        Gli alberi che crescono più forti e rigogliosi sono quelli con radici solide. Anche le radici di Ego sono 
                        solide, rinforzate da anni di esperienze professionali e collaborazioni.  Il centro estetico Ego nasce nel 
                        1997 in una posizione stupenda, a fianco della magnifica Piazza del Campo.
                    </p>
                    <p>
                        In quegli anni iniziano le collaborazioni con le migliori aziende di estetica sul mercato e Ego ha anche il 
                        piacere di collaborare con il celebre truccatore delle dive Gil Cagne.
                    </p>
                    <p>
                        Nel 2000 Ego si trasferisce in via del Paradiso, in un palazzo storico di grande suggestione, tra soffitti a 
                        volte e ambienti confortevoli. In questi locali inizia un percorso di formazione professionale con l'associazione 
                        A.M.I.A. (Associazione Medici Italiani Antiaging). Da questa esperienza il centro acquisisce la conoscenza di 
                        prodotti e tecnologie sempre più efficaci, al passo con la continua evoluzione di questo mondo.
                    </p>
                    <p>
                        Ancora oggi Ego si contraddistingue per la professionalità del suo staff, per la voglia di crescere e soddisfare 
                        le esigenze della clientela, rimanendo ancora oggi un punto di riferimento nel settore. La passione e la 
                        professionalità dello staff sono sempre al servizio dei clienti e fanno di Ego un centro accogliente, dove la cura 
                        della persona viene messa al primo posto, perché ogni cliente è unico.
                    </p>
                    <p>
                        Cerca anche tu il tuo angolo di paradiso!
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>