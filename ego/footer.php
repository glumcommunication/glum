            <div id="footer">
                <div class="container">
                    <div class="row">
               
                        <div class="col-sm-5">
                            <div class="row">
                            <div class="col-xs-6">
                                <p class="footerpr padding_f1">AL TUO SERVIZIO<br>EGO CENTRO BENESSERE<p>
                            </div>
                            <div class="col-xs-6  borderb">
                                
                                <p class="footerpl padding_f1" style="padding-bottom: 0;">p. iva: 00927620526<br>Via del Paradiso, 27<br>53100 - Siena</p>
                                <p id="faddress" class="footerpl padding_f1"  style="padding-top:0;">tel.: 0577.221011<br>email: info@esteticaego.it</p>
                            </div>
                            </div>
                        </div>
                        <div class="col-sm-2" style="text-align: center;margin: 15px auto;">
                            <img src="img/fblogo.png" alt="Facebook">
                        </div>
                        <div class="col-sm-5">
                            <div class="row">
                            <ul id="footermenu">
                                <li <?php if($pageName=='index'){echo 'class="sel"';} ?>><a href="index.php" title="Home">HOME</a></li>
                                <li <?php if($pageName=='about'){echo 'class="sel"';} ?>><a href="about.php" title="Il Progetto">CHI SIAMO</a></li>
                                <li <?php if($pageName=='servizi'){echo 'class="sel"';} ?>><a href="servizi.php" title="Puntate">SERVIZI</a></li>
                                <li <?php if($pageName=='gallery'){echo 'class="sel"';} ?>><a href="gallery.php" title="Contatti">GALLERY</a></li>
                                <li <?php if($pageName=='news'){echo 'class="sel"';} ?>><a href="news.php" title="Contatti">NEWS</a></li>
                                <li <?php if($pageName=='contatti'){echo 'class="sel"';} ?>><a href="contatti.php" title="Contatti">CONTATTI</a></li>
                            </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12"><h6 style="text-align: right;font-size: 10px;" class="credit"><a href="http://www.esteticaego.it/admin" target="_BLANK">Area Amministrazione</a></h6></div>  
        
                        <div class="col-sm-12"><h6 style="text-align: right;font-size: 10px;margin-top:15px;margin-bottom:7px;"><a href="http://www.glumcommunication.it">Crafted by GLuM Communication</a></h6></div>
                    </div>
                </div>
            </div>
        </div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>        
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).ready(function() {

	// validate signup form on keyup and submit
	var validator = $("#contatti").validate({
		rules: {
			name: "required",
                        surname: "required",
			phone: {
                required: false,
                minlength: 9
            },
                        message: "required",
                        privacy: "required",
			email: {
				required: true,
                minlength: 7,
                email: true
			}
		},
		messages: {
			name: "Campo obbligatorio - Inserisci il tuo nome",
                        surname: "Inserisci il tuo cognome",
			phone: {
				
				minlength: "Il numero deve essere composto da almeno 9 cifre"
			},
            message: "Inserisci un messaggio",
            privacy: "Accetazione informativa sulla privacy obbligatoria!<br> ",
			email: {
                required: "Campo obbligatorio - Inserisci un indirizzo email",
                minlength: "Inserisci un indirizzo email valido",
                email: "Inserisci un indirizzo email valido"
            }
			
		},
	});


});
</script>    
    </body>
</html>
