<?php
$pagename = 'gallery';
$pagetitle = 'Gallery';
$pagedesc = 'Le immagini del centro Estetica Ego e dei trattamenti che offre ai suoi clienti.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/lightbox-2.6.min.js"></script>
<div id="main">
    <div class="container">
        <div class="maincontent">
<?php
$figcap=array(
    "1" => "Estetica",
    "2" => "Lipomassage",
    "3" => "Viso",
    "4" => "Labbra",
    "5" => "Esperienza",
    "6" => "Professionalità",
    "7" => "Relax",
    "8" => "Massaggio",
    "9" => "Maschera",
    "10" => "Anti-Age",
    "11" => "Accoglienza",
    "12" => "Tecnologia",
    "13" => "Benessere",
    "14" => "Tonificazione",
    "15" => "Dimagrimento",
    "16" => "Esercizi",
    "17" => "Epilazione",
    "18" => "Il Centro",
    "19" => "Rassodamento",
    "20" => "Corpo",
    "21" => "Pedana vibrante",
    "22" => "Passione",
    );
 for ($i=1;$i<=22;$i++) {
     echo '<div class="col-sm-2 col-xs-4" style="margin:0 auto 10px;padding: 0 5px;"><figure class="cap-bot">';
     echo '<a href="img/gallery/big/ego_'.$i.'.jpg" data-lightbox="Ego Centro Benessere" title="'.$figcap[$i].'"><img src="img/gallery/tmb/ego_'.$i.'.jpg" alt="">';
     echo '<figcaption>'.$figcap[$i].'</figcaption></a>';
     echo '</figure></div>';
 }
?>            
            <div class="col-sm-4 col-xs-8" style="margin:0 auto 10px;padding: 0 5px;">
                <img src="img/frase-gallery1.png" alt="La bellezza...">
            </div>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>