            <div id="header">
                <div class="container">
                    <div class="row">
                        <a href="/" id="logourl">
                            <div id="logo" class="col-sm-4 col-xs-6">
                                <!--<img src="img/logo-ok.png">-->
                            </div>
                        </a>
                        <div class="col-sm-8 col-xs-6">
                            <div class="upperH col-md-4 col-md-offset-8 col-sm-5 col-sm-offset-7">
                                <p id="haddress">
                                    tel.: 0577.221011<br>
                                    email: <a href="mailto:info@esteticaego.it">info@esteticaego.it</a>
                                </p>
                            </div>
                            <div class="lowerH col-sm-12">
                                <nav class="navbar navbar-default" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
                                    <div class="navbar-header"><p>
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>
                                            

  <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="collapse navbar-collapse navbar-ex1-collapse">                                    
                                        <ul class="nav navbar-nav">
                                            <li <?php if($pageName=='index'){echo 'class="sel"';} ?>><a href="/" title="Home">home</a></li>
                                            <li <?php if($pageName=='about'){echo 'class="sel"';} ?>><a href="about.php" title="Il Progetto">chi siamo</a></li>
                                            <li <?php if($pageName=='servizi'){echo 'class="sel"';} ?>><a href="servizi.php" title="Puntate">servizi</a></li>
                                            <li <?php if($pageName=='gallery'){echo 'class="sel"';} ?>><a href="gallery.php" title="Contatti">gallery</a></li>
                                            <li <?php if($pageName=='news'){echo 'class="sel"';} ?>><a href="news.php" title="Contatti">news</a></li>
                                            <li <?php if($pageName=='contatti'){echo 'class="sel"';} ?>><a href="contatti.php" title="Contatti">contatti</a></li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>