<?php
$pagename = 'index';
$pagetitle = 'Home';
$pagedesc = 'Ristorante Osaka a Siena, cucina giapponese, cinese e internazionale. Sushi, wok, verdure, carne e pesce: mangi quello che vuoi ad un unico prezzo.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
<div id="main">
    <div class="container">
        <img src="img/osakahome.jpg" alt="Benventui da Osaka">
        <div class="row">
            <div class="intro">
                <h2>Benvenuti da Osaka</h2>
                <h1>IL PRIMO SUSHI WOK RESTAURANT IN CENTRO A SIENA</h1>
            </div>
        </div>
    </div>
    <div class="redstripe">
        <div class="container">
            <div class="homesquare col-xs-3">
                <h1>MENU' A<br>BUFFET</h1>
            </div>
            <div class="homesquare col-xs-2">
                <h1>Cinese</h1>
            </div>
            <div class="homesquare col-xs-2">
                <h1>Giapponese</h1>
            </div>
            <div class="homesquare col-xs-2">
                <h1>Italiano</h1>
            </div>
            <div class="homesquare col-xs-3">
                <h1>12 &euro; A PRANZO<br>20&euro; A CENA</h1>
            </div>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>
