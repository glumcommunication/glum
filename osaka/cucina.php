<?php
$pagename = 'cucina';
$pagetitle = 'La cucina';
$pagedesc = 'Sushi, sashimi, nigiri, uramaki e tempura dalla cucina giapponese. Ravioli, involtini, ramen e il meglio della cucina cinese. Pasta, carne e pesce cotti alla piastra dalla cucina internazionale.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
<div id="main">
    <div class="container">
        <div class="row">
            <div class="col-xs-6" id="leftside">
                <img src="img/cinese.jpg" alt="Cucina cinese">
            </div>
            <div class="col-xs-6" id="rightside">
                <h1>Cinese</h1>
                <p>
                    È la cucina etnica che per prima e più rapidamente si è diffusa nel resto del mondo. I sapori della pietanze tradizionali 
                    sono rielaborati ed adattati in chiave contemporanea. Il riso la fa ovviamente da padrone, ma la cucina cinese offre anche 
                    molte ricette con paste di diverso tipo come noodles o vermicelli di soia o riso. Maiale, pollo e manzo sono gli ingredienti 
                    preferiti per le portate principali, ma non mancano le verdure e piatti diventati ormai classici come gli involtini primavera 
                    o i ravioli cotti al vapore.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6" id="leftside">
                <img src="img/giapponese.jpg" alt="Cucina giapponese">
            </div>
            <div class="col-xs-6" id="rightside">
                <h1>Giapponese</h1>
                <p>
                    È nota per essere una delle cucine più bilanciate e salutari del mondo, parte importante della caratteristica longevità dei 
                    giapponesi, grazie soprattutto all'ampio uso di pesce fresco, verdure, radici e tè verde. Pietanze come il sashimi e il 
                    sushi negli ultimi anni sono entrate a far parte della consuetudine alimentare anche nel mondo occidentale. Si tratta di 
                    pietanze composte da pochi, semplici ingredienti ma che richiedono in realtà una grande abilità per la preparazione.
                    </p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6" id="leftside">
                <img src="img/italiano.jpg" alt="Cucina italiana">
            </div>
            <div class="col-xs-6" id="rightside">
                <h1>Internazionale</h1>
                <p>
                    I sapori tradizionali della cucina italiana hanno da tempo conquistato palati e chef di tutto il mondo. La cucina italiana è 
                    diventata una vera e propria base per tutti i professionisti della cucina internazionale, che la rielaborano unendo tradizione 
                    e innovazione, conservazione di antichi sapori e arricchimento tramite la sperimentazione di accostamenti nuovi. I primi piatti a 
                    base di pasta fanno la parte del leone, ma la cucina italiana è forse quella che in assoluto spazia maggiormente tra tutti i tipi di pietanze.
                </p>
            </div>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>