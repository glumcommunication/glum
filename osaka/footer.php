            
            <div class="push"></div>
        </div>
        <div id="footer">
                <div class="ftop">
                    <div class="container">
                        <div class="col-xs-5" id="one">
                            <p>
                                Via Pantaneto 107-109 - 53100 Siena<br>
                                Telefono: +39 0577 48166<br>
                                Cellulare: +39 334 7990002 - +39 327 7776290<br>
                                P. Iva: 01346310525
                            </p>
                        </div>
                        <div class="col-xs-4" id="two">
                            <p>
                                <a href="mailto:info@ristoranteosaka.com">info@ristoranteosaka.com</a>
                            </p>
                        </div>
                        <div class="col-xs-3" id="three">
                            <p>
                                <strong>Orario di Apertura</strong><br>
                                Tutti i giorni<br>
                                Dalle 12:00 alle 15:00<br>
                                e dalle 19:00 alle 23:00.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="fbottom">
                    Crafted by <a href="http://www.glumcommunication.it/" title="GLuM">GLuM Communication</a>
                </div>
            </div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>        
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/lightbox-2.6.min.js"></script>
<script>
$(document).ready(function() {

	// validate signup form on keyup and submit
	var validator = $("#contatti").validate({
		rules: {
			name: "required",
                        surname: "required",
			phone: {
                required: false,
                minlength: 9
            },
                        message: "required",
                        privacy: "required",
			email: {
				required: true,
                minlength: 7,
                email: true
			}
		},
		messages: {
			name: "Campo obbligatorio - Inserisci il tuo nome",
                        surname: "Inserisci il tuo cognome",
			phone: {
				
				minlength: "Il numero deve essere composto da almeno 9 cifre"
			},
            message: "Inserisci un messaggio",
            privacy: "Accetazione informativa sulla privacy obbligatoria!<br> ",
			email: {
                required: "Campo obbligatorio - Inserisci un indirizzo email",
                minlength: "Inserisci un indirizzo email valido",
                email: "Inserisci un indirizzo email valido"
            }
			
		},
	});


});
</script>    
    </body>
</html>
