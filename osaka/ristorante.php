<?php
$pagename = 'ristorante';
$pagetitle = 'Il ristorante';
$pagedesc = 'Osaka porta nel centro di Siena il meglio della tradizione della cucina giapponese, della cucina cinese e della cucina internazionale.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
<div id="main">
    <div class="container">
        <div class="col-xs-6" id="leftside">
            <img src="img/gal/osaka20.jpg" alt="Il ristorante">
        </div>
        <div class="col-xs-6" id="rightside">
            <h1>Il Ristorante</h1>
            <h2>Il primo Sushi Wok Restaurant nel Centro Storico di Siena</h2>
            <p>
                In pieno centro a Siena c'è un locale che riunisce la cultura alimentare dell'estremo oriente a quella occidentale. 
                Situato nella centralissima via Pantaneto, una delle strade più vive e trafficate del centro di Siena, Osaka giunge 
                ad arricchire la cultura gastronomica della città.
            </p>
            <p>
                Da Osaka la cucina tradizionale giapponese, la cucina tradizionale cinese e la cucina di stampo internazionale si 
                uniscono creando un mix in grado di accontentare ogni palato.
            </p>
            <p>
                Sia che vogliate fare una pausa pranzo diversa dal solito, sia che vogliate gustarvi un'ottima cena, il ristorante 
                Osaka rappresenta l'ambiente giusto per voi.
            </p>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>