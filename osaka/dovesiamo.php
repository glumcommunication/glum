<?php
$pagename = 'dovesiamo';
$pagetitle = 'Dove siamo';
$pagedesc = 'In pieno centro storico di Siena, ci trovate nella centralissima via Pantaneto 107-109.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
<div id="main">
    <div class="container">
        <div class="col-xs-6" id="leftside">
            <div id="map_canvas" style="width:100%; height:400px;margin-bottom: 15px;"></div>
        </div>
        <div class="col-xs-6" id="rightside" style="padding-top:120px;">
            <h2>Siamo in Via Pantaneto 107-109, 53100 Siena</h2>
            <h1>Orario di apertura</h1>
            
            <p style="text-align: center;font-size: 1.25em;">
                Siamo aperti tutti i giorni dalle 12:00 alle 15:00<br>e dalle 19:00 alle 23:00.
            </p>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>