<!doctype html>
<html>
    <head>
        <meta charset="utf-8" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <!--<script src="js/respond.js"></script>-->
        <link rel="icon" href="img/favicon.png" type="image/svg" />
        <link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Alegreya+Sans:400,500,700,400italic,500italic,700italic' rel='stylesheet' type='text/css'>
        <meta name="description" content="<?php echo $pagedesc ?>">
        <meta name="keywords" content="siena, sushi, sashimi, nigiri, uramaki, tempura, ramen, udon, riso, pasta, carne, pesce, griglia, piastra, ravioli, cucina tradizionale asiatica, all you can eat sushi, sushi menù, pranzo, cena, prezzo fisso, cucina cinese, cucina giapponese, asian fusion, cucina internazionale">
        <link href="css/lightbox.css" rel="stylesheet" />
        <link href="css/normalize.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet" media="screen">
        <script src="js/respond.js"></script>
        <script src="js/selectivizr-min.js"></script>
        
<?php
if ($pagename == "dovesiamo") {
    echo <<<_END
        
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    function initialize() {
        var latlng = new google.maps.LatLng(43.317179, 11.336075);
        var settings = {
            zoom: 15,
            center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
            navigationControl: true,
            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
            mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"), settings);

  var companyLogo = new google.maps.MarkerImage('img/positionshadow.png',
    new google.maps.Size(86,58),
    new google.maps.Point(0,0),
    new google.maps.Point(20,50)
);

var companyPos = new google.maps.LatLng(43.317179, 11.336075);
var companyMarker = new google.maps.Marker({
    position: companyPos,
    map: map,
    icon: companyLogo,
    //shadow: companyShadow,
    title:"Ristorante Osaka"
});
var contentString = '<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h1 id="firstHeading" class="firstHeading" style="font-size:14px;font-weight:bold;border-bottom: none;margin-bottom:5px;">Ristorante Osaka</h1>'+
    '<div id="bodyContent">'+
    '<p style="font-size:12px;">Via Pantaneto 107-109, 53100 Siena<br>Telefono: +39 0577 48166<br>Email: <a href="mailto:info@ristoranteosaka.com">info@ristoranteosaka.com</a><br>Website: <a href="http://www.ristoranteosaka.com" title="Ristorante Osaka">www.ristoranteosaka.com</a><br></p>'+
    '</div>'+
    '</div>';
 
var infowindow = new google.maps.InfoWindow({
    content: contentString
});
google.maps.event.addListener(companyMarker, 'click', function() {
  infowindow.open(map,companyMarker);
});
    }
    </script>
_END;
}

?>           
        <title><?php echo $pagetitle ?> | Ristorante Osaka - Cucina giapponese, cinese e italiana - Menù a buffet</title>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47178358-1', 'ristoranteosaka.com');
  ga('send', 'pageview');

</script>
    </head>
    <body <? if ($pagename == "dovesiamo") echo " onload=\"initialize()\"";?>>

        <div id="wrapper">

