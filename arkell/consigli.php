<?php
include_once 'cdb.php';
$thisPage = "Consigli";
$title = "Consigli";
include 'header.php';
?>

<div id="bigbox">

    <img src="images/consigli.jpg" alt="example"/>
    <div id="caption"><?php echo $title ?></div>

</div>


<div id="corpo">
    <a href="consiglio.php?id=1" title="$row[0]">
        <div class="project">
            <div class="project_title">
                Consiglio #1
            </div>                    
            <div class="project_cover">
                <img src="images/primoconsiglio.jpg" alt="test">
            </div>

        </div>
    </a>
    <a href="consiglio.php?id=2" title="$row[0]">
        <div class="project">
            <div class="project_title">
                Consiglio #2

            </div>                    
            <div class="project_cover">
                <img src="images/secondoconsiglio.jpg" alt="test">
            </div>

        </div>
    </a>
</div>

<?php
include 'footer.php';
?>