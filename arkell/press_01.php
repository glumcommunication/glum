<?php
$thisPage = "press_01";
$title = "Stil'è - Numero 16 - Aprile 2014";
include 'header.php';
?>

<div id="corpo" style="text-align:center;">
	<h1>Stil'è - Numero 16 - Aprile 2014</h1>
	<p>
		Nel numero di Aprile 2014 (Numero 16) Stil'è ha voluto, conoscere <strong>Gruppo Arkell</strong>, con un articolo presente nella sezione "Abitare e Costruire" a pagina 46.
		Il magazine <strong>Stil'è</strong>, allegato del quotidiano "Il Sole 24 Ore", si occupa di tutto ciò che è bello, ma anche di innovazione e tecnologia.
	</p>
	<p>Clicca sull'immagine in basso per leggere l'articolo completo.</p>
	<a href="http://www.gruppoarkell.it/images/press/arkell_stile.png" title="Articolo Stil'è" rel="lightbox[press]">
		<img src="http://www.gruppoarkell.it/images/press/arkell_stile_mini.png" alt="Stil'è Magazine">
	</a>
</div>

<?php
include 'footer.php';
?>
