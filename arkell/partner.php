<?php
$thisPage = "partner";
$title = "Partner";
include 'header.php';
?>
      
<div id="bigbox">
		
		   <img src="images/partner.jpg" alt="example"/>
		   <div id="caption"><?php echo $title ?></div>
		   
</div>


<div id="corpo">

<table id="partnerTable" class="five">
<tr><th colspan="5">PAVIMENTI E RIVESTIMENTI</th></tr>
<tr>
	<td style="text-shadow: 0 0 2px white; font-weight: bold;">COTTO</td>
	<td style="text-shadow: 0 0 2px white; font-weight: bold;">MARMI E PIETRE</td>
	<td style="text-shadow: 0 0 2px white; font-weight: bold;">PIASTRELLE</td>
	<td style="text-shadow: 0 0 2px white; font-weight: bold;">PAVIMENTI VINILICI</td>
	<td style="text-shadow: 0 0 2px white; font-weight: bold;">PAVIMENTI PER ESTERNI<br>INDUSTRIALI E GRANIGLIE</td>
</tr>
<tr>
	<td><a href="http://www.fornacepesci.it/" target="_blank">Fornace Pesci</a></td>
	<td><a href="http://www.iconci.it/" target="_blank">I Conci</a></td>
	<td><a href="http://www.porcelanosa.com/" target="_blank">Porcelanosa - Venis Gruppo</a></td>
	<td><a href="http://www.liuni.com/" target="_blank">Liuni</a></td>
	<td><a href="http://www.schlueter.it/" target="_blank">Schlüter-Systems</a></td>
</tr>
<tr>
	<td><a href="http://www.cottoetrusco.it/" target="_blank">Cotto Etrusco</a></td>
	<td><a href="http://www.antiquarex.com/" target="_blank">Antiquarex</a></td>
	<td><a href="http://www.kronosceramiche.it/" target="_blank">Kronos 2</a></td>
	<td><a href="http://www.plasticwood.it/" target="_blank">Plasticwood</a></td>
	<td><a href="http://www.plasticwood.it/" target="_blank">Plasticwood</a></td>
	</tr>
<tr>	
	<td><a href="http://www.ilferrone.it/" target="_blank">Il Ferrone</a></td>
	<td><a href="http://www.cavegontero.com/" target="_blank">Cave di Gontero</a></td>	
	<td><a href="http://www.atlasconcorde.it/" target="_blank">Atlas Concorde</a></td>
	<td><a href="http://www.lithosfloor.it/" target="_blank">Lithos</a></td>
	<td><a href="http://www.antoniazzi.it/" target="_blank">Antoniazzi</a></td>
</tr>
<tr>
	<td><a href="http://www.abcolors.it/" target="_blank">Ab Colors</a></td>
	<td><a href="http://www.aganippe.com/" target="_blank">Aganippe</a></td>
	<td><a href="http://www.ceramicheprovenza.com/" target="_blank">Provenza</a></td>
	<td>&nbsp</td>
	<td><a href="http://www.lasangiorgio.com/" target="_blank">La San Giorgio</a></td>
</tr>
<tr>
	<td><a href="http://anticcolonial.com/it" target="_blank">L'Antic Colonial</a></td>
	<td><a href="http://www.geopietra.it/" target="_blank">Geopietra</a></td>
	<td><a href="http://www.ergontile.it/" target="_blank">Ergon Ceramiche</a></td>
	<td>&nbsp;</td>
	<td><a href="http://www.mapei.it/" target="_blank">Mapei</a></td>
</tr>
<tr>
	<td><a href="http://www.emmeciterrecotte.com/" target="_blank">Camarri Cotto</a></td>
	<td><a href="http://anticcolonial.com/it" target="_blank">L'Antic Colonial</a></td>
	<td><a href="http://www.ceramicablu.com/" target="_blank">Ceramica Blu</a></td>
	<td>&nbsp;</td>
        <td>&nbsp;</td>	
</tr>
<tr>
	<td><a href="http://casabacconi.it/" target="_blank">I Bacconi Cotto</a></td>
	<td><a href="http://www.slate.it/" target="_blank">Artesia</a></td>
	<td><a href="http://www.marazzi.it/" target="_blank">Marazzi</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td><a href="http://www.francescodemaio.it/" target="_blank">Francesco De Maio</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
		<td><a href="http://www.pecchioliceramica.it/" target="_blank">Franco Pecchioli</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td><a href="http://www.tonalite.it/" target="_blank">Tonalite</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td><a href="http://www.cerdomus.com/home-it" target="_blank">Cerdomus Ceramiche</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td><a href="http://www.ermes-ceramiche.it/" target="_blank">ERMES Aurelia Ceramiche</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td><a href="http://www.cermariner.it/" target="_blank">Ceramica Mariner</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td><a href="http://www.acquarioceramiche.com/" target="_blank">Acquario Due</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td><a href="http://www.caesar.it/" target="_blank">Caesar</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td><a href="http://www.etruriadesign.it/" target="_blank">Etruria Ceramiche</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
</tr>
</table>
<table id="partnerTable" class="four">
<tr><th colspan="5">SANITARI</th></tr>
<tr>
	<td style="text-shadow: 0 0 2px white; font-weight: bold;">BOX DOCCIA</td>
	<td style="text-shadow: 0 0 2px white; font-weight: bold;">LAVABI<br>WC<br>BIDET<br>PIATTI DOCCIA</td>
	<td style="text-shadow: 0 0 2px white; font-weight: bold;">VASCHE</td>
	<td style="text-shadow: 0 0 2px white; font-weight: bold;">SAUNE ED IDROMASSAGGIO</td>

</tr>
<tr>
	<td><a href="http://megius.com/" target="_blank">Megius</a></td>
	<td><a href="http://www.azzurraceramica.it/" target="_blank">Ceramica Azzurra</a></td>
	<td><a href="http://www.vitaviva.it/" target="_blank">Vitaviva</a></td>
	<td><a href="http://www.effegibi.it/" target="_blank">Effegibi</a></td>

</tr>
<tr>
	<td><a href="http://www.provex.eu/" target="_blank">Provex</a></td>
	<td><a href="http://www.hatria.com/" target="_blank">Hatria</a></td>
	<td><a href="http://www.blubleu.it/" target="_blank">Blubleu</a></td>
	<td><a href="http://www.vitaviva.it/" target="_blank">Vitaviva</a></td>

</tr>
<tr>
	<td><a href="http://www.samo.it/" target="_blank">Samo</a></td>
	<td><a href="http://www.duravit.it/" target="_blank">Duravit</a></td>
	<td><a href="http://it.jacuzzi.eu/" target="_blank">Jacuzzi</a></td>	
	<td><a href="http://www.blubleu.it/" target="_blank">Blubleu</a></td>

</tr>
<tr>
	<td><a href="http://www.system-pool.com/" target="_blank">SystemPool</a></td>
	<td><a href="http://www.noken.com/" target="_blank">Noken</a></td>
	<td>&nbsp;</td>	
	<td><a href="http://it.jacuzzi.eu/" target="_blank">Jacuzzi</a></td>

</tr>
<tr>
	<td><a href="http://it.jacuzzi.eu/" target="_blank">Jacuzzi</a></td>
	<td><a href="http://www.falerii.it/" target="_blank">Faleri</a></td>
	<td>&nbsp;</td>
	<td><a href="http://grandform.it/" target="_blank">Grandform</a></td>

</tr>
<tr>
	<td>&nbsp;</td>
	<td><a href="http://www.pontegiulio.it/" target="_blank">Ponte Giulio Articoli per Disabili</a></td>
	<td>&nbsp;</td>
	<td><a href="http://www.system-pool.com/" target="_blank">SystemPool</a></td>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td><a href="http://www.sanitosco.it/" target="_blank">Sanitosco Articoli per Disabili</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>

</tr>
<tr>
	<td>&nbsp;</td>
	<td><a href="http://www.sbordoniceramica.com/" target="_blank">Sbordoni</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>

</tr>
<tr>
	<td>&nbsp;</td>
	<td><a href="http://www.ceramicaglobo.com/" target="_blank">Globo</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>

</tr>
<tr>
	<td>&nbsp;</td>
	<td><a href="http://www.idealstandard.it/" target="_blank">Ideal Standard</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>

</tr>
<tr>
	<td>&nbsp;</td>
	<td><a href="http://www.vitruvit.it/" target="_blank">Vitruvit</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>

</tr>
<tr>
	<td>&nbsp;</td>
	<td><a href="http://www.pozzi-ginori.com/" target="_blank">Pozzi - Ginori</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>

</tr>
</table>
<table id="partnerTable" class="one">
<tr><th colspan="1">RUBINETTERIA BAGNO E CUCINA</th></tr>
<tr>
	<td><a href="http://www.fantini.it/" target="_blank">Fantini</a></td>	
</tr>
<tr>
	<td><a href="http://www.gessi.it/" target="_blank">Gessi</a></td>	
</tr>
<tr>
	<td><a href="http://www.bongio.it/" target="_blank">Mario Bongio</a></td>	
</tr>
<tr>
	<td><a href="http://www.gattonirubinetteria.com/" target="_blank">Gattoni</a></td>	
</tr>
<tr>
	<td><a href="http://www.ponsi.com/" target="_blank">Ponsi Rubinetterie Toscane</a></td>
</tr>
</table>
<table id="partnerTable" class="one">
<tr><th colspan="1">WELLNESS SISTEMI DOCCIA/VASCA</th></tr>
<tr>
	<td><a href="http://www.fantini.it/" target="_blank">Fantini</a></td>	
</tr>
<tr>
	<td><a href="http://www.gessi.it/" target="_blank">Gessi</a></td>	
</tr>
<tr>
	<td><a href="http://www.calflex.it/" target="_blank">Calflex Srl</a></td>
</tr>
</table>
<table id="partnerTable" class="three">
<tr><th colspan="3">ARREDO BAGNO</th></tr>
<tr>
	<td style="text-shadow: 0 0 2px white; font-weight: bold;">MOBILI<br>SPECCHI<br>MENSOLE</td>
	<td style="text-shadow: 0 0 2px white; font-weight: bold;">ACCESSORI</td>
	<td style="text-shadow: 0 0 2px white; font-weight: bold;">TERMOARREDI E SCALDASALVIETTE</td>
	

</tr>
<tr>
	<td><a href="http://www.gruppoatma.com/it/arbiAz.html" target="_blank">Arbi by Gruppo ATMA</a></td>
	<td><a href="http://www.caos-srl.it/" target="_blank">Caos</a></td>
	<td><a href="http://www.brem.it/" target="_blank">Brem</a></td>
	

</tr>
<tr>
	<td><a href="http://www.arlexsrl.com/" target="_blank">Arlex</a></td>
	<td><a href="http://www.lineag.it/" target="_blank">Linea G</a></td>
	<td><a href="http://www.brandoni.com/" target="_blank">Brandoni</a></td>
	

</tr>
<tr>
	<td><a href="http://www.gruppoatma.com/it/compabAz.html/" target="_blank">Compab by Gruppo ATMA</a></td>
	<td><a href="http://www.house-design.it/" target="_blank">Metalbagno House Design</a></td>
	<td><a href="http://www.imaheating.it/" target="_blank">Ima</a></td>
	

</tr>
<tr>
	<td><a href="http://www.tullizuccari.com/" target="_blank">Tulli Zuccari</a></td>
	<td><a href="http://www.treesseci.it/" target="_blank">Treesseci</a></td>
	<td><a href="http://www.caos-srl.it/" target="_blank">Caos</a></td>
	

</tr>
<tr>
	<td><a href="http://www.mobilidicastello.com/" target="_blank">Mobili di Castello</a></td>
	<td><a href="http://www.kin.it/" target="_blank">Koh-I-Noor</a></td>	
	<td>&nbsp;</td>
	

</tr>
<tr>
	<td><a href="http://www.cerasa.it/" target="_blank">Cerasa</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	

</tr>
<tr>
	<td><a href="http://www.gama-decor.com/" target="_blank">Gamadecor</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	

</tr>
<tr>
	<td><a href="http://www.emmeuno.com/" target="_blank">Emmeuno</a></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	

</tr>
</table>
<table id="partnerTable" class="one">
<tr><th colspan="1">CAMINETTI E STUFE</th></tr>
<tr>
	<td><a href="http://www.palazzetti.it/" target="_blank">Palazzetti</a></td>
</tr>
<tr>
	<td><a href="http://www.mcz.it/" target="_blank">MCZ Caminetti</a></td>
</tr>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>&nbsp;</td>
</tr>
</table>
<table id="partnerTable" class="one">
<tr><th colspan="1">PORTE</th></tr>
<tr>
	<td><a href="http://www.adielleporte.it/" target="_blank">Adielle</a></td>
</tr>	

<tr>
	<td><a href="http://www.otcdoors.com/" target="_blank">OTC Porte</a></td>
</tr>

<tr>
	<td><a href="http://www.cristalsrl.com/" target="_blank">Cristal</a></td>
</tr>

<tr>
	<td><a href="http://www.doricacastelli.it/" target="_blank">Dorica Castelli</a></td>
</tr>

<tr>
	<td><a href="http://www.ferrerolegnoporte.it/" target="_blank">Ferrero Legno</a></td>
</tr>
<tr>
	<td><a href="http://www.metaporte.it/" target="_blank">Meta</a></td>
</tr>
</table>
<table id="partnerTable" class="one">
<tr><th colspan="1">INFISSI</th></tr>
<tr>
	<td><a href="http://www.albertini.it/" target="_blank">Albertini</a></td>
</tr>
<tr>
	<td><a href="http://www.faislegno.it/" target="_blank">Fais</a></td>
</tr>
<tr>
	<td><a href="http://www.schulzitalia.com/" target="_blank">Schulzitalia</a></td>
</tr>
<tr>
	<td><a href="http://www.cosmet.com/" target="_blank">Cosmet</a></td>
</tr>
</table>

<table id="partnerTable" class="one">
<tr><th colspan="1">SCALE</th></tr>
<tr>
	<td><a href="http://www.stscale.com/" target="_blank">ST Scale</a></td>
</tr>
<tr>
	<td><a href="http://www.novalineascale.it/" target="_blank">Novalinea</a></td>
</tr>
<tr>
	<td><a href="http://www.fantozziscale.com/" target="_blank">Fantozzi Scale</a></td>
</tr>
</table>
<table id="partnerTable" class="one">
<tr><th colspan="1">COLLANTI</th></tr>
<tr>
	<td><a href="http://www.adesital.it/" target="_blank">Adesital</a></td>
</tr>
<tr>
	<td><a href="http://www.mapei.it/" target="_blank">Mapei</a></td>
</tr>
<tr>
	<td><a href="http://www.schlueter.it/" target="_blank">Schlüter-Systems</a></td>
</tr>
</table>
<table id="partnerTable" class="one">
<tr><th colspan="1">PRODOTTI PER MANUTENZIONE E PULIZIA</th></tr>
<tr>
	<td><a href="http://www.faberchimica.com/" target="_blank">Faber Chimica</a></td>
</tr>
</table>
<table id="partnerTable" class="one">
<tr><th colspan="1">PROFILI E SISTEMI IMPERMEABILIZZANTI</th></tr>
<tr>
	<td><a href="http://www.butech.es/" target="_blank">Butech (Porcelanosa)</a></td>
</tr>
<tr>
	<td><a href="http://www.schlueter.it/" target="_blank">Schlüter-Systems</a></td>
</tr>	
<tr>
	<td><a href="http://www.arcansas.it/" target="_blank">Arcansas Profili</a></td>
</tr>	

</table>
<table id="partnerTable" class="one">
<tr><th colspan="1">LEGNO</th></tr>
<tr>
	<td><a href="http://www.itlas.it/" target="_blank">Itlas</a></td>
</tr>
<tr>
	<td><a href="http://www.lithosfloor.it/" target="_blank">Lithos</a></td>
</tr>
<tr>
	<td><a href="http://www.salis.it/" target="_blank">Salis</a></td>
</tr>
<tr>
	<td><a href="http://www.originalparquet.com/" target="_blank">Original Parquet</a></td>
</tr>
</table>
</div>
 <div style="height:100px;width: 1024px; float:left;">&nbsp;</div>
<?php
include 'footer.php';
?>