<?php
$thisPage = "carbonfree";
$title = "Siena Carbon Free";
include 'header.php';
?>
      
<div id="bigbox">
		
		   <img src="images/nuovotettoarkell.jpg" alt="Siena Carbon Free"/>

		   
</div>


<div id="corpo">
<p>
Il <strong>Gruppo Arkell</strong>, da sempre attento al confort e al benessere degli spazi interni e più intimi delle abitazioni, ha voluto dimostrare lo stesso impegno e interesse per l'ambiente che ci circonda 
scegliendo di aderire al progetto “Siena Carbon Free”, promosso dalla provincia di Siena. Lo ha fatto attraverso le opere di bonifica del tetto in eternit e la sua sostituzione con pannelli fotovoltaici 
che permetteranno la riduzione delle emissioni inquinanti di CO2, l'obbiettivo è quello di raggiungere un abbattimento del 38,3% entro il 2015.<br />
Tale opera ha permesso al Gruppo Arkell di diventare uno dei <a href="http://green.terresiena.it/it/progetto/carbon-free/protagonisti-carbon-free/Utente-Articolo/152-carbon-free/71-carbon+free/256-gruppo-arkell-s-r-l-" target="_blank">protagonisti Carbon Free</a> e di potersi fregiare del marchio “Carbon Free 2015”.<br />
Aver contribuito al miglioramento di una piccola, seppure importante, parte del nostro territorio ci emoziona e ci gratifca, sperando che iniziative di questo tipo possano essere in futuro sempre più diffuse e 
intraprese.
</p>

<img src="images/logo_carbon.png" alt="Logo Carbon Free" style="float:left;margin-bottom:10px; margin-right:10px;">
<p style="">
Il progetto “Carbon Free 2015” coinvolge imprese, enti pubblici e privati e persegue l'obbiettivo di azzerare il bilancio territoriale delle emissioni climalteranti, 
con lo scopo di arrivare ad essere nel 2015 la prima area vasta ad emissioni zero. Tale scopo viene perseguito attraverso una serie di iniziative che vedono coinvolti sia il pubblico che il privato: 
partendo dalla redazione di un nuovo Piano Energetico Provinciale, per arrivare all'erogazione di incentivi per l'efficientamento energetico e per favorire l'approvigionamento da fonti di energia rinnovabili. 
Al progetto sono invitati ad aderire i soggetti economici del territorio che completeranno la globalità delle iniziative per arrivare all'ambizioso traguardo di <strong>un territorio a zero emissioni</strong>.


</p>

	 
</div>
 
<?php
include 'footer.php';
?>