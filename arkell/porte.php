<?php
$thisPage = "porte";
$title = "Porte";
include 'header.php';
?>
      
<div id="bigbox">
		
		   <img src="images/porte.jpg" alt="example"/>
		   <div id="caption"><?php echo $title ?></div>
		   
</div>


<div id="corpo">
	<p>La porta è un elemento di passaggio da un ambiente ad un altro. L'oggetto attraverso il quale si mostra 
	l'arredamento. Il suo modo di manifestarsi preannuncia la casa stessa, il suo clima e il suo stile. 
	Gruppo Arkell è in grado di rispettare questa sua natura proponendo soluzioni che garantiscono funzionalità 
	e sicurezza senza trascurare il design e l'eleganza.</p>
	<p>Fornitori porte: Dorica Castelli, Ferrero Legno, OTC Porte. <br />
	Fornitori porte di design: Adielle, Cristal. <br />
	Fornitori porte di sicurezza: Meta. <br />
	</p>
</div>
 
<?php
include 'footer.php';
?>