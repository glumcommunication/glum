<?php
$thisPage = "pavimenti";
$title = "Pavimenti";
include 'header.php';
?>
      
<div id="bigbox">
				   <div id="caption"><?php echo $title ?></div>
		   <img src="images/parquet.jpg" alt="example"/>

		   
</div>
<div id="corpo">
	<p>Gruppo Arkell offre ogni tipologia di pavimenti: dai più classici che uniscono il legno alla ceramica, al marmo, 
	fino ad arrivare a quelli più innovativi in gres porcellanato in vari spessori e dimensioni.<br /> 
	Soluzioni uniche per ogni ambiente abitativo, dai bagni alle cucine. Soluzioni personalizzate che rifletteranno i 
	vostri gusti e rappresenteranno vere e proprie opere d’arte in casa vostra.</p> 
		
	<p><strong>COTTO E PAVIMENTI ESTERNI</strong><br />
	Il pavimento in cotto si adegua ad ogni esigenza: sicuro, duttile, resistente, pensato per essere vissuto in 
	tutta tranquillità. I continui sviluppi della ricerca garantiscono soluzioni sempre più affidabili e adattabili ad 
	ogni esigenza senza alterare la sua naturalezza e e le sue origini toscane.<br />
	Gruppo Arkell offre una vasta gamma di scelte e soluzioni per l’interno e l’esterno, dalle forme arrotate, 
	levigate, fatte a mano, smaltate fino ad arrivare alle più recenti applicazioni sulle facciate e sulle pareti 
	ventilate. Pavimenti per esterni in resina, pietra ricostruita,  cemento, clinker.</p>
	<p>Fornitori: I Bacconi Cotto, Il Ferrone, Cotto Etrusco, Fornace Pesci, Camarri Cotto,  Antoniazzi, 
	Plasticwood, Klinker Sire.</p>
		
	<p><strong>MARMO, PIETRE NATURALI  E PIETRA RICOSTRUITA</strong><br />
	Marmi, travertino, graniti e pietre naturali quali porfido, ardesia, pietra serena sono solo alcuni dei tanti 
	prodotti con cui  realizzare gli ambienti della vostra casa. Da Gruppo Arkell potrete scoprire la bellezza e 
	l’affidabilità dei prodotti naturali che da sempre vengono utilizzati per progetti e per l'arredamento.</p>
	<p>Fornitori: Antiquarex, Idealmarmi Italia (I Conci), Artesia, Saint Pierre, Aganippe, Geoopietra,
	Cave di Gontero.</p>
	
	<p><strong>PARQUET E PAVIMENTI VINILICI</strong><br />
	Il parquet è un pavimento adatto negli ambienti più svariati, da quelli privati a quelli pubblici, agli 
	ambienti interni e a quelli esterni, il legno esprime versatilità d'uso, capacità di adattarsi a tutti 
	gli stili di arredo e un'eleganza che lo distingue da qualunque altro materiale.<br />
	Da Gruppo Arkell troverai pavimenti in legno massello in essenze esotiche ed europee, tutte per 
	accontentare i gusti più tradizionali senza trascurare le intramontabili soluzioni. Gruppo Arkell 
	garantisce anche professionisti del montaggio, che realizzeranno un bellissimo pavimento in legno 
	in casa vostra.</p>
	<p>Fornitori: Itlas, Lithos, Original Parquet, Salis, Liuni, Plasticwood.</p>
  </div>

<?php
include 'footer.php';
?>