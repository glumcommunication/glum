<?php
$thisPage = "crepet";
$title = "Meeting Terre di Cuore - Ospite Paolo Crepet";
include 'header.php';
?>
      
<div id="bigbox">
		
		   <img src="images/lucisullavoroheader.jpg" alt="Luci sul lavoro 2013"  style="height:160px;" />

		   
</div>


<div id="corpo">
    <p>
        <strong>GRUPPO ARKELL</strong> ha aderito con successo all’iniziativa <strong>LUCI SUL LAVORO</strong>, la manifestazione interamente dedicata al mondo del lavoro, che si è svolta all’interno della fortezza Medicea di 			Montepulciano dal 4 al 6 luglio.<br>
        Tre giornate ricche di eventi che hanno raccontato in un modo del tutto originale il mondo del lavoro attraverso proiezioni cinematografiche, concerti musicali, rappresentazioni teatrali, incontri con autori, ma anche
        convegni, workshop, laboratori per i giovani e tavole rotonde.
    </p>
    <p>
        Sabato 6 luglio, alle ore 10 si è svolto il dibattito "<strong>CONCILIAZIONE DI VITA E LAVORO. IL WELFARE AZIENDALE.</strong>" dove è intervenuta <strong>LAURA FARASIN</strong>.
    </p>
    <p>
        Hanno partecipato all’incontro: Jole Santelli (Sottosegr. Min. Lavoro e Politiche Sociali), Giuseppe Acocella (CNEL), Cinzia Bonfrisco (Senato della Repubblica), Bianca Cuciniello (Groupama), Francesco Ferri (Resp. 
        Leadership & Management Giovani Imprenditori Confindustria), Maurizio Petriccioli (CISL), Daniela Palazzi (Vice Pres. Coop Centro Italia), Gianluca Magnani (Responsabile HR Novartis), Patrizia De Luise (Confesercenti), 
        Simonetta Pellegrini (Ass. Lavoro Prov. di Siena). Moderatrice Debora Rosciani (Radio24).
    </p>



<div class="sponsor" style="width:100%;background-color:#394b69;">
    <table class="sponsortable" style="margin: 0 auto;width: 100%;">
        <tr>
            <th colspan="9">Multimedia
            </th>		
        </tr>
    </table>
    <div id="video" style="width: 446px; padding:3px;margin-top:25px; float:left;">
        <iframe width="446" height="251" src="//www.youtube-nocookie.com/embed/WbYTFM9ynxk?rel=0&wmode=opaque" frameborder="0" allowfullscreen></iframe>
    </div>
    <div id="pics" style="width:565px;float:left;">
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_01.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_01.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_02.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_02.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_03.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_03.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_04.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_04.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_05.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_05.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_06.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_06.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_07.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_07.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_08.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_08.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_09.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_09.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_10.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_10.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_11.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_11.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_12.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_12.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_13.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_13.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_14.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_14.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_15.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_15.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_16.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_16.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_17.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_17.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_18.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_18.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_19.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_19.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/lucisullavoro/lucisullavoro_big_20.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/lucisullavoro/lucisullavoro_20.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
    </div>

</div>

</div>
 
<?php
include 'footer.php';
?>