<?php
$thisPage = "mii";
$title = "More in Italy";
include 'header.php';
?>

<div id="bigbox">
		
		   <img src="images/mii2header.jpg" alt="More in Italy"  style="height:250px;" />

		   
</div>


<div id="corpo">
<p>
<strong>More in Italy</strong> di nuovo a Siena, venerdì 21 marzo a partire dalle ore 18 presso <strong>Il Piccolo Castello</strong> a Monteriggioni (SI).<br>

More in Italy in collaborazione con Banca Mediolanum ha messo in piedi un progetto ambizioso e concreto per il quale <strong>Gruppo Arkell</strong> rappresenta un fondamentale punto di riferimento sul territorio. Visto il successo della prima edizione, l'evento torna nel territorio senese per presentare tutti i vantaggi per chi vuole ristrutturare la propria abitazione e per i professionisti del settore che sono interessati a saperne di più.<br>

Il progetto “Mii” parte da una base fondamentale quanto semplice: la soluzione per migliorare ciò che ci sta intorno è alla nostra portata, molto più vicino di quanto crediamo. Si tratta di migliorare la nostra casa, di ripartire rendendo migliore lo spazio in cui viviamo.<br>
Migliorando la nostra casa miglioreremo l'economia, miglioreremo il nostro paese, miglioreremo il futuro dei nostri figli.<br>

Vantaggi concreti:</p>
<ul>
	<li>1) Finanziamento di Banca Mediolanum a tassi bassissimi.</li>

	<li>2) Rimborso fino al 65% dell’investimento.</li>

	<li>3) Promozioni e sconti esclusivi garantiti dalle tante aziende che aderiscono al progetto “Mii”.</li>
</ul>

	<div id="regForm" style="width:1024px; ">
	<h1 class="eventTitle">Multimedia</h1>
	<div id="video" style="width: 446px; padding:3px;margin-top:25px; float:left;">
		<iframe width="446" height="251" src="//www.youtube.com/embed/FXD2joYQrkw" frameborder="0" allowfullscreen></iframe>
	</div>
	<div id="pics" style="width:565px;float:left;">
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_01.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_01.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_02.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_02.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_03.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_03.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_04.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_04.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_05.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_05.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_06.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_06.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_07.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_07.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_08.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_08.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_09.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_09.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_10.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_10.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_11.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_11.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_12.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_12.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_13.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_13.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_14.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_14.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_15.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_15.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_16.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_16.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_17.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_17.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_18.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_18.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_19.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_19.jpg" alt="More In Italy 2"></a>
		</div>
		<div class="pic" style="width:107px;padding:3px;float:left;">
			<a href="images/mii2/mii2_big_20.jpg" rel="lightbox[MoreInItaly]" title=""><img src="images/mii2/tmb/mii2_20.jpg" alt="More In Italy 2"></a>
		</div>
	</div>
<?php
/*if ($_POST['doSend']=='Invia') {
$name = $_POST[name];
$surname = $_POST[surname];    
$email = $_POST[email];
$phone = $_POST [phone];    
$message = $_POST[message];
$headers = "";
$headers .= "From: $email\n";
$headers .= "Reply-To: $email\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=utf-8\n";
$to = "gruppoarkell@gmail.com";
$subject = "Richiesta informazioni";
$messagebody = "Hai ricevuto una nuova email via gruppoarkell.it attraverso il modulo di iscrizione a More in Italy, ecco i dettagli:<br />
Nome: $name <br />
Cognome: $surname <br />
E-mail: $email<br />
Telefono: $phone <br />
Messaggio: $message<br />";

mail ($to,$subject,$messagebody,$headers);
$sent = "1";
}
if ($sent=='1') {
echo <<<_END
<div class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>Messaggio inviato!</h4>
  Sarai ricontattato al pi&ugrave; presto.
</div>
_END;
}*/
?>

                    <!--<form action="moreinitaly2.php" method="post" id="contactform">
					<div class="form-group">
					  <label for="name">Nome</label>
					  <input type="text" class="form-control" id="name" name="name" placeholder="Nome">
					</div>
					<div class="form-group">
					  <label for="surname">Cognome</label>
					  <input type="text" class="form-control" id="surname" name="surname" placeholder="Cognome">
					</div>
					<div class="form-group">
					 <label for="email">Email</label>
					  <input type="email" class="form-control" id="email" name="email" placeholder="Email">
					</div>
					<div class="form-group">
					  <label for="phone">Telefono</label>
					  <input type="phone" class="form-control" id="phone" name="phone" placeholder="Telefono">
					</div>
					<div class="form-group">
					<label for="message">Messaggio</label>
					  <textarea class="form-control" rows="7" id="message" name="message" placeholder="Messaggio"></textarea>
					</div>
					<div class="checkbox">
						<label>
						  <input type="checkbox" value="Yep" name="privacy" style="width:20px;">
							<a id="privacy" href="#" rel="tooltip" data-toggle="tooltip" title="Nel inviare i miei dati dichiaro di aver letto l'informativa e sono consapevole che il trattamento degli stessi è necessario per ottenere il servizio proposto. A tal fine, nel dichiarare di essere maggiorenne, fornisco il mio consenso." data-placement="right" style="font-size:12px;cursor: help;font-weight:400;">
								Informativa sulla Privacy ai sensi del D.Lgs 196/2003</a>
						</label>
					</div>
					<div style="width:100%;text-align:center;margin:10px auto;"><button type="submit" class="btn btn-default" name="doSend" value="Invia">Invia</button></div>
				  </form>-->
</div>
<div id="sponsor" style="width:1024px;margin-bottom:50px;float:left;">
	<h1 class="eventTitle">Partner</h1>
	<ul>
		<li style="float:left; width: 25%;text-align:center;"><img src="images/moreinitaly/provenza.jpg" alt="Provenza"></li>
		<li style="float:left; width: 25%;text-align:center;"><img src="images/moreinitaly/atlas.jpg" alt="Atlas Concorde"></li>
		<li style="float:left; width: 25%;text-align:center;"><img src="images/moreinitaly/shulz.jpg" alt="Schulz Italia"></li>
		<li style="float:left; width: 25%;text-align:center;"><img src="images/moreinitaly/azzurra.jpg" alt="Azzurra"></li>
	</ul>

</div>

</div>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<!--<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>
$(document).ready(function() {

	// validate signup form on keyup and submit
	var validator = $("#contactform").validate({
		rules: {
			name: "required",
			surname: "required",
			phone: {
				required: true,
				minlength: 9
			},
			message: {
				required: false
			},
			privacy: "required",
			email: {
				required: true,
				minlength: 7,
				email: true
			}
		},
		messages: {
			name: "Campo obbligatorio - Inserisci il tuo nome",
			surname: "Campo obbligatorio - Inserisci il tuo cognome",
			phone: {
				required: "Campo obbligatorio - Inserisci il tuo numero di telefono",
				minlength: "Il numero deve essere composto da almeno 9 cifre"
			},
			message: "Inserisci un messaggio",
			privacy: "Accetazione informativa sulla privacy obbligatoria!<br> ",
			email: {
				required: "Campo obbligatorio - Inserisci un indirizzo email",
				minlength: "Inserisci un indirizzo email valido",
				email: "Inserisci un indirizzo email valido"
			}
			
		},
	});


});
</script>->
<?php
include 'footer.php';
?>
