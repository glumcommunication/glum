<?php
$thisPage = "servizi";
$title = "Servizi";
include 'header.php';
?>
      
<div id="bigbox">
		
		   <img src="images/servizi.jpg" alt="servizi"/>
		   <div id="caption"><?php echo $title ?></div>
		   
</div>


<div id="corpo">
	<p>GRUPPO ARKELL è in grado di offrire importanti servizi che superano ed ampliano il tradizionale 
	concetto della vendita:<br />  
	Progettazione di interni<br />
	Assistenza in cantiere<br />
	Opere di posa con nuove tecnologie<br />
	Ricerca e sviluppo su materiali di ultima generazione<br /> 
	Possibilità di pagamenti rateali. 
	
	</p>
</div>
 
<?php
include 'footer.php';
?>