<?php
$thisPage = "crepet";
$title = "Meeting Terre di Cuore - Ospite Paolo Crepet";
include 'header.php';
?>
      
<div id="bigbox">
		
		   <img src="images/eventocrepet.jpg" alt="Evento Maggio 2013"  style="height:361px;" />

		   
</div>


<div id="corpo">
<p>
<strong>GRUPPO ARKELL</strong> in collaborazione con <strong>MARINER CERAMICHE</strong> e <strong>PORCELANOSA GRUPO</strong>,<br>
è lieta di invitarla al secondo appuntamento con "TERRE DI CUORE", meeting imprenditoriale per la prosperità economica della Provincia di Siena.
</p>
<p>
Ospite d'onore dell'iniziativa il <strong>Prof. Paolo Crepet</strong>, noto medico ed esperto di psichiatria, autore di numerose pubblicazioni in materia e Direttore 
scientifico della Scuola per Genitori a Vicenza, Treviso, Forlì, Cesena, Trento, Brescia, Terni, Oristano.<br>
L'esperienza di Crepet fungerà da guida autorevole per una riflessione collettiva sul rapporto tra genitori e figli in un momento storico di crisi 
economica caratterizzato dall'assenza di punti di riferimento. Per una nuova fase di sviluppo e per un futuro migliore la nostra società non 
potrà fare a meno dalle potenzialità presenti nelle nuove generazioni.<br>
Non c'è più tempo: il futuro è già domani.
</p>
<!--<p>Per permetterci di riservarLe la migliore accoglienza,<br>
La preghiamo di confermare la Sua partecipazione <strong>entro il 30 aprile 2012</strong> ai seguenti recapiti:<br>
<a href="mailto:alessandra@glumcommunication.it">alessandra@glumcommunication.it</a><br>
Tel.: <strong>0577 280213</strong>
	</p>-->
<div class="sponsor" style="width:100%;background-color:#394b69;">
    <table class="sponsortable" style="margin: 0 auto;width: 100%;">
        <tr>
            <th colspan="9">Download</th>		
        </tr>
        <tr>
            <td colspan="3">
                <p>
                        
                        
                <a href="pdf/rassegnastampa_crepet_terredicuore.pdf" title="Download">
                        <span><img src="images/pdfdownload.png" width="32" height="32" alt="Pdf" style="height:32px;width:32px;vertical-align: middle;" /></span>
                         Scarica la rassegna stampa</a></p>
            </td>
            <td colspan="3">
                <p>
                    <a href="pdf/programma_terredicuore_crepet.pdf" title="Download">
                    <span><img src="images/pdfdownload.png" width="32" height="32" alt="Pdf" style="height:32px;width:32px;vertical-align: middle;" /></span>
                    Scarica il programma dell'evento</a>
                </p>
            </td>
            <td colspan="3">
                <p>
                        
                        
                <a href="pdf/invito_terredicuore_crepet.pdf" title="Download">
                        <span><img src="images/pdfdownload.png" width="32" height="32" alt="Pdf" style="height:32px;width:32px;vertical-align: middle;" /></span>
                         Scarica l'invito all'evento</a></p>
            </td>
        </tr>
    </table>
</div>

<div class="sponsor" style="width:100%;background-color:#394b69;">
    <table class="sponsortable" style="margin: 0 auto;width: 100%;">
        <tr>
            <th colspan="9">Multimedia
            </th>		
        </tr>
    </table>
    <div id="video" style="width: 446px; padding:3px;margin-top:50px; float:left;">
        <iframe width="446" height="250" src="http://www.youtube.com/embed/pv-0GRqDyZ8?wmode=opaque" frameborder="0" allowfullscreen></iframe>
    </div>
    <div id="pics" style="width:565px;float:left;">
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_01.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_01.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_02.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_02.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_03.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_03.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_04.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_04.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_05.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_05.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_06.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_06.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_07.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_07.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_08.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_08.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_09.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_09.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_10.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_10.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_11.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_11.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_12.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_12.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_13.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_13.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_14.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_14.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_15.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_15.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_16.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_16.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_17.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_17.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_18.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_18.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_19.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_19.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/terredicuore/crepet_terredicuore_big_20.jpg" rel="lightbox[TerrediCuoreEventoCrepet]" title=""><img src="images/terredicuore/crepet_terredicuore_20.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
    </div>
</div>
<div class="sponsor" style="width:100%;background-color:#394b69;float:left;">
<table class="sponsortable" style="margin: 0 auto;">
	<tr>
		<th colspan="9">Sponsor</th>		
	</tr>
	<tr>
		<td><a href="http://www.chiantibanca.it/" target="_blank"><img src="images/terredicuore/chiantibancabcc.png" width="93" height="64" alt="ChiantiBanca BCC"></a></td>
		<td><a href="http://www.glumcommunication.it/" target="_blank"><img src="images/terredicuore/glum.png" width="93" height="64" alt="Glum Communication"></a></td>
		<td><a href="http://www.lasovranapulizie.it/" target="_blank"><img src="images/terredicuore/lasovrana.png" width="93" height="64" alt="La Sovrana Pulizie"></a></td>	
		<td><a href="http://lodicocostruzioni.it/" target="_blank"><img src="images/terredicuore/lodicosanto.png" width="93" height="64" alt="Lodico Santo Costruzioni"></a></td>
        <td><a href="http://www.giuntiumberto.it/" target="_blank"><img src="images/terredicuore/giunti.png" width="93" height="64" alt="Giunti"></a></td>
		<td><a href="http://www.falegnameriapironti.it/" target="_blank"><img src="images/terredicuore/pironti.png" width="93" height="64" alt="Falegnameria Pironti"></a></td>
		<td><a href="http://www.tecnoservicesiena.com/" target="_blank"><img src="images/terredicuore/tecnoservice.png" width="93" height="64" alt="Tecno Service"></a></td>
		<td><a href="http://www.mytours.it/" target="_blank"><img src="images/terredicuore/mytour.png" width="93" height="64" alt="My Tour"></a></td>	
		<td><a href="http://www.agriturismoilciliegio.com/" target="_blank"><img src="images/terredicuore/ciliegio.png" width="93" height="64" alt="Azienda Agricola Il Ciliegio"></a></td>
				
	</tr>
	<tr>
		<th colspan="6">Partner</th>
        <th colspan="3">Media Partner</th>
	</tr>
	<tr>
		<td colspan="3" style="text-align:center;"><a href="http://www.porcelanosa.com/" target="_blank"><img src="images/terredicuore/porcelanosa.png" width="220" height="150" alt="Porcelanosa"></a></td>
        <td colspan="3" style="text-align:center;"><a href="http://www.cermariner.it/" target="_blank"><img src="images/terredicuore/mariner.png" width="220" height="150" alt="Ceramiche Mariner"></a></td>
        <td colspan="3" style="text-align:center;"><a href="http://www.antennaradioesse.it" target="_blank"><img src="images/terredicuore/antennaradioesse.png" width="220" height="150" alt="Antenna Radio Esse"></a></td>
	</tr>
	<tr>
				
	</tr>
	<tr>
		
	</tr>
</table>
</div>
</div>
 
<?php
include 'footer.php';
?>