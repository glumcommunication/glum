<!--<div class="push"></div>-->

<div id="footer">
	<div class="copyright">
		<table>
			<tr>
				<td><strong>Gruppo Arkell</strong></td>
				<td>Sede di <strong>Colle di Val d'Elsa</strong></td>
				<td>Sede di <strong>Badesse</strong></td>
			</tr>
			<tr>
				<td>Email: <strong><a href="mailto:info@gruppoarkell.it">info@gruppoarkell.it</a></strong></td>
				<td>Via Selvamaggio 21/A</td>
				<td>Via Pietro Nenni 16</td>
			</tr>	
			<tr>
				<td>P.I.: 01222250522</td>
				<td>53034 Colle di Val d'Elsa (SI)</td>
				<td>Località Badesse, 53035 Monteriggioni (SI)</td>
			</tr>		
			<tr>
				<td>&nbsp;</td>
				<td>Telefono: 0577 924618 - Fax: 0577 924625</td>
				<td>Telefono: 0577 309058 - Fax: 0577 309141</td>
			</tr>		
		</table>
<!--		<p><strong>Gruppo Arkell</strong> - Loc. Renaccio, Via della Pace 13 53100 Siena<br />
		Tel. 0577 532037 | Fax. 0577 225405 | Email info@centroinfissisiena.com<strong> </strong>|  P.I. 01112510522</p>-->
	</div>
   <div class="footer_links">
   	<div class="footer_autori">
      	<p><a href="http://www.gruppoarkell.it/admin/login.php">Area Riservata</a></p>
      	<p style="text-align:right;font-size:12px;">Crafted by <a href="http://www.glumcommunication.it/" target="_BLANK">GLuM Communication</a></p>
      </div>
    </div>
  </div>
</div> 
<!--end of main container-->
</body>
</html>
