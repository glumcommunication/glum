<?php
$thisPage = "arredobagno";
$title = "Arredo bagno";
include 'header.php';
?>
      
<div id="bigbox">
		
		   <img src="images/rubinetteria.jpg" alt="example"/>
		   <div id="caption"><?php echo $title ?></div>
		   
</div>


<div id="corpo">
	<p>Il bagno è il luogo più intimo della casa, lo spazio destinato al proprio benessere, e come tale il suo 
	arredamento deve rispecchiare la personalità e lo stile di chi lo abita. Gruppo Arkell propone soluzioni 
	originali e inconfondibili, soluzioni per un ambiente arredato in maniera confortevole e unico nelle forme e 
	nel design.<br />
	I mobili e gli accessori proposti da Gruppo Arkell nascono da materiali pregiati, lavorazioni accurate, tecnologie 
	innovative ed un design che riesce ad individuare sempre la migliore soluzione per qualsiasi ambiente.<br /> 
	Presso Gruppo Arkell troverete una gamma vasta e qualificata di prodotti che possono rendere il vostro bagno 
	un ambiente emozionale , cambiando così radicalmente il vostro modo di vivere.<br />
	Troverete mobili ed accessori intelligenti che fanno dello stile e della qualità il proprio carattere, 
	inoltre grazie alla loro versatilità riescono ad inserirsi in qualsiasi ambiente e spazio.<br />
	Fornitori: Arbi ArredoBagno, Arlex, Caos, Cerasa, Compab, Emmeuno, Gamadecor, Linea G, 
	Housedesign, Mobili di Castello, Tulli Zuccari.</p>
	
	<p><strong>CABINE DOCCIA  E VASCHE DA BAGNO</strong><br />
	Il benessere non ha bisogno di grandi spazi. Anche per quelli più piccoli esistono soluzioni che permettono 
	l'installazione di una vasca idromassaggio o di un box-doccia che includa tutte le funzionalità così da 
	trasformare la classica “doccia” in un momento nuovo e piacevole che non ha più niente a che vedere con il suo 
	significato tradizionale.</p>
	<p>Fornitori: Provex, Samo, Megius, SystemPool, Jacuzzi</p>
	
	<p><strong>RUBINETTI E RADIATORI</strong><br />
	Non necessariamente funzionalità e sostenibilità devono opporsi al bello. Lavabi  e sanitari hi-tech e 
	multifunzionali diventano anche oggetti di design e di decoro. I rubinetti rappresentano il connubio perfetto 
	tra eleganza e sostenibilità: sistemi avanzati permettono di diminuire la portata di acqua, obbedendo alla parola 
	d'ordine “risparmio”. I radiatori rinnovano la propria immagine, divenendo anch'essi elementi di arredo.<br />
	Materiali avvenieristici si sposano perfettamente con quelli più tradizionali, con i legni pregiati, con i 
	rivestimenti preziosi magari in tessere di mosaico di memoria bizantina.</p>
	<p>Fornitori rubinetti: Mario Bongio, Fantini, Gattoni, Gessi, Ponsi Rubinetterie Toscane.<br />
	Fornitori radiatori: Brandoni, Ima, Brem.
	</p>

</div>
 
<?php
include 'footer.php';
?>