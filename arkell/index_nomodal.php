<?php
$thisPage = "index";
$title = "Home";
include 'header.php';
?>      
<div id="corpo"> 


  <div id="slidesOne">
    <div class="slides_container">

         <div>
            <a href=""><img src="images/piastrelle.jpg" alt="" border="0"/></a>
            <span class="left"></span>
         </div>
        <div>
          <a href=""><img src="images/pavimenti3.jpg" alt="Pavimenti" border="0"/></a>
            <span class="right"></span>
         </div>
        <div>
          <a href=""><img src="images/pavimenti4.jpg" alt="Pavimenti" border="0"/></a>
            <span class="right"></span>
         </div>
        <div>
          <a href=""><img src="images/pavimenti5.jpg" alt="Pavimenti" border="0"/></a>
            <span class="right"></span>
         </div>
        <div>
          <a href=""><img src="images/pavimenti7.jpg" alt="Pavimenti" border="0"/></a>
            <span class="right"></span>
         </div>
        <div>
          <a href=""><img src="images/pavimenti8.jpg" alt="Pavimenti" border="0"/></a>
            <span class="right"></span>
         </div>
        <div>
          <a href=""><img src="images/pavimenti9.jpg" alt="Pavimenti" border="0"/></a>
            <span class="right"></span>
         </div>
        <div>
          <a href=""><img src="images/pavimenti10.jpg" alt="Pavimenti" border="0"/></a>
            <span class="right"></span>
         </div>
        <div>
          <a href=""><img src="images/pavimenti2.jpg" alt="" border="0"/></a>
            <span class="right"></span>
         </div>       
    </div>
      <div id="boxLeft">
        <ul>
          <li><a href="pavimenti.php">Pavimenti</a></li>
        <!--<li><a href="#">Parquet</a></li>-->
        <li><a href="rivestimenti.php">Rivestimenti</a></li>
      </ul>
    </div>
  </div>

  <div id="slidesTwo">
     <div class="slides_container">
        <div>
          <a href=""><img src="images/sanitari.jpg" alt="" border="0"/></a>
            <span class="right"></span>
         </div>
         <div>
            <a href=""><img src="images/rubinetteria.jpg" alt="" border="0"/></a>
            <span class="left"></span>
         </div>
         <div>       
            <a href=""><img src="images/wellness.jpg" alt="" border="0" /></a>
            <span class="right">
         </div>
         <div>       
            <a href=""><img src="images/arredo_bagno01.jpg" alt="Arredo Bagno" border="0" /></a>
            <span class="right">
         </div>
         <div>       
            <a href=""><img src="images/arredo_bagno03.jpg" alt="Arredo Bagno" border="0" /></a>
            <span class="right">
         </div>
         <div>       
            <a href=""><img src="images/arredo_bagno04.jpg" alt="Arredo Bagno" border="0" /></a>
            <span class="right">
         </div>
         <div>       
            <a href=""><img src="images/arredo_bagno05.jpg" alt="Arredo Bagno" border="0" /></a>
            <span class="right">
         </div>
         <div>       
            <a href=""><img src="images/arredo_bagno06.jpg" alt="Arredo Bagno" border="0" /></a>
            <span class="right">
         </div>
         <div>       
            <a href=""><img src="images/arredo_bagno08.jpg" alt="Arredo Bagno" border="0" /></a>
            <span class="right">
         </div>
         <div>       
            <a href=""><img src="images/arredo_bagno09.jpg" alt="Arredo Bagno" border="0" /></a>
            <span class="right">
         </div>
         <div>       
            <a href=""><img src="images/arredo_bagno10.jpg" alt="Arredo Bagno" border="0" /></a>
            <span class="right">
         </div>

    </div>
      <div id="boxRight">
        <ul>
        <li><a href="sanitari.php">Sanitari</a></li>
        <!--<li><a href="#">Rubinetti</a></li>-->
        <li><a href="arredobagno.php">Arredo Bagno</a></li>
            <li><a href="wellness.php">Wellness</a></li>
    </div>  
  </div>
  <div id="slidesThree">

    <div class="slides_container">
        <div>
          <a href=""><img src="images/caminetti.jpg" alt="" border="0"/></a>
            <span class="right"></span>
         </div>
         <div>
            <a href=""><img src="images/scale.jpg" alt="" border="0"/></a>
            <span class="left"></span>
         </div>
         <div>       
            <a href=""><img src="images/cucina_muratura.jpg" alt="" border="0" /></a>
            <span class="right">
         </div>
    </div>
      <div id="boxLeft">
        <ul>
          <li><a href="caminetti.php">Caminetti</a></li>
        <li><a href="scale.php">Scale</a></li>
        <li><a href="cucinemuratura.php">Cucine in muratura</a></li>
      </ul>
    </div>

  </div>

  <div id="slidesFour">
     <div class="slides_container">
        <div>
          <a href=""><img src="images/porte.jpg" alt="" border="0"/></a>
            <span class="right"></span>
         </div>
         <div>
            <a href=""><img src="images/infissi.jpg" alt="" border="0"/></a>
            <span class="left"></span>
         </div>
    </div>
      <div id="boxRight">
        <ul>
        <li><a href="porte.php">Porte</a></li>
        <li><a href="infissi.php">Infissi</a></li>
      </ul>
    </div>  
  </div>
<!--  <div id="slider1">
        <ul id="slider1Content">
            <li class="slider1Image">
     
                <a href="infissi.html"><img src="example_images/wide/2.jpg" alt="" border="0" /></a>
              <span class="right"></span></li>
            <li class="slider1Image">
                       <a href="porte.html"><img src="example_images/wide/1.jpg" alt="" border="0" /></a>
              <span class="left"></span></li>
            <li class="slider1Image">
                <a href="sicurezza.html"> <img src="example_images/wide/3.jpg" alt="" border="0"/></a>
                <span class="right"></li>
            <li class="slider1Image">
             <a href="zanzariere.html">   <img src="example_images/wide/4.jpg" alt="" border="0"/></a>
              <span class="left"></span></li>
            <div class="clear slider1Image"></div>
        </ul>
        
  

  <div id="BOX2"><li><a href="#">Sanitari</a></li>
    <li><a href="#">Rubinetti</a></li>
    <li><a href="#">Arredo Bagno</a></li>
        <li><a href="#">Wellness</a></li></div>  </div>
    
  <div id="slider2">
        <ul id="slider2Content">
            <li class="slider2Image">
     
                <a href="infissi.html"><img src="example_images/wide/2.jpg" alt="" border="0" /></a>
              <span class="right"></span></li>
            <li class="slider2Image">
                       <a href="porte.html"><img src="example_images/wide/1.jpg" alt="" border="0" /></a>
              <span class="left"></span></li>
            <li class="slider2Image">
                <a href="sicurezza.html"> <img src="example_images/wide/3.jpg" alt="" border="0"/></a>
                <span class="right"></li>
            <li class="slider2Image">
             <a href="zanzariere.html">   <img src="example_images/wide/4.jpg" alt="" border="0"/></a>
              <span class="left"></span></li>
            <div class="clear slider2Image"></div>
        </ul>
  

  <div id="BOX2"><li><a href="#">Sanitari</a></li>
    <li><a href="#">Rubinetti</a></li>
    <li><a href="#">Arredo Bagno</a></li>
        <li><a href="#">Wellness</a></li></div>  </div>
    
  <div id="slider3">
        <ul id="slider3Content">
            <li class="slider1Image">
     
                <a href="infissi.html"><img src="example_images/wide/2.jpg" alt="" border="0"/></a>
              <span class="right"></span></li>
            <li class="slider3Image">
                       <a href="porte.html"><img src="example_images/wide/1.jpg" alt="" border="0"/></a>
              <span class="left"></span></li>
            <li class="slider3Image">
                <a href="sicurezza.html"> <img src="example_images/wide/3.jpg" alt="" border="0" /></a>
                <span class="right"></li>
            <li class="slider3Image">
             <a href="zanzariere.html">   <img src="example_images/wide/4.jpg" alt="" border="0"/></a>
              <span class="left"></span></li>
            <div class="clear slider3Image"></div>
        </ul>
 

    <div id="BOX3"> <li><a href="#">Caminetti</a></li>
    <li><a href="#">Scale</a></li>
  <li><a href="#">Cucine in muratura</a></li></div>   </div>
    
  <div id="slider4">
        <ul id="slider4Content">
            <li class="slider4Image">
     
                <a href="infissi.html"><img src="example_images/wide/2.jpg" alt="" border="0" /></a>
              <span class="right"></span></li>
            <li class="slider4Image">
                       <a href="porte.html"><img src="example_images/wide/1.jpg" alt="" border="0" /></a>
              <span class="left"></span></li>
            <li class="slider4Image">
                <a href="sicurezza.html"> <img src="example_images/wide/3.jpg" alt="" border="0"/></a>
                <span class="right"></li>
            <li class="slider4Image">
             <a href="zanzariere.html">   <img src="example_images/wide/4.jpg" alt="" border="0"/></a>
              <span class="left"></span></li>
            <div class="clear slider4Image"></div>
        </ul>
   

    <div id="BOX4"><li><a href="#">Porte</a></li>
    <li><a href="#">Infissi</a></li>
    </div> </div>
-->    <!--
Skype 'Il mio stato' button
http://www.skype.com/go/skypebuttons
-->

   </div>
    
<?php
include 'footer.php';
?> 