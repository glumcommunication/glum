<?php
$thisPage = "mii";
$title = "More in Italy";
include 'header.php';
?>
      
<div id="bigbox">
		
		   <img src="images/mii.jpg" alt="More in Italy"  style="height:250px;" />

		   
</div>


<div id="corpo">
<p>
<strong>Gruppo Arkell</strong> insieme a <strong>More in Italy</strong> organizza per il 16 gennaio un evento per illustrare il progetto Mii & You. Cos'è More in Italy? Si definisce come un progetto concreto per restituire il vero valore a ciò che ci sta più a cuore: il nostro benessere, la nostra famiglia e la nostra casa.<br>
Il clima di paura e sfiducia che sta attanagliando la società e l'economia italiana deve necessariamente essere superato. Come? Ancorandoci ai valori più profondi. Aggrappandoci a quello che abbiamo di più vicino: la nostra famiglia e la nostra casa. Il progetto More in Italy si rivolge proprio a coloro i quali credono così fermamente in questi valori da decidere di invertirvi, moralmente ed economicamente.<br>
Non esiste una formula magica per tornare allo splendore di un tempo e per uscire dalla crisi. Possiamo però iniziare da una parte: migliorare la nostra casa, migliorare il nostro territorio è migliorare il contesto in cui viviamo.<br>
Il progetto Mii poggia su basi concrete e solide. Con vantaggi effettivi per coloro che vogliono risparmiare e al tempo stesso aderire ad un progetto importante. Migliora il tuo benessere. Migliora la tua vita. Migliora l'economia italiana.
</p>


<div class="sponsor" style="width:100%;background-color:#394b69;">
    <table class="sponsortable" style="margin: 0 auto;width: 100%;">
        <tr>
            <th colspan="9">Multimedia
            </th>		
        </tr>
    </table>
    <div id="video" style="width: 446px; padding:3px;margin-top:25px; float:left;">
        <iframe width="446" height="251" src="//www.youtube.com/embed/emL-rqkw5LQ" frameborder="0" allowfullscreen></iframe>
    </div>
    <div id="pics" style="width:565px;float:left;">
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_01.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_01.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_02.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_02.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_03.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_03.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_04.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_04.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_05.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_05.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_06.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_06.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_07.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_07.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_08.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_08.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_09.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_09.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_10.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_10.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_11.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_11.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_12.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_12.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_13.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_13.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_14.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_14.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_15.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_15.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_16.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_16.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_17.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_17.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_18.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_18.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_19.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_19.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
        <div class="pic" style="width:107px;padding:3px;float:left;">
            <a href="images/moreinitaly/moreinitaly_big_20.jpg" rel="lightbox[LuciSulLavoro2013]" title=""><img src="images/moreinitaly/tmb/moreinitaly_20.jpg" alt="Terre di Cuore - Paolo Crepet"></a>
        </div>
    </div>

</div>

<div class="sponsor" style="width:100%;background-color:#394b69;float:left;">
<table class="sponsortable" style="margin: 0 auto;">
	<tr>
		<th colspan="9">Partner</th>		
	</tr>
	<tr>
		<td style="width:100px;">&emsp;</td>
		<td style="width:100px;">&emsp;</td>
        <td><img src="images/moreinitaly/provenza.jpg" alt="Provenza"></td>
        <td><img src="images/moreinitaly/atlas.jpg" alt="Atlas Concorde"></td>
        <td><img src="images/moreinitaly/shulz.jpg" alt="Schulz Italia"></td>
        <td><img src="images/moreinitaly/carton.jpg" alt="Carton Factory"></td>
        <td><img src="images/moreinitaly/azzurra.jpg" alt="Azzurra"></td>
		<td style="width:100px;">&emsp;</td>	
		<td style="width:100px;">&emsp;</td>
				
	</tr>

</table>
</div>
</div>
 
<?php
include 'footer.php';
?>