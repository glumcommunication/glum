<?php
$thisPage = "caminetti";
$title = "Caminetti";
include 'header.php';
?>
 
 <div id="bigbox">
		
		   <img src="images/caminetti.jpg" alt="example"/>
		   <div id="caption"><?php echo $title ?></div>
		   
</div>


<div id="corpo">
	<p>Il fuoco, storicamente elemento di energia e di vita, ha sempre portato con sé il significato di luce e calore. 
	La sua fiamma ardente è frutto di bellezza e poesia fin dalle origini dell'uomo.<br />
	Oggi lo sviluppo tecnologico permette di vagliare innumerevoli soluzioni che possono abbinare la bellezza ed 
	il confort dell’impianto alla sua efficacia termica per gli ambienti.<br />
	Gruppo Arkell può offrirvi una vasta gamma di caminetti, termocaminetti, caminetti ad aria calda e stufe in ghisa, 
	in maiolica, in ceramica, in pietra ollare, oltre a termocucine, stufe a pellets, caminetti a gas o a bioetanolo 
	e molto altro ancora.</p>
	<p>Fornitori: MCZ Caminetti, Palazzetti, Dim’ora Caminetti.
	</p>
</div>
     
<?php
include 'footer.php';
?>