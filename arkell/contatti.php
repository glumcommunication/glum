<?php
$thisPage = "contatti";
$title = "Contatti";
include 'header.php';
?>
<div id="corpo">
   <div class="infobox">
		<small><a href="https://maps.google.it/maps?f=q&amp;source=embed&amp;hl=it&amp;geocode=&amp;q=arkell&amp;aq=&amp;sll=43.384410,11.271520&amp;sspn=0.001880,0.004820&amp;t=h&amp;ie=UTF8&amp;hq=arkell&amp;hnear=&amp;filter=0&amp;update=1&amp;cid=18261737395712197950&amp;ll=43.414494,11.147561&amp;spn=0.010911,0.019312&amp;z=15&amp;iwloc=A" style="color:#FFFFFF;text-align:center">
		Visualizzazione ingrandita della mappa</a></small>   	
   	<iframe width="450" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
   	src="https://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=arkell&amp;aq=&amp;sll=43.384410,11.271520&amp;sspn=0.001880,0.004820&amp;t=h&amp;ie=UTF8&amp;hq=arkell&amp;hnear=&amp;filter=0&amp;update=1&amp;cid=18261737395712197950&amp;ll=43.414494,11.147561&amp;spn=0.010911,0.019312&amp;z=15&amp;iwloc=&amp;output=embed">
   	</iframe><br />

	   <p>Via Selvamaggio 21/A, 53034 Colle di Val d'Elsa (SI)<br />
	   Telefono: 0577 924618 - Fax: 0577 924625</p>
	   <p><strong>Orari:</strong><br />
	   Lunedì mattina: chiusura settimanale<br />
		Lunedì pomeriggio: 15 - 19<br />
		Dal martedì al venerdì: 9 - 13,  15 - 19<br />
		Sabato mattina: 9 - 13<br />
		Sabato pomeriggio e domenica: chiuso<br /><br />
		A luglio e agosto siamo chiusi il sabato e la domenica e aperti il lunedì mattina.
	   </p>
   </div>
	<div class="infobox">
		<small><a href="https://maps.google.it/maps?f=q&amp;source=embed&amp;hl=it&amp;geocode=&amp;q=arkell&amp;aq=&amp;sll=43.384410,11.271520&amp;sspn=0.001880,0.004820&amp;t=h&amp;ie=UTF8&amp;hq=arkell&amp;hnear=&amp;filter=0&amp;update=1&amp;cid=12513792370110760411&amp;ll=43.379725,11.272788&amp;spn=0.010917,0.019312&amp;z=15&amp;iwloc=A" style="color:#FFFFFF;text-align:center">
		Visualizzazione ingrandita della mappa</a></small>
		<iframe width="450" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
		src="https://maps.google.it/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=arkell&amp;aq=&amp;sll=43.384410,11.271520&amp;sspn=0.001880,0.004820&amp;t=h&amp;ie=UTF8&amp;hq=arkell&amp;hnear=&amp;filter=0&amp;update=1&amp;cid=12513792370110760411&amp;ll=43.379725,11.272788&amp;spn=0.010917,0.019312&amp;z=15&amp;iwloc=&amp;output=embed">
		</iframe><br />
				
	   <p>Via Pietro Nenni 16, Località Badesse, 53035 Monteriggioni (SI)<br />
	   Telefono: 0577 309058 - Fax: 0577 309141</p>
	   <p><strong>Orari:</strong><br />
	   Lunedì mattina: chiusura settimanale<br />
		Lunedì pomeriggio: 15 - 19<br />
		Dal martedì al venerdì: 9 - 13,  15 - 19<br />
		Sabato mattina: 9 - 13<br />
		Sabato pomeriggio e domenica: chiuso<br /><br />
		A luglio e agosto siamo chiusi il sabato e la domenica e aperti il lunedì mattina.
	   </p>
	</div>
	<p style="text-align: center;">
		<strong>Orario Magazzino</strong> - Località Selvamaggio<br />
		Dal lunedì al venerdì: 8 - 12, 14 - 18<br />
		Sabato e domenica: chiuso<br />		
	</p>
</div>
 
<?php
include 'footer.php';
?>