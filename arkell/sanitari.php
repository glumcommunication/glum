<?php
$thisPage = "sanitari";
$title = "Sanitari";
include 'header.php';
?>
      
<div id="bigbox">
		
		   <img src="images/sanitari.jpg" alt="example"/>
		   <div id="caption"><?php echo $title ?></div>
		   
</div>


<div id="corpo">
	<p>Il bagno ha cambiato radicalmente la sua fisionomia.<br />
	In passato era semplicemente il luogo dell’igiene personale, oggi è diventato luogo dedicato al benessere 
	grazie allo sviluppo tecnologico. I lavabi diventano trasparenti lastre di cristallo, i sanitari veri e 
	propri oggetti di design hi tech e multifunzionali, i rubinetti la sintesi di soluzioni tecnologiche 
	all’avanguardia, i radiatori piacevoli e funzionali componenti d’arredamento.<br />
	Tutto questo lo trovate presso i negozi del Gruppo Arkell. Venite a visitarci e potrete realizzare il vostro 
	ambiente bagno in completa rispondenza con le vostre aspirazioni e se volete lasciandovi guidare dalla 
	nostra esperienza.</p>
	<p>Fornitori: Duravit, Faleri,  Hatria, Ponte Giulio, Sbordoni, Ceramica Azzurra, Globo, Gessi, Noken.	
	</p>
</div>
 
<?php
include 'footer.php';
?>