<?php
$thisPage = "cucinemuratura";
$title = "Cucine in muratura";
include 'header.php';
?>
      
<div id="bigbox">
		
		   <img src="images/cucina.jpg" alt="example"/>
		   <div id="caption"><?php echo $title ?></div>
		   
</div>


<div id="corpo">
	<p>Il gusto e l’eleganza della tradizione si esprimono con le cucine in muratura. 
	Ogni elemento è curato nei minimi dettagli, per un effetto scenico che riporta alle antiche 
	tradizioni del passato. Con questa filosofia Gruppo Arkell mette a disposizione cucine in muratura 
	dalla forte personalità, costruite con materiali pregiati, naturali e autentici, per durare nel tempo.
	</p>
</div>
 
<?php
include 'footer.php';
?>