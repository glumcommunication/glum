<?php
$thisPage = "rivestimenti";
$title = "Rivestimenti";
include 'header.php';
?>
      
<div id="bigbox">
		
		   <img src="images/piastrelle.jpg" alt="example"/>
		   <div id="caption"><?php echo $title ?></div>
		   
</div>


<div id="corpo">
	<p>L’esperienza nell'industria ceramica e la vasta gamma di prodotti sono la migliore garanzia di successo che 
	Gruppo Arkell può offrire ai suoi clienti per individuare la soluzione più consona sempre con il giusto rapporto 
	qualità/prezzo.</p>
	<p><strong>PIASTRELLE</strong><br />
	Insieme alle soluzioni più tradizionali, l'azienda Arkell propone anche quelle più esclusive sia dal punto 
	di vista grafico sia per l’ampiezza dei formati e pezzi speciali capaci di dialogare con gli orientamenti 
	stilistici più attuali nel mondo dell’arredo.</p>
	<p>Fornitori: 	Ab Colors, Acquario Due, Aganippe, Atlas Concorde, Caesar, Ceramica Mariner, Cerdomus Ceramiche, 
	Ergon, Ermes, Etruria Ceramiche, Francesco De Maio, Kronos 2, L’Antic Colonial, Boxer, Marazzi, Pecchioli, 
	Porcelanosa, Provenza , Tonalite, Venis.<br />
	Fornitori accessori piastrelle: Butech (Porcelanosa), Schlüter-Systems, Arkansas Profili.
	</p>
</div>
 
<?php
include 'footer.php';
?>