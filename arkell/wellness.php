<?php
$thisPage = "wellness";
$title = "Wellness";
include 'header.php';
?>
      
<div id="bigbox">
		
		   <img src="images/wellness.jpg" alt="example"/>
		   <div id="caption"><?php echo $title ?></div>
		   
</div>


<div id="corpo">
	<p>I ritmi frenetici e il continuo stress portano con sé il bisogno di rilassarsi. È così che oggi il benessere 
	della persona assume sempre più importanza. Il desiderio di evadere prende posto ad ogni dovere. È così che 
	ognuno di noi vorrebbe nella propria casa un'area relax, un'area in cui è possibile dimenticare tutte le fonti 
	di malumori per dedicarsi totalmente a se stessi. Gruppo Arkell mette a disposizione sistemi hi-tech e 
	multifunzionali per permettere a chiunque di sentirsi sollevato dalla pesantezza della giornata nell'intimità 
	della propria casa, dalle vasche alle docce idromassaggio, dalle mini-piscine alle saune.<br />
	Presso gli showroom troverete anche la nuova linea di mini-piscine, un prodotto innovativo che potrà risolvere 
	piacevolmente ed in spazi assolutamente trascurabili la vostra esigenza per un nuovo progetto di piscina all’aperto 
	o al coperto.</p>
	<p>Fornitori: Calflex srl, Gessi, SystemPool, Effegibi, Jacuzzi, Grandform, Vitaviva, Blubleu.	
	</p>
</div>
 
<?php
include 'footer.php';
?>