<?php
$thisPage = "showroom";
$title = "Showroom";
include 'header.php';
?>
      
<div id="bigbox">
		
		   <img src="images/showroom.jpg" alt="showroom"/>
		   <div id="caption"><?php echo $title ?></div>
		   
</div>


<div id="corpo">
	<p>
		GRUPPO ARKELL, vanta due sale mostra in cui la fantasia e qualità progettuale si fondono per dare vita ad 
		ambienti unici ed irripetibili. Un'esperienza visiva originale che permette di immaginare la propria casa 
		arredata secondo i propri gusti e desideri. Gli showroom, collocati a Colle Val d’Elsa in località Selvamaggio e 
		a Monteriggioni in località Badesse, sono caratterizzati dalla vasta scelta di prodotti di aziende prestigiose, 
		nonché dall’elevato livello di professionalità ed assistenza nella  vendita che rende unico e piacevole 
		l'acquisto.	
	</p>
</div>
 
<?php
include 'footer.php';
?>