<?php
$thisPage = "scale";
$title = "Scale";
include 'header.php';
?>
      
<div id="bigbox">
		
		   <img src="images/scale.jpg" alt="example"/>
		   <div id="caption"><?php echo $title ?></div>
		   
</div>


<div id="corpo">
	<p>Qualità, design, cura dei particolari, sono solo alcune delle peculiarità che Gruppo Arkell mette a disposizione 
	per classificare le proprie tipologie di scale.<br />
	Le tipologie commercializzate sono le scale a chiocciola, le scale a giorno e le scale rettrattili.</p>
	<p><strong>SCALE A CHIOCCIOLA</strong><br />
	Facilmente adattabili a spazi minimi e in molteplici situazioni abitative come quella di collegare tra loro 
	ambienti dalle caratteristiche completamente diverse, dalla mansarda al seminterrato fino a grandi spazi per saloni 
	e negozi.</p>
	<p><strong>SCALE A GIORNO</strong><br />
	La struttura portante può essere posta lateralmente da ambedue i lati, da un solo lato oppure al di sotto dei 
	gradini, a seconda dei modelli.</p>
	<p><strong>SCALE RETRATTILI</strong><br />
	Scale a elementi in acciaio oppure in alluminio, complete di serratura in ferro, ripiegabili tramite snodi 
	bilanciati completa di serratura in ferro montata nel pannello di prima scelta. Anche su misura.</p>
	<p>Fornitori: Fantozzi Scale, ST Scale, Novalinea</p>	
</div>
 
<?php
include 'footer.php';
?>