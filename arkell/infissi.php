<?php
$thisPage = "infissi";
$title = "Infissi";
include 'header.php';
?>
      
<div id="bigbox">
		
		   <img src="images/infissi.jpg" alt="example"/>
		   <div id="caption"><?php echo $title ?></div>
		   
</div>


<div id="corpo">
	<p>Lo sviluppo tecnologico dei materiali da costruzione, dei collanti e dei trattamenti ha fatto e sta 
	facendo passi importanti che richiedono un continuo aggiornamento per poter consigliare le soluzioni 
	più affidabili e competitive.<br />
	Presso Gruppo Arkell troverete una vasta gamma di prodotti fortemente innovativi, dalle 
	finestre tetto ai vetrocementi, dai telai ai collanti, oltre che una consulenza sempre aggiornata 
	che vi permetterà, da soli o insieme al vostro progettista e alla vostra impresa, di valutare e scegliere 
	la soluzione migliore per i vostri interventi.</p>
	<p>Fornitori: Albertini, Fais, Schulzinfissi.<br />	
	Colla: Adesital, Mapei.<br />	
	Prodotti pulizia: Faber Chimica.<br />	
	Lavanderie: Famam, Arbi, Compab.
	</p>
</div>
 
<?php
include 'footer.php';
?>