<?php
$thisPage = "team";
$title = "Il team";
include 'header.php';
?>
      
<div id="bigbox">
		
		   <img src="images/team.jpg" alt="team"/>
		   <div id="caption"><?php echo $title ?></div>
		   
</div>


<div id="corpo">
	<div class="peoplebox"><img src="images/people/simoneb.jpg" width="200" height="187" alt="Simone Boschini" /><br />
		<p><strong>Simone Boschini</strong><br />
		Responsabile amministrazione<br />
		<a href="mailto:simoneb@gruppoarkell.it">simoneb@gruppoarkell.it</a></p>
	</div>
	<div class="peoplebox"  style="background-color: #8995AB;"><img src="images/people/lauraf.jpg" width="200" height="187" alt="Laura Farasin" /><br />
		<p style="color: white;"><strong>Laura Farasin</strong><br />
		Responsabile vendite<br />
		<a href="mailto:lauraf@gruppoarkell.it">lauraf@gruppoarkell.it</a></p>
	</div>
	<div class="peoplebox"><img src="images/people/antonellab.jpg" width="200" height="187" alt="Antonella Bersotti" /><br />
		<p><strong>Antonella Bersotti</strong><br />
		Venditrice (Store Colle Val D'Elsa)<br />
		<a href="mailto:antonellab@gruppoarkell.it">antonellab@gruppoarkell.it</a></p>
	</div>
	<div class="peoplebox"><img src="images/people/alessias.jpg" width="200" height="187" alt="Alessia Salvini" /><br />
		<p><strong>Alessia Salvini</strong><br />
		Receptionist - Collaboratrice amministrativa<br />
		<a href="mailto:alessias@gruppoarkell.it">alessias@gruppoarkell.it</a></p>
	</div>
	<div class="peoplebox"><img src="images/people/davidc.jpg" width="200" height="187" alt="David Campinoti" /><br />
		<p><strong>David Campinoti</strong><br />
		Responsabile vendite<br />
		<a href="mailto:davidc@gruppoarkell.it">davidc@gruppoarkell.it</a></p>
	</div>
	<div class="peoplebox"><img src="images/people/paolop.jpg" width="200" height="187" alt="Paolo Posarelli" /><br />
		<p><strong>Paolo Posarelli</strong><br />
		Responsabile vendite<br />
		<a href="mailto:paolop@gruppoarkell.it">paolop@gruppoarkell.it</a></p>
	</div>
	<div class="peoplebox"><img src="images/people/giadaf.jpg" width="200" height="187" alt="Giada Forni" /><br />
		<p><strong>Giada Forni</strong><br />
		Venditrice (Store Badesse)<br />
		<a href="mailto:giadaf@gruppoarkell.it">giadaf@gruppoarkell.it</a></p>
	</div>
	<div class="peoplebox"><img src="images/people/evag.jpg" width="200" height="187" alt="Eva Giannini" /><br />
		<p><strong>Eva Giannini</strong><br />
		Venditrice (Store Badesse)<br />
		<a href="mailto:evag@gruppoarkell.it">evag@gruppoarkell.it</a></p>
	</div>
	<div class="peoplebox"><img src="images/people/elisab.jpg" width="200" height="187" alt="Elisa Ballini" /><br />
		<p><strong>Elisa Ballini</strong><br />
		Gestione ordini<br />
		<a href="mailto:elisab@gruppoarkell.it">elisab@gruppoarkell.it</a></p>
	</div>
</div>
 
<?php
include 'footer.php';
?>
