<?php
$thisPage = "terredicuore";
$title = "Terre di Cuore";
include 'header.php';
?>
      
<div id="bigbox">
		
		   <img src="images/terredicuore.png" alt="Terre di Cuore"/>

		   
</div>


<div id="corpo">
	<p><strong>GRUPPO ARKELL</strong> in collaborazione con <strong>GESSI</strong> e <strong>ITLAS</strong>,<br>
è lieta di invitarla all'evento "TERRE DI CUORE",
Meeting imprenditoriale per la prosperità economica della Provincia di Siena.</p>

<p>Ospite d’onore: Prof. <strong>Dipak R. Pant</strong>. Antropologo, esperto di economia sostenibile, fondatore e coordinatore dell’UNITÀ 
DI STUDI INTERDISCIPLINARI PER L’ECONOMIA SOSTENIBILE, presso l’Università Cattaneo - LIUC (Castellanza - VA), e guida 
strategica su temi di sostenibilità, pianificazione del territorio e dell’ambiente, programmazione dello sviluppo e di 
sicurezza mondiale dell’umanità presso le istituzioni scientifiche dell’Europa, Cina e Stati Uniti.</p>

<p>Il professore interverrà sulla rinascita economica basandosi sul principio che la sostenibilità sociale e ambientale 
sia elemento essenziale per ogni strategia aziendale e per la competitività nel mercato globale.</p>

<!--<p>Per permetterci di riservarLe la migliore accoglienza,<br>
La preghiamo di confermare la Sua partecipazione <strong>entro il 13 Novembre 2012</strong> ai seguenti recapiti:<br>
<a href="mailto:giuliaf@idpromoter.it">giuliaf@idpromoter.it</a><br>
<a href="mailto:alessandras@idpromoter.it">alessandras@idpromoter.it</a><br>
Tel.: <strong>0577 058394</strong>
	</p>-->
<p>
<a href="pdf/programma_terredicuore_web.pdf" title="Download">
		<span><img src="images/pdfdownload.png" width="32" height="32" alt="Pdf" style="height:32px;width:32px;vertical-align: middle;" /></span>
		 Scarica il programma dell'evento</a></p>
<p>
<a href="pdf/invito_terredicuore_web.pdf" title="Download">
		<span><img src="images/pdfdownload.png" width="32" height="32" alt="Pdf" style="height:32px;width:32px;vertical-align: middle;" /></span>
		 Scarica l'invito all'evento</a></p>

<div class="flowplayer">
<iframe width="800" height="600" src="http://www.youtube.com/embed/tAPXMV_S0tU" frameborder="0" allowfullscreen></iframe>
   <!--<video src="http://www.gruppoarkell.it/images/terredicuore/intervista_farasin_arkell_terredicuore.flv"></video>-->
</div>		 
<div class="sponsor" style="width:100%;background-color:#394b69;">
<table class="sponsortable" style="margin: 0 auto;">
	<tr>
		<th colspan="9">Sponsor</th>		
	</tr>
	<tr>
		<td><a href="http://www.chiantibanca.it/" target="_blank"><img src="images/terredicuore/chiantibancabcc.png" width="93" height="64" alt="ChiantiBanca BCC"></a></td>
		<td><a href="http://www.idpromoter.it/" target="_blank"><img src="images/terredicuore/idpromoter.png" width="93" height="64" alt="Idpromoter"></a></td>
		<td><a href="http://www.agriturismoilciliegio.com/" target="_blank"><img src="images/terredicuore/ciliegio.png" width="93" height="64" alt="Azienda Agricola Il Ciliegio"></a></td>	
		<td><a href="http://www.gr2arredamenti.it/" target="_blank"><img src="images/terredicuore/gr2.png" width="93" height="64" alt="GR2 Arredamenti"></a></td>	
		<td><a href="http://www.house-design.it/" target="_blank"><img src="images/terredicuore/housedesign.png" width="93" height="64" alt="House Design"></a></td>
		<td><a href="http://www.siderurgicafiorentina.it/Home.html" target="_blank"><img src="images/terredicuore/siderurgicafiorentina.png" width="93" height="64" alt="Siderurgica Fiorentina"></a></td>
		<td><a href="http://www.class-ricevimenti.it/" target="_blank"><img src="images/terredicuore/class.png" width="93" height="64" alt="Class Ricevimenti"></a></td>	
		<td><a href="http://www.amadori.it/" target="_blank"><img src="images/terredicuore/amadori.png" width="93" height="64" alt="Amadori"></a></td>
<td><a href="http://www.giuntiumberto.it/" target="_blank"><img src="images/terredicuore/giunti.png" width="93" height="64" alt="Giunti"></a></td>				
	</tr>
	<tr>
		<th colspan="9">Con il patrocinio di:</th>		
	</tr>
	<tr>
		<td colspan="9" style="text-align:center;"><a href="http://www.confesercenti.siena.it/" target="_blank"><img src="images/terredicuore/confesercenti.png" width="220" height="150" alt="Confesercenti Siena"></a></td>		
	</tr>		
</table>
</div>		 
</div>
 
<?php
include 'footer.php';
?>