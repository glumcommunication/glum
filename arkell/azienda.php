<?php
$thisPage = "azienda";
$title = "L'Azienda";
include 'header.php';
?>
      


<div id="bigbox">
				   <div id="caption"><?php echo $title ?></div>
		   <img src="images/azienda.jpg" alt="azienda"/>

		   
</div>
<div id="corpo">
<!--   <p>Nata nel 19?? dalla fusione delle esperienze di Arkos e Ellepiemme, GRUPPO ARKELL si presenta come un'azienda giovane e dinamica, leader nel mercato delle piastrelle in ceramica nella Valdelsa e nell’area senese.
GRUPPO ARKELL è in grado di offrire una vasta gamma di soluzioni nel settore dell’edilizia e della ristrutturazione di appartamenti: pavimenti per interni ed esterni, rivestimenti, parquet, sanitari, rubinetterie, box-doccia, vasche idromassaggio, caminetti, mobili ed accessori da bagno, scale per interni, mini-piscine. 
L’ampiezza della gamma dei propri prodotti e la competenza maturata in tanti anni di lavoro dell'azienda GRUPPO ARKELL garantiscono soluzioni efficaci e di qualità.</p>-->
	<p>
		Nata nel 2001 dalla fusione delle esperienze di Arkos e Ellepiemme, GRUPPO ARKELL si presenta come un'azienda giovane e 
		dinamica, leader nel mercato dei pavimenti e rivestimenti nella Valdelsa e nell’area senese.<br />
		GRUPPO ARKELL è in grado di offrire una vasta gamma di soluzioni nel settore dell’edilizia e della ristrutturazione: 
		pavimenti per interni ed esterni, rivestimenti, parquet, sanitari, rubinetterie, box-doccia, vasche idromassaggio, 
		caminetti, mobili ed accessori da bagno, scale per interni, mini-piscine, porte e infissi.<br />
		L’ampiezza della gamma dei propri prodotti e la competenza maturata in tanti anni di lavoro dell'azienda 
		GRUPPO ARKELL garantiscono soluzioni efficaci e di qualità.<br />	
	</p>
	<p><strong>FILOSOFIA AZIENDALE</strong><br />
	Da sempre Gruppo Arkell è sinonimo di alta qualità ad un prezzo concorrenziale. Questo punto d'arrivo è frutto di un lavoro durato anni basato sulla professionalità dei propri collaboratori, costantemente  aggiornati per tutte le categorie di prodotti e per tutte le novità del settore, sull'importanza data al customer care, e sulle collaborazioni con aziende 
	produttrici leader nel settore dell'arredamento.<br />
	Gruppo Arkell è portatrice di una filosofia incentrata sulla Corporate Social Responsability: le sue marche 
	fornitrici sono infatti aziende che abbracciano il giusto connubio tra aspetti etici ed economici.<br />
	La mission di Gruppo Arkell è quella di conciliare benessere, funzionalità ed estetica, garantendo così 
	la realizzazione di ambienti confortevoli con materiali esclusivi e innovativi che rispondono alle 
	molteplici esigenze del cliente.<br />	
	</p>
  </div>
 
<?php
include 'footer.php';
?>