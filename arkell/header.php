<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="Cesare Rinaldi">
<link rel="icon" href="images/favicon.png" type="image/svg" />
<title><?php echo $title ?> | Gruppo Arkell, rivestimenti, bagno, cucine, scale e infissi</title>
<meta name= "keywords" content= "Gruppo Arkell, pavimento, pavimenti, rivestimenti, rubinetti, sanitari, arredo bagno, vasche idromassaggio, parquet, siena, colle val d'elsa, badesse, monteriggioni, caminetti, stufe, bagno, bagni, piastrelle, piastrella, cotto, parquet, azienda, arkell, wellness, piscina, piscine,"  />
<meta name= "description" content="<? echo $description; ?>" />
<link rel="stylesheet" type="text/css" href="mycss/style.css" />
<link rel="stylesheet" type="text/css" href="mycss/slider.css"  />
<script src="js/jquery-1.7.2.min.js"></script>
<link href="css/lightbox.css" rel="stylesheet" />
<script src="js/lightbox.js"></script>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>

<link href="css/flexslider.css" rel="stylesheet" type="text/css" />
<script src="js/jquery.flexslider-min.js"></script>
<script type="text/javascript">
    $(window).load(function(){
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: true,
        slideshow: true,
        itemWidth: 240,
        itemMargin: 25,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        controlNav: true,
        animationLoop: true,
        slideshow: true,
        sync: "#carousel",
        start: function(slider){
        $('body').removeClass('loading');
        }
      });
    });
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38786648-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>



<!-- JavaScripts-->


  <link rel="stylesheet" href="css/dialog.css" />


<script src="js/slides.min.jquery.js"></script>

        <script>
            $(function(){
			$('#slidesOne').slides({
				preload: true,
				preloadImage: 'images/loading.gif',
				play: 5000,
				pause: 2500,
				generatePagination: false,
				hoverPause: false,
				effect: 'fade',
				crossfade: true,
				bigTarget: true

			});
            });
       	</script>
         <script>
            $(function(){
			$('#slidesTwo').slides({
				preload: true,
				preloadImage: 'images/loading.gif',
				play: 5000,
				pause: 2500,
				generatePagination: false,
				hoverPause: false,
				effect: 'fade',
				crossfade: true,
				bigTarget: true
			});
            });
       	</script>
         <script>
            $(function(){
			$('#slidesThree').slides({
				start: 3,
				preload: true,
				preloadImage: 'images/loading.gif',
				play: 5000,
				pause: 2500,
				generatePagination: false,
				hoverPause: false,
				effect: 'fade',
				crossfade: true,
				bigTarget: true

			});
            });
       	</script>
         <script>
            $(function(){
			$('#slidesFour').slides({
				start: 4,
				preload: true,
				preloadImage: 'images/loading.gif',
				play: 5000,
				pause: 2500,
				generatePagination: false,
				hoverPause: false,
				effect: 'fade',
				crossfade: true,
				bigTarget: true

			});
            });
       	</script>

</head>

<body>
	<div id="main_container">
		<div id="header" class="center clearfix">
  			<div id="logo" class="">
				<h1><a href="index_nomodal.php"><img src="images/logo.png" width="1000" height="134" alt="Gruppo Arkell" border="0" align="right" /></a></h1>
  			</div>
			<div id="topMenu">
<!-- Start PureCSSMenu.com MENU -->
<ul class="pureCssMenu pureCssMenum">
	<li class="pureCssMenui0"><a class="pureCssMenui0" href="#"><span>AZIENDA</span><![if gt IE 6]></a><![endif]><!--[if lte IE 6]><table><tr><td><![endif]-->
	<ul class="pureCssMenum">
		<li class="pureCssMenui"><a class="pureCssMenui" href="azienda.php">GRUPPO ARKELL</a></li>
		<li class="pureCssMenui"><a class="pureCssMenui" href="showroom.php">SHOWROOM</a></li>
		<li class="pureCssMenui"><a class="pureCssMenui" href="team.php">IL TEAM</a></li>
	</ul>
	<!--[if lte IE 6]></td></tr></table></a><![endif]--></li>
	<li class="pureCssMenui0"><a class="pureCssMenui0" href="#"><span>PRODOTTI</span><![if gt IE 6]></a><![endif]><!--[if lte IE 6]><table><tr><td><![endif]-->
	<ul class="pureCssMenum">
		<li class="pureCssMenui"><a class="pureCssMenui" href="pavimenti.php">PAVIMENTI</a></li>
		<li class="pureCssMenui"><a class="pureCssMenui" href="rivestimenti.php">RIVESTIMENTI</a></li>
		<li class="pureCssMenui"><a class="pureCssMenui" href="sanitari.php">SANITARI</a></li>
		<li class="pureCssMenui"><a class="pureCssMenui" href="arredobagno.php">ARREDO BAGNO</a></li>
		<li class="pureCssMenui"><a class="pureCssMenui" href="wellness.php">WELLNESS</a></li>
		<li class="pureCssMenui"><a class="pureCssMenui" href="caminetti.php">CAMINETTI</a></li>
		<li class="pureCssMenui"><a class="pureCssMenui" href="scale.php">SCALE</a></li>
		<li class="pureCssMenui"><a class="pureCssMenui" href="cucinemuratura.php">CUCINE IN MURATURA</a></li>
		<li class="pureCssMenui"><a class="pureCssMenui" href="porte.php">PORTE</a></li>
		<li class="pureCssMenui"><a class="pureCssMenui" href="infissi.php">INFISSI</a></li>
	</ul>
	<!--[if lte IE 6]></td></tr></table></a><![endif]--></li>
	<li class="pureCssMenui0"><a class="pureCssMenui0" href="servizi.php">SERVIZI</a></li>
	<li class="pureCssMenui0"><a class="pureCssMenui0" href="progetti.php">PROGETTI</a></li>
	<li class="pureCssMenui0"><a class="pureCssMenui0" href="partner.php">PARTNER</a></li>
	<li class="pureCssMenui0"><a class="pureCssMenui0" href="contatti.php">CONTATTI</a></li>
	<li class="pureCssMenui0"><a class="pureCssMenui0" href="#"><span>OUTLET</span><![if gt IE 6]></a><![endif]><!--[if lte IE 6]><table><tr><td><![endif]-->
	<ul class="pureCssMenum">
		<li class="pureCssMenui"><a class="pureCssMenui" href="outlet.php">OFFERTE</a></li>
	</ul>
	<!--[if lte IE 6]></td></tr></table></a><![endif]--></li>
	<li class="pureCssMenui0"><a class="pureCssMenui0" href="#"><span>EVENTI</span><![if gt IE 6]></a><![endif]><!--[if lte IE 6]><table><tr><td><![endif]-->
	<ul class="pureCssMenum" style="width: 132px;">
		<li class="pureCssMenui"><a class="pureCssMenui" href="#"><span>TERRE DI CUORE</span><![if gt IE 6]></a><![endif]><!--[if lte IE 6]><table><tr><td><![endif]-->
		<ul class="pureCssMenum" style="width: 172px;">
			<li class="pureCssMenui"><a class="pureCssMenui" href="eventopant.php">EVENTO DIPAK PANT</a></li>
			<li class="pureCssMenui"><a class="pureCssMenui" href="eventocrepet.php">EVENTO PAOLO CREPET</a></li>
		</ul>
		<!--[if lte IE 6]></td></tr></table></a><![endif]--></li>
		<li class="pureCssMenui"><a class="pureCssMenui" href="sienacarbonfree.php">SIENA CARBON FREE</a></li>
		<li class="pureCssMenui"><a class="pureCssMenui" href="lucisullavoro.php">LUCI SUL LAVORO</a></li>
        <li class="pureCssMenui"><a class="pureCssMenui" href="moreinitaly.php">MORE IN ITALY #1</a></li>
        <li class="pureCssMenui"><a class="pureCssMenui" href="moreinitaly2.php">MORE IN ITALY #2</a></li>
	</ul>
	<!--[if lte IE 6]></td></tr></table></a><![endif]--></li>
	<li class="pureCssMenui0"><a class="pureCssMenui0" href="#"><span>PRESS</span><![if gt IE 6]></a><![endif]><!--[if lte IE 6]><table><tr><td><![endif]-->
	<ul class="pureCssMenum">
		<li class="pureCssMenui"><a class="pureCssMenui" href="press_01.php">STIL'e</a></li>
	</ul>
	<!--[if lte IE 6]></td></tr></table></a><![endif]--></li>
</ul>

<!-- End PureCSSMenu.com MENU -->
				<div id="social">
<!--					<ul class="clearfix">-->
						<!--<li class="active">-->
						<a href="http://www.facebook.com/pages/GruppoArkell/384340081625859"  title="Arkell on Facebook" target="_BLANK">
						<img src="images/social_fb.png" width="32" height="31" border="0"/></a>
						<!--</li>-->
<!--						<li>&nbsp;</li>
						<li>&nbsp;</li>
					</ul>
-->  				</div>
			</div>

		</div>
