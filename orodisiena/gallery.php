<?php
$pagename = 'gallery';
$pagetitle = 'Gallery';
$pagedesc = "Scoprite l'ambiente accogliente del ristorante L'Oro di Siena.";
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
			<main>
				<div class="mainContainer">
					<div class="container">
						<h1>Gallery</h1>
						<div class="col-xs-12 content">
							<div class="row">
<?php
for ($i=1;$i<=12;$i++) {
?>
                    <div class="col-sm-3 imgGal">
                        <a href="img/big/gallery<?php echo $i; ?>.jpg" data-lightbox="Wok Sushi" title="L'Oro di Siena">
                            <img src="img/small/gallery<?php echo $i; ?>.jpg" alt="L'Oro di Siena <?php echo $i; ?>" class="ireneStyle">
                        </a>
                    </div>
<?php
 }
?>
							</div>
						</div>
					</div>
					<div class="push"></div>
				</div>
				<!--<div class="homeBox col-xs-12">
					<div class="dottedBox">Ciao</div>
					<div class="greyBox">Amico</div>
					<div class="yellowBox">mio</div>
				</div>-->
			</main>

<?php
include_once 'footer.php';
?>
