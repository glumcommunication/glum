			<header>
				<div class="topbar">
					<div class="container">
						<div class="col-xs-3" id="logo">
							<img src="img/logoods.png" alt="Logo Oro di Siena">
						</div>
						<div class="col-xs-7">
							<nav>
								<ul class="nav nav-pills nav-justified">
									<li <?php if ($pagename=="index") {echo 'class="active"';} ?> ><a href="/" title="">Home</a></li>
									<li <?php if ($pagename=="gallery") {echo 'class="active"';} ?> ><a href="gallery.php" title="">Gallery</a></li>
									<li <?php if ($pagename=="menu") {echo 'class="active"';} ?> ><a href="menu.php" title="">Menu</a></li>
									<li <?php if ($pagename=="contatti") {echo 'class="active"';} ?> ><a href="contatti.php" title="">Contatti</a></li>
								</ul>
							</nav>
						</div>
						<div class="col-xs-2">
							<div class="col-xs-12 social">
								<a href="#" title="Facebook">
									<img src="img/fb.png" alt="Facebook">
								</a>
								<a href="http://www.tripadvisor.it/Restaurant_Review-g187902-d2163960-Reviews-L_Oro_di_Siena-Siena_Tuscany.html" title="Trip Advisor" target="_blank">
									<img src="img/ta.png" alt="Trip Advisor">
								</a>
							</div>
						</div>
					</div>
				</div>
			</header>
