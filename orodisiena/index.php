<?php
$pagename = 'index';
$pagetitle = 'Home';
$pagedesc = "L'Oro di Siena: cucina tipica toscana e pizza nel centro storico di Siena.";
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
			<main>
				<div class="mainContainer">
					<div id="homeslider" class="carousel slide col-xs-12" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#homeslider" data-slide-to="0" class="active"></li>
							<li data-target="#homeslider" data-slide-to="1"></li>
							<li data-target="#homeslider" data-slide-to="2"></li>
							<li data-target="#homeslider" data-slide-to="3"></li>
							<li data-target="#homeslider" data-slide-to="4"></li>
							<li data-target="#homeslider" data-slide-to="5"></li>
						</ol>
						<div class="carousel-inner">
							<div class="active item">
								<img src="img/oro-di-siena-home1.jpg" alt="...">
								<div class="carousel-caption">
								</div>
							</div>
							<div class="item">
								<img src="img/oro-di-siena-home2.jpg" alt="...">
								<div class="carousel-caption">
								</div>
							</div>
							<div class="item">
								<img src="img/oro-di-siena-home3.jpg" alt="...">
								<div class="carousel-caption">
								</div>
							</div>
							<div class="item">
								<img src="img/oro-di-siena-home4.jpg" alt="...">
								<div class="carousel-caption">
								</div>
							</div>
							<div class="item">
								<img src="img/oro-di-siena-home5.jpg" alt="...">
								<div class="carousel-caption">
								</div>
							</div>
							<div class="item">
								<img src="img/oro-di-siena-home6.jpg" alt="...">
								<div class="carousel-caption">
								</div>
							</div>
						</div>

						<a class="left carousel-control" href="#homeslider" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a class="right carousel-control" href="#homeslider" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</a>
					</div>
					<div class="container">
						<div class="col-xs-12">
							<div class="row">
								<div class="mainRight col-xs-12">
									<div class="yellowBox col-xs-12 shadowed">
										<p>
											Se vi trovate nel meraviglioso centro storico di Siena e siete in cerca di un ristorante raccolto e accogliente passate a trovarci.
										</p>
										<p>
											L'Oro di Siena prepara i piatti tradizionali della cucina tipica senese e italiana, utilizzando ingredienti semplici e freschi. Assaggiate anche la nostra pizza e concedetevi un calice di buon vino per accompagnare il pasto.
										</p>
										</p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="mainLeft col-xs-12">
									<div class="dottedBox col-xs-6">
										<h1>Orari</h1>
										<p>
											Dal lunedì alla domenica dalle 11:00 alle 15:00 e dalle 18:30 alle 11:30.
										</p>
										<p>
											Per prenotazioni chiamare al +39 0577 282729 o al +39 389 0536703.
										</p>
									</div>
									<div class="greyBox col-xs-6">
										<div id="map_canvas">
										</div>
									</div>
								</div>
								</div>
							<hr>
							<div class="row">
								<div class="col-xs-12">
									<div id="TA_cdsscrollingravewide761" class="TA_cdsscrollingravewide">
										<ul id="7bbfDiHzgaU" class="TA_links YzLFk2DvmeK">
											<li id="8oWRMmAQN" class="w7T0UzKYYoU5">
												<a target="_blank" href="http://www.tripadvisor.com/">
													<img src="http://c1.tacdn.com/img2/t4b/Stacked_TA_logo.png" alt="TripAdvisor" class="widEXCIMG" id="CDSWIDEXCLOGO"/>
												</a>
											</li>
										</ul>
									</div>
									<script src="http://www.jscache.com/wejs?wtype=cdsscrollingravewide&amp;uniq=761&amp;locationId=2163960&amp;lang=en_US&amp;border=false&amp;shadow=false&amp;backgroundColor=gray"></script>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="push"></div>
				<!--<div class="homeBox col-xs-12">
					<div class="dottedBox">Ciao</div>
					<div class="greyBox">Amico</div>
					<div class="yellowBox">mio</div>
				</div>-->
			</main>

<?php
include_once 'footer.php';
?>
