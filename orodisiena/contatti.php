<?php
$pagename = 'contatti';
$pagetitle = 'Contatti';
$pagedesc = 'Venite a trovarci: via Garibaldi 61/65, Siena. Chiuso la domenica.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
			<main>
				<div class="mainContainer">
					<div class="container">
						<h1>Contatti</h1>
						<div class="col-xs-12 content">
							<div class="row">
								<div class="col-xs-9">
									<div id="map_canvas" style="height:480px;">
									</div>
								</div>
								<aside>
									<div class="col-xs-3">
										<h2>L'ORO DI SIENA</h2>
										<p>VIA GARIBALDI 61/65<br>53100 SIENA<br>
										TELEFONO: +39 0577 282729<br>
										EMAIL: INFO@ORODISIENA.IT</p>
									</div>
								</aside>
							</div>
						</div>
					</div>
					<div class="push"></div>
				</div>
				<!--<div class="homeBox col-xs-12">
					<div class="dottedBox">Ciao</div>
					<div class="greyBox">Amico</div>
					<div class="yellowBox">mio</div>
				</div>-->
			</main>

<?php
include_once 'footer.php';
?>
