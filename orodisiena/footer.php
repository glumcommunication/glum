
		</div>
		<footer>
			<div class="container">
				<div class="credits">
					<p>
						L'ORO DI SIENA di Luppoli e C. S.n.c. | Via Garibaldi 61/65 - 53100 Siena | P. Iva: 01154400525 | Telefono: +39 0577 282729 | <a href="mailto:info@orodisiena.it">info@orodisiena.it</a>
					</p>
					<p style="font-size: 0.625em;margin-top: 5px;">Crafted by <a href="http://www.glumcommunication.it/" target="_blank">GLuM Communication</a></p>
				</div>
			</div>
		</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script>
	$('.carousel').carousel({
		interval: 3000
	})
</script>
<script src="js/lightbox-2.6.min.js"></script>


	</body>
</html>
