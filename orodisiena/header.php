<html>
	<head>
		<meta charset="utf-8" />
		<!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
		<meta name="viewport" content="width=1170">
		<script src="js/respond.js"></script>
		<link rel="icon" href="img/favicon.png" type="image/svg" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,600,700|Oswald:400,700' rel='stylesheet' type='text/css'>
		<meta name="description" content="<?php echo $pagedesc ?>">
		<link href="css/normalize.min.css" rel="stylesheet" media="screen">
		<link href="css/lightbox.css" rel="stylesheet" />
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
		<!--<link href="css/style.min.css" rel="stylesheet" media="screen">-->
		<link href="css/style.css" rel="stylesheet" media="screen">
		<meta name="keywords" content="Ristorante Siena, cucina tipica, cucina senese, pizza, cucina italiana, italian cookery, tuscan cookery, tipical, wine, cantina, vino, ingredienti di qualità, centro storico Siena, city centre Siena, pranzo, cena, lunch, dinner">
<?php
if ($pagename == "contatti" || $pagename == "index") {
?>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript">
				function initialize() {
				var latlng = new google.maps.LatLng(43.324795, 11.331325);
				var settings = {
					zoom: 15,
					center: latlng,
					mapTypeControl: true,
					mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
					navigationControl: true,
					navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				var map = new google.maps.Map(document.getElementById("map_canvas"), settings);

				var companyLogo = new google.maps.MarkerImage(
					'img/positionshadow.png',
					new google.maps.Size(86,58),
					new google.maps.Point(0,0),
					new google.maps.Point(20,50)
				);
				var companyPos = new google.maps.LatLng(43.324795, 11.331325);
				var companyMarker = new google.maps.Marker({
					position: companyPos,
					map: map,
					icon: companyLogo,
		//shadow: companyShadow,
					title:"Ristorante L'Oro di Siena"
				});
				var contentString = '<div id="content">'+
				'<div id="siteNotice">'+
				'</div>'+
				'<h1 id="firstHeading" class="firstHeading" style="font-size:14px;font-weight:bold;border-bottom: none;margin-bottom:5px;">Ristorante L\'Oro di Siena</h1>'+
				'<div id="bodyContent">'+
				'<p style="font-size:12px;">Via Garibaldi 61/65, 53100 Siena<br>Telefono: +39 0577 282729<br>Email: <a href="mailto:info@orodisiena.it">info@orodisiena.it</a><br>Website: <a href="http://www.orodisiena.it" title="Ristorante L\'Oro di Siena">www.orodisiena.it</a><br></p>'+
				'<p style="font-size: 12px"><a href="https://maps.google.com/maps?q=oro+di+siena,+siena&hl=it&ll=43.324795,11.331325&spn=0.007305,0.016512&sll=37.0625,-95.677068&sspn=63.856965,135.263672&t=h&z=17&iwloc=A" target="_BLANK">Visualizza in Google Maps</a></p>'+
				'</div>'+
				'</div>';

				var infowindow = new google.maps.InfoWindow({
					content: contentString
				});
				google.maps.event.addListener(companyMarker, 'click', function() {
					infowindow.open(map,companyMarker);
				});
			}
		</script>
<?php
}
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-49987786-1', 'orodisiena.it');
  ga('send', 'pageview');

</script>
		<title><?php echo $pagetitle ?> | Ristorante L'Oro di Siena - Via Garibaldi 61/65 - Siena</title>
	</head>
	<body <? if ($pagename == "contatti" || $pagename == "index") echo " onload=\"initialize()\"";?>>
		<div id="wrapper">
