<?php
$pagename = 'menu';
$pagetitle = 'Menù';
$pagedesc = "Le proposte della nostra cucina, realizzate con ingredienti di qualità.";
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
<main>
    <div class="mainContainer">
    <div class="container">
        <h1>Il nostro Menu</h1>
        <!-- <p style="text-align:center;margin-top:45px;">Stiamo rinnovando il nostro menù. Torna a trovarci per scoprire tutte le gustose novità.</p>
       div class="content" id="menu">
                <div class="col-xs-6 menuitems" style="border-right: 1px solid #333;">
                        <h2>Antipasti</h2>
                        <ul>
                                <li>Unoe</li>
                                <li>Duee</li>
                                <li>Tree</li>
                                <li>Quattroe</li>
                        </ul>
                </div>
                <div class="col-xs-6 menuitems">
                        <h2>Antipasti</h2>
                        <ul>
                                <li>Unoe</li>
                                <li>Duee</li>
                                <li>Tree</li>
                                <li>Quattroe</li>
                        </ul>
                </div>
        </div>
</div>
<div class="push"></div>
</div>
        <!--<div class="homeBox col-xs-12">
                <div class="dottedBox">Ciao</div>
                <div class="greyBox">Amico</div>
                <div class="yellowBox">mio</div>
        </div>-->

        <!-- Top Navigation -->
        <div class="row">
            <div class="col-sm-12">
                <div id="carouselHome2" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carouselHome" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselHome" data-slide-to="1"></li>
                        <li data-target="#carouselHome" data-slide-to="2"></li>
                        <li data-target="#carouselHome" data-slide-to="3"></li>
                        <li data-target="#carouselHome" data-slide-to="4"></li>
                        <li data-target="#carouselHome" data-slide-to="5"></li>
                        <li data-target="#carouselHome" data-slide-to="6"></li>
                        <li data-target="#carouselHome" data-slide-to="7"></li>
                        <li data-target="#carouselHome" data-slide-to="8"></li>
                        <li data-target="#carouselHome" data-slide-to="9"></li>
                        <li data-target="#carouselHome" data-slide-to="10"></li>
                        <li data-target="#carouselHome" data-slide-to="11"></li>
                        <li data-target="#carouselHome" data-slide-to="12"></li>
                 
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner"> <div class="item active">
                            <img  height="600px" width="400px" src="images/menu2/1.jpg" alt="...">
                        </div>
                        <div class="item">
                            <img src="images/menu2/2.jpg" alt="...">
                        </div>

                        <div class="item">
                            <img src="images/menu2/3.jpg" alt="...">
                        </div>
                        <div class="item">
                            <img src="images/menu2/4.jpg" alt="...">
                        </div>

                        <div class="item">
                            <img src="images/menu2/5.jpg" alt="...">
                        </div>
                        <div class="item">
                            <img src="images/menu2/6.jpg" alt="...">
                        </div>
                        <div class="item">
                            <img src="images/menu2/7.jpg" alt="...">
                        </div>
                            <div class="item">
                            <img src="images/menu2/8.jpg" alt="...">
                        </div>
                            <div class="item">
                            <img src="images/menu2/9.jpg" alt="...">
                        </div>
                            <div class="item">
                            <img src="images/menu2/10.jpg" alt="...">
                        </div>
                            <div class="item">
                            <img src="images/menu2/11.jpg" alt="...">
                        </div>
                            <div class="item">
                            <img src="images/menu2/12.jpg" alt="...">
                        </div>
                           
                    </div>
                    <a class="left carousel-control" href="#carouselHome2" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carouselHome2" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
                <div class="row">
                    <div class="text-center">
                        <h1> <a style="color:black; margin-bottom: 10px;" href="cartavini.php">La Nostra Carta dei Vini</a></h1>
                    </div>
                </div>
            </div>

        </div>

    </div>
        <?php
include_once 'footer.php';
?>
    </div>


</body>
</html>
</main>
