<?php
$pagename = 'menu';
$pagetitle = 'Il Menu';
$pagedesc = 'Scopri tutti i piatti che compongono il nostro ricchissimo menù.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
<div id="main">
    <div class="container" id="menu">
        <h1>Men&ugrave;</h1>
        <div class="row">
            <h2>Antipasti</h2>
            <div class="col-xs-6">
                <p>A01 Hiyayakko (Tofu crudo)</p>
                <p>A02 Aburaage (Tofu fritto)</p>
                <p>A03 Gyoza (Ravioli alla piastra)</p>
                <p>A04 Ebi Gyoza (Ravioli di gamberetti)</p>
                <p>A05 Edamame (Fagioli di soia)</p>
                <p>A06 Maguro Tataki (Filetto di tonno scottato)</p>
                <p>A07 Goma Wakame (Alghe insaporite)</p>                
            </div>
            <div class="col-xs-6">
                <p>A08 Nippon Rorusu (Involtini giapponesi)</p>
                <p>A09 Ebiresu No jagaimo (Gamberi in laccio di patate)</p>
                <p>A10 Yasai No Sankaku Bukuro (Sacchettini triangolari ripieni di verdure)</p>            
                <p>A11 Kaisen No Sankaku Bukuro (Sacchettini triangolari ripieni di frutti di mare)</p>
                <p>A12 Sunomono Moriawase (Misto di pesce con aceto di riso)</p>
                <p>A13 Tako No Sonomono (Polpo all'aceto di riso)</p>
            </div>            
        </div>
        <div class="row">
        	<h2>Insalate</h2>
            <div class="col-xs-6">
                <p>A14 Yasai Sarada (Insalata di verdure miste)</p>
                <p>A15 Kaisen Sarada (Insalata con frutti di mare)</p>
                <p>A16 Sashimi Sarada (Insalata con pesce crudo)</p>
            </div>
            <div class="col-xs-6">
                <p>A22 Ebi Sarada (Insalata di gamberi)</p>
                <p>A23 Tako Sarada (Insalata di polpo)</p>            
            </div>
        </div>
        <div class="row">
            <h2>Zuppe</h2>
            <div class="col-xs-6">
                <p>A17 Miso Shiro (Zuppa di miso)</p>
                <p>A18 Kaisen No Supu (Zuppa di pesce)</p>
            </div>
            <div class="col-xs-6">
            	<p>A19 Ebi To Suchimu No Tamago (Uova a vapore con gamberi)</p>
            </div>
        </div>
        <div class="row">
            <h2>Piatto unico</h2>
            <div class="col-xs-6">
            	<p>A21 Salmone Tartar (Tartare di salmone con olio di sesamo)</p>
            </div>
            <div class="col-xs-6">
            	<p>N07 Kaisen Tempura ( Frutti di mare fritti in farina di frumento)</p>
            </div>
        </div>
        <div class="row">
            
            <div class="col-xs-6">
            
            </div>
            <div class="col-xs-6">
            
            </div>
        </div>
        <div class="row">
            <h2>Nigiri Sushi</h2>
            <div class="col-xs-6">
            	<p>B01 Maguro (Tonno pinne gialle)</p>
                <p>B02 Sake (Salmone)</p>
                <p>B03 Suzuki (Branzino)</p>
                <p>B04 Tai (Orata)</p>
                <p>B05 Ama Ebi (Gamberi crudi)</p>
                <p>B06 Chori Ebi (Gamberi cotti)</p>
                <p>B07 Unagi (Anguilla)</p>
            </div>
            <div class="col-xs-6">
                <p>B08 Tako (Polpo)</p>
                <p>B10 Tobiko (Uova di pesce volante)</p>
                <p>B13 Kani (Granchio)</p>
                <p>B14 Tamago (Frittata)</p>
                <p>B17 Spicy Sake (Salmone tritato)</p>
                <p>B15 Nigiri Moriawase No Sho Kopi (Nigiri assortiti, 6 pezzi)</p>
                <p>B16 Nigiri Moriawase No Dai Bubun (Nigiri assortiti, 13 pezzi)</p>
            </div>
        </div>
        <div class="row">
            <h2>Sashimi</h2>
            <div class="col-xs-6">
                <p>C01 Maguro Sashimi (Tonno crudo)</p>
                <p>C02 Sake Sashimi (Salmone crudo)</p>
                <p>C03 Suzuki Sashimi (Branzino crudo)</p>
                <p>C04 Tai Sashimi (Orata cruda)</p>            
            </div>
            <div class="col-xs-6">
                <p>C05 Amaebi Sashimi (Gamberi crudi)</p>
                <p>C06 Sashimi Moriawase No Sho Bubun (Sashimi assortiti, 12 pezzi)</p>
                <p>C07 Sashimi No Moriawase Dai (Sashimi assortiti, 18 pezzi)</p>
            </div>
        </div>
        <div class="row">
            <h2>Hoso Maki</h2>
            <div class="col-xs-6">
                <p>D01 Tekka Maki Sei (Rotolo con tonno crudo)</p>
                <p>D02 Tekka Maki Chori (Rotolo con tonno cotto)</p>
                <p>D03 Sake Maki Sei (Rotolo con salmone crudo)</p>
                <p>D04 Sake Maki Chori (Rotolo con salmone cotto)</p>
                <p>D05 Suzuki Maki (Rotolo con branzino)</p>
                <p>D06 Ebi Maki (Rotolo con gamberi cotti)</p>            
            </div>
            <div class="col-xs-6">
                <p>D07 Unagi Maki (Rotolo con anguilla)</p>
                <p>D08 Kappa-maki (Rotolo con cetriolo)</p>
                <p>D09 Abokado Maki (Rotolo con avocado)</p>
                <p>D10 Daikon Maki (Rotolo con daikon)</p>
                <p>D11 Kani Maki (Rotolo con granchio)</p>
                <p>D12 Tamago Maki (Rotolo con frittata)</p>            
            </div>
        </div>
        <div class="row">
            <h2>Temaki (coni di alga arrotolati a mano)</h2>
            <div class="col-xs-6">
                <p>E01 Kariforunia Te Maki (Granchio, avocado, cetriolo, uova di pesce volante)</p>
                <p>E02 Ebi Te Maki (Gamberi)</p>
                <p>E03 Sake Abokado Te Maki (Salmone, avocado)</p>
            </div>
            <div class="col-xs-6">
                <p>E04 Unagi Te Maki (Anguilla)</p>
                <p>E05 Maguro tamago Te Maki (Frittata, tonno)</p>
            </div>
        </div>
        <div class="row">
            <h2>Uramaki</h2>
            <div class="col-xs-6">
                <p>F01 Furai Uramaki (Riso con cetriolo, granchio, branzino fritto)</p>
                <p>F02 Sake Uramaki (Riso con salmone , avocado)</p>
                <p>F03 Tempura Uramaki (Riso con tempura, cetriolo)</p>
                <p>F04 California Uramaki (Riso con avocado, cetriolo, granchio, decorato con salmone)</p>
                <p>F05 Tobiko Uramaki (Riso con avocado, granchio, frittata, decorato con uova di pesce volante)</p>
                <p>F06 Maguro Uramaki (Riso con tonno, avocado)</p>
                <p>F07 Hanabi Uramaki (Riso con tonno grigliato, gamberi)</p>
                <p>F08 Philadelphia Sake Uramaki (Riso con formaggio spalmabile, salmone grigliato)</p>
            </div>
            <div class="col-xs-6">
                <p>F09 Philadelphia Maguro Uramaki (Riso con formaggio spalmabile, tonno grigliato)</p>
                <p>F10 Philadelphia Ebi Abokado Uramaki (Riso con formaggio spalmabile, gamberi, avocado)</p>
                <p>F11 Reinbo Uramaki (Riso con granchio, cetriolo, decorato con salmone, tonno, branzino, avocado)</p>
                <p>F12 Yasai Uramaki (Riso con granchio, carota fritta, insalata, cetriolo, tobiko)</p>
                <p>F13 Spicy Sake Uramaki (Riso con Salmone, tobiko, tabasco, maionese, insalata)</p>
                <p>F14 Chicken Uramaki (Riso con pollo fritto, insalata, senape)</p>
                <p>F15 Ton Katsu Uramaki (Riso con maiale fritto, insalata)</p>
            </div>
        </div>
        <div class="row">
            <h2>Futomaki (Rotoli grandi con alga esterna)</h2>
            <div class="col-xs-6">
                <p>G01 Futomaki (Salmone, tonno, avocado, maionese, insalata)</p>
                <p>G02 Futomaki Moriawase (Granchio, cetriolo, ravanello, frittata, gamberi, avocado)</p>            
            </div>
            <div class="col-xs-6">
                <p>G03 Tempura Futomaki (Granchio, cetriolo, ravanello, frittata, gamberi, avocado avvolti in alga fritta)</p>
                <p>G04 Tsurai Futomaki (Leggermente piccante)</p>            
            </div>
        </div>
        <div class="row">
            <h2>Vassoi</h2>
            <div class="col-xs-6">
                <p>H04 Sushi Moriawase No Sho Kopi (6 nigiri, 3 tekkamaki, 3 sakemaki oppure 6 nigiri, 6 sakemaki)</p>
                <p>H05 Sushi Sashimi Moriawase x1 (6 nigiri, 8 sashimi, 6 hasomaki)</p>
                <p>H06 Sushi Sashimi Moriawase x2 (12 nigiri, 16 sashimi, 6 hasomaki, 8 uramaki)</p>            
            </div>
            <div class="col-xs-6">
                <p>H07 Sushi Sashimi Moriawase x3 (18 nigiri, 24 sashimi, 8 uramaki, 8 futomaki)</p>
                <p>H08 Tartar Don (Tartare di salmone con olio di sesamo e riso)</p>
            </div>
        </div>
        <div class="row">
            <h2>Primi Piatti</h2>
            <div class="col-xs-6">
                <p>J01 Hakumai (Riso bianco)</p>
                <p>J02 Sushi Mai (Riso da sushi)</p>
                <p>J03 Yasai Chahan (Riso saltato con verdure)</p>
                <p>J04 Keiniku Chahan (Riso saltato con pollo)</p>
                <p>J05 Kaisen Chahan (Riso saltato con frutti di mare)</p>
                <p>J06 Kare Ushi Meshi (Riso di curry con manzo)</p>
                <p>J07 Unagidon (Ciotola di riso con anguilla)</p>
                <p>J08 Katsudon (Ciotola di riso con cotoletta di maiale)</p>
                <p>J09 Yasai Itame Udon (Udon saltato con verdure)</p>
                <p>J10 Keiniku Itame Udon (Udon saltato con pollo)</p>
                <p>J12 Yasai Itame Soba (Soba saltato con verdure)</p>
                <p>J13 Keiniku Itame Soba (Soba saltato con pollo)</p>
                <p>J14 Kaisen Itame Soba (Soba saltato con frutti di mare)</p>
                <p>J15 Yasai Udon Supu (Udon con verdure in brodo)</p>
                <p>J16 Keiniku Udon Supu (Udon con pollo in brodo)</p>
                <p>J17 Kaisen Udon Supu (Udon con frutti di mare in brodo)</p>  
            </div>
            <div class="col-xs-6">
                <p>J18 Tempura Udon Supu (Udon con Tempura in brodo)</p>
                <p>J19 Yasai Soba Supu (Soba con verdure in brodo)</p>
                <p>J20 Keiniku Soba Supu (Soba con pollo in brodo)</p>
                <p>J21 Kaisen Soba Supu (Soba con frutti di mare in brodo)</p>
                <p>J22 Tempura Soba Supu (Soba con tempura in brodo)</p>
                <p>J23 Hiya Men (Pasta fredda)</p>
                <p>J24 Yasai Ramen Supu (Ramen con verdure in brodo)</p>
                <p>J25 Keiniku Ramen Supu (Ramen con pollo in brodo)</p>
                <p>J26 Gyuniku Ramen Supu (Ramen con manzo in brodo)</p>
                <p>J27 Kaisen Ramen Supu (Ramen con frutti di mare in brodo)</p>
                <p>J28 Tempura Ramen Supu (Ramen con tempura in brodo)</p>
                <p>J29 Ramen No Tokushoku (Ramen speciale)</p>
                <p>J30 Sake Chahan (Riso saltato con salmone)</p>
                <p>J31 Yasai Komemen (Spaghetti di riso saltato con verdure)</p>
                <p>J32 Keiniku Komemen (Spaghetti di riso saltato con pollo)</p>
                <p>J33 Kaisen Komemen (Spaghetti di riso saltato con frutti di mare)</p>
            </div>
        </div>
        <div class="row">
            <h2>Teppan Yaki (Cucina alla piastra)</h2>
            <div class="col-xs-6">
                <p>K01 Teppan'yaki No Maguro (Tonno alla piastra)</p>
                <p>K02 Teppan'yaki Sake (Salmone alla piastra)</p>
                <p>K03 Teppan'yaki Suzuki (Branzino alla piastra)</p>
                <p>K04 Teppan'yaki Tai (Orata alla piastra)</p>
                <p>K05 Teppan'yaki No Kurumaebi (Gamberoni alla piastra)</p>
                <p>K06 Teppan'yaki No Surumeika (Calamari alla piastra)</p>
                <p>K08 Teppan'yaki Shifudo No Moriawase (Misto di pesce alla piastra)</p>
            </div>
            <div class="col-xs-6">
                <p>K09 Buta Niku No Teppan'yaki (Maiale alla piastra)</p>
                <p>K10 Keiniku No Teppan'yaki (Pollo alla piastra)</p>
                <p>K11 Teppan'yaki No Gyuniku (Manzo alla piastra)</p>
                <p>K12 Teppan'yaki No Niku Kongo (Misto di carne alla piastra)</p>
                <p>K14 Teppan'yaki Yasai No Moriawase (Verdure miste alla piastra)</p>
                <p>K15 Teppan'yaki Moyashi (Germogli di soia alla piastra)</p>
            </div>
        </div>
        <div class="row">
            <h2>Kushiyaki (Spiedini)</h2>
            <div class="col-xs-6">
                <p>L01 Kushiyaki Surumeika (Spiedini di calamari)</p>
                <p>L02 Kushiyaki Kurumaebi (Spiedini di gamberoni)</p>
            </div>
            <div class="col-xs-6">
                <p>L03 Buta Niku No Kushiyaki (Spiedini di maiale)</p>
                <p>L04 Kushiyaki Gyuniku (Spiedini di manzo)</p>
            </div>
        </div>
        <div class="row">
            <h2>Tempura</h2>
            <div class="col-xs-6">
                <p>N01 Yasai Tempura (Verdure fritte)</p>
                <p>N03 Ebi Tempura (Gamberoni fritti)</p>
                <p>N04 Tempura No Moriawase (Gamberoni e verdure fritte)</p>
            </div>
            <div class="col-xs-6">
                <p>N05 To Age (Bocconcini di pollo fritto)</p>
                <p>N07 Ageta Ebiboru (Polpette di gambero fritte)</p>
            </div>
        </div>
        <div class="row">
            <h2>Dezato (Dessert)</h2>
            <div class="col-xs-6">
                <p>P01 Shiro Daifuku (Gnocco di riso glutinoso con marmellata di fagioli rossi)</p>
                <p>P02 Sakura Mochi (Riso con marmellata di fagioli azuki in foglia di ciliegio)</p>
                <p>P04 Kusaazuki Dango (Dolcetto di riso al the verde e marmellata di fagioli rossi)</p>
                <p>P05 Sushi Dezato (Gelati ai gusti limone, fragola, mango, frutti di bosco)</p>
                <p>P06 Tiramisù Ryokucha (Tiramisù al the verde)</p>
                <p>P07 Teppan'yaki Aisukurimu (Gelato alla piastra)</p>
            </div>
            <div class="col-xs-6">
                <p>P08 Ryokucha Aisukurimu (Gelato al the verde)</p>
                <p>P09 Gomaaisukurimu (Gelato al sesamo) </p>
                <p>P11 Kurimu Aisukurimu (Gelato alla crema)</p>
                <p>P12 Moriawase Aisukurimu (Gelato misto)</p>
                <p>P13 Shinsen'na Kudamono (Frutta fresca)</p>
            </div>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>
