<?php
$pagename = 'contatti';
$pagetitle = 'Contattaci';
$pagedesc = 'Contatta Minohana per informazioni e prenotazioni: 0577-274997; mobile: 333-9846829.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
if ($_POST['doSend']=='Invia') {
$name = $_POST[name];
$email = $_POST[email];
$message = $_POST[message];
$headers = "";
$headers .= "From: $email\n";
$headers .= "Reply-To: $email\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=utf-8\n";
$to = "info@ristoranteminohana.it";
$subject = "Richiesta informazioni";
/*$message = $messaggio;*/
$messagebody = "Hai ricevuto una nuova email via ristoranteminohana.it, ecco i dettagli:<br />
Nome: $name <br />

E-mail: $email<br />

Messaggio: $message<br />";

mail ($to,$subject,$messagebody,$headers);
$sent = "1";
}

?>
<div id="main">
    <div class="container">
        <div class="col-xs-6" id="leftside">
            <h1>Contattaci</h1>
<?php
if ($sent=='1') {
echo <<<_END
<div class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>Messaggio inviato!</h4>
  Sarai ricontattato al pi&ugrave; presto.
</div>
_END;
}
?>
            <form action="contatti.php" method="post" id="contatti">
                <div class="form-group">
                    <label for="name">Nome e Cognome</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Inserisci il tuo nome">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Inserisci la tua email">
                </div>
                <div class="form-group">
                      <label for="message">Messaggio</label>
                      <textarea class="form-control" rows="7" id="message" name="message"></textarea>
                </div>
                <div class="checkbox">
                    <label>
                      <input type="checkbox" value="Yep" name="privacy">
                        <a id="privacy" href="#" rel="tooltip" data-toggle="tooltip" title="Nel inviare i miei dati dichiaro di aver letto l'informativa e sono consapevole che il trattamento degli stessi è necessario per ottenere il servizio proposto. A tal fine, nel dichiarare di essere maggiorenne, fornisco il mio consenso." data-placement="right" style="font-size:14px;cursor: help;">
                            Informativa sulla Privacy ai sensi del D.Lgs 196/2003</a>
                    </label>
                </div>
                <button type="submit" class="btn btn-default" name="doSend" value="Invia">Invia</button>
            </form>
        </div>
        <div class="col-xs-6" id="rightside" style="padding-top:120px;">
            <p style="text-align: center;font-size: 1.25em;">
                Ristorante Minohana<br>
                Viale V. Emanuele II 32-34, 53100 Siena<br>
                Telefono: +39 0577 274997<br>
                Cellulare: +39 333 9846829<br>
                P. Iva: 01346310525
            </p>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>