<?php
$pagename = 'dovesiamo';
$pagetitle = 'Dove siamo';
$pagedesc = 'Viale Vittorio Emanuele II 32/34, ad un passo dalle mura del centro Storico, vicino alla suggestiva Porta Camollia, in una zona facilmente raggiungibile in auto.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
<div id="main">
    <div class="container">
        <div class="col-xs-6" id="leftside">
            <div id="map_canvas" style="width:100%; height:400px;margin-bottom: 15px;"></div>
        </div>
        <div class="col-xs-6" id="rightside" style="padding-top:120px;">
            <h2>Siamo in Viale V. Emanuele II 32-34, 53100 Siena</h2>
            <h1>Orario di apertura</h1>
            
            <p style="text-align: center;font-size: 1.25em;">
                Siamo aperti tutti i giorni dalle 12:00 alle 15:00<br>
                e dalle 19:00 alle 23:00.
            </p>
            <p style="text-align: center;font-size: 1.25em;">
                Chiuso lunedì a pranzo.
            </p>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>