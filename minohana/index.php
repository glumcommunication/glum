<?php
$pagename = 'index';
$pagetitle = 'Home';
$pagedesc = 'Ristorante Minohana a Siena, il meglio della cucina giapponese. Sushi, sashimi, tempura, zuppe e vermicelli secondo la tradizione del Sol Levante.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
<div id="main">
    <div class="container">
        <img src="img/minohana-home.jpg" alt="Ristorante Minohana">
        <div class="row">
            <div class="intro">
                <h2>Benvenuti da Minohana</h2>
                <h1>LA TRADIZIONE CULINARIA GIAPPONESE A SIENA</h1>
            </div>
        </div>
    </div>
    <div class="redstripe">
        <div class="container">
            <div class="homesquare col-xs-4">
                <h1>ALL YOU CAN EAT</h1>
            </div>
            <div class="homesquare col-xs-4">
                <h1>Cucina Giapponese</h1>
            </div>
            <div class="homesquare col-xs-4">
                <h1>13,90 &euro; A PRANZO<br>20,90&euro; A CENA</h1>
            </div>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>
