<?php
$pagename = 'intro';
$pagetitle = 'Ristorante Osaka | Ristorante Minohana';
$pagedesc = "Minohana e Osaka: la tradizione culinaria dell'estremo oriente sbarca a Siena.";
include_once 'dbc.php';
include_once 'header.php';
//include_once 'nav.php'
?>
<div id="main">
    <div class="container">
        <div class="col-xs-6 welcome" id="leftwelcome">
            <img src="img/osakaintro.jpg" alt="Ristorante Osaka">
            <a href="http://www.ristoranteosaka.com/home.php">
            <div id="entraosaka">
                <img src="img/redarrow.jpg" alt="Visita ristoranteosaka.com">
                <p>ENTRA</p>
            </div>
            </a>
            <div class="weladdress">
                <p>Via Pantaneto 107-109, 53100 Siena</p>
            </div>
        </div>
        <div class="col-xs-6 welcome" id="rightwelcome">
            <img src="img/minohanaintro.jpg" alt="Ristorante Minohana">
            <a href="http://www.ristoranteminohana.it/home.php">
                <div id="entraminohana">
                    <img src="img/blackarrow.jpg" alt="Visita ristoranteminohana.it">
                    <p>ENTRA</p>
                </div>
            </a>
            <div class="weladdress">
                <p>Viale Vittorio Emanuele II 32-34, 53100 Siena</p>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>
