            <div id="header">
                <div class="container">
                    <div id="logo" class="col-xs-3">
                        <a href="home.php" title="Home">
                            <img src="img/minohanalogo.png" alt="Ristorante Minoahana">
                        </a>
                    </div>
                    <div id="social" class="col-xs-9">
                        <!--<ul>
                            <li>
                                <a href="#" title="Twitter">Twitter</a>
                            </li>
                            <li>
                                <a href="#" title="Twitter">Facebook</a>
                            </li>
                            <li>
                                <a href="#" title="Twitter">Google+</a>
                            </li>
                        </ul>-->
                    </div>
                </div>
            </div>
            <div id="topmenu">
                <div class="container">
                    <ul class="nav nav-justified">
                        <li <?php if ($pagename == "index") {echo 'class="selected"';} ?>>
                            <a href="index.php" title="">Home</a>
                        </li>
                        <li <?php if ($pagename == "ristorante") {echo 'class="selected"';} ?>>
                            <a href="ristorante.php" title="">Ristorante</a>
                        </li>
                        <li <?php if ($pagename == "menu") {echo 'class="selected"';} ?>>
                            <a href="menu.php" title="">Men&ugrave;</a>
                        </li>
                        <li <?php if ($pagename == "gallery") {echo 'class="selected"';} ?>>
                            <a href="gallery.php" title="">Gallery</a>
                        </li>
                        <li <?php if ($pagename == "dovesiamo") {echo 'class="selected"';} ?>>
                            <a href="dovesiamo.php" title="">Dove siamo</a>
                        </li>
                        <li <?php if ($pagename == "contatti") {echo 'class="selected"';} ?>>
                            <a href="contatti.php" title="">Contatti</a>
                        </li>
                    </ul>
                </div>
            </div>
