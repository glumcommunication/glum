<?php
$pagename = 'ristorante';
$pagetitle = 'Il ristorante';
$pagedesc = 'Minohana porta a Siena la tradizione culinaria giapponese con le sue preparazioni semplici e raffinate.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
<div id="main">
    <div class="container">
        <div class="col-xs-6" id="leftside">
            <img src="img/ristpic.jpg" alt="Il ristorante">
        </div>
        <div class="col-xs-6" id="rightside">
            <h1>Il Ristorante</h1>
            <h2>La tradizione culinaria giapponese a Siena</h2>
            <p>
                A due passi dalla suggestiva Porta Camollia il ristorante Minohana propone una rilettura in chiave moderna di ricette della 
                tradizione nipponica. Situato in una posizione ottima, ad un passo dal centro storico e facilmente raggiungibile in auto, 
                Minohana è diventato un punto di riferimento per gli amanti della cucina giapponese.
            </p>
            <p>
                Nella cultura giapponese cucinare assume spesso un aspetto rituale. La cura riservata alla selezione della qualità delle 
                materie prime e alla loro preparazione ne ha fatto una delle cucine più apprezzate internazionalmente. Tutti conoscono 
                ormai la delicatezza di pietanze come sushi e sashimi, tanto semplici in apparenza quanto difficili da preparare. Ma la cucina 
                giapponese è anche molto altro: da provare la tempura, il famoso fritto alla maniera giapponese, ma anche le zuppe e gli 
                spaghetti di riso.
            </p>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>