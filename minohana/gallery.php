<?php
$pagename = 'gallery';
$pagetitle = 'La gallery';
$pagedesc = '';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
<div id="main">
    <div class="container">
<?php
for ($i=1;$i<=9;$i++) {
?>    
        <div class="col-xs-3 imgal">
            <a href="img/gal/minohana0<?php echo $i; ?>.jpg" data-lightbox="minohana">
                <img src="img/gal/minohana0<?php echo $i; ?>.jpg" alt="Ristorante Minohana" class="img-thumbnail">
            </a>
        </div>
<?php
}
for ($i=10;$i<=20;$i++) {
?>
        <div class="col-xs-3 imgal">
            <a href="img/gal/minohana<?php echo $i; ?>.jpg" data-lightbox="minohana">
                <img src="img/gal/minohana<?php echo $i; ?>.jpg" data-lightbox="minohana" alt="Ristorante Minohana" class="img-thumbnail">
            </a>
        </div>
<?php
}
?>

    </div>
</div>
<?php
include_once 'footer.php';
?>