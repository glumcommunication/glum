<?php
$pagetitle = "Contatti";
$pagename = "contatti";
$pagedesc = "Strada di Cerchiaia 31, 53100 Siena. Telefono: 057744920, Email: info@taddeucciantiquariato.it. Per informazioni o per richieste sui pezzi esposti nel nostro sito telefonaci o scrivici una email.";
include_once 'header.php';
include_once 'navigation.php';
?>
<div class="container" onload="initialize()">
    <div class="row-fluid lastrow">
        <div class="span12 shadowed">
            <div class="ribbon">
                <h2>Contatti</h2>                
            </div>
            <div class="defaultBox">
                <div id="map_canvas" style="width:100%; height:400px;">
                </div>
                <div class="row-fluid lastrow">
                    <div class="span4">
                        <h3>Informazioni</h3>
                        <h4>Taddeucci Antiquariato<br>di F. Siveri e A. Fabbiani s.n.c.</h4>
                        <p>
                            Strada di Cerchiaia 29/31, 53100 Siena
                        </p>
                        <p>
                            Telefono: +39 0577 44920<br>
                            Cellulare+39 335 5885192
                        </p>
                        <p>
                            Email: <a href="mailto:info@taddeucciantiquariato.it">info@taddeucciantiquariato.it</a><br>
                            <span style="visibility:hidden;">Email: </span><a href="mailto:taddeucci.antiquariato@gmail.com">taddeucci.antiquariato@gmail.com</a>
                        </p>
                    </div>
                    <div class="span8">
                        <h3>Come raggiungerci</h3>
                            <h4>
                                Da Roma o da sud
                            </h4>
                            <p>
                                <!--Prendere l'autostrada A1 direzione Nord, uscire al casello Valdichiana. Proseguire in direzione Siena e imboccare il raccordo autostradale Siena - Bettole.
                                Al termine del raccordo, proseguire in direzione Firenze - Tangenziale Ovest di Siena e scegliere l'uscita Siena Sud.
                                Alla rotatoria prendere la seconda uscita e proseguire in direzione Porta Tufi/Parcheggio Il Campo. All'incrocio per il Parcheggio Il Campo proseguire 
                                dritto in direzione Porta Romana per 200 metri poi svoltare a destra verso Insediamento Produttivo Cerchiaia. Proseguire fino alla rotatoria e prendere 
                                la seconda uscita. Taddeucci Antiquariato si trova sulla sinistra, in Strada di Cerchiaia 31.-->
                                Dopo l'uscita <strong>SIENA-SUD</strong>: Alla rotatoria seconda uscita direzione Buonconvento - Montalcino, poi prendere la seconda strada a destra 
                                (<strong>ZONA ARTIGIANALE/COMMERCIALE CERCHIAIA</strong>) e proseguire diritto fino al numero 29/31. 
                            </p>
                            <h4>
                                Da Firenze o da nord
                            </h4>
                            <p>
                                <!--Dirigersi verso il raccordo autostradale Firenze - Siena (uscita A1 a Firenze Impruneta), proseguire in direzione Siena/Colle val D'Elsa/Poggibonsi/Grosseto-Fi-Si.
                                Dalla tangenziale Ovest di Siena scegliere l'uscita Siena Sud. Allo Stop svoltare a sinistra, alla rotonda prendere la prima uscita e proseguire in direzione Porta Tufi/Parcheggio Il Campo. All'incrocio per il Parcheggio Il Campo proseguire 
                                dritto in direzione Porta Romana per 200 metri poi svoltare a destra verso Insediamento Produttivo Cerchiaia. Proseguire fino alla rotatoria e prendere 
                                la seconda uscita. Taddeucci Antiquariato si trova sulla sinistra, in Strada di Cerchiaia 31.-->
                                Uscita <strong>A1 IMPRUNETA</strong> prendere la superstrada per Siena e uscire a <strong>SIENA-SUD</strong>, allo stop svoltare a sinistra, alla 
                                rotatoria prima uscita direzione Buonconvento - Montalcino, poi prendere la seconda strada a destra (<strong>ZONA ARTIGIANALE/COMMERCIALE CERCHIAIA</strong>) 
                                e proseguire diritto fino al numero 29/31.
                            </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>