<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php echo $pagedesc ?>">
        <meta name="keywords" content="antiquities, antique furniture, taddeucci antichità, antiquario, antiquariato, cerchiaia, strada di cerchiaia 31, siena, siveri, fabbiani, tavoli, sedie, comò, credenze, specchiere, dipinti, lampadari, perizie giurate, stime, occasioni, arte, laboratorio">
        <link rel="icon" href="images/favicon.png" type="image/svg" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.1/jquery.min.js"></script>
<script src="js/jquery-1.7.2.min.js"></script>

<?php
include_once 'cbd.php';
if ($pagename == "contatti") {
    echo <<<_END
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    function initialize() {
        var latlng = new google.maps.LatLng(43.290772, 11.338282);
        var settings = {
            zoom: 15,
            center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
            navigationControl: true,
            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
            mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"), settings);

  var companyLogo = new google.maps.MarkerImage('images/positionshadow.png',
    new google.maps.Size(86,58),
    new google.maps.Point(0,0),
    new google.maps.Point(20,50)
);

var companyPos = new google.maps.LatLng(43.290772, 11.338282);
var companyMarker = new google.maps.Marker({
    position: companyPos,
    map: map,
    icon: companyLogo,
    //shadow: companyShadow,
    title:"Taddeucci Antiquariato"
});
var contentString = '<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h1 id="firstHeading" class="firstHeading">Taddeucci Antiquariato</h1>'+
    '<h3>di F. Siveri e A. Fabbiani s.n.c.</h3>'+
    '<div id="bodyContent">'+
    '<p>Strada di Cerchiaia 29/31 - 53100 Siena<br>Telefono: +39 0577  44920 - Cellulare: +39 335 5885192<br>'+
    'Email: <a href="mailto:info@taddeucciantiquariato.it">info@taddeucciantiquariato.it</a> - <a href="mailto:taddeucci.antiquariato@gmail.com">taddeucci.antiquariato@gmail.com</a><br>'+
    'Website: <a href="http://www.taddeucciantiquariato.it" title="Example SpA">www.taddeucciantiquariato.it</a><br></p>'+
    '</div>'+
    '</div>';
 
var infowindow = new google.maps.InfoWindow({
    content: contentString
});
google.maps.event.addListener(companyMarker, 'click', function() {
  infowindow.open(map,companyMarker);
});
    }
    </script>
_END;
}
if ($pagename == "prodotti") {
echo <<<_END
     
  <script src="js/jquery.collagePlus.min.js"></script>
  <script src="js/jquery.removeWhitespace.min.js"></script>
  <script src="js/jquery.collageCaption.min.js"></script>
  <script src="js/lightbox.js"></script>  
  <script type="text/javascript">
    $(window).load(function () {
        $(document).ready(function(){
            collage();
        });
    });
    function collage() {
        $('.Collage').removeWhitespace().collagePlus(
            {
                'allowPartialLastRow' : true,
                'effect' : 'effect-3',
                'fadeSpeed' : 2000,
                'targetHeight' : 200
            }
        ).collageCaption();
    };

        
     
    var resizeTimer = null;
    $(window).bind('resize', function() {
        // hide all the images until we resize them
        $('.Collage .Image_Wrapper').css("opacity", 0);
        // set a timer to re-apply the plugin
        if (resizeTimer) clearTimeout(resizeTimer);
        resizeTimer = setTimeout(collage, 200);
    });

  </script>
_END;
}
?>        
        <!-- Bootstrap -->
        
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet" media="screen">
        
        <link href="css/lightbox.css" rel="stylesheet" media="screen">
        
        <title>
            <?php echo $pagetitle ?> - Taddeucci Antiquariato 
        </title>
    </head>
<body
<?php
if ($pagename == "contatti") {
    echo " onload=\"initialize()\"";
}
?>
>
<?php include_once("analyticstracking.php") ?>
<div class="wrap">    