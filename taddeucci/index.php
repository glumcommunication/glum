<?php
$pagetitle = "Home";
$pagename = "home";
$pagedesc = "Per chi ama gli oggetti antichi e di pregio, ANTICHITA' TADDEUCCI ` dal 1930 a Siena, sinonimo di qualità ed esperienza. ANTICHITA' TADDEUCCI nella sua grande esposizione, offre un selezionato e vasto assortimento di pezzi prestigiosi, rigorosamente autentici e certificati. ANTICHITA' TADDEUCCI, fa consulenza, stime e perizie giurate ed ha uno dei più noti laboratori di restauro, ";
include_once 'header.php';
include_once 'navigation.php';
?>

<div class="container shadowed">
    <div id="myCarousel" class="carousel slide carousel-fade" data-interval=2000>
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
            <li data-target="#myCarousel" data-slide-to="4"></li>
            <li data-target="#myCarousel" data-slide-to="5"></li>
            <li data-target="#myCarousel" data-slide-to="6"></li>
            <li data-target="#myCarousel" data-slide-to="7"></li>
            <li data-target="#myCarousel" data-slide-to="8"></li>
            <li data-target="#myCarousel" data-slide-to="9"></li>
        </ol>
        <!-- Carousel items -->
        <div class="carousel-inner">
            <div class="active item carouselImageHeight">
                <img src="images/home/taddeucci01.jpg" alt="Taddeucci Antiquariato" class="">
            </div>
            <div class="item carouselImageHeight">
                <img src="images/home/taddeucci02.jpg" alt="Taddeucci Antiquariato" class="">
            </div>
            <div class="item carouselImageHeight">
                <img src="images/home/taddeucci03.jpg" alt="Taddeucci Antiquariato" class="">
            </div>
            <div class="item carouselImageHeight">
                <img src="images/home/taddeucci04.jpg" alt="Taddeucci Antiquariato" class="">
            </div>
            <div class="item carouselImageHeight">
                <img src="images/home/taddeucci05.jpg" alt="Taddeucci Antiquariato" class="">
            </div>
            <div class="item carouselImageHeight">
                <img src="images/home/taddeucci06.jpg" alt="Taddeucci Antiquariato" class="">
            </div>
            <div class="item carouselImageHeight">
                <img src="images/home/taddeucci07.jpg" alt="Taddeucci Antiquariato" class="">
            </div>
            <div class="item carouselImageHeight">
                <img src="images/home/taddeucci08.jpg" alt="Taddeucci Antiquariato" class="">
            </div>
            <div class="item carouselImageHeight">
                <img src="images/home/taddeucci09.jpg" alt="Taddeucci Antiquariato" class="">
            </div>
            <div class="item carouselImageHeight">
                <img src="images/home/taddeucci10.jpg" alt="Taddeucci Antiquariato" class="">
            </div>        
        </div>
        <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div>

    <div class="row-fluid lastrow">
        <div class="span6">
            <div class="ribbon">
                <h2>ORARI</h2>
            </div>
            <div class="defaultBox">
            <h3>Orari Negozio</h3>
                <p>
                Il negozio di <strong>Taddeucci Antiquariato</strong> osserva i seguenti orari di apertura:
                </p>
                <p>
                <strong>Dal lunedì al sabato</strong>: 8:30 - 13:00 15:30 - 20:00
                </p>
                <p>
                    La domenica il punto vendita è aperto solo su appuntamento, per informazioni telefonare allo 0577 44920.
                </p>
                <h3>Orari Laboratorio</h3>
                <p>
                Il laboratorio di <strong>Taddeucci Antiquariato</strong> osserva i seguenti orari di apertura:
                </p>
                <p>
                <strong>Dal lunedì al venerdì</strong>: 8:30 - 12:30 15:00 - 19:00
                </p>
                <p>
                    Il laboratorio è chiuso il sabato e la domenica.
                </p>
            </div>
        </div>
        <div class="span6">
            <div class="ribbon">
                <h2>CONTATTACI</h2>
            </div>
            <div class="defaultBox">
<?php
if ($_POST['doSend']=='Invia') {
$name = $_POST[name];
//$address = $_POST[address];
//$phone = $_POST[phone];
$email = $_POST[email];
$message = $_POST[message];
$headers = "";
$headers .= "From: $email\n";
$headers .= "Reply-To: $email\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=utf-8\n";
$to = "info@taddeucciantiquariato.it";
$subject = "Richiesta informazioni";
/*$message = $messaggio;*/
$messagebody = "Hai ricevuto una nuova email via taddeucciantiquariato.it, ecco i dettagli:<br />
Nome e Cognome: $name<br />

E-mail: $email<br />

Messaggio: $message<br />";

mail ($to,$subject,$messagebody,$headers);
$sent = "1";
}
?>
            <form class="form-horizontal" action="index.php"  method="post" id="form">
              <fieldset>
              <?php
if ($sent=='1') {
echo <<<_END
<div class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>Messaggio inviato!</h4>
  Sarai ricontattato al pi&ugrave; presto.
</div>
_END;
}
?>
                  
                <!--<p class="alert alert-block">I campi contrassegnati dall'asterisco sono obbligatori.</p>-->
                <div class="control-group">
                    <label class="control-label" for="name">Nome e Cognome</label>
                    <div class="controls">
                        <input type="text" name="name" id="name" placeholder="Es. Mario Rossi">
                    </div>
                </div>
                <!--<div class="control-group">
                    <label class="control-label" for="address">Indirizzo completo</label>
                    <div class="controls">
                        <input type="text" name="address" id="address" placeholder="Es. Via Roma 10, 53100 Siena">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="phone">Telefono *</label>
                    <div class="controls">
                        <input type="text" name="phone" id="phone" placeholder="Es. 3331234567, 057712345...">
                    </div>
                </div>-->
                <div class="control-group">
                    <label class="control-label" for="email">Email</label>
                    <div class="controls">
                        <input type="email" name="email" id="email" placeholder="Es. mail@example.com">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="message">Messaggio</label>
                    <div class="controls">
                        <textarea rows="3" name="message" id="message"></textarea>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                      <button type="submit" class="btn btn-lille" name="doSend" value="Invia" id="submitbtn">Invia</button>
                    </div>
                </div>
                    
                    <a href="#" rel="tooltip" data-toggle="tooltip" 
                    title="Nel inviare i miei dati dichiaro di aver letto l'informativa e sono 
                    consapevole che il trattamento degli stessi è necessario per ottenere 
                    il servizio proposto. A tal fine, nel dichiarare di essere maggiorenne, 
                    fornisco il mio consenso." 
                    data-placement="right" style="font-size:12px; cursor: help;">Informativa sulla Privacy ai sensi del D.Lgs 196/2003</a>
                </fieldset>
            </form>
            </div>
        </div>
    </div>
</div>


<?php
include_once 'footer.php';
?>