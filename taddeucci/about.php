<?php
$pagetitle = "Agenzia";
$pagename = "agenzia";
$pagedesc = "Per chi ama gli oggetti antichi e di pregio, ANTICHITA' TADDEUCCI ` dal 1930 a Siena, sinonimo di qualità ed esperienza. ANTICHITA' TADDEUCCI nella sua grande esposizione, offre un selezionato e vasto assortimento di pezzi prestigiosi, rigorosamente autentici e certificati. ANTICHITA' TADDEUCCI, fa consulenza, stime e perizie giurate ed ha uno dei più noti laboratori di restauro, ";
include_once 'header.php';
include_once 'navigation.php';
?>
<div class="container">
    <div class="row-fluid lastrow">
        <div class="span12 shadowed">
            <div class="container ribbon">
                <h2>Chi siamo</h2>
            </div>
            <div class="defaultBox">
            <p class="justify" style="text-align:center;">
                <img src="images/it.png" alt="Italian">
            </p>
            <p class="justify">
                Per chi ama gli oggetti antichi e di pregio, <strong>ANTICHITA' TADDEUCCI</strong> &egrave; da sempre a Siena, sinonimo di qualit&agrave; ed esperienza 
                nel settore dell'antiquariato.
            </p>
            <p class="justify">
                Una professione iniziata nel 1930 e continuata con entusiasmo e passione fino ad oggi facendosi immediatamente distinguere per seriet&agrave;, 
                disponibilit&agrave; e competenza da tutti coloro che sono diventati nel tempo prima clienti e, successivamente, anche amici.
            </p>
            <p class="justify">                 
                Infatti, <strong>ANTICHITA' TADDEUCCI</strong> nella sua grande esposizione offre un selezionato e vasto assortimento di pezzi prestigiosi, 
                rigorosamente autentici e certificati.
            </p>
            <p class="justify">                
                Da <strong>ANTICHITA' TADDEUCCI</strong> troverete la più alta professionalit&agrave; e competenza su mobili, dipinti e oggetti di antiquariato 
                dal XVI° agli inizi del XX° secolo, garantendovi gratuitamente e senza impegno il loro migliore ed armonico inserimento nella vostra casa.
            </p>
            <p class="justify">                 
                <strong>ANTICHITA' TADDEUCCI</strong> fa consulenza, stime e perizie, anche giurate, da sempre ed ha uno dei migliori e pi&ugrave; noti laboratori di 
                restauro per mobili e dipinti, con maestranze altamente specializzate.
            </p>
            <p class="justify">                 
                Da <strong>ANTICHITA' TADDEUCCI</strong> troverete solo opere italiane, provenienti direttamente dalle famiglie o comunque da collezioni private 
                al fine di proporvi oggetti assolutamente genuini e non provenienti da aste o dal mercato in generale.
            </p>
            <p class="justify" style="text-align:center;">
                <img src="images/uk.png" alt="English">
            </p>
            <p class="justify">
                For the ones who love antiques and objects of value, <strong>ANTICHITA' TADDEUCCI</strong> is a synonym of quality and experience in the antique trade 
                in Siena.
            </p>
            <p class="justify">  
                A profession that took start in 1930 and went on with enthusiasm and passion until now, immediately standing out for reliability, helpfulness and knowledge, 
                for all the people that became in the course of time at first customers and then also friends.
            </p>
            <p class="justify">  
                Indeed, <strong>ANTICHITA' TADDEUCCI</strong> in his big showroom offers a selected and ample assortment of prestigious pieces, severely certified and authentic.
            </p>
            <p class="justify">  
                At <strong>ANTICHITA' TADDEUCCI</strong> you will find the highest professionalism and knowledge about furniture, paintings and antiques from 16th to early 20th 
                century, with the free warranty of the best and most harmonious integration inside your home.
            </p>
            <p class="justify">  
                <strong>ANTICHITA' TADDEUCCI</strong> also gives his expert advice, evaluations and surveys and has one of the best known and best refurbishment workshop for 
                furniture and paintings, with highly qualified workforce.
            </p>
            <p class="justify">  
                At <strong>ANTICHITA' TADDEUCCI</strong> you will find only italian artcrafts, coming directly from the families or however from private collections, in order 
                to propose absolutely genuine objects, not coming from auctions o from the commerce in general.
            </p>
            </div>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>