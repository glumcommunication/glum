<!DOCTYPE html>
<html>
	<head>
		<title>Bootstrap 101 Template</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap -->
		<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="css/style.css" rel="stylesheet" media="screen">
	</head>
	<body>
	    <div class="wrap">
    	    <div class="header">
    	        <div class="container">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="span3" id="logo">
                                <img src="images/logo.png" alt="F@M Logo">
                            </div>
                            <div class="span9">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div id="social">
                                            <img src="images/Facebook.png" alt="Facebook" class="socialicons">
                                            <img src="images/Twitter.png" alt="Twitter" class="socialicons">
                                            <img src="images/vodafone.png" alt="Vodafone">
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span 12">
                                        <div class="navbar">
                                            <div class="navbar-inner">
                                                <div class="container">
 
      <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                                                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                    </a>    
                                                    <div class="nav-collapse collapse">
                                                
                                                        <ul class="nav">
                                                            <li><a href="#">HOME</a></li>
                                                            <li class="divider-vertical"></li>
                                                            <li><a href="#">AGENZIA</a></li>
                                                            <li class="divider-vertical"></li>
                                                            <li><a href="#">CONTATTI</a></li>
                                                            <li class="divider-vertical"></li>
                                                            <li class="active"><a href="#">OFFERTE</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                </div>
                          	        
<!--                <div class="page-header">
                    <h1>Example page header <small>Subtext for header</small></h1>
                </div>
-->
<!--                
                    <div class="navbar">
                        <div class="navbar-inner">
                            <a class="brand" href="#">VODAFONE SIENA</a>
                            <ul class="nav">
                                <li><a href="#">Home</a></li>
                                <li><a href="index.php">Index</a></li>
                                <li><a href="#">Link</a></li>
                            </ul>
                        </div>
                    </div>
-->
<!--   
                        <div class="hero-unit">
                            <h1>Heading</h1>
                                <p>Tagline</p>
                                <p>
                                    <a class="btn btn-primary btn-large">
                                    Learn more
                                    </a>
                            </p>
                        </div>
-->

                <div class="container">
                    <div id="myCarousel" class="carousel slide" data-interval=2000>
                        <ol class="carousel-indicators">
        					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        					<li data-target="#myCarousel" data-slide-to="1"></li>
        					<li data-target="#myCarousel" data-slide-to="2"></li>
        					<li data-target="#myCarousel" data-slide-to="3"></li>
        				</ol>
        				<!-- Carousel items -->
        				<div class="carousel-inner">
        					<div class="active item">
        						<img src="../examples/1.JPG" alt="1" class="">
        					</div>
        					<div class="item">
        						<img src="../examples/2.JPG" alt="2">
        					</div>
        					<div class="item">
        						<img src="../examples/3.JPG" alt="3">
        					</div>
        					<div class="item">
                                <img src="../examples/4.JPG" alt="4">
                            </div>
        				</div>
        				<!-- Carousel nav -->
        				<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
        				<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                    </div>
                <!--<div class="row-fluid">
                    <div class="span12 shadowed">
                        <div class="container ribbon">
                            <div class="ribbon_l"></div>
                            <div class="ribbon_m"><h2>Offerte</h2></div>
                            <div class="ribbon_r"></div>
                        </div>
                        <p>
                            Il Bayern Monaco ha umiliato il Barcellona sotto gli occhi dei tifosi dell’Allianz Arena. 
                            Un 4-0 difficile da prevedere alla vigilia perché ottenuto contro la squadra che negli 
                            ultimi anni ha segnato il limite superiore da provare a raggiungere, la squadra che può 
                            vantare il calciatore forse più forte di tutti i tempi, Lionel Messi, più una serie di campioni 
                            che in questi anni hanno vinto tutto con il club catalano e con la nazionale spagnola.
                        </p>
                        <p>
                            <a href="http://www.google.com" class="btn btn-large btn-danger">Leggi di più...</a>
                        </p>
                    </div>
                </div>-->
                <div class="row-fluid">
                    <div class="span6 shadowed">
                        <div class="container ribbon">
                            <div class="ribbon_l"></div>
                            <div class="ribbon_m"><h2>Secondo</h2></div>
                            <div class="ribbon_r"></div>
                        </div>
                        <p>
                            Il Bayern Monaco ha umiliato il Barcellona sotto gli occhi dei tifosi dell’Allianz Arena. 
                            Un 4-0 difficile da prevedere alla vigilia perché ottenuto contro la squadra che negli 
                            ultimi anni ha segnato il limite superiore da provare a raggiungere
                        </p>
                        <p>
                            <div class="btn btn-danger">Maggiori Dettagli</div>
                        </p>
                    </div>
                    <div class="span6 shadowed">
                        <div class="container ribbon">
                            <div class="ribbon_l"></div>
                            <div class="ribbon_m"><h2>Terzo</h2></div>
                            <div class="ribbon_r"></div>
                        </div>
                        <p>
                            Il Bayern Monaco ha umiliato il Barcellona sotto gli occhi dei tifosi dell’Allianz Arena. 
                            Un 4-0 difficile da prevedere alla vigilia perché ottenuto contro la squadra che negli 
                            ultimi anni ha segnato il limite superiore da provare a raggiungere
                        </p>
                        <p>
                            <div class="btn btn-danger">Download</div>
                        </p>
                    </div>
                </div>
            </div>
            <div class="push"></div>
        </div>
    <div class="footer">
      <div class="container">
        <p class="credit">Example courtesy <a href="http://martinbean.co.uk">Martin Bean</a> and <a href="http://ryanfait.com/sticky-footer/">Ryan Fait</a>.</p>
      </div>
    </div>
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script>
            $('.carousel').carousel({
                interval: 3000
            })
        </script>
	    </div>
	</body>
</html>
