<div>
    <div class="container" id="appartamenti">
        <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1>Appartamenti</h1>
        </div>
        <div class="row">
            <div id="myCarousel2" class="carousel slide col-xs-12">
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="item active">

                        <!-- ITEM-->
                        <div class="elemento">
                            <h3>Il Fienile</h3>

                            <a href="ilfienile.php"><img src="img/il-fienile_cover.jpg"></a>

                            <a  href="ilfienile.php" class="btn btn-default " id="scopri_btn">SCOPRI</a>
                        </div>

                        <!-- ITEM-->

                        <!-- ITEM-->
                        <div class="elemento">
                            <h3>Il Picchio</h3>

                            <a href="ilpicchio.php"><img src="img/il-picchio-cover.jpg"></a>

                            <a  href="ilpicchio.php" class="btn btn-default " id="scopri_btn">SCOPRI</a>
                        </div>
                    </div>
                    <!-- ITEM-->

                    <!-- ITEM-->

                    <!-- ITEM-->


                    <div class="item">
                        <div class="elemento">
                            <!-- ITEM-->

                            <h3>Le Querce</h3>

                            <a href="lequerce.php"><img src="img/le-querce-cover.jpg"></a>

                            <a  href="lequerce.php" class="btn btn-default " id="scopri_btn">SCOPRI</a>

                        </div>
                        <div class="elemento">
                            <!-- ITEM-->

                            <!-- ITEM-->

                            <h3>Gli Olivi</h3>

                            <a href="gliolivi.php"><img src="img/gli-olivi-cover.jpg"></a>

                            <a  href="gliolivi.php" class="btn btn-default " id="scopri_btn">SCOPRI</a>


                            <!-- ITEM-->

                            <!-- ITEM-->

                        </div>
                    </div>
                    <div class="item ">
                        <!--  ITEM-->
                        <div class="elemento">
                            <h3>Il Granaio</h3>

                            <a href="ilgranaio.php"><img src="img/il-granaio-cover.jpg"></a>

                            <a  href="ilgranaio.php" class="btn btn-default " id="scopri_btn">SCOPRI</a>


                            <!-- ITEM-->

                            <!-- ITEM-->
                        </div>
                        <div class="elemento">
                            <h3>Le Ghiande</h3>

                            <a href="leghiande.php"><img src="img/le-ghiande-cover.jpg"></a>

                            <a  href="leghiande.php" class="btn btn-default " id="scopri_btn">SCOPRI</a>


                            <!-- ITEM-->

                            <!-- ITEM-->

                            <!-- ITEM-->
                        </div>
                    </div>

                </div><!-- /INNER-->  
                <!-- Carousel nav -->
                <a class="left carousel-control" href="#myCarousel2" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#myCarousel2" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>
    </div><!-- /CONTAINER -->

</div>