<?php
	include('config2.inc.php');

		if($_GET['cat']=="aziendali")
			$_page_title = "Vetture aziendali";		
		else if($_GET['cat']=="kmzero")
			$_page_title = "Km zero";	
		else if($_GET['cat']=="promozioni")
			$_page_title = "Vetture in promozione";
		else if($_GET['cat']=="usato")
			$_page_title = "Usato selezionato";

	function getPageURL()
	{
		$pageURL = 'http';
		if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
		$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		return $pageURL;
	}
		
	if($_GET['id']!='') 
	{
		$_query = "SELECT v.*,c.nome FROM vetture AS v INNER JOIN caseauto AS c ON v.marca=c.id WHERE v.attivo = 1 AND v.id=".$_GET['id']." LIMIT 1";
		$_dati = mysql_query($_query) or exit (mysql_error());
		$array = mysql_fetch_assoc($_dati);
		
		$_PERMALINK = '/'.$_GET['cat'].toUrl(strtoupper($array['nome']).' '.strtoupper($array['modello']).' '.strtoupper($array['versione']),$array['id']);
		
		$_TITLE	= $array['nome'].' '.$array['modello'].' '.$array['versione'].' | '.$_page_title.' - Sienamotori.it';	
		
		if($_SERVER["REQUEST_URI"] != $_PERMALINK){
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: '.$_PERMALINK);
			exit();
		}
		
		$immagine .= '<img src="/phpThumb/phpThumb.php?far=c&wp=674&h=480&src=/img_vetture/'.$array['img1'].'" alt="imgMain" id="imgMain" style="border:1px solid #ccc;">';
		
		$immagine2 .= '';
		
		for($i=1; $i<13; $i++)
		{
			if($array['img'.$i] != '')
			{
				$immagine2 .=	'<div style="margin:10px 10px 0px 0px;float:left;">
									<a href="#" onclick="$(\'#imgMain\').attr(\'src\',\'/phpThumb/phpThumb.php?far=c&wp=674&h=480&src=/img_vetture/'.$array['img'.$i].'\')"><img src="/phpThumb/phpThumb.php?far=c&w=125&src=/img_vetture/'.$array['img'.$i].'" alt="" /></a>
								</div>';
			}
		}

		$_TEXT_TWITTER = strtoupper($array['nome']).' '.strtoupper($array['modello']).' '.strtoupper($array['versione']). ' '.$array['anno'].' - A SOLI '.$array['prz'].' €';
		$_MODEL_FB = strtoupper($array['nome']).' '.strtoupper($array['modello']).' '.strtoupper($array['versione']);
		$_DESC_FB = $array['mese'].'/'.$array['anno'].' - '.$array['colore'].' - '.$array['km'].' Km. - '.$array['alim'].' - Cambio '.$array['cambio'].' - '.$array['prz'].' €';

		$_CONTENT =	'<div class="row-1">
						<h3><strong class="fright">&euro; '.$array['prz'].'</strong>'.strtoupper($array['nome']).' '.strtoupper($array['modello']).' '.strtoupper($array['versione']).'</h3>
						<div class="padding">
							<div class="wrapper indent-bot2">
								<div class="row">'.$immagine.'</div>
								<div class="col-1">
									<div>'.$immagine2.'</div>									
								</div>
								<div class="col-2"><br />
									<table class="table-1 indent-bot">
										<tr>
											<td style="width:105px;">Marca:</td>
											<td><strong>'.strtoupper($array['nome']).'</strong></td>
										</tr>
										<tr>
											<td>Modello:</td>
											<td><strong> '.strtoupper($array['modello']).'</strong></td>
										</tr>
										<tr>
											<td>Cambio:</td>
											<td><strong> '.$array['cambio'].'</strong></td>
										</tr>
										<tr>
											<td>Alimentazione:</td>
											<td><strong> '.$array['alim'].'</strong></td>
										</tr>
										<tr>
											<td>Carrozzeria:</td>
											<td><strong> '.$array['carro'].'</strong></td>
										</tr>
										<tr>
											<td>Prezzo:</td>
											<td><strong>&euro; '.$array['prz'].'</strong></td>
										</tr>
									</table>                        
									<table class="table-1 indent-bot">
										<tr>
											<td style="width:105px;"><strong><strong>Dettagli auto:</strong></strong><br />&nbsp;</td>
											<td></td>
										</tr>
										<tr>
											<td>Stato vettura:</td>
											<td><strong>'.$array['stato'].'</strong></td>
										</tr>
										<tr>
											<td>Anno:</td>
											<td><strong>'.$array['mese'].'/'.$array['anno'].'</strong></td>
										</tr>
										<tr>
											<td>Colore:</td>
											<td><strong>'.$array['colore'].'</strong></td>
										</tr>
										<tr>
											<td>In esposizione:</td>
											<td><strong>'.$array['stato_car'].'</strong></td>
										</tr>  
										<tr>
											<td>Km:</td>
											<td><strong>'.$array['km'].'</strong></td>
										</tr>
									</table>
								</div>
									<p class="text-1 p1"><strong>Optionals presenti:</strong></p>
									<div class="wrapper p2">'.str_replace('|',', ',$array['optional']).'</div>
									<p class="text-1 p1"><strong>Cosa dice Sienamotori:</strong></p>
									<div class="wrapper p2">'.$array['altri_optional'].'</div>
									<p class="text-1 p1">Scarica la nostra <a href="/garanzia_conformita.pdf" target="_blank">garanzia di conformità</a> in .pdf<br /><br /></p>

								<a name="fb_share" type="button">Condividi</a>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<a href="https://twitter.com/share" class="twitter-share-button" data-text="'.$_TEXT_TWITTER.'" data-via="SienaMotori" data-lang="it">Tweet</a>
							</div>
						</div>
					</div>';

		if($_POST['act'] == 'send')
		{
			if($_POST['testo']!='' && $_POST['email']!='' && $_POST['tel']!='' && $_POST['nome']!='')
			{
				if(!eregi("^[a-z0-9][_\.a-z0-9-]+@([a-z0-9][0-9a-z-]+\.)+([a-z]{2,4})",$_POST['email']))
				{
					$_report = "L'indirizzo e-mail non risulta valido!";
				}
				else
				{
					require("phpmailer/class.phpmailer.php");
									
					if($_GET['cat']=="aziendali")
						$_info = "VETTURA AZIENDALE";		
					else if($_GET['cat']=="kmzero")
						$_info = "VETTURA KM ZERO";	
					else if($_GET['cat']=="promozioni")
						$_info = "VETTURA IN PROMOZIONE";	
					else if($_GET['cat']=="usato")
						$_info = "VETTURA USATA";	
					
					$_mail_testo = '<html>
									<body style="font-family:Verdana; font-size:12px; margn:20px;">
										<p style="font-size:16px;"><strong>Richiesta informazioni - '.$_info.'</strong></p>
										<br />
										<p style="font-size:12px;">
											<b>Vettura: </b> <a href="http://www.sienamotori.it/vetture.php?cat='.$_GET['cat'].'&id='.$_GET['id'].'">'.$_POST['info_su'].'</a><br /><br /><br />
											<b>Nome: </b>'.$_POST['nome'].'<br /><br />
											<b>Telefono: </b>'.$_POST['tel'].'<br /><br />
											<b>E-mail: </b>'.$_POST['email'].'<br /><br /><br />
											<b>Testo: </b>'.nl2br($_POST['testo']).'
										</p>
									</body>
								</html>';
					
					$mail = new PHPMailer();

					$mail->CharSet = 'UTF-8';
					
					$mail->From     = $_POST['email'];
					$mail->FromName = $_POST['nome'];
					$mail->Mailer   = "mail";
			
					$mail->Body    = $_mail_testo;
					$mail->isHTML(true);
					$mail->Subject = "SienaMotori.it - Richiesta informazioni - ".$_info;
					$mail->AddAddress("info@sienamotori.it");
					/*$mail->AddAddress("riccardo@advinser.it");*/
					$mail->Send();
					$mail->ClearAddresses();

					$_report = "Richiesta inviata correttamente!";
					$_POST = "";
				}
			}
			else
			{
				$_report = "Prima di inviare la richiesta, completa TUTTI i campi obbligatori.";
			}
		}
		
		$_report = ($_report!='') ? '<tr><td align="center"><p><font color="#ff0000"><b>'.$_report.'</b></font></p></td></tr>' : '';

		$_COL_1 =	'<h3>Richiedi Informazioni</h3>
					<div class="padding">
						<div class="border-bot indent-bot">
							<p style="padding:5px;line-height:175%;">Richiedi maggiori informazioni su <strong>'.strtoupper($array['nome'].' '.$array['modello'].' '.$array['versione']).'</strong> compilando il modulo. Verrai contattato al più presto da un nostro incaricato che ti fornir&agrave; tutte le informazioni di cui hai bisogno.</p>
							<table cellpadding="0" cellspacing="2" width="100%" class="little">
								<form method="POST" action="">
									<input type="hidden" name="act" value="send">
									<input type="hidden" name="info_su" value="'.strtoupper($array['nome'].' '.$array['modello'].' '.$array['versione']).'">
									'.$_report.'
									<tr>
										<td>
											<p>Nome: <span class="red">*</span><br /><input type="text" name="nome" value="'.$_POST['nome'].'" class="input"></p>
										</td>
									</tr>
									<tr>
										<td>
											<p>Telefono: <span class="red">*</span><br /><input type="text" name="tel" value="'.$_POST['tel'].'" class="input"></p>
										</td>
									</tr>
									<tr>
										<td>
											<p>E-mail: <span class="red">*</span><br /><input type="text" name="email" value="'.$_POST['email'].'" class="input"></p>
										</td>
									</tr>
									<tr>
										<td valign="top">
											<p>Testo richiesta: <span class="red">*</span><br /><textarea name="testo" rows="5" class="textarea">'.$_POST['testo'].'</textarea></p>
										</td>
									</tr>
									<tr>
										<td>
											<p><span class="red">*</span> Campo Obbligatorio</p>
										</td>
									</tr>
									<tr>
										<td>
											<p class="link_2">Trattamento dei dati personali nel rispetto del Dlgs n. 196/2003 in materia <a href="/privacy.php">protezione dei dati personali</a>.<br /><br /></p>
										</td>
									</tr>
									<tr>
										<td>
											<p align="center"><input type="submit" value="Invia" class="button"></p>
										</td>
									</tr>
								</form>
							</table>
						</div>
					</div>';


		include(TEMPLATE_PATH.'template3.inc.php');
	}
	else
	{
		$_PERMALINK = '/'.$_GET['cat'].'/';
		
		$_TITLE	= $_page_title.' Mitsubishi, Peugeot e multimarca - Sienamotori.it';	
		$_DESCRIPTION = $_page_title.' delle migliori marche a prezzi incredibili. Scopri le nostre offerte sul portale Sienamotori.it';
		/*
		if($_SERVER["REQUEST_URI"] != $_PERMALINK){
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: '.$_PERMALINK);
			exit();
		}
		*/

		$_query = "SELECT v.*,c.nome FROM vetture AS v INNER JOIN caseauto AS c ON v.marca=c.id WHERE v.attivo = 1 AND v.".$_GET['cat']."='Si' ORDER BY CAST(LPAD(REPLACE(v.prz,'.',''),10,'0') AS DECIMAL(10,2)) DESC";	
		$dati = mysql_query($_query) or exit (mysql_error());
		while($array=mysql_fetch_array($dati))
		{
			$this_url = '/'.$_GET['cat'].toUrl(strtoupper($array['nome']).' '.strtoupper($array['modello']).' '.strtoupper($array['versione']),$array['id']);
			$lista .=	'<div style="margin:10px;background:#ccc;overflow:hidden;">
							<figure class="img-indent">
								<a href="'.$this_url.'">
									<img src="/phpThumb/phpThumb.php?far=c&wp=250&h=220&src=/img_vetture/'.$array['img1'].'" alt="" style="border:1px solid #ccc;">
								</a>
							</figure>
							<div class="extra-box" style="height:220px;position:relative;">
								<div class="padding-top" style="height:200px;">
									<h4>
										<a href="'.$this_url.'">'.strtoupper($array['nome']).' '.strtoupper($array['modello']).' '.strtoupper($array['versione']).'</a>
										<span style="color:#000;float:right;padding-right:30px;">&euro; '.$array['prz'].'</span>
									</h4>
									
									<table style="color:#000;">
										<col width="90" />
										<col width="90" />
										<col width="100" />
										<col width="160" />
										<col width="140" />
										<tr>
											<td><strong>ANNO </strong></td>
											<td><strong>KM </strong></td>
											<td><strong>ALIM. </strong></td>
											<td><strong>COLORE </strong></td>
											<td><strong>IN ESPOSIZIONE </strong></td>
										</tr>
										<tr>
											<td>'.$array['anno'].'</td>
											<td>'.$array['km'].'</td>
											<td>'.$array['alim'].'</td>
											<td>'.$array['colore'].'</td>
											<td>'.$array['stato_car'].'</td>
										</tr>
									</table>
									<p><br /><strong>ALTRI OPTIONALS: </strong>'.str_replace('|',', ',$array['optional']).'</p>
									<a class="button1" href="?cat='.$_GET['cat'].'&id='.$array['id'].'"><strong>SCHEDA VETTURA</strong></a>
								</div>
							</div>
						</div>';
		}
		
		$_CONTENT =	'<section id="content">
						<div class="row-2">
							<h3>'.$_page_title.'</h3>
								<div class="wrapper">
									'.$lista.'
								</div>
						</div>
					</section>';

		include(TEMPLATE_PATH.'template.inc.php');
	}
?>
