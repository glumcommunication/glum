<?php
	$____PERMESSI = "1";

	$_rwmdac = Array(	"1", # Permesso Lettura:  0->No / 1->Si
					"1", # Permesso Scrittura:  0->No / 1->Si
					"1", # Permesso Modifica:  0->No / 1->Si
					"1", # Permesso Eliminazione:  0->No / 1->Si
					"1", # Permesso Attivazione:  0->No / 1->Si
					"1"	# Permesso Creazione Tabella:  0->No / 1->Si
					);

	$long_name = "Vetture";
	$tabella = "vetture";

	if($____CHECK_INCLUDE == 1)
	{

		$__CAMPI[] = array("id","key","key","ID Rif.");
		$__CAMPI[] = array("count","view","text","Contatore");
		$__CAMPI[] = array("marca","select","text","Marca");
		$__CAMPI[] = array("modello","text","text","Modello");
		$__CAMPI[] = array("versione","text","text","Versione");
		$__CAMPI[] = array("prz","text","text","Prezzo");
		$__CAMPI[] = array("condizioni","check","text","Condizioni");
		$__CAMPI[] = array("stato","text","text","Stato Vettura");
		$__CAMPI[] = array("carro","select","text","Carrozzeria");
		$__CAMPI[] = array("cambio","select","text","Cambio");
		$__CAMPI[] = array("anno","select","text","Anno");
		$__CAMPI[] = array("mese","select","text","Mese");
		$__CAMPI[] = array("colore","select","text","Colore");
		$__CAMPI[] = array("col_des","select","text","Descrizione Vernice");
		$__CAMPI[] = array("usato","check","text","Vettura Usata");
		$__CAMPI[] = array("promozioni","check","text","Vettura in Promozione");
		$__CAMPI[] = array("kmzero","check","text","Vettura Km0");
		$__CAMPI[] = array("aziendali","check","text","Vettura Aziendale");
		$__CAMPI[] = array("in_home","check","text","Visualizza in Home");
		$__CAMPI[] = array("stato_car","text","text","In Esposizione");
		$__CAMPI[] = array("alim","select","text","Alimentazione");
		$__CAMPI[] = array("km","text","text","Km");
		$__CAMPI[] = array("garanzia","check","text","Garanzia");
		$__CAMPI[] = array("optional","check","long","Optional");
		$__CAMPI[] = array("altri_optional","long","long","Altri Optional");
		$__CAMPI[] = array("img1","img","text","Immagine 1");
		$__CAMPI[] = array("img2","img","text","Immagine 2");
		$__CAMPI[] = array("img3","img","text","Immagine 3");
		$__CAMPI[] = array("img4","img","text","Immagine 4");
		$__CAMPI[] = array("img5","img","text","Immagine 5");
		$__CAMPI[] = array("img6","img","text","Immagine 6");
		$__CAMPI[] = array("img7","img","text","Immagine 7");
		$__CAMPI[] = array("img8","img","text","Immagine 8");
		$__CAMPI[] = array("img9","img","text","Immagine 9");
		$__CAMPI[] = array("img10","img","text","Immagine 10");
		$__CAMPI[] = array("img11","img","text","Immagine 11");
		$__CAMPI[] = array("img12","img","text","Immagine 12");
		$__CAMPI[] = array("attivo","select","text","Attivo");
		$__CAMPI[] = array("date_add","data","data","Data ins.");
		
		for($b=0;$b<count($__CAMPI);$b++)
		{
			$campo[$b]				= $__CAMPI[$b][0];
			$tipo_campo[$b]			= $__CAMPI[$b][1];
			$tipo_tab_campo[$b]		= $__CAMPI[$b][2];
			$nome_campo[$b]			= $__CAMPI[$b][3];
			$fnc_campo[$b]			= $__CAMPI[$b][4];
		}

		$campo_select['attivo'] = Array("1","0");
		$campo_select_name['attivo'] = Array("SI","NO");
		
		$_query_marche = "SELECT id, nome FROM caseauto ORDER BY nome ASC";
		$_data_marche = mysql_query($_query_marche);
		while($_row_marche = mysql_fetch_assoc($_data_marche ))
		{
			$campo_select['marca'][] = $_row_marche['id'];
			$campo_select_name['marca'][] = $_row_marche['nome'];
		}

		$campo_select['anno'] = Array("","da immatricolare");
		for($i=1980; $i<=date('Y'); $i++)
		{
			$campo_select['anno'][] = $i;
		}

		$campo_select['mese'] = Array("","01","02","03","04","05","06","07","08","09","10","11","12");

		$campo_select['cambio'] = Array("","Manuale 5 marce","Manuale 6 marce","Automatico","Robotizzato","CVT");

		$campo_check['condizioni'] = Array("Iva esposta","non trattabili","Iva deducibile","trattabile","fatturabile");

		$campo_check['usato'] = Array("Si");
		
		$campo_check['promozioni'] = Array("Si");
		
		$campo_check['kmzero'] = Array("Si");
		
		$campo_check['aziendali'] = Array("Si");

		$campo_check['in_home'] = Array("Si");

		$campo_check['garanzia'] = Array("Si","No");

		$campo_select['alim'] = Array("","Benzina","Diesel","Gpl","Metano","Elettrica","Benzina Gpl","Benzina Metano","Gpl Metano", "Ibrido - Elettrico/Diesel", "Ibrido - Elettrico/Benzina");

		$campo_select['col_des'] = Array("","metallizzato","pastello","perlato","bicolore");

		$campo_select['stato'] = Array("","Usata","Semestrale","Km Zero","Nuova");

		$campo_select['carro'] = Array("","cabrio","city car","coupe","berlina","fuoristrada","suv","monovolume","station wagon","ricreazionali","veicolo ricreativo","veicolo commerciale","furgone","autocarro","wagon 4 porte","pick up");

		$campo_select['colore'] = Array("","Bianco","Grigio Chiaro/Argento","Grigio Scuro/Grigio","Nero","Avorio","Arancio/Oro, Giallo","Marrone","Rosso","Bordeaux","Verde","Verdone/Verde Scuro","Azzurro/Celeste","Blu","Blu Scuro","Lilla, Rosa","Viola","Blu/Viola/Melanzana");

		//$campo_select['interni'] = Array("","interno 1","interno 2","interno 3","interno 4");

		$campo_check['optional'] = Array("4 airbag","6 airbag","8 airbag","ABS","airbag","airbag laterali","antifurto","antifurto satellitare","aria condizionata","ASP","assetto sportivo","autoradio","Blue Tooth","bull bar","capote elettrica","caricatore CD","CD frontale","cerchi in lega","chiusura centralizzata","climatizzatore automatico","climatizzatore manuale","computer di bordo","doppio airbag","ESP","fari Xeno","fendinebbia","frangivento","full optional","gancio traino","GPS/navigatore satellitare","GSM","hard top","Immobilizzatore","impianto GPL","impianto Hi-Fi","impianto metano","Interni in radica","Keyless System","kit viva voce","lavafari","marmitta catalitica","parabrezza termico","pedane laterali","per portatori Handicap","porta pacchi","sedile posteriore sdoppiato ","sedili elettrici","sedili elettrici con memoria","sedili riscaldati","sedili sportivi ","servosterzo ","sistema di navigazione ","soft top ","Specchietti retrovisori elettrici ","Specchietti retrovisori elettrici/riscaldati ","telepass ","tergilunotto ","tetto apribile ","tetto apribile/ elettrico ","tetto in vetro ","vetri elettrici ","vetri elettrici posteriori ","volante con acceleratore ","volante in pelle ","volante multifunzionale");
		
		$campo_attivo = "attivo";

		$campo_dist = Array("img1","marca","modello","anno","count");

		$_filters_default = array("id","marca","modello","anno");

		$rec_pagina = "20";
		$pagmax = "20";
		$order_senso = "DESC"; #DESC discendente ASC ascendente
		$order_var = "id"; #Campo per cui ordinare
		$cartella = "/img_vetture/"; #di default � root_princip / nome_tabella / img / , puoi cambiare da root_principale (slash da mettere)
	}
?>
