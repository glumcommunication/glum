<?php
	include('config2.inc.php');
	
	$_CONTENT = '<div class="row-1">
					<h3>Mobilit&agrave; Disabili</h3>
					<div class="padding">
	                 	<div class="border-bot indent-bot">
							<img src="/phpThumb/phpThumb.php?zc=1&wp=680&h=500&src=/servizi/partner_disabili_3quarti_antjpg" alt="" />
							<br /><br /> 
							PEUGEOT OFFRE MOLTE SOLUZIONI PER LA MOBILIT&Agrave; DEI DISABILI. IN PARTICOLARE IL MODELLO PARTNER ED EXPERT TRASFORMATO PER IL TRASPORTO DEL DISABILE CON CARROZZINA.
						</div>
					</div>
				</div>';
	if($_POST[act] == 'send')
	{
		if($_POST['testo']!='' && $_POST['email']!='' && $_POST['tel']!='' && $_POST['nome']!='')
		{
			if(!eregi("^[a-z0-9][_\.a-z0-9-]+@([a-z0-9][0-9a-z-]+\.)+([a-z]{2,4})",$_POST[email]))
			{
				$_report = "L'indirizzo e-mail non risulta valido!";
			}
			else
			{
				require("phpmailer/class.phpmailer.php");

				$_mail_testo = '<html>
									<body style="font-family:Verdana; font-size:12px; margn:20px;">
										<p style="font-size:16px;"><strong>Richiesta informazioni - Servizi disabili</strong></p>
										<br />
										<p style="font-size:12px;">
											<b>Informazioni: </b> <a href="http://www.sienamotori.it/info.php">Servizi disabili</a><br /><br /><br />
											<b>Nome: </b>'.$_POST['nome'].'<br /><br />
											<b>Telefono: </b>'.$_POST['tel'].'<br /><br />
											<b>E-mail: </b>'.$_POST['email'].'<br /><br /><br />
											<b>Testo: </b>'.nl2br($_POST['testo']).'
										</p>
									</body>
								</html>';

				$mail = new PHPMailer();

				$mail->CharSet = 'UTF-8';
					
				$mail->From     = $_POST['email'];
				$mail->FromName = $_POST['nome'];
				$mail->Mailer   = "mail";
		
				$mail->Body    = $_mail_testo;
				$mail->isHTML(true);
				$mail->Subject = "SienaMotori.it - Richiesta informazioni - Servizi disabili";
				$mail->AddAddress("info@sienamotori.it");
				$mail->Send();
				$mail->ClearAddresses();

				$_report = "Richiesta inviata correttamente!";
				$_POST = "";
			}
		}
		else
		{
			$_report = "Prima di inviare la richiesta, completa TUTTI i campi obbligatori.";
		}
	}
	
	$_report = ($_report!='') ? '<tr><td align="center"><p><font color="#ff0000"><b>'.$_report.'</b></font></p></td></tr>' : '';

	$_COL_1 =	'<h3>Richiedi Informazioni</h3>
				<div class="padding">
					<div class="border-bot indent-bot">
						<p style="padding:5px;line-height:175%;">Richiedi maggiori informazioni su <strong>Servizi disabili</strong> compilando il modulo. Verrai contattato al più presto da un nostro incaricato che ti fornir&agrave; tutte le informazioni di cui hai bisogno.</p>
						<table cellpadding="0" cellspacing="2" width="100%" class="little">
							<form method="POST" action="">
								<input type="hidden" name="act" value="send">
								'.$_report.'
								<tr>
									<td>
										<p>Nome: <span class="red">*</span><br /><input type="text" name="nome" value="'.$_POST[nome].'" class="input"></p>
									</td>
								</tr>
								<tr>
									<td>
										<p>Telefono: <span class="red">*</span><br /><input type="text" name="tel" value="'.$_POST[tel].'" class="input"></p>
									</td>
								</tr>
								<tr>
									<td>
										<p>E-mail: <span class="red">*</span><br /><input type="text" name="email" value="'.$_POST[email].'" class="input"></p>
									</td>
								</tr>
								<tr>
									<td valign="top">
										<p>Testo richiesta: <span class="red">*</span><br /><textarea name="testo" rows="5" class="textarea">'.$_POST[testo].'</textarea></p>
									</td>
								</tr>
								<tr>
									<td>
										<p><span class="red">*</span> Campo Obbligatorio</p>
									</td>
								</tr>
								<tr>
									<td>
										<p>Trattamento dei dati personali forniti nel rispetto della legge n. 675/96.<br /><br /></p>
									</td>
								</tr>
								<tr>
									<td>
										<p align="center"><input type="submit" value="Invia" class="button"></p>
									</td>
								</tr>
							</form>
						</table>
					</div>
				</div>';


	include(TEMPLATE_PATH.'template5.inc.php');
?>
