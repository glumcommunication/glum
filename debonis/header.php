<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" >
<meta charset="utf-8" />
<meta name="description" content="">
<meta name="keywords" content="ginecologo, pietragalla, potenza, ecografia 3d, ecografia 4d, translucenza, test combinato, visita ginecologica, visita ostetrica, maria de bonis, doppler, materno fetale, flussimetria, ecografia morfologica">
<!--<meta name="google-site-verification" content="s79WmfWERu9QLsscQq0mdjc6uQnlHxXNANpvnGuIQ9k" />-->
<link rel="icon" href="images/favicon.png" type="image/svg"/> 
<link href="css/stylemod.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Great+Vibes|Quattrocento+Sans:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39132694-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<title>Studio Medico De Bonis | <?php echo $pagetitle; ?></title>
</head>
<body>
<div id="wrapper">
	<div id="header">
		<a href="http://www.studiomedicodebonis.it/" title="Studio Medico De Bonis"><div id="logo">
			<h2>STUDIO MEDICO</h2>
			<h3>Medico Chirurgo</h3>
			<h1>Dott.sa Maria De Bonis</h1>
			<h3>Specialista in Ginecologia e Ostetricia</h3>
		</div></a>
<?php
if($pagename == "index"){
echo <<<_END

_END;
}
elseif($pagename == "prestazioni") {
echo <<<_END

_END;
}
elseif($pagename == "contatti") {
echo <<<_END

_END;
}
?>

	</div>
	<div id="container">
<!--		<div id="sidebar">
			<ul>
				<li><a href="index.php" name="Chi sono">Chi sono</a></li>
				<li><a href="prestazioni.php" name="Prestazioni">Prestazioni</a></li>
				<li><a href="contatti.php" name="Contatti">Contatti</a></li>			
			</ul>
		</div>-->