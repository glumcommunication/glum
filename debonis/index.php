<?php
$pagename = "index";
$pagetitle = "Chi sono";
include_once 'header.php';
?>
		<div id="ptitle">
			<h1>Chi sono</h1>
		</div>
<?php
include_once 'sidebar.php';
?>
		<div id="main">
			<img src="images/profilepic2.jpg" alt="Studio Medico De Bonis" class="homepic" />
			<p>La Dott.ssa Maria De Bonis (Potenza, 10 gennaio 1981) cresce a Pietragalla (PZ) e studia al Liceo Ginnasio Q.O.Flacco di Potenza.</p>				
			<p>Prosegue gli studi laureandosi nel 2006 con il massimo dei voti con lode in Medicina e Chirurgia presso l'Università degli Studi 
			di Siena, discutendo una tesi dal titolo "Il travaglio nel parto a termine e pretermine".</p>
			<p>Rivolge particolare attenzione allo studio della sfera femminile specializzandosi nel 2012, con il massimo dei voti, in Ginecologia 
			e Ostetricia presso l'Università degli Studi di Siena. </p>
			<p>Attualmente iscritta al secondo anno di Dottorato in Medicina Molecolare, è autrice e co-autrice di numerosi articoli scientifici 
			pubblicati su riviste specializzate, sia nazionali che internazionali.</p>
			<p>Cresce professionalmente con il Chiar.mo Professor Felice Petraglia, ginecologo, luminare riconosciuto a livello mondiale e 
			Direttore della Scuola di Specializzazione in Ginecologia e Ostetricia di Siena.</p> 
			<p>Nel corso della sua specializzazione dedica particolare attenzione alla pratica ecografica arricchendo la sua formazione presso 
			il Dipartimento Materno-Infantile della Clinica Universitaria Careggi di Firenze, acquisendo una elevata preparazione nella Diagnosi 
			Prenatale e nell'ecografia 3D e 4D.</p>
		</div>	

<?php
include_once 'footer.php';
?>