<div class="bottomcontent" style="width:690px;margin-left:260px;">
	<p style="padding10px 25px;border-bottom:3px solid #8F89BD;margin:25px 0"></p>
	<div class="infobox">
		<h1>Informazione</h1>
		<p>Avere a disposizione la consulenza e la professionalità di un medico che 
		conosce la fisiologia e le problematiche della donna in tutte le fasi della sua vita, 
		a partire dall’adolescenza. 
		</p>
	</div>
	<div class="infobox">
		<h1>Prevenzione</h1>
		<p>Il controllo annuale, affiancando i controlli previsti a livello nazionale, 
		rappresenta un prezioso strumento per la prevenzione e la diagnosi precoce 
		delle patologie ginecologiche.
		</p>
	</div>
	<div class="infobox" style="margin-right:0px;">
		<h1>Gravidanza</h1>
		<p>Ricevere tutte le informazioni necessarie sulla gravidanza fin dall’epoca preconcezionale 
		e controllare mensilmente il benessere della futura mamma e del suo bambino.
		</p>
	</div>
</div>

