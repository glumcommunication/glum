<?php
$pagename = "contatti";
$pagetitle = "Contatti";
include_once 'header.php';
?>
		<div id="ptitle">
			<h1>Contatti</h1>
		</div>
		
<?php
include_once 'sidebar.php';
?>
		<div id="main">
			<p style="text-align: center;margin: 25px auto;padding: 0;line-height: 25px;"><strong>E-mail</strong>: <a href="mailto:info@studiomedicodebonis.it">info@studiomedicodebonis.it</a><br>
			<strong>Sito web</strong>: <a href="http://www.studiomedicodebonis.it" title="Studio Medico De Bonis">www.studiomedicodebonis.it</a><br>
			<strong>Cellulare</strong>: +39 334 1932283<br>
			<strong>Indirizzo</strong>: Via Giardino 8/A - Pietragalla (PZ)<br>
		</p>			
			<h1>Come raggiungere lo studio</h1>
			<div id="mapviewer" style="box-shadow: 2px 2px 10px #8F89BD;width: 500px;height: 400px;margin: 0 auto;"><iframe id="map" Name="mapFrame" scrolling="no" width="500" height="400" frameborder="0" src="http://it.bing.com/maps/embed/?lvl=16&amp;cp=40.748559~15.883431&amp;sty=r&amp;draggable=true&amp;v=2&amp;dir=0&amp;where1=Via+Giardino%2C+85016+Pietragalla+PZ&amp;form=LMLTEW&amp;pp=40.748559~15.883431&amp;mkt=it-it&amp;gen=false&amp;emid=dd23d7e3-3402-a8d1-237a-8e51b80e82b1&amp;w=500&amp;h=400"></iframe><div id="LME_maplinks" style="line-height:20px;"></div>
</div>

		</div>	

<?php
include_once 'footer.php';
?>