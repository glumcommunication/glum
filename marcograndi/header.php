<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="description" content="<?php echo $pagedesc; ?>">
<meta name="keywords" content=" Geometra Marco Grandi, studio tecnico Siena, albo professionale geometri, certificati energetici, valutazioni immobiliari, progettazione, coordinamento, sicurezza, consulenze, pratiche catastali, formazione professionale">
<link rel="icon" href="images/favicon.png" type="image/svg"/>
<link href="css/style.css" rel="stylesheet" />
<link href='http://fonts.googleapis.com/css?family=Quattrocento+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<link href="css/flexslider.css" rel="stylesheet" type="text/css" />
<script src="js/jquery.flexslider-min.js"></script>
  <script type="text/javascript">
    $(window).load(function(){
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: true,
        slideshow: false,
        itemWidth: 160,
        itemMargin: 10,
        asNavFor: '#slider'
      });
      
      $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: true,
        slideshow: false,
        sync: "#carousel",
        start: function(slider){
        $('body').removeClass('loading');
        }
      });
    });
  </script>
<title>Studio Tecnico Professionale - Geometra Marco Grandi | <?php echo $pagetitle; ?></title>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39297484-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body <?php if ($pagename == "contatti") {echo ' onload="initialize()"';} ?>>
    <div id="wrapper">
        <div id="header">
            <div class="hleft">
                <a href="index.php" title="Home"><h1>STUDIO TECNICO PROFESSIONALE</h1></a>
                <a href="index.php" title="Home"><h2><span style="font-size:0.75em;">Geometra</span> Marco Grandi</h2></a>
            </div>
            <div class="hmiddle">
                <!--<img src="images/facebook.png" alt="Facebook">
                <img src="images/twitter.png" alt="Twitter">-->
                <a href="mailto:marcograndi80@libero.it"><img src="images/mail.png" alt="Email"></a>
            </div>
            <div class="hright">
                <ul id="topmenu">
				    <li><a href="profilo.php" title="Chi sono">Profilo</a></li>
                    <li><a href="progetti.php" title="I miei lavori">Progetti</a></li>
                    <li><a href="contatti.php" title="Contatti">Contatti</a></li>
                </ul>
            </div>
        </div>