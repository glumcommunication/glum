<?php
$pagename = 'contatti';
$pagetitle = 'Contatti';
$pagedesc = 'Il geometra Marco Grandi a Siena a Siena, in strada Massetana Romana 52/A, facilmente raggiungibile e vicina alle uscite Siena Ovest e Siena Sud della tangenziale di Siena.';
include_once 'header.php';

if ($_POST["doSend"] == "Invia") {
    

    $name = $_POST ["name"];
    $phone = $_POST["phone"];
    $email = $_POST ["mail"];
    $msg = $_POST["message"];

    $headers = "";
    $headers .= "From: $email\n";
    $headers .= "Reply-To: $email\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=utf-8\n";
    $to = "marcograndi80@libero.it";
    $subject = "NEW - Richiesta informazioni";
    /*$message = $messaggio;*/
    $message = "Hai ricevuto una nuova richiesta di informazioni, ecco i dettagli:<br />
    Nome e Cognome: $name<br />
    E-mail: $email<br />
    Telefono: $phone<br />
    Messaggio: $msg<br />";

    mail ($to,$subject,$message,$headers);
    $mailstatus = "1";
    $messassagestatus = "La vostra richiesta è stata inviata. Sarete ricontattati al più presto.";
}
?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    function initialize() {
        var latlng = new google.maps.LatLng(43.30407, 11.321679);
        var settings = {
            zoom: 15,
            center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
            navigationControl: true,
            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
            mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"), settings);

    var companyLogo = new google.maps.MarkerImage('images/positionshadow.png',
    new google.maps.Size(39,32),
    new google.maps.Point(0,0)
);

var companyPos = new google.maps.LatLng(43.30407, 11.321679);
var companyMarker = new google.maps.Marker({
    position: companyPos,
    map: map,
    icon: companyLogo,
    //shadow: companyShadow,
    title:"Geometra Marco Grandi"
});

var contentString = '<div id="content">'+
                    '<div id="siteNotice">'+
                    '</div>'+
                    '<h1 id="firstHeading" class="firstHeading" style="font-size:14px;font-weight:bold;border-bottom: none;margin-bottom:5px;">Marco Grandi Geometra</h1>'+
                    '<h2>Studio Tecnico Professionale</h2>'+
                    '<div id="bodyContent">'+
                    '<p style="font-size:12px;">Strada Massetana Romana 52/A, 53100 Siena<br>Tel. e Fax: +39 0577 221011<br>Cell.: 349 7739168<br><a href="mailto:marcograndi80@libero.it">marcograndi80@libero.it</a><br><a href="mailto:marco.grandi@geopec.it">marco.grandi@geopec.it</a>'+
                    '</div>'+
                    '</div>';
 
var infowindow = new google.maps.InfoWindow({
    content: contentString
});
google.maps.event.addListener(companyMarker, 'click', function() {
  infowindow.open(map,companyMarker);
});
    }
</script>
        <div id="maincontainer">
            <div id="contentleft">
                <div id="map_canvas" style="width:100%; height:500px;">
                </div>
                <small>Visualizza <a href="https://maps.google.it/maps/ms?msa=0&amp;msid=216594314733371648043.0004d69ddba78657ef3cc&amp;hl=it&amp;ie=UTF8&amp;ll=43.304046,11.321744&amp;spn=0,0&amp;t=m&amp;iwloc=0004d69ddf3f84a86f0b0&amp;source=embed" style="color:#0000FF;text-align:left">Geometra Marco Grandi</a> in una mappa di dimensioni maggiori</small>

            </div>
            <div id="contentright">
                <h1>Contattami</h1>
                <p class="confirm">
                <?php
                    if ($mailstatus == "1") echo $messassagestatus;
                ?>
                 </p>
                 <form method="post" action="contatti.php" id="contatti">
                    <div class="formfield">
                       <fieldset>
                           <label for="name">Nome e Cognome *</label>
                           <input type="text" id="name" name="name">
                       </fieldset>
                       <fieldset>
                           <label for="phone">Numero di Telefono</label>
                           <input type="text" id="phone" name="phone">
                       </fieldset>
                       <fieldset>
                           <label for="email">Indirizzo Email *</label>
                           <input type="text" id="email" name="email">
                       </fieldset>
                       <fieldset>
                           <label for="message">Il tuo messaggio *</label>
                           <textarea id="message" name="message"></textarea>
                       </fieldset>
                        <fieldset><p style="text-align: center; color:#f33;">* Campi obbligatori</p></fieldset>
                       <fieldset>
                           <input type ="submit" name="doSend" value="Invia">
                       </fieldset>
                    
                    </div>            
                </form>
            </div>
        </div>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).ready(function() {

	// validate signup form on keyup and submit
	var validator = $("#contatti").validate({
		rules: {
			name: "required",
            phone: {
                required: false,
                minlength: 9
            },
            message: "required",
            privacy: "required",
			email: {
				required: true,
                minlength: 7,
                email: true
			}
		},
		messages: {
			name: "Campo obbligatorio - Inserisci il tuo nome",
                        surname: "Inserisci il tuo cognome",
			phone: {
				
				minlength: "Il numero deve essere composto da almeno 9 cifre"
			},
            message: "Inserisci un messaggio",
            privacy: "Accetazione informativa sulla privacy obbligatoria!<br> ",
			email: {
                required: "Campo obbligatorio - Inserisci un indirizzo email",
                minlength: "Inserisci un indirizzo email valido",
                email: "Inserisci un indirizzo email valido"
            }
			
		},
	});


});
</script> 
<?php
include_once 'footer.php';
?>