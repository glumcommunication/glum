<?php
$pagename = 'profilo';
$pagetitle = 'Profilo';
$pagedesc = "Marco Grandi è un geometra che opera a Siena. Tra i servizi che offre ai suoi clienti ci sono la redazione di certificati energetici, valutazioni immobiliari, progettazione edile, civile e rurale, coordinamento della sicurezza nei cantieri, consulenze tecniche e pratiche catastali. È iscritto all'albo professionale dei geometri dal 2002 e la sua formazione è in continua evoluzione.";
include_once 'header.php';
?>
        <div id="maincontainer" class="mywork">
            <div id="contentright">
                <p>
                Nato a Siena il 09/01/1980. 
                Diploma di Geometra conseguito nell’Anno scolastico 1998/1999 con la votazione di 95/100, rilasciato dall’Istituto Tecnico Commerciale e per Geometri S. Bandini di Siena il 14.07.1999.
                </p>               
                <p>Diploma di abilitazione professionale all’esercizio della libera professione Esami di stato Sessione 2002, 
                iscrizione all’Albo Professionale dei Geometri della Provincia di Siena con il n° 1388.<br>
                Diploma di abilitazione con attestato di frequenza del corso di formazione per “Coordinatore per la sicurezza” 
                ai sensi del D.lgs 494/96 e s.m.i. – l’attestato è rilasciato ai sensi dell’art. 10 comma 2 del D.lgs 494/96 e s.m.i.<br>
                Corso di aggiornamento sulla normativa sismica di cui all’Ordinanza 32784 del 20/03/03 e s.m.i. rilasciato 
                nel Febbraio 2006 dal Collegio dei Geometri della Provincia di Siena con il patrocinio del Servizio sismico della Regione Toscana<br>
                Attestato di partecipazione rilasciato dall’Azienda USL 7 di Siena dipartimento di prevenzione in merito al 
                “Regolamento di attuazione dell’art. 82 comma 14-16 della L.R. 1/2005 (in riferimento al Documento unico di regolarità contributiva e installazione di linee vita) rilasciato a Marzo 2006<br>
                Formazione professionale continua seminario di “Termografia Edile la diagnostica dell’infrarosso” nel maggio 2007<br>
                Corso sull’acustica degli edifici nel maggio 2007<br>
                Formazione professionale continua seminario sulla “Legge Regionale 1/2005” alla luce dei regolamenti di attuazione nel giugno 2007<br>
                Corso di progettazione per efficienza energetica dell’involucro edilizio rilasciato a Marzo 2008<br>
                Formazione professionale continua seminario sul “Catasto dei Fabbricati nascosti” nel maggio 2008<br>
                Corso sui contenuti del “D.Lgs. 81/2008 – Testo Unico sulla sicurezza” nel Luglio 2008<br>
                Corso di aggiornamento per coordinatore alla sicurezza rilasciato in data 9 Marzo 2009 dall’Ente Senese Scuola Edile<br>
                Formazione professionale continua seminario sulla “Legge Regionale 54/2009 – Legge straordinaria sull’edilizia” nel Giugno 2009<br>
                Corso aggiornamento D.Lgs. 81/2008 – “Salute e sicurezza sui luoghi di lavoro” nel Gennaio 2010<br>
                Formazione professionale continua seminario sulla “Climatizzazione naturale nell’architettura e negli spazi urbani” nel Maggio 2010<br>
                Formazione professionale continua seminario su “Gli obblighi in tema di sicurezza del lavoro” nel Novembre 2011</p>
               
                <p>Formazione professionale continua seminario sul “Lavoratore autonomo in cantiere” nel Dicembre 2011<br>
                Corso “Dalla progettazione all’abitabilità – Residenziale SCIA COMMERCIALE passando dall’AGIBILITA’ – Ricettività, Commerciale e Strutture didattiche” nel Marzo 2012<br>
                Corso per “Certificatore Energetico” eseguito durante i primi mesi dell’anno 2012<br>
                La formazione è in continuo aggiornamento</p>  
            </div>
        </div>
<?php
include_once 'footer.php';
?>