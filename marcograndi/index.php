<?php
$pagename = 'index';
$pagetitle = 'Home';
$pagedesc = "Lo studio tecnico professionale del geometra Marco Grandi si trova a Siena, in strada Massetana Romana 52/A.";
include_once 'header.php';
?>
        <div id="maincontainer" class="myhome">
            <div id="contentleft" style="font-size:14px;">
                <ul>
                    <li>Progettazione civile per ristrutturazioni e nuove costruzioni con Direzione Lavori</li>
                    <li>Rilievi planimetrici e topografici</li>
                    <li>Computi metrici e contabilità</li>
                    <li>Coordinamento della Sicurezza</li>
                    <li>Pratiche catastali</li>
                    <li>Consulenza tecniche</li>
                    <li>Redazione certificati energetici</li>
                    <li>Valutazioni immobiliari</li>
                    <li>Redazione di tabelle millesimali per condomini</li>
                </ul>
            </div>
        </div>
<?php
include_once 'footer.php';
?>