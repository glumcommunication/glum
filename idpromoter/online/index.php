<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="images/favicon_o.png" type="image/svg"/> 
<meta http-equiv="refresh" content="3;url=welcome.php">
<META NAME="DESCRIPTION" CONTENT="Idpromoter, agenzia di pubblicità, comunicazione e grafica con sedi a Terranuova, Siena e Firenze. Concessionaria diretta di mezzi pubblicitari.">
<META NAME="KEYWORDS" CONTENT="Siena, Terranuova, Bracciolini, Valdarno, pubblicità, grafica, agenzia, comunicazione, concessionaria, mezzi, internet, Servizio, hostess, eventi, Guerrilla, Marketing, Social, newsletter">
<link rel="stylesheet" type="text/css" href="css/indexStyle.css" />
<link href='http://fonts.googleapis.com/css?family=Podkova' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
            $("body").css("display", "none");
            $("body").fadeIn(2500);
    });
</script>

<title>IdPromoter, la Passione di Comunicare: pubblicità Siena, Terranuova Bracciolini, Valdarno</title>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33180042-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>
<body>

<div id="container">
	<div id="header">
	</div>
	<div id="main">
	<!--<h1>La <span style="color: rgba(215,132,0,1)">Passione</span> di Comunicare</h1>-->
	<a href="welcome.php"><img src="images/passionedicomunicare.png"  alt="La Passione di Comunicare"/></a>
	</div>
	<div id="footer">
		<img src="images/logo_index.png" alt="IdPromoter"/>		
	</div>
</div>
	
</body>
</html>