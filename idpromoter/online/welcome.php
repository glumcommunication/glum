<?php
$thisPage = "welcome";
$description = "Idpromoter, agenzia di pubblicità, comunicazione e grafica con sedi a Terranuova, Siena e Firenze. Concessionaria diretta di mezzi pubblicitari.";
$title = "Welcome";
include("header.php");
?>
<div id="main">
<div id="boxmedia">
        <div class="flexslider">
          <ul class="slides">
            <li>
  	    	    <img src="images/slides_azienda_0.png" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/slides_azienda_1.png" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/slides_azienda_2.png" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/slides_azienda_3.png" />
  	    		</li>
            <li>
  	    	    <img src="images/slides_azienda_4.png" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/slides_azienda_5.png" />
  	    		</li>
          </ul>
        </div>

</div>
</div>
<?php
include("footer.php");
?>