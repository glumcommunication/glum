<?php
$thisPage = "contatti";
$title = "Contatti";
$description = "I contatti delle sedi e delle figure professionali di Idpromoter.";
include("header.php");
?>

	<div id="main">
		<div id="boxmedia">
			<div id="boxmediaLeft">
			<div id="contentLeft">
			<ul>
			<li>
<iframe width="400" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=firenze,+via+delle+mantellate+8&amp;sll=37.0625,-95.677068&amp;sspn=61.153041,135.263672&amp;ie=UTF8&amp;hq=&amp;hnear=Via+delle+Mantellate,+8,+50129+Firenze,+Toscana,+Italia&amp;t=m&amp;ll=43.789127,11.261244&amp;spn=0.018588,0.034332&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=it&amp;geocode=&amp;q=firenze,+via+delle+mantellate+8&amp;sll=37.0625,-95.677068&amp;sspn=61.153041,135.263672&amp;ie=UTF8&amp;hq=&amp;hnear=Via+delle+Mantellate,+8,+50129+Firenze,+Toscana,+Italia&amp;t=m&amp;ll=43.789127,11.261244&amp;spn=0.018588,0.034332&amp;z=14&amp;iwloc=A" style="color:#0000FF;text-align:left">Visualizzazione ingrandita della mappa</a></small>			
			</li>
			<li class="whiteb">SEDE LEGALE</li>
     <li>Via delle Mantellate 8</li>
     <li>50129 Firenze</li>
     <li>P.I.: 05854250486</li>
     </ul>
			<!--<table style="margin:0 auto;" cellspacing="10">
<tr>
	<td colspan="2">
	<a href="" onclick="loadNewPage('fabrizio.html'); return false;">FABRIZIO TRADITI</a><br />
	Founder & CEO<br />
	&nbsp;	
	</td>
</tr>
<tr>
	<td>
	<a href="" onclick="loadNewPage('emanuele.html'); return false;">EMANUELE BENCINI</a>	<br />
	Account Director<br />
	&nbsp;
	</td>
	<td>
	<a href="" onclick="loadNewPage('michele.html'); return false;">MICHELE SESTINI</a><br />
	Art Director Junior<br />
	&nbsp;
	</td>
</tr>
<tr>
	<td>
	<a href="" onclick="loadNewPage('gennaro.html'); return false;">GENNARO DI DOMENICO</a><br />
	Area Account Manager<br />
	&nbsp;
	</td>
	<td>
	<a href="" onclick="loadNewPage('alessandra.html'); return false;">ALESSANDRA SANTOMARCO</a><br />
	Social Media Strategist<br />
	&nbsp;	
	</td>
</tr>
<tr>
	<td>
	<a href="" onclick="loadNewPage('azzurra.html'); return false;">AZZURRA MAGHERINI</a><br />
	Account Executive<br />
	&nbsp;
	</td>

</tr>
<tr>
	<td>
	<a href="" onclick="loadNewPage('cesare.html'); return false;">CESARE RINALDI</a><br />
	Web Developer<br />
	&nbsp;
	</td>
	<td>
	<a href="" onclick="loadNewPage('carolina.html'); return false;">CAROLINA MORI</a><br />
	Account<br />
	&nbsp;
	</td>
</tr>
<tr>
	<td>
	<a href="" onclick="loadNewPage('david.html'); return false;">DAVID VAGELLINI</a><br />
	Account<br />
	&nbsp;
	</td>
	<td>
	<a href="" onclick="loadNewPage('claudia.html'); return false;">CLAUDIA POCCETTI</a><br />
	Executive Assistant<br />
	&nbsp;
	</td>
</tr>

</table>
-->
				</div>
			</div>
			<div id="boxmediaRight">
			<div id="contentRight">			
<!--					<img src="images/people/foto_ID.png" alt="contatti" />
			<table  id="tabellaContatti">
					<tr>
						<td><p><strong>Sede Legale</strong></p>
						<p>Via delle Mantellate 8<br />50129 Firenze<br />P.I.: 05854250486</p></td>
						<td><p><strong>Sede Operativa</strong></p>
						<p>Via Massetana Romana 64<br />53100 Siena<br />Tel: +39 0577058394</p></td>
						<td><p><strong>Sede Operativa</strong></p>
						<p>Via Vittorio Veneto 12<br />52028 Terranuova B.ni<br />Tel: +39 0550942816<br />
						Amministrazione: <br />+39 3807736154</p></td>
					</tr>				
				</table>-->
			<ul>
			<li>
<iframe width="400" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=it&amp;geocode=&amp;q=Via+Poggilupi+468,+Terranuova+Bracciolini,+AR,+Italia&amp;aq=&amp;sll=43.552138,11.558548&amp;sspn=0.006983,0.016512&amp;ie=UTF8&amp;hq=&amp;hnear=Via+Poggilupi,+Terranuova+Bracciolini,+Arezzo,+Toscana,+Italia&amp;t=m&amp;ll=43.558563,11.560793&amp;spn=0.01866,0.034246&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=it&amp;geocode=&amp;q=Via+Poggilupi+468,+Terranuova+Bracciolini,+AR,+Italia&amp;aq=&amp;sll=43.552138,11.558548&amp;sspn=0.006983,0.016512&amp;ie=UTF8&amp;hq=&amp;hnear=Via+Poggilupi,+Terranuova+Bracciolini,+Arezzo,+Toscana,+Italia&amp;t=m&amp;ll=43.558563,11.560793&amp;spn=0.01866,0.034246&amp;z=14&amp;iwloc=A" style="color:#0000FF;text-align:left">Visualizzazione ingrandita della mappa</a></small>
			</li>
			<li class="orangeb">SEDE OPERATIVA</li>
     <li>Via Poggilupi 468</li>
     <li>52028 Terranuova Bracciolini (AR)</li>
     <li>Tel: +39 0550942816</li>
     </ul>				
       </div>
       </div>
		</div>
	</div>

<?php
include("footer.php");
?>
