<?php
$thisPage = "portfolio";
$title = "Portfolio";
$description = "Fedele ad un approccio customer oriented, Idpromoter diventa strumento di crescita e progresso per ogni azienda. Il vasto pacchetto di clienti di tutta la Toscana e non solo, sono la testimonianza pià importante della nostra professionalita.";
include("header.php");
?>

	<div id="main">
		<div id="boxmedia">
			<div id="boxmediaLeft">
				<div id="contentLeft">
<p>Le aziende sono un insieme di risorse, idee, progetti, ambizioni e sentimenti; noi diamo voce ad ogni esigenza, carpendone le unicità e trasformandola in un'arma vincente per la propria comunicazione.</p>
<p>Fedele ad un approccio customer oriented, Idpromoter diventa strumento di crescita e progresso per ogni azienda.</p>
<p>Vestire i clienti con una comunicazione su misura è il nostro credo e il nostro traguardo.</p>
				</div>
			</div>
			<div id="boxmediaRight">
			<div style="display: table;height:500px;">
			<div id="contentRight" style="padding-right: 0;">
			<p>Pagina in costruzione.</p>

<!--			<p>Delizie di Mare&emsp;Cucine Zani&emsp;Gruppo Arkell<br />
			Luppoli Case&emsp;BluInfo&emsp;Oxford School&emsp;Tosoni Auto&emsp;La Favorita<br />
			Pianigiani Rottami&emsp;US Poggibonsi&emsp;Ristorante Lo Scudiero&emsp;MOH Radda<br />
			MOH Bologna&emsp;MOH La Spezia&emsp;MOH Parma&emsp;Lochness Pub&emsp;Secur Studi<br />
			Case di Siena&emsp;Delizie in Cucina&emsp;Edilbelmonte&emsp;Centro Infissi Siena<br />
			Gelateria Il Masgalano&emsp;Ristorante Meeting Sushi Wok&emsp;Re del Mare<br />
			Centro Sportivo Maltraverso&emsp;Polisportiva Mens Sana<br />
			Podere Casanova&emsp;Ristorante La Fornace di Meleto<br />
			Palestra Dentro Le Mura&emsp;Pd Colle Val d'Elsa<br />
			&emsp;Vodafone One&emsp;Panificio Sclavi<br />
			Termotecnica Chiarucci</p>

			<p>Fashion Valley&emsp;Bottacci Ottica&emsp;Cristal Lounge Bar<br />
			DF Alimentari&emsp;Fortini La Concessionaria&emsp;Palestra Harmony Club<br />
			ASD Korebo&emsp;Il Mercatale&emsp;Ristorante Nella e Franco&emsp;Podere San Lorenzo<br />
			Proloco Figline Valdarno&emsp;BCC Valdarno&emsp;Ristorante La Buca di Ipo&emsp;Valid<br />
			STE elettrica&emsp;Artigiana Metalli&emsp;Barbagli Tende&emsp;Every Dance studio<br />
			Metropolis Videonoleggio&emsp;Multichiusure&emsp;Saltalbero Parco Avventura<br />
			Polisportiva Salus&emsp;3 Store di Figline V.no&emsp;Ristorante La Braceria<br />
			Gioielleria Ciarponi&emsp;Forme Parrucchieri&emsp;Horae Orologeria<br />
			Manucci Elettronica&emsp;Multitec&emsp;Punto Tenda<br />
			Spagnoli Costruzioni&emsp;Thermolana<br />
			Supermarket della Scarpa
			
			</p>-->
			</div>
         </div>
         </div>
		</div>
	</div>

<?php
include("footer.php");
?>
