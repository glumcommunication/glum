<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META NAME="description" CONTENT="<?php echo $description ?>">
<META NAME="KEYWORDS" CONTENT="Siena, Terranuova, Bracciolini, Valdarno, pubblicità, grafica, agenzia, comunicazione, concessionaria, mezzi, internet, Servizio, hostess, eventi, Guerrilla, Marketing, Social, newsletter">
<link rel="icon" href="images/favicon_o.png" type="image/svg"/> 
<link rel="stylesheet" type="text/css" href="css/idpromoterStyle.css" />
<link href='http://fonts.googleapis.com/css?family=Podkova' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<link href="css/flexslider.css" rel="stylesheet" type="text/css" />
<link href="css/lightbox.css" rel="stylesheet" />
<script src="js/jquery.flexslider.js"></script>
<script src="js/lightbox.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
            $("#main").css("display", "none");
            $("#main").fadeIn(1000);
    });
</script>
<?php
if($thisPage == "welcome") {
echo <<<_END
<script type="text/javascript" charset="utf-8">
  $(window).load(function() {
    $('.flexslider').flexslider({
        animation: "slide",
        slideshow: true,
		  slideshowSpeed: 7000,
      });
  });
</script>
_END;
}
if($thisPage == "mezzi") {
echo <<<_END
<script type="text/javascript" charset="utf-8">
  $(window).load(function() {
    $('.flexslider').flexslider({
        animation: "slide",
        slideshow: true,
		  slideshowSpeed: 5000,
		  controlNav: false,
      });
  });
</script>
<!-- <script src="js/slides_bm.min.jquery.js"></script>
    
        <script>
            $(function(){
			$('#slides_bm').slides({
				preload: true,
				preloadImage: 'images/loading.gif',
				play: 5000,
				pause: 2500,
				hoverPause: true,
				paginationClass: 'pagination_bm',
				generatePagination: true,
				generateNextPrev: false,
				start: 1
			});
            });
        </script>-->
_END;
}
if($thisPage == "contatti" || "mezzi") echo "<script type=\"text/javascript\" src=\"js/loadNewPage.js\"></script>";
?>
<title>IdPromoter, la Passione di Comunicare: pubblicità Siena, Terranuova Bracciolini, Valdarno | <?php echo $title ?></title>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33180042-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body>
	<div id="container">
		<div id="header">
			<div id="headerLogo">
				<h1><a href="welcome.php" title="La Passione di Comunicare">La <span style="color: rgba(215,132,0,1)">Passione</span> di Comunicare</a></h1>
			</div>
			<div id="separator">
				<img src="images/separator.png" class="textmiddle" alt="" />
			</div>
			<nav id="navigation">
				<a <?php if ($thisPage=="azienda") echo " id=\"orange\"";  ?>href="azienda.php" title="L'azienda">Azienda</a>
				<a <?php if ($thisPage=="mezzi") echo " id=\"orange\"";  ?>href="mezzi.php" title="I mezzi di comunicazione">Mezzi</a>
				<a <?php if ($thisPage=="portfolio") echo " id=\"orange\"";  ?>href="portfolio.php" title="Portfolio">Portfolio</a>
				<a <?php if ($thisPage=="contatti") echo " id=\"orange\"";  ?>href="contatti.php" title="Contatti">Contatti</a>				
			</nav>
<!--			<div id="social">
				<a href="http://www.facebook.com/pages/Idpromoter/326093060766581" target="_BLANK" title="Facebook">
					<img src="images/sm_fb.png" height="24px" width="24px" alt="Facebook" />
				</a>
				<a href="http://twitter.com/IDPROMOTER" target="_BLANK" title="Twitter">
					<img src="images/sm_tw.png" height="24px" width="24px" alt="Twitter" />
				</a>
				<a href="http://www.youtube.com/channel/UCJvfeJfNXkwmrMgQ03aFjzQ" target="_BLANK" title="Youtube">
					<img src="images/sm_yt.png" height="24px" width="24px" alt="YouTube" />
					
				</a>			
			</div>-->
		</div>

