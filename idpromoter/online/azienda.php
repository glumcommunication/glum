<?php
$thisPage = "azienda";
$title = "L'azienda";
$description = "Agenzia di comunicazione, di consulenza marketing, studio grafico e pianificazione mezzi. Realizziamo campagne pubblicitarie, pianificazione media, progettazione immagine coordinata. Differenti competenze finalizzate a vestire i clienti con una comunicazione su misura e unica.";
include("header.php");
?>
<div id="main">
	<div id="boxmedia">
		<div id="boxmediaLeft">
			<div id="contentLeft">
				<p>Idpromoter è un'agenzia giovane, dinamica e competitiva, che ha fatto della pubblicità il suo mestiere.</p>
				<p>È la passione, che alimenta la nostra creatività e rende i nostri prodotti unici e distinguibili. La forza delle nostre idee risiede nella curiosità con cui ci affacciamo al mondo, bramosi di nuove conoscenze.</p>
				<p>Idpromoter opera nel mercato con molteplici mezzi pubblicitari di cui è concessionaria diretta, costantemente alla ricerca del giusto equilibrio tra forme di comunicazione innovative e strumenti tradizionali sempre efficaci.</p> 

			</div>
		</div>
		<div id="boxmediaRight">	
		<img src="images/logo_azienda.png" alt="idPromoter logo" class="logoazienda"/>
		</div>
	</div>
</div>
<?php
include("footer.php");
?>