<?php
$thisPage = "mezzi";
$title = "I mezzi di comunicazione";
$description = "Pianifichiamo forme di comunicazione innovative implementate su strumenti tradizionali sempre efficaci. Pubblicità cartacea, Cartellonistica, Totem, Manifesti, Strutture Bilite, Cover Page, siti internet, social media.";
include("header.php");
?>
	<div id="main">
		<div id="boxmedia">
		<div id="slides_bm">
			<div id="boxmediaLeft">
			
			<div id="contentLeft">
				<ul>
					<!--<li class="whiteb">CONSULENZA</li>
					<li>Studio e realizzazione campagne pubblicitarie</li>
					<li>Media Planning</li>
					<li>Analisi di Mercato</li>
					<li>Organizzazione Eventi</li>
					<li>Ufficio Stampa</li>					
					<li>Guerrilla Marketing</li>
					
					<li>&nbsp;</li>-->
					
					<li class="whiteb">CARTACEO</li>
					<li><a href="images/media/flyer.png" rel="lightbox[flyer]" title="Luppoli Case (Flyer)">Volantini</a>
						<div style="visibility: hidden;">
							<a href="images/media/flyer_01.png" rel="lightbox[flyer]" title="Ristorante Wine Bar Lo scudiero (Flyer)"></a>
							<a href="images/media/flyer_02.png" rel="lightbox[flyer]"title="Ristorante Il Braciere del Cantastorie (Flyer)"></a>											
						</div>	
					</li>				
					<li><a href="images/media/locandina.png" rel="lightbox[locandina]" title="Delizie di Mare (Locandina)">Locandine</a>
						<div style="visibility: hidden;">
							<a href="images/media/locandina_01.png" rel="lightbox[locandina]" title="Edil Belmonte (Locandina)"></a>
							<a href="images/media/locandina_02.png" rel="lightbox[locandina]" title="Ristorante Fornace di Meleto (Locandina)"></a>											
						</div>					
					</li>
					<li><a href="images/media/bvisita.png" rel="lightbox[bvisita]" title="Luisa Del Campana (Biglieto da visita)">Biglietti da Visita</a>
						<div style="visibility: hidden;">
							<a href="images/media/bvisita_01.png" rel="lightbox[bvisita]" title="Vodafone (Biglietto da visita)"></a>
							<a href="images/media/bvisita_02.png" rel="lightbox[bvisita]" title="Tiziano Fenu (Biglietto da visita)"></a>											
						</div>					
					</li>
					<li><a href="images/media/brochure.png" rel="lightbox[brochure]" title="Podere Casanova (Brochure)">Brochure e Pieghevoli</a>
						<div style="visibility: hidden;">
							<a href="images/media/brochure_01.png" rel="lightbox[brochure]" title="Estate a Figline (Brochure)"></a>
							<a href="images/media/brochure_02.png" rel="lightbox[brochure]" title="Delizie di Mare (Brochure)"></a>											
						</div>					
					</li>		
					<li>Gadget</li>								
					

				</ul>
				</div>
			</div>
			<div id="boxmediaRight">
					<div id="contentRight">
					<ul>
					<li class="orangeb">OUTDOOR</li>
					<li><a href="images/media/6x3.png" rel="lightbox[6x3]" title="Osteria La Braceria (Banner)">Cartellonistica</a>
						<div style="visibility: hidden;">
							<a href="images/media/6x3_01.png" rel="lightbox[6x3]" title="Forme (Banner)"></a>
							<a href="images/media/6x3_02.png" rel="lightbox[6x3]" title="in Auto (Banner)"></a>											
						</div>					
					</li>
					<li>Totem</li>										
					<li><a href="images/media/70x100.png" rel="lightbox[70x100]" title="Findomestic (Manifesto)">Manifesti</a>
						<div style="visibility: hidden;">
							<a href="images/media/70x100_01.png" rel="lightbox[70x100]" title="Materassi e Divani (Manifesto)"></a>
							<a href="images/media/70x100_02.png" rel="lightbox[70x100]" title="Due Emme Parrucchieri (Manifesto)"></a>											
						</div>					
					</li>

					<li><a href="images/media/bilite.png" rel="lightbox[bilite]" title="Studio Malatesta (Bilite)">Strutture Bilite</a>
						<div style="visibility: hidden;">
							<a href="images/media/bilite01.png" rel="lightbox[bilite]" title="Elettronica Mannucci (Bilite)"></a>
							<a href="images/media/bilite02.png" rel="lightbox[bilite]" title="Fortini (Bilite)"></a>											
						</div>					
					</li>
					<li><a href="images/media/camion.png" rel="lightbox[camion]" title="Polverini Arredamenti (Camion vela)">Camion Vela</a>
						<div style="visibility: hidden;">
							<a href="images/media/camion01.png" rel="lightbox[camion]" title="Fashion Valley (Camion vela)"></a>
							<a href="images/media/camion02.png" rel="lightbox[camion]" title="Polverini Arredamenti (Camion vela)"></a>											
						</div>					
					</li>
					
					<!--<li class="orangeb">STUDIO GRAFICO</li>
				<li><a href="images/media/logo.png" rel="lightbox[logo]" title="Secur Studi (Logo)">Ideazione Loghi</a>
						<div style="visibility: hidden;">
							<a href="images/media/logo_01.png" rel="lightbox[logo]" title="The New Oxford School (Logo)"></a>
							<a href="images/media/logo_02.png" rel="lightbox[logo]" title="Gelateria Artigianale Il Masgalano (Logo)"></a>											
						</div>					
					</li>	
					<li>Immagine Coordinata</li>
					<li>Presentazione Aziendale</li>
					<li>Packaging</li>
					<li>Rendering</li>
					<li>Presentazioni Digitali</li>
					<li>Grafica Web</li>
					<li>Grafica 3D</li>	
					<li>&nbsp;</li>					
					<li class="orangeb">INTERNET</li>
					<li><a href="images/media/sito.png" rel="lightbox[sito]" title="Ristorante Wine Bar Lo Scudiero (Sito internet)">Realizzazione Siti</a>
						<div style="visibility: hidden;">
							<a href="images/media/sito01.png" rel="lightbox[sito]" title="Ristorante Il Santo Graal (Sito internet)"></a>
						</div>					
					</li>
					<li>Restyling siti</li>
					<li>Sviluppo e Gestione Portali</li>
					<li>Social Marketing</li>
					<li>Posizionamento sui Motori di Ricerca</li>
					<li>Banner Pubblicitari</li>
					<li>Campagne Email</li>
					<li>Campagne Sms</li>-->
				
					</ul>
					</div>				

			</div>
		</div>
	</div>
	</div>
	
<?php
include("footer.php");
?>