<?php
$thisPage = "mezzi";
include("header.php");
?>
	<div id="main">
		<div id="boxmedia">
		<div id="slides_bm">
			<div id="boxmediaLeft">
			
			<div id="contentLeft">
				<ul>
					<li>CONSULENZA</li>
					<li>Studio e realizzazione campagne pubblicitarie</li>
					<li>Pianificazione e selezione di media</li>
					<li>Supervisione Progettuale</li>
					<li>Iniziative Promozionali</li>
					<li>Ideazione e Progettazione Immagine Coordinata</li>
					<li>Creazione di Logo</li>
					<li>Guerrilla Marketing</li>
					<br />
					<li>PUBBLICITA' CARTACEA</li>
					<li>Volantini</li>
					<li>Locandine</li>
					<li>Biglietti da Visita</li>
					<li>Brochure</li>
					<li>Manifesti</li>
					<li>Banner</li>
					<li>Strutture Bilite</li>
					<li>Camion Vela</li>
					<li>Cover Page</li>
					<li>Gadget</li>
					<br />
					<li>PUBBLICITA' MULTIMEDIALE</li>
					<li>Siti Internet</li>
					<li>Cataloghi Interattivi</li>
					<li>Newsletter</li>
					<li>Analisi Web</li>
					<li>Social Marketing</li>
					<li>Ottimizzazione e Posizionamento Siti Web</li>
					<br />
					<li>Servizio Hostess</li>								
				</ul>
				</div>
			</div>
			<div id="boxmediaRight">	
<!--							<div id="slides_bm">-->
            <div class="slides_bm_container">
                <div>
                    <img src="images/media/flyer.png">
                </div>
                <div>
                    <img src="images/media/flyer_01.png">
                </div>
                <div>
                    <img src="images/media/flyer_02.png">
                </div>
                <div>
                    <img src="images/media/locandina.png">
                </div>
                <div>
                    <img src="images/media/locandina_01.png">
                </div>
                <div>
                    <img src="images/media/locandina_02.png">
                </div>
                <div>
                    <img src="images/media/bvisita.png">
                </div>
                <div>
                    <img src="images/media/bvisita_01.png">
                </div>
                <div>
                    <img src="images/media/bvisita_02.png">
                </div>
                <div>
                    <img src="images/media/brochure.png">
                </div>
                <div>
                    <img src="images/media/brochure_01.png">
                </div>
                <div>
                    <img src="images/media/brochure_02.png">
                </div>
                <div>
                    <img src="images/media/70x100.png">
                </div>
                <div>
                    <img src="images/media/70x100_01.png">
                </div>
                <div>
                    <img src="images/media/70x100_02.png">
                </div>
                <div>
                    <img src="images/media/6x3.png">
                </div>
                <div>
                    <img src="images/media/6x3_01.png">
                </div>
                <div>
                    <img src="images/media/6x3_02.png">
                </div>
       			 <div>
                    <img src="images/media/bilite.png">
                </div>
                <div>
                    <img src="images/media/bilite01.png">
                </div>
                <div>
                    <img src="images/media/bilite02.png">
                </div>
       			 <div>
                    <img src="images/media/camion.png">
                </div>
                <div>
                    <img src="images/media/camion01.png">
                </div>
                <div>
                    <img src="images/media/camion02.png">
                </div>
       			 <div>
                    <img src="images/media/cover.png">
                </div>
                <div>
                    <img src="images/media/cover01.png">
                </div>
                <div>
                    <img src="images/media/cover02.png">
                </div>
       			 <div>
                    <img src="images/media/logo.png">
                </div>
                <div>
                    <img src="images/media/logo_01.png">
                </div>
                <div>
                    <img src="images/media/logo_02.png">
                </div>
       			 <div>
                    <img src="images/media/sito.png">
                </div>
                <div>
                    <img src="images/media/sito01.png">
                </div>
            </div>
				</div>
		</div>
	</div>
	</div>
	
<?php
include("footer.php");
?>