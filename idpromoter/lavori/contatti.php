<?php
$thisPage = "contatti";
include("header.php");
?>

	<div id="main">
		<div id="boxmedia">
			<div id="boxmediaLeft">
			<div id="contentLeft">
				<ul>
					<li><a href="" onclick="loadNewPage('fabrizio.html'); return false;">FABRIZIO TRADITI</a></li>
					<li>Founder & CEO</li>
					<br />
					<li><a href="" onclick="loadNewPage('emanuele.html'); return false;">EMANUELE BENCINI</a></li>
					<li>Area Account Manager</li>
					<br />
					<li><a href="" onclick="loadNewPage('gennaro.html'); return false;">GENNARO DI DOMENICO</a></li>
					<li>Area Account Manager</li>
					<br />
					<li><a href="" onclick="loadNewPage('michele.html'); return false;">MICHELE SESTINI</a></li>
					<li>Art Director Junior - Siena</li>
					<br />
					<li><a href="" onclick="loadNewPage('claudia.html'); return false;">CLAUDIA POCCETTI</a></li>
					<li>Marketing Manager</li>
					<br />
					<li><a href="" onclick="loadNewPage('azzurra.html'); return false;">AZZURRA MAGHERINI</a></li>
					<li>Account Executive</li>
					<br />
					<li><a href="" onclick="loadNewPage('alessandra.html'); return false;">ALESSANDRA SANTOMARCO</a></li>
					<li>Social Media Strategist</li>
					<br />
					<li><a href="" onclick="loadNewPage('giulia.html'); return false;">GIULIA FELICI</a></li>
					<li>Copy Writer - Web Developer</li>
				</ul>
				</div>
			</div>
			<div id="boxmediaRight">
				<img src="images/people/foto_ID.png" alt="contatti" />
<!--				<ul>
				<li>Sede Legale</li>
				<li>Sede Operativa</li>
				<li>Sede Operativa</li>
				</ul>-->
				<table width="100%">
					<tr>
						<td><p><strong>Sede Legale</strong></p>
						<p>Via delle Mantellate 8<br />50129 Firenze<br />P.I.: 05854250486</p></td>
						<td><p><strong>Sede Operativa</strong></p>
						<p>Via Massetana Romana 64<br />53100 Siena<br />Tel: +39 0577058394</p></td>
						<td><p><strong>Sede Operativa</strong></p>
						<p>Via Vittorio Veneto 12<br />52028 Terranuova B.ni<br />Tel: +39 0550942816<br />
						Amministrazione: <br />+39 3807736154</p></td>
					</tr>				
				</table>
       </div>
		</div>
	</div>

<?php
include("footer.php");
?>
