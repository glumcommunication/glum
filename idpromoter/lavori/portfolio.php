<?php
$thisPage = "portfolio";
include("header.php");
?>

	<div id="main">
		<div id="boxmedia">
			<div id="boxmediaLeft">
				<div id="contentLeft">
<p>Le aziende sono un insieme di risorse, idee, progetti, ambizioni e sentimenti; noi diamo voce ad ogni esigenza, carpendone le unicità e trasformandola in un'arma vincente per la propria comunicazione.</p>
<p>Fedele ad un approccio customer oriented, Idpromoter diventa strumento di crescita e progresso per ogni azienda.</p>
<p>Vestire i clienti con una comunicazione su misura è il nostro credo e il nostro traguardo.</p>
				</div>
			</div>
			<div id="boxmediaRight">	
			<p>Pagina in allestimento.</p>
         </div>
		</div>
	</div>

<?php
include("footer.php");
?>
