<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav4.php"; ?>

        <div class="senzamenu container" id="main" role="main">
            <section class="slider">
                <div id="sliderfienile" class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="img/gli-olivi.jpg" />
                        </li>
                        
                        <li>
                            <img src="img/gli-olivi-3.jpg" />
                        </li>
                        <li>
                            <img src="img/gli-olivi-4.jpg" />
                        </li>
                        <li>
                            <img src="img/gli-olivi-5.jpg" />
                        </li>
                          <li>
                              <img src="img/gli-olivi9.jpg" />
                        </li>
                          <li>
                              <img src="img/gli-olivi10.jpg" />
                        </li>
                        

                    </ul>
                </div>
               
            </section>

        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                  <h1>Gli Olivi</h1>

<p>&nbsp;</p>

<p>L&#39;appartamento Gli Olivi si affaccia direttamente sul grande oliveto terrazzato, al quale deve il proprio nome. Gli ospiti avranno garantita la propria privacy grazie all&#39;ingresso indipendente e al terrazzino privato.</p>

<p>&nbsp;</p>

<p>Composizione:</p>

<p>1 soggiorno con angolo cottura,1 ampia camera matrimoniale,1 bagno/doccia area esterna privata attrezzata con tavolo, sedie ed ombrellone</p>


                   

                </div>
            </div>
        </div>
        <?php include "magica.php" ?>
        <?php include "appartamenti.php";?>
        <?php include "footer.php"; ?>
