<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav4.php"; ?>

        <div class="senzamenu container" id="main" role="main">
            <section class="slider">
                <div id="sliderfienile" class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="img/il-granaio.jpg" />
                        </li>
                        <li>
                            <img src="img/il-granaio2.jpg" />
                        </li>
                        <li>
                            <img src="img/il-granaio3.jpg" />
                        </li>
                        <!--<li>
                            <img src="img/il-granaio5.jpg" />
                        </li>
                        <li>
                            <img src="img/il-granaio6.jpg" />
                        </li>-->
                        <li>
                            <img src="img/il-granaio7.jpg" />
                        </li>
                        <li>
                            <img src="img/il-granaio8.jpg" />
                        </li>
                        <li>
                            <img src="img/il-granaio9.jpg" />
                        </li>


                    </ul>
                </div>
                
            </section>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
  <h1>Il Granaio</h1>

                    <p>&nbsp;</p>
                    <p>Il&nbsp;Granaio ha mantenuto il suo nome e la sua identit&agrave; invariati nel tempo; proprio qui, in passato, venivano riposti i raccolti del podere come grano ed orzo. L&#39;appartamento, molto curato nei dettagli, si trova nella parte pi&ugrave; alta del casale, da cui gode di una fantastica vista sull&#39;oliveto.</p>

                    <p>Composizione:</p>

                    <p>1 soggiorno con angolo cottura, 1 ampia camera matrimoniale, 1 bagno/doccia, area esterna privata attrezzata con tavolo, sedie ed ombrellone</p>








                </div>
            </div>
        </div>
        <?php include "magica.php" ?>
        <?php include "appartamenti.php"; ?>
        <?php include "footer.php"; ?>
