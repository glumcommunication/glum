<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav.php"; ?>

        <div class="container">
            <p><strong>Condizioni generali</strong></p>

            <p>Per confermare la prenotazione, &egrave; necessario inviare, entro 7 giorni lavorativi, un acconto pari al 30% circa dell&#39;importo totale. Una volta effettuato il versamento (tramite bonifico bancario o altre modalit&agrave; da concordare), si prega di inviarci via mail una copia della ricevuta di pagamento.</p>

            <p>Se la prenotazione sar&agrave; cancellata almeno 21 giorni prima dell&#39;arrivo previsto, agli ospiti sar&agrave; rimborsato il 100% dell&#39;acconto versato.</p>

            <p>In caso di cancellazione tra i 20 e gli 11 giorni prima dell&#39;arrivo previsto, all&#39;ospite sar&agrave; rimborsato il 50% dell&#39;acconto versato.</p>

            <p>In caso di cancellazione con un preavviso pari o inferiore a 10 giorni dall&#39;arrivo previsto, all&#39;ospite non sar&agrave; garantito alcun rimborso.</p>

            <p>Infine, in caso di no-show (mancato arrivo, senza alcun preavviso) all&#39;ospite sar&agrave; addebitato l&#39;intero costo del soggiorno.</p>

            <p>&nbsp;</p>

            <p><strong>Imposta di soggiorno</strong></p>

            <p>&nbsp;L&rsquo;imposta di soggiorno, adottata dal Comune di Sovicille con deliberazioni del Consiglio Comunale n&deg; 8 del 06.02.2013 e della Giunta Comunale n&deg; 20 del 18.03.2013 e n&deg; 30 del 15.04.2013 &egrave; in vigore dal 1&deg; maggio 2013.</p>

            <p>L&rsquo;imposta &egrave; istituita al fine di finanziare interventi in materia di turismo, ivi compresi quelli a sostegno delle strutture ricettive, nonch&eacute; interventi di manutenzione, fruizione e recupero dei beni culturali e ambientali locali nonch&eacute; dei relativi servizi pubblici locali.</p>

            <p><strong>CHI PAGA L&rsquo;IMPOSTA?&nbsp;</strong>Chi pernotta in una delle strutture ricettive del territorio comunale, versando l&rsquo;imposta al gestore della struttura che rilascia ricevuta.</p>

            <p><strong>QUANTO SI PAGA?</strong>&nbsp;Per gli agriturismi e le strutture extra-alberghiere, l&#39;imposta &egrave; di &euro;1,50. L&rsquo;imposta &egrave; dovuta per persona e per ogni pernottamento fino ad un massimo di sei pernottamenti, anche non continuativi, nel mese solare nella stessa struttura ricettiva.</p>

            <p>L&rsquo;imposta &egrave; applicata fino ad un massimo di sei pernottamenti per persona, anche non continuativi, in ogni mese solare e nella medesima struttura. Detto limite di sei vale anche in caso di pernottamenti continuativi a cavallo di due mesi, sempre nella stessa struttura.</p>

            <p>L&rsquo;imposta non si applica per presenze che registrino almeno 12 pernottamenti consecutivi, per persona e nella stessa struttura,&nbsp;&nbsp;sia nel corso del mese che a cavallo di due mesi.</p>

            <p>Per maggiori informazioni:&nbsp;<a href="http://www.comune.sovicille.si.it/Main.aspx?ID=601">http://www.comune.sovicille.si.it/Main.aspx?ID=601</a></p>

            <p>&nbsp;</p>


        </div>



        <?php include "footer.php"; ?>
