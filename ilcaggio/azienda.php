<div class="grigia">
    <div class="container " id="azienda">
        <div class="row">

    <div class="col-xs-12 ">
        <h1>Azienda Agricola</h1>
    </div>
    <div class="col-xs-12 ">
        <img src="img/sfondo2.jpg" class="img-responsive"/>
    </div>
    <div class="col-xs-12 text-center" >

        <p>Scegliendo un agriturismo non si sceglie semplicemente una sistemazione, si abbraccia uno stile di vita. Vivere la natura significa anche toccarne con mano i frutti, apprezzarne i doni. Come i prodotti che Il Caggio prepara con amore e cura con le migliori materie prime.

        </p>
    </div>
    <div class="text-center col-xs-12 col-sm-2 col-sm-offset-5">
        <a  href="aziendaagricola.php" class="btn btn-default " id="scopri_btn">SCOPRI</a>

    </div>
    </div>
</div>
</div>
<?php include "appartamenti.php";?>

<?php include "magica.php";?>
