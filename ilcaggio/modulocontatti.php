<?php
if ($_POST['doSend'] == 'Invia') {
    $name = $_POST[name];
    $email = $_POST[email];
    $subject = $_POST[subject];
    $tel = $_POST[tel];
    $message = $_POST[message];
    $headers = "";
    $headers .= "From: $email\n";
    $headers .= "Reply-To: $email\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=utf-8\n";
    $to = "francesco@glumcommunication.it";
    /* $message = $messaggio; */
    $messagebody = "Hai ricevuto una nuova email via IlCaggio.it, ecco i dettagli della prenotazione:<br />
        
Nome: $name <br />

E-mail: $email<br />
    
Telefono: $tel<br />

Oggetto: $subject<br>

Messaggio: $message<br />";

    mail($to, $subject, $messagebody, $headers);
    ?>
    <script>
        document.("#inviata").style.display = "inline";

    </script>
    <?php
}
?>
<div class="container  text-center" id="contacts" >
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-bottom: 30px;">
        <h1>Contattaci</h1>
    </div>

    <div id="inviata" style="display: none;"class="col-sm-12" >       
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <h4>Messaggio inviato!</h4>
            Sarai ricontattato al pi&ugrave; presto.
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 text-left">
            <h4>
                <p>La centralit&agrave; dell&#39;agriturismo Il Caggio vi sorprender&agrave;. Spostarvi e visitare la nostra meravigliosa terra di Toscana sar&agrave; un gioco da ragazzi. Distiamo infatti: 15 minuti da Siena, 50 minuti dal suggestivo paese di San Gimignano, circa un&#39;ora da Montalcino e Pienza, 40 minuti dalle Terme di Rapolano, un&#39;ora dal mare e 75 minuti da Firenze.</p>

                <p>COORDINATE GPS&nbsp;Lat/Long: (43.28770446224768, 11.208667159080505)</p>

                <p>Il check in &egrave; previsto dalle ore 15.00 alle ore 19.00. In caso di arrivo al di fuori dell&#39;orario previsto, si prega di avvisare telefonicamente il nostro staff. Il check out &egrave; previsto entro e non oltre le ore 10.00.</p>

                <p>La struttura &egrave; dotata di parcheggio privato non custodito. Sono ammessi animali domestici.</p>

                <p>Questa &egrave; la nostra e-mail: ilcaggio@interfree.it</p>

                <p>Telefono: +39 0577/345506&nbsp;</p>
                <p>Fax: +39 0577/345506&nbsp;</p>
                <p>Cellulare: +39 339/3856489</p>
            </h4>
        </div>
        <div class="col-xs-12 col-sm-6 ">



            <form action="index.php#contacts" method="post" id="contatti">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Inserisci il tuo nome">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Inserisci la tua email">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="tel" name="tel" placeholder="Inserisci un recapito telefonico">
                        </div>
                    </div>
                </div>    
                <div class="form-group">
                    <input type="text" class="form-control" id="subject" name="subject" value="Oggetto">
                </div>
                <div class="form-group">
                    <textarea class="form-control" rows="7" id="message" name="message"></textarea>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="Yep" name="privacy">
                        <a id="privacy" href="#" rel="tooltip" data-toggle="tooltip" title="Nel inviare i miei dati dichiaro di aver letto l'informativa e sono consapevole che il trattamento degli stessi è necessario per ottenere il servizio proposto. A tal fine, nel dichiarare di essere maggiorenne, fornisco il mio consenso." data-placement="right" style="font-size:14px;cursor: help;">
                            Informativa sulla Privacy ai sensi del D.Lgs 196/2003</a>
                    </label>
                </div>
                <div class="text-right">
                    <button id="posta" type="submit" class="btn btn-default text-right" name="doSend" value="Invia">Invia</button>
                </div>
        </div>


    </div>

</div>
<script src="js/modernizr.custom.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        if (Modernizr.touch) {
            $("figButton").click(function() {
                $(this).toggleClass("cap-left");
            });
            $("figClose").click(function() {
                $(this).toggleClass("cap-left");
            });
        }
        ;
    })

</script>
<script>
    $(document).ready(function() {

        // validate signup form on keyup and submit
        var validator = $("#contatti").validate({
            rules: {
                name: "required",
                tel: {required: true,
                    number: true,
                    minlenght: 9
                },
                privacy: "required",
                email: {
                    required: true,
                    minlength: 7,
                    email: true
                }
            },
            messages: {
                name: "Campo obbligatorio - Inserisci il tuo nome",
                tel: {required: "Inserire un recapito valido",
                    number: "Un numero di telefono composto di sole cifre",
                    minlenght: "Un numero di telefono valido"},
                privacy: "Accettazione informativa sulla privacy obbligatoria!<br> ",
                email: {
                    required: "Campo obbligatorio - Inserisci un indirizzo email",
                    minlength: "Inserisci un indirizzo email valido",
                    email: "Inserisci un indirizzo email valido"
                }

            }

        });


    });
</script>