<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav2.php"; ?>

        <div class="container">

            <div class="row col-sm-12 ">
                <div class="col-xs-12 col-sm-3 ">
                  <!--<span class="glyphicon glyphicon-tree-deciduous"></span>-->
                    <img src="img/colazione-3.jpg" class="img-responsive thumb"/>
                    <img src="img/colazione3.jpg" class="img-responsive thumb"/>
                    <img src="img/prodotti-3.jpg" class="img-responsive thumb"/>

                </div>
                <div class="col-xs-12 col-sm-9">
                    <p>Quando si sceglie un agriturismo non si sceglie soltanto una sistemazione dove passare la notte, si abbraccia uno stile di vita. Poco importa se per pochi giorni o per periodi pi&ugrave; lunghi.</p>

                    <p>&Egrave; per questo che, ogni giorno, facciamo il possibile affinch&eacute; il vostro soggiorno sia ricco di esperienze speciali ed uniche nella loro semplicit&agrave;: seguire i ritmi della natura, toccarne con mano i frutti, imparare ad apprezzare sia i doni che ci offre, che la fatica necessaria per poterne godere.</p>

                    <p>Da anni, l&#39;agriturismo Il Caggio mette tutta la passione e la cura nel realizzare i prodotti che offre ai propri ospiti e non solo. Tra i nostri prodotti avrete la possibilit&agrave; di assaggiare:</p>

                    <p>&nbsp;</p>

                    <ul>
                        <li>
                            <p>l&#39;olio extravergine d&#39;oliva, estratto esclusivamente dalle olive raccolte dagli alberi nei nostri terrazzamenti, con spremitura a freddo.</p>

                            <p>&nbsp;</p>
                        </li>
                        <li>
                            <p>le marmellate e le conserve, rigorosamente realizzate con frutti e ortaggi da noi coltivati, come susine, fichi, mele cotogne, ciliegie, pomodori, zucca ed altro</p>

                            <p>&nbsp;</p>
                        </li>
                        <li>
                            <p>le castagne, da cui ricaviamo la farina per dolci e molte altre specialit&agrave;</p>
                        </li>
                    </ul>

                    <p>&nbsp;</p>

                    <p>Potrete a inoltre acquistare questi ed altri prodotti direttamente nella nostra struttura, dove avrete il piacere di assaggiare le bont&agrave; tipiche locali nel corso delle degustazioni e delle cene tipiche che verranno organizzate nell&#39;agriturismo.</p>

                </div>


            </div>
        </div>
        <?php include "magica.php" ?>
        <?php include "footer.php"; ?>
