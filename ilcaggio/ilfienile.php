<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav4.php"; ?>

        <div class="senzamenu container" id="main" role="main">
            <section class="slider">
                <div id="sliderfienile" class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="img/il-fienile.jpg" />
                        </li>
                        <li>
                            <img src="img/il-fienile2.jpg" />
                        </li>
                        <li>
                            <img src="img/il-fienile3.jpg" />
                        </li>
                        <li>
                            <img src="img/il-fienile4.jpg" />
                        </li>
                        <li>
                            <img src="img/il-fienile5.jpg" />
                        </li>
                        <li>
                            <img src="img/il-fienile6.jpg" />
                        </li>
                        <li>
                            <img src="img/il-fienile7.jpg" />
                        </li>
                        <li>
                            <img src="img/il-fienile8.jpg" />
                        </li>
                        <li>
                            <img src="img/il-fienile9.jpg" />
                        </li>

                    </ul>
                </div>
                
            </section>

        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Il Fienile</h1>

                    <p>&nbsp;</p>
                    <p>Ricavato da una capanna dove veniva riposto il fieno durante l&#39;inverno, l&#39;appartamento Il Fienile &egrave; molto originale, ben rifinito e completamente indipendente dal resto della struttura. Nella parte esterna, a riparo da occhi indiscreti, i clienti potranno disporre di uno spazio verde riservato all&#39;appartamento.</p>

                    
                    <p>Composizione:</p>

                    <p>1 soggiorno con angolo cottura e stufa a legna, 1 elegante camera matrimoniale,1 soppalco adibito a camera da letto (matrimoniale/doppia), 1 soggiorno con angolo cottura, 1 bagno con vasca/doccia, area esterna privata ed attrezzata con tavolo, sedie ed ombrellone.
                   

                </div>
            </div>
        </div>
        <?php include "magica.php" ?>
        <?php include "appartamenti.php";?>
        <?php include "footer.php"; ?>
