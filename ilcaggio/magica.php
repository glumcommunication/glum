<div class="grigia">
    <div class="container ">

        <div class="row">
            <div class="col-xs-12 col-sm-10">
                <h1>Vieni a Vivere un esperienza magica</h1>
                <h4><p>La campagna senese ti stregher&agrave; in questa collina la cui storia inizia dall&#39;epoca etrusca con i primi insediamenti. La natura meravigliosa, il relax, il sole e la nostra piscina sono l&#39;ideale per coccolarvi quando avete bisogno di prendervi cura di voi stessi.</p></h4>

            </div>
            <div class="col-xs-12 col-sm-2 " id="magica">
                <a  href="prenota.php" class="btn btn-default col-xs-12 " id="prenota3">PRENOTA</a>
            </div>
        </div>
    </div>
</div>