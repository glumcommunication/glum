<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav.php"; ?>

        <div class="container">
            <div class="row col-xs-12 col-sm-8 col-sm-offset-2 text-center">
                <h1>I nostri servizi</h1>

            </div>
            <div class="row col-sm-12 ">
                <div class="col-xs-12 col-sm-6 servizi">
                  <!--<span class="glyphicon glyphicon-tree-deciduous"></span>-->
                    <img src="img/albero.png" class="icona"/>
                    <h2>Agriturismo</h2>
                    <h3>
                        <p>Il Caggio sorge a pochi minuti da Sovicille, immerso nel verde della Montagnola Senese, tra querce secolari ed arbusti aromatici. Il vasto parco recintato offre una grande variet&agrave; di specie vegetali e gode di una vista speciale, con scorci unici.</p>
                    </h3>
                </div>
                <div class="col-xs-12 col-sm-6 servizi">
                <!--    <span class="glyphicon glyphicon-home"></span>-->
                    <img src="img/appartamenti.png" class="icona"/>
                    <h2>Appartamenti</h2>
                    <h3>
                        <p>La struttura, ricavata da un antico podere contadino in un&#39;area un tempo abitata dagli Etruschi, &egrave; oggi suddivisa in sette comodi appartamenti, completamente ristrutturati secondo il gusto del tempo e dotati di ogni comfort per gli ospiti.</p>

                    </h3>
                </div>
                <div class="col-xs-12 col-sm-6 servizi">
                     <!--<span class="glyphicon glyphicon-tint"></span>-->
                    <img src="img/piscina.png" class="icona" />
                    <h2>Piscina</h2>
                    <h3>
                        <p>Il Caggio &egrave; un luogo ideale per gli amanti della natura e delle escursioni in campagna, o semplicemente per coloro che desiderano trascorrere una vacanza all&#39;insegna della quiete e del riposo, magari rilassandosi nella nostra splendida piscina attrezzata.</p>

                    </h3>
                </div>
                <div class="col-xs-12 col-sm-6 servizi">
                    <!--<span class="glyphicon glyphicon-tower"></span>-->
                    <img src="img/luoghi_di_interesse.png" class="icona"/>
                    <h2>Dintorni</h2>
                    <h3>
                        <p>Il Caggio gode di una posizione centrale rispetto ai luoghi di maggiore interesse artistico-culturale ed eno-gastronomico. Siena con le sue bellezze ed il suo Palio, San Gimignano, Monteriggioni e Montalcino sono ad una manciata di minuti in macchina.</p>

                    </h3>
                </div>
            </div>
        </div>
        <?php include "azienda.php"; ?>
        <?php include "virtualtour.php"; ?>    
     
        <?php include "footer.php"; ?>
