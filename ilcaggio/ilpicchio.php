<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav4.php"; ?>

        <div class="senzamenu container" id="main" role="main">
            <section class="slider">
                <div id="sliderfienile" class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="img/il-picchio.jpg" />
                        </li>
                        <li>
                            <img src="img/il-picchio2.jpg" />
                        </li>
                        <li>
                            <img src="img/il-picchio3.jpg" />
                        </li>
                        <li>
                            <img src="img/il-picchio5.jpg" />
                        </li>
                        <li>
                            <img src="img/il-picchio-6.jpg" />
                        </li>
                        <li>
                            <img src="img/il-picchio-7.jpg" />
                        </li>
                        <li>
                            <img src="img/il-picchio-8.jpg" />
                        </li>
                        <li>
                            <img src="img/il-picchio-9.jpg" />
                        </li>
                       

                    </ul>
                </div>
               
            </section>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Il Picchio </h1>
                    <p>&nbsp;</p> <p>L&#39;appartamento pi&ugrave; grande e spazioso dell&#39;agriturismo. Il soffitto &egrave; il protagonista indiscusso: le imponenti travi che lo attraversano hanno origine plurisecolare e non possono certo passare inosservate. L&#39;ingresso d&agrave; su un terrazzino privato, ideale per rilassarsi e prendere il sole.</p>



<p>Composizione:</p>

<p>1 soggiorno con angolo cottura, 2 ampie camere matrimoniali/doppie, 1 salotto con divano-letto, 2 bagno/doccia area esterna privata attrezzata con tavolo, sedie ed ombrellone</p>



                   

                </div>
            </div>
        </div>
        <?php include "magica.php" ?>
        <?php include "appartamenti.php";?>
        <?php include "footer.php"; ?>
