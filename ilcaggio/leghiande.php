<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav4.php"; ?>

        <div class="senzamenu container" id="main" role="main">
            <section class="slider">
                <div id="sliderfienile" class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="img/le-ghiande.jpg" />
                        </li>
                        <li>
                            <img src="img/le-ghiande2.jpg" />
                        </li>
                        <li>
                            <img src="img/le-ghiande3.jpg" />
                        </li>
                        <li>
                            <img src="img/le-ghiande4.jpg" />
                        </li>
                     <!--  <li>
                            <img src="img/le-ghiande5.jpg" />
                        </li>
                        <li>
                            <img src="img/le-ghiande6.jpg" />
                        </li>
                     -->
                        <li>
                            <img src="img/le-ghiande-7.jpg" />
                        </li>
                        <li>
                            <img src="img/le-ghiande-8.jpg" />
                        </li>
                        <li>
                            <img src="img/le-ghiande-9.jpg" />
                        </li>
                        <li>
                            <img src="img/le-ghiande-10.jpg" />
                        </li>
                       

                    </ul>
                </div>
               
            </section>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                   
             <h1>Le Ghiande</h1>

<p>&nbsp;</p>

<p>Le Ghiande &egrave; l&#39;appartamento situato pi&ugrave; in alto nell&#39;antico casale. Per questo motivo &egrave; in assoluto quello pi&ugrave; luminoso e panoramico: dalle sue finestre &egrave;, infatti, possibile avere una visuale a 360 gradi dell&#39;agriturismo e della zona circostante.</p>

<p>&nbsp;</p>

<p>Composizione:</p>

<p>1 soggiorno con angolo cottura, 2 ampie camere matrimoniali, 2 bagni/docce, area esterna privata attrezzata con tavolo, sedie ed ombrellone</p>



                </div>
            </div>
        </div>
        <?php include "magica.php" ?>
        <?php include "appartamenti.php";?>
        <?php include "footer.php"; ?>
