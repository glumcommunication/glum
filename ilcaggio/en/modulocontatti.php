<?php
if ($_POST['doSend'] == 'Invia') {
    $name = $_POST[name];
    $email = $_POST[email];
    $subject = $_POST[subject];
    $tel = $_POST[tel];
    $message = $_POST[message];
    $headers = "";
    $headers .= "From: $email\n";
    $headers .= "Reply-To: $email\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=utf-8\n";
    $to = "francesco@glumcommunication.it";
    /* $message = $messaggio; */
    $messagebody = "Hai ricevuto una nuova email via IlCaggio.it, ecco i dettagli della prenotazione:<br />
        
Nome: $name <br />

E-mail: $email<br />
    
Telefono: $tel<br />

Oggetto: $subject<br>

Messaggio: $message<br />";

    mail($to, $subject, $messagebody, $headers);
    ?>
    <script>
        document.("#inviata").style.display = "inline";

    </script>
    <?php
}
?>

    <div class="container  text-center" id="contacts">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1>Contact Us</h1>
        </div>
        <div id="inviata" style="display: none;"class="col-sm-12" >       
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <h4>Message Sent!!</h4>
                You will be contacted as soon as possible
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 text-left">

                <p>Location of farmhouse Il Caggio will amaze you. Moving and visiting our fabulous Tuscany will be very easy.</p>

                <p>We are 15 minutes far from Siena, 50 minutes from the striking San Gimignano, about an hour from Montalcino and Pienza, 40 minutes from Rapolano spa, an hour from the sea and 75 minutes from Florence.</p>

                <p><strong>GPS COORDINATES</strong></p>

                <p>Lat/Long: (43.28770446224768, 11.208667159080505)</p>

                <p>&nbsp;</p>

                <p>Check in is from 3:00 to 7:00 p.m. In case of later arrival, we kindly ask you to call to inform our staff.</p>

                <p>Check out is no later than 10:00 a.m.</p>

                <p>The building is equipped with a free unmanned parking. Pet animals are admitted.</p>

                <p>This is our e-mail: <a href="mailto:ilcaggio@interfree.it">ilcaggio@interfree.it</a></p>

                <p>Phone: +39 0577/345506</p>

                <p>Fax: +39 0577/345506</p>

                <p>&nbsp;</p>

                <p>Mobile: +39 339/3856489</p>

            </div>
            <div class="col-xs-12 col-sm-6 ">



                <form action="index.php#contacts" method="post" id="contatti">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Please enter your name">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Please enter your email">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="tel" name="tel" placeholder="Please enter a phone number">
                            </div>
                        </div>
                    </div>    
                    <div class="form-group">
                        <input type="text" class="form-control" id="subject" name="subject" value="Subject">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" rows="7" id="message" name="message"></textarea>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="Yep" name="privacy">
                            <a id="privacy" href="#" rel="tooltip" data-toggle="tooltip" title="Nel inviare i miei dati dichiaro di aver letto l'informativa e sono consapevole che il trattamento degli stessi è necessario per ottenere il servizio proposto. A tal fine, nel dichiarare di essere maggiorenne, fornisco il mio consenso." data-placement="right" style="font-size:14px;cursor: help;">
                                Informativa sulla Privacy ai sensi del D.Lgs 196/2003</a>
                        </label>
                    </div>
                    <div class="text-right">
                        <button id="posta" type="submit" class="btn btn-default text-right" name="doSend" value="Invia">Send</button>
                    </div>
            </div>


        </div>

    </div>

<script src="js/modernizr.custom.js"></script>

<script type="text/javascript">
        $(document).ready(function() {
            if (Modernizr.touch) {
                $("figButton").click(function() {
                    $(this).toggleClass("cap-left");
                });
                $("figClose").click(function() {
                    $(this).toggleClass("cap-left");
                });
            }
            ;
        })

</script>
<script>
    $(document).ready(function() {

        // validate signup form on keyup and submit
        var validator = $("#contatti").validate({
            rules: {
                name: "required",
                tel: {required: true,
                    number: true,
                    minlenght: 9
                },
                privacy: "required",
                email: {
                    required: true,
                    minlength: 7,
                    email: true
                }
            },
            messages: {
                name: "Field Request! - Enter your name",
                tel: {required: "Enter a valid number",
                    number: "Only digits allow",
                    minlenght: "A valid phonr number,please."},
                privacy: "Acceptance of Privacy Policy Required!<br> ",
                email: {
                    required: "Required Field! - Enter an email address",
                    minlength: "Enter a valid email address",
                    email: "This doesn't seem  a valid address.."
                }

            }

        });


    });
</script>