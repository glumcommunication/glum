<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav4.php"; ?>

        <div class="senzamenu container" id="main" role="main">
            <section class="slider">
                <div id="sliderfienile" class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="../img/il-picchio.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-picchio2.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-picchio3.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-picchio5.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-picchio-6.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-picchio-7.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-picchio-8.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-picchio-9.jpg" />
                        </li>


                    </ul>
                </div>
                
            </section>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Il Picchio </h1>
                    <p>Il Picchio is the farmhouse most spacious apartment. The ceiling is the uncontested protagonist: its impressive timbers originated from many centuries and can not be unnoticed. The entrance is in a private little terrace, ideal to relax and sunbathe.</p>

                    <p>Composition:</p>

                    <p>Dining room with kitchenette, Two wide double bedrooms, Living room with sofa bed, Two Bathrooms with shower, private external equipped area with table, chairs and parasol</p>





                </div>
            </div>
        </div>
        <?php include "magica.php" ?>
        <?php include "appartamenti.php"; ?>
        <?php include "footer.php"; ?>
