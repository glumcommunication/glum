<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
     
    <body>
        
        <?php include "nav4.php"; ?>

        
        <script src="js/modernizr.custom.70736.js"></script>
        <noscript><link rel="stylesheet" type="text/css" href="css/noJS.css"/></noscript>
        <!--[if lte IE 7]><style>.main{display:none;} .support-note .note-ie{display:block;}</style><![endif]-->
    </head>
<body>
    <div class="container listgallery">
        <div class="row">
            <?php
            for ($i = 1; $i < 43; $i++) {

                echo "<a class='col-lg-3 col-md-3 col-sm-6 col-xs-12' href='../images/large/$i.jpg' data-toggle='lightbox' data-gallery='multiimages' data-title='Agriturismo il Caggio Sovicille'><img src='../images/small/$i.jpg' alt='img0$i'  class='imagegallery img-responsive'/></a>";
            }
            ?>
        </div>

        
    </div>
   


    <?php include "footer.php"; ?>
