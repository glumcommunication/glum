<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav4.php"; ?>

        <div class="senzamenu container" id="main" role="main">
            <section class="slider">
                <div id="sliderfienile" class="flexslider">
                   <ul class="slides">
                        <li>
                            <img src="../img/le-querce.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-querce2.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-querce3.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-querce4.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-querce5.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-querce-6.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-querce-7.jpg" />
                        </li>
                        
                    </ul>
                </div>
                
            </section>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Le Querce</h1>

<p>&nbsp;</p>
<p>This apartment appears in the farmhouse biggest terrace from which you can admire the majestic secolar oaks erected above the swimming pool. In winter, in the dining room you can limber up thanks to the ancient cast-iron wood stove.</p>

<p>Composition</p>

<p>Dining room with kitchenette, Wide double bedroom, Double bedroom with twin beds, Living room with sofa-bed, Bathroom with shower, private external equipped area with table, chairs and parasol</p>


                   

                </div>
            </div>
        </div>
        <?php include "magica.php" ?>
        <?php include "appartamenti.php";?>
        <?php include "footer.php"; ?>
