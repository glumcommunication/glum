<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav2.php"; ?>

        <div class="container">
          
            <div class="row col-sm-12 ">
                <div class="col-xs-12 col-sm-3 ">
                  <!--<span class="glyphicon glyphicon-tree-deciduous"></span>-->
                    <img src="../img/colazione-3.jpg" class="img-responsive thumb"/>
                    <img src="../img/colazione3.jpg" class="img-responsive thumb"/>
                    <img src="../img/prodotti-3.jpg" class="img-responsive thumb"/>
                    
                </div>
                <div class="col-xs-12 col-sm-9">
             <p>When choosing a farmhouse you are not looking just for an accommodation where staying at night but also for a life-style. Does not matter if for a few days or for longer periods. It is for this reason that, each day, we do our outmost so that your stay could be rich in special and unique experiences in their simplicity. Following the nature rhythm, being able to see its fruits with one&rsquo;s own eyes, learning to appreciate both the gifts it gives us and the necessary efforts to enjoy them. From years farmhouse Il Caggio, with passion and care, has realized products for its customers and more. Among our products you can taste:</p>

<ul>
	<li>
	<p>Our extra virgin olive oil, extracted exclusively from our terrains olive trees, with cold pressing method.</p>
	</li>
	<li>
	<p>Our jams and preserves, rigorously made with fruits and vegetables we cultivate, such as prunes, figs, quinces, cherries, tomatoes, pumpkin and other.</p>
	</li>
	<li>
	<p>Our chestnuts, from which we obtain the flour for cakes and other specialties.</p>
	</li>
</ul>

<p>In addition, you could buy these products and others directly in our building where you will have the pleasure to taste typical local pleasures during the tastings and typical dinners organized in the farmhouse.</p>

                </div>
              
                
            </div>
        </div>
       <?php  include "magica.php"?>
        <?php include "footer.php"; ?>
