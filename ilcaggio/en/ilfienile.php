<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav4.php"; ?>

        <div class="senzamenu container" id="main" role="main">
            <section class="slider">
                <div id="sliderfienile" class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="../img/il-fienile.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-fienile2.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-fienile3.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-fienile4.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-fienile5.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-fienile6.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-fienile7.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-fienile8.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-fienile9.jpg" />
                        </li>

                    </ul>
                </div>
                
            </section>

        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Il Fienile</h1>

                    <p>&nbsp;</p>
                    <p>Obtained from a cabin for hay during winter, Il Fienile apartment is very original, well-refined and completely independent from the other part of the building. In the external part, far from indiscreet eyes, our guests can enjoy a green space reserved to the apartment.</p>

                    <p>Composition</p>

                    <p>Dining room with kitchenette and wood stove, Charming double bedroom, Loft used as bedroom (double/twin), Living room with kitchenette, Bathroom with bath tub/shower, private equipped area with table, chairs and parasol</p>


                </div>
            </div>
        </div>
        <?php include "magica.php" ?>
        <?php include "appartamenti.php"; ?>
        <?php include "footer.php"; ?>
