<!doctype html>
<!-- ENGLISH PART-->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav.php"; ?>

        <div class="container">
            <div class="row col-xs-12 col-sm-8 col-sm-offset-2 text-center">
                <h1>Our Services</h1>

            </div>
            <div class="row col-sm-12 ">
                <div class="col-xs-12 col-sm-6 servizi">
                  <!--<span class="glyphicon glyphicon-tree-deciduous"></span>-->
                    <img src="../img/albero.png" class="icona"/>
                    <h2>Farmhouse Il Caggio</h2>
                    <h3>
                        <p>Is located a few minutes far from Sovicille, surrounded by the Montagnola Senese green, among secular oaks and aromatic small bushes. Its wide enclosed park offers a big variety of vegetal species and has a special view with unique glimpses. </p>
                    </h3>
                </div>
                <div class="col-xs-12 col-sm-6 servizi">
                <!--    <span class="glyphicon glyphicon-home"></span>-->
                    <img src="../img/appartamenti.png" class="icona"/>
                    <h2>Apartments</h2>
                    <h3>
                        <p>Obtained from an ancient rural farm in an area once inhabited by the Etruscans, today the building is divided into seven comfortable apartments, completely restored according to our Tuscan taste and provided with any comfort for guests.</p>

                    </h3>
                </div>
                <div class="col-xs-12 col-sm-6 servizi">
                     <!--<span class="glyphicon glyphicon-tint"></span>-->
                    <img src="../img/piscina.png" class="icona" />
                    <h2>Swimming Pool</h2>
                    <h3>
                        <p>Il Caggio is the ideal place for nature and countryside excursions lovers, or simply for those loving a holiday in the sign of calm and rest, maybe relaxing in our equipped swimming pool.</p>
                    </h3>
                </div>
                <div class="col-xs-12 col-sm-6 servizi">
                    <!--<span class="glyphicon glyphicon-tower"></span>-->
                    <img src="../img/luoghi_di_interesse.png" class="icona"/>
                    <h2>Surroundings</h2>
                    <h3>
                        <p>Il Caggio is located in a central position in respect to the most famous artistic-cultural, wine and food places. Siena with its beauties and  Palio, San Gimignano, Monteriggioni and Montalcino are just a few minutes by car far. </p>

                    </h3>
                </div>
            </div>
        </div>
        <?php include "azienda.php"; ?>
        <?php include "virtualtour.php"; ?>    
             
        <?php include "footer.php"; ?>
