     
<footer id="footer">
    <div class="container">
        <div class="row text-center">
            <p>loc. Il Caggio, 21 53018 Sovicille, Siena -  <a href="tel:+390577345506">phone 0577/345506</a> - <a href="tel:+393393856489">mobile 339/3856489</a> - p.iva 00630860526 </p>
            <hr>
            <p>Crafted by <a href="http://www.glumcommunication.it" target="_new">GLuM Communication</a></p>
        </div>
    </div>
</footer>


<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="css/flick/jquery-ui-1.10.4.custom.css">

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script>
      $("document").ready(function() {
        $(".backhome").hide();
        $( function(){
        $(window).scroll(function () {
         if ($(this).scrollTop() > 150) {
              $('.backhome').fadeIn();
          } else {
              $('.backhome').fadeOut();
          }
      });
    });         
  });
    $("document").ready(function() {
        $('a.backhome').click(function() {
            $('html, body').animate(
                    {scrollTop: $("#header").offset().top - 0}, 1000, "easeInOutCirc");
        });
        $('a.appartamenti').click(function() {
            $('html, body').animate({
                scrollTop: $("#appartamenti").offset().top - 0}, 1000, "easeInOutCirc");
        });
        $('a.azienda').click(function() {
            $('html, body').animate({
                scrollTop: $("#azienda").offset().top - 0}, 1000, "easeInOutCirc");
        });
        $('a.gallery').click(function() {
            $('html, body').animate({
                scrollTop: $("#gallery").offset().top - 0}, 1000, "easeInOutCirc");
        });
        $('a.offerte').click(function() {
            $('html, body').animate({
                scrollTop: $("#offerte").offset().top - 0}, 1000, "easeInOutCirc");
        });
        $('a.contacts').click(function() {
            $('html, body').animate({
                scrollTop: $("#contacts").offset().top - 0}, 1000, "easeInOutCirc");
        });
    });


 $(window).load(function(){
      $('#carouselfienile').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 210,
        itemMargin: 5,
        asNavFor: '#sliderfienile'
      });

      $('#sliderfienile').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carouselfienile",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });

</script>
</body>
</html>