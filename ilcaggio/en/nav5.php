<div id="header">
  
    <div class="container">
        <div class="row" id="barralingue">
            <div class="col-xs-12 col-sm-12">
                <div id="languages">
                    <ul >
                        <li class="fb"><a href="https://www.facebook.com/IlCaggio.Siena" title="facebook" class="news">f</a></li>
                        <li><a href="index.php" title="Inglese" class="about">En</a></li>
                        <li><a href="../fr" title="Francese" class="servizi">Fr</a></li>
                        <li><a href="../index.php" title="Italiano" class="index">It</a></li>
                    </ul>
                  
                </div>
            </div>
        </div>
    </div>
    <div id="bigscreen3">
    <div class="container"id="sfondo1" >

        <div class="row">
            <div id="logo" class="col-xs-10  col-sm-4 col-md-4 ">
                <a href="/" id="logourl">                
                    <img  class="img-responsive" src="../img/logocaggio.png">
                </a>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-8">
                <nav class="navbar navbar-default navbar-right" role="navigation">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle"  data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    MENU
                        </button>
                    </div>
                    <div class="collapse navbar-collapse navbar-ex1-collapse">                                    
                        <ul class="nav navbar-nav">
                            <li <?php
                            if ($pageName == 'index') {
                                echo 'class="sel"';
                            }
                            ?>><a href="index.php" title="Home" class="index" style="padding-left:0px;padding-right: 0;"><img src="../img/home87.png" height="16px;" style="margin-right: 0;"></a></li>
                            
                            <li <?php
                            if ($pageName == 'azienda') {
                                echo 'class="sel"';
                            }
                            ?>><a href="index.php#azienda" title="Azienda" class="azienda">FarmHouse</a></li>
                            <li <?php
                            if ($pageName == 'appartamenti') {
                                echo 'class="sel"';
                            }
                            ?>><a href="index.php#appartamenti" title="Appartamenti" class="appartamenti">Apartments</a></li>
                             <li <?php
                            if ($pageName == 'gallery') {
                                echo 'class="sel"';
                            }
                            ?>><a href="gallery.php" title="Gallery" class="gallery">Gallery</a></li>
                              <li <?php
                            if ($pageName == 'contacts') {
                                echo 'class="sel"';
                            }
                            ?>><a href="prezzi.php" title="Contatti" class="prezzi">Prices</a></li>
                            <li <?php
                            if ($pageName == 'contacts') {
                                echo 'class="sel"';
                            }
                            ?>><a href="contacts.php" title="Contatti" class="contacts" style="padding-right: 0;">Contacts</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>

       
                  

    </div>
    </div>
</div>