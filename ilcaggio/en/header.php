<!-- ENGLISH PART-->
<head>
    <meta charset="utf-8">
    <title>Farmhouse Il Caggio - Sovicille</title>
    <meta name="description" content="Beauty farm next to Siena, Sovicille, Colli Senesi landscape, weekend, holidays, sun and nature.">
    <meta name="keywords" content="farm green bio nature tipical sienna tuscany il caggio sovicille">
    <meta name="viewport" content="width=device-width">
    <!-- JAVASCRIPT-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
   <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="../js/ekko-lightbox.min.js"></script>
      <link href="../css/ekko-lightbox.min.css" rel="stylesheet">
    <link href="../css/dark.css" rel="stylesheet">

    <script src="../js/modernizr.min.js"></script>
    <script src="../js/jquery.flexslider.js"></script>

    <!-- STILI CSS-->


    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/flick/jquery-ui.css">    
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/flexslider.css">

    <script>
        $('.carousel .item').each(function() {
            var next = $(this).next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            next.find('.item-content:first-child').clone().appendTo($(this));
        });

    </script>
 <script type="text/javascript">
            $(document).ready(function ($) {
				$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
					event.preventDefault();
					return $(this).ekkoLightbox();
				});
			});
        </script>

    <!-- SCRIPT -->
</head>