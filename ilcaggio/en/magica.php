<div class="grigia">
    <div class="container ">

        <div class="row">
            <div class="col-xs-12 col-sm-10">
                <h1>Come to live a magical experience</h1>
                <h4><p>Siena countryside will enchant you in this area. Its history began during the Etruscan period with the first villages. Wonderful nature, relax, sun and our swimming pool are ideal when you want you to indulge yourselves.</p></h4>

            </div>
            <div class="col-xs-12 col-sm-2" id="magica">
                <a  href="prenota.php" class="btn btn-default col-xs-12 " id="prenota3">BOOK</a>
            </div>
        </div>
    </div>
</div>