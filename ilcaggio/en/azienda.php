<div class="grigia">
    <div class="container " id="azienda">
        <div class="row">

            <div class="col-xs-12 ">
                <h1>The farmhouse</h1>
            </div>
            <div class="col-xs-12 ">
                <img src="../img/sfondo2.jpg" class="img-responsive"/>
            </div>
            <div class="col-xs-12 col-sm-12 text-center" >

                <p>Choosing a farmhouse does not mean just choosing an accommodation but looking for a precise life-style. Living nature also means you could see with your own eyes its fruits, appreciating its gifts. Such as the products Il Caggio prepares with love and care, using the best raw materials.

                </p>
            </div>
            <div class="text-center col-xs-12 col-sm-2 col-sm-offset-5">
                <a  href="aziendaagricola.php" class="btn btn-default " id="scopri_btn">DISCOVER</a>

            </div>
        </div>
    </div></div>
<?php include "appartamenti.php"; ?>

<?php include "magica.php"; ?>
