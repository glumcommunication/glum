<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav.php"; ?>

        <div class="container">
            <p>Terms and Conditions</p>

            <p>To confirm your reservation, you must submit, within 7 working days, a deposit equal to about 30% of the total. Once you have made ​​the payment (by bank transfer or other means to be agreed), please send us by email a copy of receipt of payment.</p>

            <p>If the reservation is canceled at least 21 days prior to arrival, guests will be refunded 100% of the deposit.</p>

            <p>In case of cancellation between 20 and 11 days prior to arrival, the guest will be refunded 50% of the deposit.</p>

            <p>In the event of a cancellation notice less than or equal to 10 days prior to arrival, the guest will not be guaranteed a refund.</p>

            <p>Finally, in case of no-show (no show without notice) the guest will be charged the full cost of the stay.</p>

            <p>&nbsp;</p>

            <p>Tourist tax</p>

            <p>&nbsp; The tourist tax, which was adopted by the City of Sovicille with the deliberations of the City Council of 06.02.2013 and No. 8 of the Municipal Council of 18.03.2013 No. 20 and No. 30 of 15.04.2013 is in force since 1 May 2013.</p>

            <p>The tax is set up to finance interventions in the field of tourism, including those in support of facilities and maintenance, use and recovery of cultural heritage and local environmental and related local public services.</p>

            <p>WHO PAYS THE TAX? Those staying in one of the accommodation facilities in the municipal area, paying the tax to the operator of the facility issuing the receipt.</p>

            <p>HOW MUCH? For cottages and extra-hotel facilities, the tax is &euro; 1.50. The tax is payable per person per night up to a maximum of six nights, although not continuous, in the calendar month in the same facility.</p>

            <p>The tax is applied up to a maximum of six nights per person, although not continuous, in any calendar month and in the same structure. This limit also applies in the case of six nights of continuous riding two months, in the same structure.</p>

            <p>The tax does not apply to record attendance for at least 12 consecutive nights, per person and in the same structure, both during the month that straddles two months.</p>

            <p>For more information: http://www.comune.sovicille.si.it/Main.aspx?ID=601</p>

            <p>&nbsp;</p>



        </div>



        <?php include "footer.php"; ?>
