<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav2.php"; ?>

        <div class="container">

            <div class="row col-sm-12 ">
                <div class="col-xs-12 col-sm-3 ">
                  <!--<span class="glyphicon glyphicon-tree-deciduous"></span>-->
                    <img src="../img/colazione-3.jpg" class="img-responsive thumb"/>
                    <img src="../img/colazione3.jpg" class="img-responsive thumb"/>
                    <img src="../img/prodotti-3.jpg" class="img-responsive thumb"/>

                </div>
                <div class="col-xs-12 col-sm-9">
                    <p>Quand on choise une ferme on ne va pas choisir seulement une chambre mais aussie une style de vie. Si pour quelques jours ou longtemps n&rsquo;est pas important. Pour cette raison nous faisons tout le possible pour vous donner un s&eacute;jour plein d&rsquo;exp&eacute;riences sp&eacute;ciales, uniques et simples. En suivant le rythme de la nature, en appr&eacute;ciant ses cadeaux et les efforts pour les comprendre. D&eacute;puis beacoup d&rsquo;ann&eacute;es, Il Caggio produit avec passion et attention pour ses clients. Entre nos produits on peut go&ucirc;ter:</p>

                    <p>Notre huile extra vierge, directement des oliviers de nos terrains,obtenu par pressage &agrave; froid</p>

                    <p>Nos confitures et conserves, rigoureusement pr&eacute;par&eacute;es avec les fruits et l&eacute;gumes nous cultivons: pruneaux, figues, coings, cerises, tomates, citrouille et autres.</p>

                    <p>Nos marrons et la farine avec laquelle on pr&eacute;pare des gateaux et autres sp&eacute;cialit&eacute;s.</p>

                    <p>De plus, vous pouvez acheter ces produits et autres dans la structure o&ugrave; vous pourrais les go&ucirc;ter pendant les d&eacute;gustations et d&icirc;ners typiques organiz&eacute;es dans la ferme.</p>

                </div>


            </div>
        </div>
        <?php include "magica.php" ?>
        <?php include "footer.php"; ?>
