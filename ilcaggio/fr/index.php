<!doctype html>
<!-- ENGLISH PART-->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav.php"; ?>

        <div class="container">
            <div class="row col-xs-12 col-sm-8 col-sm-offset-2 text-center">
                <h1>Nos Services</h1>

            </div>
            <div class="row col-sm-12 ">
                <div class="col-xs-12 col-sm-6 servizi">
                  <!--<span class="glyphicon glyphicon-tree-deciduous"></span>-->
                    <img src="../img/albero.png" class="icona"/>
                    <h2>Ferme Il Caggio</h2>
                    <h3>
                        <p>La Ferme il Caggio est situ&eacute;e &agrave; quelques minutes de Sovicille, dans le vert de la Montagnola Senese, entre les ch&ecirc;nes s&eacute;culaires et les arbustes aromatiques. Le grand parc clos offre une grande variet&eacute; d&rsquo;esp&egrave;ces v&ecirc;g&eacute;tales et a une vue magnifique, avec des aperҫus uniques.</p>

                    </h3>
                </div>
                <div class="col-xs-12 col-sm-6 servizi">
                <!--    <span class="glyphicon glyphicon-home"></span>-->
                    <img src="../img/appartamenti.png" class="icona"/>
                    <h2>Appartements</h2>
                    <h3>
                        <p>L&rsquo;&eacute;difice, qui vient d&rsquo; un ancien ferme rural dans une zone une fois habit&eacute;e des &Eacute;trusques, est divis&eacute; en sept appartements tr&egrave;s confortable, enti&egrave;rement restaur&eacute;s ҫelon le saveur du temps et avec tous les conforts pour nos h&ocirc;tes.</p>

                    </h3>
                </div>
            </div>
            <div class="row col-sm-12">
                <div class="col-xs-12 col-sm-6 servizi">
                     <!--<span class="glyphicon glyphicon-tint"></span>-->
                    <img src="../img/piscina.png" class="icona" />
                    <h2>Piscine</h2>
                    <h3>
                        <p>Il Caggio est un coin id&eacute;al pour ceux qui aiment la nature ou les excursiones dans la campagne, ou simplement pour cuex qui veulent passer une vacance relaxante, aussi en utilisant notre merveilleuse piscine &eacute;quip&eacute;e.</p>


                    </h3>
                </div>
                <div class="col-xs-12 col-sm-6 servizi">
                    <!--<span class="glyphicon glyphicon-tower"></span>-->
                    <img src="../img/luoghi_di_interesse.png" class="icona"/>
                    <h2>Environs</h2>
                    <h3>
                       Il Caggio est situé dans une position central par rapport à les plus importantes lieux artistiques-culturels et eno-gastronomiques. Siena avec ses beautés et son Palio, San Gimignano, Monteriggioni et Montalcino sont à pas beaucoup de temps en voiture.

                    </h3>
                </div>
            </div>
        </div>
        <?php include "azienda.php"; ?>
        <?php include "virtualtour.php"; ?>    
         
        <?php include "footer.php"; ?>
