<?php
if ($_POST['doSend'] == 'Invia') {
    $name = $_POST[inputName];
    $surname = $_POST[inputCognome];
    $email = $_POST[inputEmail];
    $subject = "Prenotazioni da Ilcaggio.it";
    $privacy = $_POST[privacy];
    $animali = $_POST[animali];
    $culla = $_POST[culla];
    $letto = $_POST[letto];
    $persone = $_POST[persone];
    $checkin = $_POST['datepicker'];
    $checkout = $_POST['datepicker2'];
    $camere = $_POST[camere];
    $sesso = $_POST[sesso];
    $headers .= "From: $email\n";
    $headers .= "Reply-To: $email\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=utf-8\n";
    $to = "francesco@glumcommunication.it";
    /* $message = $messaggio; */
    $messagebody = "Hai ricevuto una nuova email via ilcaggio.it, ecco i dettagli della prenotazione:<br />
        
Nome: $sesso $name $surname <br />

E-mail: $email<br />
    
Telefono: $tel<br />

Nei giorni: dal $checkin al $checkout<br/>
Numero persone: $persone Numero Camere Richieste $camere<br/>

Animali: $animali<br/>
Culla Aggiuntiva: $culla<br/>
Letto Aggiuntivo: $letto<br/>
Privacy: $privacy<br/>    

Messaggio: $message<br />";

    mail($to, $subject, $messagebody, $headers);
    ?>
    <div class="col-sm-12" >       
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <h4>Message Envoye!!</h4>
            nous reviendrons d&egrave;s que possible.
            Merci, Agriturismo il Caggio
        </div>
        <?php
    }
    ?>

    <!doctype html>
    <!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
    <![endif]-->
    <!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
    <![endif]-->
    <!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
        <?php include "header.php"; ?>
        <script src="../ckeditor/ckeditor.js"></script>
        <style>

            /* Style the CKEditor element to look like a textfield */
            .cke_textarea_inline
            {
                padding: 10px;
                height: 200px;
                overflow: auto;

                border: 1px solid gray;
                -webkit-appearance: textfield;
            }

        </style>

        <script src="../js/datepicker-fr.js"></script>
        <script src="../js/datepicker-it.js"></script>
        <script>
            $(function() {
                $("#datepicker").datepicker($.datepicker.regional["fr"]);
                $("#datepicker2").datepicker($.datepicker.regional["fr"]);
            });
        </script>

        <body>
            <?php include "nav5.php"; ?>
            <div class="container">
                <form method="post" action="prenota.php" name="formAction" id="prenotaForm">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6" id="colsx">
                            <h1>Votre Reservation:</h1>                    
                            <div class="row campitesto ">
                                <div class="form-group">
                                    <div class="col-sm-3 text-left">
                                        <label for="dataInizio" >CheckIn</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" type="text" class="form-control" id="datepicker" name="dataInizio">
                                    </div>
                                </div>  
                            </div>
                            <div class="row campitesto">
                                <div class="form-group">
                                    <div class="col-sm-3 text-left">
                                        <label for="dataFine"  >CheckOut</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" type="text" class="form-control" id="datepicker2" name="dataFine">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group selection">
                                <label for="stato">Personnes
                                    <select  class="form-control " id="persone">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                        <option>9</option>
                                        <option>10</option>
                                    </select></label>

                                <label for="stato">Chambres
                                    <select  class="form-control " id="camere">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                        <option>9</option>
                                        <option>10</option>

                                    </select></label>
                            </div> 
                            <div>
                                <h1>Services Supplementaires</h1>

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="animali" id="animali" > Animaux
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="culla" id="culla"> Lit d&apos;Enfant
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="letto" id="letto"> Lit Supplementaire
                                    </label>
                                </div>


                            </div>



                        </div>


                        <div class="col-xs-12 col-sm-6">
                            <h1>Renseignements personnels</h1>
                            <div class="row text-left" id="sesso">
                                <div class="form-group ">
                                    <label for="sesso">M.</label>
                                    <input type="radio" name="sesso" id="sesso" value="sig" checked="checked" class="radio-inline">                            
                                    <label for="sesso">Mme</label>
                                    <input type="radio" name="sesso" id="sesso" value="sigra" class="radio-inline">                            
                                </div> 
                            </div>

                            <div class="row campitesto">
                                <div class="form-group">
                                    <div class="col-sm-3 text-left">
                                        <label for="inputName">Nome</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input required type="text" class="form-control anagrafe " id="inputName"  name="inputName">
                                    </div>
                                </div>
                            </div>

                            <div class="row campitesto ">
                                <div class="form-group">
                                    <div class="col-sm-3 text-left">
                                        <label for="inputCognome" >nom de famille</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input required class="form-control anagrafe"  type="text" id="inputCognome" name="inputCognome">
                                    </div>
                                </div>  
                            </div>

                            <div class="row campitesto">
                                <div class="form-group">
                                    <div class="col-sm-3 text-left">
                                        <label for="inputPhone"  >Telephone</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input  class="form-control anagrafe" type="text" id="inputPhone" name="inputPhone"  >
                                    </div>
                                </div>
                            </div>

                            <div class="row campitesto">
                                <div class="form-group">
                                    <div class="col-sm-3 text-left">
                                        <label for="inputEmail" >Email</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input  required email class="form-control  anagrafe " type="email" id="inputEmail" name="inputEmail" >
                                    </div>
                                </div>
                            </div>
                            <div class="row campitesto">
                                <div class="form-group">
                                    <div class="col-sm-3 text-left">
                                        <label for="Message" >Note:</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <textarea class=" form-control  anagrafe " type="text" id="Message" name="Message" ></textarea>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>


                    <div class="row">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" required name="privacy" id="privacy"> J&apos;ai lu le <a rel="tooltip" data-toggle="tooltip" title="Sending my information I declare that i have read the informativeabout privacy and that I know thatthe treatments of my data is necessary for the proposed service. For this, I declare to be an adult and I give my consent." href="http://www.garanteprivacy.it/web/guest/home/docweb/-/docweb-display/docweb/1311248" data-placement="right" style="font-size:14px;cursor: help;"> Loi sur la protection des renseignements personnels</a>
                                et j&apos;accepte les <a href="condizionigenerali.php" target="_new">termes et conditions</a>
                            </label>
                        </div>
                    </div>
                    <button  type="submit" id="submit" name="doSend" class="btn btn-default col-xs-12  col-sm-2 text-right" value="Invia" style="float:right;">BOOK</button>
                </form>


                <div class="row clear">
                    <div class="col-sm-12 text-left">
                        <p>La grande ferme est la partie avec plus d&rsquo;appartements, les autres sont situ&eacute;s dans les typiques cabanes toscanes restaur&eacute;es. &ldquo;La parata&rdquo; de Il Caggio est disponible pour ses clients: une large s&eacute;jour avec cuisine, la solution id&eacute;ale pour ceux qui veulent passer du temps ensemble.</p>

                        <p>Les prix sont par appartement.</p>

                        <p>L&rsquo;enregistrement est de 3 &agrave; 7 heures. Dans le cas de retard, nous vous prions gentilment d&rsquo;informer notre personnel.</p>

                        <p>D&egrave;part de l&rsquo;hotel &agrave; 10 heures.</p>
                    </div>
                </div>
                <?php include "tabellaprezzi.php"; ?>

            </div>
            <script>
                CKEDITOR.inline('Message');
            </script>
            <script>
                $(document).ready(function() {

                    // validate signup form on keyup and submit
                    var validator = $("#prenotaForm").validate({
                    rules: {
                    name: "required",
                            tel: {required: true,
                                    number: true
                            },
                            privacy: "required",
                            email: {
                            required: true,
                                    minlength: 7,
                                    email: true
                            }
                    },
                            messages: {
                            name: "Champ Obligatoire! - Entrer vous nome",
                                    tel: {required: "Entrez votre telephone ",
                                            number: "Uniquement des chiffres",
                                            minlenght: "Entrez un numero valide."},
                                    privacy: "Acceptation obligatoire<br> ",
                                    email: {
                                    required: "Champ Obligatoire! - Entrez vous email",
                                            minlength: "Entrez une email valide",
                                            email: "Entrez une email valide."
                                    }



                            });
                });
            </script>
            <?php include "footer.php"; ?>
