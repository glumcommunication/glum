<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav4.php"; ?>

        <div class="senzamenu container" id="main" role="main">
            <section class="slider">
                <div id="sliderfienile" class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="../img/le-querce.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-querce2.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-querce3.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-querce4.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-querce5.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-querce-6.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-querce-7.jpg" />
                        </li>

                    </ul>
                </div>
                
            </section>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Le Querce</h1>
                    <p>Cet appartement est situ&eacute; sur la grande terrasse de la ferme o&ugrave; on peut admirer les grandioses ch&ecirc;nes seculaires qui s&rsquo;&eacute;levent sur la piscine. En hiver, dans la salle &agrave; manger vous pourraient vous &eacute;chauffer avec l&rsquo;ancienne po&ecirc;le &agrave; bois en fonte.</p>

                    <p><strong>Composition:</strong></p>

                    <p>Salle &agrave; manger avec kitchenette, Large chambre double, S&eacute;jour avec canap&eacute;-lit, Salle de bains avec douche, espace externe priv&eacute; &eacute;quip&eacute; de table, chaises et ombrelle</p>





                </div>
            </div>
        </div>
        <?php include "magica.php" ?>
        <?php include "appartamenti.php"; ?>
        <?php include "footer.php"; ?>
