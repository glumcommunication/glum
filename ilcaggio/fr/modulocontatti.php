<?php
if ($_POST['doSend'] == 'Invia') {
    $name = $_POST[name];
    $email = $_POST[email];
    $subject = $_POST[subject];
    $tel = $_POST[tel];
    $message = $_POST[message];
    $headers = "";
    $headers .= "From: $email\n";
    $headers .= "Reply-To: $email\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=utf-8\n";
    $to = "francesco@glumcommunication.it";
    /* $message = $messaggio; */
    $messagebody = "Hai ricevuto una nuova email via IlCaggio.it, ecco i dettagli della prenotazione:<br />
        
Nome: $name <br />

E-mail: $email<br />
    
Telefono: $tel<br />

Oggetto: $subject<br>

Messaggio: $message<br />";
    mail($to, $subject, $messagebody, $headers);
    ?>
    <script>
        document.("#inviata").style.display = "inline";

    </script>
    <?php
}
?>

    <div class="container  text-center" id="contacts">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1>Contactez Nous</h1>

        </div>
        <div id="inviata" style="display: none;"class="col-sm-12" >       
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <h4>Message Envoye!!</h4>
                nous reviendrons d&egrave;s que possible.
                Merci
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 text-left">

                <p>L&rsquo;emplacement de la ferme &ldquo;Il Caggio&rdquo; vous surprendra. Se d&eacute;placer et visiter la Toscane ser&agrave; tr&egrave;s facil.</p>

                <p>Nous sommes &agrave; 15 minutes de Sienne, 50 minutes de la suggestive San Gimignano, environ une heure de Montalcino et Pienza, 40 minutes de la spa de Rapolano, une heure de la mer et 75 minutes de Florence.</p>

                <p><strong>Coordonn&eacute;es GPS</strong></p>

                <p>Lat/Long: (43.28770446224768, 11.208667159080505)</p>

                <p>&nbsp;</p>

                <p>L&rsquo;enregistrement est de 3 &agrave; 7 heures. Dans le cas de retard, nous vous prions gentilment d&rsquo;informer notre personnel.</p>

                <p>D&egrave;part de l&rsquo;hotel &agrave; 10 heures.</p>

                <p>La ferme est equip&eacute;e de place de parking gratuit non gard&eacute;. Les animals domestiques sont admis.</p>

                <p>&nbsp;</p>

                <p>Notre courrier &eacute;lectronique: <a href="mailto:ilcaggio@interfree.it">ilcaggio@interfree.it</a></p>

                <p>T&eacute;l&egrave;phone: +39 0577/345506</p>

                <p>Fax: +39 0577/345506</p>

            </div>
            <div class="col-xs-12 col-sm-6 ">



                <form action="index.php#contacts" method="post" id="contatti">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Entrer votre nom">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Entrer votre  email">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="tel" name="tel" placeholder="entrer votre telephone">
                            </div>
                        </div>
                    </div>    
                    <div class="form-group">
                        <input type="text" class="form-control" id="subject" name="subject" value="objet">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" rows="7" id="message" name="message"></textarea>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" value="Yep" name="privacy">
                            <a id="privacy" href="#" rel="tooltip" data-toggle="tooltip" title="Envoyer mes donn&eacute;es J&#39;ai lu l&#39;avertissement et sont conscients que leur traitement est n&eacute;cessaire pour obtenir le service propos&eacute;. &Agrave; cette fin, dans l&#39;&eacute;tat d&#39;&ecirc;tre un adulte, je donne mon consentement." data-placement="right" style="font-size:14px;cursor: help;">
                                Informativa sulla Privacy ai sensi del D.Lgs 196/2003</a>
                        </label>
                    </div>
                    <div class="text-right">
                        <button id="posta" type="submit" class="btn btn-default text-right" name="doSend" value="Invia">Envoyer</button>
                    </div>
            </div>


        </div>

    </div>

<script src="js/modernizr.custom.js"></script>

<script type="text/javascript">
        $(document).ready(function() {
            if (Modernizr.touch) {
                $("figButton").click(function() {
                    $(this).toggleClass("cap-left");
                });
                $("figClose").click(function() {
                    $(this).toggleClass("cap-left");
                });
            }
            ;
        })

</script>
<script>
    $(document).ready(function() {

        // validate signup form on keyup and submit
        var validator = $("#contatti").validate({
            rules: {
                name: "required",
                tel: {required: true,
                    number: true,
                    minlenght: 9
                },
                privacy: "required",
                email: {
                    required: true,
                    minlength: 7,
                    email: true
                }
            },
            messages: {
                name: "Champ Obligatoire! - Entrer vous nome",
                tel: {required: "Entrez votre telephone ",
                    number: "Uniquement des chiffres",
                    minlenght: "Entrez un numero valide."},
                privacy: "Acceptation obligatoire<br> ",
                email: {
                    required: "Champ Obligatoire! - Entrez vous email",
                    minlength: "Entrez une email valide",
                    email: "Entrez une email valide."
                }

            }

        });


    });
</script>