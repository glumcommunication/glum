<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav4.php"; ?>

        <div class="senzamenu container" id="main" role="main">
            <section class="slider">
                <div id="sliderfienile" class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="../img/gli-olivi.jpg" />
                        </li>

                        <li>
                            <img src="../img/gli-olivi-3.jpg" />
                        </li>
                        <li>
                            <img src="../img/gli-olivi-4.jpg" />
                        </li>
                        <li>
                            <img src="../img/gli-olivi-5.jpg" />
                        </li>
                          <li>
                              <img src="../img/gli-olivi9.jpg" />
                        </li>
                          <li>
                              <img src="../img/gli-olivi10.jpg" />
                        </li>
                        


                    </ul>
                </div>
                
            </section>

        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Gli Olivi</h1><p>L&rsquo;appartement &ldquo;Gli Olivi&rdquo; se souleve directement sur le grand oliveraie (l&rsquo;origine de son nom) en terrasses. Nos h&ocirc;tes pourrons rester en priv&eacute; gr&acirc;ce &agrave; l&rsquo;entr&eacute;e ind&eacute;pendante et &agrave; la petite terrace priv&eacute;e.</p>

                    <p>&nbsp;</p>

                    <p><strong>Composition:</strong></p>

                    <p>Salle &agrave; manger avec kitchenette, Large chambre double, Salle de bains avec douche, espace externe priv&eacute; &eacute;quip&eacute; de table, chaises et ombrelle</p>

                </div>
            </div>
        </div>
        <?php include "magica.php" ?>
        <?php include "appartamenti.php"; ?>
        <?php include "footer.php"; ?>
