<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav4.php"; ?>

        <div class="senzamenu container" id="main" role="main">
            <section class="slider">
                <div id="sliderfienile" class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="../img/il-picchio.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-picchio2.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-picchio3.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-picchio5.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-picchio-6.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-picchio-7.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-picchio-8.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-picchio-9.jpg" />
                        </li>


                    </ul>
                </div>
                
            </section>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Il Picchio </h1>
                    <p>&ldquo;Il Picchio&rdquo; est l&rsquo;appartement le plus spacieux de la ferme. Le plafond est le protagoniste absolu: ses structures en bois impressionants sont n&eacute;es depuis beaucoup de si&egrave;cles et ils ne peuvent pas &ecirc;tre inaperҫus. L&rsquo;&eacute;ntr&eacute;e est situ&eacute;e dans une petite terrasse priv&eacute;e, id&eacute;al pour se relaxer et se faire bronzer.</p>

                    <p><strong>Composition:</strong></p>

                    <p>Salle &agrave; manger avec kitchenette, Large chambre double, S&eacute;jour avec canap&eacute;-lit, Salle de bains avec douche, espace priv&eacute; &eacute;quip&eacute; de table, chaises et ombrelle</p>





                </div>
            </div>
        </div>
        <?php include "magica.php" ?>
        <?php include "appartamenti.php"; ?>
        <?php include "footer.php"; ?>
