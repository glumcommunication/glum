<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav4.php"; ?>

        <div class="senzamenu container" id="main" role="main">
            <section class="slider">
                <div id="sliderfienile" class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="../img/il-granaio.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-granaio2.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-granaio3.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-granaio5.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-granaio6.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-granaio7.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-granaio8.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-granaio9.jpg" />
                        </li>


                    </ul>
                </div>
                
            </section>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Il Granaio</h1>
                    <p>Le temps n&rsquo;a chang&eacute; pas le nom et l&rsquo;identit&eacute; de cet appartement; le r&ecirc;colte de bl&eacute; et orge &eacute;taient mis ici dans le temps. Bien restaur&eacute;, &ldquo;Il Granaio&rdquo; est situ&eacute; dans la plus haute part de la ferme o&ugrave; on peut voir une vue merveilleuse de l&rsquo;oliveraie.</p>

                    <p><strong>Composition:</strong></p>

                    <p>Salle &agrave; manger avec kitchenette, Large chambre double, Salle de bains avec douche, espace externe priv&eacute; &eacute;quip&eacute; de table, chaises et ombrelle</p>









                </div>
            </div>
        </div>
        <?php include "magica.php" ?>
        <?php include "appartamenti.php"; ?>
        <?php include "footer.php"; ?>
