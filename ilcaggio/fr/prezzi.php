<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>
        <?php include "nav4.php"; ?>
        <div class="container">
            <div class="row col-xs-12 col-sm-8 col-sm-offset-2 text-center">
                <h1>Prix</h1>
            </div>
            <?php include "tabellaprezzi.php"; ?>
        </div>
        <style>
            #footer{
            
                width: 100%;
                clear:both;
            }
        </style>
        <?php include "footer.php"; ?>
    
