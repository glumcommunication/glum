<div class="grigia">
    <div class="container " id="azienda">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h1>La Ferme</h1>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <img src="../img/sfondo2.jpg" class="img-responsive"/>
            </div>
            <div class="col-xs-12 col-sm-12 text-center" >

                <p>Quand on choise une ferme on ne veut pas seulement une chambre mais une pr&eacute;cis style de vie. Vivre la nature signifie aussie pouvoir voir ses fruits, en reconnaissant ses cadeaux. Comme tous les produits que le ferme r&eacute;alise avec amour et attention en utilisant les mati&egrave;res premi&egrave;res meilleures.</p>

                <p>Vien pour vivre une experience merveilleuse</p>

                <p>La campagne de Sienne vous enchantera. Son histoire commence avec les &Eacute;trusques et ses premiers villages. Nature merveilleuse, relax, soleil et notre piscine sont id&eacute;als quand on veut se d&eacute;tendre.</p>

            </div>
            <div class="text-center col-xs-12 col-sm-2 col-sm-offset-5">
                <a  href="aziendaagricola.php" class="btn btn-default " id="scopri_btn">DISCOVER</a>

            </div>
        </div>
    </div></div>
<?php include "appartamenti.php"; ?>

<?php include "magica.php"; ?>
