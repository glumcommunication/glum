<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav4.php"; ?>

        <div class="senzamenu container" id="main" role="main">
            <section class="slider">
                <div id="sliderfienile" class="flexslider">
                    <ul class="slides">
                        <li>
                            <img src="../img/il-fienile.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-fienile2.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-fienile3.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-fienile4.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-fienile5.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-fienile6.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-fienile7.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-fienile8.jpg" />
                        </li>
                        <li>
                            <img src="../img/il-fienile9.jpg" />
                        </li>

                    </ul>
                </div>
                
            </section>

        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Il Fienile</h1>

                    <p>Obtenu d&rsquo; une cabane utilis&eacute;e pour le foin en hiver, &ldquo;Il Fienile&rdquo; est tr&egrave;s originale, bien restaur&eacute; et compl&egrave;tement ind&eacute;pendant de la ferme; &agrave; l&rsquo;ext&eacute;rieure, nos h&ocirc;tes peuvent appr&eacute;cier un &eacute;space vert r&eacute;serv&eacute; seulement &agrave; l&rsquo;appartement.</p>

                    <p><strong>Composition:</strong></p>

                    <p>Salle &agrave; manger avec kitchenette et po&ecirc;le &agrave; bois, Charmant chambre double, Loft utilis&eacute; comme chambre double (&agrave; un lit/&agrave; deux lits), S&eacute;jour avec canap&eacute;-lit, s&eacute;jour avec coin cuisine, Deux Salles de bains avec baignoire, espace externe priv&eacute; &eacute;quip&eacute; de table, chaises et ombrelle</p>


                </div>
            </div>
        </div>
        <?php include "magica.php" ?>
        <?php include "appartamenti.php"; ?>
        <?php include "footer.php"; ?>
