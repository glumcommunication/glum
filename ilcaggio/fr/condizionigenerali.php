<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav.php"; ?>

        <div class="container">
            <p>Termes et conditions</p>

            <p>Pour confirmer votre r&eacute;servation, vous devez soumettre, dans un d&eacute;lai de 7 jours ouvrables, un acompte &eacute;gal &agrave; 30% du total. Une fois que vous avez effectu&eacute; le paiement (par virement bancaire ou d&#39;autres moyens &agrave; convenir), s&#39;il vous pla&icirc;t envoyez-nous par e-mail une copie du re&ccedil;u de paiement.</p>

            <p>Si la r&eacute;servation est annul&eacute;e au moins 21 jours avant la date d&#39;arriv&eacute;e, les clients seront rembours&eacute;s &agrave; 100% de l&#39;acompte.</p>

            <p>En cas d&#39;annulation entre 20 et 11 jours avant l&#39;arriv&eacute;e, le client sera rembours&eacute; 50% de l&#39;acompte.</p>

            <p>Dans le cas d&#39;un pr&eacute;avis de moins de ou &eacute;gal &agrave; 10 jours avant l&#39;arriv&eacute;e annulation, le client ne sera pas garanti un remboursement.</p>

            <p>Enfin, en cas de non-pr&eacute;sentation (no show sans pr&eacute;avis), le client sera factur&eacute; le co&ucirc;t total du s&eacute;jour.</p>

            <p>&nbsp;</p>

            <p>Taxe de s&eacute;jour</p>

            <p>La taxe de s&eacute;jour, qui a &eacute;t&eacute; adopt&eacute; par la Ville de Sovicille avec les d&eacute;lib&eacute;rations du Conseil municipal de 02/06/2013 et n &deg; 8 du conseil municipal de 18/03/2013 n &deg; 20 et n &deg; 30 du 15.04.2013 est en vigueur depuis le 1er mai 2013.</p>

            <p>La taxe est mis en place pour financer les interventions dans le domaine du tourisme, y compris ceux &agrave; l&#39;appui des installations et de l&#39;entretien, l&#39;utilisation et la r&eacute;cup&eacute;ration du patrimoine culturel et les services publics locaux de l&#39;environnement et connexes locaux.</p>

            <p>Qui paie la taxe? Ceux qui se trouvent dans l&#39;un des &eacute;tablissements d&#39;h&eacute;bergement dans le secteur municipal, de payer la taxe &agrave; l&#39;exploitant de l&#39;&eacute;tablissement qui d&eacute;livre le re&ccedil;u.</p>

            <p>Combien? Pour les chalets et les installations extra-h&ocirc;teli&egrave;res, la taxe est de 1,50 &euro;. La taxe est payable par personne et par nuit pour un maximum de six nuits, mais pas en continu, dans le mois civil dans le m&ecirc;me &eacute;tablissement.</p>

            <p>La taxe est appliqu&eacute;e jusqu&#39;&agrave; un maximum de six nuits par personne, mais pas en continu, dans un mois civil et dans la m&ecirc;me structure. Cette limite s&#39;applique &eacute;galement dans le cas de six nuits de circonscription continue de deux mois, dans la m&ecirc;me structure.</p>

            <p>La taxe ne s&#39;applique pas &agrave; une affluence record pour au moins 12 nuits cons&eacute;cutives, par personne et dans la m&ecirc;me structure, &agrave; la fois au cours du mois qui chevauche deux mois.</p>

            <p>Pour plus d&#39;informations: http://www.comune.sovicille.si.it/Main.aspx?ID=601</p>



        </div>



        <?php include "footer.php"; ?>
