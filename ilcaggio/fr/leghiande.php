<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav4.php"; ?>

        <div class="senzamenu container" id="main" role="main">
            <section class="slider">
                <div id="sliderfienile" class="flexslider">
                    <ul class="slides">
                             <li>
                            <img src="../img/le-ghiande.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-ghiande2.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-ghiande3.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-ghiande4.jpg" />
                        </li>
                     <!--  <li>
                            <img src="../img/le-ghiande5.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-ghiande6.jpg" />
                        </li>
                     -->
                        <li>
                            <img src="../img/le-ghiande-7.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-ghiande-8.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-ghiande-9.jpg" />
                        </li>
                        <li>
                            <img src="../img/le-ghiande-10.jpg" />
                        </li>


                    </ul>
                </div>
                
            </section>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <h1>Le Ghiande</h1>
                    <p>Le Ghiande est l&rsquo;appartement situ&eacute; dans la parte la plus haute de la ferme ancienne, pour cette raison c&rsquo;est le plus lumineux et panoramique: de la fen&ecirc;tre on peut admirer la ferme et environs.</p>

                    <p><strong>Composition:</strong></p>

                    <p>Salle &agrave; manger avec kitchenette, Deux larges chambres doubles, Deux salles de bains avec douche, espace externe  priv&eacute; &eacute;quip&eacute; de table, chaises et ombrelle</p>





                </div>
            </div>
        </div>
        <?php include "magica.php" ?>
        <?php include "appartamenti.php"; ?>
        <?php include "footer.php"; ?>
