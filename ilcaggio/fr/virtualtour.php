<div class="container row text-center" id="tour">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h1>Visite virtuelle</h1>
    </div>

</div>
<div id="boxvideo" class="container loading">
    <div class="rowcol-xs-12">
 <section class="slider ">
        <div class="flexslider">
          <ul class="slides">
            <li>
              <iframe id="player_1" src="http://player.vimeo.com/video/98114420?api=1&amp;player_id=player_1" width="500" height="281" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
  	    		</li>
          </ul>
        </div>
      </section>
    </div>
</div>

  <!-- FlexSlider -->
  <script defer src="js/jquery.flexslider.js"></script>

  <script type="text/javascript">
 
    $(window).load(function(){

      // Vimeo API nonsense
      var player = document.getElementById('player_1');
      $f(player).addEvent('ready', ready);

      function addEvent(element, eventName, callback) {
        (element.addEventListener) ? element.addEventListener(eventName, callback, false) : element.attachEvent(eventName, callback, false);
      }

      function ready(player_id) {
        var froogaloop = $f(player_id);

        froogaloop.addEvent('play', function(data) {
          $('.flexslider').flexslider("pause");
        });

        froogaloop.addEvent('pause', function(data) {
          $('.flexslider').flexslider("play");
        });
      }


      // Call fitVid before FlexSlider initializes, so the proper initial height can be retrieved.
      $(".flexslider")
        .fitVids()
        .flexslider({
          animation: "slide",
          useCSS: false,
          animationLoop: false,
          smoothHeight: true,
          start: function(slider){
            $('#boxvideo').removeClass('loading');
          },
          before: function(slider){
            $f(player).api('pause');
          }
      });
    });
  </script>


  <!-- Optional FlexSlider Additions -->
    <script src="../js/froogaloop.js"></script>
	<script src="../js/jquery.fitvid.js"></script>