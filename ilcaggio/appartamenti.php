<div class="container" id="appartamenti">
    <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h1>Appartamenti</h1>
    </div>
</div>
<div id="boxslider" class="container loading">

    <div class="row ">
     
            <section class="slider">
                <div id="sliderhome" class="flexslider ">
                    <ul class="slides ">
                        <li>
                            <div class="elemento col-xs-6">
                                <h3>Il Fienile</h3>
                                <a href="ilfienile.php"><img src="img/il-fienile_cover.jpg"></a>
                                <a  href="ilfienile.php" class="btn btn-default " id="scopri_btn">SCOPRI</a>
                            </div>
                            <div class="elemento col-xs-6">
                                <h3>Il Picchio</h3>
                                <a href="ilpicchio.php"><img src="img/il-picchio-cover.jpg"></a>
                                <a  href="ilpicchio.php" class="btn btn-default " id="scopri_btn">SCOPRI</a>
                            </div>
                        </li>

                        <li>
                            <div class="elemento col-xs-6">
                                <h3>Le Querce</h3>
                                <a href="lequerce.php"><img src="img/le-querce-cover.jpg"></a>
                                <a  href="lequerce.php" class="btn btn-default " id="scopri_btn">SCOPRI</a>
                            </div>
                            <div class="elemento col-xs-6">
                                <h3>Gli Olivi</h3>
                                <a href="gliolivi.php"><img src="img/gli-olivi-cover.jpg"></a>
                                <a  href="gliolivi.php" class="btn btn-default " id="scopri_btn">SCOPRI</a>
                            </div>
                        </li>
                        <li>
                            <div class="elemento col-xs-6">
                                <h3>Il Granaio</h3>
                                <a href="ilgranaio.php"><img src="img/il-granaio-cover.jpg"></a>
                                <a  href="ilgranaio.php" class="btn btn-default " id="scopri_btn">SCOPRI</a>
                            </div>
                            <div class="elemento col-xs-6">
                                <h3>Le Ghiande</h3>
                                <a href="leghiande.php"><img src="img/le-ghiande-cover.jpg"></a>
                                <a  href="leghiande.php" class="btn btn-default " id="scopri_btn">SCOPRI</a>
                            </div>
                        </li>



                    </ul>
                </div>

            </section>

      

    </div><!-- /CONTAINER -->
</div>
    <script>
         $(window).load(function(){
      $('#sliderhome').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        
        start: function(slider){
          $('#boxslider').removeClass('loading');
        }
      });

      });
    
    </script>