<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <?php include "header.php"; ?>
    <body>

        <?php include "nav4.php"; ?>

        <div class="senzamenu container" id="main" role="main">
            <section class="slider">
                <div id="sliderfienile" class="flexslider">
                   <ul class="slides">
                        <li>
                            <img src="img/le-querce.jpg" />
                        </li>
                        <li>
                            <img src="img/le-querce2.jpg" />
                        </li>
                        <li>
                            <img src="img/le-querce3.jpg" />
                        </li>
                        <li>
                            <img src="img/le-querce4.jpg" />
                        </li>
                        <li>
                            <img src="img/le-querce5.jpg" />
                        </li>
                        <li>
                            <img src="img/le-querce-6.jpg" />
                        </li>
                        <li>
                            <img src="img/le-querce-7.jpg" />
                        </li>
                        
                    </ul>
                </div>
              
            </section>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Le Querce</h1>

<p>&nbsp;</p>

<p>L&#39;appartamento Le Querce si affaccia direttamente sul pi&ugrave; grande dei terrazzi di tutto l&#39;agriturismo, dal quale &egrave; possibile ammirare le maestose querce secolari che si ergono sopra alla piscina. In soggiorno, nella stagione pi&ugrave; fredda, sar&agrave; possibile riscaldarsi grazie all&#39;antica stufa a legna in ghisa.</p>

<p>&nbsp;</p>

<p>Composizione:</p>

<p>1 soggiorno con angolo cottura, 1 ampia camera matrimoniale, 1 camera doppia, 1 salotto con divano-letto, 1 bagno/doccia area esterna privata attrezzata con tavolo, sedie ed ombrellone</p>


                   

                </div>
            </div>
        </div>
        <?php include "magica.php" ?>
        <?php include "appartamenti.php";?>
        <?php include "footer.php"; ?>
