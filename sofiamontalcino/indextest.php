<?php
$pagetitle = "Home";
$pagename = "home";
$pagedesc = "Sito ufficiale dell'enoteca pizzeria Sofia Montalcino, ristorante suggestivo in pieno centro nella città del Brunello.";
include_once 'cbd.php';
include_once 'header.php';
include_once 'navigation.php';
?>

<div class="container">
    <div class="row-fluid">
        <div id="myCarousel" class="carousel slide span10 offset1" data-interval=2000>
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li><li data-target="#myCarousel" data-slide-to="1"></li><li data-target="#myCarousel" data-slide-to="2"></li><li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>
            <!-- Carousel items -->
            <div class="carousel-inner">
                <div class="active item carouselImageHeight"><img src="images/1.jpg" alt="Sofia Montalcino" class=""></div>
                <div class="item carouselImageHeight"><img src="images/2.jpg" alt="Sofia Montalcino" class=""></div>
                <div class="item carouselImageHeight"><img src="images/3.jpg" alt="Sofia Montalcino" class=""></div>
                <div class="item carouselImageHeight"><img src="images/4.jpg" alt="Sofia Montalcino" class=""></div>
            </div>
            <!-- Carousel nav -->
            <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
            <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
        </div>
    </div>
    <hr>
    <div class="row-fluid">
        <div class="span6 boxsh" style="margin-top:5px;">
            <div class="container ribbon">
                <h2>NEWS</h2>
            </div>
<?php
	$sql = "SELECT * FROM news ORDER BY Data DESC LIMIT 2";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
	$rows = mysql_num_rows($result);
	$sum = 0;
	for ($j = 0 ; $j < $rows ; ++$j) {
		$row = mysql_fetch_row($result);
        $pageurl = "article.php?slug=".$row[4];
        $datasql = "SELECT DATE_FORMAT(Data, '%d/%m/%Y')  as data FROM news WHERE id='$row[5]'";
        $rowdata = mysql_fetch_assoc(mysql_query($datasql));
        $row[1] = strip_tags($row[1]);
    if (strlen($row[1])>=199) {
        $nlenght = 1;
        $row[1]= substr($row[1], 0, 199);
        $row[1] .= "...";

    }        
echo <<<_END
            <div class="row-fluid homenews">
                <h3 class="newstitle"><a href="$pageurl" title="Leggi la news completa">$row[0]</a></h3>
                <div class="newsbodyhome">$row[1]</div>
_END;
    if ($nlenght == 1) {
echo <<<_END
                <p><a href="article.php?slug=$row[4]">Leggi la news completa...</a></p>
_END;
    }
echo <<<_END
                <div class="newsdetail">Pubblicata il $rowdata[data], $row[3] (<a href="article.php?slug=$row[4]">Link</a>)</div>
                <hr>
            </div>

_END;
}
?>

            <div class="row-fluid" style="margin-top:25px;"><a href="offerte.php" class="btn">Vai alla pagina delle News</a></div>
        </div>
<?php
if ($_POST['doSend']=='Invia') {
$name = $_POST[name];
$address = $_POST[address];
$phone = $_POST[phone];
$email = $_POST[email];
$message = $_POST[message];
$headers = "";
$headers .= "From: $email\n";
$headers .= "Reply-To: $email\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=utf-8\n";
$to = "info@artcaketopper.com";
$subject = "Richiesta informazioni";
/*$message = $messaggio;*/
$messagebody = "Hai ricevuto una nuova email via artcaketopper.com, ecco i dettagli:<br />
Nome e Cognome: $name<br />
Indirizzo completo: $address<br />
E-mail: $email<br />
Telefono: $phone<br />
Messaggio: $message<br />";

mail ($to,$subject,$messagebody,$headers);
$sent = "1";
}

?>
 
        <div class="span6">
            <div class="span12 boxsh row-fluid" style="margin-top: 5px;">
                <div class="container ribbon">
                    <h2>CONTATTACI</h2>
                </div>
            <!--<div id="accordion">-->
                
                <!--<a class="btn btn-lille" href="#" style="margin-bottom: 10px;margin-top: 10px;"><i class="icon-envelope icon-white"></i>&emsp; Richiedi informazioni</a>-->
                
                <div>
                    <form class="form-horizontal" action="index.php"  method="post" id="form">
                        <fieldset>

<?php
if ($sent=='1') {
echo <<<_END
<div class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>Messaggio inviato!</h4>
  Sarai ricontattato al pi&ugrave; presto.
</div>
_END;
}
?>
                  
                            <p class="alert alert-block">I campi contrassegnati dall'asterisco sono obbligatori.</p>
                            <div class="control-group">
                                <label class="control-label" for="name">Nome e Cognome *</label>
                                <div class="controls">
                                    <input type="text" name="name" id="name" placeholder="Es. Mario Rossi">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="address">Indirizzo completo</label>
                                <div class="controls">
                                    <input type="text" name="address" id="address" placeholder="Es. Via Roma 10, 53100 Siena">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="phone">Telefono *</label>
                                <div class="controls">
                                    <input type="text" name="phone" id="phone" placeholder="Es. 3331234567, 057712345...">
                                </div>
                            </div>  
                            <div class="control-group">
                                <label class="control-label" for="email">Email *</label>
                                <div class="controls">
                                    <input type="email" name="email" id="email" placeholder="Es. mail@example.com">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="message">Messaggio *</label>
                                <div class="controls">
                                    <textarea rows="3" name="message" id="message"></textarea>
                                </div>
                            </div>
                            <div class="control-group" style="text-align:center;">
                                  <button type="submit" class="btn" name="doSend" value="Invia" id="submitbtn"><i class="icon-envelope"></i>&emsp; Richiedi informazioni</button>
                            </div>
                            <a href="#" rel="tooltip" data-toggle="tooltip" 
                            title="Nel inviare i miei dati dichiaro di aver letto l'informativa e sono 
                            consapevole che il trattamento degli stessi è necessario per ottenere 
                            il servizio proposto. A tal fine, nel dichiarare di essere maggiorenne, 
                            fornisco il mio consenso." 
                            data-placement="right" style="font-size:12px; cursor: help;">Informativa sulla Privacy ai sensi del D.Lgs 196/2003</a>
                        </fieldset>
                    </form>
                </div>
                    <!--</div>-->
            </div>

        </div>
    </div>
    <hr>
<?php
include_once 'footer.php';
?>



