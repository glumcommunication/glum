        <div class="header">
            <div class="container">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="span4" id="logo">
                                <a href="/" title="Torna alla Home"><img src="images/logo.png" alt="Sofia Montalcino Logo" rel="image_src"></a>
                            </div>
                            <div class="span8">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div id="social">
                                            <a href="http://www.tripadvisor.it/Restaurant_Review-g635634-d4023098-Reviews-Sofia-Montalcino_Tuscany.html" title ="Trip Advisor" target="_BLANK">
                                                <img src="images/tripadvisor.png" alt="TripAdvisor" class="socialicons">
                                            </a>
                                            <a href="https://www.facebook.com/sofia.montalcino" title ="Facebook" target="_BLANK">
                                                <img src="images/facebook.png" alt="Facebook" class="socialicons">
                                            </a>
                                            <a href="https://twitter.com/sofiamontalcino" title ="Twitter" target="_BLANK">
                                                <img src="images/twitter.png" alt="Twitter" class="socialicons">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span 12">
                                        <div class="navbar">
                                            <div class="navbar-inner">
                                                <div class="container">

            <!-- .btn-navbar is used as the toggle for collapsed navbar content -->

                                                    <a class="btn btn-navbar btn-large" data-toggle="collapse" data-target=".nav-collapse" style="margin-bottom: 5px; margin-left: -14px;float:left;">
                                                        Menu <i class="icon-white icon-align-justify"></i>
                                                    </a>   
                                                    <div class="nav-collapse collapse">
                                                
                                                        <ul class="nav">
                                                            <li<? if ($pagename == "home") echo " class=\"active\"";?>><a href="/">HOME</a></li>
                                                            
                                                            <li<? if ($pagename == "about") echo " class=\"active\"";?>><a href="about.php">ABOUT</a></li>
                                                            
                                                            <li<? if ($pagename == "news") echo " class=\"active\"";?>><a href="news.php">NEWS</a></li>
                                                            
                                                            <li<? if ($pagename == "gallery") echo " class=\"active\"";?>><a href="gallery.php">GALLERY</a></li>
                                                            
                                                            <li<? if ($pagename == "contatti") echo " class=\"active\"";?>><a href="contatti.php">CONTATTI</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>