<?php
$pagetitle = "Home";
$pagename = "home";
$pagedesc = "Sito ufficiale dell'enoteca pizzeria Sofia Montalcino, ristorante suggestivo in pieno centro nella città del Brunello.";
include_once 'cbd.php';
include_once 'header.php';
include_once 'navigation.php';
?>

        <div id="standard" class="container">
            <div class="row-fluid">
                <div id="myCarousel" class="carousel slide span10 offset1" data-interval=2000>
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li><li data-target="#myCarousel" data-slide-to="1"></li><li data-target="#myCarousel" data-slide-to="2"></li><li data-target="#myCarousel" data-slide-to="3"></li>
                    </ol>
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="active item carouselImageHeight"><img src="images/gallery/1.jpg" alt="Sofia Montalcino" class=""></div>
                        <div class="item carouselImageHeight"><img src="images/gallery/7.jpg" alt="Sofia Montalcino" class=""></div>
                        <div class="item carouselImageHeight"><img src="images/gallery/11.jpg" alt="Sofia Montalcino" class=""></div>
                        <div class="item carouselImageHeight"><img src="images/gallery/8.jpg" alt="Sofia Montalcino" class=""></div>
                    </div>
                    <!-- Carousel nav -->
                    <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                    <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                </div>
            </div>
            <hr>
            <div class="row-fluid">
                <div class="span6 boxsh" style="margin-top:5px; min-height:560px;">
                    <div class="ribbon">
                        <h2>NEWS</h2>
                    </div>
        <?php
        	$sql = "SELECT * FROM news ORDER BY Data DESC LIMIT 2";
        	$result = mysql_query($sql);
        	if (!$result) die ("Database access failed: " . mysql_error());
        	$rows = mysql_num_rows($result);
        	$sum = 0;
        	for ($j = 0 ; $j < $rows ; ++$j) {
        		$row = mysql_fetch_row($result);
                $pageurl = "article.php?slug=".$row[4];
                $datasql = "SELECT DATE_FORMAT(Data, '%d/%m/%Y')  as data FROM news WHERE id='$row[5]'";
                $rowdata = mysql_fetch_assoc(mysql_query($datasql));
                $row[1] = strip_tags($row[1]);
            if (strlen($row[1])>=199) {
                $nlenght = 1;
                $row[1]= substr($row[1], 0, 199);
                $row[1] .= "...";

            }        
        echo <<<_END
                    <div class="row-fluid homenews">
                        <h3 class="newstitle"><a href="$pageurl" title="Leggi la news completa">$row[0]</a></h3>
                        <div class="newsbodyhome">$row[1]</div>
_END;
            if ($nlenght == 1) {
        echo <<<_END
                        <p><a href="article.php?slug=$row[4]">Leggi la news completa...</a></p>
_END;
            }
        echo <<<_END
                        <div class="newsdetail">Pubblicata il $rowdata[data], $row[3] (<a href="article.php?slug=$row[4]">Link</a>)</div>
                        <hr>
                    </div>

_END;
        }
        ?>

                    <div class="row-fluid" style="margin-top:25px;"><a href="news.php" class="btn">Vai alla pagina delle News</a></div>
                </div>
        <?php
        if ($_POST['doSend']=='Invia') {
        $name = $_POST[name];
        $address = $_POST[address];
        $phone = $_POST[phone];
        $email = $_POST[email];
        $message = $_POST[message];
        $headers = "";
        $headers .= "From: $email\n";
        $headers .= "Reply-To: $email\n";
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=utf-8\n";
        $to = "info@sofiamontalcino.it";
        $subject = "Richiesta informazioni";
        /*$message = $messaggio;*/
        $messagebody = "Hai ricevuto una nuova email via sofiamontalcino.it, ecco i dettagli:<br />
        Nome e Cognome: $name<br />
        Indirizzo completo: $address<br />
        E-mail: $email<br />
        Telefono: $phone<br />
        Messaggio: $message<br />";

        mail ($to,$subject,$messagebody,$headers);
        $sent = "1";
        }

        ?>
         
                <div class="span6">
                    <div class="span12 boxsh" style="margin-top: 5px;min-height:560px;">
                        <div class="ribbon">
                            <h2>CONTATTACI</h2>
                        </div>
                        <div class="row-fluid">
                            <form class="form-horizontal" action="index.php"  method="post" id="form">
                                
                                <fieldset>
                                    <p class="alert alert-block">I campi contrassegnati dall'asterisco sono obbligatori.</p>    

        <?php
        if ($sent=='1') {
        echo <<<_END
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <h4>Messaggio inviato!</h4>
          Sarai ricontattato al pi&ugrave; presto.
        </div>
_END;
        }
        ?>
                          
                                    
                                    <div class="control-group">
                                        <label class="control-label" for="name">Nome e Cognome *</label>
                                        <div class="controls">
                                            <input type="text" name="name" id="name" placeholder="Es. Mario Rossi">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="address">Indirizzo completo</label>
                                        <div class="controls">
                                            <input type="text" name="address" id="address" placeholder="Es. Via Roma 10, 53100 Siena">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="phone">Telefono *</label>
                                        <div class="controls">
                                            <input type="text" name="phone" id="phone" placeholder="Es. 3331234567, 057712345...">
                                        </div>
                                    </div>  
                                    <div class="control-group">
                                        <label class="control-label" for="email">Email *</label>
                                        <div class="controls">
                                            <input type="email" name="email" id="email" placeholder="Es. mail@example.com">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="message">Messaggio *</label>
                                        <div class="controls">
                                            <textarea rows="3" name="message" id="message"></textarea>
                                        </div>
                                    </div>
                                    <div class="control-group" style="text-align:center;">
                                          <button type="submit" class="btn" name="doSend" value="Invia" id="submitbtn"><i class="icon-envelope"></i>&emsp; Richiedi informazioni</button>
                                    </div>
                                    <a href="#" rel="tooltip" data-toggle="tooltip" 
                                    title="Nel inviare i miei dati dichiaro di aver letto l'informativa e sono 
                                    consapevole che il trattamento degli stessi è necessario per ottenere 
                                    il servizio proposto. A tal fine, nel dichiarare di essere maggiorenne, 
                                    fornisco il mio consenso." 
                                    data-placement="right" style="font-size:12px; cursor: help;">Informativa sulla Privacy ai sensi del D.Lgs 196/2003</a>
                                </fieldset>
                            </form>
                        </div>
                            <!--</div>-->
                    </div>

                </div>
            </div>
            <hr>
            <div class="row-fluid">
                <div class="span12 boxsh">
                    <div id="box" style="float:left;text-align:center" class="span6">
                        <a href="http://www.guidacatering.it/catering-sofia-vcatering-132373.html" target="_blank">
                            <img src="http://www.guidacatering.it/stamp.xpng?com=132373&amp;v=5" alt="Sofia" border="0" />
                        </a><br />
                        <a href="http://www.guidacatering.it/catering-siena-vprovincia-237100.html" target="_blank">
                            <img alt="Catering Siena" src="http://www.guidacatering.it/img/web/stamp-footer-it-IT.png" />
                        </a>
                    </div>
                    <div class="span6" style="overflow:hidden;border:1px solid #589446;margin-top:21px;">
<div id="TA_cdsscrollingravewide723" class="TA_cdsscrollingravewide">
<ul id="PgCUU926a" class="TA_links tQq4SUS60">
<li id="PkDRmfk" class="GYKI54o">Leggi 34 recensioni di <a target="_blank" href="http://www.tripadvisor.it/Restaurant_Review-g635634-d4023098-Reviews-Sofia-Montalcino_Tuscany.html" onclick="ta.cds.handleTALink($cdsConfig.getMcid()); return true;">Sofia</a></li>
</ul>
</div>
<script src="http://www.jscache.com/wejs?wtype=cdsscrollingravewide&amp;uniq=723&amp;locationId=4023098&amp;lang=it&amp;border=false&amp;shadow=false&amp;backgroundColor=white"></script>

                    </div>
                </div>
            </div>
        </div>
        <hr>
<?php
include_once 'footer.php';
?>



