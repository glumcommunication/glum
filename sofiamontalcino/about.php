<?php
$pagetitle = "About";
$pagename = "about";
$pagedesc = "Sofia Montalcino, risotrante nel cuore della toscana, cucina tradizionale, prodotti tipici e ospitalità garantita.";
include_once 'header.php';
include_once 'navigation.php';
?>
<div class="container">
    <div class="row-fluid">
        <div class="span12 boxsh">
            <div class="ribbon">
                <h2>Chi sono</h2>
            </div>
            <img src="images/gallery/15.jpg" alt="Esterno Sofia" class="span4" style="float:right;">
            
            <p class="justify">
                Nato dall’incontro tra  Fulvia Soda, da sempre appassionata di cultura gastronomica ed enologica 
                e il fondatore dello storico pub di Montalcino il Kaffeina, il Sofia è la scelta ottimale per gustare 
                una buona pizza e bere del buon vino.
            </p>
            <p class="justify">
                Nel cuore del centro storico di Montalcino, affacciato su una splendida vista della vallata, il ristorante 
                offre una cucina raffinata e genuina, incentrata sulla semplicità e tipicità dei prodotti.
            </p>
            <p class="justify">
                Da noi puoi gustare antipasti misti composti da schiacciatine all’olio, salumi e formaggi, primi piatti tipici, 
                stuzzicheria, pizze di ottima qualità (anche d’asporto) e dolci vari. Le nostre pizze speciali, fatte con dei 
                particolari accostamenti degli ingredienti, trasformano la semplicità in raffinatezza e sono il connubio perfetto 
                tra la genuinità del cibo e la freschezza degli ingredienti. Oltre alla pizza integrale, al Sofia si possono infatti 
                trovare, ricette particolari, come la Contadina, con stracchino e pere, o la Golosa, con pecorino di Seggiano e lardo 
                di cinta senese. La carta dei vini è composta da cantine storiche e caratteristiche di Montalcino, lontane dai circuiti 
                della grande distribuzione, mentre le birre si possono trovare sia artigianali, in bottiglia che alla spina.
            </p>
            <p class="justify">
                In un ambiente molto caratteristico, disposto su due piani, dove il contesto rustico antico si sposa sapientemente con la 
                ricercatezza dell’arredamento moderno, l'atmosfera del nostro Ristorante vi farà immergere in un ambiente ricercato, ma 
                anche familiare, frutto della cortesia del servizio e di un'inclinazione particolare che ci caratterizza all'ospitalità e 
                all'accoglienza.
            </p>
            <p class="justify">
                Il Sofia di Montalcino aperto sia a pranzo che a cena, offrendo la possibilità di scegliere un menù per i più piccoli, 
                è la soluzione ideale per tutta la famiglia. Ma è adatto a tutte le età, trasformandosi, in altri orari, da ristorante 
                in sala da tè, enoteca e vineria. Se sei alla ricerca di un locale esclusivo per trascorrere piacevoli serate con gli 
                amici o festeggiare il tuo compleanno o la  tua laurea, il Sofia, dotato di due stanze, è l'ideale.
            </p>
            <p class="justify">
                Nei pressi del ristorante è disponibile un ampio parcheggio gratuito per i clienti.
            </p>

        </div>
    </div>
</div>
<hr>

<?php
include_once 'footer.php';
?>