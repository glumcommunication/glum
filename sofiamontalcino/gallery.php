<?php
$pagetitle = "Gallery";
$pagename = "gallery";
$pagedesc = "Le immagini del nostro ristorante, vieni a trovarci per scoprire di più.";
include_once 'header.php';
include_once 'navigation.php';
?>


  <script type="text/javascript">
    $(window).load(function () {
        $(document).ready(function(){
            collage();
        });
    });
    
    function collage() {
        $('.Collage').removeWhitespace().collagePlus(
            {
                'fadeSpeed' : 2000,
                'targetHeight' : 200
            }
        ).collageCaption();
    };
     
    var resizeTimer = null;
    $(window).bind('resize', function() {
        // hide all the images until we resize them
        $('.Collage .Image_Wrapper').css("opacity", 0);
        // set a timer to re-apply the plugin
        if (resizeTimer) clearTimeout(resizeTimer);
        resizeTimer = setTimeout(collage, 200);
    });

  </script>  
<div class="container">
    <div class="row-fluid">
        <div class="span12 boxsh cake">
            <div class="ribbon">
                <h2>Gallery</h2>
            </div>
<div class="Collage">
    <div class="Image_Wrapper"><a href="images/gallery/1.jpg" rel="lightbox[act]" title=""><img src="images/gallery/1.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/2.jpg" rel="lightbox[act]" title=""><img src="images/gallery/2.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/3.jpg" rel="lightbox[act]" title=""><img src="images/gallery/3.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/4.jpg" rel="lightbox[act]" title=""><img src="images/gallery/4.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/5.jpg" rel="lightbox[act]" title=""><img src="images/gallery/5.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/6.jpg" rel="lightbox[act]" title=""><img src="images/gallery/6.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/7.jpg" rel="lightbox[act]" title=""><img src="images/gallery/7.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/8.jpg" rel="lightbox[act]" title=""><img src="images/gallery/8.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/9.jpg" rel="lightbox[act]" title=""><img src="images/gallery/9.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/10.jpg" rel="lightbox[act]" title=""><img src="images/gallery/10.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/11.jpg" rel="lightbox[act]" title=""><img src="images/gallery/11.jpg"></a></div><div class="Image_Wrapper"><a href="images/gallery/12.jpg" rel="lightbox[act]" title=""><img src="images/gallery/12.jpg"></a></div>
            </div>
            </div>
        </div>
    </div>
<hr>

<?php
include_once 'footer.php';
?>
