      <div class="row-fluid">
        <div class="boxsh span12 centered lastrow" id="footer">
          <h2>SOFIA MONTALCINO - ENOTECA PIZZERIA BRUSCHETTERIA TEA ROOM</h2>
          <p>&copy; <?php echo date(Y); ?> Sofia Montalcino - Via Soccorso Saloni 35, 53024 Montalcino (SI) - Tel.: +39 0577 849408 - Email: <a href="mailto:info@sofiamontalcino.it">info@sofiamontalcino.it</a></p>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row-fluid span12">
        <p style="text-align:center;font-size:12px;">Crafted by <a href="http://www.glumcommunication.it/" target="_BLANK">GLuM Communication</a></p>
      </div>    
    </div>
  </div>

<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<?php
if ($pagename == 'home') {
    echo <<<_END
<script>
    $('.carousel').carousel({
        interval: 3000
    })
</script>


<script>
$(document).ready(function() {

	// validate signup form on keyup and submit
	var validator = $("#form").validate({
		rules: {
			name: "required",
			phone: {
                required: true,
                minlength: 9
            },
            message: "required",
			email: {
				required: true,
                minlength: 7,
                email: true
			}
		},
		messages: {
			name: "Inserisci nome e cognome",
			phone: {
				required: "Inserisci un numero di telefono",
				minlength: "Il numero deve essere composto da almeno 9 cifre"
			},
            message: "Inserisci un messaggio",
			email: {
                required: "Inserisci un indirizzo email",
                minlength: "Inserisci un indirizzo email valido",
                email: "Inserisci un indirizzo email valido"
            }
			
		},
	});


});
</script>
_END;
}
?>
<script>
  $(function() {
    $( "#accordion" ).accordion({
      collapsible: true,
      active: false,
      heightStyle: "content"
    });
  });
  </script>
        <script type="text/javascript">
          $(document).ready(function () {
            $("a").tooltip({
              'selector': ''
            });
          });
        </script>

  
</body>
</html>
