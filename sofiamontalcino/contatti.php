<?php
$pagetitle = "Contatti";
$pagename = "contatti";
$pagedesc = "Indirizzo, email e numero di telefono dell'enoteca pizzeria Sofia Montalcino: contattateci per informazioni e prenotazioni.";
include_once 'header.php';
include_once 'navigation.php';
?>
<div class="container">
    <div class="row-fluid">
        <div class="span12 boxsh">
            <div class="ribbon">
                <h2>Contatti</h2>                
            </div>

            <div id="map_canvas" style="width:100%; height:400px;">
            </div>
            <div class="row-fluid lastrow">
                <div class="span12 ribbon">
                    <h2>Sofia Montalcino</h2><br>
                    <p style="clear:both;">
                        Via Soccorso Saloni, 35 - 53024 Montalcino (SI)
                    </p>
                    <p>
                        Telefono: +39 0577 849408
                    </p>
                    <p>
                        Email: <a href="mailto:info@sofiamontalcino.it" title="Contattami">info@sofiamontalcino.it</a>
                    </p>
                </div>
			</div>
        </div>
    </div>
</div>
<hr>
<?php
include_once 'footer.php';
?>
