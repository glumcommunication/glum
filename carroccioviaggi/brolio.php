<div>
    <div class="incomingBox">
        <div class="col-xs-4">
            <div class="">
                    <img src="img/inc/brolio.jpg" alt="Catsello di Brolio">
                    
            </div>
        </div>
        <div class="col-sm-8">
            <div class="">
                <h1 class="homeTitle">Visita al castello di Brolio</h1>
                <p>
                    Appuntamento con la vostra guida e partenza per la visita guidata all’interno della proprietà con navetta privata.<br>
La visita inizia nell’antico Castello di Brolio, testimone della lunga e affascinante storia della famiglia dei Baroni  Ricasoli, a cui si devono contributi fondamentali nella storia politica italiana e nello sviluppo agro-enologico del  Chianti Classico. Il percorso si snoda poi tra i vigneti che valorizzano l’unicità della tenuta Barone Ricasoli, alla scoperta delle molteplici sfaccettature dei suoi terreni, le caratteristiche climatiche, i progetti di zonazione, di ricerca e selezione dei biotipi di Sangiovese. L’intero sistema di un moderno concetto di vini-viticultura che ha portato alla nascita dei nostri grandi Cru.<br>
Nel recentissimo impianto di vinificazione, funzionalità produttiva e valori estetici si fondono in un ambiente finalizzato alla produzione di vini d’eccellenza. Il processo di affinamento prosegue nell’ampia barriccaia sotterranea delle cantine adiacenti.<br>
La degustazione in sala privata, dei vini scelti in esclusiva per gli ospiti del tour, anticipa la piacevolissima sosta all’Osteria del Castello dove lo chef, vi accompagnerà alla scoperta della migliore cucina toscana in un ambiente rilassante e accogliente, immerso nella magia del bosco inglese del Castello di Brolio.
                
                </p><br>
                <p>La quota comprende:</p><br>
                <ul>
                    <li>Visita guidata con navetta privata all'interno della tenuta</li>
                    <li>Degustazione</li>
                    <li>Pranzo</li>
                    <li>1 notte in B&B in centro</li>
                </ul><br>
                <p>
                    In caso le persone fosse sprovviste di auto proprio , possibilità d trasferimento dalla struttura alberghiera al Castello.<br><br>
                    Prezzo per persona € 130,00<br>
                    Supplemento navetta per il transfert € 30,00<br>
                    Notte supplementare € 80,00<br>
                </p>
            </div>                    
        </div>
    </div>
</div>