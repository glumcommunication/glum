<div>
    <div class="incomingBox">
        <div class="col-xs-4">
            <div class="">
                    <img src="img/inc/palio.jpg" alt="palio">
                    
            </div>
        </div>
        <div class="col-sm-8">
            <div class="">
                <h1 class="homeTitle">Il Palio di Siena</h1>
                <p><strong>Pacchetto 4 giorni/3 notti</strong></p>
                <p>
                    <strong>30 Giugno o 14 Agosto</strong><br>
                    Arrivo e sistemazione nelle strutture prescelte.<br>
                    Intera mattinata a disposizione per visite di carattere personale.<br>
                    Cena in ristorante con tipico menu’ toscano.<br>
                    Pernottamento in hotel.<br><br>
                    
                    <strong>1 Luglio o 15 Agosto</strong> 
                    Prima colazione nella proprio struttura.<br>
                    Possibilità si assistere dall’interno della Piazza alla Prova della mattina.
                    Incontro on la visita guidata per una giornata alla scoperta della Città, ricca e affinascinate per la sua straordinaria atmosfera paliesca. <br>
                    Visita alla famosa Rocca Salimbeni, al Duomo e alla Basilica di San Francesco e San Domenico.<br>
                    Al ternime della visita guidata, sistemazione nei palchi per assistere alla Prova Generale.<br>
                    Rientro in hotel . Incontroc on l’accompagnatore per recarsi alla cena della Prova Generale in una delle contrade che parteciperemmo alla Carriera.<br>
                    Rientro in hotele e pernottamento.<br><br>
                    
                    <strong>2 Luglio o 16 Agosto.</strong><br>
                    Mattinata a disposizione per la scoperta della città.<br>
                    Incontro con l’accompagnatore che provvederà ad accompagnatore i signori visitatori sui palchi riservati in tempo utile per assistere alla Sfilata del Corteo Storico e alla Carriera.<br>
                    Al termine rientro in hotel e pernottamento.<br><br>
                    
                    <strong>3 Luglio o 17 Agosto</strong><br>                    
                    Prima colazione in hotel. Check out e fine dei nostri servizi.<br><br>
                    
                    <strong>Il pacchetto include:</strong><br>
                    Trattamento come da programma<br>
                    Guida turistica come da programma<br>
                    I biglietti per assistere alla prova generale e al Palio <br>
                    Cena in contrada<br>
                    3 pernottamenti in trattamento b/b<br><br>
                    
                    <strong>Il prezzo verrà calcolato in base alle richieste dei clienti.</strong><br><br>
                    
                    I pernottamenti possono, in caso di richiesta, essere modificati.<br>
                    Le strutture comprese nel prezzo sono hotel 3* centrali, eventualmente modificabili con strutture di livello superiore o semi centrali in base alle esigenze.
                </p>
            </div>                    
        </div>
    </div>
</div>