<?php
$pagename = '404';
$pagetitle = 'Page not found';
$pagedesc = '';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';


?>
<div class="main" style="padding-bottom:480px;">
    <div class="container">

        <p style="text-align:center;margin-top:30px;">La pagina che stai cercando non esiste. Carroccio Viaggi ha un nuovo sito, <a href="/" title="Carroccio Viaggi" class="notfound">torna alla HOME</a> e inizia a esplorarlo, speriamo ti piaccia.</p>
    </div>
</div>
<?php
include_once 'footer.php';
?>        
