<div>
    <div class="incomingBox">
        <div class="col-xs-4">
            <div class="">
                    <img src="img/inc/siena.jpg" alt="Siena">
                    
            </div>
        </div>
        <div class="col-sm-8">
            <div class="">
                <h1 class="homeTitle">In giro per la città...</h1>
                <p>
                    Partenza per la visita della città dal luogo di pernottamento con taxi riservato o in alternativa mini-van con guida turistica.<br>
                    Visita lungo le vie della città per ammirare da lontano le bellezze della città storica, passando attraverso la piazza del Campo, 
                    la via principale della città, al Duomo e potendo ammirare cosi dall’esterno tutte le bellezze.<br>
                    La visita, al termine del giro in auto privata, continuerà con la guida all’ interno delle principali Chiese, e visitando un museo 
                    delle contrade, tipico e caratteristico per i suoi contenuti.<br>
                    Pranzo in ristorante tipico in centro.
                </p><br>
                <p>Il pacchetto comprende:</p><br>
                <ul>
                    <li>Taxi privato con guida turistica</li>
                    <li>Visita guidata di mezza giornata</li>
                    <li>Pranzo in ristorante tipico in centro</li>
                    <li>Ingresso al museo di contrada</li>
                    <li>N. 2 pernottamenti in B&B centrale</li>
                </ul><br>
                <p>QUOTA PER PERSONA A PARTIRE DA € 195,00 per persona.</p>
            </div>                    
        </div>
    </div>
</div>