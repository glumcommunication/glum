<div>
    <div class="incomingBox">
        <div class="col-xs-4">
            <div class="">
                    <img src="img/inc/terme.jpg" alt="Terme">
                    
            </div>
        </div>
        <div class="col-sm-8">
            <div class="">
                <h1 class="homeTitle">Week end alle Terme</h1>
                <p>
                    Un week end all’insegna del relax nelle colline circostanti la Toscana nelle zone delle Crete Senesi.<br>
Lasciatevi coccolare tra messaggi e bagni in piscine termali.<br>
                
                </p><br>
                <p>Il pacchetto include:</p><br>
                <ul>
                    <li>2 notti in agriturismi nelle zone delle Crete in solo pernottamento</li>
                    <li>Ingresso scontato alle piscine termali delle Terme Antica Querciolaia o terme di San Giovanni, giornaliero</li>
                    <li>Cena in ristorante con degustazione di prodotti tipici</li>

                </ul><br><br>
                <p>
                    Prezzo a persona a partira da Euro 120,00<br>
                    Notti supplementati Euro 80,00<br>
                    
                </p>
            </div>                    
        </div>
    </div>
</div>