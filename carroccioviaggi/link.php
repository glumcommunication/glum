<?php
$pagename = 'link';
$pagetitle = 'Link utili';
$pagedesc = "Informazioni utili sul meteo, su passaporti e documenti necessari per viaggiare, le ambasciate italiane all'estero, le vaccinazioni necessarie e i diritti del passeggero. In più link a Google Maps, cambio valuta, fusi orari e news dal mondo.";
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<div class="main">
    <div class="container" id="link">
        <h1 class="homeTitle">LINK UTILI</h1>
        
            <div class="col-sm-4">
                <div class="col-sm-6 col-xs-6"><img src="img/link/google-map-01.jpg" alt="Google Maps"></div>
                <div class="col-sm-6 col-xs-6"><a href="http://maps.google.it/" title="http://maps.google.it/" target="_blank"><p>Google Maps</p></a></div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-6 col-xs-6"><img src="img/link/tempo-01.jpg" alt="Che tempo fa?"></div>
                <div class="col-sm-6 col-xs-6"><a href="http://www.ilmeteo.it/meteo-mondo/" title="http://www.ilmeteo.it/meteo-mondo/" target="_blank"><p>Che tempo fa?</p></a></div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-6 col-xs-6"><img src="img/link/passaporto-01.jpg" alt="Passaporti"></div>
                <div class="col-sm-6 col-xs-6"><a href="http://www.poliziadistato.it/articolo/1087-Passaporto" title="http://www.poliziadistato.it/articolo/1087-Passaporto" target="_blank"><p>Passaporti</p></a></div>
            </div>

            <div class="col-sm-4">
                <div class="col-sm-6 col-xs-6"><img src="img/link/news-01.jpg" alt="News dal mondo?"></div>
                <div class="col-sm-6 col-xs-6"><a href="http://www.ansa.it/" title="http://www.ansa.it/" target="_blank"><p>News dal mondo</p></a></div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-6 col-xs-6"><img src="img/link/cambio-01.jpg" alt="Cambio valuta"></div>
                <div class="col-sm-6 col-xs-6"><a href="http://it.finance.yahoo.com/valuta" title="http://it.finance.yahoo.com/valuta" target="_blank"><p>Cambio valuta</p></a></div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-6 col-xs-6"><img src="img/link/viaggiare-01.jpg" alt="Viaggiare sicuri"></div>
                <div class="col-sm-6 col-xs-6"><a href="http://www.viaggiaresicuri.mae.aci.it/" title="http://www.viaggiaresicuri.mae.aci.it/" target="_blank"><p>Viaggiare Sicuri</p></a></div>
            </div>
         
            <div class="col-sm-4">
                <div class="col-sm-6 col-xs-6"><img src="img/link/orari-01.jpg" alt="Fusi orari"></div>
                <div class="col-sm-6 col-xs-6"><a href="http://www.iorario.com/" title="http://www.iorario.com/" target="_blank"><p>Fusi orari</p></a></div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-6 col-xs-6"><img src="img/link/vaccinazioni-01.jpg" alt="Vaccinazioni"></div>
                <div class="col-sm-6 col-xs-6"><a href="http://www.salute.gov.it/malattieInfettive/paginaMenuMalattieInfettive.jsp?menu=viaggiatori&lingua=italiano" title="http://www.salute.gov.it/malattieInfettive/paginaMenuMalattieInfettive.jsp?menu=viaggiatori&lingua=italiano" target="_blank"><p>Vaccinazioni</p></a></div>
            </div>
            <div class="col-sm-4">
                <div class="col-sm-6 col-xs-6"><img src="img/link/diritti-01.jpg" alt="Diritti del passeggero"></div>
                <div class="col-sm-6 col-xs-6"><a href="http://www.enac.gov.it/i_diritti_dei_passeggeri/la_carta_dei_diritti_del_passeggero/index.html" title="http://www.enac.gov.it/i_diritti_dei_passeggeri/la_carta_dei_diritti_del_passeggero/index.html" target="_blank"><p>Diritti del passeggero</p></a></div>
            </div>
        
            <div class="col-sm-4">
                <div class="col-sm-6 col-xs-6"><img src="img/link/ambasciate-01.jpg" alt="Ambasciate"></div>
                <div class="col-sm-6 col-xs-6"><a href="http://www.esteri.it/MAE/IT/Ministero/Servizi/Italiani/Rappresentanze" title="http://www.esteri.it/MAE/IT/Ministero/Servizi/Italiani/Rappresentanze" target="_blank"><p>Ambasciate</p></a></div>
            </div>
        
    </div>
</div>
<?php
include_once 'footer.php';
?>