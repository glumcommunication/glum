<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="img/favicon.png" type="image/svg" />
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,600,800' rel='stylesheet' type='text/css'>
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
        <meta name="description" content="<?php echo $pagedesc ?>">
        <meta name="KEYWORDS" content="Agenzia viaggi viaggio viaggiare sicuri estero vacanze holiday soggiorno mare spiaggia montagna settimana bianca prenotazione hotel biglietti biglietteria aereo treno autobus nave crociera escursione avventura gita week end Europa Africa Asia Oceania Nord America Sud America benessere relax liste viaggi di nozze matrimonio incoming Siena Italia viaggi organizzati di gruppo ticket Palio di Siena bus Marozzi Saps Buonotourist Curcio Tiemme fly & drive offerte occasioni promozioni last minute turismo turisti.">
        <link href="css/normalize.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="css/liquid-slider.css">
        <link href="css/style.css" rel="stylesheet" media="screen">
        <script src="js/respond.js"></script>
        <!--[if IE]>
<script src="js/html5shiv.js"></script>
<![endif]-->
        
        <title><?php echo $pagetitle ?> | Il Carroccio Viaggi</title>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46654150-1', 'carroccioviaggi.com');
  ga('send', 'pageview');

</script>
    </head>
    <body>

        <div id="wrapper">
