<?php
$pagename = 'index';
$pagetitle = 'Home';
$pagedesc = "Carroccio Viaggi è un'agenzia di viaggi con oltre vent'anni di esperienza a Siena. Offre soluzioni per ogni richiesta di tipo turistico.";
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<div class="main">
    <div id="carousel-home-pictures" class="carousel slide" data-ride="carousel" data-interval="5000">
  <!-- Indicators -->


  <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="img/crsl/1.jpg" alt="">
            </div>
            <div class="item">
              <img src="img/crsl/2.jpg" alt="">
        
            </div>
            <div class="item">
                <img src="img/crsl/3.jpg" alt="">
            </div>
            <div class="item">
                <img src="img/crsl/4.jpg" alt="">
            </div>
            <div class="item">
                <img src="img/crsl/5.jpg" alt="">
            </div>
            <div class="item">
                <img src="img/crsl/6.jpg" alt="">
            </div>
        </div>
    </div>
    <div class="container">
        <h1 class="homeTitle">LE NOSTRE OFFERTE</h1>
        <div class="row">
            <div class="liquid-slider"  id="main-slider">
                
<?php

$start = 0;
$limit = 3;

$sql = "SELECT * FROM carroccio WHERE category='01' ORDER BY id ASC";
$result = mysql_query($sql);
if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
for ($j = 0 ; $j < $rows ; ++$j) {
    $row = mysql_fetch_row($result);
        if (($j)%3==0) {echo "<div>";}
?>

                <div class="col-sm-4">
                    <a href="item.php?item=<?php echo $row[5]; ?>" title="<?php echo $row[0]; ?>"><div class="offerHome">
                        <figure>
                            <img src="<?php echo $row[2] ?>">
<?php
            if ($row[9]!='') {
?>
                            <div class="caption"><?php echo $row[9]; ?></div>
<?php
                             }
?>
                        </figure>
                        <p class="title"><?php echo $row[0]; ?></p>
                    </div></a>
                </div>
<?php
    if (($j+1)%3==0) {echo "</div>";}
    }
?>
            </div>
        </div>
        <h1 class="homeTitle">L'AGENZIA</h1>
        <p>
            L'Agenzia Carroccio Viaggi è presente a Siena da oltre vent'anni. Il team specializzato dell'Agenzia Carroccio Viaggi, mette a 
            vostra completa disposizione la propria professionalità per soddisfare ogni tipo di richiesta turistica, ponendo in primo piano 
            le esigenze e le attese del cliente. Che si tratti di viaggiare per il Mondo o di venire a visitare Siena e i suoi dintorni, 
            idee sempre originali  vi attendono in agenzia.<br>
            Particolare attenzione è rivolta ai futuri sposi che intendono realizzare il proprio viaggio di nozze e renderlo unico.
            Vi aspettiamo per trovare la soluzione più adatta alle vostre esigenze.
        </p>
        <!--<h1 class="homeTitle">INCOMING</h1>
        <div class="row">
            <div class="liquid-slider"  id="main-slider-inc">-->
                
<?php
/*include_once 'palio.php';
include_once 'siena.php';
include_once 'brolio.php';
include_once 'castello.php';
include_once 'cavallo.php';
include_once 'terme.php';*/
/*$start = 0;
$limit = 3;

$sql = "SELECT * FROM products ORDER BY id ASC";
$result = mysql_query($sql);
if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
for ($j = 0 ; $j < $rows ; ++$j) {
    $row = mysql_fetch_row($result);*/

?>

      
            <!--</div>
        </div>  --> 
   
        <div class="row">
            <div class="col-sm-12 reteweb">
            <iframe src="http://www.reteviaggi.com/reteweb/offerte-viaggi-il-carroccio-viaggi-ag-7857-v-1.html" name="reteweb" scrolling="no" width="555" height="244" frameborder="0" style="background-color:transparent" allowtransparency="true"><a href="http://www.reteviaggi.com/reteweb/offerte-viaggi-il-carroccio-viaggi-ag-7857-v-1.html">Le Offerte Viaggio di Il Carroccio Viaggi</a> - Powered By <a href="http://www.reteviaggi.com">ReteViaggi</a></iframe>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 reteweb">
            <iframe src="http://www.reteviaggi.com/reteweb/cerca-offerte-il-carroccio-viaggi-ag-7857.html" name="reteweb" scrolling="no" width="555" height="735"frameborder="0" style="background-color:transparent" allowtransparency="true"><a href="reteweb/cerca-offerte-il-carroccio-viaggi-ag-7857.html">Cerca tra tutte le offerte Viaggio di Il Carroccio Viaggi</a></iframe>
            </div>
        </div>
    </div>
</div>


<?php
include_once 'footer.php';
?>
