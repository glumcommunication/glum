<div>
    <div class="incomingBox">
        <div class="col-xs-4">
            <div class="">
                    <img src="img/inc/cavallo.jpg" alt="Cavallo">
                    
            </div>
        </div>
        <div class="col-sm-8">
            <div class="">
                <h1 class="homeTitle">Passeggiate a cavallo</h1>
                <p>
                    Partenza dal luogo di incontro alla volta di Staggia con auto privata.<br>
Ritrovo e partenza a cavallo per una passeggiata tra le antiche vie che conducono al castello Medioevale di Monteriggioni.<br>
Possibilità di sosta per una visita all’interno delle mura.<br>
Ripartenza a cavallo per rientro a Staggia.<br>
Trasferimento in hotel/agriturismo<br>
                
                </p><br>
                <p>La quota comprende:</p><br>
                <ul>
                    <li>Passeggiata di tre ore a cavallo</li>
                    <li>Assicurazione</li>
                    <li>Attrezzatura base</li>
                    <li>Accompagnatore esperto</li>
                    <li>Trasferimento a/r da hotel/agriturismo</li>
                    <li>2 notti in agriturismo con uso cucina</li>
                </ul><br><br>
                <p>
                    Il pacchetto è valido tutto l'anno.<br>
                    L'escursione è soggetta a variazione in base alle condizioni climatiche.<br><br>
                    Euro 280,00 euro base 2 persone<br>
                    Bambini sotto 6 anni pernottamento gratuito.
                </p>
            </div>                    
        </div>
    </div>
</div>