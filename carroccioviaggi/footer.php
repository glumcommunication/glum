<footer>
    <div class="container">
        <h1>Carroccio Viaggi S.R.L.</h1>
        <p>Via Montanini 20 - 53100 Siena</p>
        <p>Telefono +39 0577 226964 - Fax +39 0577 274760
        <p>info@carroccioviaggi.com</p>
        <p>P.IVA: 01032310524</p>
		<p>&emsp;</p>
		<p>Crafted by <a href="http://www.glumcommunication.it/" target="_BLANK">GLuM Communication</a></p>
    </div>
</footer>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>        
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.touchSwipe.min.js"></script>
<script src="js/jquery.liquid-slider-custom.min.js"></script>  
<script>
$('#main-slider').liquidSlider({
    slideEaseFunction: "easeInOutCubic",
    continuous:true,
    autoSlide: true,
    autoSlideInterval: 10000
});
</script>
<?php
if ($pagename == "servizi") {
echo <<<_END
    
    
  <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.1/jquery.min.js"></script>   -->
    <script src="js/jquery-1.7.2.min.js"></script>
  <script src="js/jquery.collagePlus.min.js"></script>
  <script src="js/jquery.removeWhitespace.min.js"></script>
  <script src="js/jquery.collageCaption.min.js"></script>
    <script type="text/javascript">
    $(window).load(function () {
        $(document).ready(function(){
            collage();
        });
    });
    
    function collage() {
        $('.collage').removeWhitespace().collagePlus(
            {
                'fadeSpeed'     : 2000,
                'targetHeight'  : 300,
                'effect'          : 'effect-2'
                
            }
        ).collageCaption();
    };
     
    var resizeTimer = null;
    $(window).bind('resize', function() {
        // hide all the images until we resize them
        $('.collage .Image_Wrapper').css("opacity", 0);
        // set a timer to re-apply the plugin
        if (resizeTimer) clearTimeout(resizeTimer);
        resizeTimer = setTimeout(collage, 200);
    });

  </script>    
_END;
}
?>
<?php
if ($pagename == "contatti") {
?>
<script>
$(document).ready(function() {

	// validate signup form on keyup and submit
	var validator = $("#contatti").validate({
		rules: {
			name: "required",
                        subject: "required",

                        message: "required",
                        privacy: "required",
			email: {
				required: true,
                minlength: 7,
                email: true
			}
		},
		messages: {
			name: "Campo obbligatorio - Inserisci il tuo nome",
                        surname: "Inserisci il tuo cognome",
            subject: "Scegli un oggetto per il messaggio",
			phone: {
				
				minlength: "Il numero deve essere composto da almeno 9 cifre"
			},
            message: "Inserisci un messaggio",
            privacy: "Accetazione informativa sulla privacy obbligatoria!<br> ",
			email: {
                required: "Campo obbligatorio - Inserisci un indirizzo email",
                minlength: "Inserisci un indirizzo email valido",
                email: "Inserisci un indirizzo email valido"
            }
			
		},
	});


});
</script>
<?php
};
?>


        </div>
    </body>
</html>
