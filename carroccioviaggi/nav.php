<div class="social">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <a href="https://www.facebook.com/www.carroccioviaggi.it?ref=glum" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="https://twitter.com/carroccioviagg1?ref=glum" target="_blank"><i class="fa fa-twitter"></i></a>
            </div>
        </div>
    </div>
</div>
<header>
    <div class="container">
        <div class="row">
        
            <div class="col-sm-4 logo">
                <a href="/" title="Torna alla Home"><img src="img/logocropw1.png" alt="logo"></a>
            </div>
            <div class="col-sm-8" style="background-color: #fcfbf6;">

                <nav class="navbar navbar-default" role="navigation">
<!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header"><p>
                        <button type="button" class="navbar-toggle  my-button" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span>Menu</span>
                        </button>
                    </div>
                            

<!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">                                    
                        <ul class="nav navbar-nav">
                            <li <?php if($pageName=='index'){echo 'class="sel"';} ?>><a href="/" title="Home">HOME</a></li>
                            <li <?php if($pageName=='servizi'){echo 'class="sel"';} ?>><a href="servizi.php" title="Servizi">SERVIZI</a></li>
                            <li <?php if($pageName=='incoming'){echo 'class="sel"';} ?>><a href="incoming.php" title="Incoming">INCOMING</a></li>
                            <li <?php if($pageName=='linkutili'){echo 'class="sel"';} ?>><a href="link.php" title="Link utili">LINK UTILI</a></li>
                            <li <?php if($pageName=='contatti'){echo 'class="sel"';} ?>><a href="contatti.php" title="Contatti">CONTATTI</a></li>
                            <li <?php if($pageName=='offerte'){echo 'class="sel"';} ?>><a href="offerte.php" title="Offerte">OFFERTE</a></li>
                        </ul>
                    </div>
                </nav>

            </div>
        </div>

    </div>
</header>