<?php
$pagename = 'contatti';
$pagetitle = 'Contatti';
$pagedesc = 'Carroccio Viaggi ha sede a Siena, in via Montanini 20, in pieno centro storico. Contattateci allo 0577-226964.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
if ($_POST['doSend']=='Invia') {
$name = $_POST[name];
$email = $_POST[email];
$subject = $_POST[subject];
$message = $_POST[message];
$headers = "";
$headers .= "From: $email\n";
$headers .= "Reply-To: $email\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=utf-8\n";
$to = "info@carroccioviaggi.com";
/*$message = $messaggio;*/
$messagebody = "Hai ricevuto una nuova email via carroccioviaggi.com, ecco i dettagli:<br />
Nome: $name <br />

E-mail: $email<br />

Oggetto: $subject<br>

Messaggio: $message<br />";

mail ($to,$subject,$messagebody,$headers);
$sent = "1";
}
?>
<div class="main">
    <div class="container">
        <div class="row">
            <h1 class="homeTitle">CONTATTACI</h1>
            <div class="col-sm-6 formCont">
<?php
if ($sent=='1') {
?>
                <div class="alert alert-success">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <h4>Messaggio inviato!</h4>
                  Sarai ricontattato al pi&ugrave; presto.
                </div>
<?php
}
?>
                <form action="contatti.php" method="post" id="contatti">
                    <div class="form-group">
                      <input type="text" class="form-control" id="name" name="name" placeholder="Inserisci il tuo nome">
                    </div>
                    <div class="form-group">
                      <input type="email" class="form-control" id="email" name="email" placeholder="Inserisci la tua email">
                    </div>
                    <div class="form-group">
                      <input type="etext" class="form-control" id="subject" name="subject" placeholder="Oggetto del messaggio">
                    </div>
                    <div class="form-group">
                      <textarea class="form-control" rows="7" id="message" name="message"></textarea>
                    </div>
                    <div class="checkbox">
                        <label>
                          <input type="checkbox" value="Yep" name="privacy">
                            <a id="privacy" href="#" rel="tooltip" data-toggle="tooltip" title="Nel inviare i miei dati dichiaro di aver letto l'informativa e sono consapevole che il trattamento degli stessi è necessario per ottenere il servizio proposto. A tal fine, nel dichiarare di essere maggiorenne, fornisco il mio consenso." data-placement="right" style="font-size:14px;cursor: help;">
                                Informativa sulla Privacy ai sensi del D.Lgs 196/2003</a>
                        </label>
                    </div>
                    <button type="submit" class="btn my-button" name="doSend" value="Invia">Invia</button>
                  </form>
            </div>
            <div class="col-sm-6 dataCont">
                <p>
                    Carroccio Viaggi S.r.l.<br>
                    Via Montanini, 20<br>
                    53100 Siena<br><br>
                    Telefono: +39 0577 226964<br>
                    Fax: +39 0577 274760<br>
                    Email: <a href="mailto:info@carroccioviaggi.com">info@carroccioviaggi.com</a><br>
                    Partita Iva: 01032310524
                </p>
            </div>
        </div>
        <div class="row">
            <h1 class="homeTitle">TEAM</h1>
            <div class="col-sm-4 teamCont">
                <img src="img/cont1.jpg" alt="Alessandra Pepi">
                <p>
                    Alessandra Pepi<br>
                    Email: <a href="mailto:alessandra@carroccioviaggi.com">alessandra@carroccioviaggi.com</a><br>
                    Skype: alessandra8764
                </p>
            </div>
            <div class="col-sm-4 teamCont">
                <img src="img/cont2.jpg" alt="Marystella Cardiello">
                <p>
                    Marystella Cardiello<br>
                    Email: <a href="mailto:stella@carroccioviaggi.com">stella@carroccioviaggi.com</a><br>
                    Skype: stella10686
                </p>
            </div>
            <div class="col-sm-4 teamCont">
                <img src="img/cont3.jpg" alt="Donatella Ciatti">
                <p>
                    Donatella Ciatti<br>
                    Email: <a href="mailto:donatella@carroccioviaggi.com">donatella@carroccioviaggi.com</a><br>
                    Skype: donatella140658
                </p>
            </div>
        </div>
    </div>
</div>
<?php
include_once 'footer.php';
?>