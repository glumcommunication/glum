<?php
$pagename = 'servizi';
$pagetitle = 'I nostri servizi';
$pagedesc = 'Carroccio Viaggi organizza viaggi di gruppo, viaggi di nozze, crociere, soggiorni al mare, settimane bianche, soggiorni benessere, week end nelle capitali europee e in Italia; fornisce servizio di biglietteria per aerei, bus, treni e navi.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<div class="main">
    <div class="container">
        <h1 class="homeTitle">SERVIZI</h1>
        <div class="collage">
            <div class="Image_Wrapper"><img src="img/srv/weekend.jpg"></div>
            <div class="Image_Wrapper"><img src="img/srv/crociere.jpg"></div>
            <div class="Image_Wrapper"><img src="img/srv/listanozze.jpg"></div>
            <div class="Image_Wrapper"><img src="img/srv/lastminute.jpg"></div>
            <div class="Image_Wrapper"><img src="img/srv/mare.jpg"></div>
            <div class="Image_Wrapper"><img src="img/srv/ticket.jpg"></div>
            <div class="Image_Wrapper"><img src="img/srv/fly.jpg"></div>
            <div class="Image_Wrapper"><img src="img/srv/soggiorno.jpg"></div>
            <div class="Image_Wrapper"><img src="img/srv/viaggi.jpg"></div>
        </div>
        <!--<img src="img/srv/row1.png" alt="Week end e crociere">
        <img src="img/srv/row2.png" alt="Viaggi, last minute, soggiorno mare">
        <img src="img/srv/row3.png" alt="Week end e crociere">
        <div class="row">
            <div class="col-xs-8">
                <img src="img/srv/weekend.jpg">
            </div>
            <div class="col-xs-4">
                <img src="img/srv/crociere.jpg">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-8">
                <img src="img/srv/viaggi.jpg">
            </div>
            <div class="col-xs-4">
                <img src="img/srv/lastminute.jpg">
            </div>
            <div class="col-xs-4">
                <img src="img/srv/mare.jpg">
            </div>
        </div>-->
    </div>
</div>
<?php
include_once 'footer.php';
?>
