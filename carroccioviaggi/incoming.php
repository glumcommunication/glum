<?php
$pagename = 'servizi';
$pagetitle = 'Incoming';
$pagedesc = 'Carroccio Viaggi fornisce servizi a i turisti che vogliono visitare Siena e le bellezze del territorio. Che si tratti di assistere al Palio di Siena o di fare un tour nel Chianti o a San Gimignano, Montalcino e le altre località abbiamo la soluzione per voi.';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>

<div class="main">
    <div class="container">
        <div class="incoming">
            <h1 class="homeTitle">INCOMING</h1>
<?php
$sql = "SELECT * FROM carroccio WHERE category='02' ORDER BY id DESC";
$result = mysql_query($sql);
if (!$result) die ("Database access failed: " . mysql_error());
$rows = mysql_num_rows($result);
for ($j = 0 ; $j < $rows ; ++$j) {
    $row = mysql_fetch_row($result);
?>      <div class="incomingBox col-sm-12">
            <div class="col-xs-4">
                <div class="">
                        <img src="<?php echo $row[2]; ?>" alt="Terme">
                        
                </div>
            </div>
            <div class="col-sm-8">
                <div class="">
                    <h1 class="homeTitle"><a href="item.php?item=<?php echo $row[5]; ?>" title="<?php echo $row[0]; ?>"><?php echo $row[0]; ?></a></h1>
                    <?php echo $row[1]; ?>
<?php
    if ($row[8]!='') {
?>    
                    <div class="pdfdownload">
                        <p>
                            <img src="img/pdfdownload.png" alt="Download">
                            <a href="<?php echo $row[8]; ?>" title="<?php echo $row[8]; ?>">
                                Scarica il programma completo
                            </a>
                        </p>
                    </div>
<?php
                     }
?>
                </div>                    
            </div>
            
        </div>

<?php
}
?>            
<?php
//include_once 'palio.php';
//include_once 'siena.php';
//include_once 'brolio.php';
//include_once 'castello.php';
//include_once 'cavallo.php';
//include_once 'terme.php';
?>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>
