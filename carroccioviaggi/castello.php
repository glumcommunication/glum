<div>
    <div class="incomingBox">
        <div class="col-xs-4">
            <div class="">
                    <img src="img/inc/castello.jpg" alt="Castello">
                    
            </div>
        </div>
        <div class="col-sm-8">
            <div class="">
                <h1 class="homeTitle">Una notte in un castello da favola</h1>
                <p>
                    Arrivo con mezzi propri presso la struttura, soggiorno in camera doppia Classic presso un castello nella provincia di Siena. <br>
Dopo la prima colazione visita delle Cantine del Brunello. Al termine, degustazione dell’ampia gamma di vini presso l’enoteca.<br>
                
                </p><br>
                <p>Quota di partecipazione per persona 250,00 Euro.</p><br>
                <p>La quota comprende:</p><br>
                <ul>
                    <li>Una notte in camera Classic nel Castello</li>
                    <li>Una visita alle cantine de Brunello</li>
                    <li>Una degustazione di una selezioni di vini</li>
                    <li>Notte supplementare su richiesta</li>
                </ul>
            </div>                    
        </div>
    </div>
</div>