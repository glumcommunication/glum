<?php
$pagename = 'index';
$pagetitle = 'WOK SUSHI';
$pagedesc = '';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php'
?>
            <div class="main">
                <div class="container" style="overflow:hidden;">
                    <ul class="col-xs-12" id="slider">
<?php
include_once 'home.php';
include_once 'cucina.php';
include_once 'ristorante.php';
include_once 'gallery.php';
include_once 'dovesiamo.php';
include_once 'contatti.php';
?>
                    </ul>
                </div>
            </div>
<?php
include_once 'footer.php';
?>

