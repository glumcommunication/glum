            <div id="push">
            </div>
        </div>
        <div id="footer">
            <div class="fBorder">
            </div>
            <div class="fContainer">
                <div class="container">
                    <div class="row" id="fInfo">
                        <div class="col-xs-2 fLeft">
                            <p class="credit">Crafted by <a href="http://www.glumcommunication.it">GLuM Communication</a></p>
                        </div>
                        <div class="col-xs-8 fCenter">
                            <p>
                            WOK SUSHI &copy; 2014 | Piazza Maestri del Lavoro 31/interno 1 (Zona Viale Toselli) |
                            Tel.: +39 0577 530080 - Cell.: +39 393 6459999 | P. Iva: 01359860523
                            </p>
                            
                        </div>
                        <div class="col-xs-2 fRight">
                            <p class="credit"><a href="https://www.facebook.com/pages/Wok-Sushi/613025348769474" title="Wok Sushi Facebook Page" target="_blank">Facebook</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/lightbox-2.6.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.animate-enhanced.min.js"></script>
<script>
    //var value = url.substring(url.lastIndexOf('#') + 1);
var hash = window.location.hash;
var contWidth = $(".container").width();
if (hash == "#index") {
 $('#slider').css('left','0');
 $('.page01').addClass('selected');    
};
if (hash == "#cucina") {
 $('#slider').css('left','-1140px');
 $('.page02').addClass('selected');    
};
if (hash == "#ristorante") {
 $('#slider').css('left','-2280px');
    $('.page03').addClass('selected');
};
if (hash == "#gallery") {
 $('#slider').css('left','-3420px');
    $('.page04').addClass('selected');
};
if (hash == "#dovesiamo") {
 $('#slider').css('left','-4560px'); 
    $('.page05').addClass('selected');
};
if (hash == "#contatti") {
 $('#slider').css('left','-5700px');  
    $('.page06').addClass('selected');
};
    
$('a.page01').click(function(){
    var nash = window.location.hash;
    if (nash == "#index") {
        return null;
    }
    else if (nash != "#index") {
    $('*').removeClass('selected');
    $('.page01').addClass('selected');
    $('#slider').animate({left : '0'});
    }
});
$('a.page02').click(function(){
    var nash = window.location.hash;
    if (nash == "#cucina") {
        return null;
    }
    else if (nash != "#cucina") {
    $('*').removeClass('selected');
    $('.page02').addClass('selected');
    $('#slider').animate({left : '-1140px'});
    }
});
$('a.page03').click(function(){
    var nash = window.location.hash;
    if (nash == "#ristorante") {
        return null;
    }
    else if (nash != "#ristorante") { 
    $('*').removeClass('selected');
    $('.page03').addClass('selected');
    $('#slider').animate({'left' : '-2280px'});
    }
});
$('a.page04').click(function(){
    var nash = window.location.hash;
    if (nash == "#gallery") {
        return null;
    }
    else if (nash != "#gallery") {
    $('*').removeClass('selected');
    $('.page04').addClass('selected');
    $('#slider').animate({'left' : '-3420px'});
    }
});
$('a.page05').click(function(){
    var nash = window.location.hash;
    if (nash == "#dovesiamo") {
        return null;
    }
    else if (nash != "#dovesiamo") { 
    $('*').removeClass('selected');
    $('.page05').addClass('selected');
    $('#slider').animate({'left' : '-4560px'});
    }
});
$('a.page06').click(function(){
    var nash = window.location.hash;
    if (nash == "#contatti") {
        return null;
    }
    else if (nash != "#contatti") { 
    $('*').removeClass('selected');
    $('.page06').addClass('selected');
    $('#slider').animate({'left' : '-5700px'});
    }
});

</script>
<!--<script type="text/javascript">


$("document").ready(function() {
				$('a.page01').click(function(){
					$('html, body').animate(
					{scrollTop: $("#page01").offset().top-0}, 1000, "easeInOutCirc");				   
				 });
				 $('a.page02').click(function(){
					$('html, body').animate(
                    {scrollTop: $("#page02").offset().top-0}, 1000, "easeInOutCirc");	
				 });
                 $('a.page03').click(function(){
					$('html, body').animate(
                    {scrollTop: $("#page02").offset().top-0}, 1000, "easeInOutCirc");	
				 });
				 $('a.page04').click(function(){
					$('html, body').animate(
                    {scrollTop: $("#page04").offset().top-0}, 1000, "easeInOutCirc");	
				 });
				 $('a.page05').click(function(){
					$('html, body').animate({
						scrollTop: $("#page05").offset().top-0}, 1000, "easeInOutCirc");	
				 });
				 $('a.page06').click(function(){
					$('html, body').animate({
						scrollTop: $("#page06").offset().top-0}, 1000, "easeInOutCirc");	
				 });

});

</script>-->

<script>
$(document).ready(function() {

	// validate signup form on keyup and submit
	var validator = $("#contactform").validate({
		rules: {
			name: "required",
                        surname: "required",
			phone: {
                required: false,
                minlength: 9
            },
                        message: "required",
                        privacy: "required",
			email: {
				required: true,
                minlength: 7,
                email: true
			}
		},
		messages: {
			name: "Campo obbligatorio - Inserisci il tuo nome",
                        surname: "Inserisci il tuo cognome",
			phone: {
				
				minlength: "Il numero deve essere composto da almeno 9 cifre"
			},
            message: "Inserisci un messaggio",
            privacy: "Accetazione informativa sulla privacy obbligatoria!<br> ",
			email: {
                required: "Campo obbligatorio - Inserisci un indirizzo email",
                minlength: "Inserisci un indirizzo email valido",
                email: "Inserisci un indirizzo email valido"
            }
			
		},
	});


});
</script>        

    </body>
</html>
