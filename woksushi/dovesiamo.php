            <div class="content" id="page05">
                <div class="col-sm-10 col-sm-offset-1 transLayer">
                    <h1>DOVE SIAMO</h1>
                </div>
                <div class="col-sm-5 col-sm-offset-1 homeLeft">
                    <div id="map_canvas" style="width:100%; height:300px;"></div>
                </div>
                <div class="col-sm-5 homeRight" style="height:300px;padding-top:50px;">
                    <p>
                        Ristorante Sushi Wok<br>
                        Piazza Maestri del Lavoro, 31/interno 1<br>
                        53100 Siena
                    </p>
                    <p>
                        Aperto tutti i giorni<br>
                        Dalle 12:00 alle 15:00<br>
                        e dalle 19:00 alle 24:00
                    </p>
                </div>
            </div>
