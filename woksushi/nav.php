            <div id="header">
                <div id="topmenu">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-4" id="navLeft">
                                <ul class="mynav nav navbar-nav">
                                    <li>
                                        <a href="index.php#index" title="" class="page01">Home</a>
                                    </li>
                                    <li>
                                        <a href="index.php#cucina" title="" class="page02">Cucina</a>
                                    </li>
                                    <li>
                                        <a href="index.php#ristorante" title="" class="page03">Ristorante</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-xs-4" id="logo">
                                <a href="/"><img src="img/logo.png" alt="Wok Sushi Logo"></a>
                            </div>
                            <div class="col-xs-4" id="navRight">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="index.php#gallery" title="" class="page04">Gallery</a>
                                    </li>
                                    <li>
                                        <a href="index.php#dovesiamo" title="" class="page05">Dove Siamo</a>
                                    </li>
                                    <li>
                                        <a href="index.php#contatti" title="" class="page06">Contatti</a>
                                    </li>
                                </ul>
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
