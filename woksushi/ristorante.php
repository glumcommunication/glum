            <li class="content" id="page03">
                <div class="col-sm-10 col-sm-offset-1 whiteLayer">
                    <h1>IL RISTORANTE</h1>
                    <div class="row">
                        <div class="col-sm-2"><img src="img/ristorante1.png" class="img-circle"></div>
                        <div class="col-sm-8">
                            <p style="margin-top: 0;">
                                Cercate un posto accogliente ed elegante ma allo stesso tempo rilassato? Wok Sushi 
                                Siena risponde in pieno alle vostre esigenze. Da poco aperto in piazza Maestri del 
                                Lavoro (zona viale Toselli), in una location facilmente raggiungibile in auto e 
                                dotata di ampie possibilità di parcheggio, Wok sushi Siena è un must per chi cerca 
                                una cucina multietnica. Sia che vogliate assaggiare qualcosa di diverso dal solito, 
                                sia che cerchiate un posto per fare un pasto sostanzioso ma veloce in una pausa la 
                                nostra cucina saprà soddisfarvi. Wok Sushi Siena vi offre inoltre la possibilità di 
                                scegliere quello che volete e portarvelo a casa, magari per una cena in cui volete 
                                stupire qualcuno.
                            </p>
                        </div>
                        <div class="col-sm-2"><img src="img/ristorante2.png" class="img-circle"></div>
                    </div>                
                </div>
            </li>