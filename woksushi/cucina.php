            <li class="content" id="page02">
                <div class="col-sm-10 col-sm-offset-1 whiteLayer">
                    <h1>LA NOSTRA CUCINA</h1>
                    <div class="row">
                        <div class="col-sm-2 col-sm-offset-1"><img src="img/cinese.png" class="img-circle" alt="Cucina cinese"></div>
                        <div class="col-sm-2 col-sm-offset-2"><img src="img/sushi.png" class="img-circle" alt="Cucina giapponese - Sushi"></div>
                        <div class="col-sm-2 col-sm-offset-2"><img src="img/internazionale.png" class="img-circle" alt="Cucina internazionale"></div>
                    </div>
                    <p>
                        La cultura gastronomica ha ormai abbattuto ogni barriera. Le culture e gli antichi saperi continuano 
                        a vivere in armonia l'uno con l'altro in una cucina multietnica. È così anche al Wok Sushi di Siena, 
                        dove le tecniche culinarie provenienti dall'Asia riescono a coniugarsi con quelle occidentali. 
                        Accanto a piatti preparati “all'italiana” potrete scegliere tra innumerevoli proposte, come il nostro 
                        sushi, preparato al momento o addirittura potrete scegliere i vostri ingredienti da far cuocere sulla 
                        piastra oppure nella padella wok. Il wok è probabilmente il simbolo della cucina cinese nel mondo, le 
                        sue caratteristiche consentono una cottura molto rapida, in modo da mantenere inalterate le proprietà 
                        del cibo che risulta meno grasso e più saporito.
                    </p>
                
                </div>
            </li>