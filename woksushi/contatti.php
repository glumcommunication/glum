            <div class="content" id="page06">
                <div class="col-sm-10 col-sm-offset-1 transLayer">
                    <h1>CONTATTI</h1>
                </div>
                <div class="col-sm-6 col-sm-offset-3">
<?php
if ($_POST['doSend']=='Invia') {
$name = $_POST[name];
$email = $_POST[email];
$message = $_POST[message];
$headers = "";
$headers .= "From: $email\n";
$headers .= "Reply-To: $email\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=utf-8\n";
$to = "info@woksushisiena.it";
$subject = "Richiesta informazioni";
/*$message = $messaggio;*/
$messagebody = "Hai ricevuto una nuova email via woksushisiena.it, ecco i dettagli:<br />
Nome: $name <br />

E-mail: $email<br />

Messaggio: $message<br />";

mail ($to,$subject,$messagebody,$headers);
$sent = "1";
}
if ($sent=='1') {
echo <<<_END
<div class="alert alert-success">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>Messaggio inviato!</h4>
  Sarai ricontattato al pi&ugrave; presto.
</div>
_END;
}
?>
                <form action="index.php#contatti" method="post" id="contactform">
                    <div class="form-group">
<!--                      <label for="name">Nome</label>-->
                      <input type="text" class="form-control" id="name" name="name" placeholder="Nome">
                    </div>
                    <div class="form-group">
<!--                      <label for="email">Email</label>-->
                      <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                    </div>
                    <div class="form-group">
<!--                      <label for="email">Email</label>-->
                      <input type="phone" class="form-control" id="phone" name="phone" placeholder="Telefono">
                    </div>                    
                    <div class="form-group">
<!--                      <label for="message">Messaggio</label>-->
                      <textarea class="form-control" rows="7" id="message" name="message" placeholder="Messaggio"></textarea>
                    </div>
                    <div class="checkbox">
                        <label>
                          <input type="checkbox" value="Yep" name="privacy">
                            <a id="privacy" href="#" rel="tooltip" data-toggle="tooltip" title="Nel inviare i miei dati dichiaro di aver letto l'informativa e sono consapevole che il trattamento degli stessi è necessario per ottenere il servizio proposto. A tal fine, nel dichiarare di essere maggiorenne, fornisco il mio consenso." data-placement="right" style="font-size:14px;cursor: help;">
                                Informativa sulla Privacy ai sensi del D.Lgs 196/2003</a>
                        </label>
                    </div>
                    <button type="submit" class="btn btn-default" name="doSend" value="Invia">Invia</button>
                  </form>
                
                </div>
                <div class="col-sm-6 col-sm-offset-3">
                    <p class="telephone">
                        Telefono: +39 0577 530080 - Cellulare: +39 393 6459999<br>
                        Email: <a href="mailto:info@woksushisiena.it">info@woksushisiena.it</a>
                    </p>    
                </div>
            </div>