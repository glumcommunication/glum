<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
        <meta name="viewport" content="width=1170">
        <script src="js/respond.js"></script>
        <link rel="icon" href="img/favicon.png" type="image/svg" />
        <link href='http://fonts.googleapis.com/css?family=Bree+Serif|Pathway+Gothic+One' rel='stylesheet' type='text/css'>
        <meta name="description" content="Wok Sushi Siena: cucina multietnica in piazza Maestri del Lavoro 31 (int.1); formula “all you can eat” a prezzo fisso; anche piatti da asporto.">
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/lightbox.css" rel="stylesheet" />
        <link href="css/normalize.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet" media="screen">
        <meta name="keywords" content="Wok Sushi, Siena, cucina etnica, multietnica, asiatica, Cinese, Giapponese, Italiana, buffet, all you can eat, prezzo fisso, wok, espresso, piastra, cottura espressa, nigiri, tempura, ravioli, menù, pranzo, cena, riso, pasta, carne, pesce, fusion, ristorante, parcheggio, automobile">
        <script src="js/respond.js"></script>
		<script src="js/selectivizr-min.js"></script> 

<?php
    include_once 'map.php';
?>
        <title><?php echo $pagetitle ?> | Piazza Maestri del Lavoro, 31/Interno 1 (Zona Viale Toselli) - 53100 Siena</title>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48014602-1', 'woksushisiena.it');
  ga('send', 'pageview');

</script>        
    </head>
    <body onload="initialize()">

        <div id="wrapper">
