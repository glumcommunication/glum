

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    function initialize() {
        var latlng = new google.maps.LatLng(43.317181, 11.352015);
        var settings = {
            zoom: 15,
            center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
            navigationControl: true,
            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
            mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"), settings);

  var companyLogo = new google.maps.MarkerImage('img/positionshadow.png',
    new google.maps.Size(86,58),
    new google.maps.Point(0,0),
    new google.maps.Point(20,50)
);

var companyPos = new google.maps.LatLng(43.317181, 11.352015);
var companyMarker = new google.maps.Marker({
    position: companyPos,
    map: map,
    icon: companyLogo,
    //shadow: companyShadow,
    title:"Centro Estetica Ego"
});
var contentString = '<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h1 id="firstHeading" class="firstHeading" style="font-size:14px;font-weight:bold;border-bottom: none;margin-bottom:5px;">Wok Sushi Siena</h1>'+
    '<div id="bodyContent">'+
    '<p style="font-size:12px;">Piazza Maestri del Lavoro 31/Interno 1<br>53100 Siena<br>Telefono: +39 0577 530080 - Cellulare: +39 393 6459999<br>Email: <a href="mailto:info@woksushisiena.it">info@woksushisiena.it</a><br>Website: <a href="http://www.woksushisiena.it" title="Wok Sushi Siena">www.woksushisiena.it</a><br></p>'+
    '</div>'+
    '</div>';
 
var infowindow = new google.maps.InfoWindow({
    content: contentString
});
google.maps.event.addListener(companyMarker, 'click', function() {
  infowindow.open(map,companyMarker);
});
    }
    </script>