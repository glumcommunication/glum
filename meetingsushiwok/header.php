<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
<META NAME="DESCRIPTION" CONTENT="<?php echo $description ?>">
<META NAME="KEYWORDS" CONTENT="ristorante cinese, ristorante giapponese, ristorante italiano, cucina cinese, cucina giapponese, cucina italiana, sushi, 
all you can eat, menu prezzo fisso, kaiten, siena, parcheggio gratuito, colonna san marco, toscana, carta dei vini, sashimi, sake, wok, cucina a vista">

	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="icon" href="images/favicon.png" type="image/svg"/>
<!--	<link rel="stylesheet" type="text/css" href="js/MooFlow.css" />
<link rel="stylesheet" type="text/css" href="js/milkbox.css" />-->
<link href="css/flexslider.css" rel="stylesheet" type="text/css" />
 <!--   <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />-->
 	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="js/contentflow.js" load="lightbox"></script>
    <script tyle="text/javascript">
        var cf = new ContentFlow('ContentFlow_lightbox', {reflectionColor: "#000000", circularFlow: "true", startItem: "0"});
               ContentFlowGlobal.setAddOnConf('lightbox');
    </script>
    <script>

    </script>    

   

<?php
if($pagetitle == "Ristorante") {
echo <<<_END
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>

	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script src="js/jquery.flexslider.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('.flexslider').flexslider({
        animation: "fade",
        slideshow: true,
		  slideshowSpeed: 7000,
      });
  });
</script>
_END;
}
/*
if($pagetitle == "Foto") {
echo <<<_END
<script src="js/mootools-1.2-core.js"></script>
<script src="js/mootools-1.2-more.js"></script>
	<script src="js/MooFlow.js"></script>
	<script src="js/milkbox.js"></script>
<script type="text/javascript">
var myMooFlowPage = {

	start: function(){

		var mf = new MooFlow($('MooFlow'), {
			useSlider: true,
			useCaption: true,
			useMouseWheel: true,
			useKeyInput: true,
			useViewer: true,
			onClickView: function(obj){
				Milkbox.showThisImage(obj.href, obj.title + ' - ' + obj.alt);
			}
		});	
	}
	
};

window.addEvent('domready', myMooFlowPage.start);
</script>	
_END;
}
*/
if($pagetitle == "Contatti") {
echo <<<_END

    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
_END;
}
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-36197103-1', 'meetingsushiwok.it');
  ga('send', 'pageview');

</script>

<style>img{max-width:none;}
#header img {max-width: 100%;}
.leftContent img {max-width: 100%;}
.hBar img {max-width: 100%;}
</style>

	<title>Meeting Sushi Wok | <?php echo $pagetitle ?></title>
</head>
<?php
if($pagetitle == "Contatti") {
echo <<<_END
<body onload="initialize()">
_END;
}
else {
echo <<<_END
<body>
_END;
}
?>
<div style="margin: 0 auto;width: 97.9166666%;width:1000px;">
	<div id="container">
		<div id="header">
			<div class="logo">
				<a href="http://www.meetingsushiwok.com/"><img src="images/logodark.png"></a>
			</div>
			<div class="menu">
				<div class="menutop">
					<ul>
						<li><a href="index.php">RISTORANTE</a></li>
						<li><a href="cucina.php">CUCINA</a></li>
						<li><a href="foto.php">FOTO</a></li>
						<li><a href="contatti.php">CONTATTI</a></li>
					</ul>
				</div>	
			 <div class="menubottom"><img src="images/bacchettacinesenera.png" alt="menu"></div>
			</div>
			<div>
				<a href="https://www.facebook.com/pages/Meetingsushiwok/352322378182645?fref=ts" target="_blank" title="Facebook"><img src="images/facebook.png" alt="Facebook" class="social" /></a>
				<a href="https://twitter.com/MeetingSushiWok" target="_blank" title="Twitter"><img src="images/twitter.png" alt="Facebook" class="social" /></a>				
			</div>
		</div>