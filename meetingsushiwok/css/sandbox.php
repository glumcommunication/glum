<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="icon" href="images/favicon.ico" type="image/svg"/>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

    <script type="text/javascript">
    function initialize() {
		var latlng = new google.maps.LatLng(43.30988, 11.315803);


		var settings = {
			zoom: 17,
			center: latlng,
			mapTypeControl: true,
			mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
			navigationControl: true,
			navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
			mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("rightContent"), settings);
        var contentString = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Meeting Sushi Wok</h1>'+
        '<div id="bodyContent">'+
        '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>'+
        '</div>'+
        '</div>';
 
        var infowindow = new google.maps.InfoWindow({
        content: contentString
        });

        var companyLogo = new google.maps.MarkerImage('images/cursor_s.png',
        new google.maps.Size(95,73),
	    new google.maps.Point(0,0),
	    new google.maps.Point(24,73)
);
        var companyPos = new google.maps.LatLng(43.30988, 11.315803);
        var companyMarker = new google.maps.Marker({
        position: companyPos,
        map: map,
        icon: companyLogo,
        title:"Meeting Sushi Wok"
  });  
  google.maps.event.addListener(companyMarker, 'click', function() {
  infowindow.open(map,companyMarker);
});
    }
</script>



	<title>Meeting Sushi Wok | <?php echo $pagetitle ?></title>
</head>
<body onload="initialize()">

<title></title>
</head>
<body>
<div id="map_canvas" style="width:512px;height:512px;"></div>
</body>
</html>