<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
<META NAME="DESCRIPTION" CONTENT="<?php echo $description ?>">
<META NAME="KEYWORDS" CONTENT="ristorante cinese, ristorante giapponese, ristorante italiano, cucina cinese, cucina giapponese, cucina italiana, sushi, all you can eat, menu prezzo fisso, kaiten, siena, parcheggio gratuito, colonna san marco, toscana, carta dei vini, sashimi, sake, wok, cucina a vista">
<link rel="icon" href="images/favicon.png" type="image/svg"/>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="js/MooFlow.css" />
<link rel="stylesheet" type="text/css" href="js/milkbox.css" />
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>


<?php
if($pagetitle == "Foto") {
echo <<<_END


<script src="js/mootools-1.2-core.js"></script>
<script src="js/mootools-1.2-more.js"></script>
	<script src="js/MooFlow.js"></script>
	<script src="js/milkbox.js"></script>

<script type="text/javascript">
var myMooFlowPage = {

	start: function(){
		/* MooFlow instance with the Milkbox Viewer */
		var mf = new MooFlow($('MooFlow'), {
			useSlider: true,
			useCaption: true,
			useMouseWheel: true,
			useKeyInput: true,
			useViewer: true,
			onClickView: function(obj){
				Milkbox.showThisImage(obj.href, obj.title + ' - ' + obj.alt);
			}
		});	
	}
	
};

window.addEvent('domready', myMooFlowPage.start);
</script>	
_END;
}
?>



<style>img{max-width:none;}
#header img {max-width: 100%;}
.leftContent img {max-width: 100%;}
.hBar img {max-width: 100%;}
</style>

	<title>Meeting Sushi Wok | <?php echo $pagetitle ?></title>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36197103-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>	
</head>
<body>
<div style="margin: 0 auto;width: 97.9166666%;width:1000px;">
	<div id="container">
		<div id="header">
			<div class="logo">
				<a href="http://www.meetingsushiwok.it/"><img src="images/logodark.png"></a>
			</div>
			<div class="menu">
				<div class="menutop">
					<ul>
						<li><a href="index.php">RISTORANTE</a></li>
						<li><a href="cucina.php">CUCINA</a></li>
						<li><a href="foto.php">FOTO</a></li>
						<li><a href="contatti.php">CONTATTI</a></li>				
					</ul>
				</div>	
			 <div class="menubottom"><img src="images/bacchettacinesenera.png" alt="menu"></div>
			</div>
		</div>