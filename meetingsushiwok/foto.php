<?php
$pagetitle = "Foto";
$description = "Galleria fotografica di Meeting Sushi Wok, ristorante  giapponese, cinese e italiano. Guarda i piatti e il locale per scoprire un piccolo angolo d'oriente a Siena.
";
include 'header.php';

?>

<!--https://maps.google.it/maps?q=siena+massetana+romana&hl=it&ll=43.30988,11.315803&spn=0.001745,0.004128&sll=41.29085,12.71216&sspn=29.441464,67.631836&hnear=Strada+Massetana+Romana,+Siena&t=h&z=19-->



        <div id="main">
			<div class="content">

    <div id="ContentFlow_lightbox" class="ContentFlow ContentFlowAddOn_lightbox" useAddOns="lightbox">
        <!-- should be place before flow so that contained images will be loaded first -->
        <div class="loadIndicator"><div class="indicator"></div></div>
			
        <div class="flow">

            <div class="item">
    											<img class="content" src="images/gal/msw01.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw02.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw03.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw04.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw11.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw12.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw13.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw14.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw15.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw16.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw17.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw18.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw22.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw23.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw24.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw32.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw33.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw34.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw35.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw36.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw37.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw38.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw46.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw47.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw48.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw49.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw50.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw51.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw53.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw54.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw55.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw56.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw57.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw05.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw06.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw07.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw08.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw09.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw10.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw19.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw20.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw21.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw25.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw26.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw27.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw28.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw29.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw30.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>   
            <div class="item">
    											<img class="content" src="images/gal/msw31.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw39.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw40.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw41.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw42.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw43.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw44.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>
            <div class="item">
    											<img class="content" src="images/gal/msw45.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>   
            <div class="item">
    											<img class="content" src="images/gal/msw52.jpg" alt="Meeting Sushi Wok"/>
                <div class="caption"></div>
            </div>              


        </div>
        <div class="globalCaption"></div>
        <div class="scrollbar">
            <div class="slider"><div class="position"></div></div>
        </div>
        </div>
<!--  <div id="MooFlow">
    <a href="images/gal/msw01.jpg">
    <img src="images/gal/msw01.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw02.jpg">
    <img src="images/gal/msw02.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw03.jpg">
    <img src="images/gal/msw03.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw04.jpg">
    <img src="images/gal/msw04.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw11.jpg">
    <img src="images/gal/msw11.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw12.jpg">
    <img src="images/gal/msw12.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw13.jpg">
    <img src="images/gal/msw13.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw14.jpg">
    <img src="images/gal/msw14.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw15.jpg">
    <img src="images/gal/msw15.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw16.jpg">
    <img src="images/gal/msw16.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw17.jpg">
    <img src="images/gal/msw17.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw18.jpg">
    <img src="images/gal/msw18.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw22.jpg">
    <img src="images/gal/msw22.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw23.jpg">
    <img src="images/gal/msw23.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw24.jpg">
    <img src="images/gal/msw24.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>   
    <a href="images/gal/msw32.jpg">
    <img src="images/gal/msw32.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw33.jpg">
    <img src="images/gal/msw33.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw34.jpg">
    <img src="images/gal/msw34.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw35.jpg">
    <img src="images/gal/msw35.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw36.jpg">
    <img src="images/gal/msw36.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw37.jpg">
    <img src="images/gal/msw37.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw38.jpg">
    <img src="images/gal/msw38.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw46.jpg">
    <img src="images/gal/msw46.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw47.jpg">
    <img src="images/gal/msw47.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw48.jpg">
    <img src="images/gal/msw48.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw49.jpg">
    <img src="images/gal/msw49.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw50.jpg">
    <img src="images/gal/msw50.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw51.jpg">
    <img src="images/gal/msw51.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw53.jpg">
    <img src="images/gal/msw53.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw54.jpg">
    <img src="images/gal/msw54.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw55.jpg">
    <img src="images/gal/msw55.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw56.jpg">
    <img src="images/gal/msw56.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw57.jpg">
    <img src="images/gal/msw57.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw05.jpg">
    <img src="images/gal/msw05.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw06.jpg">
    <img src="images/gal/msw06.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>   
    <a href="images/gal/msw07.jpg">
    <img src="images/gal/msw07.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw08.jpg">
    <img src="images/gal/msw08.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw09.jpg">
    <img src="images/gal/msw09.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw10.jpg">
    <img src="images/gal/msw10.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw19.jpg">
    <img src="images/gal/msw19.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw20.jpg">
    <img src="images/gal/msw20.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw21.jpg">
    <img src="images/gal/msw21.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw25.jpg">
    <img src="images/gal/msw25.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw26.jpg">
    <img src="images/gal/msw26.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw27.jpg">
    <img src="images/gal/msw27.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw28.jpg">
    <img src="images/gal/msw28.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw29.jpg">
    <img src="images/gal/msw29.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw30.jpg">
    <img src="images/gal/msw30.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw31.jpg">
    <img src="images/gal/msw31.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw.jpg39">
    <img src="images/gal/msw39.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw40.jpg">
    <img src="images/gal/msw40.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw41.jpg">
    <img src="images/gal/msw41.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw42.jpg">
    <img src="images/gal/msw42.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw43.jpg">
    <img src="images/gal/msw43.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw44.jpg">
    <img src="images/gal/msw44.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>   
    <a href="images/gal/msw45.jpg">
    <img src="images/gal/msw45.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
    <a href="images/gal/msw52.jpg">
    <img src="images/gal/msw52.jpg" data-milkbox="msw" title="" alt="Meeting Sushi Wok" />
    </a>
         
    </div>-->


			
		</div>
    </div>
<?php
include 'footer.php';
?>