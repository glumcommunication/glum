<?php
$pagetitle = "Ristorante";
$description = "Meeting Sushi Wok, ristorante giapponese, cinese e italiano, si trova a Siena. Con kaiten (tapis roulant) di 15 metri e con cucina a vista, il ristorante invita ad aprirsi al gusto e alla bellezza delle pietanze. Aperto tutti i giorni a pranzo e a cena.";
include 'header.php';
?>

    	<div id="main">
			<div class="content">
				<div class="leftContent" style="padding-top:80px;">
        <div class="flexslider">
          <ul class="slides">
            <li>
  	    	    <img src="images/rDSCN0505.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/rDSCN0519.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/rDSCN0569.jpg" />
  	    		</li>
  	    		<li>
  	    	    <img src="images/rDSCN0593.jpg" />
  	    		</li>  	    	
       </ul>
        </div>
										<h3 style="text-align: center;">Cucina a vista<br>Kaiten di 15 metri</h3>
				</div>
				<div class="rightContent">
					<h2>
					IL RISTORANTE
					</h2>
					<p>Il Ristorante Meeting Sushi Wok è un piccolo angolo d’oriente a due passi dal centro storico di Siena, 
					facilmente raggiungibile in auto. L'ambiente raffinato e luminoso, il servizio rapido e cortese, permetteranno di 
					trascorrere momenti magici gustando piatti tipici della tradizione giapponese, cinese e italiana.</p>
					<p>Meeting Sushi Wok è il primo ristorante orientale a Siena con kaiten (tapis roulant): sushi, sashimi ed altre 
					prelibatezze scorrono davanti ai commensali; la cucina a vista, dal design minimale, invita ad aprirsi al gusto e 
					alla bellezza delle pietanze.</p>
					<p>Il ristorante Meeting Sushi Wok offre anche il servizio a buffet con la formula “all you can eat”: mangi quello 
					che vuoi, quanto vuoi, ad un unico prezzo.<br>
					Servirti dal ricco buffet e dal nastro sushi: ampia scelta di pietanze, cotte e crude, antipasti caldi e freddi, 
					carni, pesce, verdura, dolci e gelati a volontà con la formula del tutto compreso in un unico prezzo. Scegli quello 
					che preferisci. Il prezzo è fisso (bevande escluse). Per non accedere negli sprechi è previsto un piccolo supplemento 
					per i clienti che a fine pasto non avranno terminato le portate prese.</p>
					<p>Ottimo per una serata romantica o per un piacevole ritrovo tra amici, adatto anche a famiglie, per feste o cerimonie 
					e incontri di lavoro.</p>
					<p><strong>Aperto tutti i giorni.</strong>
					</p>

				</div>

			<div class="hBar">
				<img src="images/bacchettacinesebianca.png" alt="">
			</div>
				<div class="leftContent">
					<div style="float:left;width:100%;">
					<h3>Pranzo 10,90 <span class="smaller">euro</span><br>Cena 16,90 <span class="smaller">euro</span></h3>
					<p class="disclaimer">I bambini fino a 8 anni di età pagano la metà.<br>Le bevande sono escluse.</p>
					</div>
					<div style="float:left;width:50%;">
					<h3>Orari di apertura<br>
					<span style="color: #ffffff;">12.00 - 15.00<br>
					19.30 - 23.30</span>
					</h3>
					</div>
					<div style="float:right;width:50%;padding-top:25px;">
					<img src="images/aperti.png" alt="">
					</div>
					</div>
				<div class="rightContent">
				<div style="float:left;width:100%;padding-top:30px;">
					<img src="images/unicoprezzo.png" alt="">
				</div>
				</div>								
			</div>			
		</div>
<?php

include 'footer.php';
?>