<?php
$pagetitle = "Cucina";
$description = "Ampia scelta di pietanze giapponesi, cinesi e italiani,  piatti di qualità, servizio impeccabile e ricca cantina di vini. Il ristorante Meeting Sushi Wok offre anche il servizio a buffet con la formula “all you can eat” ad un unico prezzo. Il luogo ideale per dedicarsi una parentesi gastronomica a pranzo e a cena. ";
include 'header.php';
?>

			<div class="content">
				<div class="leftContent" style="padding-top:80px;">
					<img src="images/gal/msw39.jpg" alt="Cucina giapponese" style="box-shadow: 0 0 20px white;">
				</div>
				<div class="rightContent">
					<h2>
					CUCINA GIAPPONESE
					</h2>
					<p>La caratteristica principale della cucina nipponica è l’utilizzo di materie prime di eccellente qualità, al fine 
					di rispettare la freschezza, la genuinità, i sapori, il colore, la consistenza e la stagionalità di ogni alimento.<br> 
					Da Meeting Sushi Wok potrete trovare diverse specialità giapponesi per accontentare tutti i gusti: il Sushi, la Tempura 
					leggera e croccante, e il Teppanyaki.<br>
					Il sushi è una piccola composizione di riso cotto con una tecnica particolare insieme all’aceto e al mirin e servito 
					con uova, pesce o alghe. Il pesce crudo compone il sashimi che viene servito con verdure che lo decorano, sempre 
					seguendo la filosofia che il cibo deve nutrire sia il corpo che l’anima.<br>
					La tempura è una frittura croccante e leggera, fatta con verdure e pesce. La sua storia è molto antica: si narra 
					che durante il XVI secolo alcuni missionari portoghesi giunsero sino in Giappone e fecero conoscere la tradizione 
					di non mangiare carne durante la quaresima. Questa consuetudine non solo fu rispettata dai giapponesi, ma fu anche 
					personalizzata proponendo fritture con pastella leggera per rispettare la tradizione.<br>
					La cucina teppanyaki infine nasce dalla volontà di trovare il modo di cucinare e mangiare nello stesso "piatto". 
					Si può servire direttamente con le bacchette dalla piastra o da piatti che si trovano nella parte normale del tavolo. 
					Una vasta scelta per fare propria una cultura ricca di storia e di tradizione come il Giappone. Da Meeting Sushi Wok 
					puoi trovare i migliori piatti per vivere una sensazione di gusto e benessere orientale.
					</p>

				</div>

			<div class="hBar">
				<img src="images/bacchettacinesebianca.png" alt="">
			</div>
				<div class="leftContent" style="padding-top:80px;">
					<img src="images/gal/msw28.jpg" alt="Cucina cinese" style="box-shadow: 0 0 20px white;">
				</div>
				<div class="rightContent">
					<h2>
					CUCINA CINESE
					</h2>
					<p>Alla base della tradizione cinese, vi è il principio dell'equilibrio: come nell’arte e nell’architettura orientale 
					lo stesso principio armonico si traduce nella parità dei sapori. In ogni piatto proposto dal ristorante Meeting Sushi 
					Wok di Siena, nessun sapore deve prendere il sopravvento sull’altro, essi devono completarsi l’un l’altro in un 
					tripudio di sapori armonici ed equilibrati.<br>
					La cucina cinese è legata anche alla filosofia e alla medicina. Gli alimenti yin, femminili, umidi, teneri e 
					rinfrescanti, sono i legumi e i frutti. Gli alimenti yang, maschili, fritti, speziati o a base di carne hanno un 
					effetto riscaldante. Un pasto deve non soltanto armonizzare i gusti, ma ugualmente trovare un equilibrio tra il 
					freddo e il caldo.<br>
					Un'altra particolarità che caratterizza la cucina tradizionale cinese è l'assenza di prodotti lattieri a causa di 
					un'intolleranza al lattosio che esiste in numerosi paesi asiatici.<br>
					Nei menù cinesi prevalgono pietanze caratterizzate dal notevole impatto estetico e dai sapori contrastanti ma mai 
					fastidiosi al palato. 
					</p>


				</div>

			<div class="hBar">
				<img src="images/bacchettacinesebianca.png" alt="Bacchetta bianca">
			</div>
				<div class="leftContent" style="padding-top:80px;">
					<img src="images/gal/msw29.jpg" alt="Cucina italiana" style="box-shadow: 0 0 20px white;">
				</div>
				<div class="rightContent">
					<h2>
					CUCINA ITALIANA
					</h2>
					<p>Per accontentare tutti i gusti, il ristorante  Meeting Sushi Wok di Siena offre una vasta scelta di piatti italiani, 
					all'insegna dell'integrazione con la tradizione toscana:  sia pur rivisitati, ma in modo raffinato, non si 
					discostano mai dai sapori desiderati. La maestria dei cuochi e la genuinità dei prodotti usati, costituiscono il 
					vanto e la filosofia del ristorante Meeting Sushi Wok.<br>
					Un cuoco italiano si occuperà di preparare piatti tipici italiani e toscani la cui caratteristica principale è l'estrema 
					semplicità. L'atmosfera avvolgente, i piatti di qualità, il servizio impeccabile e la ricca cantina di vini, rendono 
					Meeting Sushi Wok il luogo ideale per dedicarsi una parentesi gastronomica a pranzo e a cena. <br>
					Che sia il gustoso accompagnamento ad un meeting, la giusta conclusione di una convention o un incontro con gli amici, 
					il nostro servizio, mette in campo tutta la passione che contraddistingue Meeting Sushi Wok per soddisfare le esigenze 
					più diverse.
					</p>

				</div>


		</div>
<?php
$pagetitle = "HOME";
include 'footer.php';
?>