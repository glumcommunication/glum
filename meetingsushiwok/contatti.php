<?php
$pagetitle = "Contatti";
$description = "Contatta Meeting Sushi Wok, ristorante  giapponese, cinese e italiano. Tel.: 0577 226958  Email: info@meetingsushiwok.it . Indirizzo Via Paolo Frajese 63 (Colonna San Marco), Siena.";
include 'header.php';

?>

<!--https://maps.google.it/maps?q=siena+massetana+romana&hl=it&ll=43.30988,11.315803&spn=0.001745,0.004128&sll=41.29085,12.71216&sspn=29.441464,67.631836&hnear=Strada+Massetana+Romana,+Siena&t=h&z=19-->



        <div id="main">
			<div class="content">
				<div class="leftContent" style="padding-top:35px;">
					<h3>
					CONTATTI<br>
										<span style="color: #ffffff;">Ristorante Meeting Sushi Wok<br>
					</h3>
					<div style="float:left;width:100%;">
					<p style="color:white;text-align:left;font-size:0.875em;">Via Paolo Frajese 63 (Colonna San Marco)<br>53100 Siena<br>Tel.: 0577 226958<br>Email: <a href="mailto:info@meetingsushiwok.it">info@meetingsushiwok.it</a></p>
					</div>
					<div style="float:left;width:50%;">
					<h3>Orari di apertura<br>
					<span style="color: #ffffff;">12.00 - 15.00<br>
					19.30 - 23.30</span>
					</h3>
					</div>
					<div style="float:right;width:50%;padding-top:25px;">
					<img src="images/aperti.png" alt="">
					</div>					
				</div>
				<div class="rightContent">



<script type="text/javascript">
    function initialize() {
		var latlng = new google.maps.LatLng(43.30932, 11.315215);


		var settings = {
			zoom: 17,
			center: latlng,
			mapTypeControl: true,
			mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
			navigationControl: true,
			navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
			mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"), settings);
        var contentString = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Meeting Sushi Wok</h1>'+
        '<div id="bodyContent">'+
        '<p>Via Paolo Frajese 63 (Colonna San Marco) - 53100 SIENA<br>Tel.: 0577 226958<br>Email: info@meetingsushiwok.it</p>'+
        '</div>'+
        '</div>';
 
        var infowindow = new google.maps.InfoWindow({
        content: contentString
        });

        var companyLogo = new google.maps.MarkerImage('images/cursor_s.png',
        new google.maps.Size(95,73),
	    new google.maps.Point(0,0),
	    new google.maps.Point(24,73)
);
        var companyPos = new google.maps.LatLng(43.30932, 11.315215);
        var companyMarker = new google.maps.Marker({
        position: companyPos,
        map: map,
        icon: companyLogo,
        title:"Meeting Sushi Wok"
  });  
  google.maps.event.addListener(companyMarker, 'click', function() {
  infowindow.open(map,companyMarker);
});
    }
</script>

                <div id="map_canvas" style="width:100%;height:360px;max-width:none;">
                
                </div>

		
            </div>
			
		</div>
    </div>
<?php
include 'footer.php';
?>