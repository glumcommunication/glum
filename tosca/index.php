<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en">
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en">
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Agrituscan -  Pasta La Tosca</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <script src="js/modernizr.min.js"></script>

        <link rel="stylesheet" href="css/bootstrap.css">
                <link rel="stylesheet" href="css/jquery.countdown.css">

        <link rel="stylesheet" href="stylesheets/style.css">
        <link href='http://fonts.googleapis.com/css?family=Roboto:700,400' rel='stylesheet' type='text/css'>
        <link id="page_favicon" href="/favicon.ico" rel="icon" type="image/x-icon" />

        <script src="js/jquery-2.1.1.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/jquery.plugin.js"></script>
        <script src="js/jquery.countdown.js"></script>
        <script type="text/javascript" src="js/jquery.countdown-it.js"></script>
        <script>
            $(function() {


                $('#defaultCountdown').countdown({until: new Date(2014, 9 - 1, 30)});
                $('#defaultCountdown').countdown($.countdown.regionalOptions['it']);

            });
        </script>
    </head>
    <body>
        <div class="container-fluid cover">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <img class="img-responsive" src="img/loghi.png">
                    <p id="titolo">STIAMO ARRIVANDO</p>
                </div>
            </div>


            <div class="row" >

                <div class="col-sm-12 " id="bandablu">
                 
                    <div  class="col-sm-6 col-sm-offset-3" id="defaultCountdown"></div>

                </div>

            </div>

        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <p id="contatti">CONTATTI</p>
                </div>
                <div class="col-sm-8 col-sm-offset-2">
                    <img src="img/b1.png" class="img-responsive">
                     <br/>
                </div>
                   
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="contatti col-sm-4 text-center">
                        <p class="bold">Indirizzo</p>
                           societ&agrave; Cooperativa a r.l.<br/>
                            Viale Europa, 1<br/>
                            53100 Siena (SI)<br/>
                            Italia
                        </p>
                    </div>
                    <div class="contatti col-sm-4 text-center">
                        <p class="bold">Telefono</p>
                        tel: +39 0577 281069<br/>
                        fax: +39 0577271281
                        
                    </div>
                    <div class="contatti col-sm-4 text-center">
                        <p class="bold">Email</p>
                        <a href="mailto:info@agrituscan.com">info@agrituscan.com</a>
                    
                    </div>
                </div>
                       <div class="col-sm-8 col-sm-offset-2">
                    <img src="img/b2.png" class="img-responsive">
                        <br/><br/>
                </div>
            </div>
        </div>

    </div>
    <div class="container-fluid">
        <div class="row">

        </div>
    </div>



    <footer>
    </footer>
    <!--<script src="js/plugins.js"></script>
    <script src="js/main.js"></script>-->


</body>
</html>
