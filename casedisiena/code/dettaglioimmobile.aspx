<%@ Page Title="" Language="C#" MasterPageFile="~/ASPX/casedisiena.Master" AutoEventWireup="true" CodeBehind="dettaglioimmobile.aspx.cs" Inherits="casedisiena.WebForm3" %>
<%@ Register Src="ASCX/agenzieleft.ascx" TagName="agenzieleft" TagPrefix="cds" %>
<%@ Register Src="ASCX/Login.ascx" TagName="login" TagPrefix="cds" %>
<%@ Register Src="ASCX/SmartRegistration.ascx" TagName="smartreg" TagPrefix="cds" %>
<%@ Register Src="ASCX/sponsorright.ascx" TagName="sponsorright" TagPrefix="cds" %>
<%@ Import Namespace="casedisiena" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">
                        <style>

                        /* Demo styles */
                        .content{color:white;font:12px/1.4 "helvetica neue",arial,sans-serif;width:580px;margin:20px auto;}
                        h1{font-size:12px;font-weight:normal;color:#ddd;margin:0;}
                        p{margin:0 0 20px}
                        a {color:#22BCB9;text-decoration:none;}
                        .cred{margin-top:20px;font-size:11px;}

                        /* This rule is read by Galleria to define the gallery height: */
                        #galleria{float:left;width:580px;height:386px;}

                    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">

    <%
        
        
        
        
        string mode = "SCHEDA";
        if (Request["MODE"] != null)
        {
            mode = Request["MODE"].ToString();
        }

        if (mode == "ADDPREFERITO" && SessionManager.GetObject("CURRENT_USER") == null)
        {
            
        }
        
        decimal idAnnuncio = Convert.ToDecimal(Request["ANN"].ToString());

         Annunci ann = AnnunciAccessHelper.SelectAnnunciByKey(idAnnuncio);

        
        
     %>

    <%string lingua="it";%>
    <div id="left">
        <cds:login id="login" runat="server"></cds:login>
        <cds:agenzieleft NumeroAgenzie="1" id="Agenzieleft1" runat="server"></cds:agenzieleft>
    </div>
    <div  id="centerfirst">

    <div id="BottoniImmobile">
        <a class="bottone_griglia" style="float:left;font-size:0.7em;" id="lnkScheda" href="dettaglioimmobile.aspx?MODE=SCHEDA&ANN=<%=idAnnuncio.ToString()%>" >Scheda</a>
        <a class="bottone_griglia" style="float:left;font-size:0.7em;margin-left: 10px;" id="lnkVideo" href="dettaglioimmobile.aspx?MODE=VIDEO&ANN=<%=idAnnuncio.ToString()%>">Video</a>
        <asp:LinkButton runat="server" CssClass="bottone_griglia_large" style="float:right;font-size:0.7em;" 
            id="lnkAggiungiPreferito" 
            text="Aggiungi a preferiti" onclick="lnkAggiungiPreferito_Click"></asp:LinkButton>
    </div>


    <asp:panel  runat="server" id="pnlgallery">
    <div class="content">
       <div id="galleria">

<%
        decimal idAnnuncio = Convert.ToDecimal(Request["ANN"].ToString());

         Annunci ann = AnnunciAccessHelper.SelectAnnunciByKey(idAnnuncio);


            List<Img> lstimm = ann.ObjGallery.ElencoImmagini;  
                foreach(Img _img  in lstimm)
                {
                    string url = "../thumbnail.aspx?FN=public/" + _img.NomeFile + "&MW=" + "600px" + "&MH=" + "300";
                    string urlthb = "../thumbnail.aspx?FN=public/" + _img.NomeFile + "&MW=" + "100px" + "&MH=" + "100";
                    %>
                    
            <a href="<%=url%>">
                <img title="Locomotives Roundhouse"
                     alt=""
                     src="<%=urlthb%>">
                     
                    
                    <%
                }
                
                %>

           </a>
        </div>
    </div>
    </asp:panel>
    <asp:panel runat="server"  ID="pnlVideo">
            <div class="content">
                <asp:Literal ID="ltVideo" runat="server">
                </asp:Literal>
           
            </div>
    </asp:panel>
    <div ID="pnlScheda">
        <div id="imgdescrizione" style="font-size:0.8em;">
            <p>
            <b><%=TestiAccessHelper.SelectTestiByKey("descrizione", this.Lingua)%>  </b>
            </p>
            <p>
            <%=ann.Descrizione%>
            </p>
        </div>
        <div id="imgschedatecnica">
            
            <div class="divCategorie" style="margin:2px 2px 10px 2px;float:left;width:250px;font-size:0.8em;">
                <b>Contratto : </b> <%=TestiAccessHelper.SelectTestiByKey(DominiAccessHelper.SelectDominiByKey("ANNUNCIO_TIPO",ann.Tipo) ,this.Lingua) %><br />
                <b>Tipologia : </b> <%=TestiAccessHelper.SelectTestiByKey(DominiAccessHelper.SelectDominiByKey("IMM_CATEGORIA",ann.Categoria.ToString()) ,this.Lingua) %> - 
                <%=TestiAccessHelper.SelectTestiByKey(DominiAccessHelper.SelectDominiByKey("IMM_SUBCATEGORIA",ann.Subcategoria.ToString()) ,this.Lingua) %><br />
                <b>Prezzo : </b> <%=ann.Prezzo.ToString()%> &euro;<br />
                <b>Superficie : </b> <%=ann.Mq.ToString()%> Mq<br />
            </div>
            <div class="divlocalizzazione" style="margin:2px 2px 10px 2px;float:left;width:260px;font-size:0.8em;">
               <b>Localit&agrave;: </b> <%=((Comuni)SessionManager.Comuni[ann.Comune]).Nome%> - <%=((Aree)SessionManager.Aree[ann.Area]).Nome%> <br />
            </div>
            <div class="divNumeri" style="margin:2px 2px 10px 2px;float:left;width:260px;font-size:0.8em;">
                <div style="width:120px;float:left;">
                   <b>Locali : </b> <%=ann.Vani.ToString()%><br />
                </div>
                <div style="width:120px;float:left;">
                    <b>Camere : </b> <%=ann.Camere.ToString()%><br />
                </div>            
                <div style="width:120px;float:left;">
                    <b>Bagni : </b> <%=ann.Bagni.ToString()%><br />
                </div>
                <div style="width:120px;float:left;">
                    <b>Livello : </b> <%=ann.Livelli.ToString()%><br />
                </div>
            </div>
            <div class="divFlag" style="margin:2px 2px 10px 2px;float:left;width:250px;font-size:0.8em;">
                <%if (ann.Arredamento == 1)
                  {%>
                <input type="checkbox" checked="checked" value="Arredato" disabled="disabled" />&nbsp;<b>Arredato</b>
                <%}
                  else
                  { %>
                <input type="checkbox" disabled="disabled" />&nbsp;<b>Arredato</b>
                <%} %>
                <%if (ann.Terrazza == 1)
                  {%>
                <input type="checkbox" checked="checked" value="Arredato" disabled="disabled" />&nbsp;<b>Terrazza</b>
                <%}
                  else
                  { %>
                <input type="checkbox" disabled="disabled" />&nbsp;<b>Terrazza</b>
                <%} %>
                <%if (ann.Balcone == 1)
                  {%>
                <input type="checkbox" checked="checked" value="Arredato" disabled="disabled" />&nbsp;<b>Balcone</b>
                <%}
                  else
                  { %>
                <input type="checkbox" disabled="disabled" />&nbsp;<b>Balcone</b>
                <%} %>
                <%if (ann.Giardino == 1)
                  {%>
                <input type="checkbox" checked="checked" value="Arredato" disabled="disabled" />&nbsp;<b>Giardino</b>
                <%}
                  else
                  { %>
                <input type="checkbox" disabled="disabled" />&nbsp;<b>Giardino</b>
                <%} %>
                <%if (ann.Garage == 1)
                  {%>
                <input type="checkbox" checked="checked" value="Arredato" disabled="disabled" />&nbsp;<b>Garage</b>
                <%}
                  else
                  { %>
                <input type="checkbox" disabled="disabled" />&nbsp;<b>Garage</b>
                <%} %>
                <%if (ann.PostoAuto == 1)
                  {%>
                <input type="checkbox" checked="checked" value="Arredato" disabled="disabled" />&nbsp;<b>Posto Auto</b>
                <%}
                  else
                  { %>
                <input type="checkbox" disabled="disabled" />&nbsp;<b>Posto Auto</b>
                <%} %>
            </div>
            <div class="divAltreInfo" style="margin:2px 2px 10px 2px;float:left;width:260px;font-size:0.8em;">
             </div>



          

        </div>
	<div style="float:left;clear:both;padding-top:25px;">
        <div style="width:180px;height:70px;margin:10px 5px 5px 5px;float:left;vertical-align:middle;font-size:1em;">
            <asp:LinkButton ID="lnkRichiediInformazioni" runat="server" CssClass="bottone_griglia_large" Text="Richiedi Info"></asp:LinkButton>
        </div>
        <div style="width:190px;height:70px;margin:10px 5px 5px 5px;float:left;text-align:center;font-size:0.7em;color:Gray;vertical-align:middle;">
            <% Agenzia _ag = AgenziaAccessHelper.SelectAgenziaByKey(ann.EmailAgenzia); 
                
                %>
            
            <%=_ag.Nome %><br />
            <%=_ag.Indirizzo %><br />
            <%= _ag.Cap + " " + ComuniAccessHelper.SelectComuniByKey(_ag.Comune).Nome %>            <br />
            <%= "tel " + _ag.Telefono + " - fax " + _ag.Fax %><br />
            <%= _ag.Email %><br />
        </div>
        <div style="width:180px;height:70px;margin:10px 5px 5px 5px;float:left;vertical-align:middle;text-align:right;">
            <img src="public/<%=_ag.Logo%>" />
        </div>
    </div>
    </div>
    <div ID="pnlPlanimetria">
    </div>


    </div>
    <div id="right">
        <cds:sponsorright  id="sponsorleft1" runat="server"></cds:sponsorright>
    </div>
    <%if(Request["MODE"] == null || Request["MODE"] != "VIDEO")
    { %>
   <script>
       // Load the classic theme
       Galleria.loadTheme('Scripts/galleria/themes/classic/galleria.classic.min.js');

       // Initialize Galleria
       Galleria.run('#galleria');

    </script>
    <% } %>
    <cds:smartreg id="smartreg" runat="server"></cds:smartreg>
</asp:Content>
