<!DOCTYPE html>
<html>
<head>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script language="Javascript" type="text/javascript" src="js/jquery.lwtCountdown-1.0.js"></script>
	<script language="Javascript" type="text/javascript" src="js/misc.js"></script>
	<link rel="Stylesheet" type="text/css" href="style/main.css"></link>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<title>Case di Siena - Sta arrivando...</title>
	<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36496147-1']);
  _gaq.push(['_setDomainName', 'casedisiena.it']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body>

	<div id="container">

<!--		<h1>UNDER CONSTRUCTION</h1>
		<h2 class="subtitle">Stay tuned for news and updates.</h2>-->
<a href="/" name="Case di Siena - Home"><img src="images/logocasedisiena.png" width="700" alt=""></a>
		<!-- Countdown dashboard start -->
		<div id="countdown_dashboard">
			<div class="dash weeks_dash">
				<span class="dash_title">Settimane</span>
				<div class="digit"><?=$date['weeks'][0]?></div>
				<div class="digit"><?=$date['weeks'][1]?></div>
			</div>

			<div class="dash days_dash">
				<span class="dash_title">days</span>
				<div class="digit"><?=$date['days'][0]?></div>
				<div class="digit"><?=$date['days'][1]?></div>
			</div>

			<div class="dash hours_dash">
				<span class="dash_title">hours</span>
				<div class="digit"><?=$date['hours'][0]?></div>
				<div class="digit"><?=$date['hours'][1]?></div>
			</div>

			<div class="dash minutes_dash">
				<span class="dash_title">minutes</span>
				<div class="digit"><?=$date['mins'][0]?></div>
				<div class="digit"><?=$date['mins'][1]?></div>
			</div>

			<div class="dash seconds_dash">
				<span class="dash_title">seconds</span>
				<div class="digit"><?=$date['secs'][0]?></div>
				<div class="digit"><?=$date['secs'][1]?></div>
			</div>

		</div>
		<!-- Countdown dashboard end -->

<!--		<div class="dev_comment">
		Marco Luppoli è un bell'uomo.<br/>
			
		</div>-->

		<div class="subscribe">
			<form action="" method="POST" id="subscribe_form">
				<input type="text" name="email" id="email_field" class="faded" value="your@email.com" />
				<input type="submit" id="subscribe_button" value="Stay updated" onclick="this.disabled=true;this.value='Sending, please wait...';this.form.submit();" />
				<div id="error_message" class="form_message"<?=($error ? ' style="display: block"' : '')?>>
					<?=$error?>
				</div>
				<div id="info_message" class="form_message"<?=($info ? ' style="display: block"' : '')?>>
					<?=$info?>
				</div>
				<div id="loading">
					<img src="images/loading.gif" />
				</div>
			</form>
		</div>
<div id="tips"> 
  <p><a href="#"> <strong>Informativa sulla privacy</strong><span>
 Il sottoscritto dichiara di avere ricevuto completa informativa ai sensi dell’art. 13 D.L.gs. 196/2003 e di avere preso atto 
 dei diritti di cui all’art. 7 del D.L.gs medesimo “Diritto di accesso ai dati personali ed altri diritti”, ed esprime il 
 proprio consenso al trattamento ed alla comunicazione dei propri dati qualificati come personali e sensibili, per le 
 finalità e per la durata precisati nell’informativa.
<br><br>
Le modalità di trattamento dei dati sarà realizzata per mezzo delle operazioni indicate all’art. 4 comma 1 lett. a) del 
sopraindicato decreto che comprende: raccolta, registrazione, organizzazione, consultazione, elaborazione, modificazione, 
selezione, estrazione, utilizzo, comunicazione, cancellazione e distruzione dei dati. Tali operazioni possono essere svolte 
con o senza l’ausilio di strumenti di elaborazione elettronici o comunque automatizzati. I dati personali potranno essere 
raccolti od inviati a soggetti terzi nel rispetto delle finalità sopra indicate con modalità e procedure strettamente 
necessarie per l’espletamento delle attività ed in ogni caso l'azienda s’impegnerà a garantire che il trattamento di tali 
dati si svolga nel rispetto dei diritti e delle libertà fondamentali, nonché della dignità dell'interessato, con particolare 
riferimento alla riservatezza, all'identità personale e al diritto alla protezione dei dati stessi.
<br><br>
Preso atto dell'informativa di cui sopra, esprimo il mio consenso al trattamento dei miei dati personali, per le finalità 
e con le modalità sopra indicate nonché alla loro comunicazione e al trattamento degli stessi da parte dei soggetti 
sopraindicati.
  
</span></a></p>

</div>
		<?php
$config = array(
	'messages' => array(	// Error and info messages. Always escape double quotes. " => \"
		'email_exists' 		=> 'Il tuo indirizzo email è già nel nostro database.',
		'no_email' 			=> 'Per favore inserisci un indirizzo email.',
		'email_invalid' 	=> 'Il tuo indirizzo email sembra non essere corretto. <br/>Per favore inserisci un indirizzo email valido.',
		'thank_you' 		=> 'Ti ringraziamo per il tuo interesse nel progetto Case di Siena<br />Ti terremo aggiornato sulle ultime novità.',
		'technical' 		=> 'Stiamo riscontrando dei problemi tecnici. <br />Per favore prova più tardi.'
	),
	'database' => array(	// Database connection settings
		'host'				=> 'localhost',
		'username'			=> 'root',
		'password'			=>	'gnugnu',
		'database'			=>	'my_skalka'
	),
	'targetDate' => array(	// Target countdown date
		'day'				=> 31,
		'month'				=> 5,
		'year'				=> 2014,
		'hour'				=> 0,
		'minute'			=> 1,
		'second'			=> 0
	)
);

?>
		<script language="javascript" type="text/javascript">
			jQuery(document).ready(function() {
				$('#countdown_dashboard').countDown({
					targetDate: {
						'day': 		<?=$config['targetDate']['day']?>,
						'month': 	<?=$config['targetDate']['month']?>,
						'year': 	<?=$config['targetDate']['year']?>,
						'hour': 	<?=$config['targetDate']['hour']?>,
						'min': 		<?=$config['targetDate']['minute']?>,
						'sec': 		<?=$config['targetDate']['second']?>
					}
				});
				
				// Subscription functions
				$('#email_field').focus(email_focus).blur(email_blur);
				$('#subscribe_form').bind('submit', subscribe_submit);
			});
		</script>
	
	</div>
</body>

</html>
