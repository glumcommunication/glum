<%@ Page Title="" Language="C#" MasterPageFile="~/ASPX/casedisiena.Master" AutoEventWireup="true" CodeBehind="elencoimmobili.aspx.cs" Inherits="casedisiena.elencoimmobili" %>
<%@ Register Src="ASCX/agenzieleft.ascx" TagName="agenzieleft" TagPrefix="cds" %>
<%@ Register Src="ASCX/RiepilogoRicerca.ascx" TagName="RiepilogoRicerca" TagPrefix="cds" %>
<%@ Register TagPrefix="FG" TagName="PAGER" Src="ascx/pagerNumbers.ascx" %>
<%@ Register Src="ASCX/sponsorright.ascx" TagName="sponsorright" TagPrefix="cds" %>
<%@ Import Namespace="casedisiena" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BodyContent" runat="server">
    <div id="left">
        <asp:ScriptManager ID="smbloccoricerca" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel class="campiricerca"  ID="bloccoricerca" runat="server" style="width:160px;padding:5px;">
        <ContentTemplate>
            <div class="itemRicerca" style="width:160px;">                  
                <asp:RadioButton style="float:left; width:80px;" ID="rdbVendita" runat="server" GroupName="tipoannuncio"  Text="Vendita" />
                <asp:RadioButton style="float:left; width:80px;" ID="rdbAffitto" runat="server" GroupName="tipoannuncio"  Text="Affitto" />
             </div>
            <div class="itemRicerca" style="width:160px;">
                <asp:DropDownList style="float:left;" ID="drpComune" AutoPostBack="true" 
                    runat="server" runat="server" CssClass="txt txtdrop" 
                    onselectedindexchanged="drpComune_SelectedIndexChanged"></asp:DropDownList>
            </div>
            <div class="itemRicerca" style="width:160px;">
                <asp:LinkButton runat="server" id="ApriAree" Text="seleziona area" onclick="ApriAree_Click"></asp:LinkButton>
                <!--<div id="divSelAree" style="position:absolute;top:190;left:700;z-index:90;background-color:White;border:1px gray solid">-->
                <asp:Panel runat="server" ID="divSelAree" Visible="false" style="position:absolute;top:190;left:700;z-index:90;background-color:White;border:1px gray solid">
                        <asp:Button ID="btnAree" runat="server" style="float:right" CssClass="chiudi" 
                            onclick="btnAree_Click" />
                        <asp:CheckBoxList ID="chkListAree" runat="server">
                    
                        </asp:CheckBoxList>
                </asp:Panel>
                    <!--</div>-->
                <!--</div>-->
            </div>
            <div class="itemRicerca" style="width:160px;">
                <asp:DropDownList style="float:left;" ID="drpTipologia" runat="server" 
                    CssClass="txt txtdrop" 
                    onselectedindexchanged="drpTipologia_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>                
             </div>
            <div class="itemRicerca" style="width:160px;">
                <asp:DropDownList style="float:left;" ID="drpSubCategoria" runat="server" CssClass="txt txtdrop"></asp:DropDownList>
            </div>
            <div class="itemRicerca" style="width:160px;">
                <div class="itemRicerca_mezzo" style="width:75px;margin-right:10px;">
                    Prezzo Min
                </div>
                <div class="itemRicerca_mezzo" style="width:75px;">
                    Prezzo Max
                </div>
            </div>
            <div class="itemRicerca" style="width:160px;">
                <div class="itemRicerca_mezzo" style="width:75px;margin-right: 10px;">
                    <asp:TextBox style="float:left;width:73px;" ID="txtPrezzoMin" runat="server" CssClass="txt txttext"></asp:TextBox>
                </div>
                <div class="itemRicerca_mezzo" style="width:75px;">
                    <asp:TextBox style="float:left;width:73px;" ID="txtPrezzoMax" runat="server" CssClass="txt txttext"></asp:TextBox>
                </div>
            </div>
            <div class="itemRicerca" style="width:160px;">
                <div class="itemRicerca_mezzo" style="width:75px;margin-right: 10px">
                    Mq Min
                </div>
                <div class="itemRicerca_mezzo" style="width:75px;">
                    Mq Max
                </div>
            </div>
            <div class="itemRicerca" style="width:160px;">
                <div class="itemRicerca_mezzo" style="width:75px;margin-right: 10px">
                    <asp:TextBox ID="txtMqDa" runat="server" CssClass="txt txttext" style="Width:73px;"></asp:TextBox>
                </div>
                <div class="itemRicerca_mezzo" style="width:75px;">
                    <asp:TextBox  ID="txtMqA" runat="server" CssClass="txt txttext"  style="Width:73px;"></asp:TextBox>
                </div>
            </div>
            <div class="itemRicerca" style="width:160px;">
                <div class="itemRicerca_mezzo" style="width:85px;">
                    <%=TestiAccessHelper.SelectTestiByKey("vanimin",this.Lingua) %>
                </div>
                <div class="itemRicerca_mezzo" style="width:75px;">
                    <asp:TextBox ID="txtVaniDa" runat="server" CssClass="txt txttext" style="Width:73px;"></asp:TextBox>
                </div>
            </div>
            <div class="itemRicerca" style="width:160px;">
                <div class="itemRicerca_mezzo" style="width:85px;">
                    <%=TestiAccessHelper.SelectTestiByKey("cameremin",this.Lingua) %>
                </div>
                <div class="itemRicerca_mezzo" style="width:75px;">
                    <asp:TextBox ID="txtCamereDa" runat="server" CssClass="txt txttext" style="Width:73px;"></asp:TextBox>
                </div>
            </div>
            <div class="itemRicerca" style="width:160px;">
                <div class="itemRicerca_mezzo" style="width:85px;">
                    <%=TestiAccessHelper.SelectTestiByKey("bagnimin",this.Lingua) %>
                </div>
                <div class="itemRicerca_mezzo" style="width:75px;">
                    <asp:TextBox ID="txtBagniDa" runat="server" CssClass="txt txttext" style="Width:73px;"></asp:TextBox>
                </div>
            </div>
            <div class="itemRicerca" style="width:170px;">
                <asp:DropDownList ID="ddlArredato" runat="server" CssClass="txt txtdrop"></asp:DropDownList>
            </div>
            <div class="itemRicerca" style="width:170px;">
                <asp:DropDownList ID="ddlClasseEnergetica" runat="server" CssClass="txt txtdrop"></asp:DropDownList>
            </div>
            <div class="itemRicerca" style="width:170px;">
                <asp:CheckBox CssClass="lblricerca" ID="chlPostoAuto" runat="server" Text="Posto auto" TextAlign ="Right" style="vertical-align: middle;" />
            </div>
            <div class="itemRicerca" style="width:170px;">
                <asp:CheckBox CssClass="lblricerca" ID="txtAccessoIndipendente" runat="server" Text="Accesso Indip." TextAlign ="Right" style="vertical-align: middle;" />
            </div>            
            <div class="itemRicerca" style="width:160px;">
                <asp:CheckBox CssClass="lblricerca" ID="chTerrazza" runat="server" Text="Terrazza Abitabile" TextAlign ="Right" style="vertical-align: middle;" />
            </div>
            <div class="itemRicerca" style="width:170px;">
                <asp:CheckBox CssClass="lblricerca" ID="chkGarage" runat="server" Text="Garage" TextAlign ="Right" style="vertical-align: middle;" />
            </div>            
            <div class="itemRicerca" style="width:160px;">            
                <asp:CheckBox CssClass="lblricerca" ID="chkGiardino" runat="server" Text="Garage" TextAlign ="Right" style="vertical-align: middle;" />
            </div>
            <div class="itemRicerca" style="width:160px;">            
                <asp:CheckBox CssClass="lblricerca" ID="chkBalcone" runat="server" Text="Garage" TextAlign ="Right" style="vertical-align: middle;" />
            </div>
            <div class="itemRicerca" style="width:160px;">            
                <asp:LinkButton CssClass="bottone_100" ID="cmdRicerca" runat="server" onclick="cmdRicerca_Click" Text="Ricerca" style="margin-left:35px;margin-right:35px;" />
           </div>
        </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <div  id="centerfirst">
    <div id="RisultatiRicercaInfo">
        <div style="float:left;width:570px;margin:0 5px">

            <span style="color:#b52a4d;">Immobili per pagina&nbsp;&nbsp; </span>
                                      <asp:DropDownList id = "drpImgForPage" runat="server" AutoPostBack="True" 
                                            onselectedindexchanged="drpImgForPage_SelectedIndexChanged">
                                        <asp:ListItem Text="10" Selected="True" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                        <asp:ListItem Text="60" Value="60"></asp:ListItem>
                                      </asp:DropDownList>

            <span style="color:#b52a4d;">ordina per&nbsp;&nbsp; </span>

            <asp:LinkButton CssClass="ordina" ID="lnkOrdinaDATA_PUBBLICAZIONE" runat="server" 
                Text="localit&agrave;" onclick="lnkOrdinaDATA_PUBBLICAZIONE_Click"></asp:LinkButton> 

            <asp:LinkButton CssClass="ordina" ID="lnkOrdinaPREZZO" runat="server" Text="prezzo" 
                onclick="lnkOrdinaPREZZO_Click"></asp:LinkButton> 
            
            <asp:LinkButton CssClass="ordina" ID="lnkOrdinaMQ" runat="server" Text="mq" 
                onclick="lnkOrdinaMQ_Click"></asp:LinkButton> 
            
            <asp:LinkButton CssClass="ordina" ID="lnkOrdinaVANI" runat="server" Text="vani" 
                onclick="lnkOrdinaVANI_Click"></asp:LinkButton>             
        </div>

    </div>
    <%string lingua="it";%>
    <asp:Label ID="lblImmobiliTrovati" Text="Annunci Trovati : 0" Visible="false" runat="server" style="float:left;color:#b52a4d;font-weight:bold;display:block;margin:25px 0 10px;"></asp:Label>
    <asp:Repeater ID="repAgenzia" runat="server" onitemdatabound="repAgenzia_ItemDataBound">
        <ItemTemplate>
        
        <div class="<%#GetAltStile(Container.ItemType)%>">
        <div style="height:auto;">
            <div style="float: left;width: 270px;margin-bottom:10px;">
                <asp:Image class="imgNoBorder" ID="img" style="float:left;display:block;" runat="server" />
            </div>
            <div style="width:270px;float:left;">
                <asp:Label style="float:left;"  ID="lblCategoria" runat="server" ></asp:Label>
            </div>
            <div>&nbsp;</div>
            <div style="width:115px;float:left;">
                <asp:Label style="float:left;"  ID="lblLocalita" runat="server"></asp:Label>
            </div>
            <div style="width:70px;float:left;">
                <asp:Label style="float:left;"  ID="lblMq" runat="server"></asp:Label>
             </div>
            <div style="width:85px;float:left;">
                <asp:Label style="float:right;"  ID="lblPrezzo" runat="server"></asp:Label>
            </div>
            <div>&nbsp;</div>            
            <div style="width:270px;float:left;">
                <asp:Label style="float:left;text-align:justify;"  ID="lbldescrizione" runat="server" ></asp:Label>
           </div>
            <div>&nbsp;</div>           
            <div style="width:270px;float:left;text-align:left;">
                <asp:Label style="float:left;text-align:left;" ID="lblRiferimento" runat="server" ></asp:Label>
            </div>
        </div>
             <div class="bottoniElencoImmobili">
                <asp:LinkButton ID="lnkAgenzia" runat="server"> <asp:Image class="" style="visibility:hidden;" ID="imglogoAgenzia" runat="server" /></asp:LinkButton>
                <asp:LinkButton ID="Dettaglio" runat="server" Text="Dettaglio"  CssClass="bottone_griglia" style="float:right;"> </asp:LinkButton>
            </div>
        </div>
        </ItemTemplate>
    </asp:Repeater>
        <div style="float:left;font-size:12px;margin:25px 201px 10px;text-align: center;">
            <div style="width:178px;"><FG:PAGER ID="pager" runat="server" /></div>
        </div>
    </div>
    <div id="right">
        <cds:sponsorright  id="sponsorleft1" runat="server"></cds:sponsorright>
    </div>
       <script type="text/javascript" language="javascript">
           $('#divSelAree').hide();
       </script>    
</asp:Content>
