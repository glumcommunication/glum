<?php
$pagename = 'corpo';
$pagetitle = 'Trattamenti corpo';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<main>
    <div class="container">
        <h1 class="title">Dimagrimento</h1>
        <div id="trattamenti">
            <div class="col-sm-4 productItem3">
                <div class="productImage">  <a target="_blank" href="http://www.lynfodigit.com">
                    <img src="img/uomo/corpo/Lynfodigit3.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style='font-size:20px; border-bottom:10px; font-weight:bold; text-align:center;' >
                        <a target="_blank" href="http://www.lynfodigit.com">  Lynfodigit</a>
                    </h3>

                   <!-- <p><strong>Lynfodigit</strong> &egrave; una macchina polifunzionale per il benessere e la bellezza del corpo. Lynfodigit grazie all&rsquo;alta tecnologia utilizzata ( magneto, infrarosso, tonificazione ed elettrostimolazione) raggiunge risultati pressoch&egrave; immediati. Gi&agrave; al primo trattamento possiamo vedere dei risultati :gambe leggere e toniche come dopo un mese di palestra !</p>

                    <p><strong>Lynfodigit</strong> tramite specifici impulsi che lavorano sia in polo positivo che in polo negativo, andr&agrave; a rimodellare il vostro corpo in modo piacevole, rilassante e per niente fastidioso poich&egrave; utilizza un&rsquo;onda quadra, bifasica e compensata. Il programma che combatte la cellulite cambier&agrave; 4 frequenze e leggendo la resistenza del corpo, andr&agrave; a variare automaticamente l&rsquo;ampiezza dell&rsquo;impulso rendendo il trattamento completamente personalizzato.</p>

                    <p>Con questo sistema innovativo &egrave; garantito il massimo risultato nel campo di applicazione dei trattamenti <strong>contro la cellulite</strong>, per il rilassamento elettronico e per la tonificazione muscolare.</p>
                    --></div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                                         <a target="_blank" href="http://www.essetika.it/body-slindy/item/279-smooth-slim-draining-pack.html">   

                                             <img src="img/trattamenti/bodyslindy.jpg"></a>
                </div>
                <div class="productCaption2">
                    <h3 style='font-size:20px; border-bottom:10px; font-weight:bold; text-align:center;' >
                                      <a target="_blank" href="http://www.essetika.it/body-slindy/item/279-smooth-slim-draining-pack.html">   
                                          Body Slindy</a>
                    </h3>

                   <!-- <p><strong>Lynfodigit</strong> &egrave; una macchina polifunzionale per il benessere e la bellezza del corpo. Lynfodigit grazie all&rsquo;alta tecnologia utilizzata ( magneto, infrarosso, tonificazione ed elettrostimolazione) raggiunge risultati pressoch&egrave; immediati. Gi&agrave; al primo trattamento possiamo vedere dei risultati :gambe leggere e toniche come dopo un mese di palestra !</p>

                    <p><strong>Lynfodigit</strong> tramite specifici impulsi che lavorano sia in polo positivo che in polo negativo, andr&agrave; a rimodellare il vostro corpo in modo piacevole, rilassante e per niente fastidioso poich&egrave; utilizza un&rsquo;onda quadra, bifasica e compensata. Il programma che combatte la cellulite cambier&agrave; 4 frequenze e leggendo la resistenza del corpo, andr&agrave; a variare automaticamente l&rsquo;ampiezza dell&rsquo;impulso rendendo il trattamento completamente personalizzato.</p>

                    <p>Con questo sistema innovativo &egrave; garantito il massimo risultato nel campo di applicazione dei trattamenti <strong>contro la cellulite</strong>, per il rilassamento elettronico e per la tonificazione muscolare.</p>
                    -->    </div>
            </div>
                 <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.natureline.it/linea-natureline.php?line=natureline&cod=1022&des=ELISIR%201022%20UOMO%20IN%20FORMA">
                    <img style="max-height:190px;" src="img/uomo/dimagrimento/naturelineuomoinforma.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style='font-size:20px; border-bottom:10px; font-weight:bold; text-align:center;' >
                                          <a target="_blank" href="http://www.natureline.it/linea-natureline.php?line=natureline&cod=1022&des=ELISIR%201022%20UOMO%20IN%20FORMA">
                                              Uomo in forma</a>
                    </h3>

                   <!-- <p><strong>Lynfodigit</strong> &egrave; una macchina polifunzionale per il benessere e la bellezza del corpo. Lynfodigit grazie all&rsquo;alta tecnologia utilizzata ( magneto, infrarosso, tonificazione ed elettrostimolazione) raggiunge risultati pressoch&egrave; immediati. Gi&agrave; al primo trattamento possiamo vedere dei risultati :gambe leggere e toniche come dopo un mese di palestra !</p>

                    <p><strong>Lynfodigit</strong> tramite specifici impulsi che lavorano sia in polo positivo che in polo negativo, andr&agrave; a rimodellare il vostro corpo in modo piacevole, rilassante e per niente fastidioso poich&egrave; utilizza un&rsquo;onda quadra, bifasica e compensata. Il programma che combatte la cellulite cambier&agrave; 4 frequenze e leggendo la resistenza del corpo, andr&agrave; a variare automaticamente l&rsquo;ampiezza dell&rsquo;impulso rendendo il trattamento completamente personalizzato.</p>

                    <p>Con questo sistema innovativo &egrave; garantito il massimo risultato nel campo di applicazione dei trattamenti <strong>contro la cellulite</strong>, per il rilassamento elettronico e per la tonificazione muscolare.</p>
                    -->    </div>
            </div>
             
        </div>
    </div>
    <!--<div class="container">
        <h1 class="title">Prodotti</h1>
        <div id="trattamenti">
            <div class="col-sm-3 col-sm-offset-1 productItem3">
                <div class="productImage">
                    <img src="img/uomo/corpo/akeyndorabdofitness.jpg">
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">Abdo Slim</h3>



                </div>
            </div>
            <div class="col-sm-3 productItem3">
                <div class="productImage">
                    <img style="max-height:190px;" src="img/uomo/dimagrimento/naturelineuomoinforma.jpg">
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                       Uomo In Forma   </h3>




                </div>
            </div>
            <div class="col-sm-3 productItem3">
                <div class="productImage">
                    <img src="img/trattamenti/nature-line.jpg">
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        Protein Line      </h3>


                </div>
            </div>
            
        </div>
    </div>
    -->
    <br/><br/>
    <div class="container">
        <h1 class="title">Partner trattamenti</h1>
        <hr style="color:#728192; background-color: #728192; border:1px solid #728192;">
        <br/><br/>
        <div id="trattamenti">
              <div class="col-sm-2 col-sm-offset-3 productItem3">
                  <a   href="http://www.lynfodigit.com/" target="_blank"> <img height="30px" src="img/trattamenti/corpo/linfodygit.jpg"></a>
            </div>
            <div class="col-sm-2  productItem3">
                <a href="http://www.natureline.it" target="_blank">
                    <img height="30px" src="img/trattamenti/dimagrimento/proteinline-h.png"></a> 
            </div>
            <div class="col-sm-2 productItem3">
                <a href="http://www.skeyndor.com/" target="_blank"> <img src="img/urban spa/skeyndor.png"></a>

            </div>

        </div>
    </div>
</main>

<?php
include_once 'footer.php';
?>   