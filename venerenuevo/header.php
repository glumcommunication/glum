<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <link href="css/ekko-lightbox.css" rel="stylesheet">
        <script src="js/ekko-lightbox.js"></script>
        <script src="js/respond.js"></script>
        <!--<link rel="icon" href="img/favicon.png" type="image/svg" />-->
        <meta name="description" content="<?php echo $pagedesc ?>">
        <meta name="robots" content="index,follow">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet" media="screen">
        <link href='http://fonts.googleapis.com/css?family=Mr+De+Haviland|Seaweed+Script|Gilda+Display|Quattrocento:400,700' rel='stylesheet' type='text/css'>
        <meta name="keywords" content="Venere, Siena, Estetica, Urban Spa, Centro estetico, estetista, fitness, benessere, relax, corpo, cura del corpo, manicure, pedicure, massaggi, trattamenti corpo, viso, antirughe, anti-aging">
        <title><?php echo $pagetitle ?> | VenereNuevo - Estetica & Urban SPA</title>
        <?php
        if ($pagename == "contatti") {
            ?>

            <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
            <script type="text/javascript">
                function initialize() {
                    var latlng = new google.maps.LatLng(43.337096, 11.306337);
                    var settings = {
                        zoom: 15,
                        center: latlng,
                        mapTypeControl: true,
                        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
                        navigationControl: true,
                        navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    var map = new google.maps.Map(document.getElementById("map_canvas"), settings);

                    var companyLogo = new google.maps.MarkerImage('img/positionshadow.png',
                            new google.maps.Size(86, 60),
                            new google.maps.Point(0, 0)

                            );

                    var companyPos = new google.maps.LatLng(43.337096, 11.306337);
                    var companyMarker = new google.maps.Marker({
                        position: companyPos,
                        map: map,
                        icon: companyLogo,
                        //shadow: companyShadow,
                        title: "Centro Estetico Venere Nuevo"
                    });
                    var contentString = '<div id="content">' +
                            '<div id="siteNotice">' +
                            '</div>' +
                            '<h1 id="firstHeading" class="firstHeading" style="font-size:14px;font-weight:bold;border-bottom: none;margin-bottom:5px;">Centro Estetico Venere Nuevo</h1>' +
                            '<div id="bodyContent">' +
                            '<p style="font-size:12px;">' +
                            'Via Celso Cittadini, 42/44<br>53100 Siena (SI)<br>' +
                            'Tel.: +39 0577 123456<br>Email: <a href="mailto:info@venerenuevo.it">info@venerenuevo.it</a></p>' +
                            '</div>' +
                            '</div>';

                    var infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });
                    google.maps.event.addListener(companyMarker, 'click', function() {
                        infowindow.open(map, companyMarker);
                    });
                }
            </script>
            <?php
        }
        ?>
        <script type="text/javascript">
            $(document).ready(function($) {
                $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
                    event.preventDefault();
                    return $(this).ekkoLightbox();
                });
            });
        </script>

        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-50510376-1', 'venerenuevo.it');
            ga('send', 'pageview');

        </script>
    </head>
    <body <? if ($pagename == "contatti") echo " onload=\"initialize()\""; ?>>
        <div class="wrapper">
            <header>
                <div class="container">
                    <div class="col-md-4 col-md-offset-4 col-xs-10 col-xs-offset-1" id="logo">
                        <a href="index.php?ref=hl" title=""><img src="img/logo.jpg" alt="Venere Nuevo Logo"></a>
                    </div>
                </div>
