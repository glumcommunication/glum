<?php
$pagename = 'viso';
$pagetitle = 'Trattamenti viso';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<main>
    <div class="container">
        <h1 class="title">Trattamenti e Prodotti Viso</h1>
        <div id="trattamenti">
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.essetika.it/for-men/item/132-energizing-anti-age-serum.html">
                    <img src="img/uomo/viso/skeyndorenergizing.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style='font-size:20px; border-bottom:10px; font-weight:bold; text-align:center;'>
                                          <a target="_blank" href="http://www.essetika.it/for-men/item/132-energizing-anti-age-serum.html">
                                              Skeyndor Energizing Anti-Age Serum					</a>			</h3>
                    <!--<p>Siero che apporta alla pelle una rivitalizzante dose di energia. Elaborato con Ginseng Siberiano, un agente adattogeno che stabilizza il metabolismo cellulare ed attiva il sistema immunitario, migliorando la resistenza della pelle contro lo stress e le condizioni ambientali estreme. Previene ed attenua i segni della stanchezza e dell&rsquo;et&agrave; sulla pelle. Con fattore di protezione solare SPF 10.</p>-->

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.essetika.it/for-men/item/133-eye-bags-recovery-gel.html">
                    <img src="img/uomo/viso/skeyndorgelocchi.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style='font-size:20px; border-bottom:10px; font-weight:bold; text-align:center;'>
                                            <a target="_blank" href="http://www.essetika.it/for-men/item/133-eye-bags-recovery-gel.html">
                                                Skeyndor – Eye Bags Recovery Gel</a> </h3>
                    <!--<p>Gel decongestionante per attenuare i segni della stanchezza intorno agli occhi e diminuire le borse e le occhiaie. Contiene cellule fresche di mango e liposomi con acqua dei ghiacciai svizzeri. La sua formula aiuta a combattere i segni dello stress, mantiene un grado di idratazione ottimale e dona una gradevole sensazione di freschezza nella zona del contorno occhi.</p>-->

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.essetika.it/for-men/item/130-shine-control-24h-aqua-emulsion.html">
                    <img src="img/uomo/viso/skeyndoridratante.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style='font-size:20px; border-bottom:10px; font-weight:bold; text-align:center;'>
                                            <a target="_blank" href="http://www.essetika.it/for-men/item/130-shine-control-24h-aqua-emulsion.html">
                                                Shine Control 24H Aqua Emulsion</a></h3>
                    <!--<p>Emulsione per idratare in profondit&agrave; e ridurre la lucidit&agrave; ed il sebo della pelle. Elaborata con agenti idratanti a lunga durata e taurina, un principio attivo osmoregolatore che migliora la resistenza della pelle contro la disidratazione. Opacizza immediatamente la pelle grazie a microparticelle che assorbono l&rsquo;eccesso di sebo e alla presenza di agenti seboregolatori che diminuiscono la produzione naturale di grasso della pelle, riducono la dimensione dei pori e normalizzano le pelli miste e grasse.</p>-->

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.essetika.it/for-men/item/128-daily-detox-face-wash.html">
                    <img src="img/uomo/viso/skeyndormousselimpiadora.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style='font-size:20px; border-bottom:10px; font-weight:bold; text-align:center;'>
                                            <a target="_blank" href="http://www.essetika.it/for-men/item/128-daily-detox-face-wash.html">
                                                Daily Detox Face Wash</a></h3>
                    <!--<p>Mousse purificante a tripla azione che deterge in profondit&agrave;, ristabilisce l&rsquo;equilibrio esterno della pelle e ne accelera il rinnovamento cellulare. Elaborata con acido glicolico ed una miscela di agenti detergenti. La sua formula extra-delicata permette un uso quotidiano senza aggredire la pelle. Prodotto ideale per eliminare le impurit&agrave; e gli eccessi di grasso, migliorando l&rsquo;aspetto esterno della pelle.</p>-->

                </div>
            </div>
                <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.essetika.it/for-men/item/271-express-energizing-professional-pack.html">
                    <img src="img/uomo/viso/pack1.jpg" >
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style='font-size:20px; border-bottom:10px; font-weight:bold; text-align:center;'>
                                            <a target="_blank" href="http://www.essetika.it/for-men/item/271-express-energizing-professional-pack.html">
                                                Treatment Pack for Men</a></h3>
                    <!--<p>Mousse purificante a tripla azione che deterge in profondit&agrave;, ristabilisce l&rsquo;equilibrio esterno della pelle e ne accelera il rinnovamento cellulare. Elaborata con acido glicolico ed una miscela di agenti detergenti. La sua formula extra-delicata permette un uso quotidiano senza aggredire la pelle. Prodotto ideale per eliminare le impurit&agrave; e gli eccessi di grasso, migliorando l&rsquo;aspetto esterno della pelle.</p>-->

                </div>
            </div>
                <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.essetika.it/for-men/item/138-express-energizing-professional-pack.html">
                    <img src="img/uomo/viso/pack2.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style='font-size:20px; border-bottom:10px; font-weight:bold; text-align:center;'>
                                            <a target="_blank" href="http://www.essetika.it/for-men/item/138-express-energizing-professional-pack.html">
                                                Express energizing professional Pack for Men</a></h3>
                    <!--<p>Mousse purificante a tripla azione che deterge in profondit&agrave;, ristabilisce l&rsquo;equilibrio esterno della pelle e ne accelera il rinnovamento cellulare. Elaborata con acido glicolico ed una miscela di agenti detergenti. La sua formula extra-delicata permette un uso quotidiano senza aggredire la pelle. Prodotto ideale per eliminare le impurit&agrave; e gli eccessi di grasso, migliorando l&rsquo;aspetto esterno della pelle.</p>-->

                </div>
            </div>

        </div>
    </div>
    
 <br/><br/>
        <div class="container">
            <h1 class="title">Partner trattamenti</h1>
            <hr style="color:#728192; background-color: #728192; border:1px solid #728192;">
            <br/><br/>
            <div id="trattamenti">
                <div class="col-sm-2 col-sm-offset-5 productItem3">
                <a href="http://www.skeyndor.com/" target="_blank"> <img src="img/urban spa/skeyndor.png"></a>

            </div>
               
            </div>
        </div>
</main>

<?php
include_once 'footer.php';
?>