<?php
$pagename = 'corpo';
$pagetitle = 'Trattamenti corpo';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<main>
    <div class="container">
        <h1 class="title">Trattamenti Corpo</h1>
        <div id="trattamenti">
            
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <img src="img/uomo/corpo/biorelaxmassaggio.jpg">
                </div>
                <div class="productCaption2">
                    <h3 style='font-size:20px; border-bottom:10px; font-weight:bold; text-align:center;' >
                        Bio Relax Massaggio
                    </h3>
                    <!--<p>
                        PINK ANGEL è l’originale trattamento anti-età modellante per il corpo in forma di mousse rosa arricchita di polvere 
                        dorata (Golden Mica Powder).<br>
                        Lasciati avvolgere dal tuo Angelo di Bellezza. Conquista e mantieni ogni giorno la giovinezza di un corpo tonico e 
                        bello. Con il suo effetto riducente, l’innovativa formulazione di Pink Angel ridefinisce i contorni della silhouette e 
                        leviga istantaneamente la pelle del corpo, rendendola divinamente vellutata.<br>
                        Ringiovanisce, Riduce, Tonifica, Illumina
                    </p>
                    <ol>
                        <li>Previene il rilassamento cutaneo con un’intensa azione rassodante.</li>
                        <li>Migliora il tono, la compattezza e l’elasticità della pelle specialmente in zone quali glutei, gambe, braccia, seno e décolleté</li>
                        <li>Combatte i radicali liberi prevenendo l’invecchiamento cutaneo</li>
                        <li>Favorisce la microcircolazione</li>
                        <li>Reintegra l’idratazione e mantiene la pelle morbida e liscia come la seta</li>
                        <li>Dona al tuo corpo una dolce e sottile profumazione unita ad una piacevole sensazione di freschezza</li>
                        <li>Illumina la pelle grazie alla finissima polvere d’oro (Golden Mica Powder)</li>
                    </ol>-->
                </div>
            </div>
           <!-- <div class="col-sm-4 productItem">
                <div class="productImage">
                    <img src="img/uomo/corpo/Lynfodigit3.jpg">
                </div>
                <div class="productCaption2">
                    <h3 style='font-size:20px; border-bottom:10px; font-weight:bold; text-align:center;' >
                        Lynfodigit
                    </h3>
                   <!-- <p><strong>Lynfodigit</strong> &egrave; una macchina polifunzionale per il benessere e la bellezza del corpo. Lynfodigit grazie all&rsquo;alta tecnologia utilizzata ( magneto, infrarosso, tonificazione ed elettrostimolazione) raggiunge risultati pressoch&egrave; immediati. Gi&agrave; al primo trattamento possiamo vedere dei risultati :gambe leggere e toniche come dopo un mese di palestra !</p>

                    <p><strong>Lynfodigit</strong> tramite specifici impulsi che lavorano sia in polo positivo che in polo negativo, andr&agrave; a rimodellare il vostro corpo in modo piacevole, rilassante e per niente fastidioso poich&egrave; utilizza un&rsquo;onda quadra, bifasica e compensata. Il programma che combatte la cellulite cambier&agrave; 4 frequenze e leggendo la resistenza del corpo, andr&agrave; a variare automaticamente l&rsquo;ampiezza dell&rsquo;impulso rendendo il trattamento completamente personalizzato.</p>

                    <p>Con questo sistema innovativo &egrave; garantito il massimo risultato nel campo di applicazione dei trattamenti <strong>contro la cellulite</strong>, per il rilassamento elettronico e per la tonificazione muscolare.</p>
                </div>
            </div>-->
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <img src="img/uomo/corpo/massaggiodecongestionantesportivo.jpg">
                </div>
                <div class="productCaption2">
                    <h3 style='font-size:20px; border-bottom:10px; font-weight:bold; text-align:center;' >
                       Massaggio Decontratturante
                    </h3>
                 <!--  <p>Il presso massaggio di Active Press agisce sulla circolazione venosa e linfatica ed &egrave; particolarmente indicato per gli arti inferiori, zona del corpo che presenta pi&ugrave; frequentemente problemi di tipo circolatorio. Active Press rende pi&ugrave; efficiente la circolazione migliorando di conseguenza inestetismi quali cellulite, ritenzione di liquidi, edemi, e alleviando il senso di pesantezza delle gambe.</p>

<p>Active Press facilita il drenaggio del liquido interstiziale attivando la circolazione venosa e liberando l&rsquo;ambiente extracellulare dalle scorie che le cellule costantemente vi riversano. La pressione non viene esercitata contemporaneamente su tutta la superficie ma secondo una sequenza centripeta che &egrave; quella seguita dal sangue e dalla linfa.</p>

<p>Pu&ograve; essere ottimizzato dall&rsquo;applicazione complementare di oli essenziali specifici ad azione drenante applicati sia manualmente, sia con l&rsquo;ausilio di particolari e specifici bendaggi.</p>

<p><strong>Il presso massaggio esercitato &egrave;:</strong></p>

<p>Sicuro, in grado di autoregolarsi per esercitare una pressione ottimale in ogni settore.</p>

<p>Efficace si dalla prima seduta, per combattere gli inestetismi della cellulite, la ritenzione idrica e rimodellare la silhouette.</p>

<p>Versatile, in quanto offre un&rsquo;ampia gamma di trattamenti attraverso programmi gi&agrave; memorizzati e ulteriormente personalizzabili sulla base delle esigenze dei clienti.</p>-->
                </div>
            </div>
             <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <img src="img/uomo/corpo/massaggio-tensioriflessogeno.jpg">
                </div>
                <div class="productCaption2">
                    <h3 style='font-size:20px; border-bottom:10px; font-weight:bold; text-align:center;' >
                     Messaggio Tensioriflessogeno
                    </h3>

<!--   <p>Gel riducente studiato per ridurre il volume dell&rsquo;addome e rimodellare la silhouette. Elaborata scientificamente con un estratto di Fucus vesiculosus, ricco di iodio, accelera il metabolismo basale e promuove lo scioglimento dei noduli adiposi. La sua formulazione inoltre, arricchita con carnitina, promuove la rapida combustione dei cataboliti prodotti, riducendo il giro vita. Ideale per recuperare una figura snella e longilinea, il cosmetico &egrave; potenziato con un estratto di Crithmum maritimum che dona al tessuto un effetto rassodante e rimodellante. Sulla pelle pulita, due volte al giorno applicare il prodotto sull&rsquo;area dell&rsquo;addome e fianchi e far assorbire con delicati massaggi. Il cosmetico si propone anche nello specifico cofanetto dotato di fascia elastica, per un risultato pi&ugrave; rapido.</p>-->
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <h1 class="title">Partner</h1>
        <hr style="color:#728192; background-color: #728192; border:1px solid #728192;">
        <div id="trattamenti">
            <div class="col-sm-4 col-sm-offset-4 productItem3">
                <div class="productImage">
                    <a href="http://www.maxpier.it//" target="_blank">  <img width="100px;" src="img/uomo/corpo/maxpier.png"></a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">Max Pier</h3>



                </div>
            </div>
            
            


        </div>
    </div>
</main>

<?php
include_once 'footer.php';
?>