<?php
$pagename = 'corpo';
$pagetitle = 'Trattamenti corpo';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<main>
    <div class="container">
        <h1 class="title">Trattamenti Viso</h1>
        <div id="trattamenti">
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.essetika.it/eternal-line.html">
                        <img src="img/trattamenti/viso/eternal.jpg"></a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.essetika.it/eternal-line.html">   Eternal       </a>           </h3>

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.essetika.it/corrective.html">
                    <img  src="img/trattamenti/corrective.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.essetika.it/corrective.html">       Corretive</a></h3>
                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.essetika.it/global-lift.html">
                    <img src="img/trattamenti/viso/globallift.jpg">
                    </a></div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.essetika.it/global-lift.html"> Global Lift</a>
                    </h3>
                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.essetika.it/aquatherm.html"><img src="img/trattamenti/viso/aquatherm.jpg"></a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.essetika.it/aquatherm.html">    Aquatherm</a>
                    </h3>


                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.essetika.it/derma-peel.html">    <img src="img/trattamenti/viso/dermapeel.jpg"></a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.essetika.it/derma-peel.html">      Dermapeel</a>
                    </h3>


                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.essetika.it/power-c-plus.html"><img src="img/trattamenti/viso/clearbalance.jpg"></a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.essetika.it/power-c-plus.html">Power C Plus</a>
                    </h3>


                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.essetika.it/urban-white.html">   <img src="img/trattamenti/viso/urban white.jpg"></a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.essetika.it/urban-white.html">  Urban White</a>
                    </h3>


                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.skeyndor.com/gamma/power-hyaluronic">   <img src="img/powerhyaluronic.jpg"></a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.skeyndor.com/gamma/power-hyaluronic">    Power Hyaluronic</a>
                    </h3>

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.intraceuticals.it/clarity.html">
                    <img src="img/trattamenti/viso/clarity.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.intraceuticals.it/clarity.html">    Clarity</a>
                    </h3>

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.intraceuticals.it/opulence.html"><img src="img/trattamenti/opulence.jpg"></a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.intraceuticals.it/opulence.html">  Opulence</a>
                    </h3>

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.intraceuticals.it/atoxelene.html"><img src="img/trattamenti/Atoxelene.jpg"></a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.intraceuticals.it/atoxelene.html">  Atoxelene</a>
                    </h3>

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.intraceuticals.it/rejuvenate.html"><img src="img/trattamenti/Rejuvenate.jpg"></a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.intraceuticals.it/rejuvenate.html">Rejuvinate</a>
                    </h3>

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.intraceuticals.it/">
                        <img src="img/trattamenti/booster-intraceuticals.jpg"></a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.intraceuticals.it/">Booster</a>
                    </h3>

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <img src="img/trattamenti/Resurfacing.jpg">
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        Resufacing
                    </h3>

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.benessens.it/">
                        <img src="img/trattamenti/viso/perlage.jpg"></a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.benessens.it/">Perlage</a>
                    </h3>

                </div>
            </div>
          <!--  <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <img src="img/trattamenti/viso/rituals.jpg">
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        Mediterranean Rituals
                    </h3>


                </div>
            </div>-->
           
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.thatso.it/en/lines/pure-body-en/42-collagen-bodywave-2/"><img src="img/trattamenti/42-Collagen-BodyWave.jpg"></a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.thatso.it/en/lines/pure-body-en/42-collagen-bodywave-2/">Collagen Body Wave</a>
                    </h3>

                </div>
            </div>
        </div>

    </div>
    <br/><br/>
    <div class="container">
        <h1 class="title">Partner trattamenti</h1>
        <hr style="color:#728192; background-color: #728192; border:1px solid #728192;">
        <br/><br/>
        <div id="trattamenti">
          
            
            <div class="col-sm-3 productItem3">
                <a target="_blank" href="http://www.skeyndor.com/" target="_blank"> <img src="img/urban spa/skeyndor.png"></a>

            </div>
             <div class="col-sm-3 productItem3">
                 <a target="_blank" href="http://www.thatso.it/" target="_blank"> <img src="img/trattamenti/viso/that.jpg"></a>

            </div>
              <div class="col-sm-3 productItem3">
                  <a target="_blank" href="http://www.benessens.it/" target="_blank"> <img src="img/trattamenti/viso/benessens.jpg"></a>

            </div>
             <div class="col-sm-3 productItem3">
                 <a target="_blank" href="http://www.intraceuticals.it/" target="_blank"> <img src="img/trattamenti/viso/intraceuticals.jpg"></a>

            </div>

        </div>
    </div>

</main>


<?php
include_once 'footer.php';
?>