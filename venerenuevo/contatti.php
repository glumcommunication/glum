<?php
$pagename = 'contatti';
$pagetitle = 'Sito in Costruzione';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
if ($_POST['doSend']=='Invia') {
$name = $_POST[name];
$email = $_POST[email];
$phone = $_POST[phone];
$message = $_POST[message];
$headers = "";
$headers .= "From: $email\n";
$headers .= "Reply-To: $email\n";
$headers .= "MIME-Version: 1.0\n";
$headers .= "Content-type: text/html; charset=utf-8\n";
$to = "info@venerenuevo.it.";
/*$message = $messaggio;*/
$messagebody = "Hai ricevuto una nuova email via venerenuevo.it, ecco i dettagli:<br />
Nome: $name <br />

E-mail: $email<br />

Telefono: $phone<br>

Messaggio: $message<br />";

mail ($to,$subject,$messagebody,$headers);
$sent = "1";
}
?>
		<main>
			<div class="container">
				<h1 class="title">Contatti</h1>
				<div class="row">
					<div class="col-sm-12">
						<div id="map_canvas" style="width:100%; height:400px;margin-bottom: 30px;"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<h2>Info</h2>
						<p>
						Per conoscere meglio i nostri servizi, gli orari di apertura o per qualsiasi altra informazione, 
						puoi contattarci attraverso il form qui accanto oppure inviandoci una email all'indirizzo 
						<a href="mailto:info@venerenuevo.it">info@venerenuevo.it</a>.
						</p>
						<p>
						Oppure vienici a trovare in
						</p>
						<p>
						<strong>Via Celso Cittadini, 42/44<br>53100 Siena<br>Telefono: +39 0577 247858 - +39 0577 594089</strong>
						</p>
                                                <p>Orari: <br/>
                                                    Lun: 13:30/19:30<br/>
                                                    Mar-Ven: 9:30/19:30<br/>
                                                Sab: 9:30/13:30</p>
					</div>
					<div class="col-sm-8">
						<?php
if ($sent=='1') {
?>
						<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<h4>Messaggio inviato!</h4>
							Sarai ricontattato al pi&ugrave; presto.<br/>
                                                        Grazie. Venere Nuevo
						</div>
			<?php
}
?>
						<form action="contatti.php" method="post" id="contatti">
							<div class="col-sm-4">
								<div class="form-group">
									<input type="text" class="form-control" id="name" name="name" placeholder="Nome e cognome">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<input type="email" class="form-control" id="email" name="email" placeholder="Indirizzo email">
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<input type="etext" class="form-control" id="phone" name="phone" placeholder="Numero di telefono">
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<textarea class="form-control" rows="7" id="message" name="message"></textarea>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="checkbox">
									<label>
										<input type="checkbox" value="Yep" name="privacy">
										<a id="privacy" href="#" rel="tooltip" data-toggle="tooltip" title="Nel inviare i miei dati dichiaro di aver letto l'informativa e sono consapevole che il trattamento degli stessi è necessario per ottenere il servizio proposto. A tal fine, nel dichiarare di essere maggiorenne, fornisco il mio consenso." data-placement="right" style="font-size:14px;cursor: help;">
											Informativa sulla Privacy ai sensi del D.Lgs 196/2003</a>
									</label>
								</div>
							</div>
							<div class="col-sm-12">
								<button type="submit" class="btn btn-default" name="doSend" value="Invia">Invia</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			</main>

<?php
include_once 'footer.php';
?>