<?php
$pagename = 'Nails';
$pagetitle = 'nails';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<main>
    <div class="container">
         
        <h1 class="title">Nails</h1>
        <div class="col-sm-12">
            <div class="basicCaption">
                <p>Le&nbsp;unghie&nbsp;hanno un ruolo fondamentale per l&#39;estetica della mano&nbsp;e quindi di tutto il corpo, le unghie curate sono testimonianza dell&#39;equilibrio della propria persona. Potrai esaltare la tua classe curandone i dettagli grazie anche ai&nbsp;nostri lavori di decorazione. I nostri servizi sono finalizzati a&nbsp;esaltare la bellezza delle vostre mani e dei vostri piedi,&nbsp;per questo motivo Venere Nuevo con il suo team specializzato vi proporr&agrave; servizi su misura per soddisfare ogni vostra esigenza. Siamo specializzati nella cura estetica delle mani e dei piedi, offriamo servizi di ricostruzioni unghie in gel, smalto semi permanente, manicure e pedicure, allungamento unghie, riparazione unghie rotte o scheggiate, trattamento unghie naturali, decorazione unghie Nail Art. &nbsp;</p>
            </div>
        </div>
         
        <div class="col-sm-12">
        
            <h1 class="title">Alcune creazioni:</h1>

            <div id="carouselHome" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carouselHome" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselHome" data-slide-to="1"></li>
                    <li data-target="#carouselHome" data-slide-to="2"></li>
                    <li data-target="#carouselHome" data-slide-to="3"></li>

                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner"> <div class="item active">
                        <img src="img/nails/nails1.jpg" alt="...">
                    </div>
                    <div class="item">
                        <img src="img/nails/nails2.jpg" alt="...">
                    </div>
                    <div class="item">
                        <img src="img/nails/nails3.jpg" alt="...">
                    </div>
                    <div class="item">
                        <img src="img/nails/nails4.jpg" alt="...">
                    </div>


                </div>
                <a class="left carousel-control" href="#carouselHome" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="right carousel-control" href="#carouselHome" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
   
        </div>
    </div>
    <div class="container">
        <div class="row">
               <br/><br/>
            <h1 class="title">
                Tipologie di trattamenti offerti:</h1>
            <br/>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <img src="img/nails/ricostruzione-gel.jpg">
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                       Ricopertura Gel                    </h3>

                </div>
            </div>
              <div class="col-sm-4 productItem3">
                    <div class="productImage">
                        <img src="img/nails/amalto-semipermanente.jpg">
                    </div>
                    <div class="productCaption2">
                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                            Smalto Semipermanente                   </h3>

                    </div>
                </div>
              <div class="col-sm-4 productItem3">
                    <div class="productImage">
                        <img src="img/nails/so-polish.jpg">
                    </div>
                    <div class="productCaption2">
                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                           So Polish Protect & Peel                  </h3>

                    </div>
                </div>
              <div class="col-sm-4 productItem3">
                    <div class="productImage">
                        <img src="img/nails/manicure-basic.jpg">
                    </div>
                    <div class="productCaption2">
                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                            Manicure Basic                    </h3>

                    </div>
                </div>
              <div class="col-sm-4 productItem3">
                    <div class="productImage">
                        <img src="img/nails/manicure-deluxe.jpg">
                    </div>
                    <div class="productCaption2">
                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                            Manicure Deluxe                   </h3>

                    </div>
                </div>
              <div class="col-sm-4 productItem3">
                    <div class="productImage">
                        <img src="img/nails/ricostruzione-alluci.jpg">
                    </div>
                    <div class="productCaption2">
                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                            Ricostruzione unghie                   </h3>

                    </div>
                </div>
              <div class="col-sm-4 productItem3">
                    <div class="productImage">
                        <img src="img/nails/pedicure-curativo.jpg">
                    </div>
                    <div class="productCaption2">
                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                            Pedicure curativo                    </h3>

                    </div>
                </div>
              <div class="col-sm-4 productItem3">
                    <div class="productImage">
                        <img src="img/nails/pedicure-basic.jpg">
                    </div>
                    <div class="productCaption2">
                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                            Pedicure Basic                    </h3>

                    </div>
                </div>
              <div class="col-sm-4 productItem3">
                    <div class="productImage">
                        <img src="img/nails/pedicure-deluxe.jpg">
                    </div>
                    <div class="productCaption2">
                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                           Pedicure Deluxe                    </h3>

                    </div>
                </div>
        </div>
         <br/><br/>
        <div class="container">
            <h1 class="title">Partner trattamenti</h1>
            <hr style="color:#728192; background-color: #728192; border:1px solid #728192;">
            <br/><br/>
            <div id="trattamenti">
                <div class="col-sm-2 col-sm-offset-4 productItem3">
                        <a   href="http://it.pronails.com/it" target="_blank"> <img style="background-color: #621322; height: 70px;" src="img/urban spa/pronails.png"></a>


                </div>
                <div class="col-sm-2 productItem3">
                        <a href="http://www.tecniwork.it/it/Mani/Smalti_/TNS_Color_Collection.htm" target="_blank"> <img src="img/nails/tecniwork.png"></a>

                </div>
               

            </div>
        </div>
            
        </div>


    </div>
</main>

<?php
include_once 'footer.php';
?>