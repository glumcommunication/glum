<?php
$pagename = 'depilazione';
$pagetitle = 'Depilazione';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<main>
    <div class="container">
        <h1 class="title">Epilazione</h1>
        <div id="trattamenti">
            <div class="col-sm-4 productItem">
                <div class="productImage">
                    <img src="img/uomo/depilazione/resineepiltech.jpg">
                </div>
                <div class="productCaption">
                    <h2>Epilazione Resina</h2>

                    <p>Partners</p>

                    <a   href="http://www.devacosmetica.it/index.php" target="_blank"> <img  src="img/uomo/epilazione/epiltech.png"></a>

                </div>
            </div>
            <div class="col-sm-4 productItem">
                <div class="productImage">
                    <img src="img/uomo/depilazione/zeropeli.jpg">
                </div>
                <div class="productCaption">
                    <h2>
                        Luce Pulsata
                    </h2>
                    <p>Partners</p>

                    <a   href="http://www.zeropeli.it/" target="_blank"> <img  src="img/uomo/epilazione/zeropeli.png"></a>



                </div>
            </div>
            <div class="col-sm-4 productItem">
                <div class="productImage">
                    <img src="img/uomo/epilazione/sugar.jpg">
                </div>
                <div class="productCaption">
                    <h2>
                        Luce Pulsata
                    </h2>
                    <p>Partners</p>
                 
                        <a   href="http://www.epiladerm.it/" target="_blank"> <img  height="49px" src="img/uomo/epilazione/epilderm.jpg"></a>
                 
                        <a href="http://www.skeyndor.com/" target="_blank"> <img height="49px" src="img/urban spa/skeyndor.png"></a>
                   
                </div>
            </div>


        </div>
    </div>
</main>

<?php
include_once 'footer.php';
?>