<?php
$pagename = 'corpo';
$pagetitle = 'Trattamenti corpo';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<main>
    <div class="container">
        <h1 class="title">Linea Mani e Piedi</h1>
        <div id="trattamenti">
            <div class="col-sm-4  productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.lcntoscana.it/?p=catalogo&display=1&id_cat=1116">
                    <img src="img/uomo/maniepiedi/lcncremamani.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">   
                                                                <a target="_blank" href="http://www.lcntoscana.it/?p=catalogo&display=1&id_cat=1116">
                                                                    LCN Man Hand Cream</a></h3>

                     
                    </h3>
                  <!--  <p>La crema per le mani ad azione intensiva con un complesso di principi attivi High Tech a base di edera, crusca di riso ed estratti di semi di girasole e di vitamine A ed E.</p>-->
                </div>
            </div>
               <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.lcntoscana.it/?p=catalogo&display=1&id_cat=1115">
                    <img src="img/uomo/maniepiedi/lcngelmani.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                                       <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;"> 
                                                               <a target="_blank" href="http://www.lcntoscana.it/?p=catalogo&display=1&id_cat=1115">
                                                                   LCN Man Hand Gel</a></h3>

                       
                     </h3>
                   <!--<p>Edera, crusca di riso ed estratti di semi di girasole proteggono ulteriormente la cute dai fattori ambientali esterni e le regalano una nuova vitalit&agrave;. Il gel per le mani rinfrescante e di facile applicazione che rivitalizza la pelle.</p>-->
                </div>
            </div>
               <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.lcntoscana.it/?p=catalogo&display=1&id_cat=1112">
                    <img src="img/uomo/maniepiedi/lcngelgambe.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;"> 
                                                                <a target="_blank" href="http://www.lcntoscana.it/?p=catalogo&display=1&id_cat=1112">
                                                                    LCN Man Leg Splash</a></h3>

                      
                      </h3>
                    <!--<p>La crema per le mani ad azione intensiva con un complesso di principi attivi High Tech a base di edera, crusca di riso ed estratti di semi di girasole e di vitamine A ed E.</p>-->
                </div>
            </div>
               <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.lcntoscana.it/?p=catalogo&display=1&id_cat=1110">
                    <img src="img/uomo/maniepiedi/lcncremapiedi.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                                      <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                                                              <a target="_blank" href="http://www.lcntoscana.it/?p=catalogo&display=1&id_cat=1110">
                                                                  LCN Man Foot Cream</a></h3>

                        
                    </h3>
                    <!--<p>La crema per le mani ad azione intensiva con un complesso di principi attivi High Tech a base di edera, crusca di riso ed estratti di semi di girasole e di vitamine A ed E.</p>-->
                </div>
            </div>
               <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.lcntoscana.it/?p=catalogo&display=1&id_cat=1114">
                    <img src="img/uomo/maniepiedi/lcnlima.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                                       <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                                                               <a target="_blank" href="http://www.lcntoscana.it/?p=catalogo&display=1&id_cat=1114">
LCN Man Files
                                                               </a></h3>

                        
                    </h3>
                    <!--<p>La crema per le mani ad azione intensiva con un complesso di principi attivi High Tech a base di edera, crusca di riso ed estratti di semi di girasole e di vitamine A ed E.</p>-->
                </div>
            </div>
        </div>
    </div>
    <br/><br/>
    <div class="container">
        <h1 class="title">Partner trattamenti</h1>
        <hr style="color:#728192; background-color: #728192; border:1px solid #728192;">
        <br/><br/>
        <div id="trattamenti">
              <div class="col-sm-2 col-sm-offset-5 productItem3">
                <a   href="http://www.lcntoscana.it/" target="_blank"> <img  src="http://www.lcntoscana.it/images/logo.png"></a>
            </div>
           

        </div>
    </div>
</main>

<?php
include_once 'footer.php';
?>