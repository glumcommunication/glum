<?php
$pagename = 'uomo';
$pagetitle = 'Uomo';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<main>
    <div class="container">
        <div class="row">
            <h1 class="title">Uomo</h1>
            <div class="col-sm-12">
                <div class="basicCaption">
                    <p>
                        L’uomo si prende cura di sé.<br>
                        La pelle maschile è fisiologicamente assai diversa da quella femminile. Possiede delle qualità 
                        naturali straordinarie per proteggersi dall’ambiente esterno con maggiore efficacia ed è più 
                        resistente al deterioramento naturale causato dall’età.<br>
                    </p>
                    <p>&nbsp;</p>
                    <p>
                        Punti deboli della tua pelle:
                    </p>
                    <p>&nbsp;</p>
                    <p>
                        1. Una volta innescato il processo di invecchiamento, questo si sviluppa velocemente.<br>
                        2. La pelle maschile produce il 75% in più di secrezione sebacea rispetto a quella femminile, con 
                        una maggiore tendenza a presentare lucidità e impurità.
                    </p>
                </div>
            </div>
            <div class="col-sm-12">
                <img src="img/uomo.jpg">
            </div>
        </div>
        <div class="row">
            <br/><br/>
            <h1 class="title">
                Tipologie di trattamenti offerti:</h1>
            <br/>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a href="depilazioneuomo.php">
                        <img src="img/uomo/depilazione/resineepiltech.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                                        <a href="depilazioneuomo.php">
                                            Depilazione Epilazione</a></h3>

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a href="visouomo.php">
                        <img src="img/uomo/viso/trattamenti-viso uomo.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a href="visouomo.php">    Trattamenti Viso</a></h3>

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a href="corpouomo.php">
                        <img src="img/uomo/corpo/massaggiodecongestionantesportivo.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                                           <a href="corpouomo.php">
 Trattamenti Corpo           </a>  </h3>

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a href="dimagrimentouomo.php">
                        <img src="img/trattamenti/bodyslindy.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                                            <a href="dimagrimentouomo.php">
                                                Trattamenti Dimagrimenti          </a>      </h3>

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a href="lcn.php">
                        <img src="img/uomo/maniepiedi/maniepiedi-uomo.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                                        <a href="lcn.php">
                                            Mani e Piedi         </a>         </h3>

                </div>
            </div>
        </div>



    </div>
</main>

<?php
include_once 'footer.php';
?>