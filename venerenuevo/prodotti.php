<?php
$pagename = 'index';
$pagetitle = 'Sito in Costruzione';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
			<main>
			<div class="container">
				<h1 class="title">Prodotti</h1>
                              
                                
<?php
$figcap=array(
    "1" => "<h2 class='text-center'>Man Hand <br/>Cream</h2><p>La crema per le mani ad azione intensiva con un complesso di principi attivi High Tech a base di edera, crusca di riso ed estratti di semi di girasole e di vitamine A ed E.</p>",
    "2" => "<h2 class='text-center'>Man Hand <br/>Gel</h2><p> Edera, crusca di riso ed estratti di semi di girasole proteggono ulteriormente la cute dai fattori ambientali esterni e le regalano una nuova vitalit&agrave;. Il gel per le mani rinfrescante e di facile applicazione che rivitalizza la pelle.</p>",
    "3" => "<h2 class='text-center'>Man Files</h2><p>La LCN ha creato un set di lime per le unghie specifiche per la struttura delle unghie maschili. La lima bianca serve per un rapido accorciamento delle unghie, mentre la lima lucidante &egrave; in grado di dare alle unghie maschili una lucentezza quattro volte maggiore.</p>",
    "4" =>"<h2 class='text-center'>Man Leg Splash Sports</h2><p>Lo spray per i piedi, che ha un&apos;efficacia deodorante e nutriente a lunga durata, costituisce la cura ideale dopo lo sport. Il complesso rivitalizzante a base di menta piperita, olio di agrumi e olio di cipresso rinfresca piacevolmente, tonifica e rilassa allo stesso tempo.</p>",
    "5" =>"<h2 class='text-center'>Man Foot Cream Sports</h2><p>La crema rivitalizzante e nutriente con Cell Active Men, un complesso a base di taurina, alga verde e ginseng siberiano agisce stimolando e tonificando. Il principio attivo antibatterico Farsenol plus impedisce la formazione di odori del piede.</p>",
    "6" =>"<h2 class='text-center'>Daily Detox Face Wash</h2><p>Mousse purificante a tripla azione che deterge in profondit&agrave;, ristabilisce l&apos;equilibrio esterno della pelle e ne accelera il rinnovamento cellulare.</p>",
    "7" =>"<h2 class='text-center'>Smoothing Shaving Gel</h2><p>Gel per una rasatura veloce, senza tagli n&egrave; irritazioni. La sua consistenza spumosa solleva i peli e favorisce lo scorrimento della lametta da barba senza aggredire la pelle.</p>",
    "8" =>"<h2 class='text-center'>Redness Preventing After Shave</h2><p>Balsamo extra rinfrescante che attenua i fastidi della rasatura. La sua formula potenzia l&apos;azione cicatrizzante dell’estratto di Sangue di Drago, un complesso biotecnologico che accelera la guarigione dei microtagli.</p>",
    "9" =>"<h2 class='text-center'>24H Aqua Emulsion</h2><p>Emulsione per idratare in profondità e ridurre la lucidit&agrave; ed il sebo della pelle. Elaborata con agenti idratanti a lunga durata e taurina, un principio attivo osmoregolatore che migliora la resistenza della pelle contro la disidratazione.</p>",
    "10" =>"<h2 class='text-center'>Eye Bags Recovery Gel</h2><p>Gel decongestionante per attenuare i segni della stanchezza intorno agli occhi e diminuire le borse e le occhiaie. Contiene cellule fresche di mango e liposomi con acqua dei ghiacciai svizzeri. Dona una sensazione di freschezza nella zona del contorno occhi.</p>",
    "11" =>"<h2 class='text-center'>Energizing Anti-Age Serum</h2><p>Siero che apporta alla pelle una rivitalizzante dose di energia. Elaborato con Ginseng Siberiano. Con fattore di protezione solare SPF 10.</p>",
    "12" =>"<h2 class='text-center'>Shaping<br/> Gel-Cream</h2><p>Gel specifico per ridurre il volume dell&apos;addome in modo rapido ed efficace. Elaborato con una serie di principi attivi sviluppati e testati scientificamente sugli uomini. Agisce riducendo i ricettori del &apos;colesterolo cattivo&apos; (LDL, VLDL) nelle cellule adipose del corpo.</p>",
    "13" => "<h2 class='text-center'>Post Depil<br/> For Men</h2><p>Concentrato attivo per attenuare l&apos;aggressione dei trattamenti depilatori sulla pelle dell&apos;uomo. Formulata con una miscela di principi attivi volti a mitigare la comparsa dei rossori e ad attivare le difese della pelle,</p>",
    "14" => "<h2 class='text-center'>Abdo Slim Draining Gel</h2><p>Gel riducente studiato per ridurre il volume dell’addome e rimodellare la silhouette. Elaborata scientificamente con un estratto di Fucus vesiculosus, ricco di iodio, accelera il metabolismo basale e promuove lo scioglimento dei noduli adiposi.</p>",
    "15" => "<h2 class='text-center'>ProNature HandCare</h2><p>La linea ProNature Hand Care è tutto ciò di cui avrete bisogno per rendere le vostre mani ben curate e morbide con prodotti completamente naturali.</p>",
    "16" => "<h2 class='text-center'>Extention Mascara</h2><p>Ciglia allungate con Deep Black Extension Mascara. Carbon Black texture multipigmentata, dall’effetto extension. L&apos;innovativo applicatore con scovolo in puro silicone permette ripetute applicazioni, fino al raggiungimento dell&apos;intensit&agrave; desiderata.</p>",
    "17" =>"<h2 class='text-center'>Candela Marrakesh</h2><p>La candela Marrakesh non solo aromatizza l&apos;ambiente rilasciando la sue fragranze orientali ed esotiche, ma anche si trasforma in un olio da massaggio naturale con la sua qualit&apos; ineguagliabile.</p>",
    "18" =>"<h2 class='text-center'>Linea Urban White</h2><p>New skin foaming cleaner<br/>Antipollution shield mist<br/>Shield day cream<br/>Overnight serum<br/>Sport eraser cream<br/>Shield hand cream<br/>Matt compact powder<br/>Perfect Light</p>",
    "19" =>"<h2 class='text-center'>Devoted Creations</h2><p>Finalmente anche in Italia l&apos;abbronzatura delle star di Hollywood. Tutti i prodotti della linea Devoted Creations sono altamente tecnologici e consentono un&apos;abbronzatura ultra rapida e perfetta.</p>",
    "20" =>"<h2 class='text-center'>Green Line</h2><p> PURO<br/>Puro è una ricca lozione per il trattamento delle pelli più secche e sensibili.<br/> TERRA E VERDE <br/>Terra e Verde sono delle potenti e naturali lozioni abbronzante.</p>",
    "21" =>"<h2 class='text-center'>Pure Sun <br/>Product Baby</h2><p>Protezione solare particolarmente indicata per le esigenze dei bambini e delle pelli pi&ugrave; sensibili grazie alla elevata protezione dai raggi UVA/UVB e alla sua speciale formula senza conservanti.</p>",
    "22" =>"<h2 class='text-center'>Fragranze Toscane</h2><p>La forza evocativa di luoghi magici della Toscana e gli odori intensi della macchia mediterranea muovono l&apos;anima verso suggestioni olfattive di benessere e tranquillit&agrave;, verso l&apos;essenza del piacere.</p>",
    "23" =>"<h2 class='text-center'>Crocofever</h2><p>CrocoFever &egrave; il top per quanto riguarda la tendenza glamour per le tue unghie.Fatti eseguire in pochi minuti la manicure in stile &apos;pelle di coccodrillo&apos;  con CrocoFever !!</p>",
    "24" =>"<h2 class='text-center'>Magnetic Power</h2><p>Particelle metalliche nelllo smalto evocano sull&apos;unghia, grazie all&apos;utilizzo di un magnete, affascinanti decorazioni in pochi secondi.</p>",
    
    );
 for ($i=1;$i<=24;$i++) {
          echo '<div class="col-sm-3 col-xs-6" style="margin:0 auto 10px;padding: 0 5px;">';

     echo '<figure class="cap-bot"><span class="pulsante glyphicon glyphicon-zoom-in"></span><img src="img/prodotti/'.$i.'.jpg" alt="">';
     echo "<figcaption class='descrizione'>$figcap[$i]</figcaption></a>";
     echo "</figure></div>";
 }
?>
			
                                           </div>
			</main>

<?php
include_once 'footer.php';
?>