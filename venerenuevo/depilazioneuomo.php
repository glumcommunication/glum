<?php
$pagename = 'depilazione';
$pagetitle = 'Depilazione';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<main>

    <div class="container">
        <div class="row">
            <h1 class="title">Depilazione</h1>
            <div id="trattamenti">
                <div class="col-sm-4 col-sm-offset-4 productItem3">
                    <div class="productImage">
                        <img src="img/uomo/depilazione/titanio.jpg">
                    </div>
                    <div class="productCaption2">
                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                            Micromica al Titanio                    </h3>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <h1 class="title">Epilazione</h1>
            <div id="trattamenti">
                <div class="col-sm-4 productItem3">
                    <div class="productImage">
                        <a target="_blank" href="http://www.devacosmetica.it/sk_prodotto.php?var=38">

                            <img src="img/uomo/depilazione/resineepiltech.jpg">
                        </a>
                    </div>
                    <div class="productCaption2">
                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                            <a target="_blank" href="http://www.devacosmetica.it/sk_prodotto.php?var=38">
                                Epilazione Resina</a></h3>



                    </div>
                </div>
                <div class="col-sm-4 productItem3">
                    <div class="productImage">
                        <a target="_blank" href="http://www.zeropeli.it/">

                            <img src="img/uomo/depilazione/zeropeli.jpg">
                        </a>
                    </div>
                    <div class="productCaption2">
                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                            <a target="_blank" href="http://www.zeropeli.it/">

                                Luce Pulsata
                            </a>
                        </h3>


                    </div>
                </div>
                <div class="col-sm-4 productItem3">
                    <div class="productImage">
                        <a target="_blank" href="http://www.epiladerm.it/index.php/epiladerm-zuckerpaste/die-zuckerpaste">

                            <img src="img/uomo/epilazione/sugar.jpg">
                        </a>
                    </div>
                    <div class="productCaption2">
                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                            <a target="_blank" href="http://www.epiladerm.it/index.php/epiladerm-zuckerpaste/die-zuckerpaste">
                                Sugar Paste</a>
                        </h3>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/><br/>
    <div class="container">
        <h1 class="title">Partner trattamenti</h1>
        <hr style="color:#728192; background-color: #728192; border:1px solid #728192;">
        <br/><br/>
        <div id="trattamenti">
            <div class="col-sm-2 col-sm-offset-1 productItem3">
                <a href="http://www.sdscosmetici.it/site/index.php/prodotti/prodotti" target="_blank">
                    <img  src="img/uomo/depilazione/efloredepil.png"></a>  
            </div>
            <div class="col-sm-2 productItem3">
                <a   href="http://www.devacosmetica.it/index.php" target="_blank"> <img  src="img/uomo/epilazione/epiltech.png"></a>
            </div>
            <div class="col-sm-2 productItem3">
                <a   href="http://www.zeropeli.it/" target="_blank"> <img  src="img/uomo/epilazione/zeropeli.png"></a>
            </div>
            <div class="col-sm-2 productItem3">
                <a href="http://www.skeyndor.com/" target="_blank"> <img   src="img/urban spa/skeyndor.png"></a>
            </div>
            <div class="col-sm-2 productItem3">
                <a   href="http://www.epiladerm.it/" target="_blank"> <img  height="60px" src="img/uomo/epilazione/epilderm.jpg"></a> 
            </div>
        </div>
    </div>
    <!--
    
        <div class="container">
            <h1 class="title">Prodotti</h1>
            <div id="trattamenti">
                <div class="col-sm-2 col-sm-offset-1 productItem3">
                    <div class="productImage">
                        <img src="img/uomo/depilazione/skeyndorpostepil.jpg">
                    </div>
                    <div class="productCaption2">
                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">Post Depil</h3>
    
    
                        <a href="http://www.skeyndor.com/" target="_blank"> <img height="49px" src="img/urban spa/skeyndor.png"></a>
    
                    </div>
                </div>
                <div class="col-sm-2 productItem3">
                    <div class="productImage">
                        <img src="img/uomo/depilazione/gelafter.jpg">
                    </div>
                    <div class="productCaption2">
                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                            Shaving Gel                    </h3>
    
                        <a href="http://www.skeyndor.com/" target="_blank"> <img height="49px" src="img/urban spa/skeyndor.png"></a>
    
    
    
                    </div>
                </div>
                <div class="col-sm-2 productItem3">
                    <div class="productImage">
                        <img src="img/uomo/depilazione/balsamoafter.jpg">
                    </div>
                    <div class="productCaption2">
                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                            After Shave                    </h3>
    
                        <a href="http://www.skeyndor.com/" target="_blank"> <img height="49px" src="img/urban spa/skeyndor.png"></a>
    
                    </div>
                </div>
                <div class="col-sm-2 productItem3">
                    <div class="productImage">
                        <img src="img/uomo/depilazione/gelafter.jpg">
                    </div>
                    <div class="productCaption2">
                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                            Gel Calmante
                        </h3>
    
                        <a href="http://www.zeropeli.it/" target="_blank"> <img height="49px" src="img/uomo/epilazione/zeropeli.png"></a>
    
                    </div>
                </div>
                <div class="col-sm-2 productItem3">
                    <div class="productImage">
                        <img src="img/uomo/depilazione/skeyndorpostepil.jpg">
                    </div>
                    <div class="productCaption2">
                        <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                            Post epilatorio                    </h3>
    
                        <a href="http://www.zeropeli.it/" target="_blank"> <img height="49px" src="img/uomo/epilazione/zeropeli.png"></a>
    
                    </div>
                </div>
    
    -->
</main>







<?php
include_once 'footer.php';
?>