<?php
$pagename = 'index';
$pagetitle = 'Sito in Costruzione';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
			<main>
				<div class="container">
					<h1 class="title">Fitness</h1>
					<div id="fitness">
						<!--<div class="col-sm-8">-->
							<div class="row">
								<div class="col-sm-6">
                                                                    <img src="img/fitness/fitness.jpg">
								</div>
								<div class="col-sm-6">
                                                                    <img src="img/fitness/macchinari.jpg">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<h1 class="title">Macchinari</h1>
									<div class="productCaption2">
										<h2>Orbitrek</h2>
										<p>
										Orbitrek è una macchina 2in1 per l’allenamento di tutto il corpo, che permette di utilizzare due 
										dei più popolari ed efficaci attrezzi da palestra: stepper ed ellittica. Passare da un allenamento 
										all’altro è incredibilmente semplice e permette di effettuare esercizi aerobici, per consumare calorie, 
										e contemporaneamente rinforzare i muscoli, in particolare della parte bassa del corpo. Orbitrek è 
										Orbitrek è un attrezzo ideale per le persone di tutte le età e per tutti i livelli di allenamento.  
										Ideale per allenare sia la parte superiore che inferiore del corpo, Orbitrek combina il potere dimagrante 
										delle macchine cardio con l'attività tonificante e modellante. I diversi livelli di resistenza consentono 
										di ottenere muscoli definiti allenamento dopo allenamento, bruciando al tempo stesso calorie in eccesso.
										</p>
                                                                                <br/>
										<h2>Vibroplate</h2>
										<p>
										La pedana vibrante può essere utilizzata tranquillamente per perseguire diversi obiettivi: in particolare, 
										gli effetti delle vibrazioni hanno conseguenze immediate sul metabolismo dell'individuo.<br>
										Infatti, attraverso le sedute sulla pedana vibrante, si è riscontrato un aumento del metabolismo, ottenendo 
										una maggiore combustione dei grassi e riducendo gli strati adiposi sottocutanei (ovvero la cellulite).<br>
										Da sottolineare, dunque, il miglioramento a livello estetico e di bellezza che la pedana vibrante può 
										offrire attivando la circolazione, irrorando in tal modo maggiormente le cellule e aiutando il corpo ad 
										espellere le tossine, in primis la cellulite, in breve tempo.<br>
										A livello di benessere psico-fisico , le vibrazioni della pedana agiscono attraverso il rilascio nel nostro 
										corpo, in modo del tutto naturale, di neurotrasmetittori quali la seratonina e la dopamina che migliorano 
										l'umore e danno una sensazione di benessere generale, del tutto simile a quella che si ha dopo che si è 
										praticato dello sport.<br>
										Inoltre, si riscontrano anche un aumento della coordinazione, dell'equilibrio corporeo e dei riflessi in quanto 
										le vibrazioni agiscono direttamente sulla propriocezione, ovvero il meccanismo che regola l'equilibrio e la 
										percezione del movimento.
										</p>
                                                                                <br/>
										<h2>Tapis Roulant</h2>
										<p>
										È l’attrezzo per l’allenamento cardiovascolare più conosciuto e usato. Questo “tappeto in movimento” permette 
										di camminare o eseguire diverse tecniche di corsa, scegliendo se lavorare su dimagrimento, tonificazione o 
										capacità cardiorespiratoria.<br>
										Corsa e camminata sono gli esercizi fisici che permettono di bruciare più calorie e grassi. Di conseguenza il 
										tapis roulant, aiutando a eseguire questi due movimenti, è l'attrezzo più efficace per dimagrire. Ovviamente il 
										dispendio calorico aumenta in proporzione alla velocità e alla distanza percorsa, che vanno scelte facendo 
										attenzione alle condizioni fisiche.<br>
										Il tapis roulant aiuta inoltre a tonificare e combattere la cellulite. Corsa e camminata coinvolgono soprattutto 
										gambe, glutei e polpacci. L'esercizio giusto per tonificarli di più e combattere anche problemi di ritenzione? 
										Diminuire la velocità e incrementare la pendenza, in modo da aumentare il tempo di lavoro (appoggio sul piede) 
										sul tappeto e di conseguenza la contrazione dei muscoli e la circolazione del sangue.
										</p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
                                                                    <img src="img/fitness/danza-orientale.jpg">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<h1 class="title">Corso di danza orientale</h1>
									<div class="productCaption2">
										<p>
										La danza comunemente conosciuta come “danza del ventre” unisce eleganza nei movimenti e lavoro fisico. È una danza 
										che si può ballare da sole ma anche in gruppo ed è molto  gratificante anche per le principianti, visto che si può 
										ballare dopo aver appreso poche e semplici figure.<br>
										Si tratta di una danza che si adatta anche a donne non più giovani e valorizza con eleganza e sensualità anche 
										le forme più generose.<br>
										Il fascino e la suggestione della musica orientale allontanano lo stress, favorendo la concentrazione su muscoli e 
										parti del corpo che nella quotidianità non siamo abituati ad usare, con evidenti  benefici psico-fisici.<br>
										Il continuo movimento delle gambe rassoda la muscolatura di piedi e polpacci. I muscoli di braccia, spalle, 
										petto, addome, fianchi e glutei si sciolgono e si tonificano e la vita si assottiglia.<br>
										La postura e l'andatura traggono vantaggio dalla tonificazione di muscoli addominali e dorsali, che contribuiscono 
										a tenere nel corretto equilibrio la colonna vertebrale.<br>
										Il continuo movimento della danza stimola la circolazione sanguigna, favorendo l'eliminazione delle tossine e 
										prevenendo problemi ad essa legati.<br>
										Il movimento dei fianchi stimola le funzionalità dell'apparato digerente e riproduttivo.<br>
										Lasciatevi trasportare da quest'arte e liberate la vostra femminilità. Perché non provare?
										</p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
                                                                    <img src="img/fitness/pilates.jpg">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<h1 class="title">Corso di Pilates Matwork</h1>
									<div class="productCaption2">
										<p>
										Il metodo Pilates in origine chiamato "Contrology" è un programma di allenamento che consiste in sequenze di 
										esercizi codificati da Joseph Pilates, eseguiti su un materassino, in piedi o con l 'ausilio di particolari 
										attrezzi che hanno il compito di facilitare l'esercizio a corpo libero o aumentarne l'intensità.<br>
										Il Matwork è considerato un programma di ginnastica globale,in cui si predilige non la quantità ma la qualità dei movimenti. 
										L'efficacia dell'allenamento con il metodo Pilates è dovuta al coinvolgimento della mente , chiamata a plasmare il 
										corpo: posizione e movimento di ogni parte del corpo sono estremamente importanti, il corpo si muove come un sistema 
										integrato. Quanto più correttamente si usa il  proprio corpo durante gli esercizi, tanto più lo si utilizzerà 
										correttamente nella vita quotidiana. Questo consentirà di migliorare la postura e favorire l'eliminazione di tensioni e 
										rigidità. Quello che succede nel corpo riflette ciò che accade nella mente e viceversa. La mente di  chi esegue gli esercizi 
										del metodo Pilates è diretta verso il corpo, concentrata su ciò che sta accadendo mentre accade.<br>
										L'obiettivo principale del matwork è quello di rendere le persone consapevoli di sé e condurle ad unire corpo e 
										mente in una sana e funzionale unità. La maggior parte delle attività sportive si pone come obiettivo il perseguimento 
										di un esito atletico esterno, il metodo Pilates parte dall'interno, ponendo come obiettivo quello di interiorizzare, 
										sentire volontariamente a livello propriocettivo, anche i muscoli  più piccoli del nostro corpo. Aumentare la nostra 
										coscienza corporea, localizzare i muscoli più profondi e piccoli, richiede grande concentrazione e sincronismo perfetto 
										tra corpo e mente. Non si tratta ad ogni modo di uno sforzo stressante, ma la sensazione provata dopo una lezione di 
										pilates è quella di esserci dedicati un'ora tutta per noi.
										</p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
                                                                    <img src="img/fitness/mamma-fit1.jpg">
								</div>
								<div class="col-sm-6">
                                                                    <img src="img/fitness/mamma-fit2.jpg">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<h1 class="title">Mamme fitness</h1>
									<div class="productCaption2">
										<h2>Programma Mamma fitness post parto, ginnastica per mamme con il bambino</h2>
										<p>
										Diventare mamme cambia tante cose nella vita di una donna, la fase del puerperio è molto delicata sia dal punto 
										di vista fisico che psicologico, occorre dare “spazio” alla nuova mamma per riprendersi dal grande lavoro che 
										ha affrontato con gravidanza e parto e che affronta ancora con l'arrivo del suo piccolo che assorbe tutte le 
										sue energie sia a livello fisico che mentale. Per questo è importante che la mamma inizi tempestivamente un 
										programma di attività fisica mirato al riequilibrio delle strutture che sono state maggiormente sollecitate 
										durante la gravidanza ed il parto (pavimento pelvico, addome, glutei, fascia toraco-lombare etc..) così la mamma 
										può tornare quanto prima alle condizioni pre-gravidanza. Quindi serve un programma di attività studiato ad hoc per 
										lei, che possa metterla in condizioni di tornare in forma, anche meglio di prima!<br>
										Spesso le mamme non possono o non vogliono separarsi dal proprio piccolo, quindi rinunciano ad allenarsi e a 
										tornare in forma dopo il parto. Il nostro è un programma di allenamento predisposto in uno spazio in cui la mamma 
										ha la possibilità di fare esercizio senza dovere rinunciare alla compagnia del proprio piccolo, in un ambiente 
										caldo e rilassante in cui entrambi possano passare tempo insieme. Il nostro metodo integra le principali tecniche 
										di tonificazione e di Pilates con i concetti della Ginnastica Decompressiva: si tratta di un corso di tonificazione 
										appositamente studiato per il recupero post parto delle mamme ma anche di mamme con bimbi più grandi che non hanno 
										avuto modo di tornare a fare attività fisica. Le tecniche utilizzate durante l'allenamento prevedono un uso mirato 
										e ponderato del respiro, abbinato a differenti figure corporee statiche e dinamiche che, nel rispetto della globalità 
										del corpo, si basano su un nuovo concetto: controllare la pressione che si crea durante l'esercizio per assottigliare 
										efficacemente il punto vita ridefinendo un ventre piatto e migliorare la postura. Questo metodo si ispira alla 
										conoscenza millenaria dello yoga sui bandha, in sanscrito “chiusura” e cioè contrazione, e pone attenzione 
										all'allenamento di un muscolo fondamentale, il pavimento pelvico. Muscolo dell'intimità e della femminilità, che 
										assorbe e trattiene tutte le nostre emozioni più profonde.<br>
										Collegarsi a questo muscolo e acquisirne consapevolezza significa aprire la porta sulla nostra interiorità, guardarsi 
										allo specchio per capirsi e migliorarsi.<br>
										Il nostro intento è quello di incoraggiate la neo mamma a svolgere una regolare attività di fitness in un contesto 
										mirato alle sue esigenze e a quelle del proprio piccolo. Perché l'allenamento fa bene alla mamma e una mamma che sta 
										bene fa stare bene anche il suo piccolo!
										</p>
                                                                                <br/>
										<h2>Programma Mamma Fitness in gravidanza</h2>
										<p>
										La donna in gravidanza presenta caratteristiche strutturali e posturali diverse da quelle usuali, anche la sua 
										condizione psicologica cambia e si adatta sin dalle prime fasi della gestazione a sostenere questa prestazione eccezionale, 
										perché gran parte delle sue energie vengono messe a disposizione del bimbo che cresce.<br>
										Per questo motivo è molto importante che la futura mamma possa svolgere sin dalle prime fasi della gestazione un'attività 
										fisica mirata che possa adattarsi alle varie fasi del cambiamento che il corpo subirà in questo delicato momento della vita. 
										Il fitness, naturalmente praticato in modo non stressante, può migliorare la condizione fisica e psicologica della futura 
										mamma e prevenire l'insorgenza di disturbi spiacevoli. <br>
										In gravidanza però è opportuno svolgere una ginnastica adattata ai cambiamenti fisiologici del corpo della futura mamma: 
										occorre un programma di tonificazione e allungamento che possa aiutare il corpo ad adattarsi alle modificazioni strutturali 
										che subisce e a sostenere il peso del bimbo. Tutto ciò per poter interagire al meglio con l'ambiente circostante e prepararsi 
										a sostenere lo sforzo del parto, per arrivare a quel momento nella "migliore condizione fisica possibile". Questi risultati 
										possono essere raggiunti più facilmente se la mamma svolge una regolare attività fisica che comprenda esercizi aerobici, 
										tonificazione, esercizi di Pilates e stretching, ovviamente concordati con il proprio medico.<br>
										Ricordando che in questo delicato periodo della vita di una donna l'allenamento non va inteso come semplice fitness o workout, 
										ma serve per dare alla luce un bimbo sano!
										</p>
									</div>
								</div>
							</div>
						<!--</div>-->
						<!--<div class="col-sm-4">
							<img src="http://www.placehold.it/640x1000">
						</div>-->
					</div>
				</div>
			</main>

<?php
include_once 'footer.php';
?>