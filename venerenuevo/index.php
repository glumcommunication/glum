<?php
$pagename = 'index';
$pagetitle = 'VenereNuevo';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<main>
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div id="carouselHome" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carouselHome" data-slide-to="0" class="active"></li>

                        <li data-target="#carouselHome" data-slide-to="1"></li>
                        <li data-target="#carouselHome" data-slide-to="2"></li>
                        <li data-target="#carouselHome" data-slide-to="3"></li>
                        <li data-target="#carouselHome" data-slide-to="4"></li>
                        <li data-target="#carouselHome" data-slide-to="5"></li>
                        <li data-target="#carouselHome" data-slide-to="6"></li>
                        <li data-target="#carouselHome" data-slide-to="7"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner"> <div class="item active">
                            <img src="img/art/home4.jpg" alt="...">
                        </div>
                        <div class="item">
                            <img src="img/art/home5.jpg" alt="...">
                        </div>
                        <div class="item">
                            <img src="img/art/home15.jpg" alt="...">
                        </div>
                        <div class="item">
                            <img src="img/art/home2.jpg" alt="...">
                        </div>
                        <div class="item">
                            <img src="img/art/home3.jpg" alt="...">
                        </div>

                        <div class="item">
                            <img src="img/art/home6.jpg" alt="...">
                        </div>
                        <div class="item">
                            <img src="img/art/home1.jpg" alt="...">
                        </div>
                        <div class="item">
                            <img src="img/art/home12.jpg" alt="...">
                        </div>
                    </div>
                    <a class="left carousel-control" href="#carouselHome" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carouselHome" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="vb">
                    <a href="uomo.php"><img src="img/conceptman.jpg" alt="Concept Man"></a>
                </div>
                <div class="vb">
                    <div class="socialBox">
                        <div class="col-xs-4 centerize">
                            <a href='https://www.facebook.com/venerenuevo' target="_blank"> <img src="img/fb.png" alt="Facebook"></a>
                        </div>
                        <div class="col-xs-4 centerize">
                            <a href="https://www.youtube.com/channel/UCiXrGE6-TWHguMlVT6XtlgQ/feed" target="_blank"> <img src="img/yt.png" alt="YouTube"></a>
                        </div><div class="col-xs-4 centerize">
                            <a href="gallery.php" target="_self"> <img src="img/gallery.png" alt="gallery"></a>
                        </div>
                    </div>
                </div>
                <div class="vb">
                    <a href="nails.php"><img src="img/nails.jpg" alt="Nails"></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <p style="line-height: 1.2;">
                    Il nuovo centro estetico Venere Nuevo nasce dopo una lunga riflessione notturna. Il progetto prende 
                    le mosse da due precedenti esperienze, con l'obiettivo di creare un luogo unico. La scelta dell'aggettivo 
                    “Nuevo” va nella direzione di veicolare un concetto di estetica e SPA nuovo per la città di Siena.
                </p>
                <p style="line-height: 1.2;">
                    Unire le forze per offrire sempre di più ai clienti; la ricerca e la continua evoluzione nel proprio 
                    lavoro. Ecco i principi che hanno guidato questa evoluzione. Venere Nuevo nella sua nuova location è benessere al servizio della vostra bellezza esteriore ed interiore.
                </p>
            </div>
        </div>
    </div>
    <br/><br/>
    <div class="container" id="boxhome">
        <div class="row">
            <div class="col-sm-4 productItem">
                <div class="productImage">
                    <img src="img/home/ciglia.jpg">
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight: bold;">
                        Colorazione Extention Ciglia
                    </h3>
                    <br/>


                    <p>La colorazione per ciglia mette in risalto i tuoi occhi senza costringerti a dover necessariamente utilizzare ogni giorno matite e mascara per definirne forma e colore.</p>

                    <p>Infoltire e allungare le ciglia per mettere in evidenza gli occhi da Venere Nuevo &egrave; possibile grazie ai trattamenti di extension ciglia resistenti ad acqua, lacrime e sudore.</p>
                </div>
            </div>
            <div class="col-sm-4 productItem">
                <div class="productImage">
                    <img src="img/makeup.jpg">
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight: bold;">
                        Make Up
                    </h3>
                    <br/>
                    <p>Con la prenotazione di un trattamento o l&rsquo;acquisto di un prodotto riceverai un make up personalizzato completamente gratuito!! solo da Venere Nuevo a Siena.</p>
                    <br><p>Sposa, Giorno, Sera, Lezioni personalizzate.</p>
                </div>
            </div>
            <div class="col-sm-4 productItem">
                <div class="productImage">
                    <img src="img/home/skinjewels.jpg">
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight: bold;">

                        Skin Jewels
                    </h3>
                    <br/>
                    <p>Gioielli sulla pelle aderiscono perfettamente, durano a lungo, l&rsquo;alternativa al tatoo e al piercing, anallergici, non lasciano residui o macchie, un caso di successo per il mondo dellla moda !!</p>
                </div>
            </div>
        </div>
    </div>


    <div class="container" id="marginetop">
        <div class="row">
            <div class="col-xs-12 col-sm-3 borderslide"><img class="img-responsive" src="img/barra-left.png"/></div> 
            <div class="col-xs-12 col-sm-6" id="sliderfooter">
                <div id="carouselHome2" class="carousel slide carousel-fade" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="img/frase1.png" alt="...">
                        </div>
                        <div class="item">
                            <img src="img/frase2.png" alt="...">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 borderslide"><img class="img-responsive" src="img/barra-right.png"/></div>     
        </div>
    </div>

</main>


<?php
include_once 'footer.php';
?>
