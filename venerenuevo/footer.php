

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<!--<script language="Javascript" type="text/javascript" src="js/jquery.lwtCountdown-1.0.js"></script>
<script language="javascript" type="text/javascript">
        jQuery(document).ready(function() {
                $('#countdown_dashboard').countDown({
                        targetDate: {
                                'day': 		<?= $config['targetDate']['day'] ?>,
                                'month': 	<?= $config['targetDate']['month'] ?>,
                                'year': 	<?= $config['targetDate']['year'] ?>,
                                'hour': 	<?= $config['targetDate']['hour'] ?>,
                                'min': 		<?= $config['targetDate']['minute'] ?>,
                                'sec': 		<?= $config['targetDate']['second'] ?>
                        }
                });
        });
</script>-->
<div class="push"></div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="innerFooter">


                    <p>Venere Nuevo Srl - Via Celso Cittadini 42-44 - Telefono +39 0577 247858 - +39 0577 594089 - 
                        Email <a href="mailto:info@venerenuevo.it" title="">info@venerenuevo.it</a> - P. Iva 01234230520</p>
                    <p>Orari: Lun: 13:30/19:30 Mar-Ven: 9:30/19:30 Sab: 9:30/13:30</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<script>
    $(document).ready(function() {

        // validate signup form on keyup and submit
        var validator = $("#contatti").validate({
            rules: {
                name: "required",
                message: "required",
                phone: {
                    number: true
                },
                privacy: "required",
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                name: "Campo obbligatorio - Inserisci il tuo nome",
                message: "Cambo obbligatorio - Inserisci un messaggio",
                phone: {
                    required: "Campo obbligatorio - Inserisci un numero di telefono",
                    number: "Il numero telefonico deve essere composto da sole cifre"
                },
                privacy: "Accetazione informativa sulla privacy obbligatoria!<br> ",
                email: {
                    required: "Campo obbligatorio - Inserisci un indirizzo email",
                    email: "Inserisci un indirizzo email valido"
                }
            },
        });


    });
</script>
</body>
</html>
