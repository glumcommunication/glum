<?php
$config = array(
	'messages' => array(	// Error and info messages. Always escape double quotes. " => \"
		'email_exists' 		=> 'Il tuo indirizzo email è già nel nostro database.',
		'no_email' 			=> 'Per favore inserisci un indirizzo email.',
		'email_invalid' 	=> 'Il tuo indirizzo email sembra non essere corretto. <br/>Per favore inserisci un indirizzo email valido.',
		'thank_you' 		=> 'Ti ringraziamo per il tuo interesse nel progetto Case di Siena<br />Ti terremo aggiornato sulle ultime novità.',
		'technical' 		=> 'Stiamo riscontrando dei problemi tecnici. <br />Per favore prova più tardi.'
	),
	'database' => array(	// Database connection settings
		'host'				=> 'localhost',
		'username'			=> 'root',
		'password'			=>	'gnugnu',
		'database'			=>	'my_skalka'
	),
	'targetDate' => array(	// Target countdown date
		'day'				=> 11,
		'month'				=> 6,
		'year'				=> 2014,
		'hour'				=> 16,
		'minute'			=> 0,
		'second'			=> 0
	)
);

?>
