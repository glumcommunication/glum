<?php
$pagename = 'spa';
$pagetitle = 'Urban Spa';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<main>
    <div class="container">
        <h1 class="title">Urban Spa</h1>
        <div id="fitness">
            <!--<div class="col-sm-8">-->
            <div class="row">

                <div class="col-sm-12">
                    <h1 class="title">Biosauna</h1>
                    <div class="productCaption2">
                        <p>&nbsp;Considerate la sauna un momento in cui staccarvi totalmente dalla frenesia quotidiana per isolarvi piacevolmente in una nicchia di grande serenit&agrave;. La temperatura ottimale &egrave; tra i 60&deg; e gli 80&deg;.</p>

                        <p>Inizialmente l&rsquo;aria va tenuta secca poi si getta dell&rsquo;acqua sulle pietre roventi in modo da creare un&rsquo;atmosfera pi&ugrave; carica d&rsquo;umidit&agrave;. Il metodo finlandese prevede infine una breve doccia in acqua gelata, procedendo dai piedi verso il tronco, lasciando per ultimo il viso e la nuca.</p>

                        <p>Si fa quindi seguire una breve attesa di riposo durante la quale si pu&ograve; fare un bagno caldo ai piedi.</p>

                        <p>A questo punto si pu&ograve; rientrare in sauna per un&rsquo;altra decina di minuti e all&rsquo;uscita ripetere la doccia fredda. Al termine ci si distende su un lettino per consentire alla pressione sanguigna di ritrovare i valori normali. In questa fase &egrave; molto proficuo e rigenerante ricevere dei massaggi o essere aspersi con creme idratanti che aiutano a rendere la pelle morbida e levigata.</p>

                        <p>La reintegrazione dei liquidi va fatta gi&agrave; subito dopo la fine del trattamento: bere abbondantemente bevande non zuccherine, non stimolanti e prive di gas serve a restituire all&rsquo;organismo la giusta quantit&agrave; di acqua espulsa durante la sauna. Tisane, succhi di frutta e di verdura sono degli ottimi reintegratori di fluidi e di sali minerali. &nbsp;</p>
                        <p>Aromi e colori concorrono a rendere il momento della doccia una pratica rigenerante contro lo stress accumulato nel corso della giornata. Lo scopo finale, come il nome stesso suggerisce, &egrave; dunque quello di &ldquo;emozionare&rdquo;, regalando quindi intensi momenti di benessere fisico e un conseguente appagamento mentale. A giovarne sar&agrave; altres&igrave; la pelle che, grazie ai differenziati getti d&rsquo;acqua, risulter&agrave; pi&ugrave; morbida ed elastica. Il principio fondamentale &egrave; quello dell&rsquo;alternanza del getto d&rsquo;acqua che si differenzia non solo nella temperatura, calda e fredda, ma anche nella pressione di uscita dall&rsquo;erogatore. Sincronicamente mutano colori e profumi per creare atmosfere e sensazioni in armonia con la fuoriuscita dell&rsquo;acqua. Potrete dunque provare sensazioni e benefici differenti: se la temperatura bassa tende a rassodare i tessuti, il calore avr&agrave; un naturale effetto vasodilatatore, con conseguente depurazione e pulizia dell&rsquo;epidermide.</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <img src="img/urban spa/bagno-turco.jpg">
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <h1 class="title">Bagno Turco con Talassoterapia</h1>
                    <div class="productCaption2">
                        <p>&nbsp;La sua origine &egrave; molto antica e ne troviamo traccia gi&agrave; tra gli egizi, greci e romani. Gli arabi ripresero questa tradizione con bagni simili ma pi&ugrave; piccoli, detti Hammam.</p>

                        <p>Si tratta di un trattamento che si svolge in un ambiente chiuso, nel quale l&#39;umidit&agrave; relativa &egrave; molto alta (dal 90 al 100%). La temperatura interna varia dai 40&deg; ai 60&deg;.</p>

                        <p>Tra i principali benefici del bagno turco ci sono la dilatazione dei vasi sanguigni che favorisce la circolazione e la purificazione della pelle tramite l&#39;apertura dei pori e l&#39;aumento della sudorazione. In pi&ugrave;, ha un effetto tonificante e riduce lo stress.</p>

                        <p>La seduta avviene solitamente in due tempi: inizialmente si sosta per 20 minuti in una stanza ricca di vapore acqueo riscaldato. In seguito ci si reca nella doccia emozionale.</p>

                        <p>Le differenze principali rispetto alla sauna sono l&#39;ambiente umido (mentre &egrave; secco nella sauna) e la temperatura. La sudorazione &egrave; meno forte nell&#39;ambiente umido, ma la permanenza &egrave; maggiore e si continua a sudare anche nell&#39;impacco a riposo, quindi la quantit&agrave; di sudore traspirato pu&ograve; essere addirittura superiore nel bagno turco.</p>

                        <p>&nbsp;</p>

                        <p>La thalassoterapia &egrave; appunto una terapia legata al mare (in greco &ldquo;Thalassa&rdquo; significa proprio mare), promossa gi&agrave; nel &#39;700 dal medico inglese Richard Russel.</p>

                        <p>Oltre ad essere una cura per i disturbi respiratori pi&ugrave; comuni, &egrave; anche un vero e proprio trattamento di bellezza. I risultati che ne derivano sono gli stessi che si sperimentano trascorrendo qualche giorno al mare: pelle morbida e liscia, libera da impurit&agrave; e benessere fisico.</p>

                        <p>Il trattamento consiste in una nuvola di vapore che aiuta i pori a dilatarsi, mentre il sale marino svolge un&#39;azione levigante e purificatrice. Le particelle di iodio contenute nel vapore acqueo vi immergeranno in un vero e proprio aerosol naturale che libera le vie respiratorie.</p>

                        <p>Gli altri elementi presenti nell&#39;acqua marina favoriscono il corretto metabolismo delle cellule. Attraverso i pori dilatati dal vapore, gli oligoelementi penetrano nell&#39;organismo e attraverso i vasi sanguigni portano benefici a tutto il corpo.</p>

                        <p>Per potenziare le capacit&agrave; curative alla thalassoterapia sono spesso associati altri trattamenti come il bagno di fanghi e gli avvolgimenti alle alghe marine. &nbsp;</p>

                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-sm-12">
                    <img src="img/urban spa/sauna2.jpg">
                </div>
            </div>


            <!--<div class="row">
                <div class="col-sm-12">
                    <h1 class="title">Thalassoterapia</h1>
                    <div class="productCaption2">
                        <p>
                            Si tratta di una terapia legata al mare (in greco “Thalassa” significa proprio mare), promossa già nel '700 dal 
                            medico inglese Richard Russel.<br>
                            Oltre ad essere una cura per i disturbi respiratori più comuni, è anche un vero e proprio trattamento di bellezza 
                            che sfrutta l'azione combinata del sole, dello iodio e dell'acqua salata.<br>
                            I risultati che ne derivano sono gli stessi che si sperimenta trascorrendo qualche giorno al mare: pelle morbida e 
                            liscia, libera da impurità e benessere fisico.<br>
                            Il trattamento consiste in una nuvola di vapore che aiuta i pori a dilatarsi, mentre il sale marino svolge un'azione 
                            levigante e purificatrice.<br>
                            Le particelle di iodio contenute nel vapore acqueo vi immergeranno in un vero e proprio aerosol naturale che libera 
                            le vie respiratorie.<br>
                            Gli altri elementi presenti nell'acqua marina (sali minerali come potassio, magnesio e piccole quantità di manganese, 
                            zinco e cromo) favoriscono il corretto metabolismo delle cellule. Attraverso i pori dilatati dal vapore, gli oligoelementi 
                            penetrano nell'organismo e attraverso i vasi sanguigni portano benefici a tutto il corpo.<br>
                            Per potenziare le capacità curative alla thalassoterapia sono spesso associati altri trattamenti  come il bagno di 
                            fanghi e gli avvolgimenti alle alghe marine.
                        </p>
                    </div>
                </div>
            </div>-->
            <!--  <div class="row">
                  <div class="col-sm-6">
                      <img src="img/urban spa/talassoterapia1.jpg">
                  </div>
                  <div class="col-sm-6">
                      <img src="img/urban spa/talassoterapia2.jpg">
                  </div>
              </div>-->
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="title">Doccia Emozionale con nebbia e pioggia tropicale</h1>
                    <div class="productCaption2">
                        <p>&nbsp;Una doccia emozionale &egrave; un momento magico in un percorso di benessere dove l&#39;acqua, elemento vitale, combinata con colori, suoni e odori che stimolano i cinque sensi, avvolgono il corpo creando piacevoli sensazioni. Grazie ad ugelli posizionati sulle pareti e sul soffitto, vengono armonizzate e combinate le propriet&agrave; dell&rsquo;acqua calda e fredda a quella dei profumi ed essenze oleose passando dalla nebbia fredda alla pioggia tropicale. Elemento cardine di questa pratica &egrave; dunque l&#39;acqua, la cui positiva energia viene sublimata dalla presenza di altri fattori.</p>

                        <p>Il trattamento &ldquo;<strong>pioggia tropicale</strong>&rdquo; si esegue all&rsquo;interno di una cabina e che consiste in una leggerissima pioggia arricchita con essenze aromatiche, che crea la sensazione di attraversare una foresta tropicale. L&rsquo;acqua miscelata a 39&deg; viene immessa con un&rsquo;essenza di aroma tropicale/agrumi con un programma terapeutico di luce ambra.</p>

                        <p>Il trattamento denominato &ldquo;<strong>nebbia fredda</strong>&rdquo; &egrave; invece particolarmente indicato dopo un bagno di vapore, questo trattamento che si esegue all&rsquo;interno di una cabina per mezzo di una finissima pioggia nebulizzata, d&agrave; una sferzata di freschezza e dona una piacevole sensazione di vitalit&agrave; e tonicit&agrave; a tutto il corpo.</p>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <img src="img/urban spa/doccia-emozionale.jpg">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="title">Piscina Idromassaggio Jacuzzi</h1>
                    <div class="productCaption2">
                        <p>&nbsp;Concedetevi una pausa dalla frenesia quotidiana nella nostra Jacuzzi idromassaggio, un piacere per il corpo e per la mente. L&#39;idromassaggio &egrave; il trattamento ideale per disintossicare e risvegliare il nostro organismo. La temperatura dell&rsquo;acqua infatti ci pu&ograve; aiutare sia a disintossicare il corpo e a depurarlo, sia a stimolare il sistema circolatorio e il metabolismo.</p>

                        <p>L&rsquo;idromassaggio in acqua costituisce il metodo ideale per recuperare le energie, provando un senso di totale distensione del fisico e della mente. I benefici di un idromassaggio infatti non interessano soltanto il corpo, che appare pi&ugrave; disteso, pi&ugrave; rilassato e pi&ugrave; riposato, ma sono attinenti soprattutto alla sfera mentale e psichica: un idromassaggio fatto bene apporta relax e riposo, aiuta a riacquistare energia e vitalit&agrave; prima di una serata speciale o per affrontare una nuova giornata lavorativa, provando una sensazione di completo rilassamento del corpo e della mente.</p>

                        <p>Durante l&#39;idromassaggio i benefici dell&#39;idroterapia e della massoterapia confluiscono per regalare intense sensazioni di relax, piacere e benessere.    Molti ormai sanno che l&#39;idromassaggio &egrave; in grado di prevenire problemi di natura estetica oltre che fisici.</p>

                        <p>Le bolle d&#39;aria e il flusso dell&#39;acqua comprimono e decomprimono i tessuti apportando vantaggi alla circolazione sanguigna e linfatica. Le gambe diventano pi&ugrave; leggere e meno gonfie mentre la tanto temuta cellulite si riduce grazie alla pressione massaggiante dell&#39;acqua, che favorisce il drenaggio dei liquidi. Ma l&#39;idromassaggio possiede molte altre virt&ugrave; collegate alla possibilit&agrave; di focalizzare l&#39;azione del getto su specifiche parti del corpo: rassoda, combatte l&#39;obesit&agrave; e i problemi connessi all&#39;uso di anticoncezionali orali, riduce la tensione e la stanchezza sessuale, combatte i reumatismi, le artriti, la gotta, l&#39;affaticamento dell&#39;apparato respiratorio e l&#39;insonnia.  La pelle inoltre giova dei rilassanti massaggi dell&#39;acqua tonificandosi, levigandosi e distendendosi, mentre lo stress abbandona progressivamente il corpo che riacquista le forze. &nbsp;</p>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <img src="img/urban spa/sauna.jpg">
                </div>
                <!--
                <div class="col-sm-6">
                    <img src="img/urban spa/idromassaggio.jpg">
                </div>
                <div class="col-sm-6">
                    <img src="img/urban spa/idromassaggio2.jpg">
                </div>-->
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="title">Angolo tisaneria - Naturline</h1>
                    <div class="productCaption2">
                        <p>&nbsp;Rilassatevi assaporando le tisane della Linea Estetica Naturale di Natureline nella nostra area relax all&#39;interno dell&#39;Urban Spa. Linea estetica Naturale &egrave; infatti un&#39;azienda specializzata nella lavorazione di fitoestratti al servizio dell&#39;estetica professionale, con un laboratorio di produzione altamente qualificato che utilizza metodi esclusivi nella preparazione di Tisane, Infusi, Decotti con soli ingredienti naturali. Le tisane Natureline sono delle miscele d&#39;erbe sapientemente formulate e lavorate nel rispetto delle pi&ugrave; antiche tradizioni, senza l&#39;aggiunta n&egrave; di sostanze conservanti, n&egrave; di alcool, n&egrave; di zucchero.</p>

                        <p>Non solo tisane da bere ma anche da applicare direttamente sulla pelle, le tisane Natureline sono infatti un prodotto cos&igrave; puro e ricco di principi attivi da poter essere considerato un cosmetico a tutti gli effetti.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <img src="img/urban spa/tisane.jpg">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="title">Massaggio</h1>
                    <div class="productCaption2">
                        <p>&nbsp;Il massaggio &egrave; la pi&ugrave; antica forma di terapia fisica, utilizzata nel tempo da differenti civilt&agrave;, per alleviare dolori e decontrarre la muscolatura allontanando la fatica. Il massaggio consiste in un insieme di diverse manovre eseguite sul corpo per lenire dolori muscolari o articolari, per tonificare il volume di alcuni tessuti, ma anche per preservare e migliorare il benessere psichico, allentando tensioni e fatiche. Il massaggio esercita la sua diretta influenza sulla circolazione sanguigna e porta al rilassamento dei muscoli e pu&ograve; avere anche un&#39;azione drenante. Una migliore irrorazione sanguigna significa anche un miglior nutrimento della pelle e in genere delle zone trattate.&nbsp;<br />
                            La stimolazione della circolazione sanguigna ottenuta con il massaggio viene dunque a sommarsi con quella provocata dal bagno in sauna. &Egrave; uso comune spalmare la pelle preparata dalla sauna con olio dermatologico: purch&eacute; si tratti di un buon olio facilmente riassorbibile dalla pelle, pu&ograve; essere tranquillamente usato anche per il massaggio. Al Centro Estetico Venere Nuevo potrete scegliere tra diversi tipi di massaggio:</p>
                        <p>Il massaggio ayurvedico &egrave; una terapia dolce, una tecnica di massaggio che aiuta a rilassare il corpo e la mente, ha origini antichissime e pu&ograve; essere un valido aiuto per ripristinare l&#39;equilibrio psicofisico della persona attraverso movimenti lenti e dolci.&nbsp;</p>
                        <p>La riflessologia plantare &egrave; un&rsquo;antica tecnica basata sulla pressione manuale di specifici punti del piede o della mano al fine di stimolare il riflesso dell&rsquo;organo corrispondente e costituisce un importante metodo di riequilibrio e di ripristino del benessere psicofisico.&nbsp;</p>
                        <p>Il massaggio metamorfico si basa sul principio per cui fin dal nostro concepimento abbiamo subito le influenze del mondo esterno. L&#39;obiettivo di questo massaggio non &egrave; soltanto quello di concedere benessere e relax; esso mira alla risoluzione dei blocchi creatisi durante il nostro periodo prenatale, alla rigenerazione di s&eacute;.</p>
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <img src="img/urban spa/lifetime.jpg">
                </div>
                <div class="col-sm-6">
                    <img src="img/urban spa/ritual.jpg">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p>Tipologie di massaggi offerte:</p>
                </div>
                <br/><br/>

                <div class="col-sm-4">

                    <p >
                        Riflessologia Plantare
                    </p>
                </div>                <div class="col-sm-4">

                    <p>
                        Massaggio Californiano
                    </p>
                </div>                <div class="col-sm-4">

                    <p >
                        Massaggio del sole
                    </p>
                </div>                <div class="col-sm-4">

                    <p >
                        Massaggio Ayurvedico Riattivante
                    </p>
                </div>                <div class="col-sm-4">

                    <p>
                        Massaggio Metamorfico
                    </p>
                </div>                <div class="col-sm-4">

                    <p >
                        Riflessologia della mano
                    </p>
                </div>                <div class="col-sm-4">

                    <p >
                        Linfodrenaggio
                    </p>
                </div>                <div class="col-sm-4">

                    <p >
                        Rituale Meditteraneo
                    </p>
                </div>                <div class="col-sm-4">

                    <p>
                        Rituale Spa Sense
                    </p>
                </div>


            </div>
            <div class="container">
                <h1 class="title">Partner trattamenti</h1>
                <hr style="color:#728192; background-color: #728192; border:1px solid #728192;">
                <br/><br/>
                <div id="trattamenti">
                    <div class="col-sm-3 col-sm-offset-3 productItem3">
                        <a href="http://www.benessens.it/" target="_blank"> <img src="img/trattamenti/viso/benessens.jpg"></a>

                    </div>


                    <div class="col-sm-2 productItem3">
                        <a href="http://www.skeyndor.com/" target="_blank"> <img  src="img/urban spa/skeyndor.png"></a>

                    </div>



                </div>
            </div>
            <!--</div>-->
            <!--<div class="col-sm-4">
                    <img src="http://www.placehold.it/640x1000">
            </div>-->
        </div>
    </div>
</main>

<?php
include_once 'footer.php';
?>