<?php
$pagename = 'corpo';
$pagetitle = 'Trattamenti corpo';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<main>
    <div class="container">

        <h1 class="title">Trattamenti Corpo</h1>

        <div id="trattamenti">
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.cesarequaranta.it/cms/index.php/apparecchiature-estetichee/active-press/">
                    <img src="img/trattamenti/corpo/Active-Press.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.cesarequaranta.it/cms/index.php/apparecchiature-estetichee/active-press/"> Active Press </a>                   </h3>

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                      <a target="_blank" href="http://www.essetika.it/body-sculpt.html">  
                          <img src="img/trattamenti/bodysculpt.jpg"></a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.essetika.it/body-sculpt.html">   Body Sculpt</a>
                    </h3>


                </div>
            </div>

            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.thatso.it/en/lines/pure-body-en/42-collagen-bodywave-2/">     <img src="img/trattamenti/dimagrimento/Collagen-BodyWave.jpg"></a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.thatso.it/en/lines/pure-body-en/42-collagen-bodywave-2/">     Collagen Body Wave</a>
                    </h3>


                </div>
            </div>

            <div class="col-sm-4 productItem3">
                <div class="productImage">
                     <a target="_blank" href="http://www.essetika.it/body-slindy/item/279-smooth-slim-draining-pack.html">   
                         <img src="img/trattamenti/bodyslindy.jpg"></a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.essetika.it/body-slindy/item/279-smooth-slim-draining-pack.html">     Body Slindy</a>
                    </h3>


                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.lynfodigit.com">     <img  src="img/trattamenti/corpo/linfodigit.jpg"></a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http.//www.lynfodigit.com"> Lynfodigit</a></h3>



                </div>
            </div>
             <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.essetika.it/tecnologie/kosmoclinic/mesoscience.html">
                    <img  src="img/trattamenti/corpo/mesoscience.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                         <a target="_blank" href="http://www.essetika.it/tecnologie/kosmoclinic/mesoscience.html">
                        MesoScience
                         </a></h3>



                </div>
            </div>
        </div>

    </div>
    <br/><br/>
    <div class="container">
        <h1 class="title">Partner trattamenti</h1>
        <hr style="color:#728192; background-color: #728192; border:1px solid #728192;">
        <br/><br/>
        <div id="trattamenti">
            <div class="col-sm-2 col-sm-offset-1 productItem3">
                <a href="http://www.lynfodigit.com" target="_blank">

                    <img height="30px" src="img/trattamenti/corpo/linfodygit.jpg"></a> 

            </div>
            <div class="col-sm-2 productItem3">
                <a   href="http://www.devacosmetica.it/index.php" target="_blank"> <img height="30px" src="img/trattamenti/viso/that.jpg"></a>

            </div>
           
            <div class="col-sm-2 productItem3">
                <a   href="http://www.cesarequaranta.it/" target="_blank"> <img  height="49px" src="img/trattamenti/corpo/cesarequaranta.png"></a>

            </div>
            <div class="col-sm-2 productItem3">
                <a href="http://www.skeyndor.com/" target="_blank"> <img  src="img/urban spa/skeyndor.png"></a>

            </div>
               <div class="col-sm-2 productItem3">
                   <a href="http://www.ginevragroup.com/" target="_blank"> <img   src="img/trattamenti/corpo/ginevra.png"></a>

            </div>



        </div>
    </div>

</main>





<?php
include_once 'footer.php';
?>