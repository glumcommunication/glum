<?php
$pagename = 'servizi';
$pagetitle = 'Servizi';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
			<main>
				<div class="container">
					<!--<h1 class="title">Servizi</h1>-->
					<div id="fitness">
						<!--<div class="col-sm-8">-->
						<div class="row">
							<div class="col-sm-12">
								<h1 class="title">Open Bar</h1>
							</div>
							<div class="col-sm-12">
                                                            <img src="img/open-bar.jpg">
							</div>
							<div class="col-sm-12">
								<div class="productCaption2">
									<p>
									Alimentarsi e bere con consapevolezza, e farlo scegliendo prodotti naturali e genuini, fa sì che il nostro 
									organismo possa godere delle proprietà benefiche dei prodotti stessi.<br>
									Il concept di Venere Nuevo nasce dall'idea di prendersi cura del benessere dei propri clienti a tutto tondo. 
									Ecco perché la struttura sarà dotata anche di un Open Bar, nel quale potrete rilassarvi unendo così l'utile 
									al dilettevole. Perché infatti non concedersi una pausa assecondando il nostro lato social e prendendoci 
									contemporaneamente cura del nostro organismo?<br>
									L'Open Bar di Venere Nuevo offre una ampia gamma di prodotti naturali e benefici. Il vino è una parte fondamentale 
									della cultura italiana. Il bere consapevole e nelle giuste quantità, rende il vino un prodotto assolutamente sano 
									e in grado di apportare benefici al nostro organismo. Per questo il nostro Open Bar propone una selezione di vini, 
									spumanti e champagne di alta qualità.<br>
									Non solo vini: da Venere Nuevo troverete non solo tutte le proprietà nutritive della frutta nei nostri succhi, ma 
									soprattutto le tisane della Linea Estetica Naturale di Natureline.</p>
                                                                        <br/><p><strong>Happy hour e party</strong></p>

<p>La nostra Urban Spa &egrave; anche il luogo ideale per organizzare le vostre feste private in un ambiente esclusivo e rilassante. Per feste private, eventi, compleannni, spa-party, happy hour o semplicemente per regalarsi insieme ai vostri amici un momento unico e indimenticabile. Ogni evento si trasformer&agrave; in un&#39;occasione davvero unica grazie alle magiche atmosfere della nostra Urban Spa.</p>
                                                                        
									
								</div>
							</div>
						</div>
						<div class="row">
							<!--<div class="col-sm-4 productItem">
							<h1 class="title">Make-up</h1>
								<div class="productImage">
                                                                    <img src="img/make-up.jpg">
								</div>
								<div class="productCaption">
                                                                    <p>Brand storico del settore make up, rappresenta l&rsquo;eleganza e la bellezza di ogni donna. Attenzione per i dettagli e cura negli abbinamenti sono i tratti distintivi del fascino Nee<br/>Con la prenotazione di un trattamento o l&apos;acquisto di un prodotto riceverai un make-up personalizzato completamente gratuito! </p>
								</div>
							</div>
							--><div class="col-sm-4 col-sm-offset-2 productItem">
							<h1 class="title">Mamme</h1>
								<div class="productImage">
                                                                    <img src="img/mamma.jpg">
								</div>
								<div class="productCaption">
									<p>
									Baby-sitter su prenotazione, fasciatoio, riduttore per wc, tritapannolini, giocattoli, matite e pennarelli...<br>
									Mentre la mamma si rilassa e si prende cura di sé, noi ci prendiamo cura dei suoi bambini.
									</p>
								</div>
							</div>
							<div class="col-sm-4 productItem">
							<h1 class="title">Parcheggio</h1>
								<div class="productImage">
                                                                    <img src="img/parcheggio.jpg">
								</div>
								<div class="productCaption">
									<p>
									Il centro Venere Nuevo dispone di un parcheggio privato per difendere la tua privacy.<br>
									Non devi scomodarti a cercare il parcheggio per la tua auto!
									</p>
								</div>
							</div>
						</div>

						<!--</div>-->
						<!--<div class="col-sm-4">
							<img src="http://www.placehold.it/640x1000">
						</div>-->
					</div>
				</div>
			</main>

<?php
include_once 'footer.php';
?>