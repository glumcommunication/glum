<?php
$pagename = 'corpo';
$pagetitle = 'Trattamenti corpo';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<main>
    <div class="container">

        <h1 class="title">Dimagrimento</h1>

        <div id="trattamenti">
            <div class="col-sm-4 col-sm-offset-2 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.bimarpharma.it/">
                    <img src="img/trattamenti/dimagrimento/Ripristiny-1.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.bimarpharma.it/">        Integratori e tisane in filtro    </a>            </h3>

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.natureline.it/proteinline.php?line=proteinline">
                    <img src="img/trattamenti/dimagrimento/nature-line.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.natureline.it/proteinline.php?line=proteinline">      Protein Line</a>
                    </h3>


                </div>
            </div>

            
        </div>

    </div>
    <br/><br/>
    <div class="container">
        <h1 class="title">Partner trattamenti</h1>
        <hr style="color:#728192; background-color: #728192; border:1px solid #728192;">
        <br/><br/>
        <div id="trattamenti">
            <div class="col-sm-2 col-sm-offset-4 productItem3">
                <a href="http://www.natureline.it" target="_blank">
                    <img height="30px" src="img/trattamenti/dimagrimento/proteinline-h.png"></a> 
            </div>
            <div class="col-sm-2 productItem3">
                <a   href="http://www.bimarpharma.it/" target="_blank"> <img height="30px" src="img/trattamenti/dimagrimento/logo-bimar-pharma.jpg"></a>
            </div>
           



        </div>
    </div>

</main>





<?php
include_once 'footer.php';
?>