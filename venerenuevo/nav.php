<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Men&ugrave;</a>
        </div>
        <div class="navbar-collapse collapse" id="mainNav">
            <ul class="nav navbar-nav nav-justified">
                <li><a href="index.php">HOME</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">TRATTAMENTI <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="depilazione.php">Depilazione</a></li>
                        <li><a href="viso.php">Viso</a></li>
                        <li><a href="corpo.php">Corpo</a></li>
                        <li><a href="dimagrimento.php">Dimagrimento</a></li>
                        <li><a href="abbronzatura.php">Abbronzatura</a></li>

                    </ul>
                </li>
                <!--<li><a href="trattamenti.php">TRATTAMENTI</a></li>-->
                <li><a href="fitness.php">FITNESS</a></li>
                <li class="dropdown">
                    <a href="uomo.php" class="dropdown-toggle" data-toggle="dropdown">UOMO <b class="caret"></b></a>
                    <ul class="dropdown-menu">

                        <li><a href="depilazioneuomo.php">Depilazione</a></li>
                        <li><a href="visouomo.php">Viso</a></li>
                        <li><a href="corpouomo.php">Corpo</a></li>
                        <li><a href="dimagrimentouomo.php">Dimagrimento</a></li>
                        <li><a href="lcn.php">Mani e Piedi</a></li>
                    </ul>
                </li>
                <li><a href="spa.php">URBAN SPA</a></li>
                <li><a href="servizi.php">SERVIZI</a></li>
                <li><a href="gallery.php">GALLERY</a></li>
                <li><a href="promozioni.php">PROMOZIONI</a></li>

                <li><a href="contatti.php">CONTATTI</a></li>
            </ul>
        </div>
    </div>
</nav>
</header>
