<?php
$pagename = 'corpo';
$pagetitle = 'Trattamenti corpo';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
<main>
    <div class="container">

        <h1 class="title">Abbronzatura</h1>

        <div id="trattamenti">
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.thatso.it/en/lines/pure-body-en/42-collagen-bodywave-2/">

                        <img src="img/trattamenti/42-Collagen-BodyWave.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.thatso.it/en/lines/pure-body-en/42-collagen-bodywave-2/">
                            Collagene Body Wave </a>               </h3>

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://it.australiangold.ag">
                        <img src="img/trattamenti/australiangold.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://it.australiangold.ag">
                            Australian Gold</a>
                    </h3>


                </div>
            </div>

            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.ergoline.it">
                        <img src="img/trattamenti/ergoline.jpg"></a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.ergoline.it">
                            Ergoline     Evolution 500/600  </a>             </h3>


                </div>
            </div>


            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.ginevragroup.com/colasun6i/">
                        <img  src="img/trattamenti/solariumginevracolasun.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.ginevragroup.com/colasun6i/">
                            Solarium Doccia Ginevra Colasun
                        </a>
                    </h3>

                </div>
            </div>
            <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.thatso.it/">
                        <img  src="img/trattamenti/thatsosprayaerografo.jpg">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.thatso.it/">
                            THAT So SunMakeup aerografo</a></h3>



                </div>
            </div>
               <div class="col-sm-4 productItem3">
                <div class="productImage">
                    <a target="_blank" href="http://www.thatso.it/">
                        <img  src="http://3.bp.blogspot.com/-hpppppH5q_Y/UH-rjIBo2nI/AAAAAAAAFK4/MwUlK7SPquI/s1600/marradi+maroni+2012+026.jpg" class="img-responsive">
                    </a>
                </div>
                <div class="productCaption2">
                    <h3 style="font-weight:bold; font-size:20px; margin-bottom:10px; text-align:center;">
                        <a target="_blank" href="http://www.thatso.it/">
                            MARONI MARRONI e palle liscie</a></h3>



                </div>
            </div>
        </div>

    </div>
    <br/><br/>
    <div class="container">
        <h1 class="title">Partner trattamenti</h1>
        <hr style="color:#728192; background-color: #728192; border:1px solid #728192;">
        <br/><br/>
        <div id="trattamenti">
            <div class="col-sm-2 productItem3">
                <a   href="http://it.australiangold.ag.it/" target="_blank"> <img height="30px" src="img/trattamenti/h-logo.gif"></a>
            </div>
            <div class="col-sm-2 productItem3">
                <a   href="http://www.thatso.it/" target="_blank"> <img height="30px" src="img/trattamenti/viso/that.jpg"></a>
            </div>
            <div class="col-sm-2 productItem3">
                <a   href="http://www.ergoline.it/" target="_blank"> <img height="40px" src="img/trattamenti/logo_ergoline.png"></a>
            </div>
            <div class="col-sm-2 productItem3">
                <a href="http://www.skeyndor.com/" target="_blank"> <img   src="img/urban spa/skeyndor.png"></a>

            </div>
            <div class="col-sm-2 productItem3">
                <a href="http://www.ginevragroup.com/" target="_blank"> <img   src="img/trattamenti/corpo/ginevra.png"></a>

            </div>
              <div class="col-sm-2 productItem3">
                <a   href="http://www.bimarpharma.it/" target="_blank"> <img height="30px" src="img/trattamenti/dimagrimento/logo-bimar-pharma.jpg"></a>
            </div>



        </div>
    </div>

</main>





<?php
include_once 'footer.php';
?>