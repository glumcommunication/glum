<?php
$pagename = 'index';
$pagetitle = 'Sito in Costruzione';
$pagedesc = 'Venere Nuevo Estetica & Urban Spa - Nuova apertura a Siena';
include_once 'dbc.php';
include_once 'header.php';
include_once 'nav.php';
?>
			<main>
				<div class="container">
					<h1 class="title">Trattamenti</h1>
					<div id="trattamenti">
						<div class="col-sm-4 productItem">
							<div class="productImage">
								<img src="http://placehold.it/640x480">
							</div>
							<div class="productCaption">
								<p>
									Lorem ipsum Officia nulla nisi commodo culpa enim commodo nisi dolor nulla anim voluptate aliquip aute mollit 
									elit anim est et consectetur in commodo ea magna veniam exercitation in sed culpa et magna Duis proident.
								</p>
							</div>
						</div>
						<div class="col-sm-4 productItem">
							<div class="productImage">
								<img src="http://placehold.it/640x480">
							</div>
							<div class="productCaption">
								<p>
									Ciao ciao ciao ciao ciao ciao.
								</p>
							</div>
						</div>
						<div class="col-sm-4 productItem">
							<div class="productImage">
								<img src="http://placehold.it/640x480">
							</div>
							<div class="productCaption">
								<p>
									Ciao ciao ciao ciao ciao ciao.
								</p>
							</div>
						</div>
						<div class="col-sm-4 productItem">
							<div class="productImage">
								<img src="http://placehold.it/640x480">
							</div>
							<div class="productCaption">
								<p>
									Ciao ciao ciao ciao ciao ciao.
								</p>
							</div>
						</div>
					</div>
				</div>
			</main>

<?php
include_once 'footer.php';
?>