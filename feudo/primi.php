<?php
$pagename = 'primi';
$pagetitle = 'Menu - Primi';
include_once 'header.php';
?>


<div id="main">
    <div class="left" style="height:465px;padding: 75px 0 0;">
        <div class="menuitem">
            <p><a href="menu.php" title="Antipasti" <?php if ($pagename == 'antipasti') {echo 'class="highlight"';} ?>>Antipasti</a></p>
        </div>
        <div class="menuitem">
            <p><a href="pizze.php" title="Pizze" <?php if ($pagename == 'pizze') {echo 'class="highlight"';} ?>>Pizze e Ciaccini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="primi.php" title="Primi" <?php if ($pagename == 'primi') {echo 'class="highlight"';} ?>>Primi</a></p>
        </div>
        <div class="menuitem">
            <p><a href="vini.php" title="Vini" <?php if ($pagename == 'vini') {echo 'class="highlight"';} ?>>Vini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="secondi.php" title="Secondi" <?php if ($pagename == 'secondi') {echo 'class="highlight"';} ?>>Secondi</a></p>
        </div>
        <div class="menuitem">
            <p><a href="panini.php" title="Panini" <?php if ($pagename == 'panini') {echo 'class="highlight"';} ?>>Panini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="contorni.php" title="Contorni" <?php if ($pagename == 'contorni') {echo 'class="highlight"';} ?>>Contorni</a></p>
        </div>
        <div class="menuitem">
            <p><a href="dessert.php" title="Dessert" <?php if ($pagename == 'dessert') {echo 'class="highlight"';} ?>>Dessert</a></p>
        </div>       
    </div>
    <div class="right">
        <h1>Primi <!--<span style="font-style: italic; font-weight: lighter; font-size: 24px;margin-left: 15px;">Appetizers</span>--></h1>
        <div id="scrollbarleft">
            <div class="item_it"><p>Ribollita</p></div><div class="item_it_p"><p>&euro; 9,00</p></div>
            <div class="item_en"><p>Hot soup with mixed vegetables and bread</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Gnocchetti saporiti con cavolfiore e salsiccia toscana</p></div><div class="item_it_p"><p>&euro; 9,00</p></div>
            <div class="item_en"><p>Tasty dumplings with cabbage and Tuscan sausage</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Pici cacio e pepe</p></div><div class="item_it_p"><p>&euro; 9,00</p></div>
            <div class="item_en"><p>Pici with cheese and black pepper</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Pici al sugo di cinghiale</p></div><div class="item_it_p"><p>&euro; 9,50</p></div>
            <div class="item_en"><p>Pici with a wild boar sauce</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Risotto ai carciofi e zafferano di San Gimignano</p></div><div class="item_it_p"><p>&euro; 9,50</p></div>
            <div class="item_en"><p>Risotto with artichokes and saffron of San Gimignano</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Pappardelle ai funghi porcini</p></div><div class="item_it_p"><p>&euro; 10,00</p></div>
            <div class="item_en"><p>Pappardelle with porcini mushrooms</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Tortellacci freschi al sugo di zucca gialla e gherigli di noci</p></div><div class="item_it_p"><p>&euro; 10,00</p></div>
            <div class="item_en"><p>Fresh tortellacci with yellow pumpkin sauce and walnuts</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Fagottini ripieni di pecorino e pere in crema di pecorino di fossa e pistacchio</p></div><div class="item_it_p"><p>&euro; 10,00</p></div>
            <div class="item_en"><p>Fagottini stuffed with sheep's milk cheese and pears with formaggio di fossa and pistachio</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Lunette ripiene di tartufo con salsa di tartufo nero</p></div><div class="item_it_p"><p>&euro; 12,00</p></div>
            <div class="item_en"><p>Lunette stuffed with truffle with black truffle sauce</p></div><div class="item_en_p"><p></p></div>

        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>
