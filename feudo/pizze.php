<?php
$pagename = 'pizze';
$pagetitle = 'Menu - Pizze, ciaccini e calzoni';
include_once 'header.php';
?>


<div id="main">
    <div class="left" style="height:465px;padding: 75px 0 0;">
        <div class="menuitem">
            <p><a href="menu.php" title="Antipasti" <?php if ($pagename == 'antipasti') {echo 'class="highlight"';} ?>>Antipasti</a></p>
        </div>
        <div class="menuitem">
            <p><a href="pizze.php" title="Pizze" <?php if ($pagename == 'pizze') {echo 'class="highlight"';} ?>>Pizze e Ciaccini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="primi.php" title="Primi" <?php if ($pagename == 'primi') {echo 'class="highlight"';} ?>>Primi</a></p>
        </div>
        <div class="menuitem">
            <p><a href="vini.php" title="Vini" <?php if ($pagename == 'vini') {echo 'class="highlight"';} ?>>Vini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="secondi.php" title="Secondi" <?php if ($pagename == 'secondi') {echo 'class="highlight"';} ?>>Secondi</a></p>
        </div>
        <div class="menuitem">
            <p><a href="panini.php" title="Panini" <?php if ($pagename == 'panini') {echo 'class="highlight"';} ?>>Panini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="contorni.php" title="Contorni" <?php if ($pagename == 'contorni') {echo 'class="highlight"';} ?>>Contorni</a></p>
        </div>
        <div class="menuitem">
            <p><a href="dessert.php" title="Dessert" <?php if ($pagename == 'dessert') {echo 'class="highlight"';} ?>>Dessert</a></p>
        </div>     
    </div>
    <div class="right">
        <h1>Pizze e ciaccini <!--<span style="font-style: italic; font-weight: lighter; font-size: 24px;margin-left: 15px;">Appetizers</span>--></h1>
        <div id="scrollbarleft">
            <h2>Ciaccini</h2>
            <div class="item_it"><p>Circe</p></div><div class="item_it_p"><p>&euro; 4,50</p></div>
            <div class="item_en"><p>Ciaccino all'olio</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Caino</p></div><div class="item_it_p"><p>&euro; 6,50</p></div>
            <div class="item_en"><p>Ciaccino mozzarella, pomodorini, capperi e basilico</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Ciacco</p></div><div class="item_it_p"><p>&euro; 7,00</p></div>
            <div class="item_en"><p>Ciaccino prosciutto cotto e mozzarella</p></div><div class="item_en_p"><p></p></div>
            
            <h2>Pizze</h2>
            <div class="item_it"><p>Re Artù</p></div><div class="item_it_p"><p>&euro; 7,00</p></div>
            <div class="item_en"><p>Pizza pomodoro e mozzarella</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Lancillotto</p></div><div class="item_it_p"><p>&euro; 9,00</p></div>
            <div class="item_en"><p>Pizza pomodoro e bufala</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Bruto</p></div><div class="item_it_p"><p>&euro; 7,00</p></div>
            <div class="item_en"><p>Pizza pomodoro e aglio</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Marte</p></div><div class="item_it_p"><p>&euro; 8,00</p></div>
            <div class="item_en"><p>Pizza pomodoro, capperi, acciughe e origano</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Minotauro</p></div><div class="item_it_p"><p>&euro; 8,50</p></div>
            <div class="item_en"><p>Pizza pomodoro, mozzarella, prosciutto cotto e funghi</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Beatrice</p></div><div class="item_it_p"><p>&euro; 9,00</p></div>
            <div class="item_en"><p>Pizza pomodoro, mozzarella, prosciutto crudo e funghi</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Ulisse</p></div><div class="item_it_p"><p>&euro; 9,00</p></div>
            <div class="item_en"><p>Pizza pomodoro, mozzarella, prosciutto cotto, funghi, olive, carciofi e salame piccante</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Virgilio</p></div><div class="item_it_p"><p>&euro; 8,50</p></div>
            <div class="item_en"><p>Pizza pomodoro, provola e prosciutto crudo</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Annibale</p></div><div class="item_it_p"><p>&euro; 8,00</p></div>
            <div class="item_en"><p>Pizza pomodoro, mozzarella e wurstel</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Orlando</p></div><div class="item_it_p"><p>&euro; 9,00</p></div>
            <div class="item_en"><p>Pizza pomodoro, aglio, rigatino e fagioli</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Penelope</p></div><div class="item_it_p"><p>&euro; 9,50</p></div>
            <div class="item_en"><p>Pizza pomodoro, mozzarella, speck, provola e rucola</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Carlo Magno</p></div><div class="item_it_p"><p>&euro; 8,00</p></div>
            <div class="item_en"><p>Pizza pomodoro, mozzarella, olio piccante e salame piccante</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Pluto</p></div><div class="item_it_p"><p>&euro; 8,00</p></div>
            <div class="item_en"><p>Pizza pomodoro, mozzarellae salsiccia</p></div><div class="item_en_p"><p></p></div>

            <div class="item_it"><p>Ercole</p></div><div class="item_it_p"><p>&euro; 8,50</p></div>
            <div class="item_en"><p>Pizza, pomodoro, salsiccia, cipolla e parmigiano</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Orfeo</p></div><div class="item_it_p"><p>&euro; 7,50</p></div>
            <div class="item_en"><p>Pizza, pomodoro, mozzarella, pomodorini e olive nere</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Icaro</p></div><div class="item_it_p"><p>&euro; 9,50</p></div>
            <div class="item_en"><p>Pizza, pomodoro, mozzarella e porcini</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Maometto</p></div><div class="item_it_p"><p>&euro; 9,00</p></div>
            <div class="item_en"><p>Pizza pomodoro, mozzarella, zucchine, melanzane e peperoni grigliati</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Achille</p></div><div class="item_it_p"><p>&euro; 9,00</p></div>
            <div class="item_en"><p>Pizza pomodoro, mozzarella, gorgonzola, pecorino e parmigiano</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Conte Ugolino</p></div><div class="item_it_p"><p>&euro; 8,00</p></div>
            <div class="item_en"><p>Pizza pomodoro, melanzana e parmigiano</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Cassio</p></div><div class="item_it_p"><p>&euro; 8,00</p></div>
            <div class="item_en"><p>Pizza pomodoro, mozzarella, tonno e cipolla</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Tizio</p></div><div class="item_it_p"><p>&euro; 8,00</p></div>
            <div class="item_en"><p>Pizza pomodoro, ricotta e olive</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Medusa</p></div><div class="item_it_p"><p>&euro; 9,00</p></div>
            <div class="item_en"><p>Pizza pomodoro, provola affumicata, gamberetti e rucola</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Giove</p></div><div class="item_it_p"><p>&euro; 8,00</p></div>
            <div class="item_en"><p>Pizza pomodoro, salsa 5 formaggi e noci</p></div><div class="item_en_p"><p></p></div>
            
            <h2>Calzoni</h2>
            <div class="item_it"><p>Teseo</p></div><div class="item_it_p"><p>&euro; 8,00</p></div>
            <div class="item_en"><p>Calzone pomodoro, mozzarella, prosciutto cotto</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Cicerone</p></div><div class="item_it_p"><p>&euro; 8,50</p></div>
            <div class="item_en"><p>Calzone pomodoro, mozzarella, prosciutto cotto e ricotta</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Dante</p></div><div class="item_it_p"><p>&euro; 9,00</p></div>
            <div class="item_en"><p>Calzone pomodoro, mozzarella, salsiccia e funghi</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it" style="width:432px;"><p style=" font-size: 12px;">* Potete modificare la Vostra pizza con un sovrapprezzo di 1&euro; ad ingrediente.</p></div>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>
