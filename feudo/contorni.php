<?php
$pagename = 'contorni';
$pagetitle = 'Menu - Contorni';
include_once 'header.php';
?>


<div id="main">
    <div class="left" style="height:465px;padding: 75px 0 0;">
        <div class="menuitem">
            <p><a href="menu.php" title="Antipasti" <?php if ($pagename == 'antipasti') {echo 'class="highlight"';} ?>>Antipasti</a></p>
        </div>
        <div class="menuitem">
            <p><a href="pizze.php" title="Pizze" <?php if ($pagename == 'pizze') {echo 'class="highlight"';} ?>>Pizze e Ciaccini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="primi.php" title="Primi" <?php if ($pagename == 'primi') {echo 'class="highlight"';} ?>>Primi</a></p>
        </div>
        <div class="menuitem">
            <p><a href="vini.php" title="Vini" <?php if ($pagename == 'vini') {echo 'class="highlight"';} ?>>Vini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="secondi.php" title="Secondi" <?php if ($pagename == 'secondi') {echo 'class="highlight"';} ?>>Secondi</a></p>
        </div>
        <div class="menuitem">
            <p><a href="panini.php" title="Panini" <?php if ($pagename == 'panini') {echo 'class="highlight"';} ?>>Panini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="contorni.php" title="Contorni" <?php if ($pagename == 'contorni') {echo 'class="highlight"';} ?>>Contorni</a></p>
        </div>
        <div class="menuitem">
            <p><a href="dessert.php" title="Dessert" <?php if ($pagename == 'dessert') {echo 'class="highlight"';} ?>>Dessert</a></p>
        </div>    
    </div>
    <div class="right">
        <h1>Contorni <!--<span style="font-style: italic; font-weight: lighter; font-size: 24px;margin-left: 15px;">Appetizers</span>--></h1>
        <div id="scrollbarleft">
            <div class="item_it"><p>Spinaci all'agro</p></div><div class="item_it_p"><p>&euro; 6,00</p></div>
            <div class="item_en"><p>Spinach and sour</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Patate al forno</p></div><div class="item_it_p"><p>&euro; 5,00</p></div>
            <div class="item_en"><p>Roasted potatoes</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Fagioli al fiasco</p></div><div class="item_it_p"><p>&euro; 6,00</p></div>
            <div class="item_en"><p>Fiasco beans</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Carciofi alla "Giudea"</p></div><div class="item_it_p"><p>&euro; 7,00</p></div>
            <div class="item_en"><p>Artichockes the "Giudea" way</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Insalata verde</p></div><div class="item_it_p"><p>&euro; 5,00</p></div>
            <div class="item_en"><p>Green salad</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Insalata al pomodoro</p></div><div class="item_it_p"><p>&euro; 6,00</p></div>
            <div class="item_en"><p>Salad with tomatoes</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Insalatissima</p></div><div class="item_it_p"><p>&euro; 7,00</p></div>
            <div class="item_en"><p>Insalatissima</p></div><div class="item_en_p"><p></p></div>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>
