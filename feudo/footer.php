        <footer>
            <p style="float:left;">
                Ristorante Il Feudo - Piazza Roma, 16 - Monteriggioni (Siena) | Cell.: +39 338 5493093 - Tel. e Fax: +39 0577 304108 |
                <!--<a href="mailto:info@ilfeudoristorante.com">info@ilfeudoristorante.com</a> | -->P. Iva: 01109880524
            </p>
            <p style="float: right;margin: 0 0 0 0;">
                <a href="https://www.facebook.com/pages/Il-feudo/383251205098289" title="Facebook" target="_BLANK" class="social"><img src="images/facebook.png" alt="Facebook"></a>
                <a href="mailto:info@ilfeudoristorante.com" title="Scrivici una mail" class="social"><img src="images/mail.png" alt="Contattaci via mail"></a>
            </p>
            
        </footer>
        <!--<p style="text-align: center;"><img src="images/glumlogo.png" alt="GLuM Communication"></p>-->
    </div>
</body>
</html>