<script>
(function($){
    $(document).ready(function(){
        $("#placetext").mCustomScrollbar({
            scrollButtons:{
                enable:false
            },
            scrollInertia:0,
            advanced:{
                updateOnContentResize:true
            },
        });
    })
})(jQuery);
</script>
<div id="placetext">
    <h1>Pienza</h1>
    <p>
        Difficilmente si può trovare la storia di un paese e quella di un personaggio storico così 
        tanto legatte come la storia di Pienza e quella di Enea Silvio Piccolomini, che prima di essere eletto 
        papa Pio II nacque nel paese, un borgo medioevale che allora si chiamava ancora Corsignano e stava 
        cadendo lentamente nel degrado.
    </p>
    <p>
        L'impulso del papa umanista fece sì che la cittadina acquistasse nel breve volgere di pochi anni un volto 
        completamente diverso, affidato alle sapienti mani dell'architetto Bernardo Rossellino. Il risultato fu – ed è ancora – 
        veramente notevole, tanto da essere considerato uno degli esempi più riusciti dell'utopia urbanistica rinascimentale 
        della “città ideale”.
    </p>
    <p>
        Al giorno d'oggi Pienza è un piccolo comune adorato dai turisti soprattutto per la centrale piazza dedicata proprio a Pio 
        II, il vero e proprio centro del progetto di Rossellino. Suggestiva la visita del Romitorio che si trova poco fuori dall'insediamento 
        cittadino, un luogo sacro scavato nella roccia in epoca medioevale, nel quale si aprono diverse finestre che guardano la splendida 
        campagna circostante della Val d'Orcia.<br> 
        Proprio dal territorio circostante arrivano le produzioni tipiche della zona, come i vini della Val d'Orcia e il formaggio Pecorino di Pienza.
    </p>
</div>