<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="description" content="<?php echo $pagedesc ?>">
<meta name= "keywords" content= "Ristorante Il Feudo, Monteriggioni, pizzeria, bar, panini, pizze, cucina toscana, siena, chianti, carta dei vini, gestione familiare, trattoria" />
<link href="css/style.css" rel="stylesheet" />
<meta name="author" content="Cesare Rinaldi">
<link href="images/favicon.png" rel="icon" type="image/x-icon">
<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Vollkorn:400italic,700italic,400,700' rel='stylesheet' type='text/css'>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/loadpic.js"></script>
<script type="text/javascript" src="js/loadNewPage.js"></script>
<!-- mousewheel plugin -->
<script src="js/jquery.mousewheel.min.js"></script>
<!-- custom scrollbars plugin -->
<script src="js/jquery.mCustomScrollbar.js"></script>

<script>
(function($){
    $(document).ready(function(){
        $("#scrollbarleft").mCustomScrollbar({
            scrollButtons:{
                enable:false
            },
            scrollInertia:0,
            advanced:{
                updateOnContentResize:true
            },
        });
    }),
    $(document).ready(function(){
        $("#scrollbarright").mCustomScrollbar({
            scrollButtons:{
                enable:false
            },
            scrollInertia:0,
            advanced:{
                updateOnContentResize:true
            },
        });
    })
})(jQuery);
</script>
<title><? echo $pagetitle ?> | Il Feudo Ristorante Pizzeria Bar a Monteriggioni dal 1989</title>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-40293387-1', 'ilfeudoristorante.com');
  ga('send', 'pageview');

</script>
</head>
<body>
    <div id="wrapper">
        <header>
            <div id="hleft">
                <ul>
                    <li class="topmenul" <?php if ($pagename == 'index') {echo 'id="selected"';} ?>><a href="/">Home</a></li>
                    <li class="topmenul" <?php if ($pagename == 'dintorni') {echo 'id="selected"';} ?>><a href="dintorni.php">Nei dintorni</a></li>
                    <li class="topmenul" <?php if ($pagename == 'antipasti' || $pagename == 'primi' || $pagename == 'secondi' 
                            || $pagename == 'contorni' || $pagename == 'vini' || $pagename == 'pizze' || $pagename == 'dessert' 
                            || $pagename == 'panini') {echo 'id="selected"';} ?>><a href="menu.php">Menu</a></li>
                </ul>
            </div>
            <a href="/" title="Ristorante il Feudo"><div id="hmiddle">
                
            </div></a>
            <div id="hright">
                <ul>
                    <li class="topmenur" <?php if ($pagename == 'contatti') {echo 'id="selected"';} ?>><a href="contatti.php">Contatti</a></li>
                    <li class="topmenur" <?php if ($pagename == 'news') {echo 'id="selected"';} ?>><a href="news.php">News</a></li>
                    <li class="topmenur" <?php if ($pagename == 'gallery') {echo 'id="selected"';} ?>><a href="gallery.php">Gallery</a></li>
                </ul>                
            </div>            
        </header>
