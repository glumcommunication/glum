<?php
$pagename = 'panini';
$pagetitle = 'Menu - Panini';
include_once 'header.php';
?>


<div id="main">
    <div class="left" style="height:465px;padding: 75px 0 0;">
        <div class="menuitem">
            <p><a href="menu.php" title="Antipasti" <?php if ($pagename == 'antipasti') {echo 'class="highlight"';} ?>>Antipasti</a></p>
        </div>
        <div class="menuitem">
            <p><a href="pizze.php" title="Pizze" <?php if ($pagename == 'pizze') {echo 'class="highlight"';} ?>>Pizze e Ciaccini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="primi.php" title="Primi" <?php if ($pagename == 'primi') {echo 'class="highlight"';} ?>>Primi</a></p>
        </div>
        <div class="menuitem">
            <p><a href="vini.php" title="Vini" <?php if ($pagename == 'vini') {echo 'class="highlight"';} ?>>Vini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="secondi.php" title="Secondi" <?php if ($pagename == 'secondi') {echo 'class="highlight"';} ?>>Secondi</a></p>
        </div>
        <div class="menuitem">
            <p><a href="panini.php" title="Panini" <?php if ($pagename == 'panini') {echo 'class="highlight"';} ?>>Panini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="contorni.php" title="Contorni" <?php if ($pagename == 'contorni') {echo 'class="highlight"';} ?>>Contorni</a></p>
        </div>
        <div class="menuitem">
            <p><a href="dessert.php" title="Dessert" <?php if ($pagename == 'dessert') {echo 'class="highlight"';} ?>>Dessert</a></p>
        </div>         
    </div>
    <div class="right">
        <h1>Panini <!--<span style="font-style: italic; font-weight: lighter; font-size: 24px;margin-left: 15px;">Appetizers</span>--></h1>
        <div id="scrollbarleft">
            <div class="item_it"><p>Prosciutto crudo e crema ai 4 formaggi tartufata</p></div><div class="item_it_p"><p>&euro; 6,00</p></div>
            <div class="item_en"><p>Cured ham and truffle cream of 4 cheeses</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Prosciutto cotto, fontina e carciofi</p></div><div class="item_it_p"><p>&euro; 6,00</p></div>
            <div class="item_en"><p>Cooked ham, Fontina and Artichokes</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Salame, fontina e pomodoro</p></div><div class="item_it_p"><p>&euro; 6,00</p></div>
            <div class="item_en"><p>Salami, Fontina and tomato</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Prosciutto crudo, funghi e pomodoro</p></div><div class="item_it_p"><p>&euro; 6,00</p></div>
            <div class="item_en"><p>Cured ham, mushrooms and tomato</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Prosciutto cotto, pomodoro e mozzarella</p></div><div class="item_it_p"><p>&euro; 6,00</p></div>
            <div class="item_en"><p>Cooked ham, tomato and mozzarella</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Pecorino, asparagi e pomodoro</p></div><div class="item_it_p"><p>&euro; 6,00</p></div>
            <div class="item_en"><p>Sheep's milk cheese, asparagus and tomato</p></div><div class="item_en_p"><p></p></div>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>
