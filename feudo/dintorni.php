<?php
$pagename='dintorni';
$pagetitle='Dintorni';
include_once 'header.php';

?>


<div id="main">
    <div class="left">
        <a href="" onclick="loadNewPage('siena.php'); return false;">
        <div class="place">
            <div class="placetitle">
                Siena
            </div>            
            <div class="placepic">
                <img src="images/siena.jpg" alt="Siena">
            </div>
        </div>
        </a>
        <a href="" onclick="loadNewPage('sangimignano.php'); return false;">
        <div class="place">
            <div class="placetitle">
                San Gimignano
            </div>            
            <div class="placepic">
                <img src="images/sangimignano.jpg" alt="San Gimignano">
            </div>
        </div>
        </a>
        <a href="" onclick="loadNewPage('montalcino.php'); return false;">
        <div class="place">
            <div class="placetitle">
                Montalcino
            </div>            
            <div class="placepic">
                <img src="images/montalcino.jpg" alt="Montalcino">
            </div>
        </div>
        </a>
        <a href="" onclick="loadNewPage('pienza.php'); return false;">
        <div class="place">
            <div class="placetitle">
                Pienza
            </div>            
            <div class="placepic">
                <img src="images/pienza.jpg" alt="Pienza">
            </div>
        </div>
        </a>
    </div>
    <div class="right">
         <div id="scrollbarright"> 
        
        
<h1>Siena</h1>
         
<p>La città di Siena è universalmente riconosciuta come una delle gemme del patrimonio storico-artistico italiano. Il suo centro storico, dichiarato patrimonio dell'Unesco nel 1995, è probabilmente l'esempio meglio conservato di urbanistica medioevale.<br>
Se le origini dell'insediamento senese sono ancora incerte – etrusche, romane, galliche o addirittura egiziane per alcune ricostruzioni piuttosto fantasiose – è certo che il periodo medioevale è stato l'età dell'oro per la cittadina toscana, che grazie ai propri banchieri arrivò a rivaleggiare con Firenze per potenza e splendore. Proprio la rivalità con Firenze condusse al declino la Repubblica Senese, che nel 1559 finì per essere definitivamente asservita ai Medici grazie all'aiuto degli Spagnoli.<br>
Del periodo di splendore è rimasto un enorme patrimonio artistico. Se per i turisti sono tappe obbligate Piazza del Campo con il Palazzo Comunale e la famosissima Torre del Mangia e il maestoso Duomo costruito in marmo bianco e verde, sperimentare davvero Siena significa anche perdersi nel reticolo di ripidi e stretti vicoli medioevali.<br>
La visita al Museo Archeologico, al complesso museale Santa Maria della Scala e alla Pinacoteca nazionale sono necessari per chiunque voglia avere un'idea, seppur incompleta, dell'enorme produzione artistica degli artisti cittadini in  epoca medioevale e rinascimentale.<br>
Parlare di Siena significa parlare anche del Palio, manifestazione che da secoli scalda il cuore dei senesi, divisi nell'amore per 17 contrade, ma anche dei turisti che visitano la città in quei giorni. Ma significa anche parlare delle stupende campagne che circondano la città, punteggiate di innumerevoli aziende agricole che producono vini, olio, formaggi e salumi tra i migliori che si possano trovare.</p>
        
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>
