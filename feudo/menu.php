<?php
$pagename = 'antipasti';
$pagetitle='Menu - Antipasti';
include_once 'header.php';
?>


<div id="main">
    <div class="left" style="height:465px;padding: 75px 0 0;">
        <div class="menuitem">
            <p><a href="menu.php" title="Antipasti" <?php if ($pagename == 'antipasti') {echo 'class="highlight"';} ?>>Antipasti</a></p>
        </div>
        <div class="menuitem">
            <p><a href="pizze.php" title="Pizze" <?php if ($pagename == 'pizze') {echo 'class="highlight"';} ?>>Pizze e Ciaccini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="primi.php" title="Primi" <?php if ($pagename == 'primi') {echo 'class="highlight"';} ?>>Primi</a></p>
        </div>
        <div class="menuitem">
            <p><a href="vini.php" title="Vini" <?php if ($pagename == 'vini') {echo 'class="highlight"';} ?>>Vini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="secondi.php" title="Secondi" <?php if ($pagename == 'secondi') {echo 'class="highlight"';} ?>>Secondi</a></p>
        </div>
        <div class="menuitem">
            <p><a href="panini.php" title="Panini" <?php if ($pagename == 'panini') {echo 'class="highlight"';} ?>>Panini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="contorni.php" title="Contorni" <?php if ($pagename == 'contorni') {echo 'class="highlight"';} ?>>Contorni</a></p>
        </div>
        <div class="menuitem">
            <p><a href="dessert.php" title="Dessert" <?php if ($pagename == 'dessert') {echo 'class="highlight"';} ?>>Dessert</a></p>
        </div>        
    </div>
    <div class="right">
        <h1>Antipasti <!--<span style="font-style: italic; font-weight: lighter; font-size: 24px;margin-left: 15px;">Appetizers</span>--></h1>
        <div id="scrollbarleft">
            <div class="item_it"><p>Bruschetta aglio e olio</p></div><div class="item_it_p"><p>&euro; 3,50</p></div>
            <div class="item_en"><p>Bruschetta garlic and oil</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Bruschetta al pomodoro</p></div><div class="item_it_p"><p>&euro; 5,00</p></div>
            <div class="item_en"><p>Bruschetta with tomato</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Crostoni misti</p></div><div class="item_it_p"><p>&euro; 7,00</p></div>
            <div class="item_en"><p>Mixed croutons</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Cofanetto di sfoglia calda con formaggio caprino senese</p></div><div class="item_it_p"><p>&euro; 8,50</p></div>
            <div class="item_en"><p>Set of hot pasta with Siena goat cheese</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Crostone pecorino e funghi</p></div><div class="item_it_p"><p>&euro; 6,50</p></div>
            <div class="item_en"><p>Crouton cheese and mushrooms</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Crostone 4 formaggi, prosciutto crudo e tartufo</p></div><div class="item_it_p"><p>&euro; 7,00</p></div>
            <div class="item_en"><p>4 cheeses crouton, truffle and cured ham</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Crostone con lardo di cinta senese</p></div><div class="item_it_p"><p>&euro; 7,00</p></div>
            <div class="item_en"><p>Crouton with lard of Cinta Senese</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Misto pecorini di Monteriggioni</p></div><div class="item_it_p"><p>&euro; 9,00</p></div>
            <div class="item_en"><p>Mix of sheep's milk cheeses of Monteriggioni</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Insalatina di petto d'anatra all'aceto balsamico</p></div><div class="item_it_p"><p>&euro; 9,00</p></div>
            <div class="item_en"><p>Duck breast salad with balsamic vinegar</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Il Feudo - Crostini, salumi misti e pecorino</p></div><div class="item_it_p"><p>&euro; 10,00</p></div>
            <div class="item_en"><p>Il Feudo - Croutons, mixed of salted pork meats and sheep's milk cheese</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Podere Toscano - Prosciutto crudo, funghi porcini e bruschetta aglio e olio</p></div><div class="item_it_p"><p>&euro; 10,00</p></div>
            <div class="item_en"><p>Podere Toscano - Cured ham, porcini mushrooms and bruschetta garlic and oil</p></div><div class="item_en_p"><p></p></div>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>
