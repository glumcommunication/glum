<?php
$pagename = 'secondi';
$pagetitle = 'Menu - Secondi';
include_once 'header.php';
?>


<div id="main">
    <div class="left" style="height:465px;padding: 75px 0 0;">
        <div class="menuitem">
            <p><a href="menu.php" title="Antipasti" <?php if ($pagename == 'antipasti') {echo 'class="highlight"';} ?>>Antipasti</a></p>
        </div>
        <div class="menuitem">
            <p><a href="pizze.php" title="Pizze" <?php if ($pagename == 'pizze') {echo 'class="highlight"';} ?>>Pizze e Ciaccini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="primi.php" title="Primi" <?php if ($pagename == 'primi') {echo 'class="highlight"';} ?>>Primi</a></p>
        </div>
        <div class="menuitem">
            <p><a href="vini.php" title="Vini" <?php if ($pagename == 'vini') {echo 'class="highlight"';} ?>>Vini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="secondi.php" title="Secondi" <?php if ($pagename == 'secondi') {echo 'class="highlight"';} ?>>Secondi</a></p>
        </div>
        <div class="menuitem">
            <p><a href="panini.php" title="Panini" <?php if ($pagename == 'panini') {echo 'class="highlight"';} ?>>Panini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="contorni.php" title="Contorni" <?php if ($pagename == 'contorni') {echo 'class="highlight"';} ?>>Contorni</a></p>
        </div>
        <div class="menuitem">
            <p><a href="dessert.php" title="Dessert" <?php if ($pagename == 'dessert') {echo 'class="highlight"';} ?>>Dessert</a></p>
        </div>     
    </div>
    <div class="right">
        <h1>Secondi <!--<span style="font-style: italic; font-weight: lighter; font-size: 24px;margin-left: 15px;">Appetizers</span>--></h1>
        <div id="scrollbarleft">
            <div class="item_it"><p>Scaloppina di manzo dello chef alla pizzaiola</p></div><div class="item_it_p"><p>&euro; 12,00</p></div>
            <div class="item_en"><p>Escalop of beef at pizzaiola</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Stracotto di manzo alla chiantigiana</p></div><div class="item_it_p"><p>&euro; 12,00</p></div>
            <div class="item_en"><p>Chiantigiana beef stew</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Pollo di campagna ai peperoni</p></div><div class="item_it_p"><p>&euro; 13,00</p></div>
            <div class="item_en"><p>Country chicken with peppers</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Cinghiale in umido con polenta grigliata</p></div><div class="item_it_p"><p>&euro; 15,00</p></div>
            <div class="item_en"><p>Wild boar stew with grilled polenta</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Filetto di Cinta Senese lardellato agli aromi</p></div><div class="item_it_p"><p>&euro; 14,00</p></div>
            <div class="item_en"><p>Cinta Senese Fillet larded with aromas</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Cosciotto d'agnello medievale al forno</p></div><div class="item_it_p"><p>&euro; 15,00</p></div>
            <div class="item_en"><p>Roasted leg of lamb</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Tagliata di manzo all'Antica</p></div><div class="item_it_p"><p>&euro; 17,00</p></div>
            <div class="item_en"><p>Old style sliced beef fillet</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Controfiletto di manzo ai funghi porcini</p></div><div class="item_it_p"><p>&euro; 18,00</p></div>
            <div class="item_en"><p>Beed controfiletto with porcini mushrooms</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it" style="width:274px;"><p>Bistecca alla Fiorentina</p></div><div class="item_it_p" style="width: 158px;"><p>&euro; 4,00 (all'etto/per 100g)</p></div>
            <div class="item_en"><p>Fiorentina steak</p></div><div class="item_en_p"><p></p></div>

        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>
