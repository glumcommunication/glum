<?php
$pagename='contatti';
$pagetitle='Contatti';
include_once 'header.php';
?>


<div id="main">
    <div class="left">
        <h1>Contatti</h1>
        <div id="scrollbarleft" style="height: 428px;display:block;">
            <p style="width:370px;text-align:center;margin: 0 auto;"><img src="images/monteriggionimap.jpg" alt="Mappa Monteriggioni"><br>
                <a href="http://maps.google.it/maps?um=1&ie=UTF-8&q=bar+il+feudo+monteriggioni&fb=1&gl=it&hq=bar+il+feudo&hnear=0x132a2e2747c08bd1:0x844b3df20f3b64cd,Monteriggioni+SI&cid=0,0,13075815162832026272&sa=X&ei=eAhxUb-vNYqf7gbNsIGYAw&ved=0CKsBEPwSMAE" 
               title="Ristorante il Feudo su Google Maps" target="_BLANK" style="text-decoration: underline;">
               Clicca qui</a> per vedere la posizione su Google Maps.<br><br>
            </p>
        <p>
            
            Ristorante Il Feudo<br>
            Piazza Roma, 16 - 53035 Monteriggioni (SI)<br>
            Telefono:  +39 0577 304108 - Cellulare: +39 338 5493093<br>
            Email: <a href="mailto:info@ilfeudoristorante.com">info@ilfeudoristorante.com</a>
            

        </p>
        </div>
    </div>
    <div class="right" id="homeright">

    </div>
</div>

<?php
include_once 'footer.php';
?>