<?php
$pagename = 'dessert';
$pagetitle = 'Menu - Dessert';
include_once 'header.php';
?>


<div id="main">
    <div class="left" style="height:465px;padding: 75px 0 0;">
        <div class="menuitem">
            <p><a href="menu.php" title="Antipasti" <?php if ($pagename == 'antipasti') {echo 'class="highlight"';} ?>>Antipasti</a></p>
        </div>
        <div class="menuitem">
            <p><a href="pizze.php" title="Pizze" <?php if ($pagename == 'pizze') {echo 'class="highlight"';} ?>>Pizze e Ciaccini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="primi.php" title="Primi" <?php if ($pagename == 'primi') {echo 'class="highlight"';} ?>>Primi</a></p>
        </div>
        <div class="menuitem">
            <p><a href="vini.php" title="Vini" <?php if ($pagename == 'vini') {echo 'class="highlight"';} ?>>Vini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="secondi.php" title="Secondi" <?php if ($pagename == 'secondi') {echo 'class="highlight"';} ?>>Secondi</a></p>
        </div>
        <div class="menuitem">
            <p><a href="panini.php" title="Panini" <?php if ($pagename == 'panini') {echo 'class="highlight"';} ?>>Panini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="contorni.php" title="Contorni" <?php if ($pagename == 'contorni') {echo 'class="highlight"';} ?>>Contorni</a></p>
        </div>
        <div class="menuitem">
            <p><a href="dessert.php" title="Dessert" <?php if ($pagename == 'dessert') {echo 'class="highlight"';} ?>>Dessert</a></p>
        </div>    
    </div>
    <div class="right">
        <h1>Contorni <!--<span style="font-style: italic; font-weight: lighter; font-size: 24px;margin-left: 15px;">Appetizers</span>--></h1>
        <div id="scrollbarleft">
            <div class="item_it"><p>Dolci della casa (Vetrina all'interno)</p></div><div class="item_it_p"><p>&euro; 4,00</p></div>
            <div class="item_en"><p>Home made cakes (choose from the desk inside)</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Panforte</p></div><div class="item_it_p"><p>&euro; 4,00 (100 g)</p></div>
            <div class="item_en"><p>(Typical specialty of Siena)</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Vinsanto e Cantucci</p></div><div class="item_it_p"><p>&euro; 6,00</p></div>
            <div class="item_en"><p>(Cookies with sweet wine)</p></div><div class="item_en_p"><p></p></div>
            
            <h2>I nostri gelati artigianali / Home made Ice Cream</h2>
            <div class="item_it"><p>Cioccolato (Chocolate)</p></div><div class="item_it_p"><p></p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Fior di Latte (Vanilla)</p></div><div class="item_it_p"><p></p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Nocciola (Hezelnut)</p></div><div class="item_it_p"><p></p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Fragola (Strawberry)</p></div><div class="item_it_p"><p></p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Yogurth</p></div><div class="item_it_p"><p></p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Limone (Lemon)</p></div><div class="item_it_p"><p></p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Crema (Cream)</p></div><div class="item_it_p"><p></p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Bacio</p></div><div class="item_it_p"><p></p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Caffé (Coffee)</p></div><div class="item_it_p"><p></p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Pistacchio (Pistachio nut)</p></div><div class="item_it_p"><p></p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Stracciatella</p></div><div class="item_it_p"><p></p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Tiramisù</p></div><div class="item_it_p"><p></p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Amarena</p></div><div class="item_it_p"><p></p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Cocco (Coconut)</p></div><div class="item_it_p"><p></p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Ananas (Pineapple)</p></div><div class="item_it_p"><p></p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Melone</p></div><div class="item_it_p"><p></p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p><strong>2 gusti</strong></p></div><div class="item_it_p"><p>&euro; 4,00</p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p><strong>3 gusti</strong></p></div><div class="item_it_p"><p>&euro; 5,00</p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p><strong>4 gusti</strong></p></div><div class="item_it_p"><p>&euro; 6,00</p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>
