<script>
(function($){
    $(document).ready(function(){
        $("#placetext").mCustomScrollbar({
            scrollButtons:{
                enable:false
            },
            scrollInertia:0,
            advanced:{
                updateOnContentResize:true
            },
        });
    })
})(jQuery);
</script>
<div id="placetext">
    <h1>Montalcino</h1>
    <p>
Un'imponente fortezza trecentesca si impone subito allo sguardo dei visitatori che si avvicinano all'antico 
borgo di Montalcino. All'interno della fortezza i nobili senesi si rifugiarono dopo la sconfitta ad opera 
di Medici e Spagnoli nel 1555, mantenendo in vita per quattro anni il sogno della Repubblica Senese. 
La struttura di borgo fortificato posto in cima ad un'altura testimonia l'importanza storica di Montalcino 
come avamposto militare.<br>
Al giorno d'oggi Montalcino è conosciuta in tutto il mondo soprattutto per i vini che si producono nella 
campagna circostante e che hanno fatto della piccola città del Brunello una delle capitali mondiali del vino.
</p>
<p>
I turisti possono unire la visita alle bellezze architettoniche del borgo medioevale – dal Palazzo dei Priori 
con la sua slanciata torre trecentesca alle chiese che vanno spaziano dallo stile romanico e gotico al neoclassico 
del Duomo – alla visita delle numerose cantine della zona. Il tutto senza dimenticare il complesso monastico 
premostratense di Sant'Antimo, che si trova a meno di 9 km dal borgo. Si tratta di un'imponente struttura 
costruita nella versione attuale a partire dal 1118, ma che la leggenda vuole fondata da Carlo Magno. 
Imperdibili e suggestive le funzioni con i canti gregoriani che i monaci tuttora celebrano ogni giorno.
    </p>
</div>