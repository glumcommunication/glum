<?php
$pagename='index';
$pagetitle='Home';
include_once 'header.php';
?>


<div id="main">
    <div class="left" id="homeleft">

    </div>
    <div class="right">
        <h1>Ristorante Il Feudo</h1>
        <div id="scrollbarleft" style="height: 428px;display:block;">
        <p>
            Ubicato dentro lo splendido centro storico di Monteriggioni, terra di passaggio e accoglienza, autentica perla senese 
            circondata da colline e paesaggi da cartolina, il Bar - Ristorante “Il Feudo” è il posto giusto per soddisfare ogni 
            esigenza di palato. Con la sua tradizionale cucina toscana ricca di sapori e sfumature e un'ampia cantina di vini selezionati, 
            il cliente potrà scoprire tutto il gusto e il fascino di un'arte culinaria famosa in tutto il mondo per le sue specialità. 
            Da oltre un decennio "Il Feudo" accoglie i clienti nelle due sale interne e nella suggestiva veranda all'aperto che si 
            affaccia su Piazza Roma, uno dei luoghi simbolo del borgo. Stile e tradizione incontrano la competenza e le buone maniere 
            di una gestione familiare che ha saputo trasformare la buona ristorazione in una passione, prima che in un lavoro. E per 
            tutti gli amanti della buona pizza artigianale e di un rapido e gustoso spuntino, "Il Feudo" è il luogo ideale per una pausa.
        </p>
        <p>&nbsp;</p>
        <p>
            Per chi desidera sostare qualche giorno a Monteriggioni offriamo la possibilità di pernottare all'interno di una location 
            esclusiva con suggestiva visuale sulla piazza principale del Castello Medievale, antico avamposto a difesa della Repubblica 
            di Siena fondato nel 1213. Dalle nostre camere potrete godere di bellissimi scorci panoramici che catturano le suggestive 
            colline del Chianti e di San Gimignano. Per <strong>info e prenotazioni</strong>: 0577 304108 - Federica: 331 8334588.

        </p>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>