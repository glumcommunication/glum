<?php
$pagename='gallery';
$pagetitle='Gallery';
include_once 'header.php';
?>


<div id="main">
    <div class="left">
        <h1>Fotogallery</h1>
        <div class="bigpiccontainer" id="bigpic">
            <img src="images/gallery/feudo_gallery_29.jpg" alt="Mappa Monteriggioni">
            <p style="text-align: center;">Clicca sulle miniature per ingrandirle</p>
        </div>
    </div>
    <div class="right">
        <div id="scrollbarright">
            <div class="galtable">
                <div class="galrow">
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_29.jpg'); return false;"><img src="images/gallery/feudo_gallery_29.jpg" alt="Ristorante Il Feudo" class="picshadow" id="bigpic"></a></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_01.jpg'); return false;"><img src="images/gallery/feudo_gallery_01.jpg" alt="Ristorante Il Feudo" class="picshadow"></a></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_02.jpg'); return false;"><img src="images/gallery/feudo_gallery_02.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                </div>
                <div class="galrow">
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_05.jpg'); return false;"><img src="images/gallery/feudo_gallery_05.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_06.jpg'); return false;"><img src="images/gallery/feudo_gallery_06.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                    <div class="galpic">
                       <a href="" onclick="loadpic('images/gallery/feudo_gallery_07.jpg'); return false;"><img src="images/gallery/feudo_gallery_07.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                </div>
                <div class="galrow">
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_10.jpg'); return false;"><img src="images/gallery/feudo_gallery_10.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_11.jpg'); return false;"><img src="images/gallery/feudo_gallery_11.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_12.jpg'); return false;"><img src="images/gallery/feudo_gallery_12.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                </div>
                <div class="galrow">
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_14.jpg'); return false;"><img src="images/gallery/feudo_gallery_14.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_15.jpg'); return false;"><img src="images/gallery/feudo_gallery_15.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_08.jpg'); return false;"><img src="images/gallery/feudo_gallery_08.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                </div>
                <div class="galrow">
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_17.jpg'); return false;"><img src="images/gallery/feudo_gallery_17.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_23.jpg'); return false;"><img src="images/gallery/feudo_gallery_23.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_24.jpg'); return false;"><img src="images/gallery/feudo_gallery_24.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                </div>
                <div class="galrow">
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_19.jpg'); return false;"><img src="images/gallery/feudo_gallery_19.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_20.jpg'); return false;"><img src="images/gallery/feudo_gallery_20.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_21.jpg'); return false;"><img src="images/gallery/feudo_gallery_21.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                </div>
                <div class="galrow">
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_04.jpg'); return false;"><img src="images/gallery/feudo_gallery_04.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_31.jpg'); return false;"><img src="images/gallery/feudo_gallery_31.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_27.jpg'); return false;"><img src="images/gallery/feudo_gallery_27.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                </div>
                <div class="galrow">
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_25.jpg'); return false;"><img src="images/gallery/feudo_gallery_25.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_28.jpg'); return false;"><img src="images/gallery/feudo_gallery_28.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_30.jpg'); return false;"><img src="images/gallery/feudo_gallery_30.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                </div>
                <div class="galrow">
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_03.jpg'); return false;"><img src="images/gallery/feudo_gallery_03.jpg" alt="Ristorante Il Feudo" class="picshadow"></a></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_09.jpg'); return false;"><img src="images/gallery/feudo_gallery_09.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_13.jpg'); return false;"><img src="images/gallery/feudo_gallery_13.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                </div>
                <div class="galrow">
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_16.jpg'); return false;"><img src="images/gallery/feudo_gallery_16.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_18.jpg'); return false;"><img src="images/gallery/feudo_gallery_18.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                    <div class="galpic">
                        <a href="" onclick="loadpic('images/gallery/feudo_gallery_25.jpg'); return false;"><img src="images/gallery/feudo_gallery_25.jpg" alt="Ristorante Il Feudo" class="picshadow"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>