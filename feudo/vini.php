<?php
$pagename = 'vini';
$pagetitle = "Menu - Vini";
include_once 'header.php';
?>


<div id="main">
    <div class="left" style="height:465px;padding: 75px 0 0;">
        <div class="menuitem">
            <p><a href="menu.php" title="Antipasti" <?php if ($pagename == 'antipasti') {echo 'class="highlight"';} ?>>Antipasti</a></p>
        </div>
        <div class="menuitem">
            <p><a href="pizze.php" title="Pizze" <?php if ($pagename == 'pizze') {echo 'class="highlight"';} ?>>Pizze e Ciaccini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="primi.php" title="Primi" <?php if ($pagename == 'primi') {echo 'class="highlight"';} ?>>Primi</a></p>
        </div>
        <div class="menuitem">
            <p><a href="vini.php" title="Vini" <?php if ($pagename == 'vini') {echo 'class="highlight"';} ?>>Vini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="secondi.php" title="Secondi" <?php if ($pagename == 'secondi') {echo 'class="highlight"';} ?>>Secondi</a></p>
        </div>
        <div class="menuitem">
            <p><a href="panini.php" title="Panini" <?php if ($pagename == 'panini') {echo 'class="highlight"';} ?>>Panini</a></p>
        </div>
        <div class="menuitem">
            <p><a href="contorni.php" title="Contorni" <?php if ($pagename == 'contorni') {echo 'class="highlight"';} ?>>Contorni</a></p>
        </div>
        <div class="menuitem">
            <p><a href="dessert.php" title="Dessert" <?php if ($pagename == 'dessert') {echo 'class="highlight"';} ?>>Dessert</a></p>
        </div>        
    </div>
    <div class="right">
        <h1>Vini <!--<span style="font-style: italic; font-weight: lighter; font-size: 24px;margin-left: 15px;">Appetizers</span>--></h1>
        <div id="scrollbarleft">
            <h2>Vini al bicchiere (Wine by the glass)</h2>
            <div class="item_it"><p>Flute di Prosecco</p></div><div class="item_it_p"><p>&euro; 4,00</p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
                        
            <div class="item_it"><p>Bianco e rosso della casa</p></div><div class="item_it_p"><p>&euro; 3,50</p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Vernaccia di San Gimignano, Chardonnay Toscano, Vermentino</p></div><div class="item_it_p"><p>&euro; 5,00</p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Rosé</p></div><div class="item_it_p"><p>&euro; 4,50</p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Chianti Classico, Morellino di Scansano</p></div><div class="item_it_p"><p>&euro; 4,00</p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Rosso di Montalcino, Nobile di Montepulciano</p></div><div class="item_it_p"><p>&euro; 5,00</p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Chianti Classico Riserva</p></div><div class="item_it_p"><p>&euro; 5,50</p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Grande Rosso Toscano (Supertuscan)</p></div><div class="item_it_p"><p>&euro; 8,00</p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Brunello di Montalcino</p></div><div class="item_it_p"><p>&euro; 9,00</p></div>
            <div class="item_en"><p></p></div><div class="item_en_p"><p></p></div>
            <h2>Vino della casa (rosso o bianco)</h2>
            <div class="item_it"><p>Fiasco (Casalgallo)</p></div><div class="item_it_p"><p>&euro; 8,00</p></div>
            <div class="item_en"><p>1/2 litro</p></div><div class="item_en_p"><p></p></div>
                        
            <div class="item_it"><p>Fiasco (Casalgallo)</p></div><div class="item_it_p"><p>&euro; 15,00</p></div>
            <div class="item_en"><p>1 litro</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Fiasco (Casalgallo)</p></div><div class="item_it_p"><p>&euro; 25,00</p></div>
            <div class="item_en"><p>2 litro</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Caraffa</p></div><div class="item_it_p"><p>&euro; 5,00</p></div>
            <div class="item_en"><p>1/4 litro</p></div><div class="item_en_p"><p></p></div>
            
            <div class="item_it"><p>Caraffa</p></div><div class="item_it_p"><p>&euro; 7,00</p></div>
            <div class="item_en"><p>1/2 litro</p></div><div class="item_en_p"><p></p></div>
            <div class="item_it" style="width:432px;"><p style=" font-size: 12px;">La nostra cantina ci permette inoltre di offrirvi una carta dei vini nazionali ed esteri ricca ed eslcusiva.</p></div>

        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>
