<script>
(function($){
    $(document).ready(function(){
        $("#placetext").mCustomScrollbar({
            scrollButtons:{
                enable:false
            },
            scrollInertia:0,
            advanced:{
                updateOnContentResize:true
            },
        });
    })
})(jQuery);
</script>
<div id="placetext">
    <h1>Siena</h1>
<p>La città di Siena è universalmente riconosciuta come una delle gemme del patrimonio storico-artistico italiano. Il suo centro storico, dichiarato patrimonio dell'Unesco nel 1995, è probabilmente l'esempio meglio conservato di urbanistica medioevale.<br>
Se le origini dell'insediamento senese sono ancora incerte – etrusche, romane, galliche o addirittura egiziane per alcune ricostruzioni piuttosto fantasiose – è certo che il periodo medioevale è stato l'età dell'oro per la cittadina toscana, che grazie ai propri banchieri arrivò a rivaleggiare con Firenze per potenza e splendore. Proprio la rivalità con Firenze condusse al declino la Repubblica Senese, che nel 1559 finì per essere definitivamente asservita ai Medici grazie all'aiuto degli Spagnoli.<br>
Del periodo di splendore è rimasto un enorme patrimonio artistico. Se per i turisti sono tappe obbligate Piazza del Campo con il Palazzo Comunale e la famosissima Torre del Mangia e il maestoso Duomo costruito in marmo bianco e verde, sperimentare davvero Siena significa anche perdersi nel reticolo di ripidi e stretti vicoli medioevali.<br>
La visita al Museo Archeologico, al complesso museale Santa Maria della Scala e alla Pinacoteca nazionale sono necessari per chiunque voglia avere un'idea, seppur incompleta, dell'enorme produzione artistica degli artisti cittadini in  epoca medioevale e rinascimentale.<br>
Parlare di Siena significa parlare anche del Palio, manifestazione che da secoli scalda il cuore dei senesi, divisi nell'amore per 17 contrade, ma anche dei turisti che visitano la città in quei giorni. Ma significa anche parlare delle stupende campagne che circondano la città, punteggiate di innumerevoli aziende agricole che producono vini, olio, formaggi e salumi tra i migliori che si possano trovare.</p>
</div>
         
