<script>
(function($){
    $(document).ready(function(){
        $("#placetext").mCustomScrollbar({
            scrollButtons:{
                enable:false
            },
            scrollInertia:0,
            advanced:{
                updateOnContentResize:true
            },
        });
    })
})(jQuery);
</script>
<div id="placetext">
    <h1>San Gimignano</h1>
        <p>Fu certamente la via Francigena, che taglia in due San Gimignano, a fare la fortuna di quello che inizialmente era soltanto un 
            piccolo insediamento, niente più di un castello. Purtroppo per San Gimignano, come avvenuto anche per molti altri fiorenti 
            insediamenti medioevali, la peste del 1348 decimò la popolazione del borgo turrito, sancendo di fatto l'inizio di un lento 
            declino che la portò sotto l'egemonia fiorentina.
        </p>
        <p>
            La visione delle 16 torri che adornano questo piccolo grande centro danno anche da lontano l'idea della singolarità del 
            posto. Si tratta di quelle sopravvissute fino ai giorni nostri delle ben 72 torri originariamente esistenti, un numero 
            veramente incredibile, che ha spesso fatto paragonare San Gimignano ad una sorta di New York medioevale.<br>Oltre all'atmosfera 
            fiabesca del centro storico con le sue torri, le sue chiese e la antica cinta muraria, San Gimignano offre ai suoi visitatori 
            una ricca e varia rete di musei. Basti pensare che oltre al patrimonio artistico medioevale e rinascimentale e uno spazio espositivo 
            per l'arte contemporanea, ci sono anche un Museo della Vernaccia, un Museo Ornitologico e persino l'esposizione Galèrie Peugeot.
        </p>
        <p>
            Tra le produzioni tipiche ci sono la già citata Vernaccia di San Gimignano, primo vino italiano a fregiarsi della D.O.C. e quella 
            dello zafferano, ritornata recentemente ai fasti del passato.
        </p>
</div>