            <div class="header">
                <div class="container">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="row-fluid">
                            <div class="span3" id="logo">
                                <a href="/standard" title="home"><img src="images/example_logo.png" alt="Example Logo"></a>
                            </div>
                            <div class="span9">

                                <div class="row-fluid">
                                    <div class="span 12">
                                        <div class="navbar">
                                            <div class="navbar-inner">
                                                <div class="container">
 
      <!-- .btn-navbar is used as the toggle for collapsed navbar content -->

                                                    <a class="btn btn-navbar btn-large" data-toggle="collapse" data-target=".nav-collapse" style="margin-bottom: 5px; margin-left: -14px;float:left;">
                                                        Menu <i class="icon-white icon-align-justify"></i>
                                                    </a>   
                                                    <div class="nav-collapse collapse">
                                                
                                                        <ul class="nav">
                                                            <li<? if ($pagename == "home") echo " class=\"active\"";?>><a href="index.php">HOME</a></li>
                                                            <li class="divider-vertical"></li>
                                                            <li<? if ($pagename == "agenzia") echo " class=\"active\"";?>><a href="agenzia.php">CHI SIAMO</a></li>
                                                            <li class="divider-vertical"></li>
                                                            <li<? if ($pagename == "news") echo " class=\"active\"";?>><a href="offerte.php">NEWS</a></li>
                                                            <li class="divider-vertical"></li>
                                                            <li<? if ($pagename == "contatti") echo " class=\"active\"";?>><a href="contatti.php">CONTATTI</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div id="social">
                                            <a href="https://www.facebook.com/pages/Fam-Group-Siena/516454581734289?ref=hl" title ="Facebook"><img src="images/Facebook.png" alt="Facebook" class="socialicons"></a>
                                            <img src="images/Twitter.png" alt="Twitter" class="socialicons">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>