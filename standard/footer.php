    <div class="push"></div>
</div>
<div class="footer">
  <div class="container">
    <p class="credit">Example SpA | P. Iva: 0123456789 | Via Roma 100, 53100 Siena | +39 0577 123456 | 
        <a href="mailto:info@example.com">info@example.com</a></p>
  </div>
</div>
<script src="http://code.jquery.com/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
    $('.carousel').carousel({
        interval: 3000
    })
</script>
</body>
</html>