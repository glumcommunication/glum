<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="images/favicon.png" type="image/svg" />
        <meta name="description" content="<?php echo $pagedesc ?>">
        <meta name="keywords" content="">        
<?php 
if ($pagename == "contatti") {
    echo <<<_END
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    function initialize() {
        var latlng = new google.maps.LatLng(43.318278, 11.333535);
        var settings = {
            zoom: 15,
            center: latlng,
            mapTypeControl: true,
            mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
            navigationControl: true,
            navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
            mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"), settings);

  var companyLogo = new google.maps.MarkerImage('images/positionshadow.png',
    new google.maps.Size(86,58),
    new google.maps.Point(0,0),
    new google.maps.Point(20,50)
);

var companyPos = new google.maps.LatLng(43.318278, 11.333535);
var companyMarker = new google.maps.Marker({
    position: companyPos,
    map: map,
    icon: companyLogo,
    //shadow: companyShadow,
    title:"Example SpA"
});
var contentString = '<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h1 id="firstHeading" class="firstHeading">Example SpA</h1>'+
    '<div id="bodyContent">'+
    '<p>Via Roma, 100 - 53100 Siena<br>Telefono: +39 0577  123456<br>Email: <a href="mailto:info@example.com">info@example.com</a><br>Website: <a href="http://www.example.com" title="Example SpA">www.example.com</a><br></p>'+
    '</div>'+
    '</div>';
 
var infowindow = new google.maps.InfoWindow({
    content: contentString
});
google.maps.event.addListener(companyMarker, 'click', function() {
  infowindow.open(map,companyMarker);
});
    }
    </script>
_END;
}

?>        
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet" media="screen">
        <title>
            Example Site
        </title>

    </head>
<body<? if ($pagename == "contatti") echo " onload=\"initialize()\"";?>>
<div class="wrap">    