<?php
$pagetitle = "Contatti";
$pagename = "contatti";
include_once 'header.php';
include_once 'navigation.php';
?>
<div class="container">
    <div class="row-fluid lastrow">
        <div class="span12 shadowed">
            <div class="container">
                <h2>Contatti</h2>                
            </div>

            <div id="map_canvas" style="width:100%; height:400px;">
            </div>
            <div class="row-fluid lastrow">
                <div class="span6">
                    <h2>Example SpA</h2>
                    <p>
                        Via Roma 100, 53100 Siena
                    </p>
                    <p>
                        Telefono: +39 0577 123456
                    </p>
                    <p>
                        Email: info@example.com
                    </p>
                </div>
                <div class="span6">
                    <h2>Info</h2>
                    <p>Orari d'apertura: 9:00 - 20:00</p>
                    <p>Giorno di chiusura: domenica</p>
                    <p>Note: ....</p>
                </div>
        </div>
    </div>
</div>
</div>
<?php
include_once 'footer.php';
?>