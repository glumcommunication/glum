<?php
$pagetitle = "Home";
$pagename = "home";
include_once 'cbd.php';
include_once 'header.php';
include_once 'navigation.php';
?>

<div class="container shadowed">
    <div id="myCarousel" class="carousel slide" data-interval=2000>
        <ol class="carousel-indicators">
<li data-target="#myCarousel" data-slide-to="0" class="active"></li><li data-target="#myCarousel" data-slide-to="1"></li><li data-target="#myCarousel" data-slide-to="2"></li><li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>
        <!-- Carousel items -->
        <div class="carousel-inner">
        
        
    <div class="active item carouselImageHeight"><a href="offerta?slug=offerta-numero5" title="">
                <img src="../examples/1.JPG" alt="Offerta numero5" class="">
            </a></div>

        
        
        
    <div class="item carouselImageHeight"><a href="offerta?slug=vodafone" title="">
                <img src="../examples/2.JPG" alt="Vodafone" class="">
            </a></div>

        
        
        
    <div class="item carouselImageHeight"><a href="offerta?slug=orologio" title="">
                <img src="../examples/3.JPG" alt="Orologio" class="">
            </a></div>

        
        
        
    <div class="item carouselImageHeight"><a href="offerta?slug=orologio-2" title="">
                <img src="../examples/4.JPG" alt="Orologio 2" class="">
            </a></div>

        
</div>
        <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div>

    <div class="row-fluid lastrow">
        <div class="span6">
            <div class="container ribbon">
                <h2>News</h2>
            </div>
            <h4>Notizia priva di senso</h4>
            <p>
                Il Bayern Monaco ha umiliato il Barcellona sotto gli occhi dei tifosi dell’Allianz Arena. 
                Un 4-0 difficile da prevedere alla vigilia perché ottenuto contro la squadra che negli 
                ultimi anni...
            </p>
            <h4>Questa è ancora più insensata</h4>
            <p>
                Aria di addio tra il Milan e Kevin Prince Boateng, e i fischi di domenica pomeriggio a San 
                Siro centrano ben poco visto che i destini del centrocampista ghanese e dei rossoneri già 
                avevano preso strade...
            </p>

                <a href="offerte.php" title="News"><div class="btn btn-inverse">Vai alla pagina delle News</div></a>

        </div>
        <div class="span6">
            <div class="container ribbon">
                 <h2>Orari</h2>
             </div>
            <p>
                <strong>Dal lunedì al venerdì</strong>: 9:00 - 13:00 14:00 - 18:00
            </p>
            <p>
                <strong>Sabato</strong>: 9:00 - 13:00
            </p>
            <p>
                <strong>Domenica chiusura settimanale</strong>
            </p>
            <p>
                Per prenotazioni chiamare lo <strong>0577 123456</strong>
            </p>

        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>