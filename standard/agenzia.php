<?php
$pagetitle = "Agenzia";
$pagename = "agenzia";
include_once 'header.php';
include_once 'navigation.php';
?>
<div class="container">
    <div class="row-fluid lastrow">
        <div class="span12 shadowed">
            <div class="container">
                <h2>Chi siamo</h2>
            </div>
            
            <p class="justify">
                <img src="../examples/3.JPG" alt="placeholder" class="img-polaroid imageTexted span4"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla auctor, ligula a iaculis euismod, 
                orci orci porttitor sapien, quis vestibulum diam lectus id lectus. Integer faucibus tempor varius. 
                Donec eu ligula id magna porttitor volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                Fusce in nisi euismod, tristique justo quis, tristique nisl. Phasellus mollis gravida volutpat. Nunc 
                vitae bibendum nulla. Integer sodales volutpat elementum. Etiam porttitor luctus massa sit amet 
                laoreet. In arcu velit, ornare at malesuada vestibulum, luctus et augue. Donec eget imperdiet enim. 
                Vestibulum varius lacus nec eros commodo volutpat. Sed pretium ornare quam at euismod.
            </p>

            <p class="justify">
                Mauris convallis eget dui at vulputate. Nullam dignissim, mauris quis molestie ultricies, magna nibh 
                bibendum nisi, in lacinia arcu lorem ac leo. Sed feugiat erat ligula, sit amet consectetur eros gravida 
                in. Quisque sed ligula ipsum. Nullam consectetur lacus mi, et imperdiet ante luctus ut. Nam est risus, 
                sodales sit amet accumsan nec, sagittis eu nunc. Duis auctor velit vel lacinia adipiscing. Pellentesque 
                pretium feugiat elit tristique faucibus. Morbi ipsum libero, suscipit eget tortor vitae, dapibus consequat 
                augue. Nullam sollicitudin, dui id euismod pharetra, tellus diam vestibulum risus, et vulputate enim mauris 
                vestibulum arcu. In hac habitasse platea dictumst.
            </p>

            <p class="justify">
                Maecenas ultricies semper vehicula. Praesent sollicitudin tellus in diam dictum scelerisque. Etiam in gravida 
                erat. Vivamus vehicula sem sit amet risus semper commodo. Morbi id diam et enim sollicitudin hendrerit nec ac 
                erat. Suspendisse gravida nunc mi, a suscipit est mattis eu. Aenean imperdiet posuere nisi nec consectetur. 
                In fermentum eros eu augue facilisis, quis vulputate mauris laoreet. Maecenas quis ipsum quam. In ac porttitor 
                libero, ac laoreet massa. Mauris pellentesque ut lorem vel mollis. Vestibulum eu elit a ipsum sollicitudin 
                tincidunt non vitae eros. Nam est justo, elementum eget erat et, mattis volutpat urna.
            </p>

            <p class="justify">Morbi interdum sapien sit amet iaculis faucibus. Sed quis turpis porttitor, sollicitudin felis 
                convallis, feugiat dolor. Nunc erat diam, vestibulum eu volutpat at, fermentum nec ligula. Ut nec malesuada augue, 
                ac dictum elit. Sed auctor vehicula velit, ut egestas ligula mollis ut. Donec accumsan malesuada arcu at rutrum. 
                Ut nec ornare nunc. Suspendisse potenti. Vivamus fermentum lorem tempus dignissim fermentum. Suspendisse malesuada 
                libero nec ante commodo, eget accumsan nisl tristique. In tincidunt rutrum hendrerit. Sed elementum elementum 
                gravida.
            </p>

            <p class="justify">
                Morbi eleifend fringilla elit, at rhoncus orci interdum sit amet. Quisque quis leo elit. Vestibulum 
                convallis odio at sodales ullamcorper. Phasellus enim tortor, mollis at enim non, molestie hendrerit nisi. 
                Donec a odio vitae nunc interdum tristique sit amet vitae felis. Integer at enim nisi. Nam mi risus, aliquam 
                et ultricies at, interdum vitae mauris. Nunc molestie justo elit, eu sodales lacus laoreet nec. Vestibulum felis 
                st, lobortis id nisi eget, consectetur porttitor dui. Nullam non imperdiet massa. Aliquam sed iaculis libero.
            </p>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>