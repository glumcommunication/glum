<?php
$pagetitle = "News";
$pagename = "news";
include_once 'cbd.php';
include_once 'header.php';
include_once 'navigation.php';
?>
        <div class="container">
            <div class="row-fluid lastrow">
                <div class="span12 shadowed">
                    <div class="container ribbon">
                        <h2>News</h2>
                    </div>
<!--
<?php
	$sql = "SELECT * FROM products ORDER BY Data DESC";
	$result = mysql_query($sql);
	if (!$result) die ("Database access failed: " . mysql_error());
	$rows = mysql_num_rows($result);
	$sum = 0;
	for ($j = 0 ; $j < $rows ; ++$j) {
		$row = mysql_fetch_row($result);
echo <<<_END
                    <div class="row-fluid">
                        <div class="span12">
                            <h4 class="newstitle">
                                $row[0]
                            </h4>
                        </div>
                    </div>
                    <div class="row-fluid lastrow redline">
                        <div class="span8">
                            <img src="$row[2]" alt="$row[0]">
                        </div>
                        <div class="span4 justify">
                            
                                $row[1]
                            
                        </div>
                    </div>
_END;
}
?>
-->
                    <div class="row-fluid">
                        <div class="span12">
                            <h4 class="newstitle">
                                Notizia priva di senso
                            </h4>
                        </div>
                    </div>
                    <div class="row-fluid lastrow">
                        <div class="span3">
                            <img src="../examples/2.JPG" alt="2">
                        </div>
                        <div class="span9 justify">
                            <p>
                                Il Bayern Monaco ha umiliato il Barcellona sotto gli occhi dei tifosi dell’Allianz Arena. 
                                Un 4-0 difficile da prevedere alla vigilia perché ottenuto contro la squadra che negli 
                                ultimi anni ha segnato il limite superiore da provare a raggiungere
                            </p>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12">
                            <h4 class="newstitle">
                                Questa è ancora più insensata
                            </h4>
                        </div>
                    </div>
                    <div class="row-fluid lastrow">
                        <div class="span3">
                            <img src="../examples/1.JPG" alt="1">
                        </div>
                        <div class="span9 justify">
                            <p>
                                Aria di addio tra il Milan e Kevin Prince Boateng, e i fischi di domenica pomeriggio a San 
                                Siro centrano ben poco visto che i destini del centrocampista ghanese e dei rossoneri già 
                                avevano preso strade diverse. Boateng, infatti, ha da tempo comunicato al proprio agente la 
                                volontà di ritornare a giocare in Inghilterra, segnatamente nel Tottenham di Villas-Boas che 
                                da novembre ha messo nel mirino l’ex giocatore del Portsmouth che esplose letteralmente nel 
                                mondiale sudafricano del 2010.
                            </p>
                            <p>
                                Nel Tottenham, tra l’altro, Boateng si è fatto le ossa alle soglie della prima squadra e oggi 
                                vede l’occasione per ritornare questa volta da protagonista nella sua amatissima Londra: la 
                                compagna Melissa Satta pare avallare e anche lei si trasferirebbe nella City. Gli inglesi sono 
                                pronti a garantire a Boateng gli stessi emolumenti annuali percepiti nel Milan arricchiti da un 
                                prolungamento di due anni rispetto all’attuale accordo con il club meneghino.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php
include_once 'footer.php';
?>